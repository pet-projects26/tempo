<?php
/**
 * 自定义的一套参数  
 */
!defined('API') && header("HTTP/1.0 404 Not Found");
return array(
    8404 => array('errCode' => 8404, 'errMsg' => '参数不能为空'),
	8405 => array('errCode' => 8405, 'errMsg' => '加密参数不能为空'),
	8406 => array('errCode' => 8406, 'errMsg' => '加密验证不过'),
	8407 => array('errCode' => 8407, 'errMsg' => '参数要求整形'),
	8408 => array('errCode' => 8408, 'errMsg' => '凭证已存在'),
	

	//数据库错误
	8500 => array('errCode' => 8500, 'errMsg' => '生成订单失败，请重试'),
	8501 => array('errCode' => 8501, 'errMsg' => '查询不到数据'),
	8502 => array('errCode' => 8502, 'errMsg' => '角色信息不存在'),


	//渠道返回参数
	8600 => array('errCode' => 8600, 'errMsg' => '登录不成功'),
	8601 => array('errCode' => 8601, 'errMsg' => '渠道订单生成失败'),
	8602 => array('errCode' => 8602, 'errMsg' => '订单商品不存在'),
	8603 => array('errCode' => 8603, 'errMsg' => '交易信息验证失败'),

);