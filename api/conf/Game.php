<?php
!defined('API') && header("HTTP/1.0 404 Not Found");

// 渠道支付和登录相关参数
class Game
{

    public static $ApplePrdId = array(
        'jzxjc_lszmzjzios_AX' => [
            101 => 'com.lings.zhumz.yb6',
            102 => 'com.lings.zhumz.yb30',
            103 => 'com.lings.zhumz.yb68',
            104 => 'com.lings.zhumz.yb98',
            105 => 'com.lings.zhumz.yb128',
            106 => 'com.lings.zhumz.yb198',
            107 => 'com.lings.zhumz.yb328',
            108 => 'com.lings.zhumz.yb648'
        ]
    );

    //游纵
    public static $youzongios1 = array(
        'AppId' => 'gz811c7bdf1872d865',
        'AppSescret' => '99f80f654592f49c58737147d013503d',
    );

    public static $apiToken = "c5190a3f-2327-4426-b4e5-2d5fabba6c24";

    /**
     * @return string 返回签名
     */
    public static function arithmetic($channel, $package, $account)
    {
        $array['channel'] = $channel;
        $array['package'] = $package;
        $array['account'] = $account;

        $array[] = self::$apiToken;
        //按照首字母大小写顺序排序
        sort($array, SORT_STRING);
        //拼接成字符串
        $str = implode($array);
        //进行加密
        $signature = sha1($str);
        $signature = md5($signature);
        //转换成大写
        $signature = strtoupper($signature);
        return $signature;
    }

    public static function checkSign($sign, $channel, $package, $account)
    {
        $signture = self::arithmetic($channel, $package, $account);

        if ($sign != $signture) {
            return false;
        }
        return true;
    }


    public static function checkSignReturn($sign = '', $channel = '', $package = '', $account = '')
    {
        if (!$channel || !$package || !$account || !Game::checkSign($sign, $channel, $package, $account)) {
            $code = Helper::getCode(8404);
            die($code);
        }
    }


}