<?php

header("Content-Type: text/html; charset=utf-8");

$filename = dirname(__FILE__)."/payPublicKey.pem";
	
	@chmod($filename, 0777);
	@unlink($filename);
$devPubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCoY+vrNNZvOCs83DYbyH9+V9XYUgB96OCvc0ezleoRVn+bCEXeX3MKtAQ3zxATArzIzNzBQvrHSds1A6DAQbBjCf8MVE6L9OyQKaB4tN73eAfrGTYk5ZWHkZ35HrFVow7Qof9PsExxmRqHJERvBd+977xXv9PdnZaWWNl1lY5X5QIDAQAB";

$begin_public_key = "-----BEGIN PUBLIC KEY-----\r\n";
$end_public_key = "-----END PUBLIC KEY-----\r\n";


$fp = fopen($filename,'ab');
fwrite($fp,$begin_public_key,strlen($begin_public_key)); 

$raw = strlen($devPubKey)/64;
$index = 0;
while($index <= $raw )
{
	$line = substr($devPubKey,$index*64,64)."\r\n";
	if(strlen(trim($line)) > 0)
	fwrite($fp,$line,strlen($line)); 
	$index++;
}
fwrite($fp,$end_public_key,strlen($end_public_key)); 
fclose($fp);
?>

