<?php
define('API', true);
define('IN_WEB', true);
define('ROOT', __DIR__ . '/../../center');
define('RPC', ROOT . '/../rpc/');
define('LOG_PATH', realpath(__DIR__ . '/../../logs/api/'));
define('VERSIONPATH', realpath(ROOT . '/../versions/'));
define('SOCKET', ROOT . '/../socket/');
require_once ROOT . '/includes/db.php';
//设置时区
date_default_timezone_set('Asia/Shanghai');
$dirs = array(
    __DIR__,
    realpath(__DIR__ . '/../controller'),
    realpath(__DIR__ . '/../core'),
    ROOT . '/includes',
    ROOT . '/model',
    ROOT . '/redis',
    ROOT . '/library'
);
// 设置PHP INCLUDE 目录
set_include_path(implode(PATH_SEPARATOR, $dirs));
// 自动加载
function autoload($className)
{

    if (strpos($className, '\\')) {
        return;
    }

    include_once "$className.php";
}

spl_autoload_register('autoload');

function getParam($name = '', $default = '')
{
    return isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $default);
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function getClientIp($type = 0, $adv = false)
{
    $type = $type ? 1 : 0;
    static $ip = null;
    if (null !== $ip) {
        return $ip[$type];
    }
    if ($adv) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) {
                unset($arr[$pos]);
            }
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    //IP地址合法验证
    $long = sprintf('%u', ip2long($ip));
    $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}

/**
 * 充值回调IP校验
 * @param array $serverLst
 * @param string $ip
 * @return boolean
 */
function checkIp(array $serverLst, $ip)
{
    if (empty($serverLst) || !is_array($serverLst) || empty($ip)) {
        return false;
    }
    $check_ip_arr = explode('.', $ip);//要检测的ip拆分成数组
    #限制IP
    if (!in_array($ip, $serverLst)) {
        foreach ($serverLst as $val) {
            if (strpos($val, '*') !== false) {//发现有*号替代符
                $arr = array();//
                $arr = explode('.', $val);
                $bl = true;//用于记录循环检测中是否有匹配成功的
                for ($i = 0; $i < 4; $i++) {
                    if ($arr[$i] != '*') {//不等于*  就要进来检测，如果为*符号替代符就不检查
                        if ($arr[$i] != $check_ip_arr[$i]) {
                            $bl = false;
                            break;//终止检查本个ip 继续检查下一个ip
                        }
                    }
                }//end for
                if ($bl) {//如果是true则找到有一个匹配成功的就返回
                    return true;
                }
            }
        }//end foreach
        header('HTTP/1.1 403 Forbidden');
        echo "Access forbidden";
        die;
    }
}


function getClientMac()
{
    @exec("ifconfig -a", $result);

    $temp_array = array();
    $macAddr = array();
    foreach ($result as $value) {
        if (preg_match("/[0-9a-f][0-9a-f][:-]" . "[0-9a-f][0-9a-f][:-]" . "[0-9a-f][0-9a-f][:-]" . "[0-9a-f][0-9a-f][:-]" . "[0-9a-f][0-9a-f][:-]" . "[0-9a-f][0-9a-f]/i", $value,
            $temp_array)) {
            $macAddr = $temp_array[0];
            break;
        }
    }
    unset($temp_array);
    return $macAddr;
}


/**
 * 拆分数据流
 * @param string &data
 * @param string $type = {'normal','json'}
 * @return array
 */
function queryParamToArr(&$data, $type = 'normal')
{
    if (empty($data)) return array();
    $ret = array();
    if ($type == 'normal') {
        //一般格式
        $param = explode('&', $data);
        foreach ($param as $val) {
            $msg = explode('=', $val);
            $ret[$msg[0]] = $msg[1];
        }
    } else if ($type == 'json') {
        $ret = json_decode($data, true, JSON_UNESCAPED_UNICODE);
    } else {
        return $ret;
    }
    return $ret;
}

//是否为https
function is_https()
{
    if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
        return TRUE;
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
        return TRUE;
    } elseif (!empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
        return TRUE;
    }

    return FALSE;
}

//随机生成字符串
function createNonceStr($length = 8)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return "w" . $str;
}

//php防注入和XSS攻击通用过滤
function SafeFilter(&$arr)
{
    $ra = Array('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '/script/', '/javascript/', '/vbscript/', '/expression/', '/applet/'
    , '/meta/', '/xml/', '/blink/', '/link/', '/style/', '/embed/', '/object/', '/frame/', '/layer/', '/title/', '/bgsound/'
    , '/base/', '/onload/', '/onunload/', '/onchange/', '/onsubmit/', '/onreset/', '/onselect/', '/onblur/', '/onfocus/',
        '/onabort/', '/onkeydown/', '/onkeypress/', '/onkeyup/', '/onclick/', '/ondblclick/', '/onmousedown/', '/onmousemove/'
    , '/onmouseout/', '/onmouseover/', '/onmouseup/', '/onunload/');

    if (is_array($arr)) {
        foreach ($arr as $key => $value) {
            if (!is_array($value)) {
                if (!get_magic_quotes_gpc())  //不对magic_quotes_gpc转义过的字符使用addslashes(),避免双重转义。
                {
                    $value = addslashes($value); //给单引号（'）、双引号（"）、反斜线（\）与 NUL（NULL 字符）
                    //加上反斜线转义
                }
                if ($value != 'playerBan') {
                    $value = preg_replace($ra, '', $value);     //删除非打印字符，粗暴式过滤xss可疑字符串
                }
                $arr[$key] = htmlentities(strip_tags($value)); //去除 HTML 和 PHP 标记并转换为 HTML 实体
            } else {
                SafeFilter($arr[$key]);
            }
        }
    }
}

//判断是否base64加密
function is_base64($str)
{
//这里多了个纯字母和纯数字的正则判断
    if (@preg_match('/^[0-9]*$/', $str) || @preg_match('/^[a-zA-Z]*$/', $str)) {
        return false;
    } elseif (is_utf8(base64_decode($str)) && base64_decode($str) != '') {
        return true;
    }
    return false;
}

//判断否为UTF-8编码
function is_utf8($str)
{
    $len = strlen($str);
    for ($i = 0; $i < $len; $i++) {
        $c = ord($str[$i]);
        if ($c > 128) {
            if (($c > 247)) {
                return false;
            } elseif ($c > 239) {
                $bytes = 4;
            } elseif ($c > 223) {
                $bytes = 3;
            } elseif ($c > 191) {
                $bytes = 2;
            } else {
                return false;
            }
            if (($i + $bytes) > $len) {
                return false;
            }
            while ($bytes > 1) {
                $i++;
                $b = ord($str[$i]);
                if ($b < 128 || $b > 191) {
                    return false;
                }
                $bytes--;
            }
        }
    }
    return true;
}
