<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/8/12
 * Time: 20:24
 */

class _9187CheckController extends BaseCheckSession
{
    public static function run($account = '', $uid = '', $AccessToken = '', $package = '', $version = '')
    {
        //根据包号获取包
        $packageData = (new ServerRedis())->getPackageById($package);

        if (empty($packageData) || !$packageData['package_id'] || !$packageData['cdn_url'] || !$packageData['channel']) {
            self::returnData([
                'state' => 0,
                'msg' => 'package error'
            ]);
        }

        self::$channel = $packageData['channel'];
        Helper::log($_REQUEST, self::$channel, 'checkToken');

        $url = _9187Controller::$tokenUrl;

        if (empty($AccessToken) || empty($url) || empty($uid) || empty($account)) {
            self::returnData([
                'state' => 0,
                'msg' => 'Missing parameter'
            ]);
        }

        $post = [];
        //发起请求
        $post['token'] = $AccessToken;

        $post_string = @http_build_query($post);

        $return = Helper::httpRequest($url, $post_string, 'get');

        Helper::log($return, self::$channel, 'return');
        $return = json_decode($return, true, JSON_UNESCAPED_UNICODE);

        if ($return['state'] != 1) {
            self::returnData([
                'state' => 0,
                'msg' => $return['msg']
            ]);
        }

        $returnData = $return['data'];

        //判断接口返回uid是否一致
        if ($uid != $returnData['uid']) {
            self::returnData([
                'state' => 0,
                'msg' => 'Inconsistent uid'
            ]);
        }

        $uid = $returnData['uid'];

        $account = self::makeAccount(self::$channel, $uid);

        $package = $packageData['package_id'];

        //记录注册  返回服务器列表
        $stat = self::register($account, $package, self::$channel);
        $returnData = self::recentServer(self::$channel, $account, $returnData['account'], $package, $version, $stat, '');

        self::returnData([
            'state' => 1,
            'data' => $returnData
        ]);
    }

    /***
     * 返回值处理
     * @param $data
     */
    private static function returnData($data)
    {
        is_array($data) && $data = json_encode($data);

        $channel = self::$channel ? self::$channel : '9187';

        Helper::log($data, self::$channel, 'dieApi[checkToken]');
        echo $data;
        exit;
    }
}