<?php
/**
 * 9130平台sdk基类
 * Created by PhpStorm.
 * User: w
 * Date: 2019/2/12
 * Time: 11:00
 * 入口:
 * 地址/api/sdk/game.php/9130/方法/参数
 */

class _9187Controller extends BaseController
{
    private static $channel = '9187';

    private static $apiName = '';

    public static $tokenUrl = 'http://sdk.9187.com/user/token/';

    //public static $talkSendUrl = 'http://msgapi.sh9130.com/';

    private static $thisServerConfig;

    public static $SecretKey = array(
        '670' => '3C2SwkfeJ4hit8m6MMamh4b4sxi5r3JQ',
    );

    public static $jsSdkHost = [
        'http' => "http://picstatic.dkmol.net/js/aksdk.js",
        'https' => "https://picstatic.dkmol.net/js/aksdk.js"
    ];

    private static $paramsFormat = [
        'payForShip' => [ //普通发货
            'order_id' => ['is_need' => 1, 'is_sign' => 1],
            'uid' => ['is_need' => 1, 'is_sign' => 1],
            'product_id' => ['is_need' => 1, 'is_sign' => 1],
            'money' => ['is_need' => 1, 'is_sign' => 1],
            'role_id' => ['is_need' => 1, 'is_sign' => 1],
            'server_id' => ['is_need' => 1, 'is_sign' => 1],
            'partner_id' => ['is_need' => 1, 'is_sign' => 1],
            'ext' => ['is_need' => 0, 'is_sign' => 1],
            'pkg' => ['is_need' => 1, 'is_sign' => 0],
            'is_test' => ['is_need' => 0, 'is_sign' => 0],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'artificialTransfer' => [ //人工转账
            'order_id' => ['is_need' => 1, 'is_sign' => 1],
            'uid' => ['is_need' => 1, 'is_sign' => 1],
            'money' => ['is_need' => 1, 'is_sign' => 1],
            'role_id' => ['is_need' => 1, 'is_sign' => 1],
            'server_id' => ['is_need' => 1, 'is_sign' => 1],
            'partner_id' => ['is_need' => 1, 'is_sign' => 1],
            'game_pkg' => ['is_need' => 1, 'is_sign' => 0],
            'is_test' => ['is_need' => 0, 'is_sign' => 0],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'playerBan' => [ //玩家封禁
            'game' => ['is_need' => 1, 'is_sign' => 1],
            'server' => ['is_need' => 1, 'is_sign' => 1],
            'roleid' => ['is_need' => 1, 'is_sign' => 1],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'ext' => ['is_need' => 1, 'is_sign' => 0],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'playerShutUp' => [ //玩家禁言
            'game' => ['is_need' => 1, 'is_sign' => 1],
            'server' => ['is_need' => 1, 'is_sign' => 1],
            'roleid' => ['is_need' => 1, 'is_sign' => 1],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'ext' => ['is_need' => 1, 'is_sign' => 0],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ]
    ];

    /***
     * 日志
     * @param $msg
     */
    private static function logs($msg)
    {
        $dir = LOG_PATH . '/sdk/' . self::$channel . '/' . self::$apiName . '/';
        if (!is_dir($dir)) mkdir($dir, 0777, true);
        $name = date('Ymd') . '.log';

        $path = $dir . $name;
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        file_put_contents($path, $contents, FILE_APPEND);
    }

    /***
     * 校验签名
     * @param $params 参数数组
     * @param $sign 验签签名
     * @param string $separator 参数连接符
     * @return bool
     */
    private static function checkSign($params, $sign, $package, $SecretKey = '', $separator = '')
    {
        $params = self::getSignParams($params);

        $forMatArr = self::$paramsFormat[self::$apiName];

        $str = "";
        foreach ($forMatArr as $key => $value) {
            if ($value['is_sign']) {
                $str .= $params[$key] . $separator;
            }
        }

        if ($SecretKey == '') {
            $SecretKey = self::$SecretKey[$package];
        }

        $str .= $SecretKey;

        $mySign = md5($str);

        if ($mySign == $sign) {
            return true;
        }

        return false;
    }

    /***
     * 过滤不参与验签的参数
     * @param $params
     * @return mixed
     */
    private static function getSignParams($params)
    {
        //unset没参与sign的数组
        $forMatArr = self::$paramsFormat[self::$apiName];
        foreach ($params as $key => &$val) {
            if (array_key_exists($key, $forMatArr)) {
                //判断是否为必须
                if (!$forMatArr[$key]['is_sign']) {
                    unset($val);
                }
            }
        }

        return $params;
    }

    /***
     * 校验参数
     * @param $params
     * @return mixed
     */
    private static function getFormatParams($params)
    {
        if (self::$apiName == '') {
            $return['status'] = false;
            $return['emptyKey'] = 'apiName';
            return $return;
        }

        $forMatArr = self::$paramsFormat[self::$apiName];

        $return = ['status' => true, 'params' => $params];

        foreach ($forMatArr as $key => $val) {
            //判断是否为必须
            if ($val['is_need'] && (!array_key_exists($key, $params) || !$params[$key])) {
                self::logs('error: Param - ' . $key . ' is empty');
                $return['status'] = false;
                $return['emptyKey'] = $key;
                return $return;
            }
        }

        return $return;
    }

    /***
     * 返回值处理
     * @param $data
     */
    private function returnData($data, $exit = true)
    {
        is_array($data) && $data = json_encode($data);

        Helper::log($data, self::$channel, 'dieApi[' . self::$apiName . ']');
        echo $data;
        $exit && exit;
    }

    /***
     *封禁玩家接口
     */
    private function ban($banType, $role_id, $time)
    {
        $str = '';
        $type = 0;

        if ($banType == 4) {
            $str = '封号';
            $type = 5;
        }
        if ($banType == 3) {
            $str = '禁言';
            $type = 4;
        }

        $content_name = [intval($role_id)];

        $other = '9130平台发起' . $str . ':' . date('Y-m-d', $time);

        //发包内容
        $sendData = array(
            'opcode' => Pact::$RoleBlockOprate,
            'str' => array(
                intval($banType),
                false,
                [],
                $content_name,
                -1,
                $other
            )
        );

        $Model = new Model();

        $msg = $Model->socketCall(self::$thisServerConfig['websocket_host'], self::$thisServerConfig['websocket_port'], 'roleBlock', $sendData);

        $json = [];

        if ($msg === 0) {

            $server = self::$thisServerConfig;

            $server_id = $server['server_id'];

            $create_time = time();

            //写入单服Mysql
            //获取封禁列表中已存在的封禁对象，如果是已存在的对象则更新，如果不是则写入
            $rs = $Model->call('Ban', 'getBanByTypeName', array('type' => $type, 'name' => $content_name, 'fields' => array('id', 'name', 'type')), $server_id);
            $rs = $rs[$server_id];

            if (empty($rs)) {

                $add_data = array();
                foreach ($content_name as $k => $v) {
                    $name = trim($v);
                    //if(!in_array($name . $type , $nametypes)){
                    $add_data[$k]['name'] = $name;
                    $add_data[$k]['type'] = $type;
                    $add_data[$k]['time'] = -1;
                    $add_data[$k]['reason'] = 0;
                    $other && $add_data[$k]['other'] = $other;
                    $add_data[$k]['user'] = '9130平台';
                    $add_data[$k]['create_time'] = $time;
                    //}
                }

                $ars = $Model->call('Ban', 'addBan', array('data' => $add_data), $server_id);//写入
                $ars = $ars[$server_id];
            } else {
                $nametypes = array();
                foreach ($rs as $row) {
                    $names[$row['id']] = $row['name'];
                    $nametypes[$row['id']] = $row['name'] . $row['type'];
                }

                $update_data = array();
                $update_data['time'] = -1;
                $update_data['reason'] = 0;
                $other && $update_data['other'] = $other;
                $update_data['user'] = '9130平台';
                $update_data['create_time'] = $time;
                $urs = $Model->call('Ban', 'updateBan', array('data' => $update_data, 'id' => array_keys($names)), $server_id);//更新
                $urs = $urs[$server_id];
            }

            $json = ['state' => 1];
        } else {
            $json = ['state' => 0, 'msg' => '封禁失败'];
        }

        return $json;
    }

    /***
     * 充值发货逻辑方法
     * 返回值说明：
     * 1 - 成功
     * 0 - 失败
     * sign = md5(order_id + uid + product_id + money + role_id + server_id + partner_id + ext + time + secret_key)
     * 其中+为字符串链接符号，secret_key由平台方提供，md5取32位小写字符
     */
    public function payForShip()
    {
        $request = $_REQUEST;

        $ServerRedis = new ServerRedis();

        $returnData = [];

        //获取包名对应的包号
        $packageData = $ServerRedis->getPackage($request['pkg']);

        if (empty($packageData) || !$packageData['package_id'] || !$packageData['channel']) {
            $msg = 'error: Package error';
            self::logs($msg);
            $returnData['ret'] = 0;
            $returnData['msg'] = $msg;
            $this->returnData($returnData);
        }

        self::$channel = $packageData['channel'];

        self::$apiName = 'payForShip';

        Helper::log(json_encode($request), self::$channel, self::$apiName);

        $formatParams = self::getFormatParams($request);

        if (!$formatParams['status']) {
            $msg = 'error: params error';
            $returnData['ret'] = 0;
            $returnData['msg'] = $msg;
            $this->returnData($returnData);
        }

        $params = $formatParams['params'];

        //服务器是否存在
        //获取服务器配置
        $serverConfig = $ServerRedis->getServerConfig($params['server_id']);

        if (empty($serverConfig)) {
            $serverConfig = (new ServerModel())->getServer($params['server_id'], ['s.server_id as server_id', 's.name as name', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port']);
        }

        if (empty($serverConfig)) {
            $msg = 'error: The server does not exist';
            self::logs($msg);
            $returnData['ret'] = 0;
            $returnData['msg'] = $msg;
            $this->returnData($returnData);
        }
        self::$thisServerConfig = $serverConfig;

        //验签
        if (!self::checkSign($params, $params['sign'], $packageData['package_id'], $packageData['secret_key'])) {
            $msg = 'error: Verification failure';
            self::logs($msg);
            $returnData['ret'] = 0;
            $returnData['msg'] = $msg;
            $this->returnData($returnData);
        }

        //获取订单信息
        //9130渠道的订单号在ext里
        $ext = json_decode($params['ext'], true, JSON_UNESCAPED_UNICODE);

        //获取订单
        $order = $this->getOrderRow($ext['order_num']);

        //订单数据加入渠道订单
        $order['corder_num'] = $params['order_id'];

        $sta = parent::checkChargeMoney($params['money'], $order, 'fen');

        if (!$sta) {
            $msg = 'error: charge money error';
            self::logs($msg);
            $returnData['ret'] = 0;
            $returnData['msg'] = $msg;
            $this->returnData($returnData);
        }

        //直接返回成功, 再去发货
        $this->returnData(['ret' => 1, 'msg' => ''], false);

        //验证通过后向服务端发货
        $sendRes = parent::sendGoodListen($order);

        $res = $sendRes ? 'success' : 'error';

        $logArr = [
            'status' => $res,
            'orderInfo' => $order
        ];

        self::logs(json_encode($logArr));
        die();
//        if (!$sendRes) {
//            self::logs('error: Failed to send');
//            $this->returnData(8);
//        }
    }

    /***
     * 人工转账逻辑方法
     * 返回值说明：
     * 0 - 成功
     * 1 - 参数错误
     * 2 - 帐号不存在
     * 3 - 接口超时
     * 4 - IP不允许
     * 5 - 订单已发货
     * 6 - 校验失败
     * 7 - 充值金额不匹配
     * 8 - 发货失败
     * 9 - 未知错误
     * sign = md5(order_id + uid + money + role_id + server_id + partner_id + time + secret_key)
     * 其中+为字符串链接符号，secret_key由平台方提供，md5取32位小写字符
     */
    public function artificialTransfer()
    {
        $request = $_REQUEST;

        $ServerRedis = new ServerRedis();

        //获取包名对应的包号
        $packageData = $ServerRedis->getPackage($request['game_pkg']);

        if (empty($packageData) || !$packageData['package_id'] || !$packageData['channel']) {
            self::logs('error: Package error');
            $this->returnData(1);
        }

        self::$channel = $packageData['channel'];

        self::$apiName = 'artificialTransfer';

        Helper::log(json_encode($request), self::$channel, self::$apiName);

        $formatParams = self::getFormatParams($request);

        if (!$formatParams['status']) {
            $this->returnData(1);
        }

        $params = $formatParams['params'];
        /*
                if (!self::checkIP()) {
                    self::logs('error: IP Not allowed');
                    $this->returnData(4);
                }
        */
        //服务器是否存在
        //获取服务器配置
        $serverConfig = $ServerRedis->getServerConfig($params['server_id']);
        if (empty($serverConfig)) {
            self::logs('error: The server does not exist');
            $this->returnData(1);
        }
        self::$thisServerConfig = $serverConfig;

        //验签
        if (!self::checkSign($params, $params['sign'], $packageData['package_id'], $packageData['secret_key'])) {
            self::logs('error: Verification failure');
            $this->returnData(6);
        }

        $params['channel'] = self::$channel;
        $params['package'] = $params['game_pkg'];

        //创建订单
        $orderRes = $this->createOrderRow($params);

        if (!$orderRes['status']) {
            self::logs($orderRes['msg']);
            $this->returnData($orderRes['code']);
        }

        $order = $orderRes['order_data'];

        // self::logs('success');
        $this->returnData(0, false);

        //验证通过后向服务端发货
        $sendRes = parent::sendGoodListen($order);
        $res = $sendRes ? 'success' : 'error';

        $logArr = [
            'status' => $res,
            'orderInfo' => $order
        ];

        self::logs(json_encode($logArr));
        die();
//
//        if (!$sendRes) {
//            self::logs('error: Failed to send');
//            $this->returnData(8);
//        }

    }

    /***
     * 封禁玩家逻辑方法
     * 返回值：JSON
     * 成功：{state:1}
     * 失败：{state:0, msg: ‘xxxx'}
     * sign=md5(game+server+roleid+time+key)
     * 其中key与充值key一致 +号为字符串连接符
     */
    public function playerBan()
    {
        $request = $_REQUEST;

        $returnData = ['state' => 0, 'msg' => ''];

        //将包id放在了ext参数里
        $ext = json_decode($request['ext'], true, JSON_UNESCAPED_UNICODE);

        $ServerRedis = new ServerRedis();

        //获取包号对应的信息
        $packageData =$ServerRedis->getPackageById($ext['package']);

        if (empty($packageData) || !$packageData['package_id'] || !$packageData['channel']) {
            $returnData['msg'] = 'error: Package error';
            $this->returnData($returnData);
        }

        $package = $packageData['package_id'];

        self::$apiName = 'playerBan';
        self::$channel = $packageData['channel'];

        Helper::log(json_encode($request), self::$channel, self::$apiName);

        $formatParams = self::getFormatParams($request);

        if (!$formatParams['status']) {
            $returnData['msg'] = $formatParams['emptyKey'] . ' is empty!';
            $this->returnData($returnData);
        }

        $params = $formatParams['params'];
        /*
                if (!self::checkIP()) {
                    self::logs('error: IP Not allowed');
                    $returnData['msg'] = 'error: IP Not allowed';
                    $this->returnData($returnData);
                }
        */
        //服务器是否存在
        //获取服务器配置
        $serverConfig = $ServerRedis->getServerConfig($params['server']);
        if (empty($serverConfig)) {
            self::logs('error: The server does not exist');
            $returnData['msg'] = 'error: The server does not exist';
            $this->returnData($returnData);
        }
        self::$thisServerConfig = $serverConfig;

        //验签
        if (!self::checkSign($params, $params['sign'], $package, $packageData['secret_key'])) {
            self::logs('error: Verification failure');
            $returnData['msg'] = 'error: Verification failure';
            $this->returnData($returnData);
        }

        //验证通过后向服务端发送协议
        $res = $this->ban(4, $params['roleid'], $params['time']);

        $this->returnData($res);
    }

    /***
     * 禁言玩家逻辑方法
     * 返回值：JSON
     * 成功：{state:1}
     * 失败：{state:0, msg: 'xxxx'}
     * sign=md5(game+server+roleid+time+key)
     * 其中key与充值key一致
     * +号为字符串连接符
     */
    public function playerShutUp()
    {
        $request = $_REQUEST;

        $returnData = ['state' => 0, 'msg' => ''];

        //将包id放在了ext参数里
        $ext = json_decode($request['ext'], true, JSON_UNESCAPED_UNICODE);

        $ServerRedis = new ServerRedis();

        //获取包号对应的信息
        $packageData = $ServerRedis->getPackageById($ext['package']);

        if (empty($packageData) || !$packageData['package_id'] || !$packageData['channel']) {
            $returnData['msg'] = 'error: Package error';
            $this->returnData($returnData);
        }

        $package = $packageData['package_id'];

        self::$channel = $packageData['channel'];
        self::$apiName = 'playerShutUp';

        Helper::log(json_encode($request), self::$channel, self::$apiName);

        $formatParams = self::getFormatParams($request);

        if (!$formatParams['status']) {
            $returnData['msg'] = $formatParams['emptyKey'] . ' is empty!';
            $this->returnData($returnData);
        }

        $params = $formatParams['params'];
        /*
                if (!self::checkIP()) {
                    self::logs('error: IP Not allowed');
                    $returnData['msg'] = 'error: IP Not allowed';
                    $this->returnData($returnData);
                }
        */
        //服务器是否存在
        //获取服务器配置
        $serverConfig = $ServerRedis->getServerConfig($params['server']);
        if (empty($serverConfig)) {
            self::logs('error: The server does not exist');
            $returnData['msg'] = 'error: The server does not exist';
            $this->returnData($returnData);
        }
        self::$thisServerConfig = $serverConfig;

        //验签
        if (!self::checkSign($params, $params['sign'], $package, $packageData['secret_key'])) {
            self::logs('error: Verification failure');
            $returnData['msg'] = 'error: Verification failure';
            $this->returnData($returnData);
        }

        //验证通过后向服务端发送协议
        $res = $this->ban(3, $params['roleid'], $params['time']);

        $this->returnData($res);
    }

    public function accountInfo($channel, $package, $account, $type, $info)
    {

        $accountInfo = [
            1 => 'isTureName',
            2 => 'isBindPhone',
            3 => 'isFollow'
        ];

        $info = json_decode($info, true);
        if ($channel && $package && $account && $type && array_key_exists($type, $accountInfo) && $info) {

            $info = json_decode($info, true);

            $info[$accountInfo[$type]] != 1 && die();

            switch ($type) {
                case '1':
                    if ($info['truename'] == '' || $info['idno'] == '') {
                        die();
                    }
                    break;
                case '2':
                    if ($info['uid'] == '' || $info['phone'] == '') {
                        die();
                    }
                    unset($info['uid']);
                    break;
            }

            $Register = new RegisterModel();

            $rs = $Register->getRow('id', ['channel' => $channel, 'package' => $package, 'account' => $account]);

            $Register->update($info, ['id' => $rs['id']]);
        }
    }
}