<?php
header("Content-Type: text/html;charset=utf-8");
require_once '../../center/includes/Helper.php';
require_once '../conf/Game.php';
class _allchannelPoint {
	public function __construct(){
    
    }
	//private  $url = 'http://log.hardtime.cn:5555/HardtimeGameStatsHttpLog/write.jsp'; 
	
	public static function logs($msg){
        
        $path = LOG_PATH . '/sdk/allchannelpoint/' . date('Ymd') . '.log';
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        @file_put_contents($path , $contents , FILE_APPEND);
    }
    /**
     * 激活
     * @param  [type] $param [description]
     */
    public function active($param){
    	self::logs('激活设备:'.json_encode($param));
    	$arr = array(
    		'channel' => $param['channel'] ,
    		'fr' => $param['version'] ? $param['version'] : '',//系统版本
    		'brand'=> $param['brand'] ? $param['brand'] : '',//品牌
            'mac' => $param['mac'] ? $param['mac'] : '',
            'ip' => getClientIp(),
            'net' => $param['net'] ? $param['net'] : '',//网络
            'imsi' => $param['imsi'] ? $param['imsi'] : '',//设备的imsi
            'res' => $param['res'] ? $param['res'] : '',//分辨率
            'model' => $param['model'] ? $param['model'] : '',//模型
            'ts' => time(),
            'platform' => $param['platform'] ,
            'package' => $param['package'],
    	);

    	if($param['platform'] == 1){
    		$arr['deviceId'] = $param['idfa'] ? $param['idfa'] : '';
    	}else{
    		$arr['deviceId'] = $param['imei'] ? $param['imei'] : '';
    	}
    	
    	$act = new Active_deviceModel();
    	$startup = new StartupdeviceModel();
    	$act->activedevice($arr);
    	echo $startup->add($arr);
    	
    }

//     /**
//      * 注册
//      * @param  [type] $param [description]
//      */
//     public function register($param){

//     	self::logs('注册帐号:'.json_encode($param));
//     	$url = 	'http://api.de0.cc/dc/tracking/rest/register';
//     	$bag = Game::$bag;

//         $channel = $param['r'];//根据渠道号去识别platform
//         if(strpos($channel,'ios')){
//             $param['platform'] = 1;
//         }else{
//             $param['platform'] = 2;
//         }

//     	$arr = array(
//     		'appId' => $bag[$param['package']] ? $bag[$param['package']] : '',
//     		'platform' => $param['platform'] ? $param['platform'] : '',
//     		'channel' => $param['r'] ? $param['r'] : '',
//     		'accountId' => $param['openId'] ? $param['openId'] : '',
//     		'registerTime'=> time(),
//             'ip' => $param['IP'] ? $param['IP'] : '',
//     	);

//     	if($param['platform'] ==1){
//     		$arr['idfa'] = $param['idfa'] ? $param['idfa'] : '';
//     	}else{
//     		$arr['imei'] = $param['imei'] ? $param['imei'] : '';
//     	}

//     	$string = http_build_query($arr);
       
//     	echo Helper::httpRequest($url,$string,'get');
//     }

//      /**
//      * 登录
//      * @param  [type] $param [description]
//      */
//     public function login($param){
//     	self::logs('登录:'.json_encode($param));
//     	$url = 	'http://api.de0.cc/dc/tracking/rest/login';
//     	$bag = Game::$bag;
//     	$arr = array(
//     		'appId' => $bag[$param['package']] ? $bag[$param['package']] : '',
//     		'platform' => $param['platform'] ? $param['platform'] : '',
//     		'channel' => $param['channel'] ? $param['channel'] : '',
//     		'accountId' => $param['accountid'] ? $param['accountid'] : '',
//     		'loginTime'=> time(),
//             'ip' => $param['IP'] ? $param['IP'] : '',
//     	);

//     	if($param['platform'] ==1){
//     		$arr['idfa'] = $param['idfa'] ? $param['idfa'] : '';
//     	}else{
//     		$arr['imei'] = $param['imei'] ? $param['imei'] : '';
//     	}

//     	$string = http_build_query($arr);
//     	echo Helper::httpRequest($url,$string,'get');
//     }
	
// 	/**
//      * 设备在线
//      * @param  [type] $param [description]
//      */
//     public function online($param){

//     	self::logs('设备在线:'.json_encode($param));
//     	$url = 	'http://api.de0.cc/dc/tracking/rest/online';
//     	$bag = Game::$bag;
//     	$arr = array(
//     		'appId' => $bag[$param['package']] ? $bag[$param['package']] : '',
//     		'platform' => $param['platform'] ? $param['platform'] : '',
//     		'channel' => $param['channel'] ? $param['channel'] : '',
//             'ip' => $param['IP'] ? $param['IP'] : '',
//     	);

//     	if($param['platform'] ==1){
//     		$arr['idfa'] = $param['idfa'] ? $param['idfa'] : '';
//     	}else{
//     		$arr['imei'] = $param['imei'] ? $param['imei'] : '';
//     	}

//     	$string = http_build_query($arr);
//     	echo Helper::httpRequest($url,$string,'get');
//     }

//     /**
//      * 付费
//      * @param  [type] $param [description]
//      */
//     public function recharge($param){
//     	self::logs('充值:'.json_encode($param));
//     	$url = 	'http://api.de0.cc/dc/tracking/rest/pay';

//     	$channel = $param['channel'];//根据渠道号去识别platform
//     	if(strpos($channel,'ios')){
//     		$platform = 1;
//     	}else{
//     		$platform = 2;
//     	}
//     	$bag = Game::$bag;
//     	$arr = array(
//     		'appId' => $bag[$param['package']] ? $bag[$param['package']] : '',
//     		'platform' => $platform ? $platform : '',
//     		'channel' => $param['channel'] ? $param['channel'] : '',
//     		'accountId' => $param['accountid'] ? $param['accountid'] : '',
//     		'orderId' => $param['orderID'] ? $param['orderID'] : '',
//     		'payTime' => time(),
//     		'currencyAmount' => $param['Amount'] ? $param['Amount'] : '',
//     		'currencyType' => 'CNY',
//             'ip' => $param['IP'] ? $param['IP'] : '',
//     		//$arr['idfa'] = $param['idfa'] ? ,
//     	);

//     	if($platform ==1){
//     		$arr['idfa'] = $param['idfa'] ? $param['idfa'] : '';
//     	}else{
//     		$arr['imei'] = $param['idfa'] ? $param['idfa'] : '';
//     	}
       
//     	$string = http_build_query($arr);
       
//     	echo Helper::httpRequest($url,$string,'get');
//     }
}
?>