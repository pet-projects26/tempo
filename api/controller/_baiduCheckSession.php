<?php
/**
 * 百度渠道登录接入 
 * 
 */
class _baiduCheckSession extends BaseCheckSession{
    
    /**
     * [获取前端的参数，和百度进行登录较验]
     * @param  string $channel [渠道标识符]
     * @param  string $package [包名]
     * @param  string $version [版本]
     * @return [type]          [返回服务器列表]
     */
    public static function run($channel = '' , $package = '' , $version = ''){
        self::$channel = $channel;
        Helper::log($_REQUEST,  $channel, __FUNCTION__);

        $AccessToken = getParam('AccessToken'); 
        $config = Game::$$channel;
        $url = $config['url'];
        $AppID = $config['AppID'];
        $SecretKey = $config['SecretKey'];

        if(empty($AccessToken) || empty($url) || empty($AppID) || empty($SecretKey)){
            $code = Helper::getCode(8404);
            die($code);
        }

        $Sign  = md5($AppID.$AccessToken.$SecretKey);

        $post = array(
            'AppID' => $AppID,
            'Sign' => $Sign,
            'AccessToken' => $AccessToken,
        );

        $post_string = http_build_query($post);

        $return = Helper::httpRequest($url, $post_string, 'post');

        Helper::log($return,  $channel, 'return');
        $return = json_decode($return,true,JSON_UNESCAPED_UNICODE);

        if($return['ResultCode'] != 1){

            $code = Helper::getCode(8600);
            die($code);
        }
        $content = base64_decode(urldecode($return['Content']));
        $content = json_decode($content,true);
        $uid = $content['UID'];
        $account = self::makeAccount($channel, $uid);
        
        $stat = self::register($account , $package);
        //self::response($account , '' , $package , $version,$stat);
        
        $result =  self::response($account , '' , $package , $version,$stat,array(),false);
        foreach ($result['server'] as $k=>$v) {
        	if (strtotime($v['open_time']) > strtotime('2018-10-01 00:00:00')) {
        		$server[] = $result['server'][$k];
        	}
        }
        $result['server'] = $server;
        die(json_encode($result));

    }
}
