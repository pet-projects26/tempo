<?php
/**
 * 主要用于小米渠道的支付
 */
class _baiduController extends BaseController {
    private $channel  = 'baidu';

    private $filename  = 'baidu'; 
    /**
     * [充值发货]
     * @param  string $channel [description]
     * @return [type]          [description]
     */
    public function send() {
        Helper::log($_REQUEST, $this->filename, __FUNCTION__);
        
        $data = $_REQUEST;

        $AppID = $data['AppID'];
        $OrderSerial = $data['OrderSerial'];//sdk内部订单号
        $CooperatorOrderSerial = $data['CooperatorOrderSerial'];//cp订单号
        $sign = $data['Sign'];
        $Content = base64_decode($data['Content']);
        $content = json_decode($Content,true);
        
        $money = $content['OrderMoney'];//元

        $config = Game::$baidu;

        $SecretKey = $config['SecretKey'];

        
        $mysign = md5($AppID.$OrderSerial.$CooperatorOrderSerial.base64_encode($Content).$SecretKey);
        
        if ($sign != $mysign) {
            $vars = array('AppID' => $AppID, 'ResultCode' => '91' , 'ResultMsg' => '签名错误', 'Sign' => md5($AppID.'91'.$SecretKey));
            die(json_encode($vars));
        } 

        $row = $this->getOrderRow($CooperatorOrderSerial);

        if (empty($row)) {
            $vars = array('AppID' => $AppID, 'ResultCode' => '91' , 'ResultMsg' => '订单不存在', 'Sign' => md5($AppID.'91'.$SecretKey));
            die(json_encode($vars));
        }

        $status = $row['status'];

        if ($status == 1) {
            $vars = array('AppID' => $AppID, 'ResultCode' => '1' , 'ResultMsg' => '发货成功', 'Sign' => md5($AppID.'1'.$SecretKey));
            die(json_encode($vars));
        }

        $sta = parent::checkChargeMoney($money,$row,'yuan');
        if($sta === true){
            $vars = array('AppID' => $AppID, 'ResultCode' => '1' , 'ResultMsg' => '发货成功', 'Sign' => md5($AppID.'1'.$SecretKey));
            print_r(json_encode($vars));
            parent::sendGoodListen($row,$OrderSerial);
        }else{
            $this->updateOrder(array('status' => 2, 'corder_num' => $OrderSerial), $row['order_num']);
            $vars = array('AppID' => $AppID, 'ResultCode' => '91' , 'ResultMsg' => '支付金额错误', 'Sign' => md5($AppID.'91'.$SecretKey));
    
            die(json_encode($vars));
        }


       /* $charge = CDict::$charge[$row['item_id']]['money'];

        if (($charge)  != $money) {
            $this->updateOrder(array('status' => 2, 'corder_num' => $OrderSerial , 'notify_time' => time()), $row['order_num']);
            $vars = array('AppID' => $AppID, 'ResultCode' => '91' , 'ResultMsg' => '支付金额错误', 'Sign' => md5($AppID.'91'.$SecretKey));
    
            die(json_encode($vars));
        }

        //向单服表发货
        $status = $this->sendGoods($row['server'], $row);

        $this->updateOrder(array('status' => $status, 'corder_num' => $OrderSerial , 'notify_time' => time()), $row['order_num']);
        //更新中央服订单表
        if ($status == 1) {
            $vars = array('AppID' => $AppID, 'ResultCode' => '1' , 'ResultMsg' => '发货成功', 'Sign' => md5($AppID.'1'.$SecretKey));
            die(json_encode($vars));
        }

        Helper::log($row['order_num'],  $this->filename,  'fail');

         $vars = array('AppID' => $AppID, 'ResultCode' => '91' , 'ResultMsg' => '发货不成功', 'Sign' => md5($AppID.'91'.$SecretKey));
        die(json_encode($vars));*/

    }
}