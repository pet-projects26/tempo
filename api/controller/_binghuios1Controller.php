<?php
/**
 * @author Administrator
 *
 */

class _binghuios1Controller extends BaseController{

    private $filename,$channel,$config;

    public function __construct(){
        $channel = 'binghuios1';
        $this->channel = $channel;
        $this->filename = $channel;
        $this->config = Game::$$channel;
    }

    public function send(){
        Helper::log($_REQUEST,$this->channel,__FUNCTION__);
        //接收返回参数
        $data = $_REQUEST;
        unset($data['c']);
        unset($data['m']);
        if(empty($data))die('fail');
        
        $strSign = $data['sign'] ;
        unset($data['sign']);
        ksort($data); //ext+game_no+order_money+order_num+time+uid
        //结果就是：extgame_no1order_num146374157387348PAY_KEY
        $md5_str = implode('', $data). $this->config['AppKey'];
        $sign = md5($md5_str);
        if(empty($strSign) || $sign != $strSign){
            die('failure');
        }

        $orderId = $data['game_no'];
        $row = parent::getOrderRow($orderId);
        if(empty($row)){
            die('failure');
        }

        $status = $row['status'];
        if($status == 1){
            die('success');
        }
        $res = parent::checkChargeMoney($data['order_money'],$row);
        if($res === true){ 
            print 'success';
            parent::sendGoodListen($row,$data['order_num']);
        }else{
            parent::updateOrder(array('status'=>2,'corder_id'=>$data['order_num']),$row['order_num']);
            die('failure');
        }
    }
    
}
?>