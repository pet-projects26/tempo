<?php
/**
 * 服务器中心调用
 */
class _centerController extends BaseController {
    private $filename  = 'center'; 

    private $key = 'KUQWHLMERYGCX';

    private $db;

    /**
     * [配置服务器]
     * @return 
     */
    public function configServer() {
        $server       = getParam('server'); //服务器标识符
        $ip           = getParam('ip'); //服务器IP
        $domain       = getParam('domain'); //域名
        $api_url      = getParam('api_url');
        $mysql_host   = getParam('mysql_host');
        $mysql_port   = getParam('mysql_port');
        $mysql_user   = getParam('mysql_user');
        $mysql_passwd = getParam('mysql_passwd');
        $mysql_db     = getParam('mysql_db');
        $mysql_prefix = getParam('mysql_prefix');
        $mongo_host   = getParam('mongo_host');
        $mongo_port   = getParam('mongo_port');
        $mongo_user   = getParam('mongo_user');
        $mongo_passwd = getParam('mongo_passwd');
        $mongo_db     = getParam('mongo_db');
        $login_host   = getParam('login_host');
        $login_port   = getParam('login_port');
        $gm_port      = getParam('gm_port');
        $mdkey        = getParam('mdkey');
        $sign         = getParam('sign');
        $channel_rv   = getParam('channel_rv');

        $data = array();
        if (!empty($server)) {
            $data['server_id'] = $server;
        }
        if (!empty($ip)) {
            $data['ip'] = $ip;
        }

        if (!empty($api_url)) {
            $data['api_url'] = $api_url;
        }

        if (!empty($mysql_host)) {
            $data['mysql_host'] = $mongo_host;
        }

        if (!empty($mysql_port)) {
            $data['mysql_port'] = $mysql_port;
        }

        if (!empty($mysql_user)) {
            $data['mysql_user'] = $mysql_user;
        }

        if (!empty($mysql_passwd)) {
            $data['mysql_passwd'] = $mysql_passwd;
        }

        if (!empty($mysql_db)) {
            $data['mysql_db'] = $mysql_db;
        }

        if (!empty($mysql_prefix)) {
            $data['mysql_prefix'] = $mysql_prefix;
        }

        if (!empty($mongo_host)) {
            $data['mongo_host'] = $mongo_host;
        }

        if (!empty($mongo_port)) {
            $data['mongo_port'] = $mongo_port;
        }

        if (!empty($mongo_user)) {
            $data['mongo_user'] = $mongo_user;
        }

        if (!empty($mongo_passwd)) {
            $data['mongo_passwd'] = $mongo_passwd;
        }

        if (!empty($mongo_db)) {
            $data['mongo_db'] = $mongo_db;
        }

        if (!empty($login_host)) {
            $data['login_host'] = $login_host;
        }

        if (!empty($login_port)) {
            $data['login_port'] = $login_port;
        }

        if (!empty($gm_port)) {
            $data['gm_port'] = $gm_port;
        }

        if (!empty($mdkey)) {
            $data['mdkey'] = $mdkey;
        }
        if(!empty($domain)){
            $data['domain'] = $domain;
        }

        if (empty($sign)) {
            die('error sign');
        }
        isset($channel_rv) && $data['channel_rv'] = $channel_rv;//提审版本号

        $str = $server .'+'. $ip . '+'.$mysql_db . '+'.$mysql_passwd . '+' .$this->key;

        $mySign = md5($server .'+'. $ip . '+'.$mysql_db . '+'.$mysql_passwd . '+' .$this->key);

        if ($mySign != $sign) {
            die ('error sign ckeck to ');
        }
        $this->db = new Model();

        $sql = "SELECT * FROM ny_server WHERE server_id = '{$server}'";

        $row = $this->db->fetchOne($sql);

        if (empty($row)) {
            die('fail to find server');
        }

        $sql = "SELECT * FROM ny_server_config WHERE server_id = '{$server}'";

        $row = $this->db->fetchOne($sql);
        if(empty($data)) die('nothing to change');
        $this->db = new Model('server_config');
        if (empty($row)) {
            $this->db->add($data);
        } else {
            $this->db->update($data, array('server_id' => $server));
        }

        $vars = array('config' => $data,);

        $postData = array(
            'r' => 'call_method',
            'class' => 'ServerconfigController',
            'method' => 'setConfig',
            'params' => json_encode($vars),
        );

        $res = Helper::httpRequest($api_url, http_build_query($postData));

        die('success');
    }

}