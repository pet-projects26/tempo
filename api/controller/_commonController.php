<?php
/***
 * 运维接口
 * Class _commonController
 */

class _commonController
{
    //cdn刷新
    public function refreshCdn()
    {
        $refreshType = (int)getParam('refreshType');
        $dirType = (int)getParam('dirType');

        if (!$refreshType || ($refreshType == 2 && !$dirType)) {
            echo 4000;
            die;
        }

        $args = $_GET; //可变参数
        $args = array_slice($args, 2);

        if (empty($args)) {
            echo 4000;
            die;
        }

        $TencentCdn = new TencentCdn();

        if ($refreshType == 1) {
            $res = $TencentCdn->RefreshCdnUrl($args);
        } else if ($refreshType == 2) {
            $res =  $TencentCdn->RefreshCdnDir($args, $dirType);
        }

        echo $res['code'];
        die;
    }
}