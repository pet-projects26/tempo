<?php

/**
 * 主要是游戏内公用的一些方法，比如说订单号的获取
 *
 */
class _gameController extends BaseController
{
    /**
     * [获取订单号]
     * @param [string] $server [<服务器标识符>]
     * @param [string] $channel [<渠道>]
     * @param [double ] $role_id [<角色ID>]
     * @param [string]  $role_name [<角色名称>]
     * @param [productID] $productID [<产品ID>]
     * @param [string] $account [<帐号>]
     * @param [int] $role_level [<角色等级>]
     * @param [double] $money [<金额>] //单位 元
     * @param [int] $package [<包名>]
     * @param [int] $gold [<元宝>]
     * @param [string] $extra [<额外参数>]
     * @param [string] $sign 加密
     * @return [string] [返回订单号]
     */
    public function getOrder($server, $channel, $role_id, $role_name, $product_id, $account, $role_level, $money, $package, $role_career, $sign, $os = 1, $sdk_name = '9130')
    {
        Game::checkSignReturn($sign, $channel, $package, $account);

        $reqData = [
            'server' => $server,
            'channel' => $channel,
            'role_id' => $role_id,
            'role_name' => $role_name,
            'product_id' => $product_id,
            'account' => $account,
            'role_level' => $role_level,
            'money' => $money,
            'package' => $package,
            'role_career' => $role_career,
            'os' => $os,
            'sdk_name' => $sdk_name
        ];

        //生成订单表
        $datas = parent::makeOrderRow($reqData);

        $charge = $datas['charge'];
        $data = $datas['insert'];

        $channel = $data['channel'];
        $order_num = $data['order_num'];

        $serverRedis = new ServerRedis();

     //   $server = (new ServerModel())->getServer($data['server'], 'name');
        $server = $serverRedis->getServerById($server, $package, $account);

        $extra = array();
        //@todo 特别渠道需求
        $applePrdId = '';
        $os == 2 && $is_ios = true;
        if ($is_ios) {
            //获取该包号的包名
            $packageData = $serverRedis->getPackageById($package);
            $packageName = $packageData['name'];

            $applePrdId = Game::$ApplePrdId[$packageName][$data['item_id']] ? Game::$ApplePrdId[$packageName][$data['item_id']] : '';
        }

        if ($sdk_name == '9130') {
            $extra = json_encode(array('order_num' => $order_num)); //9130渠道的扩展数据里放入游戏订单号

            $_9130PayApiData = [
                'cpbill' => $order_num,
                'productid' => $data['item_id'],
                'productname' => $charge['name'],
                'productdesc' => $charge['desctibe'] ? $charge['desctibe'] : $charge['name'],
                'ApplePrdId' => $applePrdId,
                'serverid' => $data['server'],
                'servername' => $server['name'],
                'roleid' => $data['role_id'],
                'rolename' => $data['role_name'],
                'rolelevel' => $data['role_level'],
                'price' => $data['money'],
                'extension' => $extra
            ];

            $data = $_9130PayApiData;
        } elseif ($sdk_name == '9187') {
            $extra = json_encode(array('order_num' => $order_num)); //9130渠道的扩展数据里放入游戏订单号

            $_9187PayApiData = [
                'cpbill' => $order_num,
                'productid' => $data['item_id'],
                'productname' => $charge['name'],
                'productdesc' => $charge['desctibe'] ? $charge['desctibe'] : $charge['name'],
                'ApplePrdId' => $applePrdId,
                'serverid' => $data['server'],
                'servername' => $server['name'],
                'roleid' => $data['role_id'],
                'rolename' => $data['role_name'],
                'rolelevel' => $data['role_level'],
                'price' => $data['money'],
                'extension' => $extra
            ];

            $data = $_9187PayApiData;
        }

        $vars = array('errCode' => 200, 'msg' => 'success', 'data' => $data);

        // $vars = Helper::chineseJson($vars);

        die(json_encode($vars));
    }

    //获取serverId为test的服务器 如果没有 则返回错误信息
    public function getTestServer($package, $account, $output = 1)
    {
        if (!$account || !$package) {
            $returnData = [
                'errCode' => 400,
                'msg' => 'account or package is empty!'
            ];
            if ($output) {
                die(json_encode($returnData));
            } else {
                return $returnData;
            }
        }

        $serverRedis = new ServerRedis();

        //根据包名获取包号
        $packageData = $serverRedis->getPackageById($package);

        if (empty($packageData) || !$packageData['package_id'] /*|| !$packageData['cdn_url']*/ || !$packageData['channel']) {
            $returnData = [
                'errCode' => 400,
                'msg' => 'package error1!'
            ];

            if ($output) {
                die(json_encode($returnData));
            } else {
                return $returnData;
            }
        }

        $account = BaseCheckSession::makeAccount($packageData['channel'], $account);

        $packageWhite = ['8070', '8080', '8090', '8060', '8010'];

        if (!in_array($package, $packageWhite)) {
            $conditions['WHERE']['account'] = $account;
            if (!(new RegisterModel())->getRow(['id'], $conditions['WHERE'])) {
                $returnData = [
                    'errCode' => 400,
                    'msg' => 'account error!'
                ];

                if ($output) {
                    die(json_encode($returnData));
                } else {
                    return $returnData;
                }
            }
        }

        //记录注册  返回服务器列表
        $stat = BaseCheckSession::register($account, $package, $packageData['channel']);

        $data = BaseCheckSession::recentServer($packageData['channel'], $account, $account, $package, '', $stat, '');
		//die(var_dump($stat));

        $returnData['errCode'] = 200;
        $returnData['msg'] = 'success';
        $returnData['data'] = $data;

        if ($output) {
            die(json_encode($returnData));
        } else {
            return $returnData;
        }
    }

    //获取所有服务器
    public function allServer($channel = '', $package = 0, $account = '', $rvtype = 0, $sign = '')
    {
        Game::checkSignReturn($sign, $channel, $package, $account);

        $server = BaseCheckSession::allServer($package, $account, $rvtype);

        $server['errCode'] = 200;

        die(json_encode($server));
    }

    //更新单个服务器的状态
    public function checkServer($channel = '', $package = 0, $server = '', $account = '', $rvtype = 0, $sign = '')
    {
        Game::checkSignReturn($sign, $channel, $package, $account);

        $server = BaseCheckSession::checkServer($package, $server, $account, $rvtype);

        $return['errCode'] = 200;
        $return['s'] = $server;

        die(json_encode($server));
    }

    //根据包名获取包号
    public function getPackage($packageName)
    {
        if (!$packageName) {
            $returnData = [
                'errCode' => 400,
                'msg' => 'error: package is empty!'
            ];

            die(json_encode($returnData));
        }

        $serverRedis = new ServerRedis();

        $packageData = $serverRedis->getPackage($packageName);

        if (empty($packageData) || !$packageData['package_id'] || !$packageData['channel']) {
            $returnData = [
                'errCode' => 400,
                'msg' => 'error: package is error!'

            ];

            die(json_encode($returnData));
        }

        $returnData = [
            'errCode' => 200,
            'packageData' => $packageData
        ];

        die(json_encode($returnData));
    }

    public function node($channel, $server, $package, $status, $account, $sign, $role_id = 0, $mac = 0)
    {
        if ($channel /*&& $server*/ && $package && $status && $account != 'undefined') {
            Game::checkSignReturn($sign, $channel, $package, $account);
            if ($server) {
                $data = [];
                $ip = getClientIp(0, true);
                $data['channel'] = $channel;
                $data['package'] = $package;
                $data['server'] = $server;
                $data['account'] = $account;
                $data['mac'] = $mac;
                $data['ip'] = $ip;
                $data['create_time'] = time();
                //记录node IP
                $NodeRedis = new NodeRedis();
                if ($status == CDict::$recordStep['StartConnectServer']) {
                    //记录账号历史服
                    if ($server != 'undefined' && $server != 'null') {
                        (new ServerRedis())->AccountRecentServerListChange($package, $server, $account);
                        //活跃redis
                        $NodeRedis->lPushActiveAccountList($channel, $server, $package, $account, $data['create_time'], $data);
                    }
                    //记录活跃
                    //(new ActiveAccountModel())->addActive($channel, $server, $package, $account, $ip, $mac);
                }
                $data['mac'] = $mac ? $mac : 0;
                $data['status'] = $status;
                $data['role_id'] = $role_id ? $role_id : 0;
                $NodeRedis->lPushAccountNodeList($channel, $server, $package, $status, $account, $data);
            }
        }
    }

    public function accountInfo($channel, $package, $account, $type, $info, $sign)
    {
        Game::checkSignReturn($sign, $channel, $package, $account);

        if ($channel == '9130' || $channel == 'fante') {
            (new _9130Controller())->accountInfo($channel, $package, $account, $type, $info);
        }
    }

    public function error($server = '', $account = '', $error = '')
    {

        $fileName = 'server:' . $server . '_clientError';

        $type = 'account:' . $account;

        $error = urldecode($error);

        $error = is_base64($error) ? base64_decode($error) : $error;

        Helper::log($error, $fileName, $type, 'clientError', 'error');
    }
	
	public static function getMyServer($account = '')
    {
		$package = 8070;
		$channel = 1;
        $account = BaseCheckSession::makeAccount($channel, $account);

        $stat = BaseCheckSession::register($account, 9, 9, $package);
        $returnData = BaseCheckSession::recentServer($channel, $account, $account, $package);
		// $returnData['newregister'] = 0;
		//var_dump($returnData);exit;

        echo json_encode([
            'code' => 0,
            'data' => $returnData
        ]);
    }
	
	private static function logs($msg)
    {
        $dir = LOG_PATH . '/sdk/';
        if (!is_dir($dir)) mkdir($dir, 0777, true);
        $name = date('Ymd') . '.log';

        $path = $dir . $name;
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        file_put_contents($path, $contents, FILE_APPEND);
    }
	
	public function recharge()
    {
		// error_reporting(E_ALL);
		// ini_set('display_errors', 'On');
		// $_GET = array (
		  // 'amount' => '600',
		  // 'channel_source' => 'sdk.365nner.com',
		  // 'game_appid' => '3E18B620EF6031062',
		  // 'out_trade_no' => 'SP_202102030223543OFQ',
		  // 'payplatform2cp' => '60.248.238.175',
		  // 'trade_no' => '210203022702E58597235',
		  // 'sign' => '7c0edd0cf5927d209bf5242e0f6aefe8',
		// );
		self::logs(var_export($_GET,true));
		$sign = $_GET['sign'];
        if (!$sign || !$_GET['trade_no']) {
            die('error sign');
        }
		unset($_GET['sign']);
        if ($this->makeSign($_GET) != $sign) {
            // die('error sign');
        }
		
		$time = time();
		$order_num = getParam('trade_no');
		$m = new Model('order_tmp');
		$tmp = $m->getRow("*", "order_num='$order_num'");
		if(null == $tmp){
			die('order not found');
		}
		$amount = getParam('amount');
		$out_trade_no = getParam('out_trade_no');
		if($amount != $tmp['fee']){
			die('order error');
		}
		$order = [
			'role_id'	=>	$tmp['role_id'],
			'account'	=>	$tmp['account'],
			'role_level'	=>	$tmp['role_level'],
			'role_name'	=>	urldecode($tmp['role_name']),
			'server'	=>	$tmp['server_id'],
			'item_id'	=>	$tmp['product_id'],
			'order_num'	=>	$tmp['order_num'],
			'corder_num'=>	$out_trade_no,
			'money'		=>	$tmp['fee']/100,
			'package'		=>   8070,
			'channel'		=>	1,
			'is_test'		=>	0,
			'create_time'	=>	$time,
		];
		// die(var_dump($order));
		$res = $this->createOrderRow($order);
		if($res['status'] == false){
			self::logs($res['msg']."|".var_export($_GET,true));
			die("order repeat");
		}		
	    $sendRes = parent::sendGoodListen($order);
        $res = $sendRes ? 'success' : 'error';
        echo json_encode(['status'=>$res]);
		exit;
    }
	
	function makeSign($data)
	{
		$game_key = 'bHJ04uUY3pQ6kDm8cMSALes';
		// ksort($data);
		// $tmp = [];
		// foreach ($data as $k => $v) {
			// $tmp[] = $k . '=' . $v;
		// }
		// $str = implode('&', $tmp) . $game_key;
		return md5($game_key);
	}
	
	private function returnData($data, $exit)
    {
		 echo json_encode(['code'=>0, 'data'=>$data]);
		 $exit && exit;
    }
	
	 private function returnError($msg, $code=1){
		 echo json_encode(['code'=>1, 'msg'=>$msg]);
		 exit;
	 }
	 
	 public function makeOrder($serverId, $channelId, $roleId, $roleName, $productId, $account, $roleLevel, $price, $package, $channelExt, $game_appid, $sdkloginmodel, $user_id){
		 // echo "<pre>";
		 // self::logs(var_export($_SERVER,true));
		if(empty($serverId) || empty($roleId) || empty($productId))
			 $this->returnError('error params');
		 
		 $cfg = $this->getCharge($serverId);
		 if(empty($cfg[$productId]))
			 $this->returnError('product not exit');
		 
		 $params= [];
		 $param['amount'] 	  	= $cfg[$productId]['price'] * 100;
		 $param['channelExt'] 	= $channelExt;
		 $param['game_appid'] 	= $game_appid;
		 $param['props_name'] 	= $cfg[$productId]['name'];
		 $param['trade_no'] 	= parent::makeOrder($roleId);
		 $param['user_id'] 		= $user_id;
		 $param['sdkloginmodel']= $sdkloginmodel;
		 $param['sign'] 		= $this->makeSign($param);
		 // $param['server_id'] 	= $serverId;
		 // $param['server_name'] 	= $serverId;
		 // $param['role_id'] 		= $roleId;
		 // $param['role_name'] 	= $roleName;
		 // die(var_dump($param));
		 
		 $m = new Model('order_tmp');
		 $data = [
			'account'	=>	$account,
			'role_id'	=>	$roleId,
			'role_name'	=>	$roleName,
			'channel_id'=>	$channelId,
			'fee'		=>	$param['amount'],
			'server_id'	=>	$serverId,
			'product_id'=>	$productId,
			'order_num'		=>	$param['trade_no'],
			'create_time'	=>	$_SERVER['REQUEST_TIME'],
		 ];
         $m->add($data);
		 // self::logs(var_export(['code'=>0, 'data'=>$param],true));
		 echo json_encode(['code'=>0, 'data'=>$param]);
		 exit;
	 }
}
