<?php
/**
 * 游戏内常用方法
 */
class _indexController extends BaseController{
    private $_key  = 'Y0FG2GR8';

    public function index() {
    }

    /**
     * [公告]
     * @return [type] [description]
     */
    public function notice () {
        $package = getParam('package');
        $server  = getParam('server');
        $tk      = getParam('tk');

        if (empty($server)) {
            $vars = Helper::getCode(8404);
            die($vars);
        }

        if (empty($tk)) {
            $vars = Helper::getCode(8404);
            die($vars);
        }

        $myTk = md5($package . '&'. $server . "&" . $this->_key);

        if ($myTk != $tk) {
            $vars = Helper::getCode(8405);
            die($vars);
        }

        /*if($package == 0 && $server == 248){
            $package = 1010101;//兼容248
        }else if ($package == 0 && $server == 'world0001'){
            $package = 1010101;//兼容外网测试服
        }*/

        if($package == 0){
            $package = 1010101;
        }

        if (!empty($package)) {
            $sql = "SELECT * FROM ny_package WHERE package_id = '{$package}' LIMIT 1";
            $db = new Model();
            $row  = $db->fetchOne($sql);

            if (empty($row)) {
                $vars = Helper::getCode(8501);
                die($vars);
            }

            $group_id = $row['group_id'];
        } else {
            $group_id = 0;
        }


        $time = time();

        $sql = "SELECT id, contents, title, groups, type, servers FROM ny_notice 
                    WHERE (
                            (
                                `groups` = '' AND  (`servers` =  '' OR FIND_IN_SET('{$server}', `servers`))
                            )
                        OR 
                        (
                            FIND_IN_SET('{$group_id}', `groups`) AND (`servers` =  '' OR FIND_IN_SET('{$server}', `servers`))
                        )
                    ) 
                 AND start_time < {$time} AND ((end_time = 0) OR (end_time > {$time})) 
                    ORDER BY sort DESC, create_time DESC, title ASC ";//AND status = 1

                    

        $res = (new Model())->query($sql);

        //@todo 查询活动
        $vars = array (
            'document' => array(
                'table' => 'bg_activity',
                'data' => array(
                    'state' => 0,
                    'tm.start' => array('$lt' => $time),
                    'tm.close' => array('$gt' => $time),
                ),
            )
        );

       
        $postData = array(
            'r' => 'call_method',
            'class' => 'SqlModel',
            'method' => 'getMongo',
            'params' => json_encode($vars),
        );

        $instance  = new ServerconfigModel();
        
        $urls = $instance->getApiUrl($server, true);

        $rs = Helper::rolling_curl($urls, $postData);

        $rs = isset($rs[$server]) ? json_decode($rs[$server], true) : array();

        $act = array();
        if(!empty($rs)){
            foreach ($rs as $k => $row) {
                array_push($act, $row['name']);
            }  
        }
        
        //规则，同一个标题只会出现一次，当选定具体渠道时，优先选择
        $arr = array();
        if(!empty($res)){
            foreach ($res as $k => $row) {
                $title   = $row['title'];
                $groups  = $row['groups'];
                $servers = $row['servers'];
                $type    = $row['type'];

                //活动
                if ($type == 2 ) {

                    if (!in_array($title, $act)) {
                        continue;
                    }
                }

                $k = 'def';

                if (!empty($groups)) {
                    $k = 'spe';
                } 

                if (!empty($servers)) {
                    $k = 'spe';
                }

                $arr[$title][$k] = $row;
            }
        }
        

        $list = array();

        foreach ($arr as $k => $row) {
            $list[$k] = isset($row['spe']) ? $row['spe']['contents'] : $row['def']['contents'];
        }

        $data = array('title' => array_keys($list), 'content' => array_values($list));

        $vars = array('errCode' => 200, 'errMsg' => 'success', 'data' => $data);
        die(Helper::chineseJson($vars));
    } 

   
}
