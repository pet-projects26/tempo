<?php
class _lzgCheckSession extends BaseCheckSession{
    /**
     * [入口文件，主要跟渠道进行登录较验]
     * @param  string $channel [渠道标识符]
     * @param  string $package [满道包标识符]
     * @param  string $version [版本号]
     * @return [type]          [返回服务器列表]
     */
    public static function run($channel = '' , $package = '' , $version = ''){
        self::$channel = $channel;
        self::logs(json_encode($_REQUEST));

        $uid = getParam('uid');
        $session_id = getParam('session_id');

        if($uid == ''){
            trigger_error('no uid' , E_USER_WARNING);
            exit('no uid');
        }

        if (empty($session_id)) {
            trigger_error('no session_id');
            exit('no session_id');
        }

        $post_data = array(
            'uid' => $uid,
            'session_id' => $session_id,
            'pid' => 1,
        );

        $post_string = urlencode(http_build_query($post_data));


        $schannel = strtolower($channel);
        
        if (!isset(Game::$$schannel)) {
            exit('no config');
        }

        $config = Game::$$schannel;

        $url = $config['Url'];

        $code = Common::httpRequest($url, $post_string);

        if ($code === '') {
            exit('session_id error');
        }

        if ($code == 0) {
            exit('session time out');
        }

        if ($code != 1) {
            exit('unknow error' .$code);
        }

        $account = self::makeAccount($uid, $channel);
        self::register($account , $package);
        self::response($account , $uid , $package , $version);
    }
}
