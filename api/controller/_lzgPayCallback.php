<?php

class _lzgPayCallback extends BasePayCallback
{

    public static function run($channel = '')
    {
        self::$channel = $channel;
        $order = $_POST;

        $params = json_encode($order);
        Helper::log($params);

        $sid = isset($_POST['sid']) ? intval($_POST['sid']) : 0; //服ID
        $orderid = isset($_POST['orderid']) ? trim($_POST['orderid']) : ''; //订单号
        $cash = isset($_POST['cash']) ? floatval($_POST['cash']) : 0.00; //玩家充值金额
        $money = isset($_POST['money']) ? floatval($_POST['money']) : 0.00; //实际收到的money
        $amount = isset($_POST['amount']) ? intval($_POST['amount']) : 0; //充值游戏币数量

        $schannel = strtolower($channel);
        if (isset(Game::$$schannel)) {
            self::log('no config', 'error');
            exit(0);
        }

        $config = Game::$$schannel;

        $key = $config['PayKey'];

        $mark = self::checkSign($order, $key);
        if (!$mark) {
            self::log('sign error ', 'error');
            exit(0);
        }

        if (empty($sid)) {
            self::log('sid error', 'error');
            exit(0);
        }

        $serverRow = self::findServerById($sid, 'server');

        if (!empty($serverRow)) {
            self::log('no server', 'error');
            exit(0);
        }

        $server = $serverRow['server_id'];

        $extra = isset($_POST['extra']) ? trim($_POST['extra']) : '';

        $info = explode('|', $extra);

        $role_id = $info[0];
        $item_id = $info[1];

        $data = array(
            'server' => $server,
            'order_num' => $orderid,
            'money' => $money,
            'role_id' => $role_id,
            'item_id' => $item_id //商品序号
        );

        self::send($channel, $orderid, $data, $params);
        exit(1); //成功
    }

    /**
     * [ckeckSign description]
     * @return [type] [description]
     */
//    private static function ckeckSign($params, $key) {
//        $sign = $params['sign'];
//        unset($params['sign']);
//
//
//        ksort($params);
//
//        $str = '';
//        foreach ($params as $k => $v) {
//            $str .= $k.'='.$v;
//        }
//
//        $str .= $str.$key;
//
//        $mySign = md5($str);
//
//        if ($sign != $mySign) {
//            return false;
//        }
//
//        return true;
//    }
}