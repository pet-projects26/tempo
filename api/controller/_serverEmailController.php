<?php
/**
 * 邮件发送接口
 * Created by PhpStorm.
 * User: w
 * Date: 2019/5/27
 * Time: 10:06
 */

class _serverEmailController extends BaseController
{
    private $Host = "smtp.exmail.qq.com"; //smtp服务器的名称
    private $Port = 465;
    private $SMTPAuth = TRUE; //启用smtp认证
    private $Username = 'chandao@joytime1.com'; //你的邮箱名
    private $Password = 'Jthy2018'; //邮箱密码(邮箱授权码)
    private $From = 'chandao@joytime1.com'; //发件人地址（也就是你的邮箱地址）
    private $FromName = '九天互娱'; //发件人姓名

    //服务端发送通知邮件接口
    public function run($content = '', $isApi = 1)
    {
        if (!$content) {
            if ($isApi) {
                die();
            } else {
                return false;
            }
        }

        $content = urldecode($content);

        if (is_base64($content)) $content = base64_decode($content);

        //$content = urldecode($content);

        require_once ROOT . "/includes/class.phpmailer.php";
        require_once ROOT . "/includes/class.smtp.php";

        $mail = new PHPMailer(); //实例化
        $mail->SMTPDebug = 1;
        $mail->IsSMTP(); // 启用SMTP
        $mail->Host = $this->Host; //smtp服务器的名称（这里以QQ邮箱为例）
        $mail->Port = $this->Port;
        $mail->SMTPAuth = $this->SMTPAuth; //启用smtp认证
        $mail->SMTPSecure = 'ssl';
        $mail->Username = $this->Username; //你的邮箱名
        $mail->Password = $this->Password; //邮箱密码(邮箱授权码)
        $mail->From = $this->From; //发件人地址（也就是你的邮箱地址）
        $mail->FromName = $this->FromName; //发件人姓名
        $mail->WordWrap = 50; //设置每行字符长度
        $mail->IsHTML(TRUE); // 是否HTML格式邮件
        $mail->CharSet = 'utf-8'; //设置邮件编码
        $mail->Subject = '系统通知'; //邮件主题
        $mail->AltBody = "这个是不支持HTML显示的邮件内容"; //邮件正文不支持HTML的备用显示
        $mail->Body = $content;

        //获取所有管理员的邮箱
        $User = $this->mailUser();

        if (empty($User)){
            if ($isApi) {
                die();
            } else {
                return false;
            }
        }

        foreach ($User as $key => $val) {
            $rs = filter_var($val['email'], FILTER_VALIDATE_EMAIL);
            if ($rs) {
                $mail->AddAddress($rs, $val['name']);
            }
        }

        $mail->send(); //发送邮件, 不需要返回值
    }

    public function mailUser()
    {
        $Model = new Model();

        $time = time();
        $sql = 'select `name`, `email` from ny_user where email != "" and status = 1 and (expiration > ' . $time . ' or expiration = 0)';
        $User = $Model->query($sql);

        return $User;
    } 

}