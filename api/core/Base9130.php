<?php
/**
 * 9130平台sdk基类
 * Created by PhpStorm.
 * User: w
 * Date: 2019/1/16
 * Time: 15:11
 */

class Base9130 extends BaseController
{
    protected static $channel = '9130';

    protected static $apiName = 'payForShip';

    public static $tokenUrl = 'http://sdk.sh9130.com/user/token/';

    public static $talkSendUrl = 'http://msgapi.sh9130.com/';

    protected static $thisServerConfig;

    protected static $SecretKey = 'nGbhQMYdrYFQmh4tw3cX2h84H2pDWHKG';

    public static $jsSdkHost = array(
        'https' => 'https://static.sh9130.com/js/aksdk.js',
        'http' => 'http://static.sh9130.com/js/aksdk.js'
    );

    //jsSDK
    public static $jsSDKName = 'AKSDK';


    protected static $paramsFormat = [
        'payForShip' => [ //普通发货
            'order_id' => ['is_need' => 1, 'is_sign' => 1],
            'uid' => ['is_need' => 1, 'is_sign' => 1],
            'product_id' => ['is_need' => 1, 'is_sign' => 1],
            'money' => ['is_need' => 1, 'is_sign' => 1],
            'role_id' => ['is_need' => 1, 'is_sign' => 1],
            'server_id' => ['is_need' => 1, 'is_sign' => 1],
            'partner_id' => ['is_need' => 1, 'is_sign' => 1],
            'ext' => ['is_need' => 1, 'is_sign' => 1],
            'game_pkg' => ['is_need' => 1, 'is_sign' => 0],
            'is_test' => ['is_need' => 0, 'is_sign' => 0],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'artificialTransfer' => [ //人工转账
            'order_id' => ['is_need' => 1, 'is_sign' => 1],
            'uid' => ['is_need' => 1, 'is_sign' => 1],
            'money' => ['is_need' => 1, 'is_sign' => 1],
            'role_id' => ['is_need' => 1, 'is_sign' => 1],
            'server_id' => ['is_need' => 1, 'is_sign' => 1],
            'partner_id' => ['is_need' => 1, 'is_sign' => 1],
            'game_pkg' => ['is_need' => 1, 'is_sign' => 0],
            'is_test' => ['is_need' => 0, 'is_sign' => 0],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'playerBan' => [ //玩家封禁
            'game' => ['is_need' => 1, 'is_sign' => 1],
            'server' => ['is_need' => 1, 'is_sign' => 1],
            'roleid' => ['is_need' => 1, 'is_sign' => 1],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'ext' => ['is_need' => 1, 'is_sign' => 0],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'playerShutUp' => [ //玩家禁言
            'game' => ['is_need' => 1, 'is_sign' => 1],
            'server' => ['is_need' => 1, 'is_sign' => 1],
            'roleid' => ['is_need' => 1, 'is_sign' => 1],
            'time' => ['is_need' => 1, 'is_sign' => 1],
            'ext' => ['is_need' => 1, 'is_sign' => 0],
            'sign' => ['is_need' => 1, 'is_sign' => 0]
        ],
        'checkToken' => [ //登录注册验签
            'account' => ['is_need' => 1],
            'uid' => ['is_need' => 1],
            'token' => ['is_need' => 1]
        ],
        'jump' => [
            'channel' => ['is_need' => 1],
            'server' => ['is_need' => 0],
        ]
    ];

    public static function logs($msg, $apiName = '')
    {
        $apiName = $apiName ? $apiName : self::$apiName;

        $dir = LOG_PATH . '/sdk/' . self::$channel . '/' . $apiName . '/';
        if (!is_dir($dir)) mkdir($dir, 0777, true);
        $name = date('Ymd') . '.log';

        $path = $dir . $name;
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        file_put_contents($path, $contents, FILE_APPEND);
    }

    /***
     * 校验签名
     * @param $params 参数数组
     * @param $sign 验签签名
     * @param string $separator 参数连接符
     * @return bool
     */
    public static function checkSign($params, $sign, $separator = '')
    {
        $params = self::getSignParams($params);

        $forMatArr = self::$paramsFormat[self::$apiName];

        $str = "";
        foreach ($forMatArr as $key => $value) {
            if ($value['is_sign']) {
                $str .= $params[$key] . $separator;
            }
        }

        $str .= self::$SecretKey;

        $mySign = md5($str);

        if ($mySign == $sign) {
            return true;
        }

        return false;
    }

    /***
     * 过滤不参与验签的参数
     * @param $params
     * @return mixed
     */
    protected static function getSignParams($params)
    {
        //unset没参与sign的数组
        $forMatArr = self::$paramsFormat[self::$apiName];
        foreach ($params as $key => &$val) {
            if (array_key_exists($key, $forMatArr)) {
                //判断是否为必须
                if (!$forMatArr[$key]['is_sign']) {
                    unset($val);
                }
            }
        }

        return $params;
    }

    /***
     * 校验参数
     * @param $params
     * @return mixed
     */
    protected static function getFormatParams($params)
    {
        $forMatArr = self::$paramsFormat[self::$apiName];

        $return = ['status' => true, 'params' => $params];

        foreach ($forMatArr as $key => $val) {
            //判断是否为必须
            if ($val['is_need'] && (!array_key_exists($key, $params) || !$params[$key])) {
                self::logs('error: Param - ' . $key . ' is empty');
                $return['status'] = false;
                $return['emptyKey'] = $key;
                return $return;
            }
        }

        return $return;
    }

    /***
     * 返回值处理
     * @param $data
     */
    protected function returnData($data)
    {
        is_array($data) && $data = json_encode($data);

        Helper::log($data, self::$channel, 'DIE-API');
        echo $data;
        exit;
    }

    /***
     *封禁玩家接口
     */
    protected function ban($banType, $role_id, $time)
    {
        $str = '';
        $type = 0;

        if ($banType == 4) $str = '封号' && $type = 5;
        if ($banType == 3) $str = '禁言' && $type = 4;

        $content_name = [intval($role_id)];

        $other = '9130平台发起' . $str . ':' . date('Y-m-d');

        //发包内容
        $sendData = array(
            'opcode' => Pact::$RoleBlockOprate,
            'str' => array(
                intval($banType),
                false,
                [],
                $content_name,
                intval($time),
                $other
            )
        );
        $msg = (new Model())->socketCall(self::$thisServerConfig['websocket_host'], self::$thisServerConfig['websocket_port'], 'roleBlock', $sendData);

        $json = [];

        if ($msg === 0) {

            $server = self::$thisServerConfig;

            $server_id = $server['server_id'];

            $create_time = time();

            //写入单服Mysql
            //获取封禁列表中已存在的封禁对象，如果是已存在的对象则更新，如果不是则写入
            $rs = $this->call('BanModel', 'getBanByTypeName', array('type' => $type, 'name' => $content_name, 'fields' => array('id', 'name', 'type')), $server_id);
            $rs = $rs[$server_id];

            if (empty($rs)) {

                $add_data = array();
                foreach ($content_name as $k => $v) {
                    $name = trim($v);
                    //if(!in_array($name . $type , $nametypes)){
                    $add_data[$k]['name'] = $name;
                    $add_data[$k]['type'] = $type;
                    $add_data[$k]['time'] = $time;
                    $add_data[$k]['reason'] = 0;
                    $other && $add_data[$k]['other'] = $other;
                    $add_data[$k]['user'] = '9130平台';
                    $add_data[$k]['create_time'] = $create_time;
                    //}
                }

                $ars = $this->call('BanModel', 'addBan', array('data' => $add_data), $server_id);//写入
                $ars = $ars[$server_id];
            } else {
                $nametypes = array();
                foreach ($rs as $row) {
                    $names[$row['id']] = $row['name'];
                    $nametypes[$row['id']] = $row['name'] . $row['type'];
                }

                $update_data = array();
                $update_data['time'] = $time;
                $update_data['reason'] = 0;
                $other && $update_data['other'] = $other;
                $update_data['user'] = '9130平台';
                $update_data['create_time'] = $create_time;
                $urs = $this->call('BanModel', 'updateBan', array('data' => $update_data, 'id' => array_keys($names)), $server_id);//更新
                $urs = $urs[$server_id];
            }

            $json = ['state' => 1];
        } else {
            $json = ['state' => 0, 'msg' => '封禁失败'];
        }

        return $json;
    }

}