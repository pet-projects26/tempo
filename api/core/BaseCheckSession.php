<?php

interface CheckSession
{
    public static function run();
}

abstract class BaseCheckSession implements CheckSession
{
    protected static $channel;

    //检查同个udid不能在不同渠道上创角
    public static function checkudid($channel, $idfa, $imei)
    {
        if (!$channel || (!$idfa && !$imei)) exit('{"errCode":"0001","errMsg":"检查同个udid时包号和idfa或者imei不能为空"}');
        $udid = !empty($idfa) ? $idfa : $imei;
        $mongo = Util::mongoConn(SERVER_MONGO_HOST, SERVER_MONGO_PORT, SERVER_MONGO_USER, SERVER_MONGO_PASSWD, SERVER_MONGO_DB);
        $result = $mongo->client_udid->findOne(['channel' => $channel, 'udid' => $udid], ['_id' => 0, 'create_time' => 1]);
//     	$result = (new ClientModel())->getRow(['create_time'],['udid'=>$udid,'channel'=>$channel],['ORDER'=>'create_time DESC']);
        if ($result['create_time']) {
            //读取时间(分钟),默认60分钟
            $time = file_get_contents('/tmp/udid_deadline.txt');
            $time = $time ? $time : 60;
            $deadline = time() - $result['create_time'];
            if (!($deadline > $time * 60)) exit('{"errCode":"0001","errMsg":"检查到您手机上已经安装过其他包了,请联系管理员处理"}');
        }
    }

    public static function logs($msg)
    {

        $path = LOG_PATH . '/sdk/' . self::$channel . '/check_session/' . date('Ymd') . '.log';
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        @file_put_contents($path, $contents, FILE_APPEND);
    }

    //返回服务器列表(mysql)
    public static function response1($channel, $account, $nick_name = '', $package = '', $version = '', $stat = '', $extra = array(), $output = true)
    {
        $data = array();
        $zone = (new ZoneModel())->getZone(array(), array('id', 'name'));
        $data['zone'] = array();
        foreach ($zone as $k => $row) {
            $data['zone'][$row['id']] = $row['name'];
        }

        //提审
        $versionRecord = self::reviewTypes($package);//包号获取渠道提审信息
        $type = 0;
        $data['rvtype'] = 0;
        $data['rvparam'] = array();

        if (!is_null($versionRecord['rv_num']) && $versionRecord['rv_num'] == $version && defined('IDFA') && !is_null(IDFA)) {
            $type = 1;//通过版本号，得到该渠道组是否需要提审
            $data['rvtype'] = $versionRecord['type'];
            $data['rvparam'] = !empty($versionRecord['rv_param']) ? json_decode($versionRecord['rv_param']) : array();//提审素材
        }
        $data['server'] = self::getServer($package, $account, $type);

        $serverData = [];
        $status = [2, 3, 4, 5];
        if ($type != 1) {
            foreach ($data['server'] as $k => $v) {
                if ($stat && in_array($v['status'], $status)) {

                    continue;
                } else {
                    $serverData[] = $data['server'][$k];
                }
            }
        }
        unset($data['server']);
        $data['server'] = $serverData;
        $data['newregister'] = $stat ? 1 : 0;

        //历史服务器
        if (!$stat) {
            $recentServer = self::getRecentServer($account, $package);
            $data['recentServer'] = $recentServer;
        }

        //渠道区分
        //uid渠道的帐号
        $data['user'] = array(
            'openId' => $account,
            'nickName' => $nick_name,
            'uid' => str_replace($channel . '_', '', $account),
        );

        $data['package'] = $package;

        //需要在外层赋值
        $data['extra'] = $extra;

        $data['notice'] = self::notice($package);
        if ($output) {
            die(json_encode($data));
        } else {
            return $data;
        }
    }

    /**
     * [公告]
     * @param  [type] $package [包]
     * @return [type]          [description]
     */
    public static function notice($package)
    {
        $sql = "SELECT `group_id` FROM ny_package WHERE package_id = '{$package}' LIMIT 1";

        $db = new Model();
        $row = $db->fetchOne($sql);

        if (empty($row)) {
            return array();
        }

        $group_id = $row['group_id'];

        $sql = "SELECT notice_type,  contents, notice_time FROM ny_notice
                    WHERE (FIND_IN_SET('{$group_id}', `groups`) OR `groups` = '' ) AND status = 1
                ORDER  BY notice_time DESC LIMIT 1 ";

        $result = $db->fetchOne($sql);

        if (empty($result)) {
            return array();
        }

        //Helper::log($result, 'textiong');

        return $result;
    }

    /**
     * 检查ip是否为白名单
     * @return
     */
    public static function checkIP()
    {
        $db = new Model();
        $ip = getClientIp(0, true);
        $sql = "SELECT id FROM game_white_ip WHERE ip = '{$ip}' AND type = 0 limit 1 ";

        $row = $db->fetchOne($sql);

        if (empty($row)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 获取服务器列表
     * @param  [string] $[package] [包号]
     * @param  [string] $[account] [帐号]
     * @param  [int]    $[type]    [提审标识] 0不提审
     * @return [type]          [description]
     */
    public static function getServer($package, $account = '', $type = 0)
    {
//        $isBlack = self::checkIP();
        $where = " s.status != 0 AND s.display = 1";

        //非白单
//        if (!$isBlack) {
//            $where = "s.display = 1";
//        }

//        //提审标志位
        if ($type == 1) {
            $where .= " and s.review = 1";
        } else {
            $where .= ' and s.review != 1';
        }

        $sql = "SELECT
                  s.server_id as server_id,s.open_time as open_time, c.domain as `domain`, s.sort as sort, s.name as `name`, s.num as num , s.channel_num as channel_num, c.login_host as login_host, c.login_port as login_port, s.status as status, s.zone as zone, s.mom_server as mom_server
                FROM
                  ny_server AS s
                  LEFT JOIN ny_server_config AS c
                    ON s.server_id = c.server_id
                  LEFT JOIN ny_package AS p
                    ON p.`group_id` = s.`group_id`
                WHERE p.package_id = '{$package}' AND  {$where} ORDER BY s.num DESC ";
        $db = new Model();

        $res = $db->query($sql);
        $servers = array();

        $time = time();

        foreach ($res as $k => $row) {
            //强制修改
//            if ($isBlack) {
//                $row['status'] = 1;
//                $row['display'] = 1;
//            }

            $flag = $row['server_id'];

            $servers[$flag] = array(
                'name' => $row['name'],
                'server_id' => $row['server_id'],
                'server_num' => (int)$row['num'],
                'channel_num' => (int)$row['channel_num'],
                'server_addr' => $row['login_host'],
                'server_port' => (int)$row['login_port'],
                'status' => (int)$row['status'],
                'zone' => $row['zone'],
                'tick' => (int)$time,
                'sign' => md5($account . $row['channel_num'] . $row['num'] . $time . '5727a407-bfc5-4b9b-a8ce-1db64e0d9dd1'),
                'package' => (int)$package,
                'mom_server' => $row['mom_server'],
            );
        }

        unset($row);

        //将子服配置改为母服的
        foreach ($servers as $sign => &$row) {
            $mom_server = $row['mom_server'];

            if (empty($mom_server)) {
                continue;
            }

            $serRes = isset($servers[$mom_server]) ? $servers[$mom_server] : array();

            if (empty($serRes)) {
                continue;
            }

            $row['domain'] = $serRes['domain'];
            $row['server_addr'] = $serRes['server_addr'];
            $row['server_port'] = $serRes['server_port'];
        }

        unset($row);

        return array_values($servers);

    }

    //返回服务器列表(mongo)
    public static function response($account, $nick_name = '', $package = '', $version = '', $stat = '', $extra = array(), $output = true)
    {
        $data = array();
//     	$zone = (new ZoneModel())->getZone(array() , array('id' , 'name'));
        $zone = [
            ['id' => 1, 'name' => '1-100'],
        ];
        $data['zone'] = array();
        foreach ($zone as $k => $row) {
            $data['zone'][$row['id']] = $row['name'];
        }

        /**--------判断是否为测试服务器 start（安卓）----------*/
        $testServerNum = self::reviewTestTypes($package);
        //print_r($testServerNum);exit;
        /**--------判断是否为测试服务器 end-------------------*/
        if (isset($testServerNum['test_server_num']) && $testServerNum['test_server_num'] == $version) {
            //通过sql获取测试服务器信息
            $data['server'] = self::getTestServer($package);
        } else {
            //通过mongo获取服务器信息
            $data['server'] = self::getMongoServer($package, $account);
        }
        if ($data['server']) {
            $versionRecord = $data['server'][0];
            if (!is_null($versionRecord['review_num']) && $versionRecord['review_num'] == $version && defined('IDFA') && !is_null(IDFA)) {
                $data['rvtype'] = $versionRecord['type'];
                $data['rvparam'] = !empty($versionRecord['rv_param']) ? json_decode($versionRecord['rv_param']) : array();//提审素材
                foreach ($data['server'] as $k => $v) {
                    if ($v['review'] != 1) unset($data['server'][$k]);
                    unset($data['server'][$k]['review_num']);
                    unset($data['server'][$k]['rv_param']);
                    unset($data['server'][$k]['review']);
                    unset($data['server'][$k]['type']);
                }
            } else {
                $data['rvtype'] = 0;
                $data['rvparam'] = array();
                $status = [2, 3, 4, 5];
                foreach ($data['server'] as $k => $v) {
                    if ($v['review'] == 1) unset($data['server'][$k]);
                    unset($data['server'][$k]['review_num']);
                    unset($data['server'][$k]['rv_param']);
                    unset($data['server'][$k]['review']);
                    unset($data['server'][$k]['type']);

                    if ($stat && in_array($v['status'], $status)) {
                        unset($data['server'][$k]);
                    }
                }
            }
            $data['server'] = array_values($data['server']);
        } else {
            $data['rvtype'] = 0;
            $data['rvparam'] = array();
        }

        $data['newregister'] = $stat ? 1 : 0;

        //渠道区分
        //uid渠道的帐号
        $data['user'] = array(
            'openId' => $account,
            'nickName' => $nick_name,
            'uid' => str_replace(self::$channel . '_', '', $account),
        );

        //需要在外层赋值
        $data['extra'] = $extra;

        $data['notice'] = self::notice($package);
        if ($output) {
            die(json_encode($data));
        } else {
            return $data;
        }
    }


    public static function getMongoServer($package, $account = '', $type = 0)
    {
        try {
            $mongo = Util::mongoConn(SERVER_MONGO_HOST, SERVER_MONGO_PORT, SERVER_MONGO_USER, SERVER_MONGO_PASSWD, SERVER_MONGO_DB);
        } catch (Exception $e) {
            echo $e->Messsge;
        }
        $isBlack = self::checkIP();
        $result = [];
        $data = $mongo->package_server_list->findOne(['package' => $package], ['_id' => 0, 'data' => 1]);
        if ($data) {
            $servers = [];
            $result = $data['data'];
            $time = time();
            foreach ($result as $k => $row) {

                //强制修改//ip白名单
                if ($isBlack) {
                    $row['status'] = 1;
                    $row['display'] = 1;
                } else {
                    if ($row['display'] == 0) continue;//只显示1的
                }

//     			//提审标志位
//     			if($type == 1){
//     				if ($row['review'] != 1) unset($result[$k]);
//     			}else{
//     				if ($row['review'] == 1) unset($result[$k]);
//     			}

                $flag = $row['server_id'];
                $servers[$flag] = array(
                    'open_time' => date('Y-m-d H:i:s', $row['open_time']),
                    'domain' => $row['domain'],
                    'sort' => $row['sort'],
                    'max_online' => $row['max_online'],
                    'name' => $row['name'],
                    'flag' => $flag,
                    'server_num' => $row['num'],
                    'channel_num' => $row['channel_num'],
                    'server_addr' => $row['login_host'],
                    'server_port' => $row['login_port'],
                    'status' => $row['status'],
                    'zone' => $row['zone'],
                    'tick' => $time,
                    'sign' => md5($account . $row['channel_num'] . $row['num'] . $time . 'lj39sdu42-348&23d'),
                    'text' => $row['tips'] ? $row['tips'] : '',
                    'online' => 0,
                    'mom_server' => $row['mom_server'],
                    'review_num' => $row['review_num'],//提审
                    'rv_param' => $row['rv_param'],//提审
                    'review' => $row['review'],//提审
                    'type' => $row['type'],//提审
                );
            }
            unset($row);
            //将子服配置改为母服的
            foreach ($servers as $sign => &$row) {
                $mom_server = $row['mom_server'];
                if (empty($mom_server)) {
                    continue;
                }
                $serRes = isset($servers[$mom_server]) ? $servers[$mom_server] : array();
                if (empty($serRes)) {
                    continue;
                }
                $row['domain'] = $serRes['domain'];
                $row['server_addr'] = $serRes['server_addr'];
                $row['server_port'] = $serRes['server_port'];
            }
            unset($row);
            return array_values($servers);
        }

    }

    /**
     * 获取服务器列表
     * @param  [string] $[package] [包号]
     * @param  [string] $[account] [帐号]
     * @param  [int]    $[type]    [提审标识] 0不提审
     * @return [type]          [description]
     */
    public static function getTestServer($account = '')
    {
        $isBlack = self::checkIP();
        //非白单
        if (!$isBlack) {
            return array();
        }
        $sql = "SELECT
                  s.*, c.*
                FROM
                  ny_server AS s
                  LEFT JOIN ny_server_config AS c
                    ON s.server_id = c.server_id
                WHERE s.server_id = 'test' AND s.display = 1 ORDER BY s.num DESC ";
        $db = new Model();

        $res = $db->query($sql);
        $servers = array();
        $time = time();
        foreach ($res as $k => $row) {
            //强制修改
            if ($isBlack) {
                $row['status'] = 1;
                $row['display'] = 1;
            }

            $flag = $row['server_id'];

            $servers[$flag] = array(
                'open_time' => date('Y-m-d H:i:s', $row['open_time']),
                'domain' => $row['domain'],
                'sort' => $row['sort'],
                'max_online' => $row['max_online'],
                'name' => $row['name'],
                'flag' => $flag,
                'server_num' => $row['num'],
                'channel_num' => $row['channel_num'],
                'server_addr' => $row['login_host'],
                'server_port' => $row['login_port'],
                'status' => $row['status'],
                'zone' => $row['zone'],
                'tick' => $time,
                'sign' => md5($account . $row['channel_num'] . $row['num'] . $time . '5727a407-bfc5-4b9b-a8ce-1db64e0d9dd1'),
                'text' => $row['tips'] ? $row['tips'] : '',
                'online' => 0,
                'mom_server' => $row['mom_server'],
                'review_num' => '',
                'review' => ''
            );
        }
        unset($row);
        //将子服配置改为母服的
        foreach ($servers as $sign => &$row) {
            $mom_server = $row['mom_server'];
            if (empty($mom_server)) {
                continue;
            }
            $serRes = isset($servers[$mom_server]) ? $servers[$mom_server] : array();
            if (empty($serRes)) {
                continue;
            }
            $row['domain'] = $serRes['domain'];
            $row['server_addr'] = $serRes['server_addr'];
            $row['server_port'] = $serRes['server_port'];
        }
        unset($row);
        return array_values($servers);
    }

    //记录新注册用户
    public static function register($account, $package, $channel = '')
    {

        !$channel && $channel = self::$channel;

        $newRegisterStat = (new RegisterModel())->newRegister($account, $channel, $package);

        //@todo 写入帐号登录日志
        $db = new Model('account');
        $db->setNewPrefix('log_');

        $ip = Helper::get_ip();

        $data = array(
            'account' => $account,
            'package' => $package,
            'channel' => $channel,
            'ip' => $ip,
            'create_time' => time(),
        );


        $a = $db->add($data);
        $db->setNewPrefix('ny_');//更新表前缀
        return $newRegisterStat;
    }

    /**
     * [makeAccount description]
     * @param  [type] $channel [description]
     * @param  [type] $account [description]
     * @return [type]          [description]
     */
    public static function makeAccount($channel, $account)
    {
//        if ($channel == 'huasheng') {
//            return $account;
//        }

        return $channel . '_' . $account;
    }

    public static function getAccount($channel, $account)
    {
        $start = strchr($account, $channel . '_');

        if ($start) {
            $account = substr($account, $start);
        }

        return $account;
    }

    /***
     * 获取账号历史选择的服务器
     * @param $package
     * @param $channel
     * @param $account
     */
    public static function getRecentServer($account, $package, $type = 0)
    {

        $db = new Model();

        $status = CDict::$recordStep['StartConnectServer'];

        $sql = "SELECT server, max(create_time) as create_time FROM ny_node WHERE account = '$account' and status = $status and package = '$package' GROUP BY server ORDER BY create_time desc";

        $server = $db->query($sql);

        if (empty($server)) return [];


        $server_str = "";
        foreach ($server as $value) {
            $server_str .= $value['server'] . "','";
        }

        $server_str = rtrim($server_str, "','");


        $where = '';

        //提审标志位
        if ($type == 1) {
            $where .= " and s.review = 1";
        } else {
            $where .= ' and s.review != 1';
        }

        $sql = "SELECT
                  s.server_id as server_id,s.open_time as open_time, c.domain as `domain`, s.sort as sort, s.name as `name`, s.num as num , s.channel_num as channel_num, c.login_host as login_host, c.login_port as login_port, s.status as status, s.zone as zone, s.mom_server as mom_server
                FROM
                  ny_server AS s
                  LEFT JOIN ny_server_config AS c
                    ON s.server_id = c.server_id
                WHERE s.server_id IN('$server_str') AND s.display = 1 AND s.status != 0 " . $where . " ORDER BY FIELD(s.server_id, '$server_str')";

        $res = $db->query($sql);
        $servers = array();
        $time = time();
        foreach ($res as $k => $row) {
            $flag = $row['server_id'];
            $servers[$flag] = array(
                'name' => $row['name'],
                'server_id' => $row['server_id'],
                'server_num' => (int)$row['num'],
                'channel_num' => (int)$row['channel_num'],
                'server_addr' => $row['login_host'],
                'server_port' => (int)$row['login_port'],
                'status' => (int)$row['status'],
                'zone' => $row['zone'],
                'tick' => (int)$time,
                'sign' => md5($account . $row['channel_num'] . $row['num'] . $time . '5727a407-bfc5-4b9b-a8ce-1db64e0d9dd1'),
                'package' => (int)$package,
                'mom_server' => $row['mom_server'],
            );
        }
        unset($row);
        //将子服配置改为母服的
        foreach ($servers as $sign => &$row) {
            $mom_server = $row['mom_server'];
            if (empty($mom_server)) {
                continue;
            }
            $serRes = isset($servers[$mom_server]) ? $servers[$mom_server] : array();
            if (empty($serRes)) {
                continue;
            }
            $row['domain'] = $serRes['domain'];
            $row['server_addr'] = $serRes['server_addr'];
            $row['server_port'] = $serRes['server_port'];
        }
        unset($row);
        return array_values($servers);

    }

    /**
     * 获取服务器列表
     * @param  [string] $[package] [包号]
     * @param  [string] $[account] [帐号]
     * @param  [int]    $[type]    [提审标识] 0不提审
     * @return [type]          [description]
     */
    public static function getNewServer($package, $account = '', $type = 0)
    {
        $where = " s.status = 2 and s.display = 1";
        //提审标志位
        if ($type == 1) {
            $where .= " and s.review = 1";
        } else {
            $where .= ' and s.review != 1';
        }
        $sql = "SELECT
                  s.server_id as server_id,s.open_time as open_time, c.domain as `domain`, s.sort as sort, s.name as `name`, s.num as num , s.channel_num as channel_num, c.login_host as login_host, c.login_port as login_port, s.status as status, s.zone as zone, s.mom_server as mom_server
                FROM
                  ny_server AS s
                  LEFT JOIN ny_server_config AS c
                    ON s.server_id = c.server_id
                  LEFT JOIN ny_package AS p
                    ON p.`group_id` = s.`group_id`
                WHERE p.package_id = '{$package}' AND  {$where} ORDER BY s.num DESC, s.open_time DESC, s.id DESC LIMIT 1";
        $db = new Model();

        $row = $db->query($sql);
        $row = $row[0];
        $server = array();

        $time = time();

        $server = array(
            'name' => $row['name'],
            'server_id' => $row['server_id'],
            'server_num' => (int)$row['num'],
            'channel_num' => (int)$row['channel_num'],
            'server_addr' => $row['login_host'],
            'server_port' => (int)$row['login_port'],
            'status' => (int)$row['status'],
            'zone' => $row['zone'],
            'tick' => (int)$time,
            'sign' => md5($account . $row['channel_num'] . $row['num'] . $time . '5727a407-bfc5-4b9b-a8ce-1db64e0d9dd1'),
            'package' => (int)$package,
            'mom_server' => $row['mom_server'],
        );


        if ($row['mom_server']) {
            $where .= " AND s.server_id = '" . $row['$mom_server'] . "'";

            //如果有母服, 查询母服信息
            $sql = "SELECT
                  c.domain, c.login_host, c.login_port
                FROM
                  ny_server AS s
                  LEFT JOIN ny_server_config AS c
                    ON s.server_id = c.server_id
                  LEFT JOIN ny_package AS p
                    ON p.`group_id` = s.`group_id`
                WHERE p.package_id = '{$package}' AND  {$where} ORDER BY s.num DESC, s.open_time DESC, s.id DESC LIMIT 1";

            $serRes = $db->query($sql);

            if ($serRes) {
                $server['domain'] = $serRes['domain'];
                $server['server_addr'] = $serRes['login_host'];
                $server['server_port'] = $serRes['login_port'];
            }
        }

        unset($row);

        return $server;

    }

    /**
     * 获取渠道提审标志,type=1则为开启状态，1则为提审
     * @param int $package
     * @version 2017-09-18
     * @author xty
     */
    public static function reviewTypes($package = null)
    {
        if (!$package) {
            return null;
        }
        $sql = "select `review_num`,`rv_param`,`type` from ny_package where package_id = '" . $package . "'";
        $model = new Model();
        $res = $model->query($sql);
        if (!empty($res[0]['review_num'])) {
            return array(
                'rv_num' => $res[0]['review_num'],
                'rv_param' => $res[0]['rv_param'],
                'type' => $res[0]['type'],
            );//版本号
        } else {
            return null;
        }
    }

    /**
     * 获取测试服版本号
     * @param int $package
     * @version 2017-09-18
     * @author xty
     */
    public static function reviewTestTypes($package = null)
    {
        if (!$package) {
            return null;
        }
        $sql = "select `test_server_num` from ny_package where package_id = '" . $package . "'";
        $model = new Model();
        $res = $model->query($sql);
        if (!empty($res[0]['test_server_num'])) {
            return array(
                'test_server_num' => $res[0]['test_server_num']
            );//版本号
        } else {
            return null;
        }
    }

    public function registeraccount($account, $channel, $param)
    {

        $sql = "select id from log_account where channel = '$channel' and account = '$account' limit 1";
        $model = new Model();
        $res = $model->query($sql);
        $param['module'] = 'register';
        $param['account'] = $account;
        if (empty($res)) {
            //@todo 转发到PHP，用于统计
            $url = "http://dpyh.center.gzfengyou.com/api/sdk/point.php";
            $post_string = http_build_query($param);
            Helper::httpRequest($url, $post_string, 'get');
        }
    }


    private static function pingAddress($address)
    {
        $status = -1;
        if (strcasecmp(PHP_OS, 'WINNT') === 0) {
            // Windows 服务器下
            $pingresult = exec("ping -n 1 {$address}", $outcome, $status);
        } elseif (strcasecmp(PHP_OS, 'Linux') === 0) {
            // Linux 服务器下
            $pingresult = exec("ping -c 1 {$address}", $outcome, $status);
        }
        if (0 == $status) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }

    public static function getPackage($packageName)
    {
        $sql = "select p.package_id as package_id, p.name as name, c.game_id as game_id, c.game_secret_key as secret_key, c.game_abbreviation as game_abbreviation, c.cdn_url as cdn_url, c.channel_id as channel, p.group_id as group_id, p.review_num as review_num, p.platform as os from ny_package as p left join ny_channel as c on p.channel_id = c.channel_id where p.`name` = '$packageName' limit 1";
        $model = new Model();
        $res = $model->query($sql);

        return $res[0];
    }

    public static function getPackageById($packageId)
    {
        $sql = "select p.package_id as package_id, p.name as name, c.game_id as game_id, c.game_secret_key as secret_key, c.game_abbreviation as game_abbreviation, c.cdn_url as cdn_url, c.channel_id as channel, p.group_id as group_id, p.review_num as review_num, p.platform as os from ny_package as p left join ny_channel as c on p.channel_id = c.channel_id where p.package_id = '$packageId' limit 1";
        $model = new Model();
        $res = $model->query($sql);

        return $res[0];
    }

    //登录验签后返回服务器信息 历史服务器或最新服务器
    public static function recentServer($channel, $account, $nick_name = '', $package = '', $version = '', $stat = '', $extra = array())
    {
        $data = array();

        $serverRedis = new ServerRedis();

        //提审
        $packageData = $serverRedis->getPackageById($package);//包号获取渠道提审信息
        $type = 0;
        $data['rvtype'] = 0;
        $group_id = $packageData['group_id'];

        if (!empty($packageData['review_num'])) {
            $type = 1;//通过版本号，得到该渠道组是否需要提审
            $data['rvtype'] = $type;
        }

        $data['s'] = [];

        //历史服务器或最新服
        $recentServer = $serverRedis->getRecentServer($group_id, $package, $account, $type);
        $servers = $recentServer;

        if (empty($servers) || empty($servers[0]) || $servers[0]['server_num'] === 0) {
            $servers = $serverRedis->getGroupServerList($group_id, $package, $account, $type);
        }

        $data['s'] = $servers[0] ? $servers[0] : [];

        //判断是否为新用户 改为通过redis是否存在选服数据判断
        $stat = $serverRedis->AccountRecentServerExists($group_id, $account);

        $data['newregister'] = $stat ? 0 : 1;

        if (!empty($data['s'])) {
            // if ($data['s']['cdn'] == '') {
                // $data['s']['cdn'] = $packageData['cdn_url'];
            // }

            // //旧服用旧资源
            // if ($data['s']['channel_num'] == 3 && $data['s']['server_num'] < 4) {
                // $data['s']['cdn'] = "https://test1.9day-game.com/game/bin/";
            // }

            //如果服务器状态为维护时, 新用户标识改为0 进入选服页
            if ($data['s']['status'] == -1) {
                $data['newregister'] = 0;
            }
        }

        //渠道区分
        //uid渠道的帐号
        $data['user'] = array(
            'openId' => $account,
            'nickName' => $nick_name,
            'uid' => str_replace($channel . '_', '', $account)
        );

        //需要在外层赋值
        $data['extra'] = $extra;
        //返回包id
        $data['package'] = $package;

        //获取初期加载资源的cdn //选服页图片
        // $data['cdn'] = $packageData['cdn_url'];

        $data['channel'] = $packageData['channel'];

        //生成签名
        $data['sign'] = Game::arithmetic($packageData['channel'], $package, $account);

        return $data;
    }

    //点击选服后返回所有服务器信息
    public static function allServer($package, $account, $rvtype = 0)
    {
        /*
         * 服务器状态
         * mysql: -1 维护中 1正常服 2最新服
         * 策划要求
         * 变换成:-1 维护中 1 繁忙 2 最新服
         * 非最新服且非维护中显示为繁忙状态。
         * 服务器维护中时显示为维护状态。
         * 最新服且非维护中显示为畅通状态。
         * */

        $serverRedis = new ServerRedis();

        $packageData = $serverRedis->getPackageById($package);

        $group_id = $packageData['group_id'];

        $returnServer = [];

        //  $newServer[] = self::getNewServer($package, $account, $rvtype);
        $newServer[] = $serverRedis->getNewServer($group_id, $package, $account, $rvtype);
        //     $recentServer = self::getRecentServer($account, $package, $rvtype);
        $recentServer = $serverRedis->getRecentServer($group_id, $package, $account, $rvtype);

        //分区
        $zones = $serverRedis->getZone();
        $zName = [];
        $s1 = [];
        $s2 = [];

        if (!empty($recentServer[0])) {
            $zName[] = '已有角色';

            foreach ($recentServer as $key =>  $row) {
                if ($row['channel_num'] == 3 && $row['server_num'] < 4) {
                    $recentServer[$key]['cdn'] = "https://test1.9day-game.com/game/bin/";
                } else {
                    //$recentServer[$key]['cdn'] = $packageData['cdn_url'];
                    if ($recentServer[$key]['cdn'] == '') {
                        $recentServer[$key]['cdn'] = $packageData['cdn_url'];
                    }
                }
            }

            $s1[] = $recentServer; //历史服
        }

        if (!empty($newServer[0])) {
            $zName[] = '最新服';
            foreach ($newServer as $key =>  $row) {
                if ($row['channel_num'] == 3 && $row['server_num'] < 4) {
                    $newServer[$key]['cdn'] = "https://test1.9day-game.com/game/bin/";
                } else {
                    //$newServer[$key]['cdn'] = $packageData['cdn_url'];
                    if ($newServer[$key]['cdn'] == '') {
                        $newServer[$key]['cdn'] = $packageData['cdn_url'];
                    }
                }
            }
            $s1[] = $newServer; //最新服
        }
        krsort($zones);
        //将分区的key换成id
        foreach ($zones as $k => $z) {
            $zName[] = $z['name'];
            $s2[$z['id']] = [];

        }
        //获取该包的服务器
        //$servers = self::getServer($package, $account, $rvtype);
        $servers = $serverRedis->getGroupServerList($group_id, $package, $account, $rvtype);

        if (!empty($servers)) {
            foreach ($servers as &$row) {
               // $row['cdn'] = $packageData['cdn_url'];
                if ($row['cdn'] == '') {
                    $row['cdn'] = $packageData['cdn_url'];
                }
                //旧服用旧资源
                if ($row['channel_num'] == 3 && $row['server_num'] < 4) {
                    $row['cdn'] = "https://test1.9day-game.com/game/bin/";
                }

                if (array_key_exists($row['zone'], $zones)) {
                    $s2[$row['zone']][] = $row;
                }
            }
        }

        krsort($s2);

        $s = array_merge($s1, $s2);

        //过滤掉没有服务器的分区
        foreach ($s as $key => $val) {
            if (empty($val)) {
                unset($s[$key]);
                unset($zName[$key]);
            }
        }

        $returnServer['z'] = array_values($zName);
        $returnServer['s'] = array_values($s);

        return $returnServer;
    }

    public static function checkServer($package, $server, $account, $rvtype = 0)
    {
        $serverRedis = new ServerRedis();

        return $serverRedis->getServerById($server, $package, $account);
    }
}
