<?php

class BaseController
{

    private $db;

    public function __construct()
    {

    }

    /**
     * [查找订单信息]
     * @param  [string] $orderNo [CP订单号]
     * @return [array]          [订单信息]
     */
    public function getOrderRow($orderNo)
    {
        $this->db = new Model('order');

        $fields = ['order_num', 'corder_num', 'role_name', 'money', 'account', 'role_id', 'role_level', 'role_career', 'gold', 'create_time', 'is_test', 'server', 'item_id', 'status'];

        return $this->db->getRow($fields, array('order_num' => $orderNo));
    }

    /**
     * [更新订单号]
     * @param  [array] $data   [更新数据]
     * @param  [type] $orderNo [订单号]
     * @return [type]          [description]
     */
    public function updateOrder($data, $orderNo)
    {
        $this->db = new Model('order');
        //$data['create_time'] = time();
        return $this->db->update($data, array('order_num' => $orderNo));
    }


    /**
     * [向单服发货]
     * @param  [string] $server [服务器标识符]
     * @param  [array]  $params [参数]
     * @return
     */
    public function sendGoods($server, $param = array())
    {
        $this->db = new Model('order');
        $data = array(
            'order_num' => $param['order_num'],
            'corder_num' => $param['corder_num'],
            'role_name' => $param['role_name'],
            'money' => $param['money'],
            'account' => $param['account'],
            'role_id' => $param['role_id'],
            'role_level' => $param['role_level'],
            'role_career' => $param['role_career'],
            'gold' => $param['gold'] ? $param['gold'] : 0,
            'create_time' => $param['create_time'],
            'ip' => getClientIp(0, true),
            'is_test' => $param['is_test']
        );
        $item_id = $param['item_id'];

        $vars = array('data' => $data, 'item_id' => $item_id);

        $rs = $this->db->call('Charge', 'addOrder', $vars, $server);

        $rs = $rs[$server];

        //当为true的时候代表发货成功
        if ($rs['state']) {

            $sendOrder = [];

            $serConfig = (new ServerRedis())->getServerConfig($server);

            if (empty($serConfig)) {
                $serConfig = (new ServerModel())->getServer($server, ['s.server_id as server_id', 's.name as name', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port']);
            }

            array_push($sendOrder, [$data['order_num'], intval($data['role_id']), intval($param['item_id']), floatval($data['money']), intval($data['create_time']) * 1000, false]);

            $sendData = ['opcode' => Pact::$SendGoodsOperate, 'str' => array($sendOrder)];

            $msg = $this->db->socketCall($serConfig['websocket_host'], $serConfig['websocket_port'], 'sendGoods', $sendData);

            if ($msg === 0) {
                return 1; //成功
            } else {
                return 2; //发货不成功
            }
        }

        return 2; //发货不成功
    }


    /**
     * [生成http 相对应的参数]
     * @param  [string]  $server_id  [服务器标识符]
     * @param  array $data [传入相对应的参数]
     * @param  boolean $encryption [是否需要相对应加密]
     * @return [array] 返回加密后的数据
     */
    public function makeRowHttpParams($server_id, $data = array(), $encryption = true)
    {
        $sql = "SELECT server_id, api_url, mdkey FROM ny_server_config WHERE server_id = '{$server_id}' LIMIT 1";

        $this->db = new Model('ny_server_config');

        $row = $this->db->fetchOne($sql);

        if (empty($row)) {
            return array();
        }

        $time = time();
        $server_id = $row['server_id'];

        $url = $row['api_url'];
        $mdkey = $row['mdkey'];
        $url = str_replace('index.php', 'api.php', $url);

        $json = json_encode($data);

        if ($encryption == true) {
            $md5 = Helper::authcode($json, 'ENCODE');
        } else {
            $md5 = $json;
        }

        $post = array('unixtime' => $time, 'data' => $md5);
        $post['sign'] = Helper::checkSign($post, $mdkey);

        return array('urls' => $url, 'params' => $post);
    }

    /**
     * [查找服务器列表]
     * @param  [string] $server [服务器标识符]
     * @return array
     */
    public function findServerBySign($server, $table = 'server_config')
    {
        $this->db = new Model($table);
        $row = $this->db->getRow('*', array('server_id' => $server));
        return $row;
    }

    /**
     * [查找服务器列表]
     * @param  [string] $server [服务器标识符]
     * @return array
     */
    protected static function findServerById($server, $table = 'server_config')
    {
        $model = new Model($table);
        $row = $model->getRow('*', array('server_id' => $server));
        return $row;
    }


    /**
     * [生产订单号]
     * @param  [int] $role_id [角色ID]
     * @return [type]          [description]
     */
    public function makeOrder($role_id)
    {
        $len = strlen($role_id);
        if ($len < 4) {
            $num = 4 - $len;
            $str = '';
            for ($i = 0; $i < $num; $i++) {
                $str .= 0;
            }
            $str .= $role_id;
        } else {
            $str = substr($role_id, -4);
        }

        //防止科学记数法出现
        $order_num = date('ymdHis') . 'E' . $str . rand(1000, 9999);


        return $order_num;
    }

    /**
     * 获取单服充值配置
     * @param $server_id
     * @return mixed
     */
    public static function getCharge($server_id)
    {
        $charge = (new Model())->call('Charge', 'recharge', ['output' => 1], $server_id);

        return $charge[$server_id];
    }


    /**
     * [生成订单信息]
     * @param  array $data [要插入订表号]
     * @return [type]        [description]
     */
    public function makeOrderRow($reqData)
    {
        Helper::log($reqData, 'game', __FUNCTION__);

        $server = $reqData['server'];
        $channel = $reqData['channel'];
        $role_id = $reqData['role_id'];
        $role_name = $reqData['role_name'];
        $productID = $reqData['product_id'];
        $account = $reqData['account'];
        $role_level = $reqData['role_level'];
        $money = $reqData['money'];
        $package = $reqData['package'];
        $role_career = $reqData['role_career'];


        if (empty($server) || empty($channel) || empty($role_id) || empty($role_name) || empty($productID) || empty($account)
            || empty($role_level) || empty($money) || empty($package) || empty($role_career)) {

            $code = Helper::getCode(8404);
            die($code);
        }

        if (!is_numeric($money) || !is_numeric($role_id) || !is_numeric($role_level) || !is_numeric($role_career)) {
            $code = Helper::getCode(8407);
            die($code);
        }

        $order_num = $this->makeOrder($role_id);

//        $charge = CDict::$charge;
        //获取充值配置
        $charge = self::getCharge($server);
        //$gold = isset($charge[$productID]) ? $charge[$productID]['gold'] : false;

        $charge = $charge[$productID];

        if ($money != $charge['price']) {
            $code = Helper::getCode(8602);
            die($code);
        }

        $time = time();
        $insert = array(
            'server' => $server,
            'channel' => $channel,
            'package' => $package,
            'order_num' => $order_num,
            'money' => $money,
            'account' => $account,
            'role_id' => $role_id,
            'role_name' => $role_name,
            'role_level' => $role_level,
            'role_career' => $role_career,
            'item_id' => $productID,
            'create_time' => $time,
            'gold' => $charge['item_id'] == CDict::$mailSource[2]['itemId'] ? $charge['item_count'] : 0,
            'reward_item_id' => $charge['item_id'],
            'reward_item_count' => $charge['item_count'],
            'idfa' => '',
        );

        $extra = array();

        $m = new Model('order');
        $m->setAlias('ny_order');

        $insertId = $m->add($insert);
        if (empty($insertId)) {
            $code = Helper::getCode(8500);
            die($code);
        }

        $data = [
            'insert' => $insert,
            'charge' => $charge
        ];

        return $data;
    }


    /**
     * [生成订单信息]
     * @param  array $data [要插入订表号]
     * @return [type]        [description]
     */
    public function createOrderRow($data)
    {
        $role_id = $data['role_id'];
        $server_id = $data['server'];
        $corder_num = $data['corder_num']; //渠道订单号
        // $user_id = $data['uid'];
        $money = $data['money'];
        $package = $data['package'];
        $channel = $data['channel'];
        $is_test = $data['is_test'];
        $create_time = $data['create_time'];
        $order_num = $data['order_num'];
        $item_id = $data['item_id'];
        $account = $data['account'];
        $role_name = $data['role_name'];
        $role_level = $data['role_level'];

        $Model = new Model('order');

        //单服获取角色信息
        // $data = $Model->call('Role', 'getRoleJson', array('role_id' => $role_id, 'fields' => ['account', 'name as role_name', 'role_level', 'career as role_career']), $server_id);

        // $role_data = $data[$server_id];

        // if (empty($role_data)) {
            // return [
                // 'status' => false,
                // 'code' => 2,
                // 'msg' => 'Role does not exist'
            // ];
        // }

        // $order_num = $this->makeOrder($role_id);

        // $charge = self::getCharge($server_id);

        // $item_id = '';

        //判断money是否在充值档位中
        /*foreach ($charge as $key => $val) {
            if ($val['price'] == $money) {
                $item_id = $key;
            }
        }

        if (!$charge[$item_id]) {
            return [
                'status' => false,
                'code' => 7,
                'msg' => 'Recharge amount does not match'
            ];
        }*/

        $insert = array(
            'server' => $server_id,
            'channel' => $channel,
            'package' => $package,
            'order_num' => $order_num,
            'corder_num' => $corder_num,
            'money' => $money,
            'account' => $account,
            'role_id' => $role_id,
            'role_name' => $role_name,
            'role_level' => $role_level,
            // 'role_career' => $role_data['role_career'],
            'item_id' => $item_id,
            // 'gold' => $charge[$item_id]['item_id'] == CDict::$mailSource[2]['itemId'] ? $charge[$item_id]['item_count'] : 0,
            // 'reward_item_id' => $charge[$item_id]['item_id'],
            // 'reward_item_count' => $charge[$item_id]['item_count'],
            'create_time' => $create_time,
            'is_test' => $is_test
            //'idfa' => !empty($idfa) ? $idfa : $imei,
        );

        $Model->setAlias('ny_order');
        $insertId = $Model->add($insert);

        if (empty($insertId)) {
            return [
                'status' => false,
                'code' => 9,
                'msg' => 'MySql Order insertion failed:' . $Model->lastError()
            ];
        }

        $insert['order_id'] = $insertId;

        return [
            'status' => true,
            'order_data' => $insert,
        ];
    }

    /**
     * 获取服务器信息
     * @param  [type] $server [description]
     * @return [type]         [description]
     */
    public function getServerInfo($server, $key = null)
    {
        $this->db = new Model();
        $sql = "select *  FROM ny_server AS ser INNER JOIN  ny_server_config AS config
                ON ser.`server_id` = config.`server_id` WHERE config.server_id = '{$server}' LIMIT 1";


        $res = $this->db->query($sql);


        if (empty($res)) {
            return false;
        }

        $res = $res[0];

        $type = $res['type'];
        $mom_server = $res['mom_server'];

        //查找母服
        if ($mom_server != $server && $type == 2) {
            $sql = "select *  FROM ny_server AS ser INNER JOIN  ny_server_config AS config
                ON ser.`server_id` = config.`server_id` WHERE ser.server_id  = '{$mom_server}' LIMIT 1 ";
            $rs = $this->db->query($sql);

            if (empty($rs)) {
                return $res;
            }

            $res = $rs[0];
        }

        if (empty($key)) {
            return $res;
        }

        return $res[$key];
    }

    /**
     * 持续监听充值发货,在执行此方法前，SDK渠道方先收到支付成功的消息
     * 中断SDK方重发机制，减轻服务器压力
     * @param array $orderInfo
     * @param string $className
     * @param string $orderId 渠道方SDK订单号
     */
    public static function sendGoodListenOld($orderInfo, $orderId, $className = __CLASS__)
    {
        if (empty($orderInfo)) die();//非法订单，不存在订单信息
        if (empty($orderInfo['server'])) die();//未能找到订单信息
        /*$reFlesh = new ReflectionClass($className);
        !$reFlesh->isSubclassOf('BaseController') && exit();//非base的派生类则退出*/
        $nowObj = new BaseController();//强实力，效率高
        $time = time();
        //更新订单时间,写入渠道订单号
        $nowObj->updateOrder(array('create_time' => $time, 'corder_num' => $orderId), $orderInfo['order_num']);

        $orderInfo['create_time'] = $time;

        fastcgi_finish_request();//使用CGI，断开与客户端连接，服务器继续执行
        set_time_limit(3 * 60);//拟定脚本执行时间为3分钟

        #code for send good by loop
        static $num = 1;
        $back_status = false;
        do {
            $status = $nowObj->sendGoods($orderInfo['server'], $orderInfo);//服务端支持同一个订单发货多次
            if ($status === 1) {
                //数据库锁表3秒后自动解锁
                $res = $nowObj->updateOrder(array('status' => 1, 'corder_num' => $orderId, 'loop_num' => $num), $orderInfo['order_num']);
                $back_status = true;
                break;//跳出持续监听
            }
            sleep(3);
            $num++;
        } while ($num <= 10);

        if ($back_status === false) {
            $nowObj->updateOrder(array('status' => 3, 'loop_num' => $num), $orderInfo['order_num']);
        }

        if ($res === 1 && $orderInfo['package'] == '1400201' && $orderInfo['params'] == 'apple') {

            $m = new Model();
            $sql = "SELECT num,name FROM ny_server WHERE server_id = '{$orderInfo['server']}' ";
            $one = $m->query($sql);
            list(, $uid) = explode("_", $orderInfo['account']);
            $pay_name = [
                '18' => '至尊.神兵宝箱',
                '30' => '300钻石',
                '98' => '980钻石',
                '128' => '1280钻石',
                '198' => '1980钻石',
                '328' => '3280钻石',
                '648' => '6480钻石',
                '6' => '60钻石',
                '108' => '无限豪礼',
            ];
            $timestamp = time();
            $pay_count = 1;
            //@todo 转发到PHP,板栗ios渠道的内购支付订单的报送
            $sendData = array(
                'app_id' => Game::$banliios1['AppId'],
                'apple_order_id' => $orderInfo['corder_num'],
                'order_id' => $orderInfo['order_num'],
                'user_id' => $uid,
                'serv_id' => $one[0]['num'],
                'serv_name' => urlencode($one[0]['name']),
                'role_id' => $orderInfo['role_id'],
                'role_name' => urlencode($orderInfo['role_name']),
                'pay_money' => $orderInfo['money'],
                'pay_count' => $pay_count,
                'pay_name' => urlencode($pay_name[(int)$orderInfo['money']]),
                'pay_time' => $orderInfo['create_time'],
                'timestamp' => $timestamp,
                'sign' => md5(Game::$banliios1['AppId'] . $timestamp . $uid . $pay_count . Game::$banliios1['AppKey']),
            );

            $url = "http://sdk.blbl666.com/analyze/pay";

            $post_string = http_build_query($sendData);
            $return = Helper::httpRequest($url, $post_string, 'get');
            file_put_contents('/tmp/banliios.txt', '时间:' . date("Y-m-d H:i:s", time()) . ',参数:' . json_encode($sendData) . ',结果:' . $return . "\n\r", FILE_APPEND);
        }

        //灵游ios报送热云充值成功的记录
//         if ($res === 1 && in_array($orderInfo['package'] , ['1900201'])) {

//         	$str = explode("_", $orderInfo['account']);
//         	$contextry['_currencyamount'] = $orderInfo['money'];
//         	$contextry['_paymenttype'] = 'apple';
//         	$contextry['_currencytype'] = 'CNY';
//         	$contextry['_deviceid'] = $orderInfo['idfa'];
//         	$contextry['_transactionid'] = $orderInfo['order_num'];
//         	$contextry['_idfa'] = $orderInfo['idfa'];
//         	$contextry['_idfv'] = '';
//         	$contextry['_imei'] = '';
//         	$contextry['_ip'] = getClientIp();
//         	$contextry['_tz'] = '+8';
//         	$contextry['_rydevicetype'] = '';

//         	switch ($orderInfo['package']) {
//         		case '1900201':
//         			$appid = '351e8e0d87fdd3e7be86770ad456e51b';
//         			break;

//         	}

//         	$query = [
//         	'appid' => $appid,
//         	'who'   => $str[1],
//         	'context'   => $contextry,
//         	];
//         	$queryjson = json_encode($query);
//         	$url           = 'http://log.reyun.com/receive/tkio/payment';
//         	$return = Helper::httpRequest($url, $queryjson);
//         	file_put_contents('/tmp/lingyouiosRyun.txt', '时间:'.date("Y-m-d H:i:s",time()).',参数:'.json_encode($query).',结果:'.$return."\n\r",FILE_APPEND);

//         }
        unset($num);
        die();
    }


    /**
     * 持续监听充值发货,在执行此方法前，SDK渠道方先收到支付成功的消息
     * 中断SDK方重发机制，减轻服务器压力
     * @param array $orderInfo
     * @param string $className
     */
    public static function sendGoodListen($orderInfo)
    {
        if (empty($orderInfo)) die();//非法订单，不存在订单信息
        if (empty($orderInfo['server'])) die();//未能找到订单信息

        $_this = new self();
        $Model = new Model();
        $create_time = time();

        //update渠道订单号
        // $_this->updateOrder(array('corder_num' => $orderInfo['corder_num'], 'create_time' => $create_time), $orderInfo['order_num']);

        // fastcgi_finish_request();//使用CGI，断开与客户端连接，服务器继续执行
        // set_time_limit(60);//拟定脚本执行时间为1分钟

        #code for send good by loop
        static $num = 0;
        $back_status = false;
        do {
            $status = $_this->sendGoods($orderInfo['server'], $orderInfo);//服务端支持同一个订单发货多次
            if ($status === 1) {
                //数据库锁表3秒后自动解锁
                $_this->updateOrder(array('status' => 1, 'loop_num' => $num), $orderInfo['order_num']);
                $back_status = true;
                break;//跳出持续监听
            }
            sleep(1);
            $num++;
        } while ($num <= 10);

        if ($back_status === false) {
            $_this->updateOrder(array('status' => 2, 'loop_num' => $num), $orderInfo['order_num']);
        }

        $singleOrderStatus = $back_status ? 1 : 2;

        //修改单服的订单状态
        $Model->call('Charge', 'update', ['data' => ['status' => $singleOrderStatus], 'where' => ['order_num' => $orderInfo['order_num']]], $orderInfo['server']);

        unset($num);
        return $back_status;
    }

    /**
     * @校验金额 高精度计算
     * @param string $sdkMoney
     * @param array $orderInfo
     * @param string $type ['yuan','jiao','fen']
     */
    public static function checkChargeMoney($sdkMoney, $orderInfo, $type = 'yuan')
    {
        $status = false;
        if (!in_array($type, array('yuan', 'jiao', 'fen')) || empty($orderInfo['item_id']) || empty($orderInfo['money']) || empty($orderInfo['server'])) return $status;
        $sdkMoney = floatval($sdkMoney);
        if ($sdkMoney <= 0) {
            return $status;
        }
        //获取充值配置
        $charge = self::getCharge($orderInfo['server']);
        $price = $charge[$orderInfo['item_id']]['price'];//充值产品配置
        $orderMoney = 0;
        if (empty($price)) return $status;
        switch ($type) {
            case 'yuan':
                $price = bcmul($price, 1);
                $orderMoney = bcmul($orderInfo['money'], 1);
                $sdkMoney = bcmul($sdkMoney, 1);
                break;
            case 'jiao':
                $price = bcmul($price, 10);
                $orderMoney = bcmul($orderInfo['money'], 10);
                $sdkMoney = bcmul($sdkMoney, 10);
                break;
            case 'fen':
                $price = bcmul($price, 100);
                $orderMoney = bcmul($orderInfo['money'], 100);
                $sdkMoney = bcmul($sdkMoney, 100);
                break;
            default:
                #类型不存在
                return false;
                break;
        }
        $status = ($price == $sdkMoney) ? ($orderMoney == $sdkMoney ? true : false) : false;
        return $status;
    }

    /**
     * 检查ip是否为白名单
     * @return
     */
    public static function checkIP()
    {
        $db = new Model();
        $ip = getClientIp();
        $sql = "SELECT id FROM game_white_ip WHERE ip = '{$ip}' AND type = 0 limit 1 ";
        $row = $db->fetchOne($sql);

        if (empty($row)) {
            return false;
        } else {
            return true;
        }
    }
}


