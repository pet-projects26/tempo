<?php

/***
 * 游戏入口类
 * Class BaseEntrance
 */
class BaseEntrance
{

    private $ServerRedis;

    public function __construct()
    {
        $this->ServerRedis = new ServerRedis();
    }

    public function init()
    {
        //获取给定链接参数
        $group = getParam('group'); //父渠道号
        $channel = getParam('channel'); //子渠道标识
        $package = getParam('package'); //包号
        $rvtype = getParam('rvtype'); //是否提审
        $version = getParam('version'); //版本号
        $sdk_name = getParam('sdk_name'); //使用哪个sdk

        //判定参数 是否存在该父渠道与子渠道标识 没有则报错
        if (!$group || !$channel || !$this->checkGroupChannelExists($group, $channel)) {
            //父渠道名或子渠道标识不存在
            return $this->returnInfo(400, 'group || channel does not exist', $_REQUEST);
        }

        //获取sdk给定参数 9130
        $_9130_game_partner_id = getParam('9130_game_partner_id'); //平台  1安卓 2IOS
        $_9130_partner_id = getParam('9130_partner_id');
        $_9130_game_pkg = getParam('9130_game_pkg'); //包名
        $_9130_game_version = getParam('9130_game_version'); //版本号

        if ($group == 4 && $channel == 'liany') {
            $_9130_game_partner_id = $_9130_partner_id;
        }

        $is_9130 = false;
        $is_9187 = false;

        $game_pkg = '';
        $game_partner_id = '';
        $sdkHost = '';

        $isHttps = is_https();

        if ($_9130_game_partner_id && $_9130_game_pkg || $sdk_name == '9130') {
            //9130 || fante
            $is_9130 = true;
            //sdkHost
            $sdkHost = $isHttps ? _9130Controller::$jsSdkHost['https'] : _9130Controller::$jsSdkHost['http'];
            $version = $_9130_game_version;

            $game_pkg = $_9130_game_pkg;
            $game_partner_id = $_9130_game_partner_id == 2 ? 2 : 1;

            $sdk_name = '9130';
        } else if ($sdk_name == '9187') {
            $is_9187 = true;
            //sdkHost
            $sdkHost = $isHttps ? _9187Controller::$jsSdkHost['https'] : _9187Controller::$jsSdkHost['http'];

            //获取sdk给定参数 9187
            $pf = getParam('pf');
            $game_pkg = getParam('pkg');
            $game_partner_id = $pf == 102 ? 2 : 1;
        }

        // 判断该包是否存在 获取
        $pkgInfo = $this->ServerRedis->getPackage($game_pkg);

        if (empty($pkgInfo)) {
            //没有包, 自动在后台增加一个该渠道的包
            $pkgInfo = $this->autoAddPackage($group, $channel, $game_pkg, $game_partner_id);
        }

        if ($pkgInfo['group_id'] != $group || $pkgInfo['channel'] != $channel) {
            //父渠道名或子渠道标识不一致  报错
            return $this->returnInfo(400, 'group || channel error', $_REQUEST);
        }

        //返回包的信息
        $package = $pkgInfo['package_id'];

        $packageData = $this->ServerRedis->getPackageById($package);

        if (empty($packageData)) {
            //包不存在
            return $this->returnInfo(400, 'package error', $_REQUEST);
        }

        $backstage = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . "/api/sdk/game.php/";

        $mac = 0;

        $select_server_cdn = "https://test1.9day-game.com/game/select_server/";

        $select_server_js = $select_server_cdn . 'select_server.js?v=' . time();

        $data = [
            'select_server_cdn' => $select_server_cdn,
            'select_server_js' => $select_server_js,
            'sdk_host' => $sdkHost,
            'mac' => 0,
            'channel' => $packageData['channel'],
            'package' => $packageData['package_id'],
            'backstage' => $backstage,
            'version' => $version ? $version : '',
            'is_9130' => $is_9130,
            'is_9187' => $is_9187,
            'sdk_name' => $sdk_name
        ];

        return $this->returnInfo(200, '', $data);
    }

    /**
     * 后台自动添加包
     * @param $group 父渠道号
     * @param $channel 子渠道标识
     */
    public function autoAddPackage($group, $channel, $packageName, $os)
    {
        $channelModel = new ChannelModel();
        $channelInfo = $channelModel->getChannel($channel, ['id']);

        $packageModel = new PackageModel();

        $packRs = $packageModel->getRow(['max(package_id) as package_id'], ['channel_id' => $channel, 'group_id' => $group]);

        //如果最大包号不存在则  (渠道号 * 100 + 子渠道号) * 1000
        $packageId = $packRs['package_id'] ? $packRs['package_id'] + 1 : ($group * 100 + $channelInfo['id']) * 1000;

        $packageInfo = array(
            'channel_id' => $channel,
            'package_id' => $packageId,
            'group_id' => $group,
            'name' => $packageName,
            'create_time' => time(),
            'platform' => intval($os),
        );

        $packageModel->addPackage($packageInfo);

        $channel = $packageInfo['channel_id'];
        unset($packageInfo['channel_id']);
        $packageInfo['channel'] = $channel;

        return $packageInfo;
    }

    /**
     * 判断渠道号与子渠道标识是否存在
     * @param $group
     * @param $channel
     * @return bool
     */
    public function checkGroupChannelExists($group, $channel)
    {
        if ($this->ServerRedis->GroupIdExists($group) && $this->ServerRedis->channelIdExists($channel)) {
            return true;
        }

        return false;
    }


    public function returnInfo($code, $msg, $data = [])
    {
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

}