<?php
interface GetOrder{
    public static function run();
}

abstract class BaseGetOrder implements GetOrder{
    protected static $channel;

    public static function logs($msg){
        $path = LOG_PATH . '/sdk/' . self::$channel . '/get_order/' . date('Ymd') . '.log';
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        @file_put_contents($path , $contents , FILE_APPEND);
    }
}
