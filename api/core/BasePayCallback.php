<?php

interface PayCallback
{
    public static function run();
}

abstract class BasePayCallback extends BaseController implements PayCallback
{
    protected static $channel;

    public static function logs($msg)
    {
        $path = LOG_PATH . '/sdk/' . self::$channel . '/pay_callback/' . date('Ymd') . '.log';
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        @file_put_contents($path, $contents, FILE_APPEND);
    }

    protected static function send($channel, $orderid, $data, $params)
    {




    }
}