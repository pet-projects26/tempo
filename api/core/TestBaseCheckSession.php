<?php
interface CheckSession{
    public static function run();
}

abstract class TestBaseCheckSession implements CheckSession{
    protected static $channel;

    public static function logs($msg){
        $path = LOG_PATH . '/sdk/' . self::$channel . '/test_check_session/' . date('Ymd') . '.log';
        $contents = '[' . date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        @file_put_contents($path , $contents , FILE_APPEND);
    }

	/**
     * [response 返回服务器列表，分区信息]
     * @param  [type] $account [渠道 accountId]
     * @param  [type] $package [渠道 包号]
     */
	public static function response($account , $nick_name = '' , $package = '' , $version = '' , $openid = ''){
        if($account == ''){
            $data = array();
            $zone = (new ZoneModel())->getZone(array() , array('id' , 'name'));
            $channel = (new ChannelModel())->getChannel(self::$channel , array('num'));
            $data['zone'] = array();
            foreach($zone as $k => $row){
                $data['zone'][$row['id']] = $row['name'];
            }
            $data['server'] = (new ServerModel())->getServerByChannelPackage(self::$channel , $package , self::$channel . '_' . $account , $version);
            $time = time();
            foreach($data['server'] as $k => $row){
                $data['server'][$k] = array(
                    'open_time' => date('Y-m-d H:i:s' , $row['open_time']),
                    'domain' => $row['domain'],
                    'sort' => $row['sort'],
                    'max_online' => $row['max_online'],
                    'name' => $row['name'],
                    'flag' => $row['server_id'],
                    'server_num' => $row['num'],
                    'channel_num' => $row['channel_num'],
                    'server_addr' => $row['login_host'],
                    'server_port' => $row['login_port'],
                    'status' => $row['status'],
                    'zone' => $row['zone'],
                    'tick' => $time,
                    'sign' => md5($openid . $channel['num'] . $row['num'] . $time . 'lj39sdu42-348&23d'),
                    'text' => $row['tips'] ? $row['tips'] : '',
                    'online' => 0
                );
            }
            $data['user'] = array(
                'openId' => $openid,
                'nickName' => $nick_name,
                'uid' => str_replace(self::$channel.'_', '', $account),
            );

            //需要在外层赋值
            $data['extra'] = array();

            $data['notice'] = self::notice($package);
            echo json_encode($data);
        }
    }


     /**
     * [公告]
     * @param  [type] $package [包]
     * @return [type]          [description]
     */
    public static function  notice($package) {
        //检测
        if (empty($package)) {
            $package = '1010101';
        }

        $sql = "SELECT * FROM ny_package WHERE package_id = '{$package}' LIMIT 1";

        $db = new Model();
        $row  = $db->fetchOne($sql);

        if (empty($row)) {
            return array();
        }

        $group_id = $row['group_id'];

        $sql = "SELECT notice_type,  contents, notice_time FROM ny_notice 
                    WHERE (FIND_IN_SET('{$group_id}', `groups`) OR `groups` = '' ) AND status = 1
                ORDER  BY notice_time DESC LIMIT 1 ";

        $result = $db->fetchOne($sql);

        if (empty($result)) {
            return array();
        }

        Helper::log($result,  'textiong');

        return $result;

    }

    public static function register($account , $package){
        (new RegisterModel())->newRegister($account , self::$channel , $package);
    }
}
