<?php
header('content-type:application:json;charset=utf8');
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:POST');
header('Access-Control-Allow-Headers:x-requested-with,content-type');
$method = $_GET['s'];
!isset($method) && header("HTTP/1.0 404 Not Found");
require_once 'conf/main.php';
$method();

//获取客户端信息接口
function client(){
	file_put_contents('/tmp/client.log',json_encode($_REQUEST). "\n", FILE_APPEND);
    $channel = getParam('channel');
    $package = getParam('package');
	$server = getParam('server');
    if($channel && $package){
        $model = getParam('model');
        $mac = getParam('mac');
        $tmemory = getParam('tmemory');
        $nmemory = getParam('nmemory');
        $network = getParam('network');
		$account = getParam('account');
		$role = getParam('role');
        $brand = getParam('brand');
        $idfa = getParam('idfa');
        $imei = getParam('imei');
        $data = array();
		$account && $data['account'] = $account;
		$role && $data['role'] = $role;
		
        $channel && $data['channel'] = $channel;
        $package && $data['package'] = $package;
		$server  && $data['server'] = $server;
        $model && $data['model'] = $model;
        $mac && $data['mac'] = $mac;
        $tmemory && $nmemory &&  $data['memory'] = $nmemory . '(' . $tmemory . ')';
        $network && $data['network'] = $network;
        $brand && $data['brand'] = $brand;
        $data['create_time'] = time();
        $data['udid'] = !empty($idfa) ? $idfa : $imei; 
       
        $result=(new ClientModel())->add($data);

//        if ($channel && $data['udid']) {
//        	$mongo = Util::mongoConn(SERVER_MONGO_HOST, SERVER_MONGO_PORT, SERVER_MONGO_USER, SERVER_MONGO_PASSWD,SERVER_MONGO_DB);
//        	$result = $mongo->client_udid->findOne(['channel' => $channel,'udid'=>$data['udid']],['_id' => 0,'create_time'=>1]);
//        	if (!$result['create_time']) {
//        		$mongo->client_udid->update(['channel' => $channel,'udid'=>$data['udid']], ['$set' => $data]);
//        	}
//
//        }
       
    }
}

//获取设备
function mac()
{
    $channel = getParam('channel');
    $package = getParam('package');
    if ($channel && $package) {
        $mac = getParam('mac');
        $data = array();
        $channel && $data['channel'] = $channel;
        $package && $data['package'] = $package;
        $mac && $data['mac'] = $mac;
        $data['create_time'] = time();
        (new MacModel())->add($data);
    }
}

//记录报错日志
//function error()
//{
//    Helper::log($_REQUEST, 'ClientError', 'error', 'error', 'error');
//    exit;
//}

//
////平台新跳转数接口
//function perHourData(){
//    $server = getParam('server');
//    $channel = getParam('channel');
//    $package = getParam('package');
//
//    $newJumps = getParam('new_jumps');
//
//    if ($channel && $server && $package) {
//        $data = array();
//        $data['channel'] = $channel;
//        $data['package'] = $package;
//        $data['server'] = $server;
//
//        $data['new_jumps'] = $newJumps ? $newJumps : 0;
//        $data['time'] = time();
//
//        if ($data['new_jumps'] == 0) echo 0;
//
//        //入队
//        if ((new PerhourRedis())->insterQueue($data)) echo 1; else echo 0;
//    }
//
//}

