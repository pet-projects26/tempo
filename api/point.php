<?php
//error_reporting(E_ALL);
header("Content-Type:text/html;charset=utf-8");
$method = $_GET['s'];
!isset($method) && header("HTTP/1.0 404 Not Found");
require_once 'conf/main.php';
require_once '../center/includes/Helper.php';

//包号=> 渠道id
$package = array(
	1 =>14,//魅族
	1 =>16,//联想
	1 =>20,//oppo
	1 =>23,//360
	1 =>54,//华为
	1 =>66,//小米
	1 =>78,//酷派
	1 =>84,//金立
	1 =>368,//vivo
	1 =>550//应用宝
);

$bag = getParam('package') ?  getParam('package') : ' ';

//赫德的包才需要执行下面的方法
if(array_key_exists($bag ,$package)){
	$method();
}

//创建角色
function rolebuild(){
	global  $package ;
	$version = getParam('version') ?  getParam('version') : ' ';
	$serverid = getParam('serverid') ?  getParam('serverid') : ' ';
	$accountid = getParam('accountid') ?  getParam('accountid') : ' ';
	$roleid = getParam('roleid') ?  getParam('roleid') : ' ';
	$rolename = getParam('rolename') ?  getParam('rolename') : ' ';	
	$bag = getParam('package') ?  getParam('package') : ' ';
	$channel = $package[$bag];
	
	$param = 'log=';
	$param .= date('Y-m-d H:i:s' , time()).',';
	$param .= 'yhxt,'; 
	$param .= $version.',';
	$param .= 'rolebuild,';
	$param .= $serverid.',';
	$param .= $channel.','; 
	$param .= $accountid.',';
	$param .= $roleid.',';
	$param .= $rolename;
	
	$url = 'http://log.hardtime.cn:5555/HardtimeGameStatsHttpLog/write.jsp';
	Helper::httpRequest($url,$param,'get'); 
}
//登录日志
function login(){
	global  $package ;
	$version = getParam('version') ?  getParam('version') : ' ';
	$serverid = getParam('serverid') ?  getParam('serverid') : ' ';
	$accountid = getParam('accountid') ?  getParam('accountid') : ' ';
	$roleid = getParam('roleid') ?  getParam('roleid') : ' ';
	$rolename = getParam('rolename') ?  getParam('rolename') : ' ';
	$accname = getParam('accname') ?  getParam('accname') : ' ';	
	$level = getParam('level') ?  getParam('level') : ' ';	
	$DeviceModel = getParam('DeviceModel') ?  getParam('DeviceModel') : ' ';	
	$mac = getParam('mac') ?  getParam('mac') : ' ';	
	$IP = getParam('IP') ?  getParam('IP') : ' ';
	$bag = getParam('package') ?  getParam('package') : ' ';
	$channel = $package[$bag];
	
	$param = 'log=';
	$param .= date('Y-m-d H:i:s' , time()).',';
	$param .= 'yhxt,'; 
	$param .= $version.',';
	$param .= 'login,';
	$param .= $serverid.',';
	$param .= $channel.','; 
	$param .= $accountid.',';
	$param .= $accname.',';
	$param .= $roleid.',';
	$param .= $rolename.',';
	$param .= $level.',';
	$param .= $DeviceModel.',';
	$param .= $mac.',';
	$param .= $IP;
	$url = 'http://log.hardtime.cn:5555/HardtimeGameStatsHttpLog/write.jsp';
	Helper::httpRequest($url,$param,'get'); 
	
}
//退出日志
function logout(){
	global  $package ;
	$version = getParam('version') ?  getParam('version') : ' ';
	$serverid = getParam('serverid') ?  getParam('serverid') : ' ';
	$accountid = getParam('accountid') ?  getParam('accountid') : ' ';
	$roleid = getParam('roleid') ?  getParam('roleid') : ' ';
	$rolename = getParam('rolename') ?  getParam('rolename') : ' ';
	$accname = getParam('accname') ?  getParam('accname') : ' ';	
	$level = getParam('level') ?  getParam('level') : ' ';	
	$onlinetime = getParam('onlinetime') ?  getParam('onlinetime') : ' ';
	$bag = getParam('package') ?  getParam('package') : ' ';
	$channel = $package[$bag];
	
	$param = 'log=';
	$param .= date('Y-m-d H:i:s' , time()).',';
	$param .= 'yhxt,'; 
	$param .= $version.',';
	$param .= 'logout,';
	$param .= $serverid.',';
	$param .= $channel.',';
	$param .= $accountid.',';
	$param .= $accname.',';
	$param .= $roleid.',';
	$param .= $rolename.',';
	$param .= $level.',';
	$param .= $onlinetime;
	$url = 'http://log.hardtime.cn:5555/HardtimeGameStatsHttpLog/write.jsp';
	Helper::httpRequest($url,$param,'get');  
}
//创建订单
function order(){
	global  $package ;
	$version = getParam('version') ?  getParam('version') : ' ';
	$serverid = getParam('serverid') ?  getParam('serverid') : ' ';
	$accountid = getParam('accountid') ?  getParam('accountid') : ' ';
	$roleid = getParam('roleid') ?  getParam('roleid') : ' ';
	$rolename = getParam('rolename') ?  getParam('rolename') : ' ';
	$Amount = getParam('Amount') ?  getParam('Amount') : ' ';	
	$level = getParam('level') ?  getParam('level') : ' ';	
	$orderID = getParam('orderID') ?  getParam('orderID') : ' ';
	$bag = getParam('package') ?  getParam('package') : ' ';
	$channel = $package[$bag];
	
	$param = 'log=';
	$param .= date('Y-m-d H:i:s' , time()).',';
	$param .= 'yhxt,'; 
	$param .= $version.',';
	$param .= 'order,';
	$param .= $serverid.',';
	$param .= $channel.','; 
	$param .= $accountid.',';
	$param .= $roleid.',';
	$param .= $level.',';
	$param .= $Amount.',';
	$param .= $rolename.',';
	$param .= $orderID;
	$url = 'http://log.hardtime.cn:5555/HardtimeGameStatsHttpLog/write.jsp';
	Helper::httpRequest($url,$param,'get'); 
}
//升级日志
function levelup(){
	global  $package ;
	$version = getParam('version') ?  getParam('version') : ' ';
	$serverid = getParam('serverid') ?  getParam('serverid') : ' ';
	$accountid = getParam('accountid') ?  getParam('accountid') : ' ';
	$roleid = getParam('roleid') ?  getParam('roleid') : ' ';
	$rolename = getParam('rolename') ?  getParam('rolename') : ' ';
	$level = getParam('level') ?  getParam('level') : ' ';	
	$level1 = getParam('level1') ?  getParam('level1') : ' ';
	$bag = getParam('package') ?  getParam('package') : ' ';
	$channel = $package[$bag];
	
	$param = 'log=';
	$param .= date('Y-m-d H:i:s' , time()).',';
	$param .= 'yhxt,'; 
	$param .= $version.',';
	$param .= 'levelup,';
	$param .= $serverid.',';
	//$param .= $channel.','; 
	$param .= $accountid.',';
	$param .= $roleid.',';
	$param .= $rolename.',';
	$param .= $level.',';
	$param .= $level1;
	$url = 'http://log.hardtime.cn:5555/HardtimeGameStatsHttpLog/write.jsp';
	Helper::httpRequest($url,$param,'get'); 
}



