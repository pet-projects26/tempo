<?php
/**
 * 人工转账接口
 * url: 域名 + 根目录文件夹名称/api/sdk/9130/artificial_transfer.php
 * Created by PhpStorm.
 * User: w
 * Date: 2019/1/17
 * Time: 16:35
 * 返回值说明：
 * 0 - 成功
 * 1 - 参数错误
 * 2 - 帐号不存在
 * 3 - 接口超时
 * 4 - IP不允许
 * 5 - 订单已发货
 * 6 - 校验失败
 * 7 - 充值金额不匹配
 * 8 - 发货失败
 * 9 - 未知错误
 * sign = md5(order_id + uid + money + role_id + server_id + partner_id + time + secret_key)
 * 其中+为字符串链接符号，secret_key由平台方提供，md5取32位小写字符
 */

require_once '../../conf/main.php';
require_once '../../conf/Game.php';

$controller = '_9130ArtificialTransfer';
$controllerObj = new $controller();
$controllerObj->run();