<?php
/**
 * 玩家封禁接口
 * url: 域名 + 根目录文件夹名称/api/sdk/9130/player_ban.php
 * 返回值：JSON
 * 成功：{state:1}
 * 失败：{state:0, msg: ‘xxxx'}
 * sign=md5(game+server+roleid+time+key)
 * 其中key与充值key一致 +号为字符串连接符
 */
require_once '../../conf/main.php';
require_once '../../conf/Game.php';
$controller = '_9130PlayerBan';
$controllerObj = new $controller();
$controllerObj->run();