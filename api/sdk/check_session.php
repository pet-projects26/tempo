<?php
//登录验证、获取服务器列表接口
//url: 域名 + 根目录文件夹名称/api/sdk/check_session.php?r=渠道
require_once '../conf/main.php';
require_once '../conf/Game.php';
ini_set('display_errors' , 'On');
error_reporting(E_ALL^ E_STRICT );
$channel = strtolower(getParam('r'));
$package = getParam('package') ? getParam('package') : '';
$version = getParam('v') ? getParam('v') : '';
if(isset($_GET['idfa'])){
    $idfa = 1;
    define('IDFA',$idfa);
}else{
    define('IDFA',null);
}
$channel == '' && exit();
$controller = '_' . $channel . 'CheckSession';
$controller::run($channel , $package , $version);