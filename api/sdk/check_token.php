<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/1/21
 * Time: 20:21
 */
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers:Authorization');
header("Access-Control-Allow-Methods: GET, POST, DELETE");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, X-Requested-With, Cache-Control,Authorization");
require_once '../conf/main.php';
require_once '../conf/Game.php';
ini_set('display_errors', 'Off');
error_reporting(0);
$channel = strtolower(getParam('r'));
$channel == '' && exit();
$package = getParam('package', '');
$version = getParam('v', '');
if (isset($_GET['idfa'])) {
    $idfa = 1;
    define('IDFA', $idfa);
} else {
    define('IDFA', null);
}
$controller = '_' . $channel . 'CheckToken';
$controllerObj = new $controller();
$controllerObj->run($channel, $package, $version);