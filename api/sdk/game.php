<?php
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Methods: GET,POST,OPTIONS');
header('Access-Control-Allow-Origin: *');

/**
 *  游戏接口的入口
 *  以restfull的模式
 *  访问方式
 *  http://localhost/dev/html/api/sdk/game.php/game/index/123
 *  game.php后面的game是控制器，index是方法，123是参数
 */
ini_set('display_errors', 'On');
error_reporting(E_ERROR);
require_once '../conf/main.php';
require_once '../conf/Game.php';
//php防注入和XSS攻击通用过滤
$_GET && SafeFilter($_GET);
$_POST && SafeFilter($_POST);
$_COOKIE && SafeFilter($_COOKIE);

if (!isset($_SERVER['REQUEST_URI'], $_SERVER['SCRIPT_NAME'])) {
    exit('');
}

// parse_url() returns false if no host is present, but the path or query string
// contains a colon followed by a number
$uri = parse_url($_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

$query = isset($uri['query']) ? $uri['query'] : '';
$uri = isset($uri['path']) ? $uri['path'] : '';

if (isset($_SERVER['SCRIPT_NAME'][0])) {
    if (strpos($uri, $_SERVER['SCRIPT_NAME']) === 0) {
        $uri = (string)substr($uri, strlen($_SERVER['SCRIPT_NAME']));
    } elseif (strpos($uri, dirname($_SERVER['SCRIPT_NAME'])) === 0) {
        $uri = (string)substr($uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
    }
}

// This section ensures that even on servers that require the URI to be in the query string (Nginx) a correct
// URI is found, and also fixes the QUERY_STRING server var and $_GET array.
if (trim($uri, '/') === '' && strncmp($query, '/', 1) === 0) {
    $query = explode('?', $query, 2);
    $uri = $query[0];
    $_SERVER['QUERY_STRING'] = isset($query[1]) ? $query[1] : '';
} else {
    $_SERVER['QUERY_STRING'] = $query;
}


parse_str($_SERVER['QUERY_STRING'], $_GET);


if (!($uri === '/' OR $uri === '')) {
    $uri = trim($uri, '/');
    if (is_base64($uri)) {
        $uri = base64_decode($uri);
    }
    $uri = explode('/', $uri);
    $uri && SafeFilter($uri);
    $preClass = isset($uri[0]) ? $uri[0] : 'index'; //类
    $method = isset($uri[1]) ? $uri[1] : 'index'; //方法

    $args = array_slice($uri, 2);
}

if (!function_exists("fastcgi_finish_request")) {
    @file_put_contents('/tmp/cgi.log', date('Y-m-d H:i:s') . "改变了fpm的模式，得配置为fast CGI\r\n", 8);
    function fastcgi_finish_request()
    {
    }
}


if (isset($_GET['c']) && !empty($_GET['c'])) {
    $preClass = trim($_GET['c']);
}

if (isset($_GET['m']) && !empty($_GET['m'])) {
    $method = trim($_GET['m']);
}


$preClass = !empty($preClass) ? $preClass : 'index';
$method = !empty($method) ? $method : 'index';

$myClass = '_' . $preClass . 'Controller';

if (!class_exists($myClass)) {
    echo '405';
    die();
}
$_instance = new $myClass();

if (!method_exists($_instance, $method)) {
    echo 406;
    die();
}


if (empty($args)) {
    $_instance->$method();
} else {
    //调用相对应的方法
    call_user_func_array(array($_instance, $method), $args);
}


