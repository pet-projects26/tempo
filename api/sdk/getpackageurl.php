<?php
require_once '../conf/main.php';
require_once '../conf/Game.php';

$packageNum = getParam('package');

if(empty($packageNum)){
    exit();
}
$conditions = array();
$conditions['WHERE']['package_id'] = $packageNum;
$row = (new PackageModel())->getRow(array('url'),$conditions['WHERE']);
print(json_encode($row));
exit();
?>