/*
Navicat MySQL Data Transfer

Source Server         : ppg
Source Server Version : 50568
Source Host           : 139.196.149.24:3306
Source Database       : backstage

Target Server Type    : MYSQL
Target Server Version : 50568
File Encoding         : 65001

Date: 2021-05-27 00:25:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `game_white_ip`
-- ----------------------------
DROP TABLE IF EXISTS `game_white_ip`;
CREATE TABLE `game_white_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) DEFAULT '' COMMENT 'ip白名单',
  `type` tinyint(1) unsigned DEFAULT '0' COMMENT '启用状态 0为启用， 1为禁用',
  `contents` varchar(255) DEFAULT '' COMMENT '说明',
  `create_time` int(11) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`) USING BTREE,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_white_ip
-- ----------------------------
INSERT INTO `game_white_ip` VALUES ('1', '127.0.0.1', '1', '', '1547710972');
INSERT INTO `game_white_ip` VALUES ('3', '192.168.240.126', '1', 'w', '1563366023');

-- ----------------------------
-- Table structure for `log_account`
-- ----------------------------
DROP TABLE IF EXISTS `log_account`;
CREATE TABLE `log_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `package` mediumint(9) DEFAULT '0' COMMENT '包号',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道',
  `account` varchar(200) NOT NULL DEFAULT '' COMMENT '帐号',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  KEY `create_time` (`create_time`),
  KEY `channel` (`channel`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log_account
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_account_seven_remain`
-- ----------------------------
DROP TABLE IF EXISTS `ny_account_seven_remain`;
CREATE TABLE `ny_account_seven_remain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `account_num` int(11) DEFAULT '0' COMMENT '创角数',
  `two` varchar(15) DEFAULT '-' COMMENT '第2天留存',
  `three` varchar(15) DEFAULT '-' COMMENT '第3天留存',
  `four` varchar(15) DEFAULT '-' COMMENT '第4天留存',
  `five` varchar(15) DEFAULT '-' COMMENT '第5天留存',
  `six` varchar(15) DEFAULT '-' COMMENT '第6天留存',
  `seven` varchar(15) DEFAULT '-' COMMENT '第7天留存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_account_seven_remain
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_action`
-- ----------------------------
DROP TABLE IF EXISTS `ny_action`;
CREATE TABLE `ny_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `group_id` int(5) DEFAULT NULL,
  `channel_id` int(5) DEFAULT NULL,
  `server` text,
  `create_time` varchar(11) DEFAULT NULL,
  `last_modified_time` varchar(11) DEFAULT NULL,
  `mark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`name`,`group_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_action
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_active`
-- ----------------------------
DROP TABLE IF EXISTS `ny_active`;
CREATE TABLE `ny_active` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '该日连接上游戏服务器的玩家数',
  `login_num` int(11) unsigned NOT NULL COMMENT '该日玩家的总登录数',
  `avg_login_num` decimal(30,2) unsigned NOT NULL COMMENT '人均登录次数',
  `avg_login_longtime` decimal(30,2) unsigned NOT NULL COMMENT '人均登录时长',
  `new_acc_num` int(11) unsigned NOT NULL COMMENT '新增用户数',
  `new_acc_avg_login_num` decimal(30,2) unsigned NOT NULL COMMENT '新增用户人均登录次数',
  `new_acc_avg_login_longtime` decimal(30,2) unsigned NOT NULL COMMENT '新增用户人均在线时长',
  `old_acc_num` int(11) unsigned NOT NULL COMMENT '老玩家数',
  `old_acc_avg_login_num` decimal(30,2) NOT NULL COMMENT '老玩家平均登录次数',
  `old_acc_avg_login_longtime` decimal(30,2) NOT NULL COMMENT '老玩家平均登录时长',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING HASH,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单服活跃统计表';

-- ----------------------------
-- Records of ny_active
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_active_account`
-- ----------------------------
DROP TABLE IF EXISTS `ny_active_account`;
CREATE TABLE `ny_active_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL,
  `package` varchar(20) DEFAULT NULL,
  `server` varchar(20) DEFAULT '0' COMMENT '服务器',
  `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
  `ip` varchar(30) DEFAULT '0' COMMENT 'ip地址',
  `account` varchar(50) DEFAULT '0' COMMENT '账号',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`),
  KEY `package` (`package`),
  KEY `server` (`server`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_active_account
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_active_account_1907`
-- ----------------------------
DROP TABLE IF EXISTS `ny_active_account_1907`;
CREATE TABLE `ny_active_account_1907` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL,
  `package` varchar(20) DEFAULT NULL,
  `server` varchar(20) DEFAULT '0' COMMENT '服务器',
  `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
  `ip` varchar(30) DEFAULT '0' COMMENT 'ip地址',
  `account` varchar(50) DEFAULT '0' COMMENT '账号',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`),
  KEY `package` (`package`),
  KEY `server` (`server`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_active_account_1907
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_active_device`
-- ----------------------------
DROP TABLE IF EXISTS `ny_active_device`;
CREATE TABLE `ny_active_device` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) DEFAULT NULL,
  `deviceId` varchar(255) DEFAULT NULL,
  `fr` varchar(100) DEFAULT NULL COMMENT '系统版本',
  `brand` varchar(255) DEFAULT NULL COMMENT '品牌',
  `mac` varchar(255) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `net` varchar(50) DEFAULT NULL COMMENT '网络',
  `imsi` varchar(255) DEFAULT NULL COMMENT '设备的imsi',
  `res` varchar(100) DEFAULT NULL COMMENT '分辨率',
  `model` varchar(100) DEFAULT NULL COMMENT '机型',
  `ts` int(10) DEFAULT NULL COMMENT '时间戳毫秒',
  `platform` tinyint(4) DEFAULT NULL COMMENT '1ios2android',
  `package` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deviceId` (`deviceId`) USING BTREE,
  KEY `channel` (`channel`) USING BTREE,
  KEY `ts` (`ts`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='激活设备';

-- ----------------------------
-- Records of ny_active_device
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_activity`
-- ----------------------------
DROP TABLE IF EXISTS `ny_activity`;
CREATE TABLE `ny_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '活动名称',
  `big_act_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '大类',
  `act_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活动ID',
  `platforms` varchar(2000) DEFAULT '' COMMENT '渠道组',
  `servers` varchar(2000) NOT NULL DEFAULT '' COMMENT '服务器列表',
  `openLvl` smallint(8) unsigned NOT NULL DEFAULT '0' COMMENT '活动等级',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '内容',
  `award` text NOT NULL COMMENT '活动奖励 【json】',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '开关：0:开1关',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '类型 0: 待审核，1已提审,2:审核通过，3被驳回',
  `add_user` varchar(50) NOT NULL DEFAULT '' COMMENT '添加人',
  `check_user` varchar(50) NOT NULL DEFAULT '' COMMENT '审核人',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `check_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '审核时间',
  `params` varchar(2000) DEFAULT '' COMMENT '参数',
  `uuid` varchar(500) NOT NULL DEFAULT '' COMMENT 'uuid',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0:未执行 1：成功 2：失败',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `model` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '模板：1是：0不是',
  `activity_sign` tinyint(2) DEFAULT '0',
  `open_ser_day` tinyint(5) DEFAULT '7',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动';

-- ----------------------------
-- Records of ny_activity
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_activity_result`
-- ----------------------------
DROP TABLE IF EXISTS `ny_activity_result`;
CREATE TABLE `ny_activity_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活动ID（自增）',
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器标识符',
  `status` tinyint(3) NOT NULL DEFAULT '-1' COMMENT '状态，-2:配置不存在，-1:插入mongo失败，0:通知活动失败，1:成功',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_activity_result
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_activity_tmp`
-- ----------------------------
DROP TABLE IF EXISTS `ny_activity_tmp`;
CREATE TABLE `ny_activity_tmp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uuid` varchar(100) NOT NULL DEFAULT '' COMMENT 'mongo的唯一ID',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '活动类型',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '活动时间',
  `content` text COMMENT '活动配置',
  `servers` text COMMENT '服务器列表',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `state` tinyint(3) NOT NULL DEFAULT '0' COMMENT '开关：0开启,1关闭',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  `last_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态0:执行1删除',
  `msg` text COMMENT '返回信息',
  `openLvl` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  PRIMARY KEY (`id`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='临时活动列表';

-- ----------------------------
-- Records of ny_activity_tmp
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_adventure_info`
-- ----------------------------
DROP TABLE IF EXISTS `ny_adventure_info`;
CREATE TABLE `ny_adventure_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通奇遇的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '该日全服所有玩家完成奇遇事件之和',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='奇遇统计表';

-- ----------------------------
-- Records of ny_adventure_info
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_app`
-- ----------------------------
DROP TABLE IF EXISTS `ny_app`;
CREATE TABLE `ny_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(50) DEFAULT NULL COMMENT '应用标识',
  `name` varchar(50) DEFAULT NULL,
  `secret` varchar(50) DEFAULT NULL,
  `platform` tinyint(4) DEFAULT NULL COMMENT '平台 0全部 1Android 2IOS',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_app
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_bantalk`
-- ----------------------------
DROP TABLE IF EXISTS `ny_bantalk`;
CREATE TABLE `ny_bantalk` (
  `channel_group` int(11) NOT NULL COMMENT '渠道组id',
  `time` int(11) DEFAULT NULL COMMENT '禁言时间(秒)',
  PRIMARY KEY (`channel_group`),
  KEY `channel_group` (`channel_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='禁言设置表';

-- ----------------------------
-- Records of ny_bantalk
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_boss_home`
-- ----------------------------
DROP TABLE IF EXISTS `ny_boss_home`;
CREATE TABLE `ny_boss_home` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器唯一标识',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '当日活跃并开启boss之家玩法的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '该日进入过boss之家的人数(去重',
  `layer` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '层数',
  `layer_join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '本层参与玩家(去重',
  `boss_die_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '本层boss死亡次数',
  `free_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '免费进入玩家数(去重',
  `free_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '免费进入总次数',
  `pay_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '付费玩家数(去重',
  `pay_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '付费进入总次数',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='boss之家';

-- ----------------------------
-- Records of ny_boss_home
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_broadcast`
-- ----------------------------
DROP TABLE IF EXISTS `ny_broadcast`;
CREATE TABLE `ny_broadcast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `starttime` int(11) DEFAULT NULL COMMENT '开始时间',
  `endtime` int(11) DEFAULT NULL COMMENT '结束时间',
  `intervaltime` int(11) DEFAULT NULL COMMENT '间隔时间',
  `synctime` int(11) DEFAULT NULL COMMENT '同步时间',
  `type` int(1) DEFAULT NULL COMMENT '类型 0 顶部 1 系统广播  2顶部加系统广播',
  `content` mediumtext COMMENT '内容',
  `server` mediumtext,
  `status` int(11) DEFAULT '0' COMMENT '0未同步 1已同步 2删除 ',
  `adder` varchar(255) DEFAULT '0',
  `syncer` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_broadcast
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_channel`
-- ----------------------------
DROP TABLE IF EXISTS `ny_channel`;
CREATE TABLE `ny_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) DEFAULT NULL COMMENT '编号',
  `channel_id` varchar(20) DEFAULT NULL COMMENT '渠道标识',
  `name` varchar(50) DEFAULT NULL COMMENT '渠道名称',
  `game_id` int(11) DEFAULT NULL COMMENT '游戏id',
  `game_secret_key` varchar(100) DEFAULT NULL,
  `game_name` varchar(20) DEFAULT NULL,
  `game_abbreviation` varchar(20) DEFAULT NULL,
  `cdn_url` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `grouplist` varchar(255) DEFAULT NULL COMMENT '权限用户组',
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_id` (`channel_id`) USING BTREE,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_channel
-- ----------------------------
INSERT INTO `ny_channel` VALUES ('1', null, 'local', '双线系', '1', '1', '1', '1', '', '1540882681', '[\"1\"]');

-- ----------------------------
-- Table structure for `ny_channel_group`
-- ----------------------------
DROP TABLE IF EXISTS `ny_channel_group`;
CREATE TABLE `ny_channel_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '渠道组名称',
  `server` text COMMENT '包含哪些服务器',
  `version` varchar(50) DEFAULT NULL,
  `channel_package` text COMMENT '开通的渠道和包',
  `create_time` int(11) DEFAULT NULL,
  `last_modified_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_channel_group
-- ----------------------------
INSERT INTO `ny_channel_group` VALUES ('1', '双线系', '[\"123\",\"2\"]', null, '[]', '1539846280', null);

-- ----------------------------
-- Table structure for `ny_charge_distribute`
-- ----------------------------
DROP TABLE IF EXISTS `ny_charge_distribute`;
CREATE TABLE `ny_charge_distribute` (
  `server` varchar(20) NOT NULL,
  `chargenum` int(11) NOT NULL DEFAULT '0' COMMENT '付费玩家(账号)',
  `range1` int(11) NOT NULL DEFAULT '0' COMMENT '[0,100]',
  `range2` int(11) NOT NULL DEFAULT '0' COMMENT '[100,200]',
  `range3` int(11) NOT NULL DEFAULT '0' COMMENT '[200,500]',
  `range4` int(11) NOT NULL DEFAULT '0' COMMENT '[500,1000]',
  `range5` int(11) NOT NULL DEFAULT '0' COMMENT '[1000,2000]',
  `range6` int(11) NOT NULL DEFAULT '0' COMMENT '[2000,5000]',
  `range7` int(11) NOT NULL DEFAULT '0' COMMENT '[5000,10000]',
  `range8` int(11) NOT NULL DEFAULT '0' COMMENT '[10000,20000]',
  `range9` int(11) NOT NULL DEFAULT '0' COMMENT '[20000,&]',
  `create_time` date NOT NULL COMMENT '日期',
  PRIMARY KEY (`server`,`create_time`),
  UNIQUE KEY `server_creater_time` (`server`,`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值分布(凌晨跑脚本写入) by william';

-- ----------------------------
-- Records of ny_charge_distribute
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_charge_index`
-- ----------------------------
DROP TABLE IF EXISTS `ny_charge_index`;
CREATE TABLE `ny_charge_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `index` int(11) NOT NULL COMMENT '充值档位',
  `name` varchar(100) NOT NULL COMMENT '档位名称',
  `price` decimal(10,2) NOT NULL,
  `charge_num` int(11) NOT NULL DEFAULT '0' COMMENT '充值人数',
  `charge_count` int(11) NOT NULL DEFAULT '0' COMMENT '充值次数',
  `charge_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='充值档位统计';

-- ----------------------------
-- Records of ny_charge_index
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_charge_total`
-- ----------------------------
DROP TABLE IF EXISTS `ny_charge_total`;
CREATE TABLE `ny_charge_total` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '开服时间',
  `channel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `server` varchar(255) DEFAULT NULL COMMENT '服务器id',
  `servername` varchar(255) DEFAULT NULL COMMENT '服务器名称',
  `one` varchar(11) DEFAULT '-',
  `two` varchar(11) DEFAULT '-',
  `three` varchar(11) DEFAULT '-',
  `four` varchar(11) DEFAULT '-',
  `five` varchar(11) DEFAULT '-',
  `six` varchar(11) DEFAULT '-',
  `seven` varchar(11) DEFAULT '-',
  `eight` varchar(11) DEFAULT '-',
  `nine` varchar(11) DEFAULT '-',
  `ten` varchar(11) DEFAULT '-',
  `eleven` varchar(11) DEFAULT '-',
  `twelve` varchar(11) DEFAULT '-',
  `thirteen` varchar(11) DEFAULT '-',
  `fourteen` varchar(11) DEFAULT '-',
  `fifteen` varchar(11) DEFAULT '-',
  `sixteen` varchar(11) DEFAULT '-',
  `seventeen` varchar(11) DEFAULT '-',
  `eighteen` varchar(11) DEFAULT '-',
  `nineteen` varchar(11) DEFAULT '-',
  `twenty` varchar(11) DEFAULT '-',
  `twentyone` varchar(11) DEFAULT '-',
  `twentytwo` varchar(11) DEFAULT '-',
  `twentythree` varchar(11) DEFAULT '-',
  `twentyfour` varchar(11) DEFAULT '-',
  `twentyfive` varchar(11) DEFAULT '-',
  `twentysix` varchar(11) DEFAULT '-',
  `twentyseven` varchar(11) DEFAULT '-',
  `twentyeight` varchar(11) DEFAULT '-',
  `twentynine` varchar(11) DEFAULT '-',
  `thirty` varchar(11) DEFAULT '-',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `date` (`date`),
  KEY `channel` (`channel`),
  KEY `server` (`server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_charge_total
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_charge_true`
-- ----------------------------
DROP TABLE IF EXISTS `ny_charge_true`;
CREATE TABLE `ny_charge_true` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(20) DEFAULT '' COMMENT '服务器id',
  `order_num` varchar(50) DEFAULT '' COMMENT '订单号',
  `gold` int(11) DEFAULT NULL COMMENT '元宝',
  `money` int(11) unsigned DEFAULT '0' COMMENT '金额',
  `account` varchar(200) DEFAULT '' COMMENT '账号',
  `role_id` varchar(50) DEFAULT '' COMMENT '角色id',
  `role_career` int(8) DEFAULT NULL,
  `role_name` varchar(50) DEFAULT '' COMMENT '角色名',
  `item_id` int(11) unsigned DEFAULT '0' COMMENT '充值档次',
  `first` tinyint(1) unsigned DEFAULT '0' COMMENT '是否首冲，1是首冲，0不是首冲',
  `create_time` int(11) unsigned DEFAULT '0' COMMENT '创建时间',
  `admin` varchar(128) DEFAULT '' COMMENT '管理员名称',
  `checked` tinyint(3) DEFAULT '0',
  `package` varchar(100) DEFAULT NULL COMMENT '包号',
  `role_level` int(11) DEFAULT NULL COMMENT '等级',
  `channel` varchar(100) DEFAULT NULL COMMENT '渠道',
  `idfa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_num` (`order_num`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `role_name` (`role_name`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='正常充值表20180919';

-- ----------------------------
-- Records of ny_charge_true
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_client`
-- ----------------------------
DROP TABLE IF EXISTS `ny_client`;
CREATE TABLE `ny_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL COMMENT '账号',
  `package` varchar(20) DEFAULT NULL,
  `server` varchar(20) DEFAULT NULL COMMENT '服务器id',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `role` varchar(50) DEFAULT NULL COMMENT '角色',
  `model` varchar(100) DEFAULT '0' COMMENT '机型，0表示未知',
  `network` varchar(20) DEFAULT '0' COMMENT '网络类型 WIFI 2G 3G 4G null表示未知',
  `mac` varchar(255) DEFAULT '0' COMMENT 'MAC地址，0表示未知',
  `brand` varchar(50) DEFAULT '' COMMENT '手机品牌',
  `memory` varchar(30) DEFAULT '0' COMMENT '内存，格式是''当前内存(总内存)''，0表示未知',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  `udid` varchar(255) DEFAULT '' COMMENT '设备码',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`),
  KEY `package` (`package`),
  KEY `server` (`server`),
  KEY `account` (`account`),
  KEY `role` (`role`),
  KEY `mac` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端信息，玩家登录账号时写入记录';

-- ----------------------------
-- Records of ny_client
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_cloudland_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_cloudland_copy`;
CREATE TABLE `ny_cloudland_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通云梦秘境的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数, 采集数大于1的人数',
  `consume_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消耗云梦密令之和',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='云梦秘境统计表';

-- ----------------------------
-- Records of ny_cloudland_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_consumer_record`
-- ----------------------------
DROP TABLE IF EXISTS `ny_consumer_record`;
CREATE TABLE `ny_consumer_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `source` smallint(8) NOT NULL COMMENT '消费项目',
  `project` varchar(50) DEFAULT '' COMMENT '预留字段',
  `money_type` smallint(8) NOT NULL COMMENT '货币类型; 1:元宝; 41:仙玉',
  `consumer_num` int(11) NOT NULL DEFAULT '0' COMMENT '消费人数',
  `consumer_money` decimal(19,2) NOT NULL DEFAULT '0.00' COMMENT '玩家消费该项目的货币',
  `consumer_all_money` decimal(19,2) NOT NULL DEFAULT '0.00' COMMENT '玩家日消费该货币总数',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `source` (`source`) USING BTREE,
  KEY `money_type` (`money_type`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='央服每日消费项目统计表';

-- ----------------------------
-- Records of ny_consumer_record
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_daily_consume`
-- ----------------------------
DROP TABLE IF EXISTS `ny_daily_consume`;
CREATE TABLE `ny_daily_consume` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃玩家数',
  `consume_gold` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '日消费元宝数量',
  `consume_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日消费人数',
  `produce_gold` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '日产出元宝数量',
  `charge_produce_gold` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值元宝产出',
  `free_produce_gold` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '免费元宝产出',
  `gold_total_stock` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '元宝总存量',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_daily_consume
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_daily_recharge`
-- ----------------------------
DROP TABLE IF EXISTS `ny_daily_recharge`;
CREATE TABLE `ny_daily_recharge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '当天活跃玩家',
  `first_recharge` int(11) NOT NULL DEFAULT '0' COMMENT '当天首充人数',
  `recharge_num` int(11) NOT NULL DEFAULT '0' COMMENT '当天充值人数',
  `recharge_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '当天充值总额',
  `new_active_num` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增跳转数',
  `new_recharge_num` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增充值人数',
  `new_recharge_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '当天新增充值金额',
  `old_user_login_num` int(11) NOT NULL DEFAULT '0' COMMENT '老玩家登录数',
  `old_user_recharge_num` int(11) NOT NULL DEFAULT '0' COMMENT '老玩家登录总数',
  `old_user_recharge_money` decimal(11,2) NOT NULL,
  `roll_user_num` int(11) NOT NULL DEFAULT '0' COMMENT '滚服玩家数',
  `roll_user_recharge_num` int(11) NOT NULL DEFAULT '0' COMMENT '滚服付费玩家数',
  `roll_user_recharge_money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '滚服付费金额',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='每日充值';

-- ----------------------------
-- Records of ny_daily_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_daily_trial`
-- ----------------------------
DROP TABLE IF EXISTS `ny_daily_trial`;
CREATE TABLE `ny_daily_trial` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '活跃玩家并且开启副本的玩家数',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '副本类型:5:金币副本6:真气副本7:仙器副本8:灵宠副本',
  `join_num` int(11) NOT NULL DEFAULT '0' COMMENT '参与人数',
  `challenge_num` int(11) NOT NULL COMMENT '挑战人数(成功才算',
  `challenge_count` int(11) NOT NULL DEFAULT '0' COMMENT '挑战次数(成功才算',
  `inspire_num` int(11) NOT NULL DEFAULT '0' COMMENT '鼓舞玩家数',
  `inspire_count` int(11) NOT NULL DEFAULT '0' COMMENT '总鼓舞次数',
  `add_num_role` int(11) NOT NULL DEFAULT '0' COMMENT '增加挑战次数玩家数(去重',
  `add_num_sum` int(11) NOT NULL DEFAULT '0' COMMENT '玩家增加挑战次数总数',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_daily_trial
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_deal_group`
-- ----------------------------
DROP TABLE IF EXISTS `ny_deal_group`;
CREATE TABLE `ny_deal_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `server` varchar(5000) DEFAULT '' COMMENT '服务器列表',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_deal_group
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_deal_tpl`
-- ----------------------------
DROP TABLE IF EXISTS `ny_deal_tpl`;
CREATE TABLE `ny_deal_tpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `type` int(1) DEFAULT NULL COMMENT '类型 0：老服 1：新服 2：合服 3：跨服',
  `start_time` int(11) DEFAULT '0' COMMENT '上架时间',
  `end_time` int(11) DEFAULT '0' COMMENT '下架时间',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_deal_tpl
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_device_daily`
-- ----------------------------
DROP TABLE IF EXISTS `ny_device_daily`;
CREATE TABLE `ny_device_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `num` int(11) DEFAULT NULL COMMENT '设备数',
  `active_num` int(11) DEFAULT NULL COMMENT '激活数',
  `login_loss` varchar(8) DEFAULT NULL COMMENT '登录页面流失率',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_device_daily
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_device_loss`
-- ----------------------------
DROP TABLE IF EXISTS `ny_device_loss`;
CREATE TABLE `ny_device_loss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL,
  `channel` varchar(20) DEFAULT NULL COMMENT '渠道',
  `package` int(11) DEFAULT NULL COMMENT '包',
  `new_num` int(11) DEFAULT '0' COMMENT '当日新增设备数',
  `two` varchar(11) DEFAULT '-' COMMENT '第2天流失',
  `three` varchar(11) DEFAULT '-' COMMENT '第3天流失',
  `seven` varchar(11) DEFAULT '-' COMMENT '第7天流失',
  `fifteen` varchar(11) DEFAULT '-' COMMENT '第15天流失',
  `thirty` varchar(11) DEFAULT '-' COMMENT '第30天流失',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `date` (`date`),
  KEY `channel` (`channel`),
  KEY `package` (`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_device_loss
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_failed_login`
-- ----------------------------
DROP TABLE IF EXISTS `ny_failed_login`;
CREATE TABLE `ny_failed_login` (
  `ip` char(15) NOT NULL DEFAULT '' COMMENT 'ip',
  `username` char(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `created` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  KEY `ip` (`ip`),
  KEY `created` (`created`),
  KEY `ip_created` (`ip`,`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户登录失败记录';

-- ----------------------------
-- Records of ny_failed_login
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_fairy_info`
-- ----------------------------
DROP TABLE IF EXISTS `ny_fairy_info`;
CREATE TABLE `ny_fairy_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通护送仙女的人数',
  `escort_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '护送人数',
  `escort_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '护送总次数',
  `rob_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '拦截人数',
  `rob_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '拦截总次数',
  `refresh_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '刷新仙女人数',
  `refresh_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '刷新仙女总次数',
  `select_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '刷新仙女人数',
  `select_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '刷新仙女总次数',
  `join_vip6` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '护送人数中≥vip6的玩家数',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='护送仙女统计表';

-- ----------------------------
-- Records of ny_fairy_info
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_gift`
-- ----------------------------
DROP TABLE IF EXISTS `ny_gift`;
CREATE TABLE `ny_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent` varchar(20) DEFAULT NULL COMMENT '平台',
  `name` varchar(255) DEFAULT NULL COMMENT '礼包名称',
  `info` mediumtext COMMENT '说明',
  `tips` mediumtext COMMENT '激活提示',
  `times` int(4) DEFAULT '1' COMMENT '使用次数',
  `type` tinyint(3) DEFAULT NULL COMMENT '0一对一 1一对多',
  `server` text COMMENT '服务器',
  `open_time` int(11) DEFAULT NULL COMMENT '生效时间',
  `end_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `item` mediumtext COMMENT '道具',
  `sort` int(11) DEFAULT NULL COMMENT '批次，即排序',
  `status` tinyint(2) DEFAULT '1' COMMENT '0禁用 1正常',
  `num` int(11) DEFAULT NULL COMMENT '生成个数',
  `pnum` int(11) DEFAULT NULL COMMENT '追加个数',
  `user` varchar(20) DEFAULT NULL COMMENT '添加者',
  `ischeck` tinyint(1) DEFAULT '0' COMMENT '审核状态 默认为0',
  `issync` tinyint(1) DEFAULT '0' COMMENT '是否同步新服',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `create_time` (`create_time`),
  KEY `type` (`type`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_gift
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_gift_order`
-- ----------------------------
DROP TABLE IF EXISTS `ny_gift_order`;
CREATE TABLE `ny_gift_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(50) DEFAULT NULL,
  `server` varchar(50) DEFAULT '' COMMENT '服务器id',
  `package` int(11) unsigned DEFAULT '0' COMMENT '包id',
  `order_num` varchar(50) DEFAULT '' COMMENT '订单号',
  `corder_num` varchar(50) DEFAULT NULL,
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色id',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名',
  `role_level` int(11) DEFAULT NULL COMMENT '角色等级',
  `index` int(11) unsigned DEFAULT '0' COMMENT '充值档次',
  `money` double(10,2) DEFAULT NULL COMMENT '商品金额',
  `reward_item_id` int(11) DEFAULT NULL COMMENT '档位获取的物品id',
  `index_name` varchar(100) DEFAULT NULL COMMENT '商品档位名称',
  `first` tinyint(1) unsigned DEFAULT '0' COMMENT '是否首赠，1是，0不是',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `send_time` int(11) NOT NULL DEFAULT '0' COMMENT '发送时间',
  `admin` varchar(128) DEFAULT '' COMMENT '管理员名称',
  `checked` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态: 0: 未审核 1:已审核',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `status` (`checked`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_gift_order
-- ----------------------------
INSERT INTO `ny_gift_order` VALUES ('1', '1', 'local', '8070', '190507213249E91213283', '12124512352345234', 'local_kikiki', '8817567859121', 'S10.kioki', '94', '110', '2998.00', '90140001', '300000元宝', '1', '1557235969', '1557236014', '1', '1');
INSERT INTO `ny_gift_order` VALUES ('2', '1', 'local', '8070', '190716141029E12282554', '2131231241243', 'local_www45', '8817567861228', 'S10.睦贤颖', '104', '110', '10.00', '0', '首充', '1', '1563257429', '1563257432', '1', '1');

-- ----------------------------
-- Table structure for `ny_guide`
-- ----------------------------
DROP TABLE IF EXISTS `ny_guide`;
CREATE TABLE `ny_guide` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL COMMENT '该日新增且连接上游戏服务器的用户数',
  `one_guide_get_num` int(11) NOT NULL DEFAULT '0' COMMENT '第一步引导触发人数',
  `guide_id` varchar(32) NOT NULL COMMENT '引导id',
  `get_num` int(11) NOT NULL DEFAULT '0' COMMENT '触发引导人数',
  `done_num` int(11) NOT NULL DEFAULT '0' COMMENT '完成引导人数',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `guide_id` (`guide_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='新手引导央服统计表';

-- ----------------------------
-- Records of ny_guide
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_hotupdate_log`
-- ----------------------------
DROP TABLE IF EXISTS `ny_hotupdate_log`;
CREATE TABLE `ny_hotupdate_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(100) NOT NULL COMMENT '渠道组',
  `server` varchar(100) DEFAULT NULL COMMENT '服务器',
  `type` int(11) NOT NULL COMMENT '操作类型',
  `pact` int(11) NOT NULL COMMENT '协议号',
  `admin` varchar(100) DEFAULT NULL COMMENT '管理员',
  `ip` varchar(100) DEFAULT NULL COMMENT 'ip地址',
  `send_data` varchar(255) DEFAULT NULL COMMENT '发送内容',
  `create_time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_hotupdate_log
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_hy_account`
-- ----------------------------
DROP TABLE IF EXISTS `ny_hy_account`;
CREATE TABLE `ny_hy_account` (
  `server` varchar(100) NOT NULL,
  `time` date NOT NULL,
  `new` int(11) DEFAULT '0' COMMENT '新增账号数',
  `old` int(11) DEFAULT '0' COMMENT '老账号数',
  `total` int(11) DEFAULT '0' COMMENT '总数',
  `wau` int(11) DEFAULT '0' COMMENT '七天账号登录数',
  `mau` int(11) DEFAULT '0' COMMENT '一个月的账号登录数',
  PRIMARY KEY (`server`,`time`),
  UNIQUE KEY `server_time` (`server`,`time`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='备注:活跃玩家表';

-- ----------------------------
-- Records of ny_hy_account
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_item_buy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_item_buy`;
CREATE TABLE `ny_item_buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(50) NOT NULL DEFAULT '' COMMENT ' 绑定物品id',
  `price` decimal(10,0) NOT NULL COMMENT '价格',
  `num` int(10) NOT NULL DEFAULT '1',
  `item_tpl` tinyint(4) DEFAULT '1' COMMENT '道具模板类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `item_id` (`item_id`) USING BTREE,
  KEY `item_tpl` (`item_tpl`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ny_item_buy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_item_check`
-- ----------------------------
DROP TABLE IF EXISTS `ny_item_check`;
CREATE TABLE `ny_item_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(255) DEFAULT NULL COMMENT '渠道组',
  `server` text COMMENT '服务器',
  `item` text COMMENT '道具配置',
  `mail` varchar(255) DEFAULT NULL COMMENT '邮件',
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `white` varchar(255) DEFAULT NULL COMMENT '白名单',
  `time` int(11) DEFAULT NULL COMMENT '时间',
  `gold` varchar(255) DEFAULT NULL COMMENT '元宝信息',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_item_check
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_item_config`
-- ----------------------------
DROP TABLE IF EXISTS `ny_item_config`;
CREATE TABLE `ny_item_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bind_item_id` varchar(50) DEFAULT '' COMMENT ' 绑定物品id',
  `item_id` varchar(50) DEFAULT '' COMMENT '物品id',
  `item_name` varchar(50) DEFAULT '' COMMENT '物品名称',
  `item_value` varchar(255) DEFAULT '' COMMENT '道具价值',
  `item_info` varchar(255) DEFAULT '' COMMENT '描述',
  `item_produce` varchar(2000) DEFAULT NULL COMMENT '非活动的产出途径',
  `item_advice` varchar(2000) DEFAULT '' COMMENT '投放建议',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `bind_item_id` (`bind_item_id`) USING BTREE,
  KEY `item_id` (`item_id`) USING BTREE,
  KEY `item_name` (`item_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_item_config
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_itemcheck_result`
-- ----------------------------
DROP TABLE IF EXISTS `ny_itemcheck_result`;
CREATE TABLE `ny_itemcheck_result` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `coin_type` tinyint(4) DEFAULT NULL COMMENT '1元宝2绑元3道具',
  `coin` bigint(20) DEFAULT NULL COMMENT '元宝/绑元',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `type` tinyint(4) DEFAULT NULL COMMENT '1产出0消耗',
  `server` varchar(255) DEFAULT NULL COMMENT '服务器',
  `check_count` int(11) DEFAULT NULL COMMENT '报警数量',
  `create_time` int(11) DEFAULT NULL,
  `item_id` double(50,0) DEFAULT NULL COMMENT '道具id',
  `role_name` varchar(50) DEFAULT NULL,
  `dayu_count` int(11) DEFAULT NULL COMMENT '该服共有多少人超出报警值',
  `get_source` varchar(2000) DEFAULT NULL COMMENT '来源',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `coin_type` (`coin_type`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `item_id` (`item_id`) USING BTREE,
  KEY `role_name` (`role_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_itemcheck_result
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_level_remain`
-- ----------------------------
DROP TABLE IF EXISTS `ny_level_remain`;
CREATE TABLE `ny_level_remain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `new_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增用户数',
  `level` int(4) unsigned NOT NULL DEFAULT '1' COMMENT '等级',
  `level_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '该等级人数',
  `reach_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '达到人数：≥该等级的玩家数。',
  `pass_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过人数： ＞该等级的玩家数。',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_level_remain
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_log`
-- ----------------------------
DROP TABLE IF EXISTS `ny_log`;
CREATE TABLE `ny_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志表自增id',
  `uid` int(10) NOT NULL DEFAULT '0' COMMENT '用户id',
  `username` varchar(128) NOT NULL DEFAULT '' COMMENT '用户名',
  `ip` varchar(15) NOT NULL DEFAULT '' COMMENT '用户ip',
  `ctrl` varchar(100) NOT NULL DEFAULT '' COMMENT '访问模块',
  `act` varchar(100) NOT NULL DEFAULT '' COMMENT '用户操作',
  `desc2` varchar(500) NOT NULL DEFAULT '' COMMENT '简要描述',
  `data` text COMMENT '数据',
  `created` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `username` (`username`) USING BTREE,
  KEY `ip` (`ip`) USING BTREE,
  KEY `ctrl_act` (`ctrl`,`act`) USING BTREE,
  KEY `created` (`created`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='日志表';

-- ----------------------------
-- Records of ny_log
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_login`
-- ----------------------------
DROP TABLE IF EXISTS `ny_login`;
CREATE TABLE `ny_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='玩家账号登录';

-- ----------------------------
-- Records of ny_login
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_ltv_record`
-- ----------------------------
DROP TABLE IF EXISTS `ny_ltv_record`;
CREATE TABLE `ny_ltv_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `new_account` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增用户',
  `one_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第一天充值数',
  `two_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第二天充值数',
  `three_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第三天充值数',
  `four_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第四天充值数',
  `five_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第五天充值数',
  `six_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第六天充值数',
  `seven_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第七天充值数',
  `eight_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第八天充值数',
  `nine_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第九天充值数',
  `ten_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第十天充值数',
  `fourteen_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第十四天充值数',
  `twenty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第二十天充值数',
  `thirty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第三十天充值数',
  `forty_five_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第四十五天充值数',
  `sixty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第六十天充值数',
  `ninety_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第九十天充值数',
  `one_hundred_twenty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第一百二十天充值数',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='LTV';

-- ----------------------------
-- Records of ny_ltv_record
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_mac`
-- ----------------------------
DROP TABLE IF EXISTS `ny_mac`;
CREATE TABLE `ny_mac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL COMMENT '渠道',
  `package` varchar(20) DEFAULT NULL,
  `mac` varchar(30) DEFAULT '0' COMMENT 'MAC地址，0表示未知',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`),
  KEY `package` (`package`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='获取mac，玩家登录角色时写入记录';

-- ----------------------------
-- Records of ny_mac
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_mail`
-- ----------------------------
DROP TABLE IF EXISTS `ny_mail`;
CREATE TABLE `ny_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups` text COMMENT '渠道组',
  `admin` varchar(50) NOT NULL DEFAULT '' COMMENT '后台操作账号，不是后台操作发送时为空',
  `role_id` text COMMENT '收件角色ID',
  `role_name` text COMMENT '收件角色名称',
  `servers` varchar(200) DEFAULT '' COMMENT '服务器标识符',
  `sender` varchar(50) DEFAULT NULL COMMENT '邮件发送人',
  `agreer` varchar(50) DEFAULT NULL COMMENT '审核人',
  `title` varchar(50) DEFAULT '' COMMENT '邮件标题',
  `content` varchar(1000) DEFAULT '' COMMENT '邮件文本内容',
  `attach` text COMMENT '附件内容',
  `send_type` tinyint(2) DEFAULT '0' COMMENT '是否定时发送;0否:1是',
  `book_time` int(11) DEFAULT '0' COMMENT '定时发送时间',
  `status` tinyint(3) unsigned DEFAULT '0' COMMENT '审核 0：未审核 1：已审核',
  `is_send` tinyint(3) unsigned DEFAULT '0' COMMENT '是否发送，0:未发送，1:已发送, 3:超时未审核未发送',
  `create_time` int(11) unsigned DEFAULT '0' COMMENT '创建时间',
  `send_time` int(11) DEFAULT NULL COMMENT '发送时间',
  `msg` varchar(2000) DEFAULT '' COMMENT '发送返回值',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `send_type` (`send_type`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_mail
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_mall_record`
-- ----------------------------
DROP TABLE IF EXISTS `ny_mall_record`;
CREATE TABLE `ny_mall_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `product_id` double(50,0) NOT NULL COMMENT '商城唯一id',
  `item_id` double(50,0) NOT NULL COMMENT '物品id',
  `item_name` varchar(60) NOT NULL COMMENT '物品名(商品名',
  `currency_id` double(50,0) NOT NULL DEFAULT '0' COMMENT '货币id',
  `currency_name` varchar(60) DEFAULT NULL,
  `mall_type` int(11) NOT NULL DEFAULT '0' COMMENT '大分类id',
  `mall_name` varchar(60) DEFAULT NULL COMMENT '大分类名',
  `child_mall_type` int(11) NOT NULL DEFAULT '0' COMMENT '子分类id',
  `child_mall_name` varchar(60) DEFAULT NULL,
  `consumer` int(11) NOT NULL DEFAULT '0' COMMENT '消费人数',
  `monetary` decimal(19,2) NOT NULL DEFAULT '0.00' COMMENT '消费总额',
  `mall_consumer` int(11) NOT NULL DEFAULT '0' COMMENT '商城总消费人数',
  `mall_monetary` decimal(19,2) NOT NULL DEFAULT '0.00' COMMENT '商城总消费金额',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`) USING BTREE,
  KEY `item_name` (`item_name`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='央服每日商品消费统计表';

-- ----------------------------
-- Records of ny_mall_record
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_many_boss`
-- ----------------------------
DROP TABLE IF EXISTS `ny_many_boss`;
CREATE TABLE `ny_many_boss` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃玩家并且开启多人BOSS的玩家数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `challenge_count` int(11) NOT NULL DEFAULT '0' COMMENT '挑战次数',
  `challenge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '挑战人数',
  `inspire_num` int(11) unsigned NOT NULL COMMENT '鼓舞人数',
  `gold_inspire_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '金币鼓舞总次数(没去重',
  `gold_inspire_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '金币鼓舞玩家数(去重',
  `ingots_inspire_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '元宝鼓舞总次数(没去重',
  `ingots_inspire_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '元宝鼓舞玩家数',
  `add_num_role` int(11) NOT NULL DEFAULT '0' COMMENT '增加挑战次数/使用卷轴的玩家数(去重',
  `add_num_sum` int(11) NOT NULL DEFAULT '0' COMMENT '玩家增加次数/使用卷轴总数',
  `create_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_many_boss
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ny_menu`;
CREATE TABLE `ny_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单自增id',
  `type` varchar(50) DEFAULT '' COMMENT '菜单类型：央服、单服',
  `name` varchar(100) DEFAULT '' COMMENT '菜单名称',
  `url` varchar(256) DEFAULT '' COMMENT '链接',
  `parent` int(11) DEFAULT '0' COMMENT '上级菜单id',
  `weight` int(11) DEFAULT '0' COMMENT '排序',
  `status` int(11) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `type_status` (`type`,`status`) USING BTREE,
  KEY `parent` (`parent`) USING BTREE,
  KEY `weight` (`weight`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2355 DEFAULT CHARSET=utf8 COMMENT='菜单';

-- ----------------------------
-- Records of ny_menu
-- ----------------------------
INSERT INTO `ny_menu` VALUES ('1', 'tree', '系统设置', '', '0', '43', '1');
INSERT INTO `ny_menu` VALUES ('2', 'tree', '央服菜单', 'admin.php?ctrl=menu&type=tree', '1', '44', '1');
INSERT INTO `ny_menu` VALUES ('4', 'tree', '用户组', 'admin.php?ctrl=usergroup', '1', '45', '1');
INSERT INTO `ny_menu` VALUES ('5', 'tree', '管理员', 'admin.php?ctrl=user', '1', '46', '1');
INSERT INTO `ny_menu` VALUES ('6', 'tree', '操作日志', 'admin.php?ctrl=log', '1', '47', '2');
INSERT INTO `ny_menu` VALUES ('7', 'tree', '权限管理', 'admin.php?ctrl=permit', '1', '48', '1');
INSERT INTO `ny_menu` VALUES ('8', 'tree', '服务器信息', '', '0', '37', '1');
INSERT INTO `ny_menu` VALUES ('9', 'tree', '计划任务', 'admin.php?ctrl=cron', '1', '49', '2');
INSERT INTO `ny_menu` VALUES ('10', 'tree', '修改密码', 'admin.php?ctrl=user&act=change_password_page', '1', '50', '1');
INSERT INTO `ny_menu` VALUES ('11', 'tree', '翻译', 'admin.php?ctrl=translate', '1', '51', '0');
INSERT INTO `ny_menu` VALUES ('12', 'tree', '平台管理', 'admin.php?ctrl=agent', '8', '38', '2');
INSERT INTO `ny_menu` VALUES ('13', 'tree', '服务器管理', 'admin.php?ctrl=server', '8', '39', '1');
INSERT INTO `ny_menu` VALUES ('14', 'tree', '单服菜单', 'admin.php?ctrl=menu&type=leaf', '1', '52', '2');
INSERT INTO `ny_menu` VALUES ('15', 'tree', '系统参数', 'admin.php?ctrl=setting', '1', '53', '2');
INSERT INTO `ny_menu` VALUES ('16', 'tree', '运维工具', 'admin.php?ctrl=variable&act=yunwei_page', '1', '54', '2');
INSERT INTO `ny_menu` VALUES ('17', 'tree', '分区管理', 'admin.php?ctrl=zone', '8', '42', '1');
INSERT INTO `ny_menu` VALUES ('18', 'tree', '渠道管理', 'admin.php?ctrl=channel', '8', '40', '1');
INSERT INTO `ny_menu` VALUES ('19', 'tree', '包管理', 'admin.php?ctrl=package', '8', '41', '1');
INSERT INTO `ny_menu` VALUES ('20', 'tree', '工具', 'admin.php?ctrl=tool', '1', '55', '0');
INSERT INTO `ny_menu` VALUES ('59', 'tree', '付费数据', '', '0', '6', '1');
INSERT INTO `ny_menu` VALUES ('22', 'tree', '充值记录', 'admin.php?ctrl=charge&act=record', '2325', '2', '1');
INSERT INTO `ny_menu` VALUES ('23', 'tree', '充值统计', '', '0', '0', '0');
INSERT INTO `ny_menu` VALUES ('24', 'tree', '版本更新管理', 'admin.php?ctrl=setting&amp;act=index', '1', '56', '2');
INSERT INTO `ny_menu` VALUES ('25', 'tree', '每日充值', 'admin.php?ctrl=charge&act=daily', '23', '3', '2');
INSERT INTO `ny_menu` VALUES ('61', 'tree', '玩家下线', 'admin.php?ctrl=role&act=manual_logout', '34', '33', '2');
INSERT INTO `ny_menu` VALUES ('27', 'tree', '充值等级', 'admin.php?ctrl=charge&act=level', '23', '4', '1');
INSERT INTO `ny_menu` VALUES ('28', 'tree', '充值排行', 'admin.php?ctrl=charge&act=index', '23', '5', '1');
INSERT INTO `ny_menu` VALUES ('2235', 'tree', 'GM指令', 'admin.php?ctrl=setting&act=gmcmd', '1', '59', '1');
INSERT INTO `ny_menu` VALUES ('54', 'tree', '客户端信息', 'admin.php?ctrl=client', '39', '19', '2');
INSERT INTO `ny_menu` VALUES ('34', 'tree', '游戏管理', '', '0', '29', '1');
INSERT INTO `ny_menu` VALUES ('35', 'tree', '充值处理', 'admin.php?ctrl=charge&act=manual', '34', '34', '0');
INSERT INTO `ny_menu` VALUES ('36', 'tree', '在线与注册', '', '0', '12', '0');
INSERT INTO `ny_menu` VALUES ('37', 'tree', '实时在线', 'admin.php?ctrl=online&act=real', '36', '13', '1');
INSERT INTO `ny_menu` VALUES ('58', 'tree', '功能开关', 'admin.php?ctrl=switch', '1', '57', '0');
INSERT INTO `ny_menu` VALUES ('38', 'tree', '发送邮件', 'admin.php?ctrl=mail&act=manual', '34', '35', '1');
INSERT INTO `ny_menu` VALUES ('39', 'tree', '数据统计', '', '0', '17', '1');
INSERT INTO `ny_menu` VALUES ('40', 'tree', '登录统计', 'admin.php?&ctrl=login&act=record', '39', '20', '0');
INSERT INTO `ny_menu` VALUES ('41', 'tree', '角色统计', 'admin.php?ctrl=role', '39', '21', '0');
INSERT INTO `ny_menu` VALUES ('42', 'tree', '新增玩家', 'admin.php?ctrl=charge&act=cnew', '59', '7', '0');
INSERT INTO `ny_menu` VALUES ('43', 'tree', 'LTV统计', 'admin.php?ctrl=stat&act=ltv_index', '59', '8', '2');
INSERT INTO `ny_menu` VALUES ('44', 'tree', '后续充值', 'admin.php?ctrl=ltv&act=follow', '59', '9', '2');
INSERT INTO `ny_menu` VALUES ('50', 'tree', '产出消耗', 'admin.php?ctrl=item', '39', '22', '0');
INSERT INTO `ny_menu` VALUES ('45', 'tree', '留存率', 'admin.php?ctrl=ltv&act=remain', '59', '10', '2');
INSERT INTO `ny_menu` VALUES ('46', 'tree', 'ARPU', 'admin.php?ctrl=charge&act=arpu', '59', '11', '2');
INSERT INTO `ny_menu` VALUES ('47', 'tree', '当前在线', 'admin.php?ctrl=login&act=online', '36', '14', '1');
INSERT INTO `ny_menu` VALUES ('48', 'tree', '注册数据', 'admin.php?ctrl=register', '36', '15', '1');
INSERT INTO `ny_menu` VALUES ('49', 'tree', '综合在线', 'admin.php?ctrl=online&act=multi', '36', '16', '1');
INSERT INTO `ny_menu` VALUES ('52', 'tree', '流失统计', 'admin.php?ctrl=loss', '39', '23', '0');
INSERT INTO `ny_menu` VALUES ('60', 'tree', '消息推送', 'admin.php?ctrl=push', '34', '36', '1');
INSERT INTO `ny_menu` VALUES ('2267', 'tree', '活动参数', 'admin.php?ctrl=sql&act=activity', '2252', '0', '2');
INSERT INTO `ny_menu` VALUES ('55', 'tree', 'vip统计', 'admin.php?ctrl=vip', '39', '25', '0');
INSERT INTO `ny_menu` VALUES ('56', 'tree', '财富统计', 'admin.php?ctrl=total', '39', '26', '0');
INSERT INTO `ny_menu` VALUES ('57', 'tree', '热更新', 'admin.php?ctrl=setting&act=hupdate', '1', '58', '1');
INSERT INTO `ny_menu` VALUES ('2236', 'tree', '兑换码', 'admin.php?ctrl=gift', '34', '32', '1');
INSERT INTO `ny_menu` VALUES ('2237', 'tree', '实时数据对比', 'admin.php?ctrl=data&act=real', '39', '18', '0');
INSERT INTO `ny_menu` VALUES ('2241', 'tree', '实时排行榜', 'admin.php?ctrl=rank&act=record', '34', '30', '1');
INSERT INTO `ny_menu` VALUES ('2240', 'tree', '广播设置', 'admin.php?ctrl=broadcast&act=index', '34', '31', '1');
INSERT INTO `ny_menu` VALUES ('2242', 'tree', '区服充值汇总', 'admin.php?ctrl=chargetotal&act=record', '23', '1', '2');
INSERT INTO `ny_menu` VALUES ('2243', 'tree', '玩家管理', '', '0', '27', '1');
INSERT INTO `ny_menu` VALUES ('2244', 'tree', '帮派信息', 'admin.php?ctrl=faction&act=index', '2243', '28', '1');
INSERT INTO `ny_menu` VALUES ('2268', 'tree', '活动', 'admin.php?ctrl=activity&act=index', '34', '99', '1');
INSERT INTO `ny_menu` VALUES ('2245', 'tree', '白名单', 'admin.php?ctrl=white', '34', '0', '0');
INSERT INTO `ny_menu` VALUES ('2246', 'tree', '封禁管理', 'admin.php?ctrl=ban', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2247', 'tree', '消耗饼图', 'admin.php?ctrl=item&act=pie_index', '39', '0', '0');
INSERT INTO `ny_menu` VALUES ('2248', 'tree', '后台充值测试列表', 'admin.php?ctrl=charge&act=manual_charge', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2249', 'tree', '玩家反馈', 'admin.php?&ctrl=feedback&act=index', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2250', 'tree', '活动更新', 'admin.php?ctrl=activity&act=update_activity', '34', '10', '1');
INSERT INTO `ny_menu` VALUES ('2251', 'tree', '活动临时列表', 'admin.php?ctrl=activityTmp&act=index', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2252', 'tree', '技术模块', '', '0', '0', '1');
INSERT INTO `ny_menu` VALUES ('2253', 'tree', 'sql查询', 'admin.php?ctrl=sql&act=getData', '2252', '0', '2');
INSERT INTO `ny_menu` VALUES ('2254', 'tree', '每日充值', 'admin.php?ctrl=stat&act=daily', '23', '0', '1');
INSERT INTO `ny_menu` VALUES ('2255', 'tree', '游戏公告', 'admin.php?ctrl=notice&act=index', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2256', 'tree', '交易组', '', '0', '0', '1');
INSERT INTO `ny_menu` VALUES ('2257', 'tree', '交易组', 'admin.php?ctrl=dealgroup&act=index', '2256', '0', '2');
INSERT INTO `ny_menu` VALUES ('2260', 'tree', '交易模版', 'admin.php?ctrl=dealgroup&act=tpl_index', '2256', '0', '2');
INSERT INTO `ny_menu` VALUES ('2271', 'tree', '活动执行结果', 'admin.php?ctrl=activity&act=activity_result', '34', '100', '1');
INSERT INTO `ny_menu` VALUES ('2262', 'tree', '数据汇总', 'admin.php?ctrl=stat&act=summary_index', '39', '0', '0');
INSERT INTO `ny_menu` VALUES ('2264', 'tree', '白名单', 'admin.php?ctrl=whiteip&act=index', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2266', 'tree', '活动参与率', 'admin.php?ctrl=stat&act=activity', '39', '0', '0');
INSERT INTO `ny_menu` VALUES ('2270', 'tree', '交易管理', 'admin.php?ctrl=dealgroup&act=deal_index', '2256', '0', '2');
INSERT INTO `ny_menu` VALUES ('2272', 'tree', '合服计划', 'admin.php?ctrl=merge&act=index', '8', '0', '1');
INSERT INTO `ny_menu` VALUES ('2274', 'tree', '开服计划', 'admin.php?ctrl=plan&act=index', '8', '0', '1');
INSERT INTO `ny_menu` VALUES ('2275', 'tree', '渠道版本号', 'admin.php?&ctrl=review&act=index', '8', '0', '1');
INSERT INTO `ny_menu` VALUES ('2276', 'tree', '功能开关控制', 'admin.php?&ctrl=action&act=index', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2277', 'tree', '数据导出', 'admin.php?&ctrl=ce&act=tab', '2252', '0', '2');
INSERT INTO `ny_menu` VALUES ('2278', 'tree', '维护计划', 'admin.php?ctrl=maintenance&act=index', '8', '0', '1');
INSERT INTO `ny_menu` VALUES ('2279', 'tree', 'VIP公告', 'admin.php?&ctrl=vipnotice&act=index', '34', '0', '1');
INSERT INTO `ny_menu` VALUES ('2280', 'tree', '聊天监控', 'admin.php?ctrl=talk&act=index', '2243', '0', '1');
INSERT INTO `ny_menu` VALUES ('2281', 'tree', '开启关闭拍卖行', 'admin.php?ctrl=dealgroup&act=trade', '2256', '0', '2');
INSERT INTO `ny_menu` VALUES ('2282', 'tree', '一键补单', 'admin.php?ctrl=supplement&act=index', '34', '0', '2');
INSERT INTO `ny_menu` VALUES ('2283', 'tree', 'udid到期时间', 'admin.php?ctrl=tooldeal&act=index', '1', '100', '1');
INSERT INTO `ny_menu` VALUES ('2284', 'tree', '充值元宝数分布', 'admin.php?ctrl=charge&act=chargeDistribute', '23', '100', '1');
INSERT INTO `ny_menu` VALUES ('2285', 'tree', '玩家数据', '', '0', '10', '1');
INSERT INTO `ny_menu` VALUES ('2286', 'tree', '新增玩家', 'admin.php?ctrl=accountctrl&act=newaccount', '2285', '1', '0');
INSERT INTO `ny_menu` VALUES ('2287', 'tree', '付费率', 'admin.php?ctrl=accountctrl&act=payRate', '23', '50', '1');
INSERT INTO `ny_menu` VALUES ('2288', 'tree', '玩家留存', 'admin.php?ctrl=accountctrl&act=remain', '2285', '2', '2');
INSERT INTO `ny_menu` VALUES ('2289', 'tree', '设备统计', 'admin.php?ctrl=accountctrl&act=device', '2285', '3', '2');
INSERT INTO `ny_menu` VALUES ('2290', 'tree', '活跃玩家', 'admin.php?ctrl=accountctrl&act=hyRoles', '2285', '4', '0');
INSERT INTO `ny_menu` VALUES ('2291', 'tree', '物品配置', 'admin.php?&ctrl=itemconfig&act=index', '1', '0', '1');
INSERT INTO `ny_menu` VALUES ('2292', 'tree', '补单', 'admin.php?ctrl=supplement&act=budan', '34', '200', '1');
INSERT INTO `ny_menu` VALUES ('2293', 'tree', '流水查询', 'admin.php?ctrl=stream&act=index', '2243', '300', '1');
INSERT INTO `ny_menu` VALUES ('2294', 'tree', '操作日志', 'admin.php?ctrl=centerlog&act=index', '1', '400', '1');
INSERT INTO `ny_menu` VALUES ('2295', 'tree', '新增玩家', 'admin.php?ctrl=accountctrl&act=newaccount', '39', '200', '0');
INSERT INTO `ny_menu` VALUES ('2296', 'tree', '设备统计', 'admin.php?ctrl=accountctrl&act=device', '39', '210', '1');
INSERT INTO `ny_menu` VALUES ('2297', 'tree', '活跃玩家', 'admin.php?ctrl=accountctrl&act=hyRoles', '39', '220', '0');
INSERT INTO `ny_menu` VALUES ('2298', 'tree', 'LTV统计', 'admin.php?ctrl=stat&act=ltv_index', '23', '600', '1');
INSERT INTO `ny_menu` VALUES ('2299', 'tree', '后续充值', 'admin.php?ctrl=ltv&act=follow', '23', '610', '1');
INSERT INTO `ny_menu` VALUES ('2300', 'tree', '留存率', 'admin.php?ctrl=ltv&act=index', '39', '800', '2');
INSERT INTO `ny_menu` VALUES ('2301', 'tree', '道具监控', 'admin.php?ctrl=item_check&act=index', '34', '990', '1');
INSERT INTO `ny_menu` VALUES ('2302', 'tree', '包数据汇总', 'admin.php?ctrl=accountctrl&act=summary_index', '39', '1000', '0');
INSERT INTO `ny_menu` VALUES ('2303', 'tree', '真实充值', 'admin.php?ctrl=chargetrue&act=index', '1', '1', '1');
INSERT INTO `ny_menu` VALUES ('2304', 'tree', '每小时数据统计', 'admin.php?ctrl=perHour&act=index', '39', '1', '1');
INSERT INTO `ny_menu` VALUES ('2335', 'tree', '多日留存', 'admin.php?&ctrl=ltv&act=newacc', '2334', '0', '1');
INSERT INTO `ny_menu` VALUES ('2306', 'tree', '排行榜', 'admin.php?ctrl=ranklist&act=index', '39', '0', '1');
INSERT INTO `ny_menu` VALUES ('2308', 'tree', '活跃统计', 'admin.php?ctrl=active&act=index', '39', '0', '1');
INSERT INTO `ny_menu` VALUES ('2309', 'tree', '在线分布', 'admin.php?ctrl=onlinedistribution&act=index', '39', '0', '1');
INSERT INTO `ny_menu` VALUES ('2310', 'tree', '在线统计', 'admin.php?ctrl=online&act=statistics', '39', '0', '1');
INSERT INTO `ny_menu` VALUES ('2311', 'tree', '实时数据', '', '0', '10', '1');
INSERT INTO `ny_menu` VALUES ('2312', 'tree', '即时数据统计', 'admin.php?ctrl=Instantdata&act=real', '2311', '0', '1');
INSERT INTO `ny_menu` VALUES ('2313', 'tree', '系统统计', '', '0', '10', '1');
INSERT INTO `ny_menu` VALUES ('2314', 'tree', '单人BOSS', 'admin.php?ctrl=singleboss&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2315', 'tree', '多人BOSS', 'admin.php?ctrl=manyboss&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2316', 'tree', '天关系统', 'admin.php?ctrl=zones&act=zones1', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2317', 'tree', '大荒古塔', 'admin.php?ctrl=zones&act=zones2', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2319', 'tree', '用户信息查询', 'admin.php?ctrl=userinfo&act=index', '2311', '0', '1');
INSERT INTO `ny_menu` VALUES ('2320', 'tree', '三界BOSS', 'admin.php?ctrl=threerealmsboss&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2321', 'tree', '每日试炼', 'admin.php?ctrl=dailytrial&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2322', 'tree', 'BOSS之家', 'admin.php?ctrl=bosshome&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2333', 'tree', '清除数据库(测试用', 'admin.php?ctrl=setting&act=cleansql', '1', '0', '1');
INSERT INTO `ny_menu` VALUES ('2324', 'tree', 'versionControl', 'admin.php?ctrl=version&act=index', '1', '0', '1');
INSERT INTO `ny_menu` VALUES ('2325', 'tree', '付费消费统计', '', '0', '10', '1');
INSERT INTO `ny_menu` VALUES ('2326', 'tree', 'VIP分布', 'admin.php?ctrl=vip&act=record', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2327', 'tree', '每日充值', 'admin.php?ctrl=dailyrecharge&act=index', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2328', 'tree', 'LTV', 'admin.php?ctrl=ltv&act=LTV', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2329', 'tree', '商城消费分布', 'admin.php?ctrl=mall&act=index', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2330', 'tree', '每日消费', 'admin.php?ctrl=dailyconsume&act=record', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2331', 'tree', '幽冥鬼境', 'admin.php?ctrl=teamcopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2332', 'tree', '远古符阵', 'admin.php?ctrl=runecopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2334', 'tree', '留存统计', '', '0', '0', '1');
INSERT INTO `ny_menu` VALUES ('2336', 'tree', '充值留存', 'admin.php?&ctrl=ltv&act=remain', '2334', '0', '1');
INSERT INTO `ny_menu` VALUES ('2337', 'tree', '等级留存', 'admin.php?&ctrl=ltv&act=level', '2334', '0', '1');
INSERT INTO `ny_menu` VALUES ('2338', 'tree', '新手任务留存', 'admin.php?&ctrl=novicetask&act=index', '2334', '0', '1');
INSERT INTO `ny_menu` VALUES ('2339', 'tree', '天降财宝', 'admin.php?ctrl=richescopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2340', 'tree', '昆仑瑶池', 'admin.php?ctrl=swimmingcopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2341', 'tree', '云梦秘境', 'admin.php?ctrl=cloudlandcopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2342', 'tree', '护送仙女', 'admin.php?ctrl=fairyinfo&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2343', 'tree', '九天之巅', 'admin.php?ctrl=ninecopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2344', 'tree', '天梯斗法', 'admin.php?ctrl=tianticopy&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2345', 'tree', '奇遇系统', 'admin.php?ctrl=adventureinfo&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2346', 'tree', '仙府洞天', 'admin.php?ctrl=xianfuinfo&act=index', '2313', '0', '1');
INSERT INTO `ny_menu` VALUES ('2347', 'tree', '月收入统计', 'admin.php?ctrl=summary&act=monthly_income', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2348', 'tree', '数据总览', 'admin.php?ctrl=summary&act=summary', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2349', 'tree', '单服汇总', 'admin.php?ctrl=summary&act=single_summary', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2350', 'tree', '充值档位统计', 'admin.php?ctrl=charge&act=charge_index', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2351', 'tree', '新手引导留存', 'admin.php?&ctrl=guide&act=index', '2334', '0', '1');
INSERT INTO `ny_menu` VALUES ('2352', 'tree', '赠送元宝', 'admin.php?ctrl=giftOrder&act=index', '2243', '0', '1');
INSERT INTO `ny_menu` VALUES ('2353', 'tree', '消费项目统计', 'admin.php?ctrl=consumer&act=index', '2325', '0', '1');
INSERT INTO `ny_menu` VALUES ('2354', 'tree', '自动开服计划', 'admin.php?ctrl=serverAutoPlan&act=index', '8', '0', '1');

-- ----------------------------
-- Table structure for `ny_merge`
-- ----------------------------
DROP TABLE IF EXISTS `ny_merge`;
CREATE TABLE `ny_merge` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent` varchar(30) NOT NULL DEFAULT '' COMMENT '母服',
  `children` varchar(2000) NOT NULL DEFAULT '' COMMENT '子服',
  `merge_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '和服时间',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开服时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:未合服，1合服成功',
  `notify` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:黙认，1:合服前邮件，2：合服时物品下架，3:合服公告,4自动开服 ，5开启拍卖行',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_merge
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_multiday_retention`
-- ----------------------------
DROP TABLE IF EXISTS `ny_multiday_retention`;
CREATE TABLE `ny_multiday_retention` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `new_acc_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新用户数',
  `one_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第一天登录数',
  `two_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第二天登录数',
  `three_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第三天登录数',
  `four_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第四天登录数',
  `five_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第五天登录数',
  `six_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第六天登录数',
  `seven_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第七天登录数',
  `eight_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第八天登录数',
  `nine_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第九天登录数',
  `ten_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第十天登录数',
  `fourteen_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第十四天登录数',
  `twenty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第二十天登录数',
  `thirty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第三十天登录数',
  `forty_five_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第四十五天登录数',
  `sixty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第六十天登录数',
  `ninety_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第九十天登录数',
  `one_hundred_twenty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第一百二十天登录数',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多日留存';

-- ----------------------------
-- Records of ny_multiday_retention
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_new_server_account`
-- ----------------------------
DROP TABLE IF EXISTS `ny_new_server_account`;
CREATE TABLE `ny_new_server_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(100) NOT NULL DEFAULT '' COMMENT '渠道标识',
  `package` int(11) NOT NULL COMMENT '包号',
  `server` varchar(100) NOT NULL COMMENT '服务器标识',
  `account` varchar(100) NOT NULL COMMENT '账号',
  `ip` varchar(100) NOT NULL DEFAULT '0' COMMENT 'ip',
  `is_create` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否创角',
  `is_loadover` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否加载完毕 ',
  `role_id` varchar(100) DEFAULT NULL,
  `is_roll` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否滚服',
  `create_time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`) USING BTREE,
  KEY `package` (`package`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `account` (`account`) USING BTREE,
  KEY `is_create` (`is_create`) USING BTREE,
  KEY `is_roll` (`is_roll`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_new_server_account
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_nine_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_nine_copy`;
CREATE TABLE `ny_nine_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通九天之巅的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `one_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第一层的人数',
  `two_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第二层的人数',
  `three_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第三层的人数',
  `four_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第四层的人数',
  `five_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第五层的人数',
  `six_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第六层的人数',
  `seven_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第七层的人数',
  `eight_layer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通过第八层的人数',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='九天之巅统计表';

-- ----------------------------
-- Records of ny_nine_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_node`
-- ----------------------------
DROP TABLE IF EXISTS `ny_node`;
CREATE TABLE `ny_node` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL,
  `package` varchar(20) DEFAULT NULL,
  `server` varchar(20) DEFAULT '0' COMMENT '服务器',
  `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip',
  `account` varchar(50) DEFAULT '0' COMMENT '账号',
  `role_id` double(50,0) DEFAULT '0',
  `status` tinyint(2) DEFAULT NULL COMMENT '节点',
  `first` tinyint(2) NOT NULL DEFAULT '0' COMMENT '为1则为新增用户',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`),
  KEY `package` (`package`),
  KEY `server` (`server`),
  KEY `first` (`first`) USING BTREE,
  KEY `account` (`account`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_node
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_node_1905`
-- ----------------------------
DROP TABLE IF EXISTS `ny_node_1905`;
CREATE TABLE `ny_node_1905` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL,
  `package` varchar(20) DEFAULT NULL,
  `server` varchar(20) DEFAULT '0' COMMENT '服务器',
  `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip',
  `account` varchar(50) DEFAULT '0' COMMENT '账号',
  `role_id` double(50,0) DEFAULT '0',
  `status` tinyint(2) DEFAULT NULL COMMENT '节点',
  `first` tinyint(2) NOT NULL DEFAULT '0' COMMENT '为1则为新增用户',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`),
  KEY `package` (`package`),
  KEY `server` (`server`),
  KEY `create_time` (`create_time`),
  KEY `first` (`first`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_node_1905
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_node_daily`
-- ----------------------------
DROP TABLE IF EXISTS `ny_node_daily`;
CREATE TABLE `ny_node_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `device_num` int(11) DEFAULT '0' COMMENT '新增设备数',
  `one` varchar(20) DEFAULT NULL COMMENT '节点1：成功进入注册页面的人数',
  `two` varchar(20) DEFAULT NULL COMMENT '节点2：注册完毕进入游戏创角页面的人数',
  `three` varchar(20) DEFAULT NULL COMMENT '节点3：完成创建角色的人数',
  `four` varchar(20) DEFAULT NULL COMMENT '节点4：进入加载页面的人数',
  `five` varchar(20) DEFAULT NULL COMMENT '节点5：加载完成进入装逼副本的人数',
  `six` varchar(20) DEFAULT NULL COMMENT '节点6：清完第一波小怪的人数',
  `seven` varchar(20) DEFAULT NULL COMMENT '节点7：打完敌对NPC的人数',
  `eight` varchar(20) DEFAULT NULL COMMENT '节点8：打完装逼副本第1波BOSS的人数',
  `nine` varchar(20) DEFAULT NULL COMMENT '节点9：打完装逼副本第2波小怪的人数',
  `ten` varchar(20) DEFAULT NULL COMMENT '节点10：打完装逼副本BOSS进入故事包装页的人数',
  `eleven` varchar(20) DEFAULT NULL COMMENT '节点11：故事页播放完进入加载界面的人数',
  `twleve` varchar(20) DEFAULT NULL COMMENT '节点12：进入新手村的人数',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_node_daily
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_node_daily_hour`
-- ----------------------------
DROP TABLE IF EXISTS `ny_node_daily_hour`;
CREATE TABLE `ny_node_daily_hour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(20) NOT NULL COMMENT '服务器标识',
  `create_time` int(11) NOT NULL,
  `new_active` int(11) NOT NULL DEFAULT '0' COMMENT '单位时间内新增用户数(指连接服务器成功',
  `one` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点1：开始加载类库',
  `two` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点2：所有类库加载完成',
  `two_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第二步加载总时长',
  `three` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点3：WEBSOCKET是否可用',
  `three_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第三步加载总时长',
  `four` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点4：开始加载配置文件',
  `four_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第四步加载总时长',
  `five` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点5：开始连接服务器',
  `five_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第五步加载总时长',
  `six` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点6：连接服务器成功',
  `six_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第六步加载总时长',
  `seven` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点7：登录成功',
  `seven_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第七步加载总时长',
  `eight` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点8：创角跳转',
  `eight_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第八步加载总时长',
  `nine` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点9：登录失败',
  `nine_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第九步加载总时长',
  `ten` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点10：开始创角',
  `ten_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第十步加载总时长',
  `eleven` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点11：创角成功',
  `eleven_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第十一步加载总时长',
  `twleve` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点12：开始进入场景（首次进入记录）',
  `twleve_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第十二步加载总时长',
  `thirteen` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点13：开始加载通用资源',
  `thirteen_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第十三步加载总时长',
  `fourteen` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点14: 通用资源加载完成',
  `fourteen_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第十四步加载总时长',
  `fifteen` double(20,0) NOT NULL DEFAULT '0' COMMENT '节点15: 进入场景（首次进入记录）',
  `fifteen_load_time` int(11) NOT NULL DEFAULT '0' COMMENT '第十五步加载总时长',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_node_daily_hour
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_notice`
-- ----------------------------
DROP TABLE IF EXISTS `ny_notice`;
CREATE TABLE `ny_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups` varchar(2000) NOT NULL DEFAULT '' COMMENT '渠道组',
  `contents` varchar(2000) NOT NULL DEFAULT '' COMMENT '公告内容',
  `servers` varchar(2000) DEFAULT '' COMMENT '服务器',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '公告类型 0：新服公告 1：通用公告 2：活动公告',
  `notice_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '发布类型 0 永久 1 一次',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0 未发布 1已发布',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `notice_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间',
  `admin` varchar(255) NOT NULL DEFAULT '' COMMENT '发布者',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `sort` tinyint(3) NOT NULL DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_notice
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_novice_task`
-- ----------------------------
DROP TABLE IF EXISTS `ny_novice_task`;
CREATE TABLE `ny_novice_task` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL COMMENT '该日新增且连接上游戏服务器的用户数',
  `one_task_get_num` int(11) NOT NULL DEFAULT '0' COMMENT '第一步任务领取人数',
  `task_id` varchar(32) NOT NULL COMMENT '任务id',
  `task_name` varchar(100) NOT NULL COMMENT '任务名',
  `get_num` int(11) NOT NULL DEFAULT '0' COMMENT '领取任务人数',
  `done_num` int(11) NOT NULL DEFAULT '0' COMMENT '完成任务人数',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='新手任务央服统计表';

-- ----------------------------
-- Records of ny_novice_task
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_online_distribution`
-- ----------------------------
DROP TABLE IF EXISTS `ny_online_distribution`;
CREATE TABLE `ny_online_distribution` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '活跃人数',
  `(0,1]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线0-1分钟的玩家数',
  `(1,5]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线1-5分钟的玩家数',
  `(5,10]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线5-10分钟的玩家数',
  `(10,20]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线10-20分钟的玩家数',
  `(20,30]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线20-30分钟的玩家数',
  `(30,60]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线30-60分钟的玩家数',
  `(60,90]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线60-90分钟的玩家数',
  `(90,120]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线90-120分钟的玩家数',
  `(120,150]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线120-150分钟的玩家数',
  `(150,180]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线150-180分钟的玩家数',
  `(180,240]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线180-240分钟的玩家数',
  `(240,300]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线240-300分钟的玩家数',
  `(300,1440]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '在线300-1440分钟的玩家数',
  `(1440,-]` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '1440以上',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线分布统计';

-- ----------------------------
-- Records of ny_online_distribution
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_order`
-- ----------------------------
DROP TABLE IF EXISTS `ny_order`;
CREATE TABLE `ny_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `server` varchar(20) NOT NULL DEFAULT '' COMMENT '服务器id',
  `channel` varchar(20) NOT NULL DEFAULT '' COMMENT '渠道id',
  `package` varchar(20) NOT NULL DEFAULT '' COMMENT '包id',
  `order_num` varchar(50) NOT NULL DEFAULT '' COMMENT '订单号',
  `corder_num` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道订单号',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `item_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品编号',
  `account` varchar(50) NOT NULL DEFAULT '' COMMENT '账号',
  `role_id` double(50,0) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `role_name` varchar(50) NOT NULL DEFAULT '' COMMENT '充值时角色名',
  `role_level` mediumint(5) unsigned NOT NULL DEFAULT '0' COMMENT '充值时角色等级',
  `role_career` mediumint(5) unsigned NOT NULL DEFAULT '0' COMMENT '充值时职业',
  `gold` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '获得的元宝数量',
  `reward_item_id` int(11) NOT NULL DEFAULT '0' COMMENT '获取的物品id',
  `reward_item_count` int(11) NOT NULL DEFAULT '0' COMMENT '获取的物品数量',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态，1支付成功，2支付失败，3错误订单',
  `first` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否首冲，1是首冲，0不是首冲（弃用）',
  `params` mediumtext COMMENT '参数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  `notify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '渠道订单到账时间',
  `idfa` varchar(255) NOT NULL DEFAULT '' COMMENT '设备码',
  `loop_num` int(11) DEFAULT NULL,
  `is_test` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1为测试0不是',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_num` (`order_num`) USING BTREE,
  KEY `role_id` (`role_id`,`create_time`) USING BTREE,
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of ny_order
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_order_return`
-- ----------------------------
DROP TABLE IF EXISTS `ny_order_return`;
CREATE TABLE `ny_order_return` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `money` decimal(32,0) DEFAULT NULL,
  `server` varchar(20) NOT NULL COMMENT '服务器id',
  `type` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account` (`account`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ny_order_return
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_order_tmp`
-- ----------------------------
DROP TABLE IF EXISTS `ny_order_tmp`;
CREATE TABLE `ny_order_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(100) CHARACTER SET latin1 DEFAULT NULL COMMENT '用户账号',
  `role_id` bigint(20) DEFAULT NULL,
  `role_name` varchar(50) DEFAULT NULL,
  `role_level` int(11) unsigned DEFAULT '0' COMMENT '充值时角色等级',
  `channel_id` int(11) DEFAULT '0',
  `server_id` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `fee` float(10,2) DEFAULT '0.00',
  `create_time` int(11) DEFAULT NULL,
  `order_num` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `order_num` (`order_num`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_order_tmp
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_package`
-- ----------------------------
DROP TABLE IF EXISTS `ny_package`;
CREATE TABLE `ny_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '渠道组ID',
  `channel_id` varchar(50) NOT NULL DEFAULT '' COMMENT '属于哪个渠道',
  `package_id` varchar(50) DEFAULT '' COMMENT '包号',
  `name` varchar(50) DEFAULT '' COMMENT '包名称',
  `game_name` varchar(50) DEFAULT '' COMMENT '游戏名',
  `create_time` int(11) DEFAULT NULL,
  `review_num` varchar(25) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `cdn_url` varchar(255) NOT NULL COMMENT 'cdn资源加载地址',
  `platform` tinyint(2) DEFAULT '0',
  `rv_param` varchar(100) DEFAULT NULL COMMENT '提审参数设置JSON',
  `test_server_num` varchar(20) DEFAULT '0',
  `type` tinyint(8) DEFAULT '0' COMMENT '提审类型0不是提审1原来(默认)2新的',
  PRIMARY KEY (`id`),
  UNIQUE KEY `package_id` (`package_id`) USING BTREE,
  KEY `channel_id` (`channel_id`),
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_package
-- ----------------------------
INSERT INTO `ny_package` VALUES ('1', '1', 'local', '8070', '8070', '九州仙剑传', '1540882756', '', '1234', '1', '1', '{\"rolebg\":\"5\",\"sceneid\":\"108\"}', '', '0');

-- ----------------------------
-- Table structure for `ny_payroles`
-- ----------------------------
DROP TABLE IF EXISTS `ny_payroles`;
CREATE TABLE `ny_payroles` (
  `server` varchar(100) NOT NULL,
  `time` date NOT NULL,
  `payRoles_1` int(11) DEFAULT '0' COMMENT '日付费数',
  `loginRoles_1` int(11) DEFAULT '0' COMMENT '日活跃数',
  `payRoles_7` int(11) DEFAULT '0' COMMENT '周付费数',
  `loginRoles_7` int(11) DEFAULT '0' COMMENT '周活跃数',
  `payRoles_30` int(11) DEFAULT '0' COMMENT '月付费数',
  `loginRoles_30` int(11) DEFAULT '0' COMMENT '月活跃数',
  PRIMARY KEY (`server`,`time`),
  UNIQUE KEY `server_time` (`server`,`time`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_payroles
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_perhour`
-- ----------------------------
DROP TABLE IF EXISTS `ny_perhour`;
CREATE TABLE `ny_perhour` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `channel` varchar(20) DEFAULT NULL COMMENT '渠道/平台',
  `package` varchar(20) DEFAULT NULL COMMENT '包',
  `server` varchar(20) DEFAULT NULL COMMENT '服务器',
  `new_jumps` int(11) NOT NULL DEFAULT '0' COMMENT '平台新增跳转数',
  `login_sus` int(11) NOT NULL DEFAULT '0' COMMENT '连接登录成功数',
  `create_role` int(11) NOT NULL DEFAULT '0' COMMENT '创角数',
  `time` int(11) NOT NULL COMMENT '时间戳',
  `year` smallint(4) DEFAULT NULL COMMENT '年',
  `month` smallint(2) DEFAULT NULL COMMENT '月',
  `day` smallint(2) DEFAULT NULL COMMENT '日',
  `hour` smallint(2) DEFAULT NULL COMMENT '时',
  `update_time` int(11) NOT NULL COMMENT '上次修改时间',
  PRIMARY KEY (`id`),
  KEY `time` (`year`,`month`,`day`,`hour`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='每小时统计数据表';

-- ----------------------------
-- Records of ny_perhour
-- ----------------------------
INSERT INTO `ny_perhour` VALUES ('2', '1', '1', '1', '3', '3', '3', '1540969595', '2018', '10', '31', '15', '1540969596');

-- ----------------------------
-- Table structure for `ny_permit`
-- ----------------------------
DROP TABLE IF EXISTS `ny_permit`;
CREATE TABLE `ny_permit` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限表自增id',
  `type` char(10) NOT NULL DEFAULT '' COMMENT '类型：央服center、单服single',
  `group` varchar(100) NOT NULL DEFAULT '' COMMENT '权限分组名称',
  `group_id` int(11) DEFAULT NULL COMMENT '分组id; 对应ny_menu表父级分类',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '权限名称',
  `modules` text COMMENT '模块',
  `created` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of ny_permit
-- ----------------------------
INSERT INTO `ny_permit` VALUES ('226', 'center', '付费消费统计', '2325', 'VIP分布', 'a:1:{i:0;s:5:\"vip_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('224', 'center', '系统统计', '2313', '幽冥鬼境', 'a:1:{i:0;s:10:\"teamcopy_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('220', 'center', '系统统计', '2313', '大荒古塔', 'a:1:{i:0;s:7:\"zones_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('218', 'center', '系统统计', '2313', '多人BOSS', 'a:1:{i:0;s:10:\"manyboss_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('249', 'center', '游戏管理', '34', '广播设置', 'a:1:{i:0;s:11:\"broadcast_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('235', 'center', '数据统计', '39', '每小时数据统计', 'a:1:{i:0;s:9:\"perHour_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('294', 'center', '游戏管理', '34', '白名单', 'a:1:{i:0;s:9:\"whiteip_*\";}', '1557239815');
INSERT INTO `ny_permit` VALUES ('246', 'center', '游戏管理', '34', 'VIP公告', 'a:1:{i:0;s:11:\"vipnotice_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('208', 'center', '游戏管理', '34', '活动临时列表', 'a:1:{i:0;s:13:\"activityTmp_*\";}', '1498037970');
INSERT INTO `ny_permit` VALUES ('209', 'center', '游戏管理', '34', '活动列表', 'a:15:{i:0;s:14:\"activity_index\";i:1;s:17:\"activity_activity\";i:2;s:21:\"activity_activitylist\";i:3;s:26:\"activity_activitylist_data\";i:4;s:15:\"activity_config\";i:5;s:13:\"activity_edit\";i:6;s:13:\"activity_save\";i:7;s:14:\"activity_check\";i:8;s:16:\"activity_offSale\";i:9;s:15:\"activity_remark\";i:10;s:14:\"activity_model\";i:11;s:19:\"activity_model_data\";i:12;s:17:\"activity_setmodel\";i:13;s:17:\"activity_delmodel\";i:14;s:12:\"activity_del\";}', '1502963563');
INSERT INTO `ny_permit` VALUES ('210', 'center', '游戏管理', '34', '活动审核', 'a:5:{i:0;s:14:\"activity_audit\";i:1;s:19:\"activity_audit_data\";i:2;s:14:\"activity_agree\";i:3;s:13:\"activity_push\";i:4;s:17:\"activity_disagree\";}', '1502963575');
INSERT INTO `ny_permit` VALUES ('211', 'center', '游戏管理', '34', '功能开关控制', 'a:1:{i:0;s:8:\"action_*\";}', '1506310691');
INSERT INTO `ny_permit` VALUES ('244', 'center', '游戏管理', '34', '游戏公告', 'a:1:{i:0;s:8:\"notice_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('242', 'center', '游戏管理', '34', '后台充值测试列表', 'a:1:{i:0;s:8:\"charge_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('243', 'center', '游戏管理', '34', '玩家反馈', 'a:1:{i:0;s:10:\"feedback_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('240', 'center', '玩家管理', '2243', '流水查询', 'a:1:{i:0;s:8:\"stream_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('239', 'center', '玩家管理', '2243', '帮派信息', 'a:1:{i:0;s:9:\"faction_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('274', 'center', '系统统计', '2313', '昆仑瑶池', 'a:1:{i:0;s:14:\"swimmingcopy_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('275', 'center', '系统统计', '2313', '云梦秘境', 'a:1:{i:0;s:15:\"cloudlandcopy_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('276', 'center', '系统统计', '2313', '护送仙女', 'a:1:{i:0;s:11:\"fairyinfo_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('277', 'center', '系统统计', '2313', '九天之巅', 'a:1:{i:0;s:10:\"ninecopy_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('278', 'center', '系统统计', '2313', '天梯斗法', 'a:1:{i:0;s:12:\"tianticopy_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('232', 'center', '数据统计', '39', '活跃统计', 'a:1:{i:0;s:8:\"active_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('233', 'center', '数据统计', '39', '在线分布', 'a:1:{i:0;s:20:\"onlinedistribution_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('273', 'center', '系统统计', '2313', '天降财宝', 'a:1:{i:0;s:12:\"richescopy_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('238', 'center', '玩家管理', '2243', '聊天监控', 'a:1:{i:0;s:6:\"talk_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('194', 'center', '游戏管理', '34', '发送邮件', 'a:1:{i:0;s:6:\"mail_*\";}', '1490200548');
INSERT INTO `ny_permit` VALUES ('195', 'center', '游戏管理', '34', '消息推送', 'a:1:{i:0;s:6:\"push_*\";}', '1490200556');
INSERT INTO `ny_permit` VALUES ('196', 'center', '服务器信息', '8', '服务器管理', 'a:1:{i:0;s:8:\"server_*\";}', '1490200571');
INSERT INTO `ny_permit` VALUES ('197', 'center', '服务器信息', '8', '分区管理', 'a:1:{i:0;s:6:\"zone_*\";}', '1490200584');
INSERT INTO `ny_permit` VALUES ('198', 'center', '服务器信息', '8', '渠道管理', 'a:1:{i:0;s:9:\"channel_*\";}', '1490200591');
INSERT INTO `ny_permit` VALUES ('199', 'center', '服务器信息', '8', '包管理', 'a:1:{i:0;s:9:\"package_*\";}', '1490200597');
INSERT INTO `ny_permit` VALUES ('201', 'center', '系统设置', '1', '用户组', 'a:1:{i:0;s:11:\"usergroup_*\";}', '1490200628');
INSERT INTO `ny_permit` VALUES ('202', 'center', '系统设置', '1', '管理员', 'a:1:{i:0;s:6:\"user_*\";}', '1490200639');
INSERT INTO `ny_permit` VALUES ('203', 'center', '系统设置', '1', '权限管理', 'a:0:{}', '1490200649');
INSERT INTO `ny_permit` VALUES ('204', 'center', '系统设置', '1', '热更新', 'a:1:{i:0;s:17:\"setting_hotupdate\";}', '1490200658');
INSERT INTO `ny_permit` VALUES ('206', 'center', '系统设置', '1', '央服菜单', 'a:1:{i:0;s:6:\"menu_*\";}', '1490200914');
INSERT INTO `ny_permit` VALUES ('227', 'center', '付费消费统计', '2325', '每日充值', 'a:1:{i:0;s:15:\"dailyrecharge_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('225', 'center', '系统统计', '2313', '远古符阵', 'a:1:{i:0;s:10:\"runecopy_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('223', 'center', '系统统计', '2313', 'BOSS之家', 'a:1:{i:0;s:10:\"bosshome_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('222', 'center', '系统统计', '2313', '每日试炼', 'a:1:{i:0;s:12:\"dailytrial_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('221', 'center', '系统统计', '2313', '三界BOSS', 'a:1:{i:0;s:17:\"threerealmsboss_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('219', 'center', '系统统计', '2313', '天关系统', 'a:1:{i:0;s:7:\"zones_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('217', 'center', '系统统计', '2313', '单人BOSS', 'a:1:{i:0;s:12:\"singleboss_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('216', 'center', '实时数据', '2311', '用户信息查询', 'a:1:{i:0;s:10:\"userinfo_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('215', 'center', '实时数据', '2311', '即时数据统计', 'a:1:{i:0;s:13:\"Instantdata_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('248', 'center', '游戏管理', '34', '实时排行榜', 'a:1:{i:0;s:6:\"rank_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('247', 'center', '游戏管理', '34', '活动更新', 'a:1:{i:0;s:10:\"activity_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('234', 'center', '数据统计', '39', '在线统计', 'a:1:{i:0;s:8:\"online_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('231', 'center', '数据统计', '39', '排行榜', 'a:1:{i:0;s:10:\"ranklist_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('241', 'center', '游戏管理', '34', '封禁管理', 'a:1:{i:0;s:5:\"ban_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('236', 'center', '数据统计', '39', '设备统计', 'a:1:{i:0;s:13:\"accountctrl_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('252', 'center', '游戏管理', '34', '活动执行结果', 'a:1:{i:0;s:10:\"activity_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('251', 'center', '游戏管理', '34', '活动', 'a:1:{i:0;s:10:\"activity_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('263', 'center', '系统设置', '1', '修改密码', 'a:1:{i:0;s:6:\"user_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('228', 'center', '付费消费统计', '2325', 'LTV', 'a:1:{i:0;s:5:\"ltv_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('229', 'center', '付费消费统计', '2325', '商城消费分布', 'a:1:{i:0;s:6:\"mall_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('230', 'center', '付费消费统计', '2325', '每日消费', 'a:1:{i:0;s:14:\"dailyconsume_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('250', 'center', '游戏管理', '34', '兑换码', 'a:1:{i:0;s:6:\"gift_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('253', 'center', '游戏管理', '34', '补单', 'a:1:{i:0;s:12:\"supplement_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('254', 'center', '游戏管理', '34', '道具监控', 'a:1:{i:0;s:12:\"item_check_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('255', 'center', '服务器信息', '8', '合服计划', 'a:1:{i:0;s:7:\"merge_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('256', 'center', '服务器信息', '8', '开服计划', 'a:1:{i:0;s:6:\"plan_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('257', 'center', '服务器信息', '8', '渠道版本号', 'a:1:{i:0;s:8:\"review_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('258', 'center', '服务器信息', '8', '维护计划', 'a:1:{i:0;s:13:\"maintenance_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('259', 'center', '系统设置', '1', '物品配置', 'a:1:{i:0;s:12:\"itemconfig_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('260', 'center', '系统设置', '1', '清除数据库(测试用', 'a:1:{i:0;s:9:\"setting_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('261', 'center', '系统设置', '1', 'versionControl', 'a:1:{i:0;s:9:\"version_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('262', 'center', '系统设置', '1', '真实充值', 'a:1:{i:0;s:12:\"chargetrue_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('264', 'center', '系统设置', '1', 'GM指令', 'a:1:{i:0;s:9:\"setting_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('265', 'center', '系统设置', '1', 'udid到期时间', 'a:1:{i:0;s:10:\"tooldeal_*\";}', '1553049498');
INSERT INTO `ny_permit` VALUES ('295', 'center', '系统设置', '1', '操作日志', 'a:1:{i:0;s:11:\"centerlog_*\";}', '1557239815');
INSERT INTO `ny_permit` VALUES ('267', 'center', '留存统计', '2334', '多日留存', 'a:1:{i:0;s:5:\"ltv_*\";}', '1553952881');
INSERT INTO `ny_permit` VALUES ('268', 'center', '留存统计', '2334', '充值留存', 'a:1:{i:0;s:5:\"ltv_*\";}', '1553952881');
INSERT INTO `ny_permit` VALUES ('269', 'center', '留存统计', '2334', '等级留存', 'a:1:{i:0;s:5:\"ltv_*\";}', '1553952881');
INSERT INTO `ny_permit` VALUES ('270', 'center', '留存统计', '2334', '新手任务留存', 'a:1:{i:0;s:27:\"novicetask_* | noviceTask_*\";}', '1553952881');
INSERT INTO `ny_permit` VALUES ('279', 'center', '系统统计', '2313', '奇遇系统', 'a:1:{i:0;s:15:\"adventureinfo_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('280', 'center', '系统统计', '2313', '仙府洞天', 'a:1:{i:0;s:12:\"xianfuinfo_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('281', 'center', '付费消费统计', '2325', '月收入统计', 'a:1:{i:0;s:9:\"summary_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('282', 'center', '付费消费统计', '2325', '数据汇总', 'a:1:{i:0;s:9:\"summary_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('283', 'center', '付费消费统计', '2325', '单服汇总', 'a:1:{i:0;s:9:\"summary_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('284', 'center', '付费消费统计', '2325', '充值档位统计', 'a:1:{i:0;s:8:\"charge_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('285', 'center', '付费消费统计', '2325', '充值记录', 'a:1:{i:0;s:8:\"charge_*\";}', '1557133252');
INSERT INTO `ny_permit` VALUES ('290', 'center', '留存统计', '2334', '新手引导留存', 'a:1:{i:0;s:7:\"guide_*\";}', '1557239726');
INSERT INTO `ny_permit` VALUES ('291', 'center', '玩家管理', '2243', '赠送元宝', 'a:5:{i:0;s:15:\"giftOrder_index\";i:1;s:14:\"giftOrder_info\";i:2;s:13:\"giftOrder_add\";i:3;s:20:\"giftOrder_add_action\";i:4;s:19:\"giftOrder_info_data\";}', '1557239726');
INSERT INTO `ny_permit` VALUES ('296', 'center', '玩家管理', '2243', '赠送元宝审核', 'a:1:{i:0;s:15:\"giftOrder_check\";}', '1557240079');

-- ----------------------------
-- Table structure for `ny_push`
-- ----------------------------
DROP TABLE IF EXISTS `ny_push`;
CREATE TABLE `ny_push` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(50) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '推送类型 0notification 1message',
  `object` text COMMENT '推送对象，json格式',
  `platform` tinyint(4) DEFAULT NULL COMMENT '发送给哪些平台 0全部 1Android 2IOS',
  `cron` tinyint(4) DEFAULT NULL COMMENT '是否定时推送 0否 1是',
  `cron_time` char(5) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `msg_id` int(11) DEFAULT '0' COMMENT 'api返回的消息id',
  `android_received` int(11) DEFAULT NULL COMMENT 'Android平台收到该消息的用户数',
  `ios_received` int(11) DEFAULT NULL COMMENT 'IOS平台收到该消息的用户数',
  `errno` int(11) DEFAULT NULL COMMENT '0表示成功， 非0表示失败',
  `errstr` varchar(100) DEFAULT NULL COMMENT '错误信息',
  `last_modify_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_push
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_qipa_rebate`
-- ----------------------------
DROP TABLE IF EXISTS `ny_qipa_rebate`;
CREATE TABLE `ny_qipa_rebate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rebate_no` int(11) DEFAULT '0',
  `game_order_no` varchar(250) DEFAULT NULL,
  `user_id` varchar(250) DEFAULT NULL,
  `pay_money` decimal(10,2) DEFAULT NULL,
  `gold` int(11) DEFAULT '0',
  `service_id` varchar(255) DEFAULT NULL,
  `role_id` double(50,0) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `state` tinyint(4) DEFAULT '0' COMMENT '1返利成功0未返利',
  PRIMARY KEY (`id`),
  UNIQUE KEY `game_order_no` (`game_order_no`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_qipa_rebate
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_ranklist`
-- ----------------------------
DROP TABLE IF EXISTS `ny_ranklist`;
CREATE TABLE `ny_ranklist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `rank` smallint(3) unsigned NOT NULL DEFAULT '1' COMMENT '排名',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '排行类型:0:等级;1:战斗力;2:付费排行',
  `role_id` double(50,0) unsigned NOT NULL COMMENT '角色id',
  `role_name` varchar(100) NOT NULL,
  `faction_id` double(50,0) unsigned NOT NULL DEFAULT '0' COMMENT '仙盟id',
  `level` int(11) unsigned NOT NULL COMMENT '等级',
  `power` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '战斗力',
  `recharge_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值金额',
  `over_ingots` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '剩余元宝',
  `offline_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '离线时长, 以天为单位',
  `create_time` int(11) NOT NULL COMMENT '数据生成时间',
  PRIMARY KEY (`id`),
  KEY `type` (`type`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='排行榜';

-- ----------------------------
-- Records of ny_ranklist
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_recharge_remain`
-- ----------------------------
DROP TABLE IF EXISTS `ny_recharge_remain`;
CREATE TABLE `ny_recharge_remain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `new_paid_user` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增付费用户',
  `one_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第一天登录数',
  `two_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第二天登录数',
  `three_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第三天登录数',
  `four_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第四天登录数',
  `five_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第五天登录数',
  `six_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第六天登录数',
  `seven_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第七天登录数',
  `eight_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第八天登录数',
  `nine_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第九天登录数',
  `ten_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第十天登录数',
  `fourteen_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第十四天登录数',
  `twenty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第二十天登录数',
  `thirty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第三十天登录数',
  `forty_five_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第四十五天登录数',
  `sixty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第六十天登录数',
  `ninety_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第九十天登录数',
  `one_hundred_twenty_day` varchar(50) NOT NULL DEFAULT '0' COMMENT '第一百二十天登录数',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值留存';

-- ----------------------------
-- Records of ny_recharge_remain
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_register`
-- ----------------------------
DROP TABLE IF EXISTS `ny_register`;
CREATE TABLE `ny_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `channel` varchar(20) DEFAULT NULL,
  `package` varchar(20) DEFAULT NULL COMMENT '包号',
  `isTrueName` tinyint(1) DEFAULT '0' COMMENT '是否实名认证, 1: 是 0:否',
  `truename` varchar(20) DEFAULT NULL COMMENT '身份证姓名',
  `idno` varchar(20) DEFAULT NULL COMMENT '身份证号',
  `isBindPhone` tinyint(1) DEFAULT '0' COMMENT '是否绑定手机: 0 否 1是',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `isFollow` tinyint(1) DEFAULT '0' COMMENT '是否关注公众号',
  `create_time` int(11) DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `channel` (`channel`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_register
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_register_count`
-- ----------------------------
DROP TABLE IF EXISTS `ny_register_count`;
CREATE TABLE `ny_register_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(20) DEFAULT NULL,
  `countlist` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `date` (`date`),
  KEY `channel` (`channel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_register_count
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_riches_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_riches_copy`;
CREATE TABLE `ny_riches_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通天降财宝的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数, 采集数大于1的人数',
  `pick_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '采集总数',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='天降财宝统计表';

-- ----------------------------
-- Records of ny_riches_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_rolecreat`
-- ----------------------------
DROP TABLE IF EXISTS `ny_rolecreat`;
CREATE TABLE `ny_rolecreat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(20) DEFAULT NULL COMMENT '日期',
  `rolecount` int(11) DEFAULT NULL COMMENT '创角数',
  `rolelist` varchar(255) DEFAULT NULL COMMENT '创角数列表',
  `accountcount` int(11) DEFAULT NULL COMMENT '创角设备数',
  `accountlist` varchar(255) DEFAULT NULL COMMENT '创角设备数列表',
  `server` varchar(255) DEFAULT NULL COMMENT '服务器',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `date` (`date`),
  KEY `server` (`server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_rolecreat
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_rune_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_rune_copy`;
CREATE TABLE `ny_rune_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '活跃玩家并且开启远古符阵的玩家数',
  `join_num` int(11) NOT NULL DEFAULT '0' COMMENT '参与人数',
  `challenge_num` int(11) NOT NULL COMMENT '挑战次数(成功才算',
  `pass_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃玩家中通关数≥1的玩家数',
  `reward_num` int(11) NOT NULL COMMENT '领取每日奖励人数',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='远古符阵统计表';

-- ----------------------------
-- Records of ny_rune_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_sensit`
-- ----------------------------
DROP TABLE IF EXISTS `ny_sensit`;
CREATE TABLE `ny_sensit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL COMMENT '说明',
  `name` varchar(250) DEFAULT NULL COMMENT '名称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='敏感规则表';

-- ----------------------------
-- Records of ny_sensit
-- ----------------------------
INSERT INTO `ny_sensit` VALUES ('1', '', '我', '1556274643');

-- ----------------------------
-- Table structure for `ny_server`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server`;
CREATE TABLE `ny_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` varchar(20) NOT NULL COMMENT '服务器标识',
  `channel_num` smallint(5) unsigned DEFAULT '0' COMMENT '渠号',
  `num` int(11) DEFAULT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '服务器名称',
  `status` varchar(2) DEFAULT '4' COMMENT '状态 -1维护中 0未开服 1新服 2流畅 3推荐 4火爆 5正常',
  `zone` int(11) DEFAULT NULL COMMENT '所属分区',
  `type` tinyint(4) DEFAULT NULL COMMENT '1正常服 2合服 3测试服',
  `sort` int(11) DEFAULT '0' COMMENT '第几服 0则无排序',
  `review` int(5) DEFAULT '0' COMMENT '提审状态，1提审。0不提审',
  `open_time` int(11) DEFAULT NULL COMMENT '开服时间',
  `close_time` int(11) DEFAULT NULL COMMENT '关服时间',
  `display` tinyint(4) unsigned DEFAULT '0' COMMENT '是否可见 0内部可见 1外部可见 2外部不可见',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `max_online` int(8) DEFAULT '3000' COMMENT '最大承载量',
  `tips` tinytext,
  `group_id` varchar(20) DEFAULT NULL,
  `merge_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '合服状态',
  `merge_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '合服时间',
  `mom_server` varchar(20) NOT NULL DEFAULT '' COMMENT '母服',
  `display_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server_id` (`server_id`) USING BTREE,
  KEY `open_time` (`open_time`) USING BTREE,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_server
-- ----------------------------
INSERT INTO `ny_server` VALUES ('4', 'sf1', '1', '1', 'Www.BL20166.Com', '2', '1', '1', '1', '0', '1564563600', null, '1', null, '3000', '1', '1', '0', '0', '', '0');

-- ----------------------------
-- Table structure for `ny_server_auto_plan`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server_auto_plan`;
CREATE TABLE `ny_server_auto_plan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL COMMENT '自动开服渠道组',
  `name` varchar(255) NOT NULL COMMENT '开服计划名',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态: 0未配置 1已配置 2已作废',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '判断类型: 1:或 2:且 3:根据单服的自己的开服时间',
  `create_role_num` int(11) DEFAULT NULL COMMENT '创角数',
  `pay_num` int(11) DEFAULT NULL COMMENT '付费人数',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_server_auto_plan
-- ----------------------------
INSERT INTO `ny_server_auto_plan` VALUES ('2', '1', 'cesu ', '2', '2', '12', '5', '1564730181');

-- ----------------------------
-- Table structure for `ny_server_config`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server_config`;
CREATE TABLE `ny_server_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` varchar(20) NOT NULL COMMENT '服务器ID',
  `ip` varchar(50) DEFAULT NULL COMMENT '服务器IP',
  `api_url` varchar(200) DEFAULT NULL COMMENT 'API入口地址',
  `domain` varchar(200) DEFAULT NULL COMMENT '域名',
  `mysql_host` varchar(30) DEFAULT NULL,
  `mysql_port` int(5) DEFAULT NULL,
  `mysql_user` varchar(50) DEFAULT NULL,
  `mysql_passwd` varchar(50) DEFAULT NULL,
  `mysql_db` varchar(50) DEFAULT NULL,
  `mysql_prefix` varchar(10) DEFAULT NULL,
  `mongo_host` varchar(30) DEFAULT NULL,
  `mongo_port` int(5) DEFAULT NULL,
  `mongo_user` varchar(50) DEFAULT NULL,
  `mongo_passwd` varchar(50) DEFAULT NULL,
  `mongo_db` varchar(50) DEFAULT NULL,
  `login_host` varchar(50) DEFAULT NULL,
  `login_port` int(5) DEFAULT NULL,
  `gm_port` int(5) DEFAULT NULL,
  `mdkey` varchar(50) DEFAULT NULL COMMENT '服务器 认证密钥',
  `channel_rv` varchar(20) DEFAULT '0',
  `redis_host` varchar(30) DEFAULT NULL COMMENT 'redis地址',
  `redis_port` int(5) DEFAULT NULL COMMENT 'redis端口',
  `redis_passwd` varchar(50) DEFAULT NULL COMMENT 'redis_密码',
  `redis_db` varchar(50) DEFAULT NULL COMMENT 'redis数据库',
  `redis_prefix` varchar(50) DEFAULT NULL COMMENT 'redisKEY前缀',
  `websocket_host` varchar(30) DEFAULT NULL COMMENT '与服务端通讯地址',
  `websocket_port` int(5) DEFAULT NULL COMMENT '与服务端通讯端口',
  `configs_path` varchar(100) DEFAULT NULL COMMENT '服务端configs路径',
  `bin_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server_id` (`server_id`) USING BTREE,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_server_config
-- ----------------------------
INSERT INTO `ny_server_config` VALUES ('4', 'sf1', '127.0.0.1', '127.0.0.1:83/index.php', '127.0.0.1', '127.0.0.1', '3306', 'root', 'root', 'game_1', 'ny_', null, null, null, null, null, '192.168.200.128', '8010', '8012', '', '0', '127.0.0.1', '6379', '', '1', null, '127.0.0.1', '8012', '/root/server/publish/configs', '/root/server/publish/bin');

-- ----------------------------
-- Table structure for `ny_server_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server_copy`;
CREATE TABLE `ny_server_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` varchar(20) NOT NULL COMMENT '服务器标识',
  `num` int(11) DEFAULT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '服务器名称',
  `status` varchar(2) DEFAULT '4' COMMENT '状态 -1维护中 0未开服 1新服 2流畅 3推荐 4火爆 5正常',
  `zone` int(11) DEFAULT NULL COMMENT '所属分区',
  `type` tinyint(4) DEFAULT NULL COMMENT '1正常服 2合服 3测试服',
  `sort` int(11) DEFAULT '0' COMMENT '第几服 0则无排序',
  `open_time` int(11) DEFAULT NULL COMMENT '开服时间',
  `close_time` int(11) DEFAULT NULL COMMENT '关服时间',
  `display` tinyint(4) unsigned DEFAULT '0' COMMENT '是否可见 0内部可见 1外部可见 2外部不可见',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `max_online` int(8) DEFAULT '3000' COMMENT '最大承载量',
  `tips` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server_id` (`server_id`) USING BTREE,
  KEY `open_time` (`open_time`) USING BTREE,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_server_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_server_maintenance`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server_maintenance`;
CREATE TABLE `ny_server_maintenance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_num` varchar(2000) DEFAULT NULL COMMENT '渠号',
  `server_id` varchar(2000) DEFAULT '',
  `version` varchar(255) DEFAULT '',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0 未配置 1已配置 2 维护中 3 维护完成',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '维护开始时间',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '维护结束时间',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_server_maintenance
-- ----------------------------
INSERT INTO `ny_server_maintenance` VALUES ('1', '3', '', '1', '1', '1563984000', '1564070400', '1564060564');
INSERT INTO `ny_server_maintenance` VALUES ('2', '3', '', '1', '3', '1564132860', '1564133040', '1564132902');

-- ----------------------------
-- Table structure for `ny_server_plan`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server_plan`;
CREATE TABLE `ny_server_plan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `num` smallint(8) unsigned NOT NULL DEFAULT '0' COMMENT '服号',
  `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '渠道组id',
  `server_id` varchar(20) NOT NULL DEFAULT '' COMMENT '服务器名称',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器名称',
  `zone` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属分区',
  `review` int(5) NOT NULL DEFAULT '0' COMMENT '提审标志位，1提审，0不提审',
  `open_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开服时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `is_config` tinyint(8) unsigned NOT NULL DEFAULT '0' COMMENT '是否配置 0 未配置 1 已配置',
  `display_time` int(11) DEFAULT NULL,
  `tips` tinytext COMMENT '未开服提醒',
  PRIMARY KEY (`id`),
  UNIQUE KEY `server_id` (`server_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_server_plan
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_server_remain`
-- ----------------------------
DROP TABLE IF EXISTS `ny_server_remain`;
CREATE TABLE `ny_server_remain` (
  `server` varchar(100) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `data` text,
  `created` date DEFAULT NULL,
  UNIQUE KEY `server_create` (`server`,`created`) USING BTREE,
  KEY `server` (`server`) USING BTREE,
  KEY `created` (`created`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_server_remain
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_single_boss`
-- ----------------------------
DROP TABLE IF EXISTS `ny_single_boss`;
CREATE TABLE `ny_single_boss` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且大于等于50级的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `join_for_level_50_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '50级关卡的挑战人数',
  `join_for_level_50_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '50级关卡挑战次数',
  `join_for_rebirth_1_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '1转关卡的挑战人数',
  `join_for_rebirth_1_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '1转关卡挑战次数',
  `join_for_rebirth_2_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '2转关卡的挑战人数',
  `join_for_rebirth_2_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '2转关卡挑战次数',
  `join_for_rebirth_3_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '3转关卡挑战人数',
  `join_for_rebirth_3_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '3转关卡挑战次数',
  `join_for_rebirth_4_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '4转关卡挑战人数',
  `join_for_rebirth_4_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '4转关卡挑战次数',
  `join_for_rebirth_5_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '5转关卡挑战人数',
  `join_for_rebirth_5_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '5转关卡挑战次数',
  `join_for_rebirth_6_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '6转关卡挑战人数',
  `join_for_rebirth_6_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '6转关卡挑战次数',
  `join_for_rebirth_7_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '7转关卡挑战人数',
  `join_for_rebirth_7_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '7转关卡挑战次数',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_single_boss
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_startup_device`
-- ----------------------------
DROP TABLE IF EXISTS `ny_startup_device`;
CREATE TABLE `ny_startup_device` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) DEFAULT NULL,
  `deviceId` varchar(255) DEFAULT NULL,
  `fr` varchar(100) DEFAULT NULL COMMENT '系统版本',
  `brand` varchar(255) DEFAULT NULL COMMENT '品牌',
  `mac` varchar(255) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `net` varchar(50) DEFAULT NULL COMMENT '网络',
  `imsi` varchar(255) DEFAULT NULL COMMENT '设备的imsi',
  `res` varchar(100) DEFAULT NULL COMMENT '分辨率',
  `model` varchar(100) DEFAULT NULL COMMENT '机型',
  `ts` int(10) DEFAULT NULL COMMENT '时间戳毫秒',
  `platform` tinyint(4) DEFAULT NULL COMMENT '1ios2android',
  `package` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`) USING BTREE,
  KEY `ts` (`ts`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='激活设备';

-- ----------------------------
-- Records of ny_startup_device
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_summary`
-- ----------------------------
DROP TABLE IF EXISTS `ny_summary`;
CREATE TABLE `ny_summary` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `server` varchar(50) DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `acc_reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '帐号注册数',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创角数',
  `reg_no_distinct_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '非重复创角数',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色登录数',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃数',
  `new_num` int(11) NOT NULL DEFAULT '0' COMMENT '新增用户数',
  `old_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老角色数',
  `charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值数',
  `charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `charge_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值次数',
  `new_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新充值人数',
  `new_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '新增充值金额',
  `old_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老充值人数',
  `old_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '老充值金额',
  `max_online_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最高在线人数',
  `avg_online_num` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '平均在线人数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  `active_charge_money` decimal(10,0) DEFAULT '0' COMMENT '活跃充值',
  `active_charge_num` int(11) DEFAULT '0' COMMENT '活跃充值人数',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_summary
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_swimming_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_swimming_copy`;
CREATE TABLE `ny_swimming_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通昆仑瑶池的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `soap_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '捡肥皂人数',
  `soap_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '捡肥皂总次数',
  `swim_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '总沐浴时长/ 秒',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='昆仑瑶池统计表';

-- ----------------------------
-- Records of ny_swimming_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_switch`
-- ----------------------------
DROP TABLE IF EXISTS `ny_switch`;
CREATE TABLE `ny_switch` (
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_switch
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_talk`
-- ----------------------------
DROP TABLE IF EXISTS `ny_talk`;
CREATE TABLE `ny_talk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `package` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `psId` double(50,0) NOT NULL DEFAULT '0' COMMENT '服务器psId',
  `account` varchar(250) NOT NULL DEFAULT '' COMMENT '账号',
  `role_id` double(50,0) NOT NULL COMMENT '角色id',
  `role_name` varchar(100) NOT NULL DEFAULT '0' COMMENT '角色名',
  `role_level` int(11) NOT NULL COMMENT '角色等级',
  `money` int(11) NOT NULL DEFAULT '0' COMMENT '玩家充值数',
  `content` text NOT NULL COMMENT '聊天内容',
  `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1九洲 2本服',
  `ip` varchar(32) NOT NULL COMMENT '用户ip',
  `create_time` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `package` (`package`) USING BTREE,
  KEY `account` (`account`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `psId` (`psId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_talk
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_team_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_team_copy`;
CREATE TABLE `ny_team_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '活跃玩家并且开启多人副本的玩家数',
  `join_num` int(11) NOT NULL DEFAULT '0' COMMENT '参与人数',
  `challenge_num` int(11) NOT NULL COMMENT '挑战人数(成功才算',
  `challenge_count` int(11) NOT NULL DEFAULT '0' COMMENT '挑战成功次数(成功才算',
  `add_num_role` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '增加挑战次数玩家数(去重',
  `add_num_sum` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '玩家增加挑战次数总数',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='多人副本(幽冥鬼境)统计';

-- ----------------------------
-- Records of ny_team_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_three_realms_boss`
-- ----------------------------
DROP TABLE IF EXISTS `ny_three_realms_boss`;
CREATE TABLE `ny_three_realms_boss` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃玩家并且开启多人BOSS的玩家数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `challenge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '挑战次数',
  `inspire_num` int(11) unsigned NOT NULL COMMENT '鼓舞人数',
  `gold_inspire_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '金币鼓舞总次数(没去重',
  `gold_inspire_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '金币鼓舞玩家数(去重',
  `ingots_inspire_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '元宝鼓舞总次数(没去重',
  `ingots_inspire_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '元宝鼓舞玩家数',
  `relive_count` int(11) NOT NULL DEFAULT '0' COMMENT '涅槃复活玩家总次数(没去重',
  `relive_num` int(11) NOT NULL DEFAULT '0' COMMENT '涅槃复活玩家数',
  `add_num_role` int(11) NOT NULL DEFAULT '0' COMMENT '增加挑战次数玩家数(去重',
  `add_num_sum` int(11) NOT NULL DEFAULT '0' COMMENT '玩家增加挑战次数总数',
  `create_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_three_realms_boss
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_tianti_copy`
-- ----------------------------
DROP TABLE IF EXISTS `ny_tianti_copy`;
CREATE TABLE `ny_tianti_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通天梯斗法的人数',
  `join_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与人数',
  `count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '天梯斗法参与次数之和',
  `add_num_role` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '天梯斗法购买次数人数',
  `add_num_sum` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '天梯斗法购买总次数',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='天梯斗法统计表';

-- ----------------------------
-- Records of ny_tianti_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_user`
-- ----------------------------
DROP TABLE IF EXISTS `ny_user`;
CREATE TABLE `ny_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户自增id',
  `username` varchar(128) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(128) NOT NULL DEFAULT '' COMMENT '加密密码',
  `groupid` int(10) NOT NULL DEFAULT '0' COMMENT '用户组id',
  `agent_flag` varchar(50) DEFAULT NULL,
  `channel_flag` varchar(256) DEFAULT NULL,
  `name` varchar(128) DEFAULT '' COMMENT '姓名',
  `email` varchar(256) DEFAULT '' COMMENT 'email',
  `tel` varchar(128) DEFAULT '' COMMENT '电话',
  `department` varchar(128) DEFAULT '' COMMENT '所在部门',
  `create_time` int(10) DEFAULT '0' COMMENT '创建用户时间戳',
  `expiration` int(10) DEFAULT '0' COMMENT '用户过期时间戳',
  `last_login_time` int(10) DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` varchar(128) DEFAULT '' COMMENT '最后登录ip',
  `login_flag` char(16) DEFAULT '' COMMENT '登录跳转验证',
  `data` text COMMENT '其他数据',
  `status` int(10) DEFAULT '0' COMMENT '用户状态',
  PRIMARY KEY (`uid`),
  KEY `username` (`username`),
  KEY `channel_flag` (`channel_flag`(255))
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of ny_user
-- ----------------------------
INSERT INTO `ny_user` VALUES ('1', '1', '28905703d41a4f024180df5dde7f6d7c', '1', '', '-1', '', '', '', '', '0', '0', '1615173671', '118.112.57.133', '', '', '1');

-- ----------------------------
-- Table structure for `ny_user_group`
-- ----------------------------
DROP TABLE IF EXISTS `ny_user_group`;
CREATE TABLE `ny_user_group` (
  `groupid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组自增id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '用户组名称',
  `description` varchar(256) NOT NULL DEFAULT '' COMMENT '描述',
  `center_permit` text COMMENT '央服权限数据',
  `single_permit` text COMMENT '单服权限数据',
  `center_modules` text COMMENT '央服权限模块',
  `single_modules` text COMMENT '单服权限模块',
  `agent` text COMMENT '代理平台',
  `server` text COMMENT '服务器',
  `channel` text,
  `allow_ip` text COMMENT 'ip白名单',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建用户组时间戳',
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='用户组';

-- ----------------------------
-- Records of ny_user_group
-- ----------------------------
INSERT INTO `ny_user_group` VALUES ('1', '超级管理员', '具有所有权限', 'a:35:{i:0;s:3:\"170\";i:1;s:3:\"171\";i:2;s:3:\"172\";i:3;s:3:\"182\";i:4;s:3:\"173\";i:5;s:3:\"174\";i:6;s:3:\"175\";i:7;s:3:\"176\";i:8;s:3:\"177\";i:9;s:3:\"178\";i:10;s:3:\"179\";i:11;s:3:\"180\";i:12;s:3:\"181\";i:13;s:3:\"183\";i:14;s:3:\"184\";i:15;s:3:\"185\";i:16;s:3:\"186\";i:17;s:3:\"188\";i:18;s:3:\"189\";i:19;s:3:\"190\";i:20;s:3:\"191\";i:21;s:2:\"20\";i:22;s:3:\"201\";i:23;s:3:\"202\";i:24;s:3:\"203\";i:25;s:3:\"204\";i:26;s:3:\"206\";i:27;s:3:\"192\";i:28;s:3:\"193\";i:29;s:3:\"194\";i:30;s:3:\"195\";i:31;s:3:\"196\";i:32;s:3:\"197\";i:33;s:3:\"198\";i:34;s:3:\"199\";}', '', 'a:11:{i:0;s:3:\"all\";i:1;s:6:\"mail_*\";i:2;s:6:\"push_*\";i:3;s:8:\"server_*\";i:4;s:6:\"zone_*\";i:5;s:9:\"channel_*\";i:6;s:9:\"package_*\";i:7;s:11:\"usergroup_*\";i:8;s:6:\"user_*\";i:9;s:17:\"setting_hotupdate\";i:10;s:6:\"menu_*\";}', 'a:1:{i:0;s:3:\"all\";}', 'a:1:{i:0;s:3:\"all\";}', '', 'all', 'a:0:{}', '1348107818');
INSERT INTO `ny_user_group` VALUES ('54', '测试', '测试', 'a:4:{i:0;s:3:\"170\";i:1;s:3:\"171\";i:2;s:3:\"172\";i:3;s:3:\"182\";}', null, 'a:0:{}', 'a:0:{}', null, null, '{\"world\":\"all\",\"test224\":\"all\",\"z121\":\"all\",\"eee213\":\"all\",\"ceshi123\":\"all\"}', 'a:0:{}', '0');
INSERT INTO `ny_user_group` VALUES ('55', '权限1', '权限1', 'a:35:{i:0;s:3:\"224\";i:1;s:3:\"220\";i:2;s:3:\"218\";i:3;s:3:\"225\";i:4;s:3:\"223\";i:5;s:3:\"222\";i:6;s:3:\"221\";i:7;s:3:\"219\";i:8;s:3:\"217\";i:9;s:3:\"196\";i:10;s:3:\"197\";i:11;s:3:\"198\";i:12;s:3:\"199\";i:13;s:3:\"255\";i:14;s:3:\"256\";i:15;s:3:\"257\";i:16;s:3:\"258\";i:17;s:3:\"201\";i:18;s:3:\"202\";i:19;s:3:\"203\";i:20;s:3:\"204\";i:21;s:3:\"206\";i:22;s:3:\"263\";i:23;s:3:\"259\";i:24;s:3:\"260\";i:25;s:3:\"261\";i:26;s:3:\"262\";i:27;s:3:\"264\";i:28;s:3:\"265\";i:29;s:3:\"216\";i:30;s:3:\"215\";i:31;s:3:\"267\";i:32;s:3:\"268\";i:33;s:3:\"269\";i:34;s:3:\"270\";}', null, 'a:34:{i:0;s:10:\"teamcopy_*\";i:1;s:7:\"zones_*\";i:2;s:10:\"manyboss_*\";i:3;s:8:\"server_*\";i:4;s:6:\"zone_*\";i:5;s:9:\"channel_*\";i:6;s:9:\"package_*\";i:7;s:11:\"usergroup_*\";i:8;s:6:\"user_*\";i:9;s:17:\"setting_hotupdate\";i:10;s:6:\"menu_*\";i:11;s:10:\"runecopy_*\";i:12;s:10:\"bosshome_*\";i:13;s:12:\"dailytrial_*\";i:14;s:17:\"threerealmsboss_*\";i:15;s:7:\"zones_*\";i:16;s:12:\"singleboss_*\";i:17;s:10:\"userinfo_*\";i:18;s:13:\"Instantdata_*\";i:19;s:6:\"user_*\";i:20;s:7:\"merge_*\";i:21;s:6:\"plan_*\";i:22;s:8:\"review_*\";i:23;s:13:\"maintenance_*\";i:24;s:12:\"itemconfig_*\";i:25;s:9:\"setting_*\";i:26;s:9:\"version_*\";i:27;s:12:\"chargetrue_*\";i:28;s:9:\"setting_*\";i:29;s:10:\"tooldeal_*\";i:30;s:5:\"ltv_*\";i:31;s:5:\"ltv_*\";i:32;s:5:\"ltv_*\";i:33;s:27:\"novicetask_* | noviceTask_*\";}', 'a:0:{}', null, null, '{\"z121\":\"all\",\"eee213\":\"all\",\"o232\":\"all\",\"ceshi123\":\"all\",\"test\":\"all\"}', 'a:0:{}', '0');
INSERT INTO `ny_user_group` VALUES ('56', '权限2', '权限2', 'a:5:{i:0;s:3:\"170\";i:1;s:3:\"171\";i:2;s:3:\"172\";i:3;s:3:\"182\";i:4;s:3:\"189\";}', null, 'a:0:{}', 'a:0:{}', null, null, '{\"cs123\":\"all\",\"h222\":\"all\",\"9130\":\"all\"}', 'a:0:{}', '0');
INSERT INTO `ny_user_group` VALUES ('57', '权限3', '权限3', 'a:12:{i:0;s:3:\"170\";i:1;s:3:\"171\";i:2;s:3:\"172\";i:3;s:3:\"182\";i:4;s:3:\"183\";i:5;s:3:\"184\";i:6;s:3:\"185\";i:7;s:3:\"186\";i:8;s:3:\"188\";i:9;s:3:\"189\";i:10;s:3:\"190\";i:11;s:3:\"191\";}', null, 'a:0:{}', 'a:0:{}', null, null, '{\"huawei\":\"all\"}', 'a:0:{}', '0');
INSERT INTO `ny_user_group` VALUES ('58', '权限4', '权限4', 'a:16:{i:0;s:3:\"170\";i:1;s:3:\"171\";i:2;s:3:\"172\";i:3;s:3:\"182\";i:4;s:3:\"178\";i:5;s:3:\"179\";i:6;s:3:\"180\";i:7;s:3:\"181\";i:8;s:3:\"183\";i:9;s:3:\"184\";i:10;s:3:\"185\";i:11;s:3:\"186\";i:12;s:3:\"188\";i:13;s:3:\"189\";i:14;s:3:\"190\";i:15;s:3:\"191\";}', null, 'a:0:{}', 'a:0:{}', null, null, '{\"world\":[\"9455555\"]}', 'a:0:{}', '0');

-- ----------------------------
-- Table structure for `ny_vip`
-- ----------------------------
DROP TABLE IF EXISTS `ny_vip`;
CREATE TABLE `ny_vip` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) DEFAULT NULL COMMENT '服务器标识',
  `paying_user_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '付费用户数',
  `vip_msg` text NOT NULL COMMENT 'vip数量; msgpack_pack后插入',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `server` (`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='vip统计表;\r\n付费用户数：从开服至截止到所选时间为止，总付费用户数;\r\nVIP玩家数：从开服至截止到所选时间为止，各档次VIP玩家人数。';

-- ----------------------------
-- Records of ny_vip
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_vipnotice`
-- ----------------------------
DROP TABLE IF EXISTS `ny_vipnotice`;
CREATE TABLE `ny_vipnotice` (
  `id` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(15) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `type` char(10) NOT NULL COMMENT '推送类型 1：邮件推送，2：公告更新',
  `title` varchar(255) DEFAULT NULL COMMENT '邮件标题',
  `connect` varchar(255) DEFAULT NULL,
  `content` text COMMENT '邮件内容',
  `level` varchar(255) DEFAULT NULL COMMENT 'param type json 充值档位',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态 1：同意，2：拒绝',
  `back_massage` text,
  `manage` varchar(50) NOT NULL COMMENT '管理员',
  `sign` char(32) DEFAULT NULL COMMENT '数据签名，用于更新校验',
  `create_time` char(11) DEFAULT NULL COMMENT '创建时间',
  `last_modified_time` char(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_vipnotice
-- ----------------------------
INSERT INTO `ny_vipnotice` VALUES ('1', '19', null, '1', '[\"12\"]', '[\"12\"]', '[\"12\"]', '[\"12\"]', '2', null, 'GM', '9FD130F3EFC42E57E023174931592BCC', '1540261455', '1540261455');

-- ----------------------------
-- Table structure for `ny_virtual_order`
-- ----------------------------
DROP TABLE IF EXISTS `ny_virtual_order`;
CREATE TABLE `ny_virtual_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(255) DEFAULT NULL,
  `server` varchar(20) DEFAULT '' COMMENT '服务器id',
  `order_num` varchar(50) DEFAULT '' COMMENT '订单号',
  `corder_num` varchar(50) DEFAULT NULL,
  `money` int(11) unsigned DEFAULT '0' COMMENT '金额',
  `account` text COMMENT '账号',
  `role_id` text COMMENT '角色id',
  `role_name` text COMMENT '角色名',
  `item_id` int(11) unsigned DEFAULT '0' COMMENT '充值档次',
  `first` tinyint(1) unsigned DEFAULT '0' COMMENT '是否首冲，1是首冲，0不是首冲',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `admin` varchar(128) DEFAULT '' COMMENT '管理员名称',
  `checked` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_virtual_order
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_white_ip`
-- ----------------------------
DROP TABLE IF EXISTS `ny_white_ip`;
CREATE TABLE `ny_white_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(20) DEFAULT NULL COMMENT '服务器',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip白名单',
  `user` varchar(128) DEFAULT NULL COMMENT '操作的管理员',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `server` (`server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_white_ip
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_xianfu_info`
-- ----------------------------
DROP TABLE IF EXISTS `ny_xianfu_info`;
CREATE TABLE `ny_xianfu_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) unsigned NOT NULL COMMENT '活跃并且开通仙府洞天的人数',
  `huiling_num` int(11) unsigned NOT NULL COMMENT '该日收获汇灵阵次数≥1的玩家数',
  `jubao_num` int(11) unsigned NOT NULL COMMENT '该日收获聚宝盆次数≥1的玩家数',
  `lianzhi_num` int(11) unsigned NOT NULL COMMENT '该日进行炼制(3种炼制都算)次数≥1的玩家数',
  `lianzhi_count` int(11) unsigned NOT NULL COMMENT '该日进行炼制(3种炼制都算)次数总和',
  `youli_num` int(11) unsigned NOT NULL COMMENT '该日进行灵兽游历(4只灵兽都算)次数≥1的玩家数',
  `youli_count` int(11) unsigned NOT NULL COMMENT '该日进行灵兽游历(4只灵兽都算)次数总和',
  `is_compass_num` int(11) unsigned NOT NULL COMMENT '该日进行灵兽游历使用罗盘人数',
  `is_compass_count` int(11) unsigned NOT NULL COMMENT '该日进行灵兽游历使用罗盘总次数',
  `is_sihai_or_jiuzhou_count` int(11) unsigned NOT NULL COMMENT '该日全服【四海八荒】及【九州三界】的游历总次数',
  `is_end_num` int(11) unsigned NOT NULL COMMENT '该日全服进行过立即结束游历的人数',
  `is_end_count` int(11) unsigned NOT NULL COMMENT '该日全服游历中立即结束的游历次数',
  `active_val_20_num` int(11) unsigned NOT NULL COMMENT '该日仙府任务活跃度≥20 的玩家数',
  `active_val_60_num` int(11) unsigned NOT NULL COMMENT '该日仙府任务活跃度≥60 的玩家数',
  `active_val_100_num` int(11) unsigned NOT NULL COMMENT '该日仙府任务活跃度≥100 的玩家数',
  `active_val_150_num` int(11) unsigned NOT NULL COMMENT '该日仙府任务活跃度≥150 的玩家数',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='仙府统计表';

-- ----------------------------
-- Records of ny_xianfu_info
-- ----------------------------

-- ----------------------------
-- Table structure for `ny_zone`
-- ----------------------------
DROP TABLE IF EXISTS `ny_zone`;
CREATE TABLE `ny_zone` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL COMMENT '所属渠道',
  `name` varchar(100) DEFAULT NULL COMMENT '分区名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '分区备注',
  `cdn_url` varchar(255) DEFAULT NULL COMMENT 'cdn链接 不填则取服务器对呀渠道的cdn',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ny_zone
-- ----------------------------
INSERT INTO `ny_zone` VALUES ('1', '1', '1-100服', '双线系列', '', '1', '1555335138');

-- ----------------------------
-- Table structure for `ny_zones`
-- ----------------------------
DROP TABLE IF EXISTS `ny_zones`;
CREATE TABLE `ny_zones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL COMMENT '服务器标识',
  `active_num` int(11) NOT NULL DEFAULT '0' COMMENT '活跃玩家并且开启副本的玩家数',
  `zones_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '副本类型:1天关系统2大荒古塔',
  `join_num` int(11) NOT NULL DEFAULT '0' COMMENT '参与人数',
  `challenge_num` int(11) NOT NULL COMMENT '挑战次数(成功才算',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ny_zones
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_account_remain`
-- ----------------------------
DROP TABLE IF EXISTS `stat_account_remain`;
CREATE TABLE `stat_account_remain` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '类型 1:角色，2:帐号,3:付费角色，4:付费帐号',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册数',
  `login_2` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第2天登录数',
  `login_3` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第3天登录数',
  `login_4` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第4天登录数',
  `login_5` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第5天登录数',
  `login_6` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第6天登录数',
  `login_7` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第7天登录数',
  `login_8` int(11) unsigned NOT NULL DEFAULT '0',
  `login_9` int(11) unsigned NOT NULL DEFAULT '0',
  `login_10` int(11) unsigned NOT NULL DEFAULT '0',
  `login_11` int(11) unsigned NOT NULL DEFAULT '0',
  `login_12` int(11) unsigned NOT NULL DEFAULT '0',
  `login_13` int(11) unsigned NOT NULL DEFAULT '0',
  `login_14` int(11) unsigned NOT NULL DEFAULT '0',
  `login_15` int(11) unsigned NOT NULL DEFAULT '0',
  `login_16` int(11) unsigned NOT NULL DEFAULT '0',
  `login_17` int(11) unsigned NOT NULL DEFAULT '0',
  `login_18` int(11) unsigned NOT NULL DEFAULT '0',
  `login_19` int(11) unsigned NOT NULL DEFAULT '0',
  `login_20` int(11) unsigned NOT NULL DEFAULT '0',
  `login_21` int(11) unsigned NOT NULL DEFAULT '0',
  `login_22` int(11) unsigned NOT NULL DEFAULT '0',
  `login_23` int(11) unsigned NOT NULL DEFAULT '0',
  `login_24` int(11) unsigned NOT NULL DEFAULT '0',
  `login_25` int(11) unsigned NOT NULL DEFAULT '0',
  `login_26` int(11) unsigned NOT NULL DEFAULT '0',
  `login_27` int(11) unsigned NOT NULL DEFAULT '0',
  `login_28` int(11) unsigned NOT NULL DEFAULT '0',
  `login_29` int(11) unsigned NOT NULL DEFAULT '0',
  `login_30` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第30天登录数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`) USING BTREE,
  KEY `type` (`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of stat_account_remain
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_account_summary`
-- ----------------------------
DROP TABLE IF EXISTS `stat_account_summary`;
CREATE TABLE `stat_account_summary` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '帐号数',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色登录数',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃数',
  `old_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老角色数',
  `charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值数',
  `charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `charge_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值次数',
  `new_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新充值人数',
  `new_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '新增充值金额',
  `old_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老充值人数',
  `old_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '老充值金额',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帐号数据汇总';

-- ----------------------------
-- Records of stat_account_summary
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_activity`
-- ----------------------------
DROP TABLE IF EXISTS `stat_activity`;
CREATE TABLE `stat_activity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `server` varchar(50) DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '活动类型',
  `total_consume_gold` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '总消耗元宝',
  `total_consume_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '总消耗人数',
  `charge_gold` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值元宝',
  `charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值人数',
  `inventory_gold` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '库存元宝',
  `single_consume_gold` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '单活动消耗的元宝数',
  `single_consume_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '单活动消耗的人数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`server`,`package`,`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stat_activity
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_charge_daily`
-- ----------------------------
DROP TABLE IF EXISTS `stat_charge_daily`;
CREATE TABLE `stat_charge_daily` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色注册数',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色登录数',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值人数',
  `charge_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值次数',
  `new_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增充值次数',
  `new_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '新增充值总额',
  `first_charge_num` int(11) NOT NULL DEFAULT '0' COMMENT '首充人数',
  `first_charge_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '首充金额',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='每日充值';

-- ----------------------------
-- Records of stat_charge_daily
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_item`
-- ----------------------------
DROP TABLE IF EXISTS `stat_item`;
CREATE TABLE `stat_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1元宝 2绑定元宝 3铜钱 4道具 5积分',
  `use_num` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '消耗货币',
  `use_role_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与消耗的角色数',
  `get_num` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '产出货币',
  `get_role_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与产出的角色数',
  `inventory` bigint(20) NOT NULL DEFAULT '0' COMMENT '库存',
  `total_use` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '总消耗',
  `total_get` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '总产出',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `use_item_id` text COMMENT '消耗的道具(新增的字段20180315)',
  `use_source` text COMMENT '去向(新增的字段20180315)',
  `get_item_id` text COMMENT '获得的道具(新增的字段20180315)',
  `get_source` text COMMENT '来源(新增的字段20180315)',
  `use_coin` text COMMENT '消耗金钱数',
  `use_coin_source` text COMMENT '消耗金钱的去向',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产出消耗表';

-- ----------------------------
-- Records of stat_item
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_level`
-- ----------------------------
DROP TABLE IF EXISTS `stat_level`;
CREATE TABLE `stat_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(20) NOT NULL,
  `level` int(4) NOT NULL DEFAULT '1' COMMENT '等级',
  `level_count` int(11) NOT NULL DEFAULT '0' COMMENT '该等级人数',
  `role_count` int(11) NOT NULL COMMENT '玩家总人数',
  `level_rate` varchar(20) NOT NULL DEFAULT '0' COMMENT '等级分布率',
  `stay_num` int(11) NOT NULL DEFAULT '0' COMMENT '等级滞留人数',
  `level_loss_num` int(11) NOT NULL DEFAULT '0' COMMENT '等级流失人数',
  `level_loss_rate` varchar(20) NOT NULL DEFAULT '0' COMMENT '等级流失比率',
  `create_time` int(11) NOT NULL COMMENT '创建时间 年月日int',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of stat_level
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_login`
-- ----------------------------
DROP TABLE IF EXISTS `stat_login`;
CREATE TABLE `stat_login` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录人数',
  `login_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `old_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老玩家数',
  `old_online_time` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '老玩家在线时长',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃人数',
  `fans_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '忠实玩家',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `server` (`server`,`package`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stat_login
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_ltv`
-- ----------------------------
DROP TABLE IF EXISTS `stat_ltv`;
CREATE TABLE `stat_ltv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'ltv类型 1创角ltv 2注册ltv',
  `num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '人数',
  `day1` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '1天付费金额',
  `day2` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '2天付费金额',
  `day3` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '3天付费金额',
  `day4` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '4天付费金额',
  `day5` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '5天付费金额',
  `day6` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '6天付费金额',
  `day7` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '7天付费金额',
  `day14` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '14天付费金额',
  `day30` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '30天付费金额',
  `day60` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '60天付费金额',
  `day90` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '90天付费金额',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `time` (`time`),
  KEY `server` (`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='LTV统计';

-- ----------------------------
-- Records of stat_ltv
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_remain`
-- ----------------------------
DROP TABLE IF EXISTS `stat_remain`;
CREATE TABLE `stat_remain` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '类型 1:角色，2:帐号,3:付费角色，4:付费帐号',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册数',
  `login_2` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第2天登录数',
  `login_3` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第3天登录数',
  `login_4` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第4天登录数',
  `login_5` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第5天登录数',
  `login_6` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第6天登录数',
  `login_7` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第7天登录数',
  `login_8` int(11) unsigned NOT NULL DEFAULT '0',
  `login_9` int(11) unsigned NOT NULL DEFAULT '0',
  `login_10` int(11) unsigned NOT NULL DEFAULT '0',
  `login_11` int(11) unsigned NOT NULL DEFAULT '0',
  `login_12` int(11) unsigned NOT NULL DEFAULT '0',
  `login_13` int(11) unsigned NOT NULL DEFAULT '0',
  `login_14` int(11) unsigned NOT NULL DEFAULT '0',
  `login_15` int(11) unsigned NOT NULL DEFAULT '0',
  `login_16` int(11) unsigned NOT NULL DEFAULT '0',
  `login_17` int(11) unsigned NOT NULL DEFAULT '0',
  `login_18` int(11) unsigned NOT NULL DEFAULT '0',
  `login_19` int(11) unsigned NOT NULL DEFAULT '0',
  `login_20` int(11) unsigned NOT NULL DEFAULT '0',
  `login_21` int(11) unsigned NOT NULL DEFAULT '0',
  `login_22` int(11) unsigned NOT NULL DEFAULT '0',
  `login_23` int(11) unsigned NOT NULL DEFAULT '0',
  `login_24` int(11) unsigned NOT NULL DEFAULT '0',
  `login_25` int(11) unsigned NOT NULL DEFAULT '0',
  `login_26` int(11) unsigned NOT NULL DEFAULT '0',
  `login_27` int(11) unsigned NOT NULL DEFAULT '0',
  `login_28` int(11) unsigned NOT NULL DEFAULT '0',
  `login_29` int(11) unsigned NOT NULL DEFAULT '0',
  `login_30` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第30天登录数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stat_remain
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_role`
-- ----------------------------
DROP TABLE IF EXISTS `stat_role`;
CREATE TABLE `stat_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `server` varchar(50) NOT NULL COMMENT '服务器标识符',
  `package` mediumint(8) NOT NULL DEFAULT '0' COMMENT '包号',
  `reg_num` int(11) NOT NULL DEFAULT '0' COMMENT '创角数',
  `mac_num` int(11) NOT NULL DEFAULT '0' COMMENT '创角设备数',
  `time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`server`,`package`,`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stat_role
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_summary`
-- ----------------------------
DROP TABLE IF EXISTS `stat_summary`;
CREATE TABLE `stat_summary` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `server` varchar(50) DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `acc_reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '帐号注册数',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创角数',
  `reg_no_distinct_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '非重复创角数',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色登录数',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃数',
  `new_num` int(11) NOT NULL COMMENT '新增用户数',
  `old_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老角色数',
  `charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值数',
  `charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `charge_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值次数',
  `new_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新充值人数',
  `new_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '新增充值金额',
  `old_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老充值人数',
  `old_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '老充值金额',
  `max_online_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最高在线人数',
  `avg_online_num` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '平均在线人数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  `active_charge_money` decimal(10,0) DEFAULT '0' COMMENT '活跃充值',
  `active_charge_num` int(11) DEFAULT '0' COMMENT '活跃充值人数',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stat_summary
-- ----------------------------

-- ----------------------------
-- Table structure for `stat_summary_min`
-- ----------------------------
DROP TABLE IF EXISTS `stat_summary_min`;
CREATE TABLE `stat_summary_min` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `server` varchar(50) DEFAULT '' COMMENT '服务器标识符',
  `package` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
  `acc_reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '帐号注册数',
  `reg_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创角数',
  `reg_no_distinct_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '非重复创角数',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色登录数',
  `active_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活跃数',
  `old_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老角色数',
  `charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值数',
  `charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `charge_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值次数',
  `new_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新充值人数',
  `new_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '新增充值金额',
  `old_charge_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '老充值人数',
  `old_charge_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '老充值金额',
  `max_online_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最高在线人数',
  `avg_online_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '平均在线人数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`server`,`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stat_summary_min
-- ----------------------------
