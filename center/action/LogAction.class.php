<?php
!defined('IN_WEB') && exit('Access Denied');

class LogAction{
    public function __construct(){
        $this->logModel = new DbcenterModel('log');
        //不能记录用户修改密码的页面的数据，否则用户密码会以明文保存在数据库中
        $this->canNotLogData = array('user&change_password' , 'index&login');
    }

    public function logAdd($desc){
        $ctrl = isset($_GET['ctrl']) ? trim($_GET['ctrl']) : 'index';
        $act = isset($_GET['act']) ? trim($_GET['act']) : 'index';
        $now = time();
        $userAction = new UserAction();
        if(!in_array($ctrl.'&'.$act, $this->canNotLogData)){
            $data = array('POST' => $_POST, 'GET' => $_GET);
        }
        else{
            $data = array('POST' => '不保存用户密码明文');
        }
        $log = array(
            'uid' => $userAction->userSessionGet('uid'),
            'username' => $userAction->userSessionGet('username'),
            'ip' => get_ip(),
            'ctrl' => $ctrl,
            'act' => $act,
            'desc2' => $desc,
            'data' => serialize($data),
            'created' => $now,
        );
        return $this->logModel->insert($log);
    }

    public function logList($conditions=null){
        //page, count, sort
        $page = $conditions['page']? $conditions['page']: 0;
        $count = $conditions['count']? $conditions['count']: DEFAULT_PAGE_COUNT;
        $from = $conditions['from']? $conditions['from']: $page*$count;
        $sortBy = $conditions['sort'] ? $conditions['sort'] : '`log_id` DESC';
        $where = $conditions['where']? $conditions['where']: array();
        $logs = $this->logModel->getList('*', $where, $from, $count, $sortBy);
        return $logs;
    }

    public function logListJson($conditions = null){
        $logs = $this->logList($conditions);
        $countThis = count($logs);
        $countAll = $this->logModel->count($conditions['where']);
        $json_arr = array(
            'sEcho' => intval($_GET['sEcho']),
            'iTotalRecords' => $countThis,
            "iTotalDisplayRecords" => $countAll
        );
        $aaData = array();
        foreach ($logs as $log) {
            $aaData[] = array(
                "<img src=\"style/images/details_open.png\" />
                    <span style=\"display:none;\" class=\"log_id\">{$log['log_id']}</span>",
                $log['ctrl'],
                $log['act'],
                $log['desc'],
                $log['username'],
                $log['ip'],
                date('Y-m-d H:i:s', $log['created']),
            );
        }
        $json_arr['aaData'] = $aaData;
        return json_encode($json_arr);
    }

    public function logGetOne($lid){
        return $this->logModel->getOneById($lid);
    }
}
