<?php
!defined('IN_WEB') && exit('Access Denied');

class MenuAction {
    protected $menuModel;

    function __construct(){
        $this->menuModel = new DbcenterModel('menu');
    }

    function menuTree($type, $parent=0, $showAll = true){
        static $list;
        if(empty($list[$type])){
            $where['type'] = $type;
            if($showAll == false) {
                $where['status'] = MENU_STATUS_SHOW;
            }
            $list[$type] = $this->menuModel->getList('*', $where, 0, 0, 'weight');
        }

        if(empty($list[$type])) return array();

        $menuTree = array();
        foreach($list[$type] as $menu){
            if($menu['parent'] == $parent){
                $menu['children'] = $this->menuTree($type, $menu['id'], $showAll);

                //判断权限是否显示
                $hasPurview = 1;
                if($menu['url'] && $showAll == false){
                    $ctrl = $act = '';
                    $parse_url = parse_url($menu['url']);
                    $query = $parse_url['query'];
                    parse_str($query);

                    $hasPurview = has_purview($ctrl, $act);
                }

                if($menu['parent'] == 0 && empty($menu['children']) && empty($menu['url'])) $hasPurview = 0;
                $hasPurview == 1 && $menuTree[$menu['id']] = $menu;
            }
        }

        return $menuTree;
    }

    function menuGetOne($id){
        return $this->menuModel->getOneById($id);
    }
    function menuRoot($type){
        $where = "`type` ='{$type}' AND `parent`= 0 ORDER BY weight";
        return $this->menuModel->getList('*', $where);
    }
    function menuGetBySelf($where){
        return $this->menuModel->getList('*', $where);
    }

    function menuGetChildren($parent){
        return $this->menuModel->getList('*', 'parent='.$parent);
    }
    function menuVersion(){
        $menuVersion = variable_get('menu_version');
        $menuVersion = $menuVersion? intval($menuVersion): 0;
        return $menuVersion;
    }
    function menuVersionUpdate(){
        $menuVersion = $this->menuVersion();
        $menuVersion++;
        variable_set('menu_version', $menuVersion);
    }
    function menuAdd($data){
        $this->menuVersionUpdate();
        return $this->menuModel->insert($data);
    }
    function menuUpdate($data){
        $this->menuVersionUpdate();
        return $this->menuModel->update($data);
    }

    function menuDelete($id){
        $this->menuVersionUpdate();
        $children = $this->menuGetChildren($id);
        if(empty($children))
            return $this->menuModel->deleteById($id);
    }
    function save_weight($menu_root_ids, $menu_sub_ids){
        $this->menuVersionUpdate();
        $data = array();
        $weight = 0;
        $menu_root_arr = array_filter(explode(',', $menu_root_ids));
        $menu_sub_arr = array_filter(explode('|', $menu_sub_ids));
        foreach($menu_root_arr as $k=>$root_id){
            $row = array(
                'id' =>  $root_id,
                'weight' => $weight,
                'parent' => 0,
            );
            $data[] = $row;

            $weight++;
            $tmp_sub_arr = array_filter(explode(',', $menu_sub_arr[$k]));
            foreach($tmp_sub_arr as $sub_id){
                $row = array(
                    'id' =>  $sub_id,
                    'weight' => $weight,
                    'parent' => $root_id,
                );
                $data[] = $row;
                $weight++;
            }
        }

        $sql = '';
        foreach($data as $row){
            $this->menuModel->update($row);
        }
        return 1;

    }


}