<?php
!defined('IN_WEB') && exit('Access Denied');

class PermitAction
{
    function __construct()
    {
        $this->permitModel = new DbcenterModel('permit');
    }

    function permitTree($type)
    {
        $list = $this->permitListByType($type);
        $tree = array();
        foreach ($list as $row) {
            $row["modules_show"] = $this->modulesShow($row["modules"]);
            $tree[$row['group']][$row['name']] = $row;
        }

        return $tree;
    }

    function modulesShow($modules = array())
    {
        if (!empty($modules)) {
            return implode(" | ", unserialize($modules));
        }
    }

    function modulesSaveFormat($modules = array())
    {
        return serialize($modules);
    }

    function permitAdd($type, $group, $groupName, $name, $modules)
    {
        $data = array();
        $data['type'] = $type;
        $data['group'] = $groupName;
        $data['group_id'] = $group;
        $data['name'] = $name;
        $data['modules'] = $this->modulesSaveFormat($modules);
        $data['created'] = time();
        return $this->permitModel->insert($data);
    }

    function permitListByType($type)
    {
        $list = $this->permitModel->getList("", array("type" => $type));
        return $list;
    }

    function permitDelete($pid)
    {
        return $this->permitModel->deleteById($pid);
    }

    function permitUpdate($pid, $field, $value)
    {
        if ($field == "modules") {
            $value = $this->modulesSaveFormat(textareaStrToArr($value));
        }
        $data = array(
            'pid' => $pid,
            $field => $value,
        );
        return $this->permitModel->update($data);
    }

    function permitModulesTextarea($pid)
    {
        $permit = $this->permitModel->getOneById($pid);
        $str = $permit["modules"] ? implode("\n", unserialize($permit["modules"])) : '';
        return $str;
    }

    function permitListByIds($ids)
    {
        $list = array();
        if (empty($ids)) {
            return $list;
        }
        if (is_numeric($ids)) {
            $ids = array($ids);
        }
        $list = $this->permitModel->getList("", "pid IN (" . implode(",", $ids) . ")");
        return $list;
    }

    //一键同步菜单权限
    function permitSynchronize()
    {
        $time = time();

        //获取央服菜单
        $menuTree = (new MenuAction())->menuTree('tree');

        if (empty($menuTree)) {
            return false;
        }

//        $list = $this->permitModel->getList('pid', " `group` = '数据统计'");
//
//        foreach ($list as $v) {
//            $this->permitUpdate($v['pid'], 'group', '综合数据统计');
//        }


        $this->permitUpdate(208, 'name', '活动临时列表');

        foreach ($menuTree as $val) {
            if ($val['children']) {
                foreach ($val['children'] as $child) {

                    $pid = $this->permitModel->getOne(" `group` = '" . $val['name'] . "' and `name` = '" . $child['name'] . "' and `group_id`is null and `type` = 'center'");

                    if (!$pid) continue;

                    $this->permitUpdate($pid['pid'], 'group_id', $val['id']);
                }
            }
        }

        $this->permitUpdate(209, 'group_id', 34);
        $this->permitUpdate(210, 'group_id', 34);

        unset($val);
        unset($menuTree);

        $menuTree = (new MenuAction())->menuTree('tree');
        //$str = '';
        foreach ($menuTree as $val) {
            if ($val['children']) {
                foreach ($val['children'] as $child) {
                    if ($val['status'] == 1) {
                        $pid = $this->permitModel->getOne(" `group` = '" . $val['name'] . "' and `name` = '" . $child['name'] . "' and `group_id` = '" . $val['id'] . "'");
                        if ($child['status'] == 1) {
                            if (!$pid) {
                                //insert一个新的权限
                                $iData = [
                                    'type' => 'center',
                                    'group' => $val['name'],
                                    'group_id' => $val['id'],
                                    'name' => $child['name'],
                                    'modules' => '',
                                    'created' => $time
                                ];

                                if ($child['url']) {
                                    $url = parse_url($child['url']);
                                    parse_str($url['query'], $query);
                                    $modules = $query['ctrl'] ? $this->modulesSaveFormat(textareaStrToArr($query['ctrl'] . '_*')) : '';
                                    $iData['modules'] = $modules;
                                }


                                $this->permitModel->insert($iData);
                            }
                        } else {
                            if ($pid) {
                                // $str .= $pid['group'].'_'.$pid['name']."\n";
                                $this->permitModel->deleteById($pid['pid']);
                            }
                        }
                    } else {
                        $pid = $this->permitModel->getOne(" `group` = '" . $val['name'] . "' and `name` = '" . $child['name'] . "' and `group_id` = '" . $val['id'] . "'");
                        if ($pid) {
                            // $str .= $pid['group'].'_'.$pid['name']."\n";
                            $this->permitModel->deleteById($pid['pid']);
                        }
                    }
                }
            }
        }

        //顺便更新用户组权限
        (new UsergroupAction())->updateAllGroup();

        //  var_dump($str);exit;
        return true;
    }
}
//ALTER TABLE `ny_permit`
//ADD COLUMN `group_id`  int(11) NULL COMMENT '分组id; 对应ny_menu表父级分类' AFTER `group`;