<?php
!defined('IN_WEB') && exit('Access Denied');

class UserAction
{
    protected $userModel;

    public function __construct(){
        $this->userModel        = new DbcenterModel('user');
        $this->userGroupModel   = new DbcenterModel('user_group');
        $this->failedLoginModel = new DbcenterModel('failed_login');
    }

    public function clearLoginErr($username = ''){
        if (!empty($username)) {
            $this->failedLoginModel->delete("`username` = '{$username}' AND created >= " . (time() - 3600));
        }
    }

    public function resetPassword($uid = 0)
    {
        if ($uid > 0) {
            $randomPassword = $this->_randomPassword();
            $password       = $randomPassword['secret'];
            $data           = array('uid' => $uid, 'password' => $password);
            $this->userUpdate($data);

            return $randomPassword['str'];
        }

        $users = $this->userGetList();
        $now   = time();
        foreach ($users as $user) {
            $user_data = $user['data'] ? unserialize($user['data']) : array();
            if ($now - $user_data['change_password_time'] > 86400 * 720) {
                $randomPassword = $this->_randomPassword();
                $password       = $randomPassword['secret'];
                $data           = array('uid' => $user['uid'], 'password' => $password);
                $this->userUpdate($data);
                $resetUsers[] = $user['username'];
            }
        }

        return $resetUsers;
    }

    public function hasPurview($ctrl = '', $act = '')
    {
        global $__global;
        $noPermissionPage = $__global['noPermissionPage'];
        $mustPassPage     = $__global['mustPassPage'];
        $ctrl             = $ctrl ? $ctrl : 'index';
        $act              = $act ? $act : 'index';
        $permission       = "{$ctrl}_{$act}";

        //如果是登录页面和显示验证码的页面则不需要权限，直接通过
        if (in_array($permission, $noPermissionPage)) {
            return 1;
        }

        $user = $this->adminIsLogin();
        //未登录
        if (!$user) {
            return -1;
        }

        //如果用户状态不正确也不能通过，实现在线封禁
        if ($user['status'] != USER_STATUS_NORMAL) {
            return -4;
        }

        $usergroupAction = new UsergroupAction();
        $group           = $usergroupAction->groupGetOne($user['groupid']);

        //用户组ip白名单过滤
        if ($group['allow_ip_data'] && !in_array(get_ip(), $group['allow_ip_data'])) {
            return -2;
        }

        //已登录用户访问必选权限页面必须通过，如index_index
        if (in_array("{$ctrl}_*", $mustPassPage) || in_array($permission, $mustPassPage)) {
            $pass = "pass";
        }
        /**
         * 已登录用户
         * 如果具有所有权限
         * 或者具有所在组的权限
         * 或者具有当前页面权限
         * 则通过
         */
        if ($group && (in_array('all', $group['center_modules_data'])
            || in_array("{$ctrl}_*", $group['center_modules_data'])
            || in_array($permission, $group['center_modules_data']))) {
            $pass = "pass";

        }
        if ($pass == "pass") {
            $now = time();
            $this->userSessionSet('login_time', $now);

            return 1;
        } else {
            //没有访问权限
            return -3;
        }

    }

    public function userSessionSet($name, $value)
    {
        $_SESSION[$name] = $value;

        return $value;
    }

    public function userSessionGet($name)
    {
        return $_SESSION[$name];
    }

    public function userSessionDestroy($name)
    {
        unset($_SESSION[$name]);
    }

    public function adminIsLogin()
    {
        $now        = time();
        $login_time = $this->userSessionGet('login_time');
        if ($this->userSessionGet('admin') != true || !$login_time || $login_time < $now - 43200 || !$this->userSessionGet('uid')) {
            return false;
        }

        $user = $this->getUserByUid($this->userSessionGet('uid'));
        if (!$user) {
            return false;
        }

        //已登录用户重新获取用户信息保存在session中
        $this->setLoginSession($user);

        return $user;
    }

    public function logout()
    {
        if ($this->userSessionGet('admin')) {
            $this->userSessionDestroy('admin');
        }

    }

    public function userFailedLogin($username, $password, $ip)
    {
        //记录日志
        $logAction      = new LogAction();
        $hiddenPassword = substr($password, 0, 1) . '***' . substr($password, -1, 1);
        $data           = array(
            'username' => $username,
            'password' => $password,
            'ip'       => $ip,
        );
        $logAction->logAdd("用户登录失败，用户名:{$username}， 密码：{$hiddenPassword}，ip：{$ip}");
        $this->failedLoginAdd($ip, $username);
    }

    public function failedLoginAdd($ip, $username)
    {
        //所有错误登录均进行记录，但登录ip白名单中的ip不进行封禁
        $now = time();

        $data = array(
            'ip'       => $ip,
            'username' => $username,
            'created'  => $now,
        );

        $this->failedLoginModel->insert($data);
    }

    public function failedLogin($ip = null)
    {
        $login_ip_white_arr = $this->loginWhiteIp();
        if(!empty($login_ip_white_arr) && is_array($login_ip_white_arr)){
            if (in_array($ip, $login_ip_white_arr)) {
                return true;
            }
        }
        $now       = time();
        $failTimes = $this->failedLoginModel->count("created >" . ($now - 3600) . " AND ip='{$ip}'");
        if ($failTimes >= 10) {
            return false;
        }

        return true;
    }

    public function loginWhiteIp()
    {
        $variableAction = new VariableAction();
        $ip_arr         = $variableAction->variableGet("login_white_ip");

        return $ip_arr;

    }
    /**
     *
     * @param $local true为本地登录，false为远程登录，本地登录需要验证login_token，验证码
     * @param  unknown_type $username
     * @param $password 如果为远程登录，$password需要是已加密的
     * @param  unknown_type $agent
     * @param  unknown_type $server
     * @param  unknown_type $ip
     * @return string
     */
    public function doLogin($username, $password, $local = true, $agent = null, $server = null, $ip = null, $login_flag = '')
    {
        !$ip && $ip = get_ip();
        $now        = time();

        //如果是中央服本地登录，则判断login_token和验证码
        if ($local) {
            if ($_POST['login_token'] != $this->userSessionGet('login_token')) {
                //return 'can not login'; //不允许登录
            }
            /*if (!$this->userSessionGet('login_verify') && strtolower($_POST['verification_code']) != strtolower($this->userSessionGet('verification_code'))) {
                return 'verification code error'; //验证码错误
            }*/
        }

        //一小时内账号登录错误5次进行封禁账号处理
        if (!$this->failedLoginAccount($username)) {
            return 'fail login many times';
        }
        //一小时内同ip错误登录10次以上进行封ip处理
        if (!$this->failedLogin($ip)) {
            return 'fail login many times';
        }

        if ($login_flag) {
            $user = $this->getUserByNameAndFlag($username, $login_flag);
        } else {
            $password = $local ? $this->secretPassword($password) : $password;
			//var_dump($password);exit;
            $user     = $this->getUserByNameAndPass($username, $password);
        }

        if (empty($user)) {
            //用户名密码错误
            $this->userFailedLogin($username, $password, $ip);

            return 'user error';
        }

        if ($user['status'] != USER_STATUS_NORMAL) {
//账号状态异常

            return 'status error';
        }
        if ($user['expiration'] > 0 && $user['expiration'] < $now) {
            return 'expirate'; //账号过期
        }

        if ($local) {
            $this->setLoginSession($user);
            //更新用户最后登录时间和ip
            $this->updateUserLoginData($user['uid'], $ip);
        }

        return 'success'; //登录成功
    }

    public function failedLoginAccount($account = null, $times = 5)
    {
        $now       = time();
        $failTimes = $this->failedLoginModel->count("`created` >" . ($now - 3600) . " AND `username`='{$account}'");
        if ($failTimes >= $times) {
            return false;
        }

        return true;
    }

    //距离上次修改密码的时间
    public function distanceLastChangePassword($user)
    {
        $now                  = time();
        $user_data            = unserialize($user['data']);
        $change_password_time = $user_data['change_password_time'] ? $user_data['change_password_time'] : $user['created'];
        $days                 = intval(($now - $change_password_time) / 86400);

        return $days;
    }

    public function updateUserLoginData($uid, $ip)
    {
        $now  = time();
        $data = array(
            'uid'             => $uid,
            'last_login_time' => $now,
            'last_login_ip'   => $ip,
        );
        $this->userUpdate($data);
    }

    //登录成功后设置session
    public function setLoginSession($user)
    {
        $now = time();
        $this->userSessionSet('uid', $user['uid']);
        $this->userSessionSet('username', $user['username']);
        $this->userSessionSet('groupid', $user['groupid']);
        $this->userSessionSet('admin', true);
        $this->userSessionSet('login_time', $now);

        $groupid                   = $user['groupid'];
        $group                     = (new UsergroupAction())->groupGetOne($groupid);
        $_SESSION['group_channel'] = $group['channel'];
    }

    public function getUserByNameAndPass($username, $password)
    {
        $where = array(
            array(
                'username', $username,
            ),
            array(
                'password', $password,
            ),
        );

        return $this->userModel->getOne($where);
    }

    public function getUserByNameAndFlag($username, $flag)
    {
        return $this->userModel->getOne(array('username' => $username, 'login_flag' => $flag));
    }

    public function getUserByName($username)
    {
        $where = array(
            array(
                'username', $username,
            ),
        );

        return $this->userModel->getOne($where);
    }

    public function getUserByUid($uid)
    {
        return $this->userModel->getOneById($uid);
    }

    public function validPassword($password)
    {
        if (strlen($password) < PASSWORD_LENGTH) {
            return -11; //密码长度小于PASSWORD_LENGTH位
        }
        if (!preg_match("/\d+/", $password) || !preg_match("/[a-zA-Z]+/", $password)) {
            return -12; //密码必须同时包含数字和字母
        }

        return 1;
    }

    public function addUser($data)
    {
        $now = time();
        if (!preg_match("/^[a-z|\d|_]{3,14}$/", $data['username'])) {
            return array('code' => -6);
        }

        $data['expiration'] = strtotime($data['expiration']);
        if ($data['expiration'] < $now) {
            return array('code' => -5);
        }

        $data['username'] = trim($data['username']);
        $user             = $this->getUserByName($data['username']);

        if ($user) {
            //将超级管理员的组别重置
            if ($data['groupid'] == 1 && $user['groupid'] != 1) {
                $data_t = array(
                    'uid'     => $user['uid'],
                    'groupid' => 1,
                );
                $this->userUpdate($data_t);
            }

            return array('code' => -2); //已存在的用户名
        }

        $randomPassword   = $this->_randomPassword();
        $data['password'] = $randomPassword['secret'];
        $data['status']   = USER_STATUS_NORMAL;
        $data['created']  = $now;
        $result           = $this->userModel->insert($data);

        return $result ? array('code' => 1, 'msg' => $randomPassword['str']) : array('code' => -1); //成功：失败
    }

    public function _randomPassword()
    {
        $str    = create_rand_code(12);
        $secret = $this->secretPassword($str);

        return array('str' => $str, 'secret' => $secret);
    }

    public function userGetList()
    {
        $userList   = $this->userModel->getList();
        $groupNames = $this->usergroupGetNames();
        foreach ($userList as $k => $user) {
            $user['agent_flag'] == -1 and $user['agent_flag']     = '-';
            $user['channel_flag'] == -1 and $user['channel_flag'] = '-';
            $user['groupname']                                    = $groupNames[$user['groupid']];
            $user['created_format']                               = $user['created'] ? date('Y-m-d', $user['created']) : '-';
            $user['last_login_time_format']                       = $user['last_login_time'] ? date('Y-m-d', $user['last_login_time']) : '-';
            $user['expiration_format']                            = $user['expiration'] ? date('Y-m-d', $user['expiration']) : '-';
            if ($user['status'] != USER_STATUS_NORMAL || ($user['expiration'] > 0 && $user['expiration'] < time())) {
                $user['status_msg'] = '异常';
            } else {
                $user['status_msg'] = '正常';
            }

            $userList[$k] = $user;
        }

        return $userList;
    }

    public function usergroupGetNames()
    {
        $groupList = $this->userGroupModel->getList();
        $data      = array();
        foreach ($groupList as $g) {
            $data[$g['groupid']] = $g['name'];

        }

        return $data;
    }

    public function userDelete($uid)
    {
        return $this->userModel->deleteById($uid);
    }

    public function userUpdate($data)
    {
        return $this->userModel->update($data);
    }

    public function delayAccount($uid, $days = 30)
    {
        $sql = "UPDATE " . MYSQL_PREFIX . "user SET expiration = expiration + 86400* {$days}
            WHERE uid = {$uid}";
        $this->userModel->query($sql);
    }

    public function secretPassword($password)
    {
        return md5(SALT . $password);
    }
    public function makeLoginToken()
    {
        return $this->userSessionSet('login_token', random(10));
    }

    public function changePassword($data, $uid = null)
    {
        $now = time();
        if ($data['password'] != $data['password2']) {
            return array('code' => -3);
        }
//两次密码输入不同

        $validPassword = $this->validPassword($data['password']);
        if ($validPassword <= 0) {
            return array('code' => $validPassword);
        }

        if (!$uid) {
            $uid = $this->userSessionGet('uid');
        }
        $user = $this->getUserByUid($uid);
        if ($user['password'] != $this->secretPassword($data['old_password'])) {
            return array('code' => -2);
        }
        $user_data        = unserialize($user['data']);
        $password_history = $user_data['password_history'] ? $user_data['password_history'] : array();
        //判断密码是否在最近5次以内使用过的
        if (!empty($password_history)) {
            foreach ($password_history as $tmp) {
                $arr = explode('|', $tmp);
                if ($this->secretPassword($data['password']) == $arr[0]) {
                    return array('code' => -4, 'msg' => date('Y-m-d H:i:s', $arr[1]));
                }
            }
        }

        //记录密码历史
        if (count($password_history) >= 5) {
            array_shift($password_history);
        }
        array_push($password_history, $this->secretPassword($data['password']) . '|' . $now);
        $user_data['password_history'] = $password_history;

        //记录最后修改密码时间
        $user_data['change_password_time'] = $now;
        $data                              = array(
            'uid'      => $uid,
            'password' => $this->secretPassword($data['password']),
            'data'     => serialize($user_data),
        );

        return array('code' => $this->userUpdate($data));
    }

    public function setLoginFlag($uid)
    {
        $login_flag = create_rand_code(16);
        $user       = array('uid' => $uid, 'login_flag' => $login_flag);

        $this->userModel->update($user);

        return $login_flag;
    }
}
