<?php
!defined('IN_WEB') && exit('Access Denied');

class UsergroupAction {
    function __construct () {
        $this->userGroupModel = new DbcenterModel('user_group');
        
    }

    function groupGetList(){
        return $this->userGroupModel->getList();
    }
    
    function usergroupAdd($data){
        $data['created'] = time();
        return $this->userGroupModel->insert($data);
    }
    
    function usergroupUpdate($data){
        return $this->userGroupModel->update($data);
    }
    
    function groupDelete($groupid){
        return $this->userGroupModel->deleteById($groupid);
    }
    
    function groupGetOne($groupid){
        $usergroup = $this->userGroupModel->getOneById($groupid);
        return $this->renderUsergroup($usergroup);
    }
    
    function renderUsergroup($usergroup){       
        
        $usergroup['center_permit_data'] = unserialize($usergroup['center_permit'])? unserialize($usergroup['center_permit']): array();
        $usergroup['single_permit_data'] = unserialize($usergroup['single_permit'])? unserialize($usergroup['single_permit']): array();
        
        $usergroup['center_modules_data'] = unserialize($usergroup['center_modules'])? unserialize($usergroup['center_modules']): array();
        $usergroup['single_modules_data'] = unserialize($usergroup['single_modules'])? unserialize($usergroup['single_modules']): array();
        
        $usergroup['agent_data'] = unserialize($usergroup['agent'])? unserialize($usergroup['agent']): array();
        $usergroup['server_data'] = unserialize($usergroup['server'])? unserialize($usergroup['server']): array();
        
        $usergroup['allow_ip_data'] = unserialize($usergroup['allow_ip'])? unserialize($usergroup['allow_ip']): array();
        $usergroup['allow_ip_data_show'] = implode("\n", $usergroup['allow_ip_data']);

        $usergroup['channel'] = $usergroup['channel'] == 'all' ? 'all' : json_decode($usergroup['channel'], true);
        return $usergroup;
    }
    
    function serverPurview($group, $agent, $server){
        if($group && (is_array($group['agent_data']) && in_array('all', $group['agent_data'])
                || is_array($group['agent_data']) &&  in_array($agent, $group['agent_data'])
                || $server && is_array($group['server_data']) &&  in_array($server, $group['server_data'][$agent]))){
            return true;
        }else{
            //没有访问权限
            return false;
        }
    }
    
    
    function getAgentByGroupId($groupid){
        $agent = array();
        $group = $this->groupGetOne($groupid);
        $agents = array_keys($group['server_data']);
        
        
        $agentAll = array();
        if(in_array('all', $group['agent_data'])){
        	$agentAction = new AgentAction();
        	$agentList = $agentAction->agentList('flag');
        	foreach($agentList as $tmp_agent){
        		$agentAll[] =  $tmp_agent['flag'];
        	}
        }
        
        return array_unique(array_merge($group['agent_data'], $agents, $agentAll));
    }
    
    function getServerByGroupId($groupid, $agent){
        $group = $this->groupGetOne($groupid);
        $serverAction = new ServerAction();
        
        if(in_array('all', $group['agent_data']) || in_array($agent, $group['agent_data'])){
            $data = $serverAction->server_listByAgent($agent);
        }else{
            $flags = $group['server_data'][$agent];
            $data = $serverAction->server_listByFlags($flags);
        }
        
        $servers = array();
        foreach($data as $s){
            $servers[] = array(
            		'name' => $s['name'],
                    'flag' => $s['flag'],
                    'domain' => $s['domain'],
                    'sync' => $s['sync'],
                    's_type' => $s['s_type'],
                    'opened' => $s['opened'],
                    );
        }
        return $servers;
    }
    
    function permitUpdate($groupid, $type, $permits, $all=false){
        $permitAction = new PermitAction();
        $permitList = $permitAction->permitListByIds($permits);
        $modules = $all? array("all"): array();
        foreach($permitList as $row){
            $modules = $row['modules'] ? array_merge($modules, unserialize($row['modules'])) : $modules;
        }
        
        $data = array(
                "groupid" => $groupid,
                "{$type}_permit" => serialize($permits),
                "{$type}_modules" => serialize($modules),
                );
        return $this->usergroupUpdate($data);
        
    }
    
    function updateAllGroup(){
        $permitAction = new PermitAction();
        $groupList = $this->groupGetList();
        foreach($groupList as $group){
            $data['groupid'] = $group['groupid'];
            foreach(array('center', 'single') as $type){
                $permits = unserialize($group[$type.'_permit']);
                $modules = unserialize($group[$type.'_modules']);
                $new_modules = $modules ? in_array('all', $modules) ? array("all") : array() : array();
                $permitList = $permitAction->permitListByIds($permits);
                foreach($permitList as $row){
                    $new_modules = $row['modules'] ? array_merge($new_modules, unserialize($row['modules'])) : $new_modules;
                }
                $data["{$type}_modules"] = serialize($new_modules);
            }
            $result = $this->usergroupUpdate($data);
            $result && $msg_arr[] = $group['name'];
        }
        return "更新用户组:".implode(', ', $msg_arr);
    }
}
















