<?php
if (! defined('IN_WEB')) {
	exit('Access Denied');
}

class UserstatAction {
	
	public function __construct () {
		$this->UserstatDb = new DbcenterModel('user_stat');
	}
	
	public function getStatList($agent, $server, $sTime, $eTime, $limit){
		$aaData['sEcho'] = intval($_GET['sEcho']);
		$where = " u.agent = '{$agent}' and u.server = '{$server}' ";
		$cwhere  = " agent = '{$agent}' and server = '{$server}' ";
		if($sTime){
			$where .= " and u.time >= {$sTime} ";
			$cwhere .= " and time >= {$sTime} ";
		}
		if($eTime){
			$where .= " and u.time <= {$eTime} ";
			$cwhere .= " and time <= {$eTime} ";
		}
	
		$sql = "SELECT 
				u.time,u.create_role,u.old_role_num,u.active_role_num,u.loyal_role_num,u.all_role_num,u.all_role_num_yesterday,
				l.next_day_loss_roles_num,l.twodaysago_role_num,l.loss_role_num,l.all_role_num
				FROM ".MYSQL_PREFIX."user_stat as u
				LEFT JOIN ".MYSQL_PREFIX."level_loss AS l ON l.time = u.time AND l.agent = u.agent AND l.server = u.server
				WHERE {$where}
				ORDER BY u.time DESC {$limit}";
		$res = $this->UserstatDb->query($sql);
		$aaData['aaData'] = array();
		$i = 0;
		while($row = $res->fetch_assoc()){
			$aaData['aaData'][$i][] = date('Y-m-d(D)', $row['time']);
			$aaData['aaData'][$i][] = $row['create_role'];
			$aaData['aaData'][$i][] = $row['old_role_num'];
			$aaData['aaData'][$i][] = $row['active_role_num'];
			$aaData['aaData'][$i][] = $row['loyal_role_num'];
			$aaData['aaData'][$i][] = $row['all_role_num'];
			$aaData['aaData'][$i][] = round($row['next_day_loss_roles_num']/$row['twodaysago_role_num']*100, 2).'%';
			$aaData['aaData'][$i][] = $row['all_role_num_yesterday'] ? round(($row['all_role_num_yesterday'] - $row['old_role_num'])/$row['all_role_num_yesterday']*100, 2).'%' : '-';
			$i++;
		}
		$i = count($aaData['aaData']);
		$aaData['iTotalRecords'] = $i;
		$aaData["iTotalDisplayRecords"] = $this->UserstatDb->count($cwhere);
		return json_encode($aaData);
	}
}