<?php
if (! defined('IN_WEB')) {
    exit('Access Denied');
}

class VariableAction
{
    public function __construct()
    {
        $this->variableModel = new DbCenterModel('variable');
    }

    public function variableGet($name)
    {
        $value = $this->variableModel->getOne("name='{$name}'");

        return unserialize($value['value']);
    }

    public function variableSet($name, $value)
    {
        $data = array(
                "name" => $name,
                "value" => serialize($value),
                );

        return $this->variableModel->replace($data);
    }

    public function variableDelete($name)
    {
        return $this->variableModel->delete("name='{$name}'");
    }

}
