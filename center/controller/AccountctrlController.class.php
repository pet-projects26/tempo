<?php
!defined('IN_WEB') && exit('Access Denied');

class AccountctrlController extends Controller{

    public function __construct(){
        parent::__construct();
    }
    
    public function summary_index(){
    	$tabs = array(
    			array('title' => '包数据汇总', 'url' => 'admin.php?&ctrl=accountctrl&act=summary_package'),
    			//array('title' => '帐号数据汇总', 'url' => 'admin.php?&ctrl=stat&act=summary_account'),
    	);
    	$this->tabs($tabs);
    }
    
    public function summary_package(){
    	$header  = '说明：<br>';
    	$header .= '1. <span style="color:red">注册数 指成功创角的帐号数（未创角的账号数无法统计入内)</span><br>';
    	$header .= '4. 活跃数 指当天登录游戏时长总和大于等于30分钟的角色数量<br>';
    	$header .= '6. 充值人数 指当天充值成功的角色数量<br>';
    	$header .= '18. 次留和3留 指次日留存率和三日留存率 (之后的7留 ，15留 ，30留同理)<br>';
    	$header .= '19. 三日留存率 = 当天有登录且在三天前也有登录的角色数量 / 总角色数量 <br> ';
    	$header .= '<span style="color:red;">23. 默认是显示最近七天的数据</span><br>';
    
    	$this->setHeader($header);
    	$this->setSearch('日期' , 'range_date');
    	$this->setSearch('' , 'scp' , array(1, 0, 0 , 1));
    	$this->setField('日期' , 100);
    	$this->setFields(array(
    			'包号','注册数' ,'登录数' , '充值人数' , '充值金额' , '次留' , '3留'  , '7留' , '15留' , '30留'
    	));
    	//$this->smarty->assign('select', 'radio');
    	$this->setSource('accountctrl' , 'summary_package_data' , 1);
    	$this->displays();
    }
    
    public function summary_package_data(){
    	$this->setCondition(0 , 'date' , 's.time');
    	$this->setCondition(1 , 'scp','','',array(1,0,1,1));
    	$this->setOrder('s.time');
    	$this->setLimit();
    	$method = $this->setDataExport('summary_package_data' , 'summary_package_export');
    	$this->call('StatModel' , $method , $this->getConditions());
    }

    //新增玩家
    public function newaccount(){
    	$header  = '说明：<br>';
    	$header .= '1. 设备激活:当日新增加的激活设备量。 <br>';
    	$header .= '2. 新增玩家:当日新增加的玩家帐户数。<br>';
    	$this->setHeader($header);
    	
    	$this->setSearch('日期' , 'range_time');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array(
    			'日期' , '激活设备(台)' ,
    			'新增玩家(账号)',
    	));
    	

    	$this->smarty->assign('select','select');
    	$this->setSource('accountctrl' , 'newaccount_data' , 1);
    	$this->displays();
    }
    
    public function newaccount_data(){
    	$this->setCondition(0 , 'time' , 'time');
    	$this->setCondition(1 , 'scp');
    	$this->setOrder('time');
    	$this->setLimit(1000000);
    	$method = $this->setDataExport('newaccount_data' , 'newaccount_export');
    	$this->call('NewaccountModel' , $method , $this->getConditions());
    }
    //玩家留存
    public function remain(){
    	$header  = '说明：<br>';
    	$header .= '1. 新增角色数:当日新增加的角色数。 <br>';
    	$header .= '2. 次留:第二天有登录过的角色的留存。<br>';
    	$header .= '3. 周留:第七天有登录过的角色的留存。<br>';
    	$header .= '4. 月留:第三十天有登录过的角色的留存。<br>';
    	$this->setHeader($header);
    	
    	$this->setSearch('日期' , 'range_time');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array(
    			'日期' , '新增角色数' ,
    			'次留','周留','月留',
    	));
    	
    	$this->smarty->assign('select','select');
    	$this->setSource('accountctrl' , 'remain_data' , 1);
    	$this->displays();
    }
    
    public function remain_data(){
    	$this->setCondition(0 , 'time' , 'created');
    	$this->setCondition(1 , 'scp');
    	$this->setOrder('created');
    	$this->setLimit(1000000);
    	$method = $this->setDataExport('remain_data' , 'remain_export');
    	$this->call('AccountremainModel' , $method , $this->getConditions());
    }
    //设备统计
    public function device(){
    	$header  = '说明：<br>';
    	$header .= '1. 日期:默认最近七天。 <br>';
    	$header .= '2. 活跃玩家:最近七天登录过的玩家。 <br>';
    	$header .= '3. 流失玩家:最近三天没有登录过的玩家。<br>';
    	$this->setHeader($header);
    	
    	$this->setSearch('日期' , 'range_time');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array(
    			'机型' , '玩家数(账户)' ,'百分比',
    			'活跃玩家(账户)' , '百分比' ,'流失玩家(账户)','百分比',
    	));
    	
    	$this->smarty->assign('select','select');
    	$this->setSource('accountctrl' , 'device_data' , 1);
    	$this->displays();
    }
    
    public function device_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit(1000000);
    	$method = $this->setDataExport('device_data' , 'device_export');
    	$this->call('ClientModel' , $method , $this->getConditions());
    }
    //付费率
    public function payRate(){
    	$header  = '说明：<br>';
    	$header .= '1. 日付费率：当日进行付费的玩家占当日活跃玩家的比例。 <br>';
    	$header .= '2. 周付费率：某自然周中进行付费的玩家（排重）占该自然周活跃玩家的比例。 <br>';
    	$header .= '3. 月付费率：某自然月中进行付费的玩家（排重）占该自然月活跃玩家的比例。<br>';
    	$this->setHeader($header);
    	
    	$this->setSearch('日期' , 'range_time');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array(
    			'日期' , '日付费率' ,'周付费率','月付费率' , 
    	));
    	
    	$this->smarty->assign('select','select');
    	$this->setSource('accountctrl' , 'payRate_data' , 1);
    	$this->displays();
    }
    
    public function payRate_data(){
    	
    	$this->setCondition(0 , 'time' , 'time');
    	$this->setCondition(1 , 'scp');
    	$this->setOrder('time');
    	$this->setLimit(1000000);
    	$method = $this->setDataExport('payRate_data' , 'payRate_export');
    	$this->call('PayrolesModel' , $method , $this->getConditions());
    }
    
    //活跃玩家
    public function hyRoles(){
    	$header  = '说明：<br>';
    	$header .= '1. 新玩家(账户):当日新增的玩家数。 <br>';
    	$header .= '2. 老玩家(账户):除当日新增的玩家。 <br>';
    	$header .= '3. WAU:当日的最近一周（含当日的倒推7日）活跃玩家。(角色) <br>';
    	$header .= '4. MAU:当日的最近一月（含当日的倒退30日）活跃玩家。(角色)<br>';
    	$this->setHeader($header);
    	 
    	$this->setSearch('日期' , 'range_time');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array(
    			'日期' , '新玩家(账户)' ,'老玩家(账户)','总计' ,'WAU' ,'MAU' ,
    	));
    	 
    	$this->smarty->assign('select','select');
    	$this->setSource('accountctrl' , 'hyRoles_data' , 1);
    	$this->displays();
    }
    
    public function hyRoles_data(){
    	 
    	$this->setCondition(0 , 'time' , 'time');
    	$this->setCondition(1 , 'scp');
    	$this->setOrder('time');
    	$this->setLimit(1000000);
    	$method = $this->setDataExport('hyRoles_data' , 'hyRoles_export');
    	$this->call('HyaccountModel' , $method , $this->getConditions());
    }
}