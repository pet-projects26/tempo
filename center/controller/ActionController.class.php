<?php
!defined('IN_WEB') && exit('Access Denied');

/** 功能开关 */
class ActionController extends Controller
{
    private $groupAccs = array();

    use tChannelGroupServerAccess;

    public function __construct(){
        parent::__construct();
        $groupAcce = self::getChannelGroupByTrait();
        $this->groupAccs = array_column($groupAcce,'id');
    }
    /**
     * 显示功能列表 type 1
     * @version 2017-09-18
     */
    public function index()
    {
        $tabs = array(
            array('title' => '关闭列表', 'url' => 'admin.php?&ctrl=action&act=lst'),
            array('title' => '功能开关', 'url' => 'admin.php?&ctrl=action&act=operate'),
            /*array('title' => '服务器详情','url' => 'admin.php?&ctrl=action&act=action_hist')*/
        );

        $this->tabs($tabs);
    }

    public function lst()
    {
        $this->setSearch('功能名称');
        $this->setSearch('状态', 'select', array(3 => '关闭', 2 => '开启'));
        //$this->setSearch('' , 'scp' , array(1 , 0 , 0 , 0));
        $this->setSearch('渠道组');
        $this->setSearch('操作时间', 'range_date');
        $this->setField('选择', 30);
        $this->setFields(array('ID', '功能名称', '功能状态', '渠道组', '操作时间', '服务器', '操作'));
        $js = <<<END
        //详情
    $(document).ready(function(){
        $("#checkall").change(function(){
            if($(this).prop('checked')){
                $(".action_list").attr('checked',true);
            }else{
                $(".action_list").attr('checked',false);
            }
        });
    })
        function checkAll(){
            var action_list = [];
            var channel_group = [];
            $(".action_list").each(function(){
                if($(this).prop('checked')){
                    action_list.push($(this).val());
                    channel_group.push($(this).attr('data-channel'))
                }
            })
            if(action_list.length == 0){
                alert('请选择要开启的功能！');
                return false;
            }
            if(channel_group.length == 0){
                alert('请选择需要开启功能的渠道组！');
                return false;
            }
            console.log(channel_group);
            $.ajax({
                type:'POST',
                url:'admin.php?&ctrl=action&act=handel',
                dataType: 'JSON',
                data:{
                    'channel_group':channel_group,
                    'action':action_list,
                    'status':2,
                },
                beforeSend:function(){
                    alert('确定要开启所勾选功能？');
                },
                success:function(data){
                console.log(data);
                   $.dialog({
                     'title':data.title,
                     'max':false,
                     'min':false,
                     'content':data.html
                   })
                },
                error:function(data){
                     //$.dialog.tips(data);
					 \$dialog({
					    'title':'错误提示',
					    'max':false,
					    'min':false,
					    'content':data
					 });
                }
            })
        }
        //详情 转义字符
        function get_details(key,cid){

            if(!key){
                alert('无法获取功能开关！');
                return false;
            }

            if(!cid){
                alert('无法获取渠道组！')
            }

            $.ajax({
                type:'POST',
                url:'admin.php?&ctrl=action&act=get_details',
                dataType: 'JSON',
                data:{
                    'group_id':cid,
                    'key':key,
                },
                success:function(data){
                    console.log(data);
                    $.dialog({
                        'title':data.title,
                        'max':false,
                        'min':false,
                        'content':data.html
                    })
                },
                error:function(data){
                    console.log(data)
                }
            })
        }
        //编辑
        function edit(key,cid){
            if(!key){
                alert('获取功能ID失败！');
            }

            if(!cid){
                alert('获取渠道组失败！')
            }
            \$tabs.tabs('select',1);
            \$tabs.tabs('url',1,'admin.php?&ctrl=action&act=edit&key='+key+'&cid='+cid+'')
            \$tabs.tabs('load',1);
            \$tabs.tabs('url',1,'admin.php?&ctrl=action&act=edit&key='+key+'&cid='+cid+'')
        }
        //复制
        function copy(key){
            if(!key){
                alert('复制失败！');
            }
            \$tabs.tabs('select',1);
            \$tabs.tabs('url',1,'admin.php?&ctrl=action&act=copy&key='+key+'');
            \$tabs.tabs('load',1);
            \$tabs.tabs('url',1,'admin.php?&ctrl=action&act=copy&key='+key+'');
        }
END;
        $header = '<div class="search_float" style="margin-top: 24px">
						<input type="checkbox" id="checkall" class="checkbox"> <span>全选</span>
						<input id="tongbu"  type="button" class=" gbutton" value="开启" onclick="checkAll()">
					</div>';
        $this->setJs($js);
        $this->setSource('action', 'get_lst');
        $this->setHeader($header);
        $this->smarty->assign('select', 'radio');
        $this->displays();
    }

    public function get_lst()
    {
        $this->setCondition(0, 'like', 'act.mark');
        $this->setCondition(1, 'eq', 'act.status');
        //$this->setCondition(3,'scp','','',array(1,0,0,0));
        $this->setCondition(2, 'like', 'cng.name');
        $this->setCondition(3, 'date', 'act.last_modified_time');
        $this->setOrder('act.last_modified_time');
        $this->setLimit();

        $this->call('ActionListModel', 'get_list', $this->getConditions());
    }

    /**
     * 功能开关操作界面
     */
    public function operate()
    {
        //服务器，渠道组
        $actionListModel = new ActionListModel();
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $server = (new ServerModel())->getServerByTrait();
        $actionList = $actionListModel->action_config();
        $actionList = json_decode($actionList, true);
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('action_list', $actionList);
        $this->smarty->assign('servers', $server);
        $this->smarty->display('action/operate.tpl');
    }

    /**
     * 功能开关编辑界面
    */
    public function edit(){
        $channelGroupId = $this->getParam('cid');
        $actionKey = $this->getParam('key');

        $actionListModel = new ActionListModel();
        $ret = $actionListModel->getActionMsg($channelGroupId,$actionKey,array('server','status'));
        $actionLst = json_decode($actionListModel->action_config(),true);
        $serverMsg = json_decode($ret['server'],true);
        $servers = array_unique(array_keys($serverMsg));
        /*foreach($servers as &$ser){
            $ser = '\''.$ser.'\'';
        }*/
        $server = (new ServerModel())->getServerByTrait();
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $action_list = array($actionKey=>$actionLst[$actionKey]);
        $this->smarty->assign('data',array('agent'=>array($channelGroupId),'server'=>$servers));
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('action_key',$actionKey);
        $this->smarty->assign('servers', $server);
        $this->smarty->assign('status',$ret['status']);
        $this->smarty->assign('action_list',$action_list);
        $this->smarty->display('action/edit.tpl');
    }


    /**
     * 功能操作
     * @version 2017-09-19
     */
    public function handel()
    {
        $actionkey = $this->getParam('action', array());
        $unviewkey = $this->getParam('unview_key','');
        $type = $this->getParam('status');
        $servers = $this->getParam('server');
        $servers = empty($servers[0])?array():$servers;
        $channelGroupId = $this->getParam('channel_group');
        $channelGroupId = empty($channelGroupId[0])?$this->groupAccs:$channelGroupId;
        $serverModel = new ServerModel();
        if (!isset($actionkey) || !is_array($actionkey) || !isset($type) || !in_array($type, array(2, 3))) {
            die(json_encode(['msg' => '参数错误', 'code' => 0]));
        }
        $msg = (new ActionListModel())->action_config();
        $msg = json_decode($msg, true);
        if (empty($msg) || !is_array($msg)) {
            die(json_encode(['code' => 0, 'msg' => '获取配置文件失败']));
        }

        $diff = @array_diff($actionkey, array_keys($msg));
        if (!empty($diff)) {
            $diff = join(',', $diff);
            die(json_encode(['code', 'msg' => $diff . "，功能开关不存在"]));
        }
        $servers = $serverModel->getServerByServerAndChannelGroup($servers, $channelGroupId);//获取所有服务器列表
        if (empty($servers)) die(json_encode(['code' => 0, 'msg' => '渠道组服务器不存在']));
        $server_id = array_keys($servers);
        foreach($server_id as &$val){
            $val = '\''.$val.'\'';
        }
        $sql = "select `server_id` from ny_server where `server_id` in (".join(',',$server_id).")";
        $res = $serverModel->query($sql);
        //$res = $serverModel->getGm(array(), array_keys($servers));//
        $serverRes = [];
        foreach ($res as $row) {
            $serverRes[$row['server_id']] = 1;
        }
        !empty($unviewkey) && array_push($actionkey,$unviewkey);
        $actionkey = array_unique($actionkey);
        $url = Helper::v2();
        $res = Helper::rpc($url, $serverRes, 'action', array('type' => (int)$type, 'key' => $actionkey));
        $tmp = $serverModel->getServerBygroupId($channelGroupId);
        $backMsg = array();
        foreach($tmp as $key=>$val){
           foreach($res as $k=>$msg){
               if(in_array($k,$val)){
                   $backMsg[$key][$k] = ($msg == 1)?1:0;//渠道:服务器:状态（0则为成功）
                   //记录日志
               }
           }
        }
        $title = "执行结果";
        $html = "";
        $sInfo = $serverModel->getRows(array('server_id,name'));
        $info = [];
        foreach($sInfo as $serInfo){
            $info[$serInfo['server_id']] = $serInfo;
        }
        if (!is_array($res)) {
            $html = $res;
        } else {
            $temp = [];
            $sta = ($type == 2) ? '开启' : '关闭';
            foreach ($res as $sid => $msg) {
                $temp[$sid] = $info[$sid]['name']." ".$sta . ($msg == 1 ? '<span style="">成功</span>' : '<span style="color: #ee0000;font-weight: 600">失败</span>');//服务器:状态,不予保存
            }
            //记录操作结果
            $result = (new ActionListModel())->setHandelRecord($actionkey, $type,$backMsg);
            foreach ($temp as $key => $val) {
                $html .= '<span>' .$key.' : '. $val . '</span><br/>';
            }
        }
        exit(json_encode(array('title' => $title, 'html' => $html),JSON_UNESCAPED_UNICODE));

    }

    /**
     * 各服务器功能开关详情
     * @mark 拟定需求，查询各服务器功能开关情况
     * @version 2017-09-29
    */
    public function action_hist(){
        //获取所有服务器列表
        $serverModel = new ServerModel();
        $servers = $serverModel->getServer(array());
        $server_id = $this->getParam('server_id');
        if($server_id){
            $serverIds = [];
            foreach($servers as $info){
                $serverIds = $info['server_id'];
            }
            if(empty($serverIds) && !in_array($server_id,$serverIds)){
                $var = array('code'=>0,'msg'=>'服务器不存在');
                exit(json_encode($var));
            }
            $serverMsg = (new ServerconfigModel())->getConfig($server_id,array('server_id','api_url','gm_port'));
            $res = $serverModel->rpcCall($serverMsg['api_url'],$serverMsg['gm_port'],'action',array('type'=>1));
            if(!$res){
                exit(json_encode(array()));
            }

            $res = json_decode($res,true);
            //解析功能开关
            $actionLst = (new ActionListModel())->action_config();
            $actionLst = json_decode($actionLst,true);
            $temp = [];
            foreach($actionLst as $key=>$row){
                if(in_array($key,$res)){
                    $temp[$key][] = array('name'=>$row,'access'=>1);
                }else{
                    $temp[$key][] = array('name'=>$row,'access'=>0);
                }
            }
            $this->smarty->assign('row',$temp);//返回功能开关明细
        }
        foreach($servers as &$val){
            $val['name'] = $serverModel->formatServerName($val);
        }
        $this->smarty->assign('servers',$servers);
        $this->smarty->display('action/action_hist.tpl');
    }

    /**
     * 获取详情
     */
    public function get_detail()
    {
        $key = $this->getParam('key');
        $channel_group_id = $this->getParam('group_id');
        $actionListModel = new ActionListModel();
        $serverModel = new ServerModel();
        $title = "功能详情";
        $actionConf = $actionListModel->action_config();
        $actionConf = json_decode($actionConf, true);

        if (!in_array($key, $actionConf)) {
            exit(json_encode(array('title' => $title, 'html' => '<span style="color: #cc1e2f">获取失败</span>')));
        }
        $servers = $serverModel->getServerByServerAndChannelGroup('', $channel_group_id);
        if (empty($servers)) exit(json_encode(array('title' => $title, 'html' => '<span style="color: #cc1e2f">获取服务器列表失败</span>')));

        $res = $serverModel->getGm(array(), array_keys($servers));

        $serverRes = [];
        foreach ($res as $row) {
            $serverRes[$row['server_id']] = 1;
        }
        $url = Helper::v2();
        $res = Helper::rpc($url, $serverRes, 'action', array('type' => 4, 'key' => $key));

        $row = $serverModel->getServer(array_keys($res));
        $serRow = [];
        foreach ($row as $key => $val) {
            $val['server_name'] = $serverModel->formatServerName($val);
            $serRow[$val['server_id']] = $val;
        }
        $html = '';
        foreach ($res as $key => $msg) {
            $msg = $msg == 1 ? '开启' : '关闭';
            $html .= $serRow[$key]['server_name'] . ' ' . $msg;
        }

        exit(json_encode(array('title' => $title, 'html' => $html)));
    }

    /**
     * 获取详情
     * @mark 显示功能开关操作历史
    */
    public function get_details(){
        $groupId = $this->getParam('group_id');
        $actionKey = $this->getParam('key');
        $actionLstModel = new ActionListModel();
        $serverModel = new ServerModel();
        $header = "操作详情";
        if(!$groupId || !$actionKey){
            $html = '获取信息失败';
            exit(json_encode(array('title'=>$header,'html'=>$html)));
        }

        $conditions['WHERE']['name'] = $actionKey;
        $conditions['WHERE']['group_id'] = $groupId;

        $row = $actionLstModel->getRow('*',$conditions['WHERE']);
        $servers = $serverModel->getServer(array(),array('server_id,name'));
        $serInfo = [];
        foreach($servers as $info){
            $serInfo[$info['server_id']] = $info;
        }
        $html = '功能：<span style="font-weight: 600;color: #ca1b36">'.$row['mark'].'</span><br/>';

        $serMsg = json_decode($row['server'],true);
        $way = $row['status'] == 2?'开启':($row['status'] == 3?'关闭':'未知操作');
        foreach($serMsg as $key=>&$val){
            $val = $serInfo[$key]['name']." ". $way." ".($val==1?'<span style="color: #3c763d">成功</span>':'<span style="color: #cc1e2f">失败</span>');
        }

        foreach($serMsg as $msg){
            $html .= '<span>'.$msg.'</span></br>';
        }

        exit(json_encode(array('title'=>$header,'html'=>$html)));

    }

    /**
     * 复制,产生新的ID
     * @mark 功能必要性在哪
    */
    public function copy(){
        $actionModel = new ActionListModel();
        $actionKey = $this->getParam('key');
        $action_conf = json_decode($actionModel->action_config(),true);
        $action_list = array($actionKey=>$action_conf[$actionKey]);
        $server = (new ServerModel())->getServerByTrait();
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->assign('action_list',$action_list);
        $this->smarty->display('action/edit.tpl');
    }
}

?>