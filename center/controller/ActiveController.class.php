<?php
!defined('IN_WEB') && exit('Access Denied');

class ActiveController extends Controller{
    //每小时统计数据
    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日连接上游戏服务器的玩家数。 <br>';
        $header .= '登录次数：该日玩家的总登录次数（不用去重）<br>';
        $header .= '人均登录次数 =  登录次数 / 活跃人数 （小数点后2位，四舍五入）<br>';
        $header .= '人均在线时长 = 玩家总在线时长 / 活跃人数 （小数点后2位，四舍五入）<br>';
        $header .= '新增用户数：即单位时间内新增连接上游戏服务器人数。每个账号从连接上游戏服务器成功则标记一次，标记后该账号在本服的后续跳转不再算为新增跳转。<br>';
        $header .= '新增用户人均登录次数 = 新增用户总登录次数 / 新增用户数   （小数点后2位，四舍五入）<br>';
        $header .= '新增用户人均在线时长 = 新增用户玩家总在线时长 / 新增用户数 （小数点后2位，四舍五入）<br>';
        $header .= '老玩家 = 该日连接上游戏服务器的老玩家人数     （老玩家定义：非本日新增创角的玩家）<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '登录次数' , '人均登录次数', '人均在线时长', '新增用户数', '新增用户人均登录次数', '新增用户人均在线时长', '老玩家数', '老玩家人均登录次数', '老玩家人均在线时长'));
        $this->setSource('Active' , 'index_data' , 1);
        $this->setLimit('24');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');

        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('active_data' , 'active_export');
        $this->call('ActiveModel' , $method , $this->getConditions());
    }
}