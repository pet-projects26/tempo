<?php
!defined('IN_WEB') && exit('Access Denied');

class ActivityController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->am = new ActivityModel();
        $this->g = new ChannelgroupModel();
    }

    /**
     * 活动更新
     * @return [type] [description]
     */
    public function update_activity()
    {
        // $server = $this->am->getGm();
        // $this->smarty->assign('server' , $server);

        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        $this->smarty->display('activity/update_activity.tpl');
    }

    /**
     *  活动
     * @return [type] [description]
     */
    public function update_activity_action()
    {
        $res = $this->am->update_activity_action($_POST);

        die(json_encode(array('msg' => $res)));
    }

    public function index()
    {
        //第一栏菜单
        $tabs = array(
            array('title' => '充值活动', 'url' => 'admin.php?&ctrl=activity&act=activity&type=1'),
            array('title' => '消费活动', 'url' => 'admin.php?&ctrl=activity&act=activity&type=2'),
            array('title' => '活跃活动', 'url' => 'admin.php?&ctrl=activity&act=activity&type=3'),
            array('title' => '活动模版', 'url' => 'admin.php?&ctrl=activity&act=activity&type=4'),
        );
        $this->tabs($tabs);
    }

    public function activity()
    {
        $type = $this->getParam('type');//通过get方式获取第二栏类型

        if ($type != 4) {
            $tabs = array(
                array('title' => '活动列表', 'url' => 'admin.php?&ctrl=activity&act=activitylist&type=' . $type),
                array('title' => '活动配置', 'url' => 'admin.php?&ctrl=activity&act=config&type=' . $type),
                array('title' => '活动审核', 'url' => 'admin.php?&ctrl=activity&act=audit&type=' . $type),
                array('title' => '活动操作', 'url' => 'admin.php?&ctrl=activity&act=edit&type=' . $type),
            );
        } else {
            $tabs = array(
                array('title' => '活动列表', 'url' => 'admin.php?&ctrl=activity&act=model'),
                array('title' => '活动操作', 'url' => 'admin.php?&ctrl=activity&act=edit&type=' . $type),
            );
        }

        $this->tabs($tabs, 'activity');
    }

    //活动列表
    public function activitylist()
    {
        $header = '<div class="search_float">
						<input type="checkbox" id="checkall" class="checkbox"> <span>全选</span>  
						<input id="tongbu"  type="button" class=" gbutton" value="审核" onclick="checkAll()">
					</div>';
        $this->setHeader($header);

        $type = $this->getParam('type');
        $form = CDict::$activity[$type];

        $groups = $this->g->getRows(array('id', 'name'));

        $group = array(
            '' => '-'
        );

        foreach ($groups as $key => $value) {
            $group[$value['id']] = $value['name'];
        }

        $this->setSearch('渠道组', 'select', $group);
        $this->setSearch('活动名称');
        $this->setSearch('活动状态', 'select', array('' => '-', 0 => '待提审', 1 => '已提审', 2 => '已发布', 3 => '被驳回', 4 => '下架'));
        $this->setSearch('活动类型', 'select', $form);
        $this->setSearch('开始时间', 'range_time');
        $this->setSearch('结束时间', 'range_time');
        $this->setSearch('创建人');
        $this->setSearch('审核人');
        $this->setFields(array('ID', '活动名称', '开始时间', '结束时间', '活动状态', '活动类型ID', '活动类型', '渠道组', '创建人', '审核人', '审核时间', '执行状态', '执行结果', '操作'));
        $this->setSource('activity', 'activitylist_data', 0, array('big_act_id' => $type));

        $js = <<< END
		
		  function _model(id){
			  if(confirm("确定要设置为模版吗？")){
				  $.ajax({
					url: 'admin.php?ctrl=activity&act=setmodel',
					type: 'POST',
					dataType: 'JSON',
					data: {'id':id}
				  }).done(function(data){
					  if(data ==1){
						  $.dialog.tips('设置成功');
						  \$tabs.tabs('load' , 0);
					  }else{
						  $.dialog.tips('设置失败,请重试');
					  }
				  });
			  }
			}
			//编辑
            function _edit(id){
                \$tabs.tabs('select' , 3);
                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=activity&act=edit&id=' + id);
                \$tabs.tabs('load' , 3);
                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=activity&act=edit&id=' + id);
            }
			//复制
            function _copy(id){
                \$tabs.tabs('select' , 3);
                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=activity&act=edit&id=' + id + '&copy=1');
                \$tabs.tabs('load' , 3);
                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=activity&act=edit&id=' + id + '&copy=1');
            }
			//删除
			function _del(id){
			  if(confirm("确定要删除吗？")){
				  $.ajax({
					url: 'admin.php?ctrl=activity&act=del',
					type: 'POST',
					dataType: 'JSON',
					data: {'id':id}
				  }).done(function(data){
					  if(data ==1){
						  $.dialog.tips('删除成功');
						  \$tabs.tabs('load' , 0);
					  }else{
						  $.dialog.tips('删除失败,请重试');
					  }
				  });
			  }
			}
		
			//下架
            function _sync(id){
            	if(confirm("你确定要下架么？")){
	                \$tabs.tabs('select' , 3);
	                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=activity&act=offSale&id=' + id);
	                \$tabs.tabs('load' , 3);
	                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=activity&act=offSale&id=' + id);
            	}
            }
			//全选
			var check=1;
			$("#checkall").click(function(){
				 if(check == 1){
					 $(" .sorting_1 :checkbox").attr("checked", true); 
					 check =0; 
				 }else{
					 check=1;	 
					 $(" .sorting_1 :checkbox").attr("checked", false); 
				 } 
			 });
			 //批量审核
			 function checkAll(){
				var id = new Array();
				$('.sorting_1:checked').each(function(){
					id.push($(this).val());	
				});	
				
				if(id  == ''){
					$.dialog.tips('请选择要审核的活动');	return false;			
				}
				_check(id);
			}
			//审核
			function _check(id){
				if(confirm("确定要提交审核吗？")){
				$.ajax({
				  url: 'admin.php?ctrl=activity&act=check',
				  type: 'POST',
				  dataType: 'JSON',
				  data: {'id':id}
			  	}).done(function(data){
					if(data ==1){
						$.dialog.tips('提交成功');
				  		\$tabs.tabs('load' , 0);
					}else{
						$.dialog.tips('提交失败,请重试');
					}
					
			  	})	 
			 } 	 	
			}
			//查看执行结果
			function _remark(id){
                $.ajax({
                    url: 'admin.php?ctrl=activity&act=remark',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json'
                }).done(function(data){
                    $.dialog({
                    	width:400,
                        title: data.title,
                        max: false,
                        min: false,
                        content: data.html
                    });
                });
            }
			
END;
        $this->setJs($js);

        $this->displays(true, 1);
    }

    /**
     * 活动数据
     * @return [type] [description]
     */
    public function activitylist_data()
    {
        $this->setCondition(0, 'find', 'platforms');
        $this->setCondition(1, 'like', 'title');
        $this->setCondition(2, 'eq', 'type');
        $this->setCondition(3, 'eq', 'act_id');
        $this->setCondition(4, 'time', 'start_time');
        $this->setCondition(5, 'time', 'end_time');
        $this->setCondition(6, 'like', 'add_user');
        $this->setCondition(7, 'like', 'check_user');
        $this->setCondition(8, 'eq', 'big_act_id', $this->getParam('big_act_id'));
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $this->call('ActivityModel', 'activitylist_data', $this->getConditions());

    }

    /**
     * 活动配置
     * @return [type] [description]
     */
    public function config()
    {
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->assign('big_act_id', $this->getParam('type'));//获取第一栏菜单类型值
        $this->smarty->display('activity/config.tpl');
    }

    /**
     * 编辐
     */
    public function edit()
    {
        $id = $this->getParam('id');
        if (empty($id)) {
            echo '请选择要编辑的活动';
            die();
        }
        $data = $this->am->getRow('*', array('id' => $id));
        if (empty($data)) {
            echo '查询不到数据';
            die();
        }
        $data['start_time'] = date('Y-m-d H:i:s', $data['start_time']);
        $data['end_time'] = date('Y-m-d H:i:s', $data['end_time']);
        $award = json_decode($data['award'], true);
        $data['award'] = $award;
        if (isset($award['top'])) {
            $data['topLen'] = count(array_keys($award['top']));
        }
        $data['amount'] = $award['amount'];
        $data['grade'] = $award['grade'];
        $data['itemId'] = $award['item_id'];
        $data['itemCount'] = $award['item_count'];
        $data['score_num'] = $award['score_num'];
        $data['career'] = $award['career'];
        isset($award['weight']) ? $data['weight'] = $award['weight'] : '';
        isset($award['config']) ? $data['config'] = $award['config'] : '';
        isset($data['content']) ? $data['content'] = trim($data['content']) : '';
        isset($award['charge_gold']) ? $data['charge_gold'] = $award['charge_gold'] : 0;
        isset($award['vip_level']) ? $data['vip_level'] = $award['vip_level'] : 0;
        $act_id = $data['act_id'];
        $data['act'] = isset(CDict::$activityType[$act_id]) ? CDict::$activityType[$act_id] : $act_id;
        //固定
        $platforms = array_filter(explode(',', $data['platforms']));
        $servers = array_filter(explode(',', $data['servers']));
        //当是复制的时候，强制将 id置为0
        $copy = isset($_GET['copy']) ? $_GET['copy'] : 0;
        if (!empty($copy)) {
            $data['id'] = 0;
        }
        isset($data['list']) && $jifu = $data['list'];

        $this->smarty->assign('row', $data);
        $this->smarty->assign('data', array('agent' => $platforms, 'server' => $servers));
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->assign('big_act_id', $data['big_act_id']);

        function get_max_key($params)
        {
            krsort($params['arr']);
            $params['arr'] = array_keys($params['arr']);
            return isset($params['arr'][0]) ? $params['arr'][0] : 1;
        }

        function get_last_key($params)
        {
            krsort($params['arr']);//数组{"1":"50","2":"100","3":"200"}
            $params['arr'] = array_keys($params['arr']);
            $lastKey = isset($params['arr'][0]) ? intval($params['arr'][0]) + 1 : 1;
            return $lastKey;
        }

        function get_last_key2($params)
        {
            krsort($params['arr']);//数组{"1":"50","2":"100","3":"200"}
            $params['arr'] = array_keys($params['arr']);
            $lastKey = isset($params['arr'][0]) ? intval($params['arr'][0]) + 2 : 1;
            return $lastKey;
        }

        $this->smarty->register_function('getMaxKey', 'get_max_key');
        $this->smarty->register_function('getLastKey', 'get_last_key');
        $this->smarty->register_function('getLastKey2', 'get_last_key2');
        $file = "edit_" . $data['act_id'] . '.tpl';
        $this->smarty->display('activity/' . $file);
    }

    /**
     * [添加活动]
     * @return [] [description]
     */
    public function save()
    {
        $big_act_id = isset($_POST['big_act_id']) ? intval($_POST['big_act_id']) : 0;
        if (empty($big_act_id)) {
            $arr = array('code' => 404, 'msg' => '非法参数');
            die(json_encode($arr));
        }
        $act_id = isset($_POST['act_id']) ? intval($_POST['act_id']) : 0;
        if (empty($act_id)) {
            $arr = array('code' => 404, 'msg' => '请选择活动类型');
            die(json_encode($arr));
        }
        $title = isset($_POST['title']) ? trim($_POST['title']) : '';
        if (empty($title)) {
            $arr = array('code' => 404, 'msg' => '标题不能为空');
            die(json_encode($arr));
        }
        switch ($act_id) {
            case '1004':
                $start_time = isset($_POST['start_time']) ? trim($_POST['start_time']) : '';

                if (empty($start_time)) {
                    $arr = array('code' => 404, 'msg' => '开始时间不能为空');
                    die(json_encode($arr));
                }


                $end_time = isset($_POST['end_time']) ? trim($_POST['end_time']) : '';

                if (empty($end_time)) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能为空');
                    die(json_encode($arr));
                }

                if (strtotime($end_time) <= time()) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于当前时间');
                    die(json_encode($arr));
                }

                if (strtotime($end_time) <= strtotime($start_time)) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于等于开始时间');
                    die(json_encode($arr));
                }

                $grade = isset($_POST['grade']) ? $_POST['grade'] : array();

                if (empty($grade)) {
                    $arr = array('code' => 404, 'msg' => '档次不能为空');
                    die(json_encode($arr));
                }

                $amount = isset($_POST['amount']) ? $_POST['amount'] : array();
                if (empty($amount)) {
                    $arr = array('code' => 404, 'msg' => '领取条件不能为空');
                    die(json_encode($arr));
                }

                $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : array();
                if (empty($item_id)) {
                    $arr = array('code' => 404, 'msg' => '请输入物品ID');
                    die(json_encode($arr));
                }

                $item_count = isset($_POST['item_count']) ? $_POST['item_count'] : array();
                if (empty($item_count)) {
                    $arr = array('code' => '404', 'msg' => '请输入物品数量');
                    die(json_encode($arr));
                }

                break;
            case '1002':
                $amount = isset($_POST['amount']) ? $_POST['amount'] : array();
                if (empty($amount)) {
                    $arr = array('code' => 404, 'msg' => '领取条件不能为空');
                    die(json_encode($arr));
                }

                $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : array();
                if (empty($item_id)) {
                    $arr = array('code' => 404, 'msg' => '请输入物品ID');
                    die(json_encode($arr));
                }

                $item_count = isset($_POST['item_count']) ? $_POST['item_count'] : array();
                if (empty($item_count)) {
                    $arr = array('code' => '404', 'msg' => '请输入物品数量');
                    die(json_encode($arr));
                }

                break;
            case '1008': //烟花
            case '1033': //扭蛋
                $start_time = isset($_POST['start_time']) ? trim($_POST['start_time']) : '';

                if (empty($start_time)) {
                    $arr = array('code' => 404, 'msg' => '开始时间不能为空');
                    die(json_encode($arr));
                }


                $end_time = isset($_POST['end_time']) ? trim($_POST['end_time']) : '';

                if (empty($end_time)) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能为空');
                    die(json_encode($arr));
                }

                if (strtotime($end_time) <= time()) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于当前时间');
                    die(json_encode($arr));
                }

                if (strtotime($end_time) <= strtotime($start_time)) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于等于开始时间');
                    die(json_encode($arr));
                }


                $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : array();
                if (empty($item_id)) {
                    $arr = array('code' => 404, 'msg' => '请输入物品ID');
                    die(json_encode($arr));
                }

                $item_count = isset($_POST['item_count']) ? $_POST['item_count'] : array();
                if (empty($item_count)) {
                    $arr = array('code' => '404', 'msg' => '请输入物品数量');
                    die(json_encode($arr));
                }
                break;
            case '1032':
                $start_time = isset($_POST['start_time']) ? trim($_POST['start_time']) : '';
                if (empty($start_time)) {
                    $arr = array('code' => 404, 'msg' => '开始时间不能为空');
                    die(json_encode($arr));
                }
                $end_time = isset($_POST['end_time']) ? trim($_POST['end_time']) : '';
                if (empty($end_time)) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能为空');
                    die(json_encode($arr, JSON_UNESCAPED_UNICODE));
                }
                if (strtotime($end_time) <= time()) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于当前时间');
                    die(json_encode($arr, JSON_UNESCAPED_UNICODE));
                }
                if (strtotime($start_time) >= strtotime($end_time)) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    die(json_encode($arr, JSON_UNESCAPED_UNICODE));
                }
                //奖励
                $list = isset($_POST['list']) ? $_POST['list'] : array();
                if (empty($list)) {
                    $arr = array('code' => 404, 'msg' => '奖励不能为空');
                    die(json_encode($arr, JSON_UNESCAPED_UNICODE));
                }
                break;
            default:
                # code...
                break;
        }
        $server = isset($_POST['server']) ? $_POST['server'] : '';
        $res = $this->am->save($_POST);
        die(json_encode($res));
        // echo $this->am->lastQuery();
    }

    //活动审核
    public function check()
    {
        $id = $this->getparam('id');
        if (is_array($id)) {
            $id = implode(',', $id);
        }
        $sql = "update ny_activity  set type = 1 where id in ($id)";
        $rs = $this->am->query($sql);
        if ($rs !== false) {
            echo 1;
        } else {
            echo 0;
        }
    }

    /**
     * 下架
     * @return [type] [description]
     */
    public function offSale()
    {
        $id = intval($this->getParam('id'));
        $res = $this->am->offSale($id);
        print_r($res);
        die();
    }

    //审核页面
    public function audit()
    {
        $header = '<div class="search_float" >
					  <input type="checkbox" id="checkall" class="checkbox">全选  
	        		 <input id="agree"  type="button" class=" gbutton" value="发布" onclick="agreeAll()">
	        		  <input id="disagree"  type="button" class=" gbutton" value="驳回" onclick="disagreeAll()">
        		    </div>';// <input id="agree"  type="button" class=" gbutton" value="发布" onclick="agreeAll()">  
        $this->setHeader($header);
        $type = $this->getParam('type');
        $form = CDict::$activity[$type];
        $this->setSearch('活动名称');
        $this->setSearch('活动类型', 'select', $form);
        $this->setSearch('创建时间', 'range_time');
        $this->setFields(array('ID', '活动名称', '开始时间', '结束时间', '活动状态', '活动类型', '渠道组', '创建人', '审核人', '审核时间', '操作'));
        $this->setSource('activity', 'audit_data', 0, array('big_act_id' => $type));
        $js = <<< END
			//全选
			var check=1;
			$("#checkall").click(function(){
				 if(check == 1){
					 $(" .sorting_1 :checkbox").attr("checked", true); 
					 check =0; 
				 }else{
					 check=1;	 
					 $(" .sorting_1 :checkbox").attr("checked", false); 
				 } 
			 });
			 //批量通过
			 function agreeAll(){
				var id = new Array();
				$('.sorting_1:checked').each(function(){
					id.push($(this).val());	
				});	
				
				if(id  == ''){
					$.dialog.tips('请选择需要审核的活动');	return false;			
				}
				_agree(id);
			}
			//通过
			function _agree(id){
				
				if(confirm("确定要通过活动吗？")){
					$.ajax({
					  url: 'admin.php?ctrl=activity&act=agree',
					  type: 'POST',
					  dataType: 'JSON',
					  data: {'id':id}
					}).done(function(data){
						var code = data['code'];

						var msg  = data['msg'];

						var str = '';

						switch (code) {
							case 200: //直接提交
								addActData(id);
								break;
							case 202: //确认后提交
								$.each(msg, function(index, val) {
									str += val;
									str += "\\n";
									str += "\\n";
								});

								if (confirm(str)) { 
									//表单提交
									addActData(id);
								}
								break;
							case 404:
								//找不到服务器
								$.dialog.tips(msg);
							default:
								alert(msg);
								break;
						}
						// $.dialog.tips('提交成功');
						// \$tabs.tabs('load' , 2);
					});	 
			 	} 	 	
			}

			/**
			 * 添加活动
			 * @param {[string]} post [urlencode的字符串]
			 */
			function addActData(id) {
				$.ajax({
					url: 'admin.php?ctrl=activity&act=push',
					type: 'POST',
					dataType: 'JSON',
					data: {'id':id}
				}).done(function(data){
					var code = data['code'];

					var msg  = data['msg'];

					var str = '';
					if (code  == 200) {
						$.each(msg, function(index, val) {
							str += val;
							str += "\\n";
							str += "\\n";
						});
						alert(str);
						\$tabs.tabs('load' , 2);
						return false;
					} else {
						alert(msg);
						return false;
					}
					//$.dialog.tips(data.msg);
				});
			}


			//批量驳回
			function disagreeAll(){
				var id = new Array();
				$('.sorting_1:checked').each(function(){
					id.push($(this).val());	
				});	
				
				if(id  == ''){
					$.dialog.tips('请选择需要审核的活动');	return false;			
				}
				_disagree(id);
			}

			//驳回
			function _disagree(id){
				if(confirm("确定要驳回活动吗？")){
					$.ajax({
					  url: 'admin.php?ctrl=activity&act=disagree',
					  type: 'POST',
					  dataType: 'JSON',
					  data: {'id':id}
					}).done(function(data){
						if(data ==1){
							$.dialog.tips('提交成功');
							\$tabs.tabs('load' , 2);	
						}else{
							$.dialog.tips('提交失败，请重试');	
						}
						
					})	 
			 	} 	
			}
			
END;
        $this->setJs($js);
        $this->displays(true, 1);
    }

    public function audit_data()
    {
        $this->setCondition(0, 'like', 'title');
        $this->setCondition(1, 'eq', 'type');
        $this->setCondition(2, 'time', 'create_time');
        $this->setCondition(3, 'eq', 'big_act_id', $this->getParam('big_act_id'));
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $this->call('ActivityModel', 'audit_data', $this->getConditions());
    }


    //通过申请
    public function agree()
    {
        $id = $this->getparam('id');
        if (is_array($id)) {
            $id = implode(',', $id);
        }
        //@todo 向 mongo 库插入订单
        $id = explode(',', $id);
        foreach ($id as $actId) {
            $res = $this->am->check($actId);

            if (empty($res)) {
                $code = 200;
            } else if ($res == 'empty') {
                $code = 404;
                $res = "未找到合适服务器";
            } else {
                $code = 202;
            }
            die(json_encode(array('code' => $code, 'msg' => $res)));
        }
    }

    /**
     * 推送活动
     * @return [type] [description]
     */
    public function push()
    {

        $id = $this->getParam('id');
        if (empty($id)) {
            die(json_encode(array('code' => 202, 'msg' => '非法请求')));
        }
        if (is_array($id)) {
            foreach ($id as $value) {
                $res1[] = $this->am->push($value);
            }
            foreach ($res1 as $k => $v) {
                foreach ($v as $kk => $vv) {
                    $res[$kk] = $vv;
                }
            }
        } else {
            $res = $this->am->push($id);
        }
        die(json_encode(array('code' => 200, 'msg' => $res)));

    }


    //驳回申请
    public function disagree()
    {
        $id = $this->getparam('id');
        if (is_array($id)) {
            $id = implode(',', $id);
        }
        $username = $_SESSION['username'];
        $sql = "update ny_activity  set type = 3 ,check_user ='{$username}', check_time = " . time() . " where id in ($id)";
        $rs = $this->am->query($sql);
        if ($rs !== false) {
            echo 1;
        } else {
            echo 0;
        }
    }

    //查看执行结果
    public function remark()
    {
        $id = $this->getParam('id');
        $sql = "select remark from ny_activity where id = $id";
        $result = $this->am->query($sql);
        $remark = json_decode($result[0]['remark'], true);
        //$remark = implode(',',$remark);
        /*foreach($remark as $v){
            $rm .= $v."<br>";
        }*/
        $rm = self::resTips($remark);

        $html = ' <span style="line-height:24px;">' . $rm . '</span>';

        echo json_encode(array('title' => '执行结果', 'html' => $html));
    }

    //活动执行结果
    public function activity_result()
    {
        $this->setSearch('活动id');
        $this->setSearch('活动名称');
        $this->setSearch('状态', 'select', array('' => '-', 1 => '更新成功', 2 => '配置不存在', 3 => '写入mongo库失败', 4 => '未知错误', 5 => '更新失败'));
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setFields(array('活动id', '活动名称', '服务器标识', '状态', '创建时间'));
        $this->setSource('activity', 'activity_result_data');
        $this->displays();
    }

    public function activity_result_data()
    {
        $this->setCondition(0, 'eq', 'a.activity_id');
        $this->setCondition(1, 'like', 'ac.title');
        $this->setCondition(2, 'eq', 'a.status');
        $this->setCondition(3, 'scp');
        $this->setOrder('a.create_time');
        $this->setLimit();
        $this->call('ActivityModel', 'activity_result_data', $this->getConditions());
    }

    //活动模版
    public function model()
    {
        $this->setSearch('活动名称');
        $form = CDict::$activityType;
        $this->setSearch('活动类型', 'select', $form);
        $this->setSearch('创建时间', 'range_time');
        $this->setFields(array('ID', '活动名称', '开始时间', '结束时间', '活动类型', '渠道组', '创建人', '操作'));
        $this->setSource('activity', 'model_data');
        $js = <<< END
			//复制
            function _copy(id){
                \$tabs.tabs('select' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=activity&act=edit&id=' + id + '&copy=1');
                \$tabs.tabs('load' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=activity&act=edit&id=' + id + '&copy=1');
            }
			//编辑
            function _edit(id){
                \$tabs.tabs('select' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=activity&act=edit&id=' + id);
                \$tabs.tabs('load' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=activity&act=edit&id=' + id);
            }
			
			function _del(id){
			  if(confirm("确定要删除模版吗？")){
				  $.ajax({
					url: 'admin.php?ctrl=activity&act=delmodel',
					type: 'POST',
					dataType: 'JSON',
					data: {'id':id}
				  }).done(function(data){
					  if(data ==1){
						  $.dialog.tips('删除成功');
						  \$tabs.tabs('load' , 0);
					  }else{
						  $.dialog.tips('删除失败,请重试');
					  }
				  });
			  }
			}
END;
        $this->setJs($js);
        $this->displays();
    }

    public function model_data()
    {
        $this->setCondition(0, 'like', 'title');
        $this->setCondition(1, 'eq', 'act_id');
        $this->setCondition(2, 'time', 'create_time');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $this->call('ActivityModel', 'model_data', $this->getConditions());
    }

    //设为模版
    public function setmodel()
    {
        $id = $this->getparam('id');
        $sql = "update ny_activity  set model = 1 where id = $id";
        $rs = $this->am->query($sql);
        if ($rs !== false) {
            echo 1;
        } else {
            echo 0;
        }
    }

    //删除模版
    public function delmodel()
    {
        $id = $this->getparam('id');
        $sql = "update ny_activity  set model = 0 where id = $id";
        $rs = $this->am->query($sql);
        if ($rs !== false) {
            echo 1;
        } else {
            echo 0;
        }
    }

    //删除活动列表

    public function del()
    {
        $id = $this->getparam('id');
        $sql = "delete from ny_activity where id = $id";
        $rs = $this->am->query($sql);
        if ($rs !== false) {
            echo 1;
        } else {
            echo 0;
        }
    }

    /**
     * 显示提示
     * @param array $result
     */
    private static function resTips(array $result)
    {
        $out = '';
        $serverIds = $gtps = $row = [];
        if (empty($result)) return $out;

        foreach ($result as $key => $value) {
            array_push($serverIds, $key);//得到所有返回的服务器ID
        }
        $serverModel = new Model();
        $where = ' 1=1 ';
        if (!empty($serverIds)) {
            $where .= " and s.server_id in('" . join('\',\'', $serverIds) . "') ";
        }
        $sql = "select s.server_id,g.name from ny_server as s left join ny_channel_group as g on s.group_id = g.id where " . $where;
        $row = $serverModel->query($sql);
        foreach ($row as $value) {
            $gtps[$value['server_id']] = $value['name'];
        }
        //构造数据
        foreach ($result as $sid => $msg) {
            $red = '';
            $ret = explode('/', $msg);
            $code = !isset($ret[1]) ? 1 : $ret[0];
            if ($code != 1) {
                $red = 'style="color: #ca1b36"';
            }
            !isset($ret[1]) && $ret[1] = $msg;//兼容前期数据结构
            $out .= '<span ' . $red . ' >' . $gtps[$sid] . '-' . $ret[1] . '</span><br/>';
        }
        return $out;
    }


}