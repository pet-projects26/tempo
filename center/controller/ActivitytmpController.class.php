<?php
!defined('IN_WEB') && exit('Access Denied');

class ActivitytmpController extends Controller{
    
    public function __construct(){
        parent::__construct();
        $this->m = new ActivityTmpModel();
        $this->db = new Model();
    }

    public function index(){
        $tabs = array(
            array('title' => '活动记录', 'url' => 'admin.php?ctrl=activityTmp&act=record'),
            array('title' => '活动添加', 'url' => 'admin.php?ctrl=activityTmp&act=add'),
            array('title' => '活动编辑', 'url' => 'admin.php?ctrl=activityTmp&act=edit'),
        );
        $this->tabs($tabs);
    }


    public function record(){
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('标题' );
        // $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('ID');
        $this->setField('服');
        $this->setField('标题');
        $this->setField('类型');
        //$this->setField('开启等级');
        
        $this->setField('开始时间');
        $this->setField('结束时间');

        $this->setField('开关');

        $this->setField('状态');

        $this->setField('添加时间');
        $this->setField('操作');

        $this->setSource('ActivityTmp' , 'record_data' , 1);

        $js = <<< END
            function edit(uuid , type){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=activityTmp&act=edit' + '&uuid=' + uuid + '&type=' +type );
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=activityTmp&act=edit' + '&uuid=' + uuid + '&type=' +type);
            }


            function del(uuid){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=activityTmp&act=del ' + '&uuid=' + uuid);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=activityTmp&act=del' + '&uuid=' + uuid );
            }
END;
        $this->setJs($js);

        $this->displays();

    }
	
    public function record_data(){
        $this->setCondition(0 , 'date' , 'create_time');
        $this->setCondition(1 , 'like' , 'name');
        $this->setOrder('create_time' , 'desc');
        $this->setLimit();
        $this->call('ActivityTmpModel' , 'record_data' , $this->getConditions());
    }


    /**
     * [编辑活动]
     * @return [type] [description]
     */
    public function edit() {
    	$server = (new ServerModel())->getServer(array() , array('server_id' , 'name'));
		$channel = (new ChannelModel())->getChannel(array() , array('channel_id' , 'name'));
		$uuid = isset($_GET['uuid']) ? trim($_GET['uuid']) : '';
		$type = isset($_GET['type']) ? trim($_GET['type']) : 0;

		$res = $this->m->getRowByuuid($uuid);

		if (empty($res)) {
			echo  '查询不到相关信息';
			die();
		}

		$res['start_time'] = date('Y-m-d H:i:s', $res['start_time']);
		$res['end_time'] = date('Y-m-d H:i:s', $res['end_time']);
		$res['servers'] = json_decode($res['servers'], true);

		// $content= json_decode($res['content'], true);

		// $res['content'] = Helper::chineseJson($content);

		$res['servers_json'] = json_encode($res['servers']);

		$this->smarty->assign('data' , $res);
		$this->smarty->assign('type' , $type);
		$this->smarty->assign('server' , $server);
		$this->smarty->assign('channel' , $channel);
		$this->smarty->display('activityTmp/edit.tpl');	
    }

    /**
     * [删除活动]
     * @return [type] [description]
     */
    public function del () {
    	$uuid = isset($_GET['uuid']) ? trim($_GET['uuid']) : ''; //删除活动

    	$res = $this->m->del($uuid);

    	foreach ($res['msg'] as $key => $msg) {
			echo $msg;
			echo '<p>';
    	}
    	die();
    } 

    /**
     * [添加活动]
     */
    public function add() {
    	$server = (new ServerModel())->getServer(array() , array('server_id' , 'name'));
		$channel = (new ChannelModel())->getChannel(array() , array('channel_id' , 'name'));
		$this->smarty->assign('server' , $server);
		$this->smarty->assign('channel' , $channel);
		$this->smarty->display('activityTmp/config.tpl');	
    }

    /**
     * [格式代json]
     */
    public function check_data($action = 'check') {
    	$title = isset($_POST['title']) ?  trim($_POST['title']) :  '';
		if (empty($title)) {
			$arr = array('code' => 404, 'msg' => '标题不能为空');
			die(json_encode($arr));
		}

		$start_time = isset($_POST['start_time']) ? trim($_POST['start_time']) : '';

		if (empty($start_time)) {
			$arr = array('code' => 404, 'msg' => '开始时间不能为空');
			die(json_encode($arr));
		}

		$type = isset($_POST['type']) ? intval($_POST['type']) : 0;

		$end_time = isset($_POST['end_time']) ? trim($_POST['end_time']) : '';

		if (empty($end_time)) {
			$arr = array('code' => 404, 'msg' => '结束时间不能为空');
			die(json_encode($arr));
		}
		
		if (!isset($_POST['uuid']) || $type == 1) {
			if(strtotime($start_time) <= time()){
				$arr = array('code' => 404, 'msg' => '开始时间不能小于当前时间');
				die(json_encode($arr));	
			}

			if(strtotime($end_time) <= time()){
				$arr = array('code' => 404, 'msg' => '结束时间不能小于当前时间');
				die(json_encode($arr));	
			}

		}
		
		
		if(strtotime($end_time) <= strtotime($start_time)){
			$arr = array('code' => 404, 'msg' => '结束时间不能小于等于开始时间');
			die(json_encode($arr));	
		}

		$content = isset($_POST['content']) ? trim($_POST['content']) : '';

		$old_content = $content;

		if (empty($content)) {
			$arr = array('code' => 404,  'msg' => '内容不能为空');
			die(json_encode($arr));
		}

		//去掉注释
		$content = preg_replace("/(\/\*(.*)\*\/)/sU", '',  $content);


		$content = json_decode($content, true);

		if (!is_array($content)) {
			$arr = array('code' => 404,  'msg' => '内容格式错误');
			die(json_encode($arr));
		}

		$aid = isset($_POST['aid']) ? trim($_POST['aid']) : '';
		if (empty($aid)) {
			$arr = array('code' => 404, 'msg' => '活动类型不能为空');
			die(json_encode($arr));
		}

		if (!is_numeric($aid)) {
			$arr = array('code' => 404, 'msg' => '活动类型请输入整数');
			die(json_encode($arr));
		}

		$server = isset($_POST['server']) ? $_POST['server'] : '';


		if (empty($server)) {
			$arr = array('code' => 404, 'msg' => '请选择服务器');
			die(json_encode($arr));
		}

		if ($server == '[]') {
			$arr = array('code' => 404, 'msg' => '请选择服务器,服务器不能为空');
			die(json_encode($arr));
		}

		$channel = isset($_POST['channel']) ? $_POST['channel'] : '';

		$uuid = isset($_POST['uuid'])  ? $_POST['uuid'] : false;
		$type = isset($_POST['type'])  ? $_POST['type'] : 0;
		
		$data = array(
			'title' => $title,
			'start_time' => strtotime($start_time),
			'end_time' => strtotime($end_time),
			'state' => intval($_POST['state']),
			'openLvl' => intval($_POST['openLvl']),
			'content' => $content,
			'server' => $server,
			'channel' => $channel,
			'aid' => $aid, 
			'name' => $title,
			'old_content' => $old_content,
			'type' => $type,
		);

		if ($action == 'check') {
			$res = $this->m->check($data, $uuid);
			if (empty($res)) {
				$code = 200;
			} else {
				$code = 202;
			}

			die(json_encode(array('code' => $code, 'msg' => $res)));

		} 
		
		$res = $this->m->addData($data, $uuid);

		die(json_encode(array('code' => 200, 'msg' => $res)));

		
    }

    public function add_data() {
    	$this->check_data('add');
    }

}
