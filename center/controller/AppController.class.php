<?php
!defined('IN_WEB') && exit('Access Denied');

class AppController extends Controller{
    private $am;

    public function __construct(){
        parent::__construct();
        $this->am = new AppModel();
    }

    public function record(){
        $this->setFields(array('应用标识' , '应用名称' , 'API密码' , '平台信息' , '创建日期' , '操作'));
        $this->setSource('app' , 'record_data');
        $js = <<< END
            function edit(aid){
                \$tabs.tabs('select' , 4);
                \$tabs.tabs('url' , 4 , 'admin.php?ctrl=app&act=edit&aid=' + aid);
                \$tabs.tabs('load' , 4);
                \$tabs.tabs('url' , 4 , 'admin.php?ctrl=app&act=edit&aid=' + aid);
            }
            function del(aid){
            if(confirm('确定删除此条记录？')){
                $.ajax({
                    url: 'admin.php?ctrl=app&act=del_action&aid=' + aid,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(){
                    \$tabs.tabs('load' , 2);
                })
                }
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    public function record_data(){
        $this->setOrder();
        $this->setLimit();
        $this->call('AppModel' , 'record_data' , $this->getConditions());
    }

    public function add(){
        unset(CDict::$platform[0]);
        $this->smarty->assign('platform' , CDict::$platform);
        $this->smarty->display('app/add.tpl');
    }
    public function add_action(){
        $app_id = trim($this->getParam('app_id'));
        $name = trim($this->getParam('name'));
        $secret = trim($this->getParam('secret'));
        $platform = $this->getParam('platform');
        $platform = array_sum($platform);
        $platform = $platform == 3 ? 0 : $platform;
        $data = array(
            'app_id' => $app_id,
            'name' => $name,
            'secret' => $secret,
            'platform' => $platform,
            'create_time' => time()
        );
        if(!preg_match('/^[0-9a-zA-Z._]+$/' , $app_id)){
            $json = array('msg' => '应用标识格式不正确' , 'code' => 0);
        }
        else if($this->am->getApp($app_id)){
            $json = array('msg' => '应用标识已存在' , 'code' => 0);
        }
        else if($this->am->addApp($data)){
            $json = array('msg' => '添加成功' , 'code' => 1);
        }
        else{
            $json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
        }
        echo json_encode($json);
    }

    public function edit(){
        unset(CDict::$platform[0]);
        $platform = CDict::$platform;
        $app_id = $this->getParam('aid');
        $app = $this->am->getApp($app_id);
        if($app){
            $app_id && $this->smarty->assign('app_id' , $app_id);
            $app['name'] && $this->smarty->assign('name' , $app['name']);
            $app['secret'] && $this->smarty->assign('secret' , $app['secret']);
            if($app['platform']){
                foreach($platform as $k => $name){
                    $checked = $app['platform'] == $k ? 1 : 0;
                    $platform[$k] = array('name' => $name , 'checked' => $checked);
                }
            }
            else{
                foreach($platform as $k => $name){
                    $platform[$k] = array('name' => $name , 'checked' => 1);
                }
            }
            $this->smarty->assign('platform' , $platform);
        }
        $this->smarty->display('app/edit.tpl');
    }
    public function edit_action(){
        $app_id = $this->getParam('app_id');
        if($app_id){
            $old_app_id = $this->getParam('old_app_id');
            $name = $this->getParam('name');
            $secret = $this->getParam('secret');
            $platform = $this->getParam('platform');
            $platform = array_sum($platform);
            $platform = $platform == 3 ? 0 : $platform;
            $data = array('app_id' => $app_id , 'name' => $name , 'secret' => $secret , 'platform' => $platform);
            if(!preg_match('/^[0-9a-zA-Z._]+$/' , $app_id)){
                $json = array('msg' => '应用标识格式不正确' , 'code' => 0);
            }
            else if($this->am->editApp($data , $old_app_id)){
                $json = array('msg' => '保存成功' , 'code' => 1);
            }
            else{
                $json = array('msg' => '没有任何改变' , 'code' => 0);
            }
            echo json_encode($json);
        }
    }

    public function del_action(){
        $app_id = $this->getParam('aid');
        $this->am->delApp($app_id);
    }

    public function getPlatform(){
        $app_id = $this->getParam('aid');
        $app = $this->am->getApp($app_id , array('app_id' , 'platform'));
        echo $app ? json_encode(array('platform' => $app['platform'] , 'code' => 1)) : json_encode(array('msg' => 'app_id不存在' , 'code' => 0));
    }
}