<?php
!defined('IN_WEB') && exit('Access Denied');

class BanController extends Controller
{
    public $ro;
    public $sm;

    public function __construct()
    {
        parent::__construct();
        $this->ro = new RoleModel();
        $this->sm = new ServerModel();
    }

    //渠道管理（菜单）
    public function index()
    {
        $tabs = array(
            array('title' => '封禁列表', 'url' => 'admin.php?&ctrl=ban&act=record'),
            array('title' => '批量封禁', 'url' => 'admin.php?&ctrl=ban&act=edit'),
            array('title' => '玩家下线', 'url' => 'admin.php?ctrl=role&act=manual_logout')
        );
        $this->tabs($tabs);
    }

    public function record()
    {
        $type = array('' => '全部');
        foreach (CDict::$banType as $k => $row) {
            $type[$k] = $row['title'];
        }
        // $this->setButton('header', '全选', 'selectAll', 'unselect');
        //$this->setButton('header', '解封', 'delete');
        $this->setSearch('封禁类型', 'select', $type);
        $this->setSearch('账号/角色名/IP');
        //$this->setSearch('角色名');
        $this->setSearch('IP');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('ID', 10);
        $this->setField('封禁类型', 30);
        $this->setField('封禁时间', 30);
        $this->setField('账号/角色名/IP', 30);
        $this->setField('过期时间', 100);
        $this->setField('原因', 30);
        $this->setField('状态', 30);
        $this->setField('管理员', 20);
        $this->setField('封禁时间', 20);
        $this->setLength(20);
        $this->setSource('ban', 'record_data');

        $js = <<< END
            function _deleted(id, server){
                if(confirm('确定要解封？')){
                    $.ajax({
                        url: 'admin.php?ctrl=ban&act=delete&id='+id+'&server='+server,
                        type: 'GET',
                        dataType: 'JSON',
                        beforeSend:function(){
                            $(".checked").attr({disabled:"disabled"});
                        },
                        complete:function(){
                            $(".checked").removeAttr('disabled');
                        }

                    }).done(function(data){
                        $.dialog.tips(data.msg);
                        window.location.reload();
                    });
                }
            }
            
END;

        $this->setJs($js);
        $this->smarty->assign('select', 'radio');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'eq', 'type');
        $this->setCondition(1, 'eq', 'name');
        //$this->setCondition(2 , 'eq' , 'name');
        $this->setCondition(2, 'eq', 'ip');
        $this->setCondition(3, 'scp');
        $this->setOrder();
        $this->setLimit(20);
        $this->call('BanModel', 'record_data', $this->getConditions());
    }

    public function edit()
    {
        $reason = CDict::$banReason;
        krsort($reason);

        $servers = $this->sm->getServerByTrait();
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach ($channelGroups as $key => $val) {
            $groups[$val['id']] = $val['name'];
        }

        $this->smarty->assign('servers', $servers);
        $this->smarty->assign('groups', $groups);

        $this->smarty->assign('reason', $reason);
        $this->smarty->assign('bantype', CDict::$banType);
        $this->smarty->assign('bantime', CDict::$banTime);
        $this->smarty->display('ban/edit.tpl');
    }

    public function edit_action()
    {
        $server = $this->getParam('server');
        $content = $this->getParam('content');
        $save = $this->getParam('save');
        if ($server && $content && $save) {
            $content_name = $this->getTextWrapContent($content);
            $type = intval($this->getParam('type'));
            $time = intval($this->getParam('time'));
            $reason = trim($this->getParam('reason'));
            $other = trim($this->getParam('other'));
            $user = $_SESSION['username'];
            $create_time = time();

            //获取服务器websocket配置
            $server = (new ServerconfigModel())->getConfig($server, ['server_id', 'websocket_host', 'websocket_port']);
            $server_id = $server['server_id'];

            //获取封禁type
            $banField = CDict::$banType[$type]['field'];
            $banName = CDict::$banType[$type]['name'];
            $banType = CDict::$banType[$type]['type'];

            if (!$banField) die(json_encode(array('msg' => '该封禁类型的field无效, 请联系相关开发')));

            $role_msg = '';
            $all_role_id = [];
            $all_str = [];
            //查询角色是否存在 查得到就是正确的角色并获取role_id 查不到直接抛出错误
            if ($type == 2 || $type == 4 || $type == 5) {
                foreach ($content_name as $key => $val) {
                    $data = $this->call('RoleModel', 'getRoleIdToType', array('type' => $banField, 'value' => $val), $server_id);
                    $role_id = $data[$server_id];
                    if (empty($role_id)) {
                        $role_msg .= $banName . ':' . $val . " :角色信息查找不到<br/>";
                    } else {
                        $all_role_id[] = intval($role_id);
                    }
                }
            } else {
                $all_str = $content_name;
            }

            if($role_msg != ''){
                die(json_encode(array('state' => false , 'code' => 200 , 'msg' => $role_msg)));
            }

            //然后向服务器发送协议
            if ($server['websocket_host'] == '' || $server['websocket_port'] == '') {
                die(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
            }


            //获取封禁列表中已存在的封禁对象，如果是已存在的对象则更新，如果不是则写入
            $rs = $this->call('BanModel', 'getBanByTypeName', array('type' => $type, 'name' => $content_name, 'fields' => array('id', 'name', 'type')), $server_id);
            $rs = $rs[$server_id];

            if (empty($rs)) {

                $add_data = array();
                foreach ($content_name as $k => $v) {
                    $name = trim($v);
                    //if(!in_array($name . $type , $nametypes)){
                    $add_data[$k]['name'] = $name;
                    $add_data[$k]['type'] = $type;
                    $add_data[$k]['time'] = $time;
                    $add_data[$k]['reason'] = $reason;
                    $other && $add_data[$k]['other'] = $other;
                    $add_data[$k]['user'] = $user;
                    $add_data[$k]['create_time'] = $create_time;
                    //}
                }

                $ars = $this->call('BanModel', 'addBan', array('data' => $add_data), $server_id);//写入
                $ars = $ars[$server_id];
            } else {
                $nametypes = array();
                foreach ($rs as $row) {
                    $names[$row['id']] = $row['name'];
                    $nametypes[$row['id']] = $row['name'] . $row['type'];
                }

                $update_data = array();
                $update_data['time'] = $time;
                $update_data['reason'] = $reason;
                $other && $update_data['other'] = $other;
                $update_data['user'] = $user;
                $update_data['create_time'] = $create_time;
                $urs = $this->call('BanModel', 'updateBan', array('data' => $update_data, 'id' => array_keys($names)), $server_id);//更新
                $urs = $urs[$server_id];
            }


            if ($ars['state'] || $urs['state']) {

                $other = !$reason ? $other : CDict::$banReason[$reason];

                $ban = array(intval($banType), false, $all_str, $all_role_id, intval($time), $other);

                $sendData = array('opcode' => Pact::$RoleBlockOprate, 'str' => $ban);

                $msg = $this->sm->socketCall($server['websocket_host'], $server['websocket_port'], 'roleBlock', $sendData);

                $json = array('msg' => $msg === 0 ? '提交成功' : '提交失败');
            } else {
                $json = array('msg' => '数据记录失败');
            }
            echo json_encode($json);
        }
    }

    public function edit_action_old()
    {

        $server = $this->getParam('server');

        $content = $this->getParam('content');
        if ($server && $content) {
            //$server = explode(':' , $server);
            $content = explode(',', $content);
            $type = $this->getParam('type');
            $time = $this->getParam('time');
            $reason = $this->getParam('reason');
            $other = $this->getParam('other');
            $user = $_SESSION['username'];
            $create_time = time();
            $content_name = array();
            foreach ($content as $k => $v) {
                $content_name[] = trim($v);
            }

            $sql = "select server_id , ip,gm_port from ny_server_config where  server_id = '$server'";
            $serverarr = $this->sm->query($sql);
            unset($server);
            //兼容之前的写法
            $server[0] = $serverarr[0]['server_id'];
            $server[1] = $serverarr[0]['ip'];
            $server[2] = $serverarr[0]['gm_port'];

            $server_id = $server[0];
            if ($type == 1) {//封帐号

                $account = "'" . implode("','", $content_name) . "'";
                $sql = "select role_id from ny_role where account in ($account)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];

                foreach ($data as $k => $v) {
                    $this->ro->rpcCall($server[1], $server[2], 'logout', array('role_id' => $v['role_id']));
                }

            } elseif ($type == 2) {//封角色名
                $role_name = "'" . implode("','", $content_name) . "'";
                $sql = "select role_id from ny_role where name in ($role_name)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];

                foreach ($data as $k => $v) {
                    $this->ro->rpcCall($server[1], $server[2], 'logout', array('role_id' => $v['role_id']));
                }
            } elseif ($type == 3) {//封IP
                $last_login_ip = "'" . implode("','", $content_name) . "'";
                $sql = "select role_id from ny_role where last_login_ip in ($last_login_ip)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];

                foreach ($data as $k => $v) {
                    $this->ro->rpcCall($server[1], $server[2], 'logout', array('role_id' => $v['role_id']));
                }
            } elseif ($type == 4) {//封MAC
                $mac = "'" . implode("','", $content_name) . "'";
                $sql = "select role_id from ny_role where mac in ($mac)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];

                foreach ($data as $k => $v) {
                    $this->ro->rpcCall($server[1], $server[2], 'logout', array('role_id' => $v['role_id']));
                }
            } elseif ($type == 5) {//禁言
                $role_name = "'" . implode("','", $content_name) . "'";
                $sql = "select role_id from ny_role where name in ($role_name)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];

//                 foreach($data as $k=>$v){
//                     $this->ro->rpcCall($server[1] , $server[2] , 'logout' ,  array('role_id' => $v['role_id'])); 
//                 }
            } elseif ($type == 6) {//封角色id(转化成封角色名)
                $role_id = "'" . implode("','", $content_name) . "'";
                $sql = "select name from ny_role where role_id in ($role_id)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];
                $content = [];
                foreach ($data as $value) {
                    $content[] = $value['name'];
                }
                $type = 2;//转成封角色名
                foreach ($content_name as $k => $v) {
                    $this->ro->rpcCall($server[1], $server[2], 'logout', array('role_id' => $v));
                }
            }

            //获取封禁列表中已存在的封禁对象，如果是已存在的对象则更新，如果不是则写入
            $rs = $this->call('BanModel', 'getBanByTypeName', array('type' => $type, 'name' => $content_name, 'fields' => array('id', 'name', 'type')), $server[0]);
            $rs = $rs[$server[0]];


            if (empty($rs)) {

                $add_data = array();
                foreach ($content as $k => $v) {
                    $name = trim($v);
                    //if(!in_array($name . $type , $nametypes)){
                    $add_data[$k]['name'] = $name;
                    $add_data[$k]['type'] = $type;
                    $add_data[$k]['time'] = $time;
                    $add_data[$k]['reason'] = $reason;
                    $other && $add_data[$k]['other'] = $other;
                    $add_data[$k]['user'] = $user;
                    $add_data[$k]['create_time'] = $create_time;
                    //}
                }

                $ars = $this->call('BanModel', 'addBan', array('data' => $add_data), $server[0]);//写入
                $ars = $ars[$server[0]];
            } else {


                $nametypes = array();
                foreach ($rs as $row) {
                    $names[$row['id']] = $row['name'];
                    $nametypes[$row['id']] = $row['name'] . $row['type'];
                }

                $update_data = array();
                $update_data['time'] = $time;
                $update_data['reason'] = $reason;
                $other && $update_data['other'] = $other;
                $update_data['user'] = $user;
                $update_data['create_time'] = $create_time;
                $urs = $this->call('BanModel', 'updateBan', array('data' => $update_data, 'id' => array_keys($names)), $server[0]);//更新
                $urs = $urs[$server[0]];
            }


            if ($ars['state'] || $urs['state']) {
                $ban = array( //发包内容
                    'delete' => 1,
                    'content' => !empty($names) ? $names : $ars['content'],
                    'type' => $type,
                    'time' => $time
                );

                $msg = (new BanModel())->rpcCall($server[1], $server[2], 'ban', $ban);
                $json = array('msg' => $msg ? $msg : '提交成功');
            } else {
                $json = array('msg' => '数据记录失败');
            }
            echo json_encode($json);
        }
    }

    public function delete()
    {
        $id = $this->getParam('id');
        $server = $this->getParam('server');

        if (!$id || !$server) {
            echo json_encode(['msg' => '参数错误']);
            exit;
        }

        //获取发包内容
        $data = $this->call('BanModel', 'getBan', ['id' => $id], $server);
        $data = $data[$server];

        if (empty($data)) {
            echo json_encode(['msg' => '参数错误']);
            exit;
        }

        if ($data['deleted']) {
            echo json_encode(['msg' => '解封成功']);
            exit;
        }

        $banType = CDict::$banType[$data['type']]['type'];

        $other = !$data['reason'] ? $data['other'] : CDict::$banReason[$data['reason']];

        $all_str = [];

        $all_role_id = [];

        if ($data['type'] == 2 || $data['type'] == 4 || $data['type'] == 5) {
            $all_role_id = [intval($data['name'])];
        } else {
            $all_str = [$data['name']];
        }

        //获取服务器websocket配置
        $server = (new ServerconfigModel())->getConfig($server, ['server_id', 'websocket_host', 'websocket_port']);
        $server_id = $server['server_id'];

        if ($server['websocket_host'] == '' || $server['websocket_port'] == '') {
            die(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
        }

        $ban = array(intval($banType), true, $all_str, $all_role_id, intval($data['time']), $other);

        $sendData = array('opcode' => Pact::$RoleBlockOprate, 'str' => $ban);

        $msg = $this->sm->socketCall($server['websocket_host'], $server['websocket_port'], 'roleBlock', $sendData);

        if ($msg === 0) {
            $rs = $this->call('BanModel', 'delBan', array('id' => $id), $server_id);
            $rs = $rs[$server_id];
            if ($rs['state']) {
                $json = array('msg' => '解封成功');
            } else {
                $json = array('msg' => '解封遇到错误'); //就是mysql数据库写入错误
            }
        } else {
            $json = array('msg' => '解封遇到错误'); //就是通讯失败
        }

        echo json_encode($json);
    }
}