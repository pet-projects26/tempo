<?php
!defined('IN_WEB') && exit('Access Denied');

class BosshomeController extends Controller{

    public function index(){
        if (!empty(CDict::$bossHomeLayer)) {
            $tabs = [];
            foreach (CDict::$bossHomeLayer as $key => $val) {
                $tab = [
                    'title' => $val,
                    'url' => 'admin.php?&ctrl=bosshome&act=record&layer='.$key
                ];

                array_push($tabs, $tab);
            }
            $this->tabs($tabs);
        } else {
            echo 'CDict::$bossHomeLayer is empty! 请联系开发人员';
        }
    }

    public function record()
    {
        $layer= $this->getParam('layer');

        if (!array_key_exists($layer, CDict::$bossHomeLayer)) return false;

        $header = '说明：<br>';
        $header .= '活跃人数：该日成功连接上游戏服务器并且开启BOSS之家的玩家人数（备注：开启条件待定，已上线前版本为准）。<br>';
        $header .= '参与人数：该日进行过BOSS之家挑战玩法（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '本层人均击杀BOSS次数 = 本层BOSS死亡次数 / 进入本层的玩家数（去重）   精确到小数点后两位<br>';
        $header .= '本层免费进入占比 = 免费进入本层的玩家数 / 进入本层的玩家数 *100%   四舍五入精确到小数点后两位<br>';
        $header .= '本层免费进入人均次数 = 免费进入本层的次数 /  免费进入本层的玩家数  四舍五入精确到小数点后两位<br>';
        $header .= '本层付费进入人均次数  = 付费进入本层的次数 / 付费进入本层的玩家数  四舍五入精确到小数点后两位<br>';


        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '参与人数' , '参与度', '本层人均击杀BOSS次数', '本层免费进入占比', '本层免费进入人均次数', '本层付费进入人均次数'));
        $this->setSource(' Bosshome' , 'record_data' , 1, ['layer' => $layer]);
        $this->setLimit('100');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0 , 'time' , "create_time");
        $this->setCondition(1 , 'scp');
        $this->setCondition(2 , 'eq' , 'layer' ,$this->getParam('layer'));
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('BosshomeModel' , $method , $this->getConditions());
    }

}