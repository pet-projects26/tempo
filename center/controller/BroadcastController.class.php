<?php
!defined('IN_WEB') && exit('Access Denied');

class BroadcastController extends Controller{
  
	private $bc;
    public function __construct(){
        parent::__construct();
		$this->bc=new BroadcastModel();
    }

    //分区管理（菜单）
    public function index(){
        $tabs = array(
            array('title' => '广播列表', 'url' => 'admin.php?&ctrl=broadcast&act=record'),
            array('title' => '添加广播', 'url' => 'admin.php?&ctrl=broadcast&act=add'),
			 array('title' => '编辑广播', 'url' => 'admin.php?&ctrl=broadcast&act=edit'),
        );
        $this->tabs($tabs);
    }
	public function record(){

		$server = $this->getChannelServer();
		$header  = '<div class="search_float "><input type="checkbox" id="checkall">全选';
		$header.='<input id="tongbut"  type="button" class=" gbutton" value="同步" onclick="tongbuB()">
		 	<div id="server" style=" margin-top:20px;display:none" class="ui-tabs ui-widget ui-widget-content ui-corner-all"><table class="itable itable-color">'.$server.'</table></div>
		 </div>';
         $this->setHeader($header);
		 $this->setFields(array('id','开始时间','结束时间','显示','间隔','内容','状态','同步时间','同步日志','操作'));

		 $this->setSource('broadcast','record_data');
		 $js = <<< END
         function edit(id){
			 \$tabs.tabs('select',2);
             \$tabs.tabs('url' , 2 , 'admin.php?ctrl=broadcast&act=edit&id=' + id);
             \$tabs.tabs('load' , 2);
         }
		 function deleteid(id){
			if(confirm("确定要取消同步广播吗？")){
				$.ajax({
				  url: 'admin.php?ctrl=broadcast&act=delete',
				  type: 'POST',
				  dataType: 'JSON',
				  data: {'id':id}
			  	}).done(function(){
					$.dialog.tips('取消同步广播成功');
				  	\$tabs.tabs('load' , 0);
			  	})	 
			 } 	 
		 }
		 var check=1;
		 $("#checkall").click(function(){
			 if(check == 1){
				 $(" .sorting_1 :checkbox").attr("checked", true); 
				 check =0; 
			 }else{
				 check=1;	 
				 $(" .sorting_1 :checkbox").attr("checked", false); 
			 } 
		 });
		 var click=0;
		 function tongbuB(){
			$("#server").css('display','block');
			//var checkboxServer= $("#checkboxServer").attr("value");
			var server_select = [];
			var group = [];
			$("input[name='server[]']").each(function(){
				if($(this).prop('checked')){
					server_select.push($(this).val());
				}
			})
			$("input[name='channel_group[]']").each(function(){
				if($(this).prop('checked')){
					group.push($(this).val());
				}
			})
			var checkServer = JSON.stringify(server_select);
			var checkGroup = JSON.stringify(group);
			if(click==0){
				click=1;return false;
			}
			if(checkServer==='[""]'){
				checkServer = '[]';
			}
			if(checkGroup === '[""]'){
				checkGroup = '[]';
			}
			var broadcast = new Array();
			
			$('.checked:checked').each(function(){
				broadcast.push($(this).val());	
			});
			if(broadcast.length == 0){
				$.dialog.tips('请选择要同步的广播数据');	return false;		
			}
			
			if(confirm("确定要提交广播吗？")){
				$.ajax({
				  url: 'admin.php?ctrl=broadcast&act=sync',
				  type: 'POST',
				  dataType: 'JSON',
				  data: {
				  		'broadcast':broadcast,
				  		'checkboxServer':checkServer,
				  		'checkboxGroup':checkGroup
				  }
			  	}).done(function(){
					$.dialog.tips('同步广播成功');
				    \$tabs.tabs('load' , 0);
			  	})	 
			 } 
    	}
		 
END;
		
         $this->setJs($js);
		
		 $this->displays();
	}
	public function record_data(){
		$this->setOrder('id');
		$this->call('BroadcastModel','getBroadcast',$this->getConditions());
	}
	public function add(){
        $this->smarty->assign('broadCastType', CDict::$broadCastType);
		$this->smarty->display('broadcast/add.tpl');
	}
	public function edit($id=''){
		$id=$_GET['id'];
		if($id){
			$sql="select * from ny_broadcast where id =$id";
			$result=$this->bc->query($sql);
			$result[0]['starttime']=date('Y-m-d H:i',$result[0]['starttime']);
			$result[0]['endtime']=date('Y-m-d H:i',$result[0]['endtime']);
			$this->smarty->assign('data',$result[0]);
		}
        $this->smarty->assign('broadCastType', CDict::$broadCastType);
		$this->smarty->display('broadcast/edit.tpl');
	}	
	
	public function add_data(){
		$post=$_POST;
		$id=$post['id'];
		$starttime=(int)$post['starttime'];
		$endtime=(int)$post['endtime'];
		$intervaltime=(int)$post['intervaltime'];
		$type=(int)$post['type'];
        $content = addslashes(htmlentities($post['content'])); //转义
		$adder = $_SESSION['username'];
		
		if($id==0){
			$sql="insert into ny_broadcast set starttime=$starttime,endtime=$endtime,intervaltime=$intervaltime,type=$type,content='$content' , adder = '$adder'";	
		}else{
			$sql="update ny_broadcast set starttime=$starttime,endtime=$endtime,intervaltime=$intervaltime,type=$type,content='$content' , adder = '$adder' where id =$id";		
		}
		$result=$this->bc->query($sql);
		echo json_encode($result);
	}
	public function sync(){
		$post=$_POST;
		$checkboxServer=json_decode($post['checkboxServer'],true);//服务器名称
		$checkboxGroup = json_decode($post['checkboxGroup'],true);//渠道组
		$broadcast=$post['broadcast'];//广播的id

		if(empty($checkboxGroup)){
			$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
			$checkboxGroup = array_column($channelGroupRes, 'id');
		}

		$serverids = (new ServerModel())->getServerByServerAndChannelGroup($checkboxServer,$checkboxGroup);

		$checkboxServer = array_keys($serverids);

		//先把同步时间和同步的服务器,同步状态写入mysql数据库
		$broadcastid=implode(',',$broadcast);
		$checkboxServerid=implode(',',$checkboxServer);
		$syncer = $_SESSION['username'];

		foreach($broadcast as $k=>$v){
			$sql="update ny_broadcast set synctime=".time().",server='$checkboxServerid',status=1 , syncer = '$syncer' where id=$v";
			$this->bc->query($sql);
		}
		//再从数据库把内容全部读出来
		$sql="select * from ny_broadcast where id in ($broadcastid)";
		$result=$this->bc->query($sql);
		$arr=array();
		foreach($result as $kk=>$vv){
			$data = [];
			$data[] = (string)$vv['id'];
			$data[] = $vv['starttime'] * 1000;
			$data[] = $vv['endtime'] * 1000;
			$data[] = $vv['intervaltime'] * 1000;
			$data[] = CDict::$broadCastType[$vv['type']]['value'];
            $data[] = html_entity_decode(stripslashes($vv['content']));

			$arr[] = $data;
		}

		//获取需要同步的服务器的host和port
		$serverid=implode("','",$checkboxServer);
		$serversql="select websocket_host, websocket_port, server_id  from ny_server_config where server_id in ('$serverid')";
		$server=$this->bc->query($serversql);
		$returnMsg = '';
		foreach($server as $k=>$v){
		    $sendData = ['opcode' => Pact::$Broadcast, 'str' => [$arr]];
			$msg =  $this->bc->socketCall($v['websocket_host'],$v['websocket_port'],'broadCast', $sendData);
			if ($msg === 0) {
                $returnMsg .= $v['server_id'].'同步成功';
            } else {
                $returnMsg .= $v['server_id'].'同步失败';
            }
		}

		echo json_encode(['code' => 1, 'msg' => $returnMsg]);
	}
	public function delete(){
		$id=$_POST['id'];
		if($id){
			//先把数据的同步状态改变
			$sql="update ny_broadcast set status = 0 where id =$id";
			$this->bc->query($sql);
			//再把该条数据需要发送的服务器读出来，发送消息给游戏后端删除记录
			$sql="select server from ny_broadcast where id =$id";
			$result=$this->bc->query($sql);
			//获取需要同步的服务器的host和port
			$serverid=$result[0]['server'];
			$serverid=explode(',',$serverid);
			$serverid=implode("','",$serverid);
			$serversql="select websocket_host, websocket_port from ny_server_config where server_id in ('$serverid')";
			$server=$this->bc->query($serversql);
			$arr=array((string)$id);

			foreach($server as $k=>$v){
                $sendData = ['opcode' => Pact::$DelBroadcast, 'str' => $arr];
				$msg=$this->bc->socketCall($v['websocket_host'],$v['websocket_port'],'broadCast', $sendData);
				echo json_encode($msg);
			}
		}
	}
	
}



















