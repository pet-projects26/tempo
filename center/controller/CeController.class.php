<?php
!defined('IN_WEB') && exit('Access Denied');

class CeController extends Controller{
    private $zm;
    private $groupAccs = array();

    use tChannelGroupServerAccess;

    public function __construct(){

        parent::__construct();
        $this->zm = new ZoneModel();
        $groupAcce = self::getChannelGroupByTrait();
        $this->groupAccs = array_column($groupAcce,'id');
    }

    public function tab(){
        $tabs = array(
            array('title'=>'背包库存','url'=>'admin.php?ctrl=ce&act=index'),
            array('title'=>'排名导出','url'=>'admin.php?ctrl=ce&act=prev20'),
            array('title'=>'MAC导出','url'=>'admin.php?ctrl=ce&act=mac'),
        );
        $this->tabs($tabs);
    }

    /**
     * -- 激活MAC
    select count(DISTINCT(mac)) as count , FROM_UNIXTIME(create_time,'%Y-%m') FROM ny_mac where mac !=0
     * and channel in ('leishen','shouqu','xingwan') GROUP BY  FROM_UNIXTIME(create_time,'%Y-%m');

    -- 充值MAC
    select count(DISTINCT(mac)) as count , FROM_UNIXTIME(create_time,'%Y-%m') as time
    from ny_client where role
    in(select DISTINCT(role_id) from ny_order where FROM_UNIXTIME(create_time,'%Y-%m') >= '2017-07' && FROM_UNIXTIME(create_time,'%Y-%m')<='2017-09')

    and mac !=0 and channel in ('leishen','shouqu','xingwan','huashen') GROUP BY  FROM_UNIXTIME(create_time,'%Y-%m')
     *
    */


    /**
     * 背包库存
     * @version 2017-1014
     */
    public function index(){
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->display('ce/index.tpl');
    }
    public function indexExport(){

        $start_time = $this->getParam('start_date');
        $end_time = $this->getParam('end_date');
        $where = '1=1';
        if($end_time){
            if($start_time){
                $where .= " and from_unixtime(`create_time`,'%Y-%m-%d') >= '".$start_time."' && from_unixtime(`create_time`,'%Y-%m-%d') <= '".$end_time."' ";
            }else{
                $where .= " and from_unixtime(`create_time`,'%Y-%m-%d') <= '".$end_time."'";
            }
        }else if($start_time){
            $where .= " and from_unixtime(`create_time`,'%Y-%m-%d') >= '".$start_time."'";
        }

        $channelGroupIds = $this->getParam('channel_group');
        $servers = $this->getParam('server');
        empty($channelGroupIds[0]) && $channelGroupIds = $this->groupAccs;
        empty($servers[0]) && $servers = array();

        $serverModel = (new ServerModel());
        $serverIds = $serverModel->getServerByServerAndChannelGroup($servers,$channelGroupIds);

        //获取产出道具
        $outsql = "select `item_id`,sum(`item_num`) as num from ny_consume_produce where `type`=0 and ".$where." group by `item_id`";
        //获取消耗道具
        $cresql = "select `item_id`,sum(`item_num`) as num from ny_consume_produce where `type`=1 and ".$where." group by `item_id`";

        $data = array(
            'consuncre'=>$cresql,
            'consunout'=>$outsql
        );

        $serverIds = array_keys($serverIds);//获取所有服务器ID
        $arr  = array('query' => array('data' => $data));

        $res  = (new ServerconfigModel())->makeHttpParams($serverIds, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $rs = Helper::rolling_curl($urls, $params);
        $result = self::getDatas($rs);
        $ser = array();
        $row = array();
        foreach($result as $key=>$val){

            $cre = $val['consuncre'];//产出
            $cut = $val['consunout'];//消耗

            //计算每个道具所有产出$ser
            foreach ($cre as $k => $cres) {
                $ser[$cres['item_id']]['item_id'] = $cres['item_id'];
                @$ser[$cres['item_id']]['num'] += (int)$cres['num'];
            }

            //计算每个道具所有消耗$ser
            foreach ($cut as $cuts) {
                $row[$cuts['item_id']]['item_id'] = $cuts['item_id'];
                @$row[$cuts['item_id']]['num'] += (int)$cuts['num'];
            }
        }
        //合并所有服务器信
        $itmes = self::getItemList();

        $msg = array();
        foreach($ser as $ky=>$v){
            $num = 0;
            @$msg[$ky]['item_id'] = $ky;
            @$msg[$ky]['name'] = $itmes[$ky]?$itmes[$ky]:'';
            if(isset($row[$ky]['num'])){
                $num = $v['num']-$row[$ky]['num'];
            }
            $msg[$ky]['num'] = $num>0?$num:0;
        }
        $result = array();
        $result[] = array(
            '道具ID' , '道具名称','数量'
        );
        foreach($msg as $row){
            $result[] = $serverModel->formatFields($row);
        }
        Util::exportExcel($result , '背包库存（' . date('Y年m月d日H时i分s秒') . '）');//保存excel文件
    }

    /**
     * 前20排行
     * @version 2017-10-14
     */
    public function prev20(){

        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->display('ce/prev20.tpl');
    }

    public function prev20Export(){
        $start_time = $this->getParam('start_date');
        $end_time   = $this->getParam('end_date');
        $list_num   = $this->getParam('list_num')?$this->getParam('list_num'):20;
        $model = new ServerModel();

        $where1 = $where2 = $where = '1=1';
        if($end_time){
            if($start_time){
                $where .= " and from_unixtime(`create_time`,'%Y-%m-%d') >= '".$start_time."' && from_unixtime(`create_time`,'%Y-%m-%d') <= '".$end_time."' ";
                $where2 .= " and from_unixtime(`start_time`,'%Y-%m-%d') >= '".$start_time."' && from_unixtime(`start_time`,'%Y-%m-%d') <= '".$end_time."' ";
                $where1 .=" and o.create_time >= '".strtotime($start_time)."' && o.create_time <= '".strtotime($end_time)."'";
            }else{
                $where .= " and from_unixtime(`create_time`,'%Y-%m-%d') <= '".$end_time."'";
                $where2 .= " and from_unixtime(`start_time`,'%Y-%m-%d') <= '".$end_time."'";
                $where1 .=" and o.create_time <= '".strtotime($end_time)."'";
            }
        }else if($start_time){
            $where .= " and from_unixtime(`create_time`,'%Y-%m-%d') >= '".$start_time."'";
            $where2 .= " and from_unixtime(`start_time`,'%Y-%m-%d') >= '".$start_time."'";
            $where1.= " and o.create_time >= '".strtotime($start_time)."'";
        }

        $channelGroupIds = $this->getParam('channel_group');
        $servers = $this->getParam('server');
        empty($channelGroupIds[0]) && $channelGroupIds = array();
        empty($servers[0]) && $servers = array();
        $serverIds = $model->getServerByServerAndChannelGroup($servers,$channelGroupIds);
        $serverIds = array_keys($serverIds);//获取所有服务器ID

        #获取角色,服务器，央服充值数据
        $sql = "select server_id,count(*) as charge_num,sum(money) as money,role_name,`name`,sum(gold),role_id from ny_order as o
      left join ny_server as s on o.server = s.server_id where s.server_id in ('".join('\',\'',$serverIds)."')
        and o.`status` = 1 and ".$where1." group by role_id order by money desc limit ".intval($list_num)."";

        $row = $model->query($sql);
        $row = $row?$row:array();
        $roleMsg = array();
        foreach($row as $info){
            $serverid = $info['server_id'];
            $role_id = $info['role_id'];
            //获取玩家登录次数
            $sqlLogin = "select role_id,count(*) as login_num,count(DISTINCT(FROM_UNIXTIME(start_time,'%Y-%m-%d'))) as login_day from ny_login where role_id = '".$role_id."' and ".$where2." ";
            //获取消耗元宝数
            $sqldelgold = "select sum(gold) as del_gold FROM ny_payment where role_id = '".$role_id."' and type = 0 and ".$where."";
            //获取新增元宝数
            $sqladdgold = "select sum(gold) as add_gold from ny_payment where role_id = '".$role_id."' and type = 1 and ".$where."";
            //获取充值天数
            $sqlchargeNum = "select count(DISTINCT(FROM_UNIXTIME(create_time,'%Y-%m-%d'))) as charge_day from ny_order where role_id = '".$role_id."' and ".$where."";
            $data = array(
                'login'=>$sqlLogin,
                'delgold'=>$sqldelgold,
                'addgold'=>$sqladdgold,
                'chargenum'=>$sqlchargeNum
            );
            $arr  = array('query' => array('data' => $data));

            $res  = (new ServerconfigModel())->makeHttpParams(array($serverid), $arr);
            $urls = $res['urls'];
            $params = $res['params'];
            $rs = Helper::rolling_curl($urls, $params);
            $result = self::getDatas($rs);
            $res = $result[$serverid];
            $ret = array_merge($res['login'][0],$res['delgold'][0],$res['addgold'][0],$res['chargenum'][0]);
            $roleMsg[$role_id] = $ret;
        }
        //print_r($roleMsg);
        $out = [];
        foreach($row as $k=>&$val){
            $role_id = $val['role_id'];
            $out[$role_id]['role_name']     = $val['role_name'];
            $out[$role_id]['name']          = $val['name'];
            $out[$role_id]['charge_num']    = $val['charge_num'];
            $out[$role_id]['charge_day']    = $roleMsg[$role_id]['charge_day'];
            $out[$role_id]['money']         = $val['money'];
            $out[$role_id]['login_num']     = $roleMsg[$role_id]['login_num'];
            $out[$role_id]['login_day']     = $roleMsg[$role_id]['login_day'];
            $out[$role_id]['add_gold']      = $roleMsg[$role_id]['add_gold'];
            $out[$role_id]['del_gold']      = $roleMsg[$role_id]['del_gold'];
            $num = $roleMsg[$role_id]['add_gold']-$roleMsg[$role_id]['del_gold'];
            $out[$role_id]['last_gold']     = $num>0?$num:0;
        }

        $result = array();
        $result[] = array(
            '角色名' , '服务器','充值次数','充值天数','金额','登录次数','登录天数','产生元宝数','消耗元宝数','剩余元宝数','游戏币/充值花费率'
        );
        foreach($out as $row){
            $result[] = $model->formatFields($row);
        }
        Util::exportExcel($result , '充值前20（' . date('Y年m月d日H时i分s秒') . '）');//保存excel文件
    }

    /**
     * 返回MAC数据,充值MAC，激活MAC
    */
    public function mac(){
        $server = $this->getChannelServer();
        $header = "<strong>*玩家激活MAC，玩家充值MAC</strong>";
        $this->setHeader($header);
        $this->setSearch('日期','range_date');
        $this->setSearch('','scp',array(1,1,1,0));
        $this->setFields(array('日期/年月','激活MAC','充值MAC'));
        $this->setSource('Ce','mac_data');
        $this->displays();
    }

    public function mac_data(){
        $Model = new PackageModel();
        $this->setCondition(0,'date','create_time');
        $this->setCondition(1,'scp','','',array(1,1,1,0));
        $conditions = $this->getConditions();
        $start_time = $conditions['WHERE']['create_time::>='];
        $end_time = $conditions['WHERE']['create_time::<='];
        $where = '1=1';
        if($end_time){
            if($start_time){
                $where .= " and create_time >= '".$start_time."' && create_time <= '".$end_time."'";
            }else{
                $where .= " and create_time <='".$end_time."'";
            }
        }else if($start_time){
            $where .= " and create_time >= '".$start_time."'";
        }
        $channel_id  = (new ChannelModel())->getChannelByPackage($conditions['WHERE']['package::IN']);
        $channel = array();
        foreach($channel_id as $val){
            $channel[$val['channel_id']] = $val;
        }
        $channel_id = array_keys($channel);
        //查询激活MAC
        $lieve_sql = "select count(distinct(mac)) as `count1`,from_unixtime(create_time,'%Y-%m') as `time` from ny_mac where ".$where." and mac !=0 and channel in ('".join('\',\'',$channel_id)."') group by from_unixtime(create_time,'%Y-%m')";
        $liveRow = $Model->query($lieve_sql);
        $liveRow = empty($liveRow)?array():$liveRow;
        $out = $row1 = $row2 = array();
        foreach($liveRow as $live){
            $row1[$live['time']] = $live;
        }
        //查询充值MAC
        $charge_sql = "select count(distinct(mac)) as `count2`,from_unixtime(create_time,'%Y-%m') as `time` from ny_client where role in(select distinct(role_id) from ny_order where  mac !=0 and channel in('".join('\',\'',$channel_id)."')) and ".$where." group by from_unixtime(create_time,'%Y-%m')";
        $chargeRow = $Model->query($charge_sql);
        $chargeRow = empty($chargeRow)?array():$chargeRow;
        foreach($chargeRow as $charge){
            $row2[$charge['time']] = $charge;
        }

        foreach($row1 as $key=>$val){
            $out[$key][] = $val['time'];
            $out[$key][] = $val['count1'];
            $out[$key][] = $row2[$key]['count2'];
        }
        $num = count($out)?count($out):0;
        $Model->setPageData(array_values($out),$num,$conditions['Extends']['LIMIT']);
    }

    /**
     * 跨服获取item配置
     * @version 2017-10-14
    */
    private  static function getItemList(){
        $Model = new Model();
        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = $Model->query($sql);

        $goods= $Model->call('Gift' , 'getItem' , array() , $server[0]['server_id']);//全部物品

        $goods = $goods[$server[0]['server_id']];

        return $goods;
    }

    /**
     * 统计充值区间万家等级
     */
    public function get_charge_level(){
        $total_first = filter_input(INPUT_GET,'first');
        $total_last  = filter_input(INPUT_GET,'last');
        $row = $tmp = [];
        $model = new Model();
        $sql = "select `server`,`role_id` from ny_order where `status` = 1 and `create_time` <= '1515747600' and `server` not like '%9999'
          and `server` not like '%9998' group by `role_id` having sum(`money`) > ".$total_first." and sum(`money`) <= ".$total_last."";
        $row = $model->query($sql);
        if(empty($row))exit('empty role');
        foreach($row as $key=>$val){
            $tmp[$val['server']][] = $val['role_id'];
        }
        /*print_r($tmp);exit;
        file_put_contents('/tmp/server.log',json_encode($tmp)."\n\r",8);exit;*/
        $backMsg['log_num'] = 0;
        $backMsg['rol_num'] = 0;
        //遍历每一条记录
        foreach($tmp as $server_id => $role_id){
            if(empty($server_id) || empty($role_id))continue;
            $roles = array_unique($role_id);
            //单服计算登录天数
            $sqlLogin = "select count(*) as log_num from (select role_id,from_unixtime(`start_time`,'%Y-%m-%d') from ny_login
          where `role_id` in ('".join('\',\'',$roles)."') group by from_unixtime(`start_time`,'%Y-%m-%d')) as t";
            //单服计算角色总等级
            $sqlLevel = "select sum(`role_level`) as rol_num from ny_role where `role_id` in('".join('\',\'',$roles)."')";

            $data = array(
                'role_login'=>$sqlLogin,
                'role_level'=>$sqlLevel
            );
            $arr  = array('query' => array('data' => $data));

            $res  = (new ServerconfigModel())->makeHttpParams(array($server_id), $arr);
            $urls = $res['urls'];
            $params = $res['params'];
            $rs = Helper::rolling_curl($urls, $params);
            $result = self::getDatas($rs);
            if(empty($result) || !is_array($result))continue;
            //file_put_contents('/tmp/server.log',json_encode($result)."\n\r",8);
            $logNum = $result[$server_id]['role_login'][0]['log_num']?(int)$result[$server_id]['role_login'][0]['log_num']:0;
            $rolNum = $result[$server_id]['role_level'][0]['rol_num']?(int)$result[$server_id]['role_level'][0]['rol_num']:0;
            $backMsg['log_num'] += $logNum;
            $backMsg['rol_num'] += $rolNum;
        }
        print_r($backMsg);
    }
    /**
     * 格式化返回数据
     * @param array $arr
     * @return array $tmp
    */
    private static function getDatas($arr) {
        $tmp = array();
        foreach ($arr as $server_id => $json) {
            $res = json_decode($json, true);

            if (!is_array($res)) {
                echo PHP_EOL . $server_id . ' == ' .PHP_EOL;
                print_r($json);
                echo PHP_EOL;
                continue;
            }
            $code = $res['code'];

            if ($code != 1) {
                continue;
            }
            $data = $res['data']['query'];
            $tmp[$server_id] = $data;
        }
        return $tmp;
    }

}