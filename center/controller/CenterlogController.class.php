<?php
if(!defined('IN_WEB')) {
	exit('Access Denied');
}

class CenterlogController extends Controller{
    function __construct(){
        parent::__construct();
    }
    
    public function index(){
    	 
    	$this->setSearch('查询日期' , 'range_time');
    	$this->setSearch('管理员');
    	$this->setSearch('访问ip');
    	$this->setFields(array(
    			'ID' , '模块','操作','描述','管理员','IP','时间'
    	));
    	 
    	$this->setSource('centerlog' , 'centerlog_data' ,0);
    	$this->displays();
    }
    
    public function centerlog_data(){
    	$this->setCondition(0 , 'time' , 'created');
    	$this->setCondition(1 , 'eq' , 'username');
    	$this->setCondition(2 , 'eq' , 'ip');
    	$this->setOrder('created');
    	$this->setLimit();
    	$method = $this->setDataExport('centerlog_data' , 'centerlog_export');
    	$this->call('CenterlogModel' , $method , $this->getConditions());
    }
    
}