<?php
!defined('IN_WEB') && exit('Access Denied');

class ChannelController extends Controller{
    private $cm;
    private $pm;

    public function __construct(){
        parent::__construct();
        $this->cm = new ChannelModel();
        $this->pm = new PackageModel();
    }

    //渠道管理（菜单）
    public function index(){
        $tabs = array(
            array('title' => '渠道列表', 'url' => 'admin.php?&ctrl=channel&act=record'),
            array('title' => '添加渠道', 'url' => 'admin.php?&ctrl=channel&act=add'),
            array('title' => '编辑渠道', 'url' => 'admin.php?&ctrl=channel&act=edit'),
            array('title' => '渠道组列表', 'url' => 'admin.php?&ctrl=channelgroup&act=record'),
            array('title' => '添加渠道组', 'url' => 'admin.php?&ctrl=channelgroup&act=add'),
            array('title' => '编辑渠道组', 'url' => 'admin.php?&ctrl=Channelgroup&act=edit')
        );
        $this->tabs($tabs);
    }

    //渠道列表（子菜单）
    public function record(){
        $this->setSearch('渠道标识');
        $this->setSearch('渠道名称');
        $this->setFields(array('标识' , '名称' , '创建时间' , '操作'));
        $this->setSource('channel' , 'record_data');
        $js = <<< END
            function edit(cid){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=channel&act=edit&cid=' + cid);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=channel&act=edit&cid=' + cid);
            }
            function del(cid){
            if(confirm('删除该渠道？')){
                $.ajax({
                    url: 'admin.php?ctrl=channel&act=del_action&cid=' + cid,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(){
                    \$tabs.tabs('load' , 0);
                })
                window.location.reload();
                }
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    //添加渠道（数据）
    public function record_data(){
        $this->setCondition(0,'like','channel_id');
        $this->setCondition(1,'like','name');
        $this->setLimit();
        $this->call('ChannelModel' , 'record_data' , $this->getConditions());
    }

    //添加渠道（子菜单）
    public function add(){
    	$this->usergroupAction = new UsergroupAction();
    	$groupList = $this->usergroupAction->groupGetList();
    	$this->smarty->assign('groupList' , $groupList);
        $this->smarty->display('channel/add.tpl');
    }

    public function add_action(){
        $channel_id = $this->getParam('channel_id');
        if($channel_id){
            $name = $this->getParam('name');
            $game_id = (int)$this->getParam('game_id');
            $game_name = trim($this->getParam('game_name'));
            $game_abbreviation = trim($this->getParam('game_abbreviation'));
            $game_secret_key = trim($this->getParam('game_secret_key'));
            $cdn_url = trim($this->getParam('cdn_url'));

            $groupList = $this->getParam('groupList');
            $data = array(
                'channel_id' => $channel_id,
                'name' => $name,
                'game_id' => $game_id,
                'game_name' => $game_name,
                'game_abbreviation' => $game_abbreviation,
                'game_secret_key' => $game_secret_key,
                'cdn_url' => $cdn_url,
                'create_time' => time(),
                'grouplist' => json_encode($groupList),
            );
            
            $this->usergroupAction = new UsergroupAction();
            if ($groupList) {
            	foreach ($groupList as $value) {
            		$one = $this->usergroupAction->groupGetOne(intval($value));
            
            		if ($one['channel']) {
            			if ( $one['channel'] != 'all' && !in_array($channel_id, array_keys($one['channel']))) {
            				$one['channel'][$channel_id] = 'all';
            				$update['groupid'] = intval($value);
            				$update['channel'] = json_encode($one['channel']);
            				$this->usergroupAction->usergroupUpdate($update);
            			}
            
            		}else {
            			$one['channel'][$channel_id] = 'all';
            			$update['groupid'] = intval($value);
            			$update['channel'] = json_encode($one['channel']);
            			$this->usergroupAction->usergroupUpdate($update);
            		}
            	}
            } else {
                $data['groupList'] = '["1"]';
            }
            
            if($this->cm->getChannel($channel_id)){
                $json = array('msg' => '渠道标识已存在' , 'code' => 0);
            }
            else if(!preg_match('/^[0-9a-zA-Z_\-]+$/' , $channel_id)){
                $json = array('msg' => '渠道标识格式不正确' , 'code' => 0);
            }
            else if($this->cm->addChannel($data)){
            	(new LogAction())->logAdd("添加渠道：{$channel_id}");
                $json = array('msg' => '添加成功' , 'code' => 1);

                //执行成功后清楚redis服务器缓存
                (new ServerRedis(true))->delServerKey();
            }
            else{
                $json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
            }
            echo json_encode($json);
        }
    }

    //编辑渠道（子菜单）
    public function edit(){
        $channel_id = $this->getParam('cid');
        $channel = $this->cm->getChannel($channel_id);
        if($channel){
            $channel_id && $this->smarty->assign('channel_id' , $channel_id);
            $channel['num'] && $this->smarty->assign('num' , $channel['num']);
            $channel['name'] && $this->smarty->assign('name' , $channel['name']);
            $channel['game_id'] && $this->smarty->assign('game_id', $channel['game_id']);
            $channel['game_name'] && $this->smarty->assign('game_name', $channel['game_name']);
            $channel['game_abbreviation'] && $this->smarty->assign('game_abbreviation', $channel['game_abbreviation']);
            $channel['game_secret_key'] && $this->smarty->assign('game_secret_key', $channel['game_secret_key']);
            $channel['cdn_url'] && $this->smarty->assign('cdn_url', $channel['cdn_url']);
            $channel['grouplist'] && $this->smarty->assign('groupuser' , json_decode($channel['grouplist'],true));
            $groupuserstr = $channel['grouplist'] ? implode(',', json_decode($channel['grouplist'])) : '';
            $channel['grouplist'] && $this->smarty->assign('groupuserstr', $groupuserstr, true);
        }
        $this->usergroupAction = new UsergroupAction();
        $groupList = $this->usergroupAction->groupGetList();
        $this->smarty->assign('groupList' , $groupList);
        $this->smarty->display('channel/edit.tpl');
    }

    public function edit_action(){
        $channel_id = $this->getParam('channel_id');
        if($channel_id){
            $old_channel_id = $this->getParam('old_channel_id');
            $name = $this->getParam('name');
            $game_id = (int)$this->getParam('game_id');
            $game_name = trim($this->getParam('game_name'));
            $game_abbreviation = trim($this->getParam('game_abbreviation'));
            $game_secret_key = trim($this->getParam('game_secret_key'));
            $cdn_url = trim($this->getParam('cdn_url'));
            $groupList = $this->getParam('groupList');
            $data = array(
                'channel_id' => $channel_id,
                'name' => $name,
                'game_id' => $game_id,
                'game_name' => $game_name,
                'game_abbreviation' => $game_abbreviation,
                'game_secret_key' => $game_secret_key,
                'cdn_url' => $cdn_url,
                'grouplist' => json_encode($groupList),
            );

            if (!$groupList) {
                $data['groupList'] = '["1"]';
            }

            if(!preg_match('/^[0-9a-zA-Z_\-]+$/' , $channel_id)){
                $json = array('msg' => '渠道标识格式不正确' , 'code' => 0);
            }
            else if($this->cm->editChannel($data , $old_channel_id)){
                $this->pm->editPackages(array('channel_id' => $channel_id) , array('channel_id' => $old_channel_id));
                $json = array('msg' => '保存成功' , 'code' => 1);
                //执行成功后清楚redis服务器缓存
                (new ServerRedis(true))->delServerKey();
                (new LogAction())->logAdd("编辑渠道：{$old_channel_id}=>{$channel_id}");
            }
            else{
                $json = array('msg' => '没有任何改变' , 'code' => 0);
            }
            
            $this->usergroupAction = new UsergroupAction();
            if ($groupList) {
            	foreach ($groupList as $value) {
            		$one = $this->usergroupAction->groupGetOne(intval($value));
            		if ($one['channel']) {
                        if ($one['channel'] != 'all' && !in_array($channel_id, array_keys($one['channel']))) {
            				$one['channel'][$channel_id] = 'all';
            				$update['groupid'] = intval($value);
            				$update['channel'] = json_encode($one['channel']);
            				$this->usergroupAction->usergroupUpdate($update);
            			}
            
            		}else {
            			$one['channel'][$channel_id] = 'all';
            			$update['groupid'] = intval($value);
            			$update['channel'] = json_encode($one['channel']);
            			$this->usergroupAction->usergroupUpdate($update);
            		}
            	}
            
            }
            
            echo json_encode($json);
        }
    }

    public function del_action(){
        $channel_id = $this->getParam('cid');
        $channel_id && $this->cm->delChannel($channel_id);
        (new LogAction())->logAdd("删除渠道：{$channel_id}");
    }
}