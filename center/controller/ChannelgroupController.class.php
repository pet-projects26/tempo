<?php
!defined('IN_WEB') && exit('Access Denied');

class ChannelGroupController extends Controller{
    private $cgm;
    private $cm;
    private $pm;
    private $db;

    public function __construct(){
        parent::__construct();
        $this->cgm = new ChannelgroupModel();
        $this->cm = new ChannelModel();
        $this->pm = new PackageModel();
    }

    //渠道组列表（子菜单）
    public  function record(){
        $this->setFields(array('id','名称' , '创建时间' , '操作'));
        $this->setSource('channelgroup' , 'record_data');
        $js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 5);
                \$tabs.tabs('url' , 5 , 'admin.php?ctrl=channelgroup&act=edit&id=' + id);
                \$tabs.tabs('load' , 5);
                \$tabs.tabs('url' , 5 , 'admin.php?ctrl=channelgroup&act=edit&id=' + id);
            }
            function del(id){
                if(confirm('确定删除？')){
                $.ajax({
                    url: 'admin.php?ctrl=channelgroup&act=del_action&id=' + id,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(){
                    \$tabs.tabs('load' , 3);
                })
        		  window.location.reload();
                }
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    //渠道组列表（数据）
    public function record_data(){
        $this->setLimit();
        $this->call('ChannelgroupModel' , 'record_data' , $this->getConditions());
    }

    //添加渠道组（子菜单）
    public function add(){
        $this->smarty->display('channelgroup/add.tpl');
    }

    public function add_action(){
            $name = $this->getParam('name');
            $data = array(
                'name' => $name,
                'create_time' => time()
            );
            if($this->cgm->addChannelGroup($data)){
            	(new LogAction())->logAdd("添加渠道组：{$name}");
                $json = array('msg' => '添加成功' , 'code' => 1);
            }
            else{
                $json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
            }
            echo json_encode($json);
    }

    //编辑渠道组（子菜单）
    public function edit(){
        $id = $this->getParam('id');
        $group = $this->cgm->getChannelGroup($id);
        if($group){
            $group['id'] && $this->smarty->assign('id' , $group['id']);
            $id && $this->smarty->assign('id' , $id);
            $group['name'] && $this->smarty->assign('name' , $group['name']);

            $rsc = $this->cm->getChannel(array() , array('channel_id' , 'name'));
            $rsp = $this->pm->getPackage(array() , array('channel_id' , 'package_id'));
            $channels = array();
            foreach($rsc as $row){
                $channels[$row['channel_id']] = $row['name'];
            }
            $channel_package = array();
            if($id){
                $rscg = $this->cgm->getChannelGroup($id , array('channel_package'));
                $rscg = json_decode($rscg['channel_package'] , true);
                if($rscg){
                    foreach($rscg as $channel_id => $package){
                        foreach($package as $package_id){
                            $channel_package[] = $channel_id . '_' . $package_id;
                        }
                    }
                }
            }
            $packages = array();
            foreach($rsp as $row){
                $val = $row['channel_id'] . '_' . $row['package_id'];
                $checked = in_array($val , $channel_package) ? 1 : 0;
                $packages[$channels[$row['channel_id']]][]  = array('name' => $row['package_id'] , 'value' => $val , 'checked' => $checked);
            }
            $this->smarty->assign('id' , $id);
            $this->smarty->assign('packages' , $packages);
            $this->smarty->assign('channel_package' , $channel_package);
        }
        $this->smarty->display('channelgroup/edit.tpl');
    }

    public function edit_action(){
        $id = $this->getParam('id');
        $name = $this->getParam('name');
        $package = $this->getParam('package');
        $channel_package = array();
        if($package){
            foreach($package as $row){
                $cp = explode('_' , $row);
                $channel_package[$cp[0]][] = $cp[1];
            }
        }
        $data = array('id' => $id , 'name' => $name , 'channel_package' => json_encode($channel_package));
        if(!preg_match('/^[0-9a-zA-Z]+$/' , $id)){
            $json = array('msg' => '渠道组标识格式不正确' , 'code' => 0);
        }
        else if($this->cgm->editChannelGroup($data , $id)){
            $json = array('msg' => '保存成功' , 'code' => 1);
        }
        else{
            $json = array('msg' => '没有任何改变' , 'code' => 0);
        }
        echo json_encode($json);
    }

    public function del_action(){
        $id = $this->getParam('id');
        $id && $this->cgm->delChannelGroup($id);
        (new LogAction())->logAdd("删除渠道组：{$id}");
    }

    public function open_action(){
        $id = $this->getParam('id');
        $server_id = $this->getParam('server_id');

        $id = intval($id);

        $this->db = new Model();

        $sql = "UPDATE ny_server SET `group_id` = '{$id}' WHERE `server_id` = '{$server_id}'";

        $this->db->query($sql);

        $channel_group = $this->cgm->getChannelGroup(array() , array('id' , 'server'));
        $servers = array();
        foreach($channel_group as $k => $row){
            if(!empty($row['server'])){
                $row['server'] = json_decode($row['server'] , true);
                $num = array_keys($row['server'] , $server_id); //判断是否在数组，如果有则返回在数组的位置
                if($num){
                    if($row['id'] == $id){
                        $json = array('msg' => '该服务器已开通该渠道组' , 'code' => 0);
                        exit(json_encode($json));
                    }
                    else{
                        unset($row['server'][$num[0]]);
                        $data = array('server' => json_encode(array_values($row['server'])));
                        $this->cgm->editChannelGroup($data , $row['id']);
                    }
                }
            }
            $servers[$row['id']] = $row['server'];
        }
        $servers[$id][] = $server_id;
        file_put_contents('logs/server.txt' , json_encode($servers));
        $data = array('server' => json_encode($servers[$id]));
        

        if($this->cgm->editChannelGroup($data , $id)){
            $json = array('msg' => '保存成功' , 'code' => 1);
        }
        else{
            $json = array('msg' => '保存未成功，请再次尝试' , 'code' => 0);
        }

        die(json_encode($json));
    }


   
}