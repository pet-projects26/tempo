<?php
!defined('IN_WEB') && exit('Access Denied');

class ChargeController extends Controller
{
    private $cm;

    public function __construct()
    {
        parent::__construct();
        $this->cm = new ChargeModel();
    }

    //充值分布
    public function chargeDistribute()
    {
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));

        $this->setFields(array(
            '日期', '总付费玩家',
            '[0,100]',
            '[100,200]',
            '[200,500]',
            '[500,1000]',
            '[1000,2000]',
            '[2000,5000]',
            '[5000,10000]',
            '[10000,20000]',
            '[20000,&]',
        ));


        $this->smarty->assign('select', 'select');
        $this->setSource('charge', 'chargeDistribute_data', 1);
        $this->displays();
    }

    public function chargeDistribute_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time');
        $this->setLimit(1000000);

        $method = $this->setDataExport('chargeDistribute_data', 'chargeDistribute_export');
        $this->call('ChargeDistributeModel', $method, $this->getConditions());
    }

    public function index()
    {
        $tabs = array(
            array('title' => '充值记录', 'url' => 'admin.php?ctrl=charge&act=rank'),
            array('title' => '充值记录(区服)', 'url' => 'admin.php?ctrl=charge&act=rank_cs')
        );
        $this->tabs($tabs);
    }

    //充值记录（菜单）
    public function record()
    {
        //设置页面头部内容
        $header = '说明：<br>';
        $header .= '1. 订单状态分为两种状态，成功表示订单正常交易成功，失败则说明订单的正常流程中途出错，例如充值不到账等';
        $this->setHeader($header);
        //设置搜索项，按照页面显示的顺序设置
        $this->setSearch('角色名');
        $this->setSearch('角色id');
        $this->setSearch('账号');
        $this->setSearch('日期', 'range_time');//范围
        //$this->setSearch('元宝', 'range');
        $this->setSearch('订单号', 'input', '', '200px');
        $this->setSearch('第三方订单号');
        $this->setFields(array('渠道', '区服', '包号', '包名', '第三方订单号'));
        $this->setSearch('订单状态', 'select', CDict::$chargeStatus);
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        //设置显示项，按照页面显示的顺序设置
        $this->setField('订单号', 100);
        $this->setFields(array('账号', '角色名', '角色id', '等级', '金额', '充值档位')); //如果显示项只需要设置名称，则可使用setFields方法批量设置显示项
        $this->setField('时间', 100);
        $this->setField('订单状态');
        //设置获取页面数据的类方法
        $this->setSource('charge', 'record_data', 1);

        //显示页面
        $this->displays();
    }

    public function record_data()
    {
        //设置搜索条件
        $this->setCondition(0, 'eq', 'role_name');
        $this->setCondition(1, 'eq', 'role_id');
        $this->setCondition(2, 'eq', 'account');
        $this->setCondition(3, 'time', 'o.create_time');
        //$this->setCondition(3, 'between', 'gold');
        $this->setCondition(4, 'eq', 'order_num');
        $this->setCondition(5, 'eq', 'corder_num');
        $this->setCondition(6, 'eq', 'o.status');
        $this->setCondition(7, 'scp');
        //$this->setCondition(7 , 'eq','order',1);
        //设置搜索排序
        $this->setOrder('o.id', 'desc', array(5 => array('money'), 6 => array('create_time')));
        //设置搜索条数限制
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('ChargeModel', $method, $this->getConditions());
    }

    //每日充值（菜单）
    public function daily()
    {
        $header = '说明：<br>';
        $header .= '1. 新增充值人数：当天新增玩家并在当天进行充值的角色数 ';
        $header .= '2. 充值人数：当天充值的人数';
        $header .= '3. 新增充值金额： 新增充值人数当天充值的金额';
        $header .= '4. 登录ARPU = 充值总额 / 登录人数 ';
        $header .= '4. 新增充值ARPU = 新增充值总额 / 充值人数 ';
        $header .= '4. ARPU = 充值总额 / 充值人数 ';
        $header .= '4. 注册ARPU = 充值总额 / 注册人数 ';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        //$this->setSearch('' , 'scp' , array(1 , 1 , 1 , 1));
        $this->setFields(array('日期', '充值总额', '充值人数', '充值次数', '首冲总额', '首冲人数', '首冲次数', 'ARPU', '创角数', '登录人数'));
        $this->setSource('charge', 'daily_data', 1);
        $this->displays();
    }

    public function daily_data()
    {
        $this->setCondition(0, 'date', 'date');
        $this->setCondition(1, 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('daily_data', 'daily_export');
        $this->call('ChargedailyModel', $method, $this->getConditions());
    }

    //充值等级（菜单）
    public function level()
    {
        $tabs = array(
            array('title' => '充值等级', 'url' => 'admin.php?&ctrl=charge&act=level_list'),
            array('title' => '充值等级折线图', 'url' => 'admin.php?&ctrl=charge&act=level_chart')
        );
        $this->tabs($tabs);
    }

    public function level_list()
    {
        $header = '说明：<br>';
        $header .= '1. 付费率 = 充值人数 / 登录人数 <br>';
        $header .= '2. 等级间隔 指假设等级间隔为N，等级范围的每N个等级分为一个区间，默认是N等于10<br>';
        $header .= '3. 对比昨天 指今天与昨天（或者指定日期与指定日期的昨天）的充值总额的差值对比，单位是元';
        $this->setHeader($header);
        $this->setSearch('日期', 'date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('等级范围', 30);
        $this->setFields(array('登录人数', '充值人数', '充值次数', '付费率', '充值总额', '对比昨天'));
        $this->setSource('charge', 'level_data', 1);
        $this->displays();
    }

    public function level_data()
    {
        $this->setCondition(0, 'eq', "from_unixtime(start_time,'%Y-%m-%d')");
        $this->setCondition(1, 'scp');
        $this->setOrder('role_level');
        $this->setLimit();
        $method = $this->setDataExport('level_data', 'level_export');
        $this->call('ChargeModel', $method, $this->getConditions());
    }

    public function level_chart()
    {
        $conditions = $this->getSession('conditions');

        if (isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            unset($conditions['Extends']['LIMIT']);
            $data = $this->call('ChargeModel', 'getLevelChargeData', array('conditions' => $conditions), $server_id);

            $data = $data[$server_id][0];
            $times_data = array();
            $gold_data = array();

            if (count($data)) {

                foreach ($data as $k => $row) {

                    $times_data['x'][] = $row[0];
                    $times_data['y'][0][] = intval($row[3]); // 充值次数

                    $gold_data['x'][] = $row[0];
                    $gold_data['y'][0][] = doubleval($row[5]);

                }
            } else {
                $times_data['x'] = array();
                $times_data['y'][0] = array();

                $gold_data['x'] = array();
                $gold_data['y'][0] = array();
            }


            $size = array('x' => 3);
            $name = array('充值次数');
            $title = array('main' => '充值次数', 'x' => '充值等级', 'y' => '充值次数');
            $times_chart = init_highchart_line('times_chart', $title, $times_data, $name);

            $name = array('充值总额');
            $title = array('main' => '充值总额', 'x' => '充值等级', 'y' => '充值总额');
            $gold_chart = init_highchart_line('gold_chart', $title, $gold_data, $name);

            $this->smarty->assign('times_chart', $times_chart);
            $this->smarty->assign('gold_chart', $gold_chart);
            $this->smarty->display('charge/level_chart.tpl');
        } else {
            echo '<div style="min-width:400px;height:20px;padding:10px 15px;margin:6px auto;border:solid 1px #ABABAB;border-radius:5px;">请先在充值等级页面选择服务器</div>';
        }
    }

    //充值排行（菜单）
    public function rank()
    {

        $header = '说明：<br>';
        $header .= '1. 平均充值周期（在没有填写搜索项的日期的时候） = (当前时间 - 角色创建时间) / 从角色创建时间到当前时间的充值次数<br>';
        $header .= '2. 平均充值周期（在填写搜索项的日期的开始部分的时候） = (当前时间 - 开始时间) / 从开始时间到当前时间的充值次数<br>';
        $header .= '3. 平均充值周期（在填写搜索项的日期的开始部分和结束部分的时候） = (结束时间 - 开始时间) / 从开始时间到结束时间的充值次数<br>';
        $header .= '4. 排名 根据 充值总金额 排序';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('金额', 'range');
        $this->setSearch('职业');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('排名', 10);
        $this->setField('账号', 40);
        $this->setField('角色名', 30);
        $this->setField('职业', 20);
        $this->setField('等级', 20);
        $this->setField('单次最高充值金额', 40);
        $this->setField('充值总金额', 30);
        $this->setField('元宝总额', 40);
        $this->setField('充值次数', 30);
        $this->setField('最后充值时间', 80);
        $this->setField('最后登录时间', 80);
        $this->setField('平均充值周期', 60);
        $this->setSource('charge', 'rank_data', 1);

        $this->smarty->assign('select', 'radio');

        $this->displays();
    }

    public function rank_data()
    {
        $this->setCondition(0, 'time', 'o.create_time');
        $this->setCondition(1, 'between', 'o.money');
        $this->setCondition(2, 'eq', 'o.role_career');
        $this->setCondition(3, 'scp');
        $this->setOrder('sum_money', 'desc', array(array('sum_money', 'DESC'), array('max_money', 'DESC')));
        $this->setLimit();
        $method = $this->setDataExport('rank_data', 'rank_export');
        $this->call('ChargeModel', $method, $this->getConditions());
    }

    public function rank_cs()
    {
        $header = '说明：<br>';
        $header .= '1. 排名 根据 充值总金额 排序';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_time', array(date('Y-m-d H:i:s', mktime('0', '0', '0', date('m'), 1, date('Y'))), date('Y-m-d H:i:s', time())), 150);
        $this->setSearch('金额', 'range');
        $this->setSearch('职业');
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        $this->setField('排名', 10);
        $this->setField('服务器', 10);
        $this->setField('账号', 40);
        $this->setField('角色名', 30);
        $this->setField('职业', 20);
        $this->setField('等级', 20);
        $this->setField('单次最高充值金额', 40);
        $this->setField('充值总金额', 30);
        $this->setField('元宝总额', 40);
        $this->setField('充值次数', 30);
        $this->setField('最后充值时间', 80);
        $this->setSource('charge', 'rank_cs_data', 1);
        $this->displays();
    }

    public function rank_cs_data()
    {
        $this->setCondition(0, 'time', 'o.create_time');
        $this->setCondition(1, 'between', 'o.money');
        $this->setCondition(2, 'eq', 'o.role_career');
        $this->setCondition(3, 'scp');
        $this->setOrder('sum_money', 'desc', array(array('sum_money', 'DESC'), array('max_money', 'DESC')));
        $this->setLimit();
        $method = $this->setDataExport('rank_cs_data', 'rank_cs_export');
        $this->call('ChargeModel', $method, $this->getConditions());
    }

    //新增玩家（菜单）
    public function cnew()
    {
        $header = '说明<br>';
        $header .= '1. 新增充值总额 指新增玩家当日的充值总额<br>';
        $header .= '2. 新增付费率 = 新增首充人数 / 新增人数 ';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setFields(array('日期', '新增首充人数', '新增注册数', '新增充值金额', '新增付费率'));
        $this->setSource('charge', 'cnew_data', 1);
        $this->displays();
    }

    public function cnew_data()
    {
        $this->setCondition(0, 'date', 'date');
        $this->setCondition(1, 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('cnew_data', 'cnew_export');
        $this->call('ChargenewModel', $method, $this->getConditions());
    }

    //ARPU（菜单）
    public function arpu()
    {
        $header = '说明：<br>';
        $header .= '1. ARPU = 充值金额 / 登录人数 <br>';
        $header .= '2. ARPPU = 充值金额 / 充值人数 <br>';
        $header .= '3. 首充ARPU = 首充金额 / 首充人数 <br>';
        $header .= '4. 注册ARPU = 充值金额 / 注册人数 ';
        $this->setHeader($header);
        $month = array();
        $month[0] = CDict::$month;
        krsort(CDict::$month);
        $month[1] = CDict::$month;
        $this->setSearch('', 'select', array('0' => '日', '1' => '月'), 120, 'time_select');
        $this->setSearch('日期', 'range_date', '', 120, 'day_select');
        $this->setSearch('日期', 'selects', $month, 120, 'month_select');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setFields(array('日期', 'ARPU', 'ARPPU', '首充ARPU', '注册ARPU'));
        $this->setSource('charge', 'arpu_data', 1);
        $js = <<< END
        $(function(){
            $('.day_select').show();
            $('.month_select').hide();
            $('.time_select select').change(function(){
                if($(this).val() == "1"){
                    $('.day_select').hide();
                    $('.month_select').show();
                }
                else{
                    $('.day_select').show();
                    $('.month_select').hide();
                }
            });
        });
END;
        $this->setJs($js);
        $this->displays();
    }

    public function arpu_data()
    {
        $conditions = array();
        //where
        $s0 = trim($this->getParam('sSearch_0'));
        if ($s0) {
            //月
            $s2 = trim($this->getParam('sSearch_2'));//create_time
            $splits = explode('|', $s2);
            $s = trim($splits[0]);
            $s = ($s < 10) ? '0' . $s : $s;
            $conditions['WHERE']['date::>='] = strtotime(date('Y-' . $s));
            $e = trim($splits[1]);
            if ($e == 12) {
                $time = 'Y-' . $e . '-31 24:00:00';
            } else {
                $e += 1;
                $e = ($e < 10) ? '0' . $e : $e;
                $time = 'Y-' . $e;
            }
            $conditions['WHERE']['date::<='] = strtotime(date($time));
            $this->setConditions($conditions);
        } else {
            //日
            $this->setCondition(1, 'date', 'date');
        }
        $this->setCondition(3, 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('arpu_data', 'arpu_export');
        $this->call('ArpuModel', $method, $this->getConditions());
    }

    public function manual_charge()
    {
        $tabs = array(
            array('title' => '测试充值列表', 'url' => 'admin.php?ctrl=charge&act=manuallist'),
            array('title' => '测试充值操作', 'url' => 'admin.php?ctrl=charge&act=manual')
        );
        $this->tabs($tabs);
    }

    //手动充值（菜单）
    public function manual()
    {

        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
/*
        $serverid = (new ServerModel())->getLatestServer();
        $serverid = 'local';

        $charge = $this->call('ChargeModel', 'recharge', ['output' => 1], $serverid);

        $charge = $charge[$serverid];

        $this->smarty->assign('charge', $charge); */
        $this->smarty->display('charge/manual.tpl');
    }

    public function manual_action()
    {

        $role_name = trim($this->getParam('role_name'));

        $server = $this->getParam('server');
        $corder_num = $this->getParam('corder_num');
        $serverid = $server;
        $server = array($server);
        $admin = $_SESSION['username'];

        $item_id = intval($this->getParam('item'));

//        $sql = "select account,role_id from ny_role where name = '$role_name'";
//
//        $arr = array('query' => array('data' => array($sql)));
//        $res = (new ServerconfigModel())->makeHttpParams($server, $arr);
//
//        $urls = $res['urls'];
//        $param = $res['params'];
//        $res = Helper::rolling_curl($urls, $param);
//
//        $data = json_decode($res[$server[0]], true);
//
//        $data = $data['data']['query'][0][0];

        //根据角色名找到其role_id
        $data = $this->call('RoleModel', 'getRoleInfoToType', array('type' => 'name', 'value' => $role_name, 'fields' => ['account', 'role_id']), $serverid);

        $data = $data[$serverid];

        $role_id = $data['role_id'];

        if (empty($role_id)) {

            $json = array('msg' => '找不到该角色1，请核实名称或区服');

        } else {

            $charge = $this->call('ChargeModel', 'recharge', ['output' => 1], $serverid);

            $charge = $charge[$serverid];

            $rs = array(
                'server' => $server[0],
                'money' => $charge[$item_id]['price'],
                'account' => $data['account'],
                'role_id' => $role_id,
                'role_name' => $role_name,
                'item_id' => $item_id,
                'first' => 0,
                'create_time' => time(),
                'admin' => $admin,
                'order_num' => $this->makeOrder($role_id),
                'corder_num' => $corder_num
            );

            $result = (new VirtualOrderModel())->add($rs);

            if ($result) {

                $json = array('msg' => '添加成功，请等待审核');

            } else {

                $json = array('msg' => '添加失败，请重试');
            }

        }

        echo json_encode($json);

    }

    //补单
    public function order()
    {
        $order_id = $this->getParam('order_id');
        $orderModel = new OrderModel();
        $conditions = array();
        $conditions['WHERE']['order_num'] = $order_id;
        $order = $orderModel->getRow('*', $conditions['WHERE']);
        $data = array(
            'order_num' => $order['order_num'],
            'corder_num' => $order['corder_num'],
            'money' => $order['money'],
            'account' => $order['account'],
            'role_id' => $order['role_id'],
            'role_name' => $order['role_name'],
            'role_level' => $order['role_level'],
            'role_career' => $order['role_career'],
            'gold' => $order['gold'],
            'status' => 1,
            'first' => $order['first'],
            'create_time' => $order['create_time']
        );
        $params = json_decode($order['params'], true);
        $item_id = $params['product_id'];
        $server = $order['server'];
        $chargeModel = new chargeModel();
        $chargeModel->supplementOrder($data, $item_id, $server);

        $fields = array('status');
        $neworder = $orderModel->getRow($fields, $conditions['WHERE']);
        echo $neworder['status'];
    }

    public function manuallist()
    {
        $this->setSearch('账号');
        $this->setSearch('角色ID');
        $this->setSearch('角色');
        $this->setSearch('金额');
        $this->setSearch('', 'scp', array(1, 0, 1, 0));
        $this->setFields(array('服务器', '渠道订单号', '账号', '角色ID', '角色名', '金额', '申请者', '创建时间', '审核'));
        $this->setSource('charge', 'manuallist_data', 1);

        $js = <<< END
            function _checked(id){
                if(confirm('确定1审核？')){
                    $.ajax({
                        url: 'admin.php?ctrl=charge&act=check&id='+id,
                        type: 'GET',
                        dataType: 'JSON',
                        beforeSend:function(){
                            $(".checked").attr({disabled:"disabled"});
                        },
                        complete:function(){
                            $(".checked").removeAttr('disabled');
                        }

                    }).done(function(data){
                        $.dialog.tips(data.msg);
                        window.location.reload();
                    });
                }
            }
            
END;

        $this->setJs($js);

        $this->displays();
    }

    public function manuallist_data()
    {
        $this->setCondition(0, 'eq', 'account');
        $this->setCondition(1, 'eq', 'role_id');
        $this->setCondition(2, 'eq', 'role_name');
        $this->setCondition(3, 'eq', 'money');
        $this->setCondition(4, 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('manuallist_data', 'manuallist_export');
        $this->call('VirtualOrderModel', $method, $this->getConditions());
        // $this->call('VirtualOrderModel' , 'manuallist_data' , $this->getConditions());
    }

    public function check()
    {

        $id = $this->getParam('id');

        $result = (new VirtualOrderModel())->getRow('*', array('id' => $id));

        $rs = $this->cm->manual($result);

        if ($rs == 1) {
            $json = array('msg' => '充值成功');
        } else {
            $json = array('msg' => '充值失败');
        }
        (new VirtualOrderModel())->update(array('checked' => $rs), array('id' => $id));
        echo json_encode($json);
    }

    //充值档位统计
    public function charge_index()
    {
        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(1, 0, 1, 0));
        $this->setField('日期', 100);

        $this->setFields(array('渠道', '服务器', '档位ID', '档位描述', '档位金额', '充值人数', '充值次数', '总金额', '充值档次占比'));
        $this->setSource('Charge', 'charge_index_data', 1);
        $this->displays();
    }

    public function charge_index_data()
    {
        $this->setCondition(0, 'date', 's.time');
        $this->setCondition(1, 'scp', '', '', array(1, 0, 1, 0));
        $this->setOrder('s.index', 'asc');
        $this->setLimit();
        $method = $this->setDataExport('charge_index_data', 'charge_index_export');
        $this->call('ChargeIndexModel', $method, $this->getConditions());
    }

    public function getServerCharge()
    {
        $server = $this->getParam('server');

        $json = [
            'code' => 400,
            'msg' => 'server为空'
        ];

        if (!$server) {
            echo json_encode($json);
            exit;
        }

        $charge = $this->call('ChargeModel', 'recharge', ['output' => 1], $server);

        $charge = $charge[$server];

        if (empty($charge)) {
            $json['msg'] = '获取充值档位失败';
            echo json_encode($json);
            exit;
        }


        $json['code'] = 200;
        unset($json['msg']);

        $json['charge'] = $charge;

        echo json_encode($json);
    }


}