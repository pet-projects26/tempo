<?php
!defined('IN_WEB') && exit('Access Denied');

class ChargetotalController extends Controller{
    public function __construct(){
        parent::__construct();
    }
    public function record(){
		
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
		$this->setField('开服日期',800);
		$this->setField('区服',400);
		$day = array();
		for($i=1;$i<=30;$i++){
			$day[] = $i;
		}
		$this->setFields($day);
		
		$this->setSource('chargetotal' , 'record_data');
        $this->displays();
    }
	
    public function record_data(){
		
		$default = $this ->getServerPackage(0,0,1,0);
        $this->setCondition(0 , 'scp' ,'' , $default);
        $this->setOrder('date' , 'desc');
        $this->setLimit();
        $this->call('ChargetotalModel' , 'record_data' , $this->getConditions());
    }
}