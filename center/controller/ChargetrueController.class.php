<?php
!defined('IN_WEB') && exit('Access Denied');

class ChargetrueController extends Controller{
    private $cm;

    public function __construct(){
        parent::__construct();
        $this->cm = new ChargeModel();
    }
    
    public function index(){
        $tabs = array(
            array('title'=>'正常充值列表','url'=>'admin.php?ctrl=chargetrue&act=manuallist'),
            array('title'=>'设置正常充值','url'=>'admin.php?ctrl=chargetrue&act=manual')
        );
        $this->tabs($tabs);
    }

    public function manual(){
       
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        
        $this->smarty->assign('charge' , CDict::$charge);
        $this->smarty->display('chargetrue/manual.tpl');
    }
    
    public function manual_action(){

        $role_name = trim($this->getParam('role_name'));

        $server = $this->getParam('server');
        $server = array($server);
        $admin = $this->userAction->userSessionGet('username');

        $item_id = intval($this->getParam('item'));
        $sql = "select account,role_id,role_level,package,career,mac from ny_role where name = '$role_name'";

        $arr  = array('query' => array('data' => array($sql)));
        $res  = (new ServerconfigModel())->makeHttpParams($server, $arr);
        
        $urls = $res['urls'];
        $param = $res['params'];
        $res = Helper::rolling_curl($urls, $param);

        $data = json_decode($res[$server[0]],true);

        $data = $data['data']['query'][0][0];

        $role_id = $data['role_id'];
        
        list($channel,) = explode("_", $data['account']);

        if(empty($role_id)){
         //   $json = array('msg' => '找不到该角色2，请核实名称或区服');
       // }else{
            $charge = CDict::$charge;
            $rs = array(
                'server' => $server[0],
                'money' => $charge[$item_id]['money'],
                'gold' => $charge[$item_id]['gold'],
                'account' => $data['account'],
                'role_id' => $role_id,
                'role_name' => $role_name,
                'role_level' => $data['role_level'],
                'role_career' => $data['career'],
                'idfa' => $data['idfa'],
                'channel' => $channel,
                'package' =>  $data['package'],
                'item_id' => $item_id,
                'first' => 0,
                'create_time' => time(),
                'admin' => $admin,
                'order_num' => 0 .time(). $role_id ,

            );

            $result =  (new ChargetrueModel())->add($rs);
            if($result){

                $json = array('msg' => '添加成功，请等待审核'); 

           }else{

                $json = array('msg' => '添加失败，请重试');
           }
           
        }
        echo json_encode($json);
    }
  
    public function manuallist(){
    	
    	$header  = '说明：<br>';
    	$header .= '1. 此功能会生成真实订单的,请慎用。 <br>';
    	$header .= '2. 审核提示失败时在,最好先去查一下玩家的流水是否收到元宝,防止让玩家多收元宝,并告知后台管理员查明原因。 <br>';
    	$this->setHeader($header);
    	
        $this->setSearch('订单号');
        $this->setSearch('账号');
        $this->setSearch('角色ID' );
        $this->setSearch('角色' );
        $this->setSearch('金额' );
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setFields(array('服务器','订单号' ,'包号' , '账号' , '角色ID' , '角色名' , '金额','申请者','创建时间' , '审核'));
        $this->setSource('chargetrue' , 'manuallist_data');

        $js = <<< END
            function _checked(id){
                if(confirm('确定审2核？')){
                    $.ajax({
                        url: 'admin.php?ctrl=chargetrue&act=check&id='+id,
                        type: 'GET',
                        dataType: 'JSON',
                        beforeSend:function(){
                            $(".checked").attr({disabled:"disabled"});
                        },
                        complete:function(){
                            $(".checked").removeAttr('disabled');
                        }

                    }).done(function(data){
                        $.dialog.tips(data.msg);
                        window.location.reload();
                    });
                }
            }
        		
            function _outchecked(id){
                if(confirm('确定要驳回？')){
                    $.ajax({
                        url: 'admin.php?ctrl=chargetrue&act=checkout&id='+id,
                        type: 'GET',
                        dataType: 'JSON',
                        beforeSend:function(){
                            $(".checked").attr({disabled:"disabled"});
                        },
                        complete:function(){
                            $(".checked").removeAttr('disabled');
                        }

                    }).done(function(data){
                        $.dialog.tips(data.msg);
                        window.location.reload();
                    });
                }
            }
            
END;
        
        $this->setJs($js);

        $this->displays();  
    }
    public function manuallist_data(){
        $this->setCondition(0 , 'eq' , 'order_num');
        $this->setCondition(1 , 'eq' , 'account');
        $this->setCondition(2 , 'eq' , 'role_id');
        $this->setCondition(3 , 'eq' , 'role_name');
        $this->setCondition(4 , 'eq' , 'money');
        $this->setCondition(5 , 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $this->call('ChargetrueModel' , 'manuallist_data' , $this->getConditions());  
    }

    public function check(){
    	require_once(ROOT.'/../api/core/BaseController.php');
        $id = $this->getParam('id');
        $result =  (new ChargetrueModel())->getRow('*',array('id'=>$id));
        $insert = array(
        		'server'=> $result['server'],
        		'channel' => $result['channel'],
        		'package' => $result['package'],
        		'order_num' => $result['order_num'],
        		'corder_num' => $result['order_num'],
        		'money' => $result['money'],
        		'account' => $result['account'],
        		'role_id' => $result['role_id'],
        		'role_name' => $result['role_name'],
        		'role_level' => $result['role_level'],
        		'role_career' =>  $result['role_career'],
        		'gold' =>$result['gold'],
        		'create_time' => $result['create_time'],
        		'idfa' => $result['idfa']? $result['idfa']:1,
        		'item_id' => $result['item_id'],
        		'params' => 'truecharge',//后台正常充值功能标志
        );
        $m = new OrderModel();
        $insertId = $m->add($insert);
        $nowObj = new BaseController();//强实力，效率高
       	if ($insertId) {
       		$status = $nowObj->sendGoods($insert['server'],$insert);//服务端支持同一个订单发货多次
       		if($status == 1){
       			$res = $nowObj->updateOrder(array('status'=>1,'corder_num'=>$insert['order_num'],'loop_num'=>1),$insert['order_num']);
       			 if($res ==1){
		            $json = array('msg' => '充值成功');
		        }else{
		             $json = array('msg' => '充值失败');
		        }
		        (new ChargetrueModel())->update(array('checked'=>$res),array('id'=>$id));
       		}else {
       			$json = array('msg' => '充值失败');
       		}
       	}else {
       		$json = array('msg' => '充值失败');
       	}
       	echo json_encode($json);
    }
    
    public function checkout(){
        $id = $this->getParam('id');
        (new ChargetrueModel())->update(array('checked'=>2),array('id'=>$id));
        echo json_encode(['msg' => '驳回成功']);
    }
    
}