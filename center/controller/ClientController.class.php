<?php
!defined('IN_WEB') && exit('Access Denied');

class ClientController extends Controller{

    //客户端信息（菜单）
    public function index(){
        $tabs = array(
            array('title' => '机型网络', 'url' => 'admin.php?&ctrl=client&act=model_network'),
        );
        $this->tabs($tabs);
    }

    //机型网络（子菜单）
    public function model_network(){
        $header  = '说明：<br>';
        $header .= '1. 比例 = 该类型的数量 / 所有类型的数量的总和 <br>';
        $this->setHeader($header);
        $this->setSearch('类型' , 'select' , array(0 => '机型' , 1 => '网络'));
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 1));
        $this->setField('类型');
        $this->setField('数量');
        $this->setField('比例');
        $this->setSource('client' , 'model_network_data' , 1);
        $this->displays();
    }
    public function model_network_data(){
        $this->setCondition(0 , 'eq' , 'type'); //机型或者网络
        $this->setCondition(1 , 'scp');
        $this->setOrder('num' , 'desc');
        $this->setLimit();
        $method = $this->setDataExport('model_network_data' , 'model_network_export');
        $this->call('ClientModel' , $method , $this->getConditions());
    }

    //设备数（子菜单）
    public function device(){
        $header  = '说明：<br>';
        $header .= '1. 设备数 指打开游戏的玩家人数<br>';
        $header .= '2. 激活数 指登录游戏的玩家人数<br>';
        $header .= '3. 登陆页面流失率 = (设备数 - 激活数) / 设备数 ';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 1));
        $this->setField('日期');
        $this->setField('设备数');
        $this->setField('激活数');
        $this->setField('登陆页面流失率');
        $this->setSource('client' , 'device_data' , 1);
        $this->displays();
    }
    public function device_data(){
        $this->setCondition(0 , 'time' , 'date');
        $this->setOrder();
        $this->setLimit();
        $method = $this->getParam('export') ? 'device_export' : 'device_data';
        $this->call('DevicedailyModel' , $method , $this->getConditions());
    }

    //机型流失（子菜单）
    public function model_loss(){
        $header  = '说明：<br>';
        $header .= '1. 流失数量 指连续三天没有上线的机型数<br>';
        $header .= '2. 机型流失率 = 流失数量 / 总数 ';
        $this->setHeader($header);
        $this->setSearch('日期' , 'date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 1));
        $this->setField('机型' , 100);
        $this->setField('流失数量');
        $this->setField('总数');
        $this->setField('机型流失率');
        $this->setLength(50);
        $this->setSource('client' , 'model_loss_data' , 1);
        $this->displays();
    }
    public function model_loss_data(){
        $this->setCondition(0 , 'eq' , 'date' , date('Y-m-d'));
        $this->setCondition(1 , 'scp' , 'date');
        $this->setOrder();
        $this->setLimit(50);
        $method = $this->setDataExport('model_loss_data' , 'model_loss_export');
        $this->call('ClientModel' , $method , $this->getConditions());
    }

    //设备流失（子菜单）
    public function device_loss(){
    	$header  = '说明：<br>';
    	$header .= '1. 第N天流失率 = 上线设备数 / 新增设备数 , 格式：流失人数（流失率）<br>';
    	$header .= '2. 第N天流失 格式：流失人数（流失率）';
    	$this->setHeader($header);
    	$this->setSearch('日期' , 'range_date');
    	$this->setSearch('' , 'scp' , array(0 , 1 , 0 , 1));
    	$this->setField('日期' , 100);
    	$this->setField('新增设备数');
    	$this->setField('第2天流失');
    	$this->setField('第3天流失');
    	$this->setField('第7天流失');
    	$this->setField('第15天流失');
    	$this->setField('第30天流失');
    	$this->setSource('client' , 'device_loss_data' , 1);
    	$this->displays();
    }
    public function device_loss_data(){
    	$this->setCondition(0 , 'time' , 'date');
    	$this->setCondition(1 , 'scp' );
    	$this->setOrder('date');
    	$this->setLimit();
    	$method = $this->setDataExport('device_loss_data' , 'device_loss_export');
    	$this->call('DevicelossModel' , $method , $this->getConditions());
    }
}
