<?php
!defined('IN_WEB') && exit('Access Denied');

class CloudlandCopyController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启云梦秘境的人数。<br>';
        $header .= '参与人数：该日进行过云梦秘境（消耗云梦秘令≥1）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均杀怪数量 = 该日全服所有玩家消耗云梦秘令之和 / 参与人数   （确定到小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '人均杀怪次数'));
        $this->setSource('CloudlandCopy', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('CloudlandCopyModel', $method, $this->getConditions());
    }
}