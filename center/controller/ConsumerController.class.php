<?php
!defined('IN_WEB') && exit('Access Denied');

class ConsumerController extends Controller{

    protected $moneyType;

    public function __construct()
    {
        parent::__construct();

        $moneyType = [
            1 => '元宝',
            41 => '仙玉'
        ];

        $this->moneyType = $moneyType;

    }

    public function index()
    {
        $tabs = array();

        foreach ($this->moneyType as $key => $val) {
            array_push($tabs, ['title' => $val, 'url' => 'admin.php?&ctrl=consumer&act=record&money_type='.$key]);
        }

        $this->tabs($tabs);
    }

    //每小时统计数据
    public function record()
    {

        $money_type = $this->getParam('money_type');

        $money_name = $this->moneyType[$money_type];

        $header = '说明：<br>';
        $header .= '消费项目名称：具体消费项目待统计。<br>';
        $header .= '消费金额：该服玩家当天在该项目的消费'.$money_name.'数之和。<br>';
        $header .= '消费占比 ：消费金额 /日消费 *100% （小数点后两位，四舍五入）    日消费：当日该服全部玩家花费的总'.$money_name.'数量。<br>';

        $this->setHeader($header);

        $this->setSearch('日期' , 'date');
        $this->setSearch('服务器' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array('服务器', '消费项目名称', '消费金额', '消费占比'));
        $this->setSource('consumer', 'record_data', 1, ['money_type' => $money_type]);
        $this->setLimit();
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function record_Data()
    {
        $this->setCondition(0 , 'eq' , "create_time");
        $this->setCondition(1 , 'scp');
        $this->setCondition(2, 'eq', 'money_type', $this->getParam('money_type'));

        $this->setOrder('source' , 'asc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('ConsumerModel', $method, $this->getConditions());
    }
}