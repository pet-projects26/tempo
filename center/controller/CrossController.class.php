<?php
!defined('IN_WEB') && exit('Access Denied');

class CrossController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->c = new CrossModel();
    }

    public function index()
    {

        $tabs = array(
            array('title' => '跨服列表', 'url' => 'admin.php?&ctrl=cross&act=record'),
            array('title' => '添加跨服', 'url' => 'admin.php?&ctrl=cross&act=add'),
            array('title' => '编辑跨服', 'url' => 'admin.php?&ctrl=cross&act=edit'),
        );
        $this->tabs($tabs);
    }

    public function record()
    {
        $this->setSearch('系统', 'select', array('' => '全部', 1 => 'IOS', 2 => '安卓', 3 => '混服'));
        $this->setFields(array('编号', '名称', '跨服组', '系统', '版本号', '是否配置', '操作时间', '操作'));
        $this->setSource('cross', 'record_data');
        $js = <<< END
         function edit(id){
             \$tabs.tabs('select',2);
             \$tabs.tabs('url' , 2 , 'admin.php?ctrl=cross&act=edit&id=' + id);
             \$tabs.tabs('load' , 2);
         } 
         function config(id){
            if(confirm("确定要提交配置吗？")){
                $.ajax({
                  url: 'admin.php?ctrl=cross&act=config&id=' + id,
                  type: 'POST',
                  dataType: 'JSON',
                  data: {
                  }
                }).done(function(data){

                    if(data == 1){
                        $.dialog.tips('配置成功');
                        \$tabs.tabs('load' , 0);

                    }else{
                        $.dialog.tips('配置失败');
                    }
                })   
             } 
         } 
         
END;

        $this->setJs($js);
        $this->displays();
    }

    public function record_data()
    {

        $this->setCondition(0, 'eq', 'type');
        $this->setOrder('id', 'desc');
        $this->setLimit();
        $this->call('crossModel', 'record_data', $this->getConditions());
    }

    public function add()
    {
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $this->smarty->assign('groups', $channelGroupRow);

        $this->smarty->display('cross/add.tpl');
    }

    public function add_data()
    {
        $groups = $this->getParam('groups');
        $type = $this->getParam('type');
        $name = $this->getParam('name');
        if (empty($groups)) {
            die(json_encode(array('msg' => '请选择渠道组', 'code' => 0)));
        }

        //已被勾选的渠道组不能再重复添加
        foreach ($groups as $v) {
            $sql = "select id from ny_server_cross where find_in_set($v,groups)";
            $rs = $this->c->query($sql);
            $id = $rs[0]['id'];
            if (!empty($rs)) {
                $sql = "select name from ny_channel_group where id = $v";
                $rs = $this->c->query($sql);
                $name = $rs[0]['name'];
                die(json_encode(array('msg' => '渠道组' . $name . '在编号' . $id . '已被勾选', 'code' => 0)));
            }
        }

        //判断勾选的渠道组的版本号，版本号不一致，不能添加
        $group = implode(',', $groups);
        $versionsql = "select version from ny_channel_group where id in ($group)";
        $result = $this->c->query($versionsql);

        $result = array_column($result, 'version');

        if (count(array_count_values($result)) > 1) {
            die(json_encode(array('msg' => '勾选的渠道组版本号不一致', 'code' => 0)));
        }

        $data = array(
            'groups' => implode(',', $groups),
            'type' => $type,
            'create_time' => time(),
            'version' => $result[0],
            'name' => $name,
        );

        $return = $this->c->add($data);

        if ($return) {
            die(json_encode(array('msg' => '添加成功', 'code' => 1)));
        } else {
            die(json_encode(array('msg' => '添加失败', 'code' => 0)));
        }

    }

    public function edit()
    {
        $id = $this->getParam('id');
        if (empty($id)) {
            die('请选择要编辑的数据');
        }
        $rs = $this->c->getRow('*', array('id' => $id));

        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $this->smarty->assign('type', $rs['type']);
        $this->smarty->assign('group', explode(',', $rs['groups']));

        $this->smarty->assign('groups', $channelGroupRow);

        $this->smarty->assign('ids', $id);
        $this->smarty->assign('name', $rs['name']);

        $this->smarty->display('cross/edit.tpl');
    }

    public function edit_data()
    {
        $groups = $this->getParam('groups');
        $type = $this->getParam('type');
        $ids = $this->getParam('id');
        $name = $this->getParam('name');
        if (empty($groups)) {
            die(json_encode(array('msg' => '请选择渠道组', 'code' => 0)));
        }

        $day = date('d');

        $days = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25');

        if (in_array($day, $days)) {
            die(json_encode(array('msg' => '当前时间不可编辑', 'code' => 0)));
        }

        //已被勾选的渠道组不能再重复添加,撇除自身
        foreach ($groups as $v) {

            $sql = "select id from ny_server_cross where find_in_set($v,groups) and id !=$ids ";

            $rs = $this->c->query($sql);
            $id = $rs[0]['id'];
            if (!empty($rs)) {
                $sql = "select name from ny_channel_group where id = $v";
                $rs = $this->c->query($sql);
                $name = $rs[0]['name'];
                die(json_encode(array('msg' => '渠道组' . $name . '在编号' . $id . '已被勾选', 'code' => 0)));
            }
        }

        //判断勾选的渠道组的版本号，版本号不一致，不能添加
        $group = implode(',', $groups);
        $versionsql = "select version from ny_channel_group where id in ($group)";
        $result = $this->c->query($versionsql);

        $result = array_column($result, 'version');

        if (count(array_count_values($result)) > 1) {
            die(json_encode(array('msg' => '勾选的渠道组版本号不一致', 'code' => 0)));
        }

        $data = array(
            'groups' => implode(',', $groups),
            'type' => $type,
            'create_time' => time(),
            'version' => $result[0],
        );

        $return = $this->c->update($data, array('id' => $ids));

        if ($return) {
            die(json_encode(array('msg' => '更新成功', 'code' => 1)));
        } else {
            die(json_encode(array('msg' => '更新失败', 'code' => 0)));
        }

    }

    public function config()
    {
        $id = $this->getParam('id');

        if (empty($id)) {
            die('请选择要编辑的数据');
        }

        $rs = $this->c->getRow(array('name', 'groups'), array('id' => $id));
        $group = $rs['groups'];
        $sql = "select name from ny_channel_group where id in ($group)";

        $result = $this->c->query($sql);

        $group_name = array_column($result, 'name');

        $group_name = implode(',', $group_name);

        $webhook = "https://oapi.dingtalk.com/robot/send?access_token=49e5417f91c3ce1e6d563e674babe0705bc414ea99e9acbcb96bc95127542843";

        $message = "类型：跨服， 跨服服务器：" . $rs['name'] . " , 渠道组：" . $group_name;

        $data = array('msgtype' => 'text', 'text' => array('content' => $message));
        $data_string = json_encode($data);

        $return = Helper::httpRequest($webhook, $data_string, 'post', 5, 5, true);
        $return = json_decode($return, true);

        if ($return['errcode'] == 0) {
            $this->c->update(array('config' => 1), array('id' => $id));
            echo 1;
        } else {
            echo 0;
        }

    }
}

?>