<?php
!defined('IN_WEB') && exit('Access Denied');

class DailyconsumeController extends Controller{

    public function record()
    {
        $header = '说明：<br>';
        $header .= '日消费：当日该服全部玩家花费的总元宝数量。<br>';
        $header .= '总元宝产出：当日该服总产出的元宝数量。<br>';
        $header .= '充值元宝产出：当日该服充值产出的元宝数量。<br>';
        $header .= '充值产出占比 = 充值元宝产出 / 总元宝产出 *100% （小数点后两位，四舍五入）<br>';
        $header .= '免费元宝产出= 总元宝产出 -充值元宝产出<br>';
        $header .= '免费产出占比 = 免费元宝产出 / 总元宝产出*100% （小数点后两位，四舍五入）<br>';
        $header .= '元宝滞留数 = 总元宝产出 - 日消费<br>';
        $header .= '元宝滞留率 = 元宝滞留数 / 总元宝产出 *100% （小数点后两位，四舍五入）<br>';
        $header .= '元宝总存量：该服截止至当天全部玩家拥有的元宝总量。<br>';
        $header .= '活跃人数：该日到达选服界面的玩家数。<br>';
        $header .= '消费人数：当日该服进行元宝消费行为的玩家数。<br>';
        $header .= '消费率 = 消费人数 / 活跃人数 *100% （小数点后两位，四舍五入）<br>';
        $header .= '消费arppu = 日消费 / 消费人数 （小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器', '日消费', '总元宝产出', '充值元宝产出', '充值产出占比', '免费元宝产出', '免费元宝产出占比', '元宝滞留数', '元宝滞留率', '元宝总存量', '活跃人数', '消费人数', '消费率', '消费ARPPU'));
        $this->setSource('Dailyconsume' , 'record_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('DailyconsumeModel' , $method , $this->getConditions());
    }
}