<?php
!defined('IN_WEB') && exit('Access Denied');

class DailyrechargeController extends Controller{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '本日流水：当天充值总流水<br>';
        $header .= '付费用户：当天充值的人数 <br>';
        $header .= 'ARPPU = 充值总额 / 充值总人数 <br>';
        $header .= 'ARPU = 充值总额 /当天活跃玩家的人数            （活跃玩家定义：当天连接上游戏服务器的玩家）<br>';
        $header .= '付费率 = 充值总人数 / 当天活跃玩家人数 *100%<br>';
        $header .= '新增付费用户：当天新增连接上游戏服务器的玩家并在当天进行充值的人数 <br>';
        $header .= '新增付费金额： 新增充值人数当天充值的金额 <br>';
        $header .= '新增用户ARPPU = 新增充值总额 / 新增充值人数  <br>';
        $header .= '新增用户ARPU = 新增充值总额 / 新增跳转人数 <br>';
        $header .= '新增用户付费率 = 新增充值人数 / 新增跳转人数 *100%<br>';
        $header .= '老玩家付费用户 = 当天进行充值的老玩家人数     （老玩家定义：已创角且连接上游戏服务器的玩家） <br>';
        $header .= '老玩家付费金额 = 老玩家当天充值金额 <br>';
        $header .= '老玩家ARPPU = 老玩家付费金额 / 老玩家付费用户  <br>';
        $header .= '老玩家ARPU = 老玩家付费金额 / 老玩家登录人数 <br>';
        $header .= '首冲人数：当天第一次充值的人数<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '本日流水' , '付费用户' , '首充人数', 'ARPPU', 'ARPU', '付费率', '新增付费用户', '新增付费金额', '新增用户ARPPU', '新增用户ARPU', '新增用户付费率', '老玩家付费用户', '老玩家付费金额', '老玩家ARPPU', '老玩家ARPU'));
        $this->setSource('Dailyrecharge' , 'index_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('DailyrechargeModel' , $method , $this->getConditions());
    }
}