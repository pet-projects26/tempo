<?php
!defined('IN_WEB') && exit('Access Denied');

class DailytrialController extends Controller{

    public function index(){
        if (!empty(CDict::$trialTypes)) {
            $tabs = [];
            foreach (CDict::$trialTypes as $key => $val) {
                $tab = [
                    'title' => $val['name'],
                    'url' => 'admin.php?&ctrl=dailytrial&act=record&type='.$key
                ];

                array_push($tabs, $tab);
            }
            $this->tabs($tabs);
        } else {
            echo 'CDict::$trialTypes is empty';
        }
    }

    public function record()
    {
        $type = $this->getParam('type');

        if (!array_key_exists($type, CDict::$trialTypes)) return false;

        $typeName = CDict::$trialTypes[$type]['name'];

        $header = '说明：<br>';
        $header .= '活跃人数：该日成功连接上游戏服务器并且开启'.$typeName.'的玩家人数（备注：读功能开启表）。 <br>';
        $header .= '参与人数：该日进行过'.$typeName.'挑战玩法（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均挑战次数 = 该日全服每个玩家挑战'.$typeName.'次数之和（有奖励即算一次，不需要去重） / 参与人数   （确定到小数点后两位，四舍五入）<br>';
        if ($type == 5 || $type == 6) {
            $header .= '鼓舞人数占比 = 在'.$typeName.'玩法中进行过鼓舞的玩家数（需去重） / 参与人数 *100%（精确到小数点后两位，四舍五入）<br>';
            $header .= '人均鼓舞次数 = 该日本服在'.$typeName.'玩法中进行鼓舞的总次数 / 在'.$typeName.'玩法中进行过鼓舞的玩家数 （需去重） （精确到小数点后两位，四舍五入）<br>';
        }
        $header .= '购买次数玩家占比 = 该日本服购买' . $typeName . '次数的玩家数（去重） / 该日本服进行过' . $typeName . '挑战玩法（有奖励即算，需去重）的玩家数 * 100% <br>';
        $header .= '人均购买次数 = 该日本服购买' . $typeName . '总次数 / 该日本服购买' . $typeName . '次数的玩家数（去重）  精确到小数点后两位，四舍五入 <br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '参与人数' , '参与度', '人均挑战次数'));
        if ($type == 5 || $type == 6) {
            $this->setFields(array('鼓舞人数占比' , '人均鼓舞次数'));
        }
        $this->setFields(array('购买次数玩家占比', '人均购买次数'));
        $this->setSource(' Dailytrial' , 'record_data' , 1, ['type' => $type]);
        $this->setLimit('100');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0 , 'time' , "create_time");
        $this->setCondition(1 , 'scp');
        $this->setCondition(2 , 'eq' , 'type' ,$this->getParam('type'));
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('DailytrialModel' , $method , $this->getConditions());
    }

}