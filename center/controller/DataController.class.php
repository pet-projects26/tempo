<?php
!defined('IN_WEB') && exit('Access Denied');

class DataController extends Controller{
    public function __construct(){
        parent::__construct();
    }
	public function real(){
		$header  = '说明：<br>';
        $header .= '1. ARPU = 充值金额 / 登录人数 <br>';
        $header .= '2. 新增付费AP=新增付费金额/新增付费人数<br>';
       
		$this->setHeader($header);
		$this->setSearch('日期','range_date');
		$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
		$this->setField('日期','120');
		$this->setFields(array(
            '新增注册' , '新增角色数' , '活跃人数' , '当前时段在线人数' ,
            '新增付费人数' , '新增付费金额' , '新增付费AP' ,
			'充值付费人数' , '充值付费金额' , '充值付费AP' ,
        ));
		$this->setLimit();
        $this->setSource('data' , 'real_data' , 1);
		$this->displays();
	}
	public function real_data(){
		$this->setCondition(0 , 'date','create_time');
		$this->setCondition(1 , 'scp');
		$method = $this->setDataExport('real_data' , 'real_export');
        $this->call('DataModel' , $method , $this->getConditions());
	}
}