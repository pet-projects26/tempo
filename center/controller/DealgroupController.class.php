<?php
!defined('IN_WEB') && exit('Access Denied');

class DealgroupController extends Controller{
	public function __construct(){
        parent::__construct();
        $this->dgm = new DealgroupModel();
		$this->cgm = new ChannelgroupModel();
		$this->cm = new ChannelModel();
        $this->pm = new PackageModel();
		$this->sm = new ServerModel();
		$this->at = new ActivityTmpModel();
		$this->at = new ActivityTmpModel();
		$this->ic = new ItemBuyModel();
    }
	
	public function index(){
		$tabs = array(
            array('title' => '交易组列表', 'url' => 'admin.php?&ctrl=dealgroup&act=record'),
            array('title' => '添加交易组', 'url' => 'admin.php?&ctrl=dealgroup&act=add'),
            array('title' => '编辑交易组', 'url' => 'admin.php?&ctrl=dealgroup&act=edit'),
			array('title' => '归属查询', 'url' => 'admin.php?&ctrl=dealgroup&act=search'),
        );
        $this->tabs($tabs);	
	}
	
	//交易组列表
    public  function record(){
		/*$header  =  '<input type="button" class="gbutton" value="更新" onclick="message()">';
		
        $this->setHeader($header);*/
		
        $this->setFields(array('名称' , '创建时间' , '操作'));
        $this->setSource('dealgroup' , 'record_data');
        $js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=dealgroup&act=edit&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=dealgroup&act=edit&id=' + id);
            }
            function del(id){
				if(confirm('确定删除？')){
					$.post('admin.php?ctrl=dealgroup&act=del_action&id=' + id,'',function(data){
						 $.dialog.tips(data.msg);
						\$tabs.tabs('load' , 0);
					},'json');
				}    
            }
			/*function message(){
				if(confirm('确定发布？')){
					$.post('admin.php?ctrl=dealgroup&act=message','',function(data){

						 $.dialog.tips(data.msg);
						 \$tabs.tabs('load' , 0);
					},'json');
				}
			}*/
END;
        $this->setJs($js);
        $this->displays();
    }
	
    //交易组列表
    public function record_data(){
        $this->setLimit();
        $this->call('DealgroupModel' , 'record_data' , $this->getConditions());
    }

    //添加交易组
    public function add(){
		
        $this->smarty->display('dealgroup/add.tpl');
    }

    public function add_action(){
		$id = file_get_contents('controller/dealgroup.txt');
		$id  = intval($id);
		$name = $this->getParam('name');
		$data = array(
			'_id' => new MongoInt32($id),
			'name' => $name,
			'create_time' =>new MongoInt32(time())
		);
		
		if($this->dgm->addDealGroup($data)){
			$json = array('msg' => '添加成功' , 'code' => 1);
			file_put_contents('controller/dealgroup.txt',$id+1);
		}
		else{
			$json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
		}
		echo json_encode($json);
    }
	
	public function del_action(){
		
        $id = $this->getParam('id');
		
        $id && $rs = $this->dgm->delDealGroup($id);
		$rs = $this->dgm->message();//通知游戏后端 ，交易组的变动

		if($rs === 0){
			echo json_encode(array('code'=>1 , 'msg' => '删除成功'));
		}else{
			echo json_encode(array('code'=>1 , 'msg' => '删除失败'));
		}
			

    }
	
	public function edit(){
		$id = $this -> getParam('id');
		
		$result = $this->dgm->findOne($id);
		
		$serverarr = $this->sm->getRows(array('server_id','name','channel_num' , 'num' , 'group_id'));
		
		foreach($serverarr as $k=>$v){
			
			$serverlist[$v['server_id']]['name'] = $v['name'];	
			$serverlist[$v['server_id']]['channel_num'] = $v['channel_num'];	
			$serverlist[$v['server_id']]['num'] = $v['num'];	
		}
		
		if($result['server']){
			$result['server'] = explode(',' , $result['server']);	
		}

		$this->smarty->assign('name',$result['name']);
		
        $group = $this->cgm->getChannelGroup(array());
		
		$arr= array();
        if($group){
         	foreach($group as $k=>$v){

         		foreach ($serverarr as $key => $value) {
         			if($v['id'] == $value['group_id']){
         				if(in_array($value['server_id'],$result['server'])){
							$arr[$v['name']][$key]['checked'] = 1;		
						}else{
							$arr[$v['name']][$key]['checked'] = 0;	
						}
	         			
	         			$num = $this->dgm->createChannelServer($value['channel_num'] , $value['num']);
	         			$arr[$v['name']][$key]['server_id'] = $value['server_id'];
	         			$arr[$v['name']][$key]['server_name'] =$value['name'].'('.$num.')';
         			}
         			
         		}
     
			}
        }
		
		$this->smarty->assign('arr' , $arr);
		$this->smarty->assign('id' , $id);
        $this->smarty->display('dealgroup/edit.tpl');
    
	}
	
	public function edit_action(){
		//查询交易组的全部数据
		$dealgroup = $this->dgm->findAll();
		
		foreach($dealgroup as $k=>$v){
			$deal[$v['name']] = $v['server']; 	
		}
		
		$id = $this -> getParam('id');
		$name = $this->getParam('name');
		$oldname = $this->getParam('oldname');
		$server = $this -> getParam('server');//传递过来的服务器数组
		
		$addserver=array_diff($server,explode(',' , $deal[$name]));//新数组比原有的数组增加的服务器，删除的可以不用判断
		
		//判断服务器是否被勾选过了
		foreach ($deal as $k=>$v){
			
			foreach($addserver as $kk=>$vv){
				
				if($k != $oldname){
					
					if(stripos($v,$vv) !== false ){
		
						$conditions['WHERE']['server_id'] = $vv;
						$servername = $this->sm->getRow(array('name'),$conditions['WHERE']);
						exit(json_encode(array('msg' => '服务器'.$servername['name'].'已经在交易组'.$k.'被勾选过了' , 'code' => 2)));
					}		
				}
			}	
		}
		
		$conditions = array();
		
		if($server){
			
			$conditions['WHERE']['server_id::IN'] = $server;
			
			$serverarr = $this->sm->getRows(array('channel_num' , 'num') , $conditions['WHERE']);
			
			foreach($serverarr as $k=>$v){
	
				 $serverid = $this->dgm->createChannelServer($v['channel_num'],$v['num']);
				 $group[] = $serverid;	
			}
		}else{
			$group = '[]';	
		}
		
		
		//更新数据
		$server = implode(',',$server);
		
		$data = array(
			'name' => $name,
			'server' => $server,
			'group' => $group
		);
		
		$res = $this->dgm->editDealGroup($data , $id);
		
		if($res['ok'] == 1 && $res['nModified'] ==1){
			$rs = $this->dgm->message();//通知游戏后端 ，交易组的变动
			if($rs === 0 ){
				exit(json_encode(array('msg' => '保存成功' , 'code' => 1)));
			}else{
				exit(json_encode(array('msg' => '保存失败' , 'code' => 0)));
			}
			
			
		}elseif($res['ok'] == 1 && $res['nModified'] ==0){
			
			exit(json_encode(array('msg' => '没有变化' , 'code' => 0)));
			
		}else{
			
			exit(json_encode(array('msg' => '保存失败' , 'code' => 0)));
		}
		
	}
	
	//归属查询
	public function search(){
		$this->smarty->display('dealgroup/search.tpl');
	}
	
	public function search_action(){
		$server = $this -> getParam('server');
		$sql = "select name,server_id from ny_server where name like '%$server%'";
		$res = $this->sm->query($sql);
		
		foreach($res as $k => $v){
			
			$server_id = $v['server_id'];
			
			$result = $this->dgm->search($server_id);
			
			foreach($result as $kk => $vv){
				
				$arr[]['name'] = $vv['name'];	
			}
		}
		
		if($arr){
			$html ='<div style="padding:5px">';
			$html .= '<table cellpadding="0" cellspacing="0" border="0" class="table_list dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">';
			$html .= '<thead>';
			$html .= '<tr>';
			
			$html .= ' <th rowspan="1" colspan="1">交易组</th>';	
			
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			
			foreach($arr as $k=>$v){
				if($k %2 == 0){
					$html .= '<tr class="odd">';	
				}else{
					$html .= '<tr class="even">';	
				}
				
				$html .= '<td>'.$v['name'].'</td>';	
				
				$html .= '</tr> ';
			}
			
			$html .= ' </tbody>';
			$html .= ' </table>';
			$html .= '</div>';
			
			exit(json_encode(array('code' => 200 , 'msg' => $html)));
				
		}else{
			exit(json_encode(array('code' => 400 , 'msg' => '查询不到数据')));
			
		}
	}
	
	/*public function message(){
		$rs = $this->dgm->message();
		$rs =0;
		if($rs === 0){
			echo json_encode(array('code'=>1 , 'msg' => '更新成功'));	
		}else{
			echo  json_encode(array('code'=>0 , 'msg' => '更新失败，请重试'));	
		}
	}*/
	
/*--------------------下面的是交易模版---------------------------*/	
	
	public function tpl_index(){
		
		$tabs = array(
            array('title' => '模版列表', 'url' => 'admin.php?&ctrl=dealgroup&act=tpl_record'),
            array('title' => '添加模版', 'url' => 'admin.php?&ctrl=dealgroup&act=tpl_add'),
            array('title' => '编辑模版', 'url' => 'admin.php?&ctrl=dealgroup&act=tpl_edit'),
            array('title' => '导入模板道具', 'url' => 'admin.php?&ctrl=dealgroup&act=tpl_item'),
			
        );
        $this->tabs($tabs);
	}
	
	public function tpl_record(){
		
		$this->setFields(array('ID' , '标题' , '类型' ,  '添加时间' , '操作'));
        $this->setSource('dealgroup' , 'tpl_record_data');
        $js = <<< END
            function edit(id,type){
            	
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=dealgroup&act=tpl_edit&id=' + id + '&type='+ type);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=dealgroup&act=tpl_edit&id=' + id + '&type='+ type);
            }
            function del(id){
				if(confirm('确定删除？')){
					$.post('admin.php?ctrl=dealgroup&act=tpl_del_action&id=' + id,'',function(){
						\$tabs.tabs('load' , 0);
					});
				}
                
            }
            
END;
        $this->setJs($js);
        $this->displays();	
	}
	
	public function tpl_record_data(){
		
		$this->setLimit();
		
        $this->call('DealgroupModel' , 'tpl_record_data' , $this->getConditions());
			
	}
	
	public function tpl_add(){
		$this->smarty->assign('item_tpl',CDict::$item_tpl);
		$this->smarty->display('dealgroup/tpl_add.tpl');
	}
	public function item_tpl_action(){
		$item_tpl = $this->getParam('item_tpl');
		if ($item_tpl) {
			$itemData = $this->ic->getRows(['id','item_id','price','num'],['item_tpl'=>$item_tpl]);
			if ($itemData) {
				echo json_encode(['status' => 1 ,'con'=>$itemData]);
			}else{
				echo json_encode(['status' => 0 ,'con'=>[]]);
			}
			
		}
	}
	
	public function tpl_add_action(){
		
		$title = $this->getParam('title');
		/*$day = $this->getParam('day');

		$day = !empty($day) ? $day : 2;*/
		
		$type = $this->getParam('type');
		$start_time = $this->getParam('start_time');
		$end_time = $this->getParam('end_time');
		$item_id = $this->getParam('item_id');
		$item_count = $this->getParam('item_count');
		$item_price = $this->getParam('item_price');
		$status = $this->getParam('status');

		$id = $this->getParam('id');


		foreach($item_id as $k => $v){

			$res[$k]['item_id'] = $item_id[$k];
			$res[$k]['item_price'] = $item_price[$k];
			$res[$k]['item_count'] = $item_count[$k];
		}

		$res  = array_values($res);

		$arr['content'] = json_encode($res);

		$arr['title'] = $title;

		$arr['type'] = $type;

		$arr['start_time'] = strtotime($start_time);
		$arr['end_time'] = strtotime($end_time);

		//$arr['day']  = $day*86400;

		$arr['create_time']  =  time();

		$arr['status'] =  $status;

		$arr['id'] =  $id;


		if($this->dgm->tpl_add($arr)){
			if($status =='add'){
				$msg ='添加成功';
			}else{
				$msg ='编辑成功';
			} 
			$json = array('msg' => $msg , 'code' => 1);
		}
		else{
			if($status =='add'){
				$msg ='未添加成功，请再次尝试';
			}else{
				$msg ='未编辑成功，请再次尝试';
			} 
			$json = array('msg' => $msg , 'code' => 0);
		}
		echo json_encode($json);

	}
	
	/**
     * [tpl_edit 编辑模版]
     * @param  [int] $id 	[被编辑的id]
     * @param  [int] $type  [类型，1：编辑 2：复制]
     */
	public function tpl_edit(){
		//根据id查询出数据
		$id = $this -> getParam('id');
		$type = $this -> getParam('type');

		$result = $this->dgm->tpl_findOne($id);
		

		//$result['day'] = $result['day'] /86400;
		$result['start_time'] = date('Y-m-d H:i' , $result['start_time']);
		$result['end_time'] = !empty($result['end_time']) ? date('Y-m-d H:i' , $result['end_time']) : '';
		$result['content'] = json_decode($result['content'] , true);

		$num = 0;
		foreach ($result['content'] as $key => $value) {
			$num ++;
		}

		$this->smarty->assign('id' , $id);
		$this->smarty->assign('result' , $result);
		$this->smarty->assign('type' , $type);
		$this->smarty->assign('num' , $num);
		
		$this->smarty->display('dealgroup/tpl_edit.tpl');
	}
	
	public function tpl_del_action(){
	
		$id = $this->getParam('id');
		
        $id && $this->dgm->tpl_delete($id);	
	}

	
	
/*--------------------下面的是交易管理---------------------------*/	


	public function deal_index(){
	
		$tabs = array(
            array('title' => '交易详情', 'url' => 'admin.php?&ctrl=dealgroup&act=deal'),
            array('title' => '添加交易', 'url' => 'admin.php?&ctrl=dealgroup&act=deal_add'),
        );
        $this->tabs($tabs);	
	}
	
	public function deal(){
		//查询全部交易组
		$group = $this->dgm->findAll();
		$rs[''] = '-';
		foreach ($group as $key => $value) {
    			
    		$rs[$value['_id']] = $value['name'];
   
    	}
  
    	$this->smarty->assign('group' , $rs);
        $this->smarty->display('dealgroup/deal.tpl');
		
	}

	public function deal_data(){

		$group = $this->getParam('group');
		
		if($group){
			
			$groupitem  = $this->dgm->checkItem($group);

            $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";
		
			$server = $this->dgm->query($sql);
			
			$AllItem= $this->call('GiftModel' , 'getItem' , array() , $server[0]['server_id']);//全部物品
	
			$AllItem = $AllItem[$server[0]['server_id']];
				
			$item =array();
			
			foreach($groupitem as $key => $value) {
				
				$itemId = $value['itemId'];
				
				$item[$key]['uuid'] = $value['_id'];
				$item[$key]['price'] = $value['priceT'];
				
				$item[$key]['count'] = $value['item']['count'];
				
				$item[$key]['name'] = $itemId.'/'.$AllItem["$itemId"];//物品id/物品名称
				$item[$key]['startTm'] = date('Y-m-d H:i',$value['startTm']);
				$item[$key]['endTm'] = date('Y-m-d H:i',$value['endTm']);
				switch($value['flag']){
					case 0:
						$item[$key]['flag'] = '<p>上架</p>'; break;
					case 1:
						$item[$key]['flag'] = '<p style="color:red">下架</p>'; break;
					case 2:
						$item[$key]['flag'] = '<p style="color:green">售罄</p>'; break;
				}
				
			}
			
			$this->smarty->assign('item',$item);
			$this->smarty->display('dealgroup/deal_data.tpl');

		}else{

            echo '<div style="min-width:400px;height:20px;padding:10px 15px;margin:6px auto;border:solid 1px #ABABAB;border-radius:5px;">请选择交易组</div>';
        
		}
		
	}
	
	public function unshelve(){
		$item = $this->getParam('item');

		$ip = TRADE_IP;
		
		$port =TRADE_PORT;
		
		$rs = $this->dgm->rpcCall($ip , $port , 'delitem' , $item);
		//$rs =0;
		if($rs === 0){

			exit(json_encode(array('code'=>1 , 'msg' => '删除物品成功')));

		}else{
			
			exit(json_encode(array('code'=>0 , 'msg' => '删除物品失败，请重试')));	
		}
		
	}
	
	public function deal_add(){
		//查询交易组
		$group = $this->dgm->findAll();
		
		//查询模版
		$sql = "select id,title from ny_deal_tpl";
		$res = $this->dgm->query($sql);
		
		$tpl = array();
		$tpl[''] = '-';
		foreach($res as $k=>$v){
			$tpl[$v['id']] = $v['title'];	
		}
		
		$this->smarty->assign('tpl',$tpl);
		$this->smarty->assign('group',$group);
		$this->smarty->display('dealgroup/deal_add.tpl');
	}
	
	public function getTpl(){
		$id = $this -> getParam('id');	
		$num = $this -> getParam('num');	
		
		$sql = "select content,start_time,end_time from ny_deal_tpl where id = $id ";
		$res = $this->dgm->query($sql);
		
		$content = json_decode($res[0]['content'],true);
	
		$start_time = $res[0]['start_time'];
		$end_time = $res[0]['end_time'];
		
		if($start_time != 0){
			$start_time = date('Y-m-d H:i' ,$start_time);	
		}else{
			$start_time = '';	
		}
		
		if($end_time != 0){
			$end_time = date('Y-m-d H:i' ,$end_time);	
		}else{
			$end_time = '';	
		}
		
		$html = '';
		if($content){
			foreach($content as $k=>$v){
				$num++;
				$html .= '<tr class="tr_t itemWidget'.$num.'">';
				$html .= '<td style="width:150px;"><input type="text" num="'.$num.'" name="item_id['.$num.']" value="'.$v['item_id'].'" style="width:150px;" 				class="item_id" ></td>';
				$html .= '<td style="width:150px;"><input type="text" name="item_price['.$num.']" value="'.$v['item_price'].'" style="width:150px;" class="item_name" ></td>';
				$html .= '<td style="width:150px;"><input type="text" name="item_count['.$num.']" value="'.$v['item_count'].'" style="width:150px;" class="item_count"></td>';
				$html .= '<td style="width:150px;"><input type="text" name="start_time['.$num.']" value="'.$start_time.'" style="width:150px;" class="start_time" ></td>';
				$html .= '<td style="width:150px;"><input type="text" name="end_time['.$num.']" value="'.$end_time.'" style="width:150px;" class="end_time" ></td>';
				$html .= '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
				$html .= '<td><input type="button" value="复制" num="'.$num.'" class="gbutton copyitem" title="复制该行"></td>';
				$html .= '</tr>';
			}	
		}
		echo json_encode(array('html' => $html , 'num' => $num));
		
	}
	
	public function deal_add_action(){
		
		$item_id  = $this ->getParam('item_id');
		$item_price  = $this ->getParam('item_price');
		$item_count  = $this ->getParam('item_count');
		$start_time  = $this ->getParam('start_time');
		$end_time  = $this ->getParam('end_time');
		$group  = $this ->getParam('group');
		
		$group = json_decode($group ,true);
		
		foreach($item_id as $k=>$v){
			
			$id = explode('/' , $v);
			$item_id[$k] =  $id[0];	
		}
		
		$item = array();
		$num =0;
		foreach($item_id as $k=>$v){
			foreach($group as $kk => $vv){
				
				$item[$num]['uuid'] =  $this->at->uuid();
				$item[$num]['itemId'] = $item_id[$k];
				$item[$num]['price'] = $item_price[$k];
				$item[$num]['count'] = $item_count[$k];
				$item[$num]['startTm'] = !empty($start_time[$k]) ? strtotime($start_time[$k]) : time();
				$item[$num]['endTm'] = !empty($end_time[$k]) ? strtotime($end_time[$k]) : $item[$num]['startTm']+(86400*2);
				$item[$num]['groupId'] = $vv;
				
				$num++;
			}
		}
		
		//如果物品数量大于200，就不给发送
		if($num >200){

			exit(json_encode(array('code'=>0 , 'msg' => '物品数量不能超过200')));	
		}
		
		$ip = TRADE_IP;
		
		$port =TRADE_PORT;
		
		$rs = $this->dgm->rpcCall($ip , $port , 'additem' , $item);
		
		//$rs = 0;
		if($rs === 0){
			
			exit(json_encode(array('code'=>1 , 'msg' => '添加物品成功')));	
		}else{
			
			exit(json_encode(array('code'=>0 , 'msg' => '添加物品失败，请重试')));	
		}
	}
	
	//获取配置表的全部物品
	public function getAllItem(){
        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";
		
		$server = $this->dgm->query($sql);
		
		$AllItem= $this->call('GiftModel' , 'getItem' , array() , $server[0]['server_id']);//全部物品

		$AllItem = $AllItem[$server[0]['server_id']];
		
		$item = array();
		
		//print_r($AllItem);
		foreach($AllItem as $k=>$v){
			
			$item[] =$k.'/'. $v;
		
		}
		echo json_encode($item);
			
	}

	//开启关闭拍卖行
    public function trade(){

    	$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        $this->smarty->display('dealgroup/trade.tpl');
    }


    //拍卖行
    public function trade_action(){

        $type   = $this->getParam('type');

        $group  = $this->getParam('channel_group');
        $server = $this->getParam('server');

        //$server = implode("','", $server);

        $params = array();

        $where = 1;

        if(!empty($group[0])){
        	$group_id = implode(',', $group);
        	$where .= " and  group_id  in  ($group_id)";
        }

        if(!empty($server[0])){
        	$server_id = implode("','", $server);
        	$where .=" and server_id  in  ('$server_id')";
        }

        $sql = "select channel_num,num,server_id from ny_server where $where ";
       
        $rs = $this->sm->query($sql);

        $servers = array();

        foreach ($rs as $k2 => $r2) {
          $psId          = $r2['channel_num'];
          $sid           = $r2['num'];
          $sign          = $r2['server_id']; 
          $params[$sign] = array( 0 => array('pId' => $psId, 'sId' => $sid));
          $servers[$sign] = $r2;

        }

      

        //todo 发送交易组
        $url = Helper::v2();
        if($type == 1){

            $res = Helper::rpc($url , $servers,'openTrade' , $params);

        }else{

            $res = Helper::rpc($url , $servers,'closeTrade' , $params);
        }
       		
        foreach ($res as $key => $value) {
            $msg .= $key .':'. ($value == 1 ? '成功' : '失败')."\r\n"; 
        }

        
        echo $msg;
    }
	
    public function uploadCsv(){
    
    	if ($_FILES["file"]["error"] > 0){
    		die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "错误: " . $_FILES["file"]["error"] . "<br />")));
    	}else{
    		$item_tpl = $this->getParam('item_tpl');
    		
    		if (!$item_tpl){
    			die(json_encode(array('data'=>'' ,  'status' =>0 , 'msg' => "请选择道具模板类型")));
    		}
    		
    		$filename = $_FILES["file"]["name"];
    		var_dump($filename);die;
    		$suffix = substr($filename, strrpos($filename, '.') + 1);
    		$filetype = array('csv' ,'xlsx' , 'xls');
    
    		if(!in_array($suffix , $filetype)){
    
    			die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "只允许上传Excel文件")));
    		}else if($_FILES['file']['size'] > 20*1024*1024){
    
    			die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "文件大小不允许超过20M")));
    		}else{
    			if(strtolower($suffix) == 'csv'){
    				$handle = fopen($_FILES['file']['tmp_name'],"r");
    				$csv = array();
    				while ($row = fgetcsv($handle)) {
    					foreach($row as $k=>$v){
    						$csv[$k][] = iconv('gbk','utf-8',$v);
    					}
    				}
    			}else{
    				$csv = Util::readExcel($_FILES["file"]["tmp_name"]);
    			}
    			if($csv){
    				
    				
    				unset($csv[1]);
    				
    				$rs = array();
    			   //上传表头
                    $info = array(0=>'item_id', 1=>'price', 2=>'num');

                    foreach ($csv as $k => $v) {
                        foreach ($v as $key => $value) {
                           $rs[$k][$info[$key]] =$value ;
                        }
                        $rs[$k]['item_tpl'] = $item_tpl ;
                    }
                    $sql = "DELETE FROM ny_item_buy WHERE item_tpl = '{$item_tpl}';";
                    $this->ic->query($sql);
                    
                    $result = $this->multInsert('ny_item_buy',$rs);
                    if($result !== false){
                        $return = array('status' => 1 , 'msg' => '添加成功');
                    }else{
                        $return = array('status' => 0 , 'msg' => '添加失败，请检查文件');
                    }
    			
    
    			}else{
    
    				$return = array('status' => 0 , 'msg' => '请检查文件是否为空');
    			}
    
    			exit(json_encode($return));
    
    		}
    	}
    
    }
    
    /**
     * 分批次插入
     * @return [type] [description]
     */
    public static function multInsert($table, $data) {
    	$tmp = $data;
    	$keys = array_keys(array_pop($tmp));
    
    	$len = count($data);
    
    	$length = 1000;
    
    	$times = ceil($len / $length);
    
    	foreach ($keys as &$v) {
    		$v = "`{$v}`";
    	}
    	unset($v);
    
    
    	$dbh = new Model();
    
    	$insertId = 0;
    
    	//防止单次插入过多，分批次插入
    	for ($i = 0; $i < $times; $i++) {
    		$sql = "INSERT INTO {$table} (". implode(',', $keys).") values ";
    
    		$offset = ($i * $length);
    		$tmp = array_slice($data, $offset, $length);
    
    		$r2 = array();
    		foreach ($tmp as $k => $row) {
    
    			foreach ($row as $k2 => &$v2) {
    				$v2 = "'{$v2}'";
    			}
    
    			$r2[$k] = "(" . implode(',', $row) . ")";
    		} 
    
    		$sql .= implode(',', $r2);
    
    		try {
    			$dbh->beginTransaction();
    
    			$res = $dbh->query($sql);
    
    			$insertId = $dbh->lastInsertId();
    
    			$dbh->commit();
    
    		} catch (Exception $e) {
    			$dbh->rollBack();
    		}
    
    	}
    	//end for
    
    	return $insertId;
    
    }
    
    public function tpl_item() {
    	$this->smarty->assign('item_tpl',CDict::$item_tpl);
    	$this->smarty->display('dealgroup/tpl_item.tpl');
    }
    
    public function export(){
    	$m = new Model();
    	$item_tpl = $this->getParam('item_tpl');
    	if ($item_tpl) {
    		$itemData = $this->ic->getRows(['item_id','price','num'],['item_tpl'=>$item_tpl]);
    		$result[] = array(
    				'道具id', '价格','数量'
    		);
    		foreach($itemData as $row){
    			$result[] = $m->formatFields($row);
    		}
    		 
    		Util::exportExcel($result , '交易系统道具模板导出（' . date('Y年m月d日H时i分s秒') . '）');
    			
    	}
    
    }
	
}
