<?php
!defined('IN_WEB') && exit('Access Denied');

class FactionController extends Controller{
    private $zm;

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $tabs = array(
            array('title' => '帮派列表', 'url' => 'admin.php?&ctrl=faction&act=factionlist'),
			//array('title' => '帮派成员', 'url' => 'admin.php?&ctrl=faction&act=factionmember'),
        );
        $this->tabs($tabs);
    }
	public function factionlist(){
		$this->setSearch('帮派ID' ,'input');
		$this->setSearch('帮派名' ,'input');
		$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
		$this->setFields(array('ID','名字','等级','帮主','创建时间'));	
		$this->setSource('faction' , 'factionlist_data' );
		$js = <<< END
		 
END;
        $this->setJs($js);
		$this->displays();
	}
	public function factionlist_data(){
		$this->setCondition(0 , 'eq','faction_id');
		$this->setCondition(1 , 'eq','name');
		$this->setCondition(2 , 'scp');
		$this->setLimit();
		$this->call('FactionModel' , 'factionlist_data' , $this->getConditions());
	}
	public function factionmember(){
		$id='27E478E5-683F-4D15-85D8-7441E2F1F9B6';
		$server='uc001';
		$result=$this->call('FactionModel','getMember',array('id'=>$id),$server);
		//print_r($result);
		$this->smarty->assign('member',$result);
		$this->smarty->display('faction/member.tpl');
		
	}
  
}