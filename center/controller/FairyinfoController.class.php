<?php
!defined('IN_WEB') && exit('Access Denied');

class FairyInfoController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启护送仙女的人数。<br>';
        $header .= '参与人数：该日进行过护送仙女（进行的护送≥1次）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均护送次数 = 该日全服所有玩家进行护送次数之和 / 参与人数   （确定到小数点后两位，四舍五入）<br>';
        $header .= '拦截人数 = 该日进行过拦截的玩家数  去重<br>';
        $header .= '拦截参与度 = 拦截人数 / 活跃人数 <br>';
        $header .= '人均拦截次数 = 该日全服所有玩家进行拦截次数之和 / 拦截人数   （确定到小数点后两位，四舍五入）<br>';
        $header .= '刷新参与度 = 该日进行过刷新仙女的人数（含选择女帝） / 参与人数 *100%  <br>';
        $header .= '人均刷新次数 = 该服刷新仙女的总次数（含选择女帝） /   该日进行过刷新仙女的人数（含选择女帝） <br>';
        $header .= '选择女帝参与度  =  该日进行过选择女帝的人数 / 参与人数中≥vip6的玩家数 *100%<br>';
        $header .= '人均选择女帝次数 = 该日选择女帝的总次数 / 该日进行过选择女帝的人数 <br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '人均护送次数', '拦截人数', '拦截参与度', '人均拦截次数', '刷新参与度', '人均刷新次数', '选择女帝参与度', '人均选择女帝次数'));
        $this->setSource('FairyInfo', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('FairyInfoModel', $method, $this->getConditions());
    }
}