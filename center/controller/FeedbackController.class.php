<?php
!defined('IN_WEB') && exit('Access Denied');

class FeedbackController extends Controller{

    public function __construct() {
        parent::__construct();
        $this->db = new FeedbackModel();
    }

    //角色统计（菜单）
    public function index(){
        $tabs = array(
            array('title' => '玩家反馈', 'url' => 'admin.php?&ctrl=feedback&act=record'),
            //array('title' => '玩家回复', 'url' => 'admin.php?&ctrl=feedback&act=reply')
        );
        $this->tabs($tabs);
    }


    //（菜单）
    public function record(){
        //设置页面头部内容
        /*
        $header  = '说明：<br>';
        $header .= '1. 订单状态分为两种状态，成功表示订单正常交易成功，失败则说明订单的正常流程中途出错，例如充值不到账等';
        $this->setHeader($header);
        */
        //设置搜索项，按照页面显示的顺序设置
        $this->setSearch('角色名');
        $this->setSearch('充值金额', 'range');
        $this->setSearch('类型', 'select', CDict::$questionType);

        $statusArr = [
            -1 => '-',
            0 => '未回',
            1 => '已回'
        ];

        $this->setSearch('状态', 'select', $statusArr);

        $this->setSearch('服务器', 'scp', array(0, 0, 1, 0));
        //设置显示项，按照页面显示的顺序设置

        $this->setFields(array('渠道', '服务器', '类型', '角色名', '充值金额', '内容', '提交时间', '回复', '回复人')); //如果显示项只需要设置名称，则可使用setFields方法批量设置显示项
      
        //设置获取页面数据的类方法
        $this->setSource('feedback' , 'record_data' , 1);
        $js = <<< END

            function attr(id, server_id, type, role_id, uuid){
                \$tabs.tabs('select' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=feedback&act=reply' + '&id=' + id + '&server_id=' + server_id + '&type=' + type + '&role_id='+ role_id + '&uuid=' + uuid);
                \$tabs.tabs('load' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=feedback&act=reply' + '&id=' + id + '&server_id=' + server_id + '&type=' + type + '&role_id='+ role_id + '&uuid=' + uuid);
            }
            
            var div1;
            var div2;
            
            function replyHtml(id, server_id, type, role_id, uuid) {            
                $.ajax({
                    url: 'admin.php?ctrl=feedback&act=reply_html',
                    type: 'post',
                    data: {'id': id, 'server_id': server_id, 'type': type, 'role_id': role_id, 'uuid': uuid},
                    dataType: 'json'
                }).done(function(data){                    
                    if (data.status != 1) {                        
                        $.dialog.tips(data.msg);                      
                        return false;
                    }                        				
                    div1 = $.dialog({
                        title: data.title,
                        max: false,
                        min: false,
                        content: data.html
                    });
                });          
            }
            
            function reply(id, server_id, type, role_id, uuid) {
                
                var text = $('textarea').val();    
                
                if (text == '') {
                     $.dialog.tips('请输入回复内容');
                     return false;            
                }                     
            
                $.ajax({
                    url: 'admin.php?ctrl=feedback&act=post_reply',
                    type: 'post',
                    data: {
                        'text' : text,
                        'fid' : id,
                        'server_id' : server_id,
                        'type' : type,
                        'role_id': role_id,
                        'uuid': uuid
                    },
                    dataType: 'json'
                }).done(function(data){                    
                    if (data.code === 0) {
                    $.dialog.tips('添加成功');
                    div1.close();                   
                    \$dataTable.fnMultiFilter(getFilter());
                  } else {
                    $.dialog.tips('添加失败');
                  }                      
                }); 
            }
            
            function history(role_id, server_id) {
                $.ajax({
                    url: 'admin.php?ctrl=feedback&act=history',
                    type: 'post',
                    data: {'role_id': role_id, 'server_id': server_id},
                    dataType: 'json'
                }).done(function(data){                    
                    if (data.status != 1) {                        
                        $.dialog.tips(data.msg);                      
                        return false;
                    }                        				
                    div2 = $.dialog({
                        title: data.title,
                        max: false,
                        min: false,
                        content: data.html
                    });
                });                           
            }
           
            
END;
        $this->setJs($js);
        //显示页面
        $this->displays();
    }
    
    /**
     * [玩家反馈数据]
     * @return [type] [description]
     */
    public function record_data(){
        //设置搜索条件
        $this->setCondition(0, 'eq', 'f.name');
        $this->setCondition(1, 'between', 'o.money');
        $this->setCondition(2, 'eq', 'f.type');
        $this->setCondition(3, 'eq', 'f.status');
        $this->setCondition(4, 'scp');
        //设置搜索排序
        $this->setOrder('f.create_time', 'desc');
        //设置搜索条数限制
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');

        $this->call('FeedbackModel' , $method , $this->getConditions());
    }

    /**
     * [玩家回复]
     * @return [type] [description]
     */
    public function reply() {
        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        $server_id = isset($_GET['server_id']) ? trim($_GET['server_id']) : ''; 
        $type  = isset($_GET['type']) ? intval($_GET['type']) : 0;
        $role_id = isset($_GET['role_id']) ? doubleval($_GET['role_id']) : 0;
        $uuid = isset($_GET['uuid']) ? trim($_GET['uuid']) : '';

        if (empty($id)) {
            echo '非法参数';
            die();
        }

        if (empty($server_id)) {
            echo '非法参数';
            die();
        }

        if (empty($role_id)) {
            echo '非法参数';
            die();
        }

        if (empty($role_id)) {
            echo '非法参数';
            die();
        }

        $reply = $this->db->find_reply($id, $server_id);

        foreach ($reply as $key => &$row) {
            $row['create_time'] = date('y-m-d H:i:s', $row['create_time']);
        }
        unset($row);

        $this->smarty->assign('fid', $id);
        $this->smarty->assign('role', $role_id);
        $this->smarty->assign('server_id', $server_id);
        $this->smarty->assign('type', $type);
        $this->smarty->assign('role_id', $role_id);
        $this->smarty->assign('uuid', $uuid);
        
        $this->smarty->assign('reply',$reply);
        $this->smarty->display('feedback/reply.tpl');
    }

    public function post_reply() {
       $server_id = trim($_POST['server_id']);
       $text = trim($_POST['text']);
       $fid = intval($_POST['fid']);
       $type = intval($_POST['type']);
       $role_id = doubleval($_POST['role_id']);
        $uuid = trim($_POST['uuid']);

        $msg = $this->db->post_reply($server_id, $fid, $text, $role_id, $type, $uuid);
       
       $arr = array('code' => $msg ,  'time' => date('y-m-d H:i:s'));
       die(json_encode($arr));
    }

    public function reply_html()
    {
        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
        $server_id = isset($_POST['server_id']) ? trim($_POST['server_id']) : '';
        $type = isset($_POST['type']) ? intval($_POST['type']) : 0;
        $role_id = isset($_POST['role_id']) ? doubleval($_POST['role_id']) : 0;
        $uuid = isset($_POST['uuid']) ? trim($_POST['uuid']) : '';

        $returnJson = [
            'status' => 0,
            'msg' => '非法参数'
        ];

        if ($id && $server_id && $type && $role_id && $uuid) {
            $returnJson['status'] = 1;
            unset($returnJson['msg']);

            $str = $id . "," . "'" . $server_id . "'" . "," . $type . "," . $role_id . ",'" . $uuid . "'";

            $html = <<<END
        <table class="itable itable-color" id="replyTable">
            <tbody>
              <tr>
                  <td style="width:150px;">回复</td>
                  <td>
                      <textarea name="tips" id="tips" style="width:600px;height:70px;margin:0;"></textarea>
                  </td>
              </tr>
              <tr>
                  <td style="width:150px;"></td>
                  <td>
                      <input type="button" value="提交" class="gbutton" onclick="reply({$str})" >
                  </td>
              </tr>
            </tbody>
        </table>
END;
            $returnJson['title'] = '回复玩家';
            $returnJson['html'] = $html;
        }

        echo json_encode($returnJson);
    }

    function history()
    {
        $role_id = isset($_POST['role_id']) ? doubleval($_POST['role_id']) : 0;
        $server_id = isset($_POST['server_id']) ? trim($_POST['server_id']) : '';

        $returnJson = [
            'status' => 0,
            'msg' => '非法参数'
        ];

        if ($role_id && $server_id) {
            $returnJson['status'] = 1;
            unset($returnJson['msg']);

            //获取角色历史所有数据
            $data = $this->db->roleHistory($role_id, $server_id);

            $html = '';

            if (!empty($data)) {
                $html = <<<End
  <table border="0" id="baseinfo">
    <thead>
      <th>玩家昵称</th>
      <th>接收时间</th>
      <th>类型</th>
      <th>问题内容</th>
      <th>是否回复</th>
      <th>回复人</th>
      <th>回复时间</th>
      <th>回复内容</th>
    </thead>

    <tbody>
End;
                foreach ($data as $row) {
                    $html .= '<tr class="even">';
                    $html .= '<td  style="padding: 5px;">' . $row['name'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['create_time'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['type'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['content'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['status'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['reply_user'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['reply_time'] . '</td>';
                    $html .= '<td  style="padding: 5px;">' . $row['reply'] . '</td>';
                    $html .= '</tr>';
                }

                <<<End
    </tbody>
  </table>
End;
            }
            $returnJson['html'] = $html;
            $returnJson['title'] = '玩家提问历史';
        }

        echo json_encode($returnJson);
    }
}