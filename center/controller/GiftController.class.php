<?php
!defined('IN_WEB') && exit('Access Denied');

class GiftController extends Controller
{
    private $gm;
    private $cgm;
    private $db;
    private $groupAccs = array();

    use tChannelGroupServerAccess;

    public function __construct()
    {
        parent::__construct();
        $this->gm = new GiftModel();
        $this->cgm = new ChannelgroupModel();
        $groupAcce = self::getChannelGroupByTrait();
        $this->groupAccs = array_column($groupAcce, 'id');
    }

    //兑换码（菜单）
    public function index()
    {
        $tabs = array(
            array('title' => '礼包列表', 'url' => 'admin.php?&ctrl=gift&act=record'),
            array('title' => '添加礼包', 'url' => 'admin.php?&ctrl=gift&act=add'),
            array('title' => '编辑礼包', 'url' => 'admin.php?&ctrl=gift&act=edit'),
            array('title' => '兑换码查询', 'url' => 'admin.php?&ctrl=gift&act=check')
        );
        $this->tabs($tabs);
    }

    public function record()
    {
        $header = '';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        $this->setFields(array(
            'ID', '说明', '使用次数', '类型', '生效时间', '过期时间', '服务器', '道具',
            '生成个数', '追加个数', '添加者', '添加时间', '发布', '禁用'
        ));
        $this->setField('操作', 600);
        //$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setSource('gift', 'record_data', 1);
        $js = <<< END
            function _export1(id , num){
                var url = 'admin.php?ctrl=gift&act=export&id=' + id + '&num=' + num;
                window.open(url);
            }
            function _export_select(id){
                $.ajax({
                    url: 'admin.php?ctrl=gift&act=export_select',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'html'
                }).done(function(html){
                    $.dialog({
                        title: '选择导出',
                        content: html,
                        max: false,
                        min: false
                    });
                });
            }
            
            function _check(type, id){            
                var str = type === 1 ? '同意并发布' : '拒绝发布';           
                $.dialog({
                    title: '',
                    content: '确定要'+str+'吗？',
                    max: false,
                    min: false,
                    button : [{
                            name : '确定',
                            callback: function(){
                                $.ajax({
                                    url: 'admin.php?ctrl=gift&act=check_gift',
                                    type: 'post',
                                    data: {type: type, id: id},
                                    dataType: 'json'
                                }).done(function(data){
                                    $.dialog.tips(data.msg);
                                    if (data.status == 200) {
                                         \$dataTable.fnMultiFilter(getFilter());
                                    }                                  
                                });                     
                            }
                        },{ name : '取消' }]
                });
            }

            var moreDiv = '';           
            function _more(id){
                $.dialog({
                    title: '',
                    content: '确定要追加兑换码？',
                    max: false,
                    min: false,
                    button : [{
                            name : '确定',
                            callback: function(){
                                $.ajax({
                                    url: 'admin.php?ctrl=gift&act=more_html',
                                    type: 'post',
                                    data: 'id=' + id,
                                    dataType: 'json'
                                }).done(function(data){                                  
                                    if (data.status == 400) {                        
                                        $.dialog.tips(data.msg);                      
                                        return false;
                                    }                        				
                                    moreDiv = $.dialog({
                                        title: data.title,
                                        max: false,
                                        min: false,
                                        content: data.html
                                    });
                                    
                                });
                            }
                        },{ name : '取消' }]
                });
            }
            
            function _submitMore(id) {
                
                var pnum = $('#pnum').val();    
                if (pnum === 0) {
                     $.dialog.tips('请输入追加数量');
                     return false;            
                }                                 
                $.ajax({
                    url: 'admin.php?ctrl=gift&act=more_action',
                    type: 'post',
                    data: {
                        'id' : id,
                        'pnum' : pnum,
                    },
                    dataType: 'json'
                }).done(function(data){                    
                    if (data.status === 200) {
                    $.dialog.tips(data.msg);
                    moreDiv.close();                   
                    \$dataTable.fnMultiFilter(getFilter());
                  } else {
                    $.dialog.tips(data.msg);
                  }                      
                }); 
              
            
            }
            
            function _cancelMore() {
                if (moreDiv) {
                     moreDiv.close();
                }
            }            
                  
            
            function _item(id){
                $.ajax({
                    url: 'admin.php?ctrl=gift&act=item_action',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json'
                }).done(function(data){
                    $.dialog({
                        title: data.title,
                        max: false,
                        min: false,
                        content: data.html
                    });
                });
            }

            function _forbid(type, id){
            var str = type === 1 ? '禁用' : '恢复'; 
                $.dialog({
                    title: '',
                    content: '确定要'+ str +'兑换码？',
                    max: false,
                    min: false,
                    button : [{
                            name : '确定',
                            callback: function(){
                                $.ajax({
                                    url: 'admin.php?ctrl=gift&act=forbid',
                                    type: 'post',
                                    data: {type: type, id: id},
                                    dataType: 'json'
                                }).done(function(data){
                                    $.dialog.tips(data.msg);
                                    if (data.status == 200) {
                                         \$dataTable.fnMultiFilter(getFilter());
                                    }  
                                });
                            }
                        },{ name : '取消' }]
                });
            }

           function _edit(sid){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=gift&act=edit&id=' + sid);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=gift&act=edit&id=' + sid);
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'date', 'date');
        //$this->setCondition(1 , 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('GiftModel', $method, $this->getConditions());
    }

    public function export()
    {
        $id = $this->getParam('id');
        $num = $this->getParam('num');
        $this->gm->_export($id, $num);
    }

    public function export_select()
    {
        $id = $this->getParam('id');
        //$gid = $this->gm->formatGid($id);
        $gift = file_get_contents(ROOT . '/export/gift/' . $id . '.txt');
        $gift = json_decode($gift, true);
        $html = '<input type="button" class="gbutton" onclick="_export1(' . $id . ' , -1)" value="全部兑换码" style="margin-bottom:5px;"><br>';
        $html .= '<input type="button" class="gbutton" onclick="_export1(' . $id . ' , 0)" value="初次导入的兑换码" style="margin-bottom:5px;"><br>';
        for ($i = 1; $i < count($gift); $i++) {
            $html .= '<input type="button" class="gbutton" onclick="_export1(' . $id . ' , ' . $i . ')" value="第' . $i . '次追加的兑换码" style="margin-bottom:5px;"><br>';
        }
        echo $html;
    }

    public function add()
    {
        $channelGroup = $this->cgm->getChannelGroup(array(), array('id', 'name'));
        $this->smarty->assign('channelGroup', $channelGroup);

        //服务器，渠道组

        $server = (new ServerModel())->getServerByTrait();
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->assign('giftCDKEYType', CDict::$giftCDKEYType);
        $this->smarty->display('gift/add.tpl');
    }

    public function scp_action()
    {
        $id = $this->getParam('id');
        $channelGroup = $this->cgm->getChannelGroup($id, array('server'));
        $server = json_decode($channelGroup['server'], true);
        $server = $server ? $server : 'no server';
        echo $this->getScp(array('showGroup' => 0, 'showServer' => 1, 'showChannel' => 0, 'showPackage' => 0), array(), $server);
    }

    /**
     * [添加礼包码]
     */
    public function add_action()
    {
        $channelGroup = $this->getParam('channel_group');
        $server = $this->getParam('server');
        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;

        $return = [
            'status' => 400,
            'msg' => '参数错误'
        ];

        //判断是否为所有渠道，并过滤若选了其他渠道再选所有渠道的情况
        if (!empty($channelGroup) && count($channelGroup) == 1 && in_array('', $channelGroup, true)) {
            $channelGroup = array();
        } else {
            $channelGroup = !empty($channelGroup) ? array_filter($channelGroup) : array();
        }

        if (empty($channelGroup)) {
            $channelGroup = $this->groupAccs;
        }
        //服务器
        if (!empty($server) && count($server) == 1 && in_array('', $server, true)) {
            $server = array();
        } else {
            $server = !empty($server) ? array_filter($server) : array();
        }

        $num = $this->getParam('num', 0);
        $num = intval($num);
/*
        if ($num > 20000) {
            $return['msg'] = '单次最多发送5000; 如果要增加请后续追加';
            die(json_encode($return));
        } */

        $info = $this->getParam('info');
        $item_id = $this->getParam('item_id');
        $item_count = $this->getParam('item_count');
        $open_time = $this->getParam('open_time');
        $end_time = $this->getParam('end_time');
        $times = $this->getParam('times');
        $sync = $this->getParam('issync');
        $sync = $sync[0];

        $pnum = $this->getParam('pnum', 0);
        $type = $this->getParam('type');
        $item = array();

        $Goods = $this->getGoodsItem(0);

        if (empty($item_id) || !is_array($item_count) || !is_array($item_id) || empty($item_count) || count($item_id) != count($item_count)) {
            $return['msg'] = '物品信息有误; 请重试';
            die(json_encode($return));
        }

        foreach ($item_id as $k => $v) {
            if (!in_array($v, $Goods)) {
                $return['msg'] = '物品信息' . $v . '不是游戏物品; 请重试';
                die(json_encode($return));
            }

            $v_count = (int)$item_count[$k];

            if ($v_count <= 0) {
                $return['msg'] = '物品' . $v . '数量不正常; 请重试';
                die(json_encode($return));
            }

            $res = explode('/', $v);
            $goods_id = isset($res[0]) ? $res[0] : $v;
            $item[$k] = array((int)trim($goods_id), $v_count);
        }

        //@todo 查找服务器列表
        $serverList = $this->gm->getServer($server, $channelGroup);

        $data = array();
        $data['agent'] = implode(',', $channelGroup);
        $info && $data['info'] = $info;
        $item && $data['item'] = json_encode($item);
        $open_time && $data['open_time'] = strtotime($open_time);
        $end_time && $data['end_time'] = strtotime($end_time);
        $data['issync'] = (int)$sync;

        $data['server'] = json_encode($server);

        $times && $data['times'] = (int)$times;
        $num && $data['num'] = (int)$num;
        $pnum && $data['pnum'] = (int)$pnum;
        $gift = $this->gm->getGift('', array('max(sort) as max_sort'));
        $data['sort'] = $gift['max_sort'] ? $gift['max_sort'] + 1 : 1;
        $data['type'] = (int)$type;
        $data['user'] = $_SESSION['username'];
        $data['create_time'] = $id ? (int)$this->getParam('create_time') : time();

        if (empty($id)) {
            $insertId = $this->gm->addGift($data);
            $return['msg'] = $insertId ? '添加成功' : '添加失败';
            $return['status'] = $insertId ? 200 : 400;
        } else {
            //直接发布为新的
            $server_config = $this->serverConfig(array_keys($serverList));
            //发送给后端
            $sendInfo = [];
            $sendInfo['opcode'] = Pact::$CDKEY['Produce'];
            $CDKEY = [(int)$id, [], (int)($data['open_time'] * 1000), (int)($data['end_time'] * 1000), $item, (int)$data['times'], (int)$data['type']];
            $sendInfo['str'] = [$CDKEY];
            $msg = '';
            foreach ($server_config as $row) {
                $rs = $this->gm->socketCall($row['websocket_host'], $row['websocket_port'], 'giftCDKEY', $sendInfo);

                if ($rs === 0) {
                    $msg .= $row['name'] . ': 发送成功';
                } else {
                    $msg .= $row['name'] . ': 发送失败';
                }
            }

            $this->gm->update($data, array('id' => $id));

            $return['status'] = 200;
            $return['msg'] = $msg;
        }

        die(json_encode($return));
    }

    public function edit()
    {
        $id = $this->getParam('id');

        if (empty($id)) {
            echo '请选择要编辑的礼包码';
            die();
        }

        $sql = "select * from ny_gift where id  = '{$id}' limit 1";
        $data = $this->gm->fetchOne($sql);

        if (empty($data)) {
            echo '查询不到数据';
            die();
        }

        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = $this->gm->query($sql);

        $AllItem = $this->call('GiftModel', 'getItem', array(), $server[0]['server_id']);//全部物品

        $goods = $AllItem[$server[0]['server_id']];

        $item = json_decode($data['item'], true);

        foreach ($item as $k => &$row) {
            $goods_id = $row[0];
            $name = isset($goods[$goods_id]['name']) ? $goods[$goods_id]['name'] : '';

            $row[0] = $row[0] . '/' . $name;
        }

        unset($row);

        $data['item'] = $item;


        $data['open_time'] = date('Y-m-d', $data['open_time']);
        $data['end_time'] = date('Y-m-d', $data['end_time']);

        $server = (new ServerModel())->getServerByTrait();
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        $data['agent'] = array_filter(explode(',', $data['agent']));
        $data['server'] = json_decode($data['server'], ture);

        $this->smarty->assign('data', $data);
        $this->smarty->assign('giftCDKEYType', CDict::$giftCDKEYType);
        $this->smarty->display('gift/edit.tpl');
    }

    /**
     * [追加礼包码]
     * @return [type] [description]
     */
    public function more_html()
    {
        $id = $this->getParam('id');
        $gift = $this->gm->getGift($id, array('id'));

        $return = [
            'status' => 400,
        ];

        if (empty($id)) {
            $return['msg'] = '请选择要追加的礼包';
            die(json_encode($return));
        }

        if (empty($gift)) {
            $return['msg'] = '参数错误';
            die(json_encode($return));
        }

        $html = <<<END
        <table class="itable itable-color" id="moreTable">
            <tbody>               
              <tr>
                  <td>
                      <input type="number" name="pnum" id="pnum">
                  </td>
              </tr>
              <tr>
                  <td>
                      <input type="button" value="提交" class="gbutton" onclick="_submitMore({$id})" >
                      <input type="button" value="取消" class="gbutton" onclick="_cancelMore()" >
                  </td>
              </tr>
            </tbody>
        </table>
END;
        $return['title'] = '请输入追加数量';
        $return['html'] = $html;

        $return['status'] = 200;

        die(json_encode($return));

    }


    /**
     * [追加礼包码]
     * @return [type] [description]
     */
    public function more_action()
    {
        $id = $this->getParam('id');
        $pnum = $this->getParam('pnum');

        $return = [
            'status' => 400,
            'msg' => '参数错误'
        ];

        if (!$id && !$pnum) {
            die(json_encode($return));
        }

        /*
        if ($pnum > 5000) {
            $return['msg'] = '单次最多发送5000; 如果要增加请后续追加';
            die(json_encode($return));
        } */

        $gift = $this->gm->getGift($id, array('id', 'server', 'num', 'pnum', 'open_time', 'end_time', 'times', 'item', 'type', 'agent'));
        $server = json_decode($gift['server'], true);
        if ($gift['agent'] == '') {
            $channelGroup = [];
        } else {
            $channelGroup = explode(',', $gift['agent']);
        }

        $serverList = $this->gm->getServer($server, $channelGroup);

        $server_config = $this->serverConfig(array_keys($serverList));

        $this->gm->addCode($gift, $pnum);

        $item = json_decode($gift['item'], true);

        //发到后端
        $sendInfo = [];
        $sendInfo['opcode'] = Pact::$CDKEY['Produce'];
        $CDKEY = [(int)$gift['id'], null, (int)($gift['open_time'] * 1000), (int)($gift['end_time'] * 1000), $item, (int)$gift['times'], (int)$gift['type']];
        $sendInfo['str'] = [$CDKEY];
        $msg = '';
        foreach ($server_config as $row) {
            $rs = $this->gm->socketCall($row['websocket_host'], $row['websocket_port'], 'giftCDKEY', $sendInfo);

            if ($rs === 0) {
                $msg .= $row['name'] . ': 发送成功';
            } else {
                $msg .= $row['name'] . ': 发送失败';
            }
        }

        $this->gm->update(['pnum' => $gift['pnum'] + $pnum], ['id' => $gift['id']]);

        $return['status'] = 200;
        $return['msg'] = $msg;

        die(json_encode($return));
    }

    /**
     * [审核并发布礼包码]
     * @return [type] [description]
     */
    public function check_gift()
    {
        $id = (int)$this->getParam('id');
        $type = (int)$this->getParam('type');

        $return = [
            'status' => 400,
            'msg' => '参数错误'
        ];

        if (empty($id)) {
            die(json_encode($return));
        }

        if ($type != 1 && $type != 2) {
            die(json_encode($return));
        }

        $return['status'] = 200;
        if ($type == 1) {

            $gift = $this->gm->getGift($id);

            if (empty($gift)) {
                die(json_encode($return));
            }

            $item = json_decode($gift['item'], true);

            $server = json_decode($gift['server'], true);
            if ($gift['agent'] == '') {
                $channelGroup = [];
            } else {
                $channelGroup = explode(',', $gift['agent']);
            }

            $serverList = $serverList = $this->gm->getServer($server, $channelGroup);;

            $server_config = $this->serverConfig(array_keys($serverList));

            $this->gm->generateCode($gift);

            //发送给后端
            $sendInfo = [];
            $sendInfo['opcode'] = Pact::$CDKEY['Produce'];
            $CDKEY = [(int)$gift['id'], null, (int)($gift['open_time'] * 1000), (int)($gift['end_time'] * 1000), $item, (int)$gift['times'], (int)$gift['type']];
            $sendInfo['str'] = [$CDKEY];
            $msg = '';
            foreach ($server_config as $row) {
                $rs = $this->gm->socketCall($row['websocket_host'], $row['websocket_port'], 'giftCDKEY', $sendInfo);

                if ($rs === 0) {
                    $msg .= $row['name'] . ': 发送成功';
                } else {
                    $msg .= $row['name'] . ': 发送失败';
                }
            }

            $return['msg'] = $msg;
        } else {
            $return['msg'] = '已拒绝';
        }

        $this->gm->update(['ischeck' => $type], array('id' => $id));
        die(json_encode($return));
    }

    /**
     * [禁用恢复礼包码]
     * @return [type] [description]
     */
    public function forbid()
    {
        $type = (int)$this->getParam('type');
        $id = $this->getParam('id');

        $return = [
            'status' => 400,
            'msg' => '参数错误'
        ];

        if (!$id) {
            die(json_encode($return));
        }

        if ($type !== 1 && $type !== 0) {
            die(json_encode($return));
        }

        $gift = $this->gm->getGift($id, array('server', 'id', 'agent'));

        if (empty($gift)) {
            die(json_encode($return));
        }

        $server = json_decode($gift['server'], true);
        if ($gift['agent'] == '') {
            $channelGroup = [];
        } else {
            $channelGroup = explode(',', $gift['agent']);
        }

        $serverList = $this->gm->getServer($server, $channelGroup);;

        $server_config = $this->serverConfig(array_keys($serverList));
        //发送到后端禁用
        //发送给后端
        $sendInfo = [];
        $sendInfo['opcode'] = Pact::$CDKEY['State'];
        $sendInfo['str'] = [(int)$gift['id'], (bool)$type];
        $msg = '';
        foreach ($server_config as $row) {
            $rs = $this->gm->socketCall($row['websocket_host'], $row['websocket_port'], 'giftCDKEY', $sendInfo);

            if ($rs === 0) {
                $msg .= $row['name'] . ': 发送成功';
            } else {
                $msg .= $row['name'] . ': 发送失败';
            }
        }

        $status = $type ? 0 : 1;

        $this->gm->update(array('status' => $status), array('id' => $id));

        $return['status'] = 200;
        $return['msg'] = $msg;

        die(json_encode($return));
    }

    public function check()
    {
        $this->setSearch('激活码');
        $this->setSearch('角色名');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setFields(array('编号', '激活码', '礼包id', '角色名', '领取时间'));
        $this->smarty->assign('select', 'radio');
        $this->setSource('gift', 'check_acton', 1);
        $this->displays();
    }

    public function check_acton()
    {
        $this->setCondition(0, 'eq', 'code');
        $this->setCondition(1, 'eq', 'role_name');
        $this->setCondition(2, 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('check_gift_acton', 'check_gift_export');
        $this->call('GiftModel', $method, $this->getConditions());
    }

    public function item_action()
    {
        $id = $this->getParam('id');
        $gift = $this->gm->getGift($id, array('name', 'item'));
        $item = json_decode($gift['item'], true);
        $html = '';
        $itemList = $this->getItem($id);
        foreach ($item as $row) {
            $html .= '<span style="color:#228b22;">' . $itemList[$row[0]]['name'] . '</span> <span style="color:#ff0000;">' . $row[1] . '个</span><br>';
        }
        echo json_encode(array('title' => $gift['name'], 'html' => $html));
    }

    public function getItem($id)
    {
        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = $this->gm->query($sql);

        $item = $this->call('GiftModel', 'getItem', array(), $server[0]['server_id']);
        return $item[$server[0]['server_id']];
    }

    /**
     * [查找礼包码]
     * @return [type] [description]
     */
    public function getGoodsItem($output = 1)
    {

        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = $this->gm->query($sql);

        $goods = $this->call('GiftModel', 'getItem', array(), $server[0]['server_id']);//全部物品

        $goods = $goods[$server[0]['server_id']];

        $item = array();

        //print_r($AllItem);
        foreach ($goods as $k => $v) {
            $item[] = $k . '/' . $v['name'];
        }
        if ($output == 1) {
            die(json_encode($item));
        }

        return $item;

    }

    public function serverConfig($serverList)
    {

        $return = [];
        $return['status'] = 400;

        $instance = new ServerModel();
        $server_config = $instance->getServer($serverList, ['s.server_id as server_id', 's.name as name', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port']);
        //判断服务器是否配置
        $serverIds = [];
        foreach ($server_config as $key => $msg) {
            if (empty($msg) || is_null($msg)) {
                array_push($serverIds, $key);
            }
        }
        $serRow = (new ServerModel())->getServer($serverIds, array('server_id', 'name'));
        $serInfo = array();
        foreach ($serRow as $tem) {
            $serInfo[$tem['server_id']] = $tem['name'];
        }
        $str = [];
        foreach ($serverIds as $sId) {
            array_push($str, $serInfo[$sId] . " : 服务器未配置<br/>");
        }
        if (!empty($str)) {
            $return['msg'] = implode('', $str);
            die(json_encode($return));
        }

        return $server_config;
    }

    /*
    public function reset_action(){
        $id = $this->getParam('id');
        $rs = $this->gm->getGift($id , array('server' , 'item' , 'open_time' , 'end_time'));
        $gift = file_get_contents(ROOT . '/export/gift/' . $id . '.txt');
        $gift = json_decode($gift , true);
        $document = array();
        foreach($gift as $k => $row){
            foreach($row as $code){
                $document[] = array(
                    'code' => $code,
                    'itemList' => json_decode($rs['item'] , true),
                    'activeId' => (int)$id,
                    'startTm' => (int)$rs['open_time'],
                    'endTm' => (int)$rs['end_time'],
                    'type' => (int)$rs['type'],
                    'flag' => 0
                );
            }
        }
        $server_id = json_decode($rs['server'] , true);
        $this->call('GiftModel' , 'resetGift' , array('document' => $document) , $server_id[0]);
        echo json_encode(array('msg' => '恢复成功'));
    }*/
}