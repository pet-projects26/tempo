<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/5/7
 * Time: 17:12
 */

class GiftOrderModel extends Model
{
    public function __construct()
    {
        parent::__construct('gift_order');
        $this->alias = 'a';
    }

    public function giftOrderListData($conditions)
    {
        unset($conditions['WHERE']['package::IN']);

        if ($conditions['WHERE']['server::IN']) {
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['a.server::IN'] = $server;
        }

        $rs = array();

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
            'p' => array('name' => 'package', 'type' => 'LEFT', 'on' => 'p.package_id = a.package'),
            'g' => array('name' => 'channel_group', 'type' => 'LEFT', 'on' => 'g.id = a.group')
        );
        $fields = array('a.id as id', 'a.order_num as order_num', 'a.corder_num as corder_num', 'g.name as `group`', 'p.name as package', 's.name as server', 'a.account as account', 'a.role_id as role_id', 'a.role_name as role_name', 'a.role_level as role_level', 'a.first as first', 'a.money as money', 'a.index as `index`', 'a.index_name as index_name', 'a.admin as admin', 'from_unixtime(a.create_time,"%Y-%m-%d %H:%i:%s") as create_time', 'a.checked as checked', 'a.send_time as send_time');
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $count = $this->getCount();

        foreach ($rs as $k => $row) {

            $row['first'] = $row['first'] ? '是' : '否';

            $row['send_time'] = $row['send_time'] ? date('Y-m-d H:i:s', $row['send_time']) : '未发送';

            $row['checked'] = $row['checked'] == 1 ? '<button class="btn btn-success">已审核</button> ' : '<input type="button" class="gbutton checked" value="审2核" onclick="_checked(\'' . $row['id'] . '\')">';

            unset($row['id']);
            $rs[$k] = array_values($row);
        }

        $this->setPageData($rs, $count);
    }

    public function giftOrderListExport($conditions)
    {
        unset($conditions['WHERE']['package::IN']);
        unset($conditions['Extends']['LIMIT']);
        $rs = array();
        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
            'p' => array('name' => 'package', 'type' => 'LEFT', 'on' => 'p.package_id = a.package'),
            'g' => array('name' => 'channel_group', 'type' => 'LEFT', 'on' => 'g.id = a.group')
        );
        $fields = array('a.id as id', 'a.order_num as order_num', 'a.corder_num as corder_num', 'g.name as `group`', 'p.name as package', 's.name as server', 'a.account as account', 'a.role_id as role_id', 'a.role_name as role_name', 'a.role_level as role_level', 'a.first as first', 'a.money as money', 'a.index as `index`', 'a.index_name as index_name', 'a.admin as admin', 'from_unixtime(a.create_time,"%Y-%m-%d %H:%i:%s") as create_time', 'a.checked as checked', 'a.send_time as send_time');
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $result = array();
        $result[] = array('订单号', '平台订单号', '渠道', '渠道包', '服务器', '账号', '角色ID', '角色名', '等级', '是否首赠', '赠送RMB', '赠送档位', '档位名称', '申请者', '创建时间', '审核', '发送时间'
        );
        foreach ($rs as $k => $row) {
            $result[$k + 1]['order_num'] = $row['order'];
            $result[$k + 1]['corder_num'] = $row['corder_num'];
            $result[$k + 1]['group'] = $row['group'];
            $result[$k + 1]['server'] = $row['server'];
            $result[$k + 1]['package'] = $row['package'];
            $result[$k + 1]['account'] = $row['account'];
            $result[$k + 1]['role_id'] = $row['role_id'];
            $result[$k + 1]['role_name'] = $row['role_name'];
            $result[$k + 1]['role_level'] = $row['role_level'];
            $result[$k + 1]['first'] = $row['first'] ? '是' : '否';
            $result[$k + 1]['money'] = $row['money'];
            $result[$k + 1]['index'] = $row['index'];
            $result[$k + 1]['index_name'] = $row['index_name'];
            $result[$k + 1]['admin'] = $row['admin'];
            $result[$k + 1]['create_time'] = $row['create_time'];
            $result[$k + 1]['cheacked'] = $row['checked'] == 1 ? '已审核' : '未审核';
            $result[$k + 1]['send_time'] = $row['send_time'] ? date('Y-m-d H:i:s', $row['send_time']) : '未发送';
        }
        Util::exportExcel($result, '赠送元宝列表（' . date('Y年m月d日H时i分s秒') . '）');

    }

    public function send($data)
    {
        //获取服务器配置
        $server_config = (new ServerconfigModel())->getConfig($data['server'], ['server_id', 'websocket_host', 'websocket_port']);

        if ($server_config['websocket_host'] == '' || $server_config['websocket_port'] == '') {
            exit(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
        }

        $sendOrder = [];

        array_push($sendOrder, [$data['order_num'], intval($data['role_id']), intval($data['index']), floatval($data['money']), intval($data['create_time']) * 1000, true]);

        $sendData = ['opcode' => Pact::$SendGoodsOperate, 'str' => array($sendOrder)];

        $result = $this->socketCall($server_config['websocket_host'], $server_config['websocket_port'], 'sendGoods', $sendData);

        if ($result === 0) {
            return 1;
        } else {
            return 2;
        }


    }
}