<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/5/7
 * Time: 16:05
 */
!defined('IN_WEB') && exit('Access Denied');

class GiftOrderController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $tabs = array(
            array('title' => '赠送列表', 'url' => 'admin.php?ctrl=giftOrder&act=info'),
            array('title' => '赠送操作', 'url' => 'admin.php?ctrl=giftOrder&act=add')
        );
        $this->tabs($tabs);
    }

    public function info()
    {
        $this->setSearch('账号');
        $this->setSearch('角色ID');
        $this->setSearch('角色');
        $this->setSearch('金额');
        $this->setSearch('', 'scp', array(1, 0, 1, 0));
        $this->setFields(array('订单号', '平台订单号', '渠道', '渠道包', '服务器', '账号', '角色ID', '角色名', '等级', '是否首赠', '赠送RMB', '赠送档位', '档位名称', '申请者', '创建时间', '审核', '发送时间'));
        $this->setSource('giftOrder', 'info_data', 1);

        $js = <<< END
            function _checked(id){
                if(confirm('确定3审核？')){
                    $.ajax({
                        url: 'admin.php?ctrl=giftOrder&act=check&id='+id,
                        type: 'GET',
                        dataType: 'JSON',
                        beforeSend:function(){
                            $(".checked").attr({disabled:"disabled"});
                        },
                        complete:function(){
                            $(".checked").removeAttr('disabled');
                        }

                    }).done(function(data){
                        $.dialog.tips(data.msg);
                        window.location.reload();
                    });
                }
            }
            
END;

        $this->setJs($js);

        $this->displays();
    }

    public function info_data()
    {
        $this->setCondition(0, 'eq', 'account');
        $this->setCondition(1, 'eq', 'role_id');
        $this->setCondition(2, 'eq', 'role_name');
        $this->setCondition(3, 'eq', 'money');
        $this->setCondition(4, 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('giftOrderListData', 'giftOrderListExport');
        $this->call('GiftOrderModel', $method, $this->getConditions());
    }

    public function add()
    {
        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        // $serverid = (new ServerModel())->getLatestServer();

        // $charge = $this->call('ChargeModel', 'recharge', ['output' => 1], $serverid);

        //   $charge = $charge[$serverid];

        // $this->smarty->assign('charge', $charge);
        $this->smarty->display('giftOrder/add.tpl');

    }

    public function add_action()
    {

        $role_id = trim($this->getParam('role_id'));
        $corder_num = trim($this->getParam('corder_num'));
        $server = $this->getParam('server');
        $group = $this->getParam('channel_group');
        $serverid = $server;
        $server = array($server);
        $admin = $_SESSION['username'];
        $index = intval($this->getParam('item'));

        //根据角色名找到其role_id
        $data = $this->call('RoleModel', 'getRoleInfoToType', array('type' => 'role_id', 'value' => $role_id, 'fields' => ['role_id', 'account', 'name', 'role_level', 'package']), $serverid);

        $data = $data[$serverid];

        $role_id = $data['role_id'];
        $role_name = $data['name'];
        $role_level = $data['role_level'];
        $package = $data['package'];
        $account = $data['account'];

        if (empty($role_id)) {

            $json = array('msg' => '找不到该角色3，请核实名称或区服');

        } else {

            $Gift = new GiftOrderModel();

            //查询是否为首赠
            $row = $Gift->getRow('id', ['role_id' => $role_id]);

            $first = 1;

            !empty($row) && $first = 0;

            $charge = $this->call('ChargeModel', 'recharge', ['output' => 1], $serverid);

            $charge = $charge[$serverid];

            $rs = array(
                'group' => $group,
                'server' => $serverid,
                'package' => $package,
                'account' => $account,
                'role_id' => $role_id,
                'role_name' => $role_name,
                'role_level' => $role_level,
                'index' => $index,
                'money' => $charge[$index]['price'],
                'reward_item_id' => $charge[$index]['item_id'],
                'index_name' => $charge[$index]['name'],
                'first' => $first,
                'create_time' => time(),
                'admin' => $admin,
                'order_num' => $this->makeOrder($role_id),
                'corder_num' => $corder_num
            );

            $result = $Gift->add($rs);

            if ($result) {

                $json = array('msg' => '添加成功，请等待审核');

            } else {

                $json = array('msg' => '添加失败，请重试');
            }

        }

        echo json_encode($json);

    }

    public function check()
    {
        $id = $this->getParam('id');

        $Gift = new GiftOrderModel();

        $send_time = time();

        $result = $Gift->getRow(['role_id', 'order_num', 'money', '`index`', 'server'], array('id' => $id));

        if (!$result) {
            $json = array('msg' => '充值失败');
            echo json_encode($json);
            exit;
        }
        $result['create_time'] = $send_time;
        $rs = $Gift->send($result);
        if ($rs === 1) {
            $json = array('msg' => '充值成功');
            $Gift->update(array('checked' => $rs, 'send_time' => $send_time), array('id' => $id));
        } else {
            $json = array('msg' => '充值失败');
        }
        echo json_encode($json);
    }

}