<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/5/6
 * Time: 20:43
 */

class GuideController extends Controller
{
    public function index()
    {
        $header = '说明：<br>';
        $header .= '新增用户数：该日新增且连接上游戏服务器的用户数。<br>';
        $header .= '引导ID：各个引导步骤的ID。<br>';
        $header .= '达到人数：到达过该引导的玩家数（不是指停留在该引导）。<br>';
        $header .= '通过人数：指完成该引导的人数。<br>';
        $header .= '该引导通过率 = 通过人数 / 达到人数 *100% （小数点后两位，四舍五入）<br>';
        $header .= '整体通过率 = 通过人数 /  第一步引导的达到人数 *100% （小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '新增用户数', '引导ID', '达到人数', '通过人数', '该引导通过率', '整体通过率'));
        $this->setSource('Guide', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'eq', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('guide_id', 'ASC');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('GuideModel', $method, $this->getConditions());
    }
}