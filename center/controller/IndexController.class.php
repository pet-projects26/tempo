<?php
!defined('IN_WEB') && exit('Access Denied');

class IndexController extends Controller{

    public function __construct(){
        parent::__construct();
        $this->userAction = new UserAction();
        $this->menuAction = new MenuAction();
        $this->admin_url = 'admin.php';
        //日志记录
        $this->logAction = new LogAction();
    }

    public function index(){
        if(empty($_SESSION['group_channel'])){
            session_unset();//free all session variable
            session_destroy();//销毁一个会话中的全部数据
            setcookie(session_name(),'',time()-3600);//销毁与客户端的卡号
            exit('<script language="javascript">alert("非法账号，请联系管理员");top.location.href="admin.php?ctrl=index&act=login"</script>');
        }
        $menuTree = $this->menuAction->menuTree(MENU_TYPE_CENTER, 0, false);
        global $current_language, $language_arr;
        $this->smarty->assign('current_language', $current_language);
        $this->smarty->assign('language_arr', $language_arr);
        $this->smarty->assign('menuTree', $menuTree);
        $this->smarty->assign('web_name', WEB_NAME);
        //$this->smarty->assign('now_time',date('Y-m-d H:i:s',time()));
        $this->smarty->display('index.tpl');
    }

    public function login(){
        if(empty($_POST['login_token'])){
            //打开登录页面
            $this->userAction->logout();
        }
        else{
            //登录操作
            $username = trim($_POST['username']);
            $password = trim($_POST['password']);
            $result = $this->userAction->doLogin($username, $password, true);
            $this->logAction->logAdd("登录后台， 用户:{$_POST['username']}， 登录结果：{$result}");
            switch ($result) {
                case 'can not login':
                    $msg = '不允许的登录';
                    break;
                case 'user error':
                    $msg = '用户名密码错误';
                    break;
                case 'verification code error':
                    $msg = '验证码错误';
                    break;
                case 'status error':
                    $msg = '账号异常';
                    break;
                case 'expirate':
                    $msg = '账号过期';
                    break;
                case 'fail login many times':
                    $msg = '错误登录次数过多,已被系统列入黑名单';
                    break;
                case 'no server permission':
                    $msg = '没有权限';
                    break;
                case 'success':
                    setcookie('_username', $username, time() + 15552000);
                    header('Location:'.$this->admin_url);
                    exit();
                    break;
                default:
                    $msg = '未知登录错误'.$result;
            }
        }
        $login_token = $this->userAction->makeLoginToken();
        $this->userAction->userSessionSet('verification_code', random(6));
        $this->smarty->assign('login_token', $login_token);
        $this->smarty->assign('msg', $msg);
        $this->smarty->display('login.tpl');
    }

    public function user_status(){
        $user = $_GET['username'] !== '' ? trim($_GET['username']) : $_COOKIE['_username'];
        $user && $hasFailed = $this->userAction->failedLoginAccount($user);
        $ret['verify'] = ($_COOKIE['_username'] != $user || !$hasFailed) ? true : false;
        if ($ret['verify']) { //需要验证码验证
            if($this->userAction->userSessionGet('login_verify'))
                $this->userAction->userSessionDestroy('login_verify');
        } else {//标示为不需要验证码
            $this->userAction->userSessionSet('login_verify', true);
        }
        echo json_encode($ret);
    }

    public function verification_code(){
		ob_clean();
        require_once(ROOT.'/library/ValidationCode.class.php');
        $verifyCode = new verifyCode();
        $verifyCode->font = ROOT.'/style/font/ariblk.ttf';
        $verifyCode->height = 32;
        $verifyCode->createVerify();
        $this->userAction->userSessionSet('verification_code', $verifyCode->getVerify());
        $verifyCode->outputVerify();
    }

    public function exportStr(){
        $tabCss = '<style>table{border-collapse:collapse;border:0;margin:0;}td{ border:#000 solid 0.5pt; padding:5px;}</style>';
        fputexcel($_POST['fileName'], $tabCss.$_POST['str'], 'str');
    }

    public function get_now_time(){
       /*$time = date('Y-m-d H:i:s',time());
        $this->smarty->assign('now_time',$time);
        fastcgi_finish_request();
        do{
            $this->smarty->assign('now_time',$time);
            sleep(1);
        }while(1);*/
        exit(json_encode(array('time'=>date('Y-m-d H:i:s',time()))));
    }
    
     public function check()
    {
        $id = $this->getParam('id');

        $Gift = new GiftOrderModel();

        $send_time = time();

        $result = $Gift->getRow(['role_id', 'order_num', 'money', '`index`', 'server'], array('id' => $id));

        if (!$result) {
            $json = array('msg' => '充值失败');
            echo json_encode($json);
            exit;
        }
        $result['create_time'] = $send_time;
        $rs = $Gift->send($result);
        if ($rs === 1) {
            $json = array('msg' => '充值成功');
            $Gift->update(array('checked' => $rs, 'send_time' => $send_time), array('id' => $id));
        } else {
            $json = array('msg' => '充值失败');
        }
        echo json_encode($json);
    }
}
