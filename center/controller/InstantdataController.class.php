<?php
!defined('IN_WEB') && exit('Access Denied');

class InstantdataController extends Controller{
    public function __construct(){
        parent::__construct();
    }
	public function real(){
		$header  = '说明：<br>';
        $header .= '当前在线：服务器当前在线总玩家数。 <br>';
        $header .= '当前活跃：服务器当天的活跃玩家数。  <br>';
        $header .= '当前充值：服务器当天的充值总流水。 <br>';
        $header .= '充值人数：服务器当天充值的人数。<br>';
        $header .= '付费率 = 充值人数 / 当天活跃玩家人数 *100% <br>';
        $header .= 'ARPPU = 充值金额 / 充值人数  <br>';
       
		$this->setHeader($header);
        $this->setSearch('日期', 'date');
		$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
		$this->setFields(array(
            '服务器', '当前创角', '当前在线' , '当前活跃' , '当前充值' , '充值人数' ,
            '付费率', 'ARPPU' ));
		$this->setLimit();
        $this->setSource('Instantdata' , 'real_data' );
		$this->displays();
	}
	public function real_data(){
        $this->setCondition(0, 'eq', "time");
        $this->setCondition(1, 'scp');
		$method = $this->setDataExport('real_data' , 'real_export');
        $this->call('InstantdataModel' , $method , $this->getConditions());
	}
}