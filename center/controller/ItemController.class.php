<?php
!defined('IN_WEB') && exit('Access Denied');

class ItemController extends Controller{
	
	 public function __construct(){
        parent::__construct();
        $this->py = new PaymentModel();
        
    }

    //产出消耗（菜单）
    public function index(){

        $tabs = array();
        foreach(CDict::$coinType as $k => $row){
            $tabs[] = array('title' => $row['name'] , 'url' => 'admin.php?&ctrl=item&act=record&type=' . $k);
        }
        $this->tabs($tabs);
    }

    //道具和货币的产出消耗（子菜单）
    public function record(){
        $type = $this->getParam('type' , 1);
        !array_key_exists($type , CDict::$coinType) && $type = 1;
        $name = CDict::$coinType[$type]['name'];
        $header  = '说明：<br>';
        $header .= '1. 产生' . $name . ' 指产生' . $name . '的数量<br>';
        $header .= '2. 参与产生数 指参与产生的人数<br>';
        $header .= '3. 消耗' . $name . ' 指消耗' . $name . '的数量<br>';
        $header .= '4. 参与消耗数 指参与消耗的人数<br>';
        $header .= '5. 滞留率  = 1-总消耗/总产出  (保留2位小数)';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(1 , 0 , 1 , 1));
        $this->setField('日期' , 100);
        
        $js = <<< END
        function logup(item_id,source)
        {
            $.post('admin.php?ctrl=item&act=get_use_item', {item_id:item_id,source:source}, function (data) {
                    $.dialog({
				        title: '内容查看',
				        width:500,
				        height:'auto',
				        content:data,
				   	})
                })
        }
        		
       function export_logup(item_id,source)
        {
        	window.location.href = 'admin.php?ctrl=item&act=get_use_item&export=1&item_id='+item_id+'&source='+source;
        }
        function logup_coin(coin,source)
        {
            $.post('admin.php?ctrl=item&act=get_use_coin', {coin:coin,source:source}, function (data) {
                    $.dialog({
				        title: '内容查看',
				        width:500,
				        height:'auto',
				        content:data,
				   	})
                })
        }
        		
       function export_logup_coin(coin,source)
        {
        	window.location.href = 'admin.php?ctrl=item&act=get_use_coin&export=1&coin='+coin+'&source='+source;
        }
     
END;
        
        if ($type == 4) {
        	$this->setFields(array('产生' . $name , '产生道具名称和来源','参与产生数' ,  '消耗' . $name ,'消耗道具名称和去向', '参与消耗数' ,'库存'. $name ,  '总产出', '总消耗' , '滞留率'));
        }elseif ($type == 1 || $type == 3 || $type == 2){
        	$this->setFields(array('产生' . $name ,'参与产生数' ,  '消耗' . $name ,'消耗'.$name.'数和去向', '参与消耗数' ,'库存'. $name ,  '总产出', '总消耗' , '滞留率'));
        }else {
        	$this->setFields(array('产生' . $name , '参与产生数' ,  '消耗' . $name , '参与消耗数' ,'库存'. $name ,  '总产出', '总消耗' , '滞留率'));
        }
        $this->setJs($js);
        $this->setSource('item' , 'record_data' , 1 , array('type' => $type));
        $this->displays();
    }
    

    public function get_use_item(){
    	 
    	$m = new Model();
        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";
    	$server = $m->query($sql);
    	$AllItem= $this->call('GiftModel' , 'getItem' , array() , $server[0]['server_id']);//全部物品
    	$AllItem = $AllItem[$server[0]['server_id']];
    	 
    	$itemType = CDict::$itemType;
    	 
    	$item_id     = $this->getParam("item_id");
    	$source     = $this->getParam("source");
    	$item_id  = rtrim($item_id, ',');
    	$source  = rtrim($source, ',');
    	//导出
    	if ($this->getParam("export") == 1) {
    		if ($item_id) {
    			$item_idArr = explode(",", $item_id);
    			$sourceArr = explode(",", $source);
    			 
    			foreach ($item_idArr as $k=>$v) {
    				$rs[$k]['item'] = $AllItem[$v]?$AllItem[$v]:$v;
    				$rs[$k]['source'] = $itemType[$sourceArr[$k]]?$itemType[$sourceArr[$k]]:$sourceArr[$k];
    			}
    			$result[] = array(
    					'道具名称', '来源/去向'
    			);
    			foreach($rs as $row){
    				$result[] = $m->formatFields($row);
    			}
    			 
    			Util::exportExcel($result , '道具消耗（' . date('Y年m月d日H时i分s秒') . '）');
    		}
    	}else {
    
    		if ($item_id) {
    			$item_idArr = explode(",", $item_id);
    			$sourceArr = explode(",", $source);
    			$str .= '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="dataTable"><table cellpadding="0" cellspacing="0" border="1" style=" width:350px  !important;" class="table_list dataTable" id="DataTables_Table_8"  aria-describedby="DataTables_Table_8_info">';
    			$str .= '<thead><tr><th >道具名称</th><th >来源/去向</th></tr></thead><tbody role="alert" >';
    			foreach ($item_idArr as $k=>$v) {
    				$item_idArr[$k] = $AllItem[$v]?$AllItem[$v]:$v;
    				$sourceArr[$k] = $itemType[$sourceArr[$k]]?$itemType[$sourceArr[$k]]:$sourceArr[$k];
    				$str .= '<tr>';
    				$str .= '<td>'.$item_idArr[$k].'</td>';
    				$str .= '<td>'.$sourceArr[$k].'</td>';
    				$str .= '</tr>';
    			}
    			$str .='</tbody></table></div>';
    		}
    		echo $str;
    	}
    
    }
    
    public function get_use_coin(){

    	$m = new Model();
    	
    	$itemType = CDict::$itemType;
    	
    	$coin     = $this->getParam("coin");
    	$source     = $this->getParam("source");
    	$coin  = rtrim($coin, ',');
    	$source  = rtrim($source, ',');
    	//导出
    	if ($this->getParam("export") == 1) {
    		if ($coin) {
    			$item_idArr = explode(",", $coin);
    			$sourceArr = explode(",", $source);
    			 
    			$Arr = [];
    			foreach ($sourceArr as $key=>$val) {
    				$Arr[$val] += $item_idArr[$key];
    			}
    			arsort($Arr);
    			 
    			foreach ($Arr as $k=>$v) {
    				$rs[$k]['source'] = $itemType[$k]?$itemType[$k]:$k;
    				$rs[$k]['item'] = $v;
    			}
    			$result[] = array(
    					'去向','金钱数量'
    			);
    			foreach($rs as $row){
    				$result[] = $m->formatFields($row);
    			}
    	
    			Util::exportExcel($result , '金钱消耗（' . date('Y年m月d日H时i分s秒') . '）');
    		}
    	}else {
    	
    		if ($coin) {
    			$item_idArr = explode(",", $coin);
    			$sourceArr = explode(",", $source);
    			$Arr = [];
    			foreach ($sourceArr as $key=>$val) {
    				$Arr[$val] += $item_idArr[$key];
    			}
    			arsort($Arr);
    			$str .= '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="dataTable"><table cellpadding="0" cellspacing="0" border="1" style=" width:350px  !important;" class="table_list dataTable" id="DataTables_Table_8"  aria-describedby="DataTables_Table_8_info">';
    			$str .= '<thead><tr><th >去向</th><th >金钱数量</th></tr></thead><tbody role="alert" >';
    			foreach ($Arr as $k=>$v) {
    				$item_idArr1 = $v;
    				$sourceArr1 = $itemType[$k]?$itemType[$k]:$k;
    				$str .= '<tr>';
    				$str .= '<td>'.$sourceArr1.'</td>';
    				$str .= '<td>'.$item_idArr1.'</td>';
    				$str .= '</tr>';
    			}
    			$str .='</tbody></table></div>';
    		}
    		echo $str;
    	}
    	
    }
    
    public function record_data(){
        $this->setCondition(0 , 'date' , 'time');
        $this->setCondition(1 , 'scp','','',array(1,0,1,1));
        $this->setCondition('type' , 'eq' , 'type' , 1);
        $this->setOrder('time');
        $this->setLimit();
        $method = $this->setDataExport('item_data' , 'item_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    //物品流水查询
    public function consume_produce(){
        $header  = '说明：<br>';
        $header .= '1. 若物品被使用，则物品来源一列显示 物品被使用<br>';
        $header .= '2. 查询条件 类型 筛选的是物品是消耗还是产出';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('角色ID');
        $this->setSearch('角色名');
        $this->setSearch('角色等级');
        $this->setSearch('物品ID');
        $this->setSearch('物品个数');
        $this->setSearch('类型');
        $this->setSearch('物品来源');
        $this->setSearch('职业');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField(array('角色ID' , '角色名' , '职业' , '角色等级' , '类型' , '物品来源' , '物品ID' , '物品个数'));
        $this->setFields('创建时间' , 100);
        $this->setSource('item' , 'consume_produce_data' , 1);
        $this->displays();
    }

    public function consume_produce_data(){

    }
	
	
	public function pie_index(){
		$tabs = array(
			 array('title' => '消耗饼图', 'url' => 'admin.php?ctrl=item&act=pie'),
			 array('title' => '商城购买饼图', 'url' => 'admin.php?ctrl=item&act=store_pie'),
		);	
		$this->tabs($tabs);
	}
	
	//消耗饼图
	public function pie(){
		$this->smarty->assign('scp' , $this->getScp(array('showGroup' => 0 , 'showServer' => 1 , 'showChannel' => 0 , 'showPackage' => 0), array(),array(),array(),array(),1));
		$this->smarty->assign('select', 'radio');
		$this->smarty->display('item/pie.tpl');	
	}
	public function pie_data(){
		$start_time=$this->getParam('start_time');
		$end_time=$this->getParam('end_time');
		
		$start_time = !empty($start_time) ? strtotime($start_time) : 0 ;
		$end_time = !empty($end_time) ? strtotime($end_time)+86399 : 0 ;
		
		$type=$this->getParam('type');
		$scp=$this->getParam('scp');
		$scp=json_decode($scp,true);
		$scp=$scp[2][0];//只能选一个服

		empty($scp) && die('服务器不能为空');
		
		$result=$this->call('ItemModel','pie_data',array('start_time'=>$start_time,'end_time'=>$end_time,'type'=>$type,'scp'=>$scp));
		
		if(empty($result)){
			die('没有数据');
		}
		
		$moneyType = CDict::$moneyType;
		$title = $moneyType[$type].'消耗占比';

		$arr=array();
		
		if($result){
			$result = $this->array_sort($result , 'sum_coin','desc');
			foreach($result as $k => $v){
				$arr[]=array($v['coin_source'],intval($v['sum_coin']));
			}	
		}
		
		$detail=$this->call('ItemModel','pie_detail',array('start_time'=>$start_time,'end_time'=>$end_time,'type'=>$type,'scp'=>$scp));
		
		$detail = $this->array_sort($detail , 'sum_coin','desc');
		
		$pie=init_highchart_pie('pie',$title,$arr);
		$this->smarty->assign('pie',$pie);
		$this->smarty->assign('detail',$detail);
		$this->smarty->display('item/pie_data.tpl');	
		
	}
	
	public function array_sort($arr,$keys,$type='asc'){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		
		if($type == 'asc'){
		    asort($keysvalue);
		}else{
		  	arsort($keysvalue);
	 	}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
		  	$new_array[] = $arr[$k];
		}
		return $new_array; 
	}
	
	//商城购买饼图
	public function store_pie(){
		$this->smarty->assign('scp' , $this->getScp(array('showGroup' => 0 , 'showServer' => 1 , 'showChannel' => 0 , 'showPackage' => 0)));
		$this->smarty->display('item/store_pie.tpl');	
	}
	public function store_pie_data(){
		$start_time=$this->getParam('start_time');
		$end_time=$this->getParam('end_time');
		
		$start_time = !empty($start_time) ? strtotime($start_time) : 0 ;
		$end_time = !empty($end_time) ? strtotime($end_time)+86399 : 0 ;
		
		$type=$this->getParam('type');
		$scp=$this->getParam('scp');
		
		$scp=json_decode($scp,true);
		
		$scp=$scp[2];//选择的服

		empty($scp) && die('服务器不能为空');
		
		$time = ' 1';
		if($start_time !=0){
			$time .= " and create_time >= $start_time ";
		}
		if($end_time !=0){
			$time .= " and create_time <= $end_time ";
		}
		//查询商城购买的全部数据
		$sql="select count(DISTINCT(role_id)) as role_num, sum(item_num) as item_num ,item_id from ny_consume_produce where type =1 and source ={$type} and $time GROUP BY item_id order by item_num desc;";
		
		$arr  = array('query' => array('data' => array($sql)));
	 
		$res  = (new ServerconfigModel())->makeHttpParams($scp, $arr);
  		
		$urls = $res['urls'];
		
		$params = $res['params'];
		
		$res = Helper::rolling_curl($urls, $params);
		
		foreach($scp as $k=>$v){
			
			$data = json_decode($res[$v],true);
			
			if(!is_array($data)){
				
				exit(json_encode(array('code' => 400 , 'msg' => json_encode($res))));
			}
		
			$data = $data['data']['query'][0];
			foreach($data as $kk => $vv){
				$all[] =$vv;
			}
		}
		
		$new = array();
		//合并多个服的数据
		foreach($all as $v){
			$item_id = $v['item_id'];
			if(isset($new["$item_id"])){
				$new["$item_id"]['role_num'] +=$v['role_num'];
				$new["$item_id"]['item_num'] +=$v['item_num'];
			}else{
				$new["$item_id"]['role_num'] =$v['role_num'];
				$new["$item_id"]['item_num'] +=$v['item_num'];
				$new["$item_id"]['item_id'] = $v['item_id'];
				
			}
			$total_num  += $v['item_num'];
		}	
		
		$result = $new;	
		
		if(empty($result)){
			die('没有数据');
		}
		
		$title =  '商城购买占比';

		$arr=array();


        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";
		
		$server = $this->py->query($sql);
		
		$AllItem= $this->call('GiftModel' , 'getItem' , array() , $server[0]['server_id']);//全部物品

		$AllItem = $AllItem[$server[0]['server_id']];
		
		

		$sum = $total_num;
		foreach($result as $k=>$v){
			$item_id = $v['item_id'];
			$percent=sprintf('%.4f' , $v['item_num']/$total_num);
			if($percent >= 0.005){
				$arr[$k]['item_id']=$AllItem["$item_id"];
				$arr[$k]['item_num']=$v['item_num'];
				$sum = $sum-$v['item_num'];
			}
			//详情的数据
			$detail[$k]['item_id'] = $AllItem["$item_id"];
			$detail[$k]['item_num'] = $v['item_num'];
			$detail[$k]['role_num'] = $v['role_num'];
			$detail[$k]['percent'] = sprintf('%.1f', $v['item_num'] / $total_num* 100) . '%'; 

		}
		$arr[100000]['item_id']='其他';
		$arr[100000]['item_num']=$sum;
		
		//构造饼图需要的数据
		if($arr){
			$arr = $this->array_sort($arr , 'item_num','desc');
			foreach($arr as $k => $v){
				$array[]=array($v['item_id'],intval($v['item_num']));
			}	
		}
		
		$pie=init_highchart_pie('pie',$title,$array);
		$this->smarty->assign('pie',$pie);
		$this->smarty->assign('detail',$detail);
		$this->smarty->display('item/store_pie_data.tpl');	
		
	}
	
	 
}