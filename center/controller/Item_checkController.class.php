<?php
!defined('IN_WEB') && exit('Access Denied');

class Item_checkController extends Controller{
	private $sm;
    public function __construct(){
        parent::__construct();
        $this->sm = new Item_checkModel();
    }

    //渠道管理（菜单）
    public function index(){
        $tabs = array(
            array('title' => '产出列表', 'url' => 'admin.php?&ctrl=Item_check&act=record'),
            array('title' => '监控规则', 'url' => 'admin.php?&ctrl=Item_check&act=add'),
        );
        $this->tabs($tabs);
    }

    public function record(){
    	$coin_type = ['1'=>'元宝','2'=>'绑元','3'=>'道具','4'=>'内部元宝'];
    	$this->setSearch('日期' , 'range_time');//范围
        $this->setSearch('类型','select',$coin_type);
        $this->setSearch('角色名');
        $this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
        $this->smarty->assign('select','select');
        $this->setFields(array('类型' , '报警数量' , '最大产出' , '来源' , '服务器','角色id','角色名','超出报警值人数','最后扫描时间'));
        $this->setSource('Item_check' , 'record_data');
        $js = <<< END
            function edit(cid){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=Item_check&act=edit&cid=' + cid);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=Item_check&act=edit&cid=' + cid);
            }
        
        function logup(source)
        {
            $.post('admin.php?ctrl=Item_check&act=get_source', {source:source}, function (data) {
                    $.dialog({
				        title: '内容查看',
				        width:500,
				        height:'auto',
				        content:data,
				   	})
                })
        }
END;
        $this->setJs($js);
        $this->displays();
    }
    
    public function get_source(){
    
    	$m = new Model();
    	 
    	$itemType = CDict::$itemType;
    	 
    	$source     = $this->getParam("source");
    	$source  = rtrim($source, ',');
    	if ($source) {
    		$sourceArr = explode(",", $source);
    		$Arr = [];
    		foreach ($sourceArr as $key=>$val) {
    			$Arr[$val] += $item_idArr[$key];
    		}
    		arsort($Arr);
    		$str .= '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="dataTable"><table cellpadding="0" cellspacing="0" border="1" style=" width:350px  !important;" class="table_list dataTable" id="DataTables_Table_8"  aria-describedby="DataTables_Table_8_info">';
    		$str .= '<thead><tr><th >来源</th></tr></thead><tbody role="alert" >';
    		foreach ($Arr as $k=>$v) {
    			$sourceArr1 = $itemType[$k]?$itemType[$k]:$k;
    			$str .= '<tr>';
    			$str .= '<td>'.$sourceArr1.'</td>';
    			$str .= '</tr>';
    		}
    		$str .='</tbody></table></div>';
    	}
    	echo $str;
    	 
    }

    //
    public function record_data(){
    	$this->setCondition(0 , 'time' , 'o.create_time');
        $this->setCondition(1,'eq','coin_type');
        $this->setCondition(2,'eq','role_name');
        $this->setCondition(3 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('record_data' , 'record_export');
    	$this->call('ItemcheckresultModel' , $method , $this->getConditions());
    }

    public function add(){
    	
    	$list = $this->sm->getItemCheck();
    	if ($list) {
    		$groups =  array_filter(explode(',', $list['group']));
    		$servers =  array_filter(explode(',', $list['server']));
    		//服务器，渠道组
    		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
    		
    		$channelGroupRow = array();
    		
    		foreach ($channelGroupRes as $row) {
    			$group_id = $row['id'];
    			$channelGroupRow[$group_id] = $row['name'];
    		}
    		
    		$server = (new ServerModel())->getServerByTrait();
    		
    		
    		//道具
//     		$item = json_decode($list['item'],true); 
//     		$sql="select server_id from ny_server where status  not in ('-1' ,'0') and display = 1 limit 1";
//     		$ser = $this->sm->query($sql);
//     		$AllItem= $this->call('GiftModel' , 'getItem' , array() , $ser[0]['server_id']);//全部物品
//     		$goods = $AllItem[$ser[0]['server_id']];
//     		foreach ($item as $k => &$row) {
//     			$goods_id = $row['id'];
//     			$name = isset($goods[$goods_id]) ? $goods[$goods_id] : '';
//     			$row['id']  = $row['id'] . '/'.$name;
//     		}
//     		unset($row); 
//     		$list['item'] = $item;
    		
    		//元宝
    		$list['gold'] = json_decode($list['gold'],true); 
    		
    		//白名单
    		$list['white'] = json_decode($list['white'],true);
    		
    		$serverData = [];
    		foreach ($server as $key=>$value) {
    			foreach ($value as $k=>$v) {
    				$serverData[$k] = $v;
    			}
    		}
    		$this->smarty->assign('serverData',$serverData);
    		
    		$this->smarty->assign('groups', $channelGroupRow);
    		$this->smarty->assign('servers', $server);
    		$this->smarty->assign('id' , $list['id']);
    		$this->smarty->assign('result' , $list);
    		$this->smarty->assign('data', array('agent' => $groups, 'server' => $servers));
    		
    		$this->smarty->display('item_check/edit.tpl');
    	}else {
    		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
    		$channelGroupRow = array();
    		foreach ($channelGroupRes as $row) {
    			$group_id = $row['id'];
    			$channelGroupRow[$group_id] = $row['name'];
    		}
    		$server = (new ServerModel())->getServerByTrait();
    		$this->smarty->assign('groups', $channelGroupRow);
    		$this->smarty->assign('servers', $server);
    		$serverData = [];
    		foreach ($server as $key=>$value) {
    			foreach ($value as $k=>$v) {
    				$serverData[$k] = $v;
    			}
    		}
    		$this->smarty->assign('serverData',$serverData);
    		 
    		$this->smarty->display('item_check/add.tpl');
    	} 
    	
    }

    public function add_action(){
    	
    	
    	$data = $_REQUEST;
    	$group = $data['channel_group'];
    	$server = $data['server'];
    	
    	if($group[0] =='[]' || $group[0] == ''){
    			
    		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
    		$group = array_column($channelGroupRes, 'id');
    		$group = implode(',', $group);
    	
    	}else{
    		$group = implode(',', $group);
    	}
    	
    	if($server[0] =='[]' || $server[0] == ''){
    		$server = '';
    	}else{
    		$server = implode(',', $server);
    	}
    	
    	
//     	if ($data['item_id'] && count($data['item_id'])) {
//     		foreach($data['item_id'] as $k => $row){
//     			$item_id = explode('/' , $row);
//     			$row = $item_id[0];
//     			if (empty($row)) {
//     				continue;
//     			}
    	
//     			$count = $data['item_count'][$k];
//     			$item[$k]['id'] = $row;
//     			$item[$k]['count'] = $count;
//     		}
//     	}
    	if ($data['server_white'] && count($data['server_white'])) {
    		foreach($data['server_white'] as $k => $row){
    			if (empty($row)) {
    				continue;
    			}
    			$count = $data['role_white'][$k];
    			$white[$k]['server'] = $row;
    			$white[$k]['role'] = $count;
    		}
    	}
    	if ($data['gold'] && count($data['gold'])) {
    		foreach($data['gold'] as $k => $row){
    			if (empty($row)) {
    				continue;
    			}
//     			$count = $data['gold_num'][$k];
    			$gold[$k]['gold'] = $row;
//     			$gold[$k]['gold_num'] = $count;
    		}
    	}
    	
    	$arr['gold'] = json_encode($gold);
//     	$arr['item'] = json_encode($item);
    	$arr['white'] = json_encode($white);
    	$arr['group'] = $group;
    	$arr['mail'] = trim($data['mail']);
    	$arr['server'] = $server;
    	$arr['title'] = trim($data['title']);
    	$arr['update_time'] = time();
    	$arr['content'] = $data['content'];
    	
    	if (!$data['id']) {
    		$arr['time'] = time();
    		if($this->sm->add($arr)){
    			$json = array('msg' => '添加成功' , 'code' => 1);
    		}else{
    			$json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
    		}
    	}else {
    		$where['id'] = $data['id'];
    		if($this->sm->update($arr, $where)){
    			$json = array('msg' => '更新成功' , 'code' => 1);
    		}else{
    			$json = array('msg' => '更新失败，请再次尝试' , 'code' => 0);
    		}
    	}
    	
    	echo json_encode($json);
    }

  
}