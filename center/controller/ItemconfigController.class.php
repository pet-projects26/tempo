<?php
!defined('IN_WEB') && exit('Access Denied');

class ItemconfigController extends Controller
{

    public function __construct(){
        parent::__construct();
        $this->ic = new ItemconfigModel();
    }

    public function index(){

        $tabs = array(
            array('title' => '配置列表', 'url' => 'admin.php?&ctrl=itemconfig&act=record'),
            array('title' => '添加物品', 'url' => 'admin.php?&ctrl=itemconfig&act=add'),
            array('title' => '编辑物品', 'url' => 'admin.php?ctrl=itemconfig&act=edit'),
            array('title' => '上传配置表', 'url' => 'admin.php?ctrl=itemconfig&act=upload'),
        );
        $this->tabs($tabs);
    }

    public function record(){

        $this->setSearch('绑定ID');
        $this->setSearch('不绑定ID');
        $this->setSearch('道具名称');
        $this->setFields(array('ID','绑定ID', '不绑定ID', '道具名称', '道具价值', '描述', '非活动的产出途径', '投放方式建议' , '操作'));
        $this->setSource('itemconfig' , 'record_data' , 1);
        $js = <<< END
        function edit(id){
             \$tabs.tabs('select',2);
             \$tabs.tabs('url' , 2 , 'admin.php?ctrl=itemconfig&act=edit&id=' + id);
             \$tabs.tabs('load' , 2);
         }
         function del(id){
            if(confirm("确定要删除物品配置吗？")){
                $.ajax({
                  url: 'admin.php?ctrl=itemconfig&act=del',
                  type: 'POST',
                  dataType: 'JSON',
                  data: {'id':id}
                }).done(function(data){
                     $.dialog.tips(data.msg);
                    if(data.code == 1){
                        \$tabs.tabs('load' , 0);
                    } 
                })   
             }   
         }
END;
        $this->setJs($js);
        $this->displays();

    }

    public function record_data(){

        $this->setCondition(0 , 'eq' , 'bind_item_id');
        $this->setCondition(1 , 'eq' , 'item_id');
        $this->setCondition(2 , 'like' , 'item_name');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('ItemconfigModel' , $method , $this->getConditions());
    }

    public function add(){
        $this->smarty->display('itemconfig/add.tpl');
    }


    public function add_action(){

        $data = $_POST;

        $rs = $this->ic->add($data);

        if($rs){
            $return = array('msg' => '添加成功' , 'code' => 1);
        }else{
             $return = array('msg' => '添加失败' , 'code' => 0);
        }

        echo json_encode($return);

    }

    public function edit(){
        $id = $this->getParam('id');
        if(empty($id)){
            die('请选择要编辑的数据');
        }
        $rs =$this->ic->getRow('*' ,array('id'=> $id));
       
        $this->smarty->assign('rs' , $rs);
        $this->smarty->display('itemconfig/edit.tpl');
    }

    public function edit_action(){
        $data = $_POST;

        $rs = $this->ic->update($data ,array('id' => $data['id']));

        if($rs){
            $return = array('msg' => '编辑成功' , 'code' => 1);
        }else{
             $return = array('msg' => '编辑失败' , 'code' => 0);
        }

        echo json_encode($return);
    }

    public function del(){
        $id = $this->getParam('id');
        $rs = $this->ic->delete(array('id' => $id));

        if($rs){
            $return = array('msg' => '删除成功' , 'code' => 1);
        }else{
             $return = array('msg' => '删除失败' , 'code' => 0);
        }

        echo json_encode($return);

    }

    /**
     * [upload 上传配置表]
     * @return [type] [description]
     */
    public function upload(){
        $this->smarty->display('itemconfig/upload.tpl');
    }

    /**
     * [uploadCsv description]
     * @return [type] [description]
     */
    public function uploadCsv(){
        
        if ($_FILES["file"]["error"] > 0){  
            die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "错误: " . $_FILES["file"]["error"] . "<br />")));
        }else{ 
            $filename = $_FILES["file"]["name"];
            $suffix = substr($filename, strrpos($filename, '.') + 1);
            $filetype = array('csv' ,'xlsx' , 'xls');
            
            if(!in_array($suffix , $filetype)){

                die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "只允许上传Excel文件")));
            }else if($_FILES['file']['size'] > 20*1024*1024){
                
                die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "文件大小不允许超过20M")));
            }else{
                if(strtolower($suffix) == 'csv'){
                    $handle = fopen($_FILES['file']['tmp_name'],"r");
                    $csv = array();
                    while ($row = fgetcsv($handle)) {
                        foreach($row as $k=>$v){
                            $csv[$k][] = iconv('gbk','utf-8',$v);
                        }
                    }
                }else{
                    $csv = Util::readExcel($_FILES["file"]["tmp_name"]);
                }

                if($csv){
                    unset($csv[1]);
                    $rs = array();

                    //上传表头
                    $info = array(0=>'bind_item_id', 1=>'item_id', 2=>'item_name', 3=>'item_value', 4=>'item_info', 5=>'item_produce', 6=>'item_advice');

                    foreach ($csv as $k => $v) {
                        foreach ($v as $key => $value) {
                            
                           $rs[$k][$info[$key]] =$value ;
                           
                        }
                    }

                    $sql = "TRUNCATE table ny_item_config;";

                    $this->ic->query($sql);

                    $result = $this->multInsert('ny_item_config',$rs);

                    if($result !== false){
                        $return = array('status' => 1 , 'msg' => '添加成功');
                    }else{
                        $return = array('status' => 0 , 'msg' => '添加失败，请检查文件');
                    }

                }else{

                    $return = array('status' => 0 , 'msg' => '请检查文件是否为空');
                }

                echo json_encode($return);

            }
        }
            
    }


    /**
     * 分批次插入
     * @return [type] [description]
     */
    public static function multInsert($table, $data) {
      $tmp = $data;
      $keys = array_keys(array_pop($tmp));

      $len = count($data);

      $length = 1000;

      $times = ceil($len / $length);

      foreach ($keys as &$v) {
        $v = "`{$v}`";
      }
      unset($v);


      $dbh = new Model();

      $insertId = 0;

      //防止单次插入过多，分批次插入
      for ($i = 0; $i < $times; $i++) {
        $sql = "INSERT INTO {$table} (". implode(',', $keys).") values ";

        $offset = ($i * $length);
        $tmp = array_slice($data, $offset, $length);

        $r2 = array();
        foreach ($tmp as $k => $row) {

          foreach ($row as $k2 => &$v2) {
            $v2 = "'{$v2}'";
          }

          $r2[$k] = "(" . implode(',', $row) . ")";
        }

        $sql .= implode(',', $r2);

        try {  
          $dbh->beginTransaction();
          
          $res = $dbh->query($sql);

          $insertId = $dbh->lastInsertId();

          $dbh->commit();
          
        } catch (Exception $e) {
          $dbh->rollBack();
        }

      }
      //end for 
      
      return $insertId;

    }



}

?>