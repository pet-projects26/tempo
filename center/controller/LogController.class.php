<?php
!defined('IN_WEB') && exit('Access Denied');

class LogController extends Controller{
    private $zm;

    public function __construct(){
        parent::__construct();
    }
	//装备强化流水
	public function equip(){
		$this->setHeader('时间默认为今天');
		$this->setDataTableId('equip');
		$this->setSearch('时间' , 'range_time');
		$this->setFields(array('部位','等级','时间' ));
		$this->setSource('log' , 'equip_data',1);
		$this->displays();
		
	}
	public function equip_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(2 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('equip_data' , 'equip_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	//装备替换流水
	public function equip_replace(){
		$this->setHeader('时间默认为今天');
		$this->setDataTableId('equip_replace');
		$this->call('LogModel','getItem',array());
		$this->setSearch('时间' , 'range_time');
		$this->setSearch('装备');
		$this->setFields(array('类型','装备ID','装备','时间' ));
		$this->setSource('log' , 'equip_replace_data',1);
		$this->displays();
	}
	public function equip_replace_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'item_id');
		$this->setCondition(2 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(3 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('equip_replace_data','equip_replace_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	
	
	//道具流水
	public function prop(){
		$this->setHeader('时间默认为今天');
		$this->call('LogModel','getItem',array());
		$this->setSearch('时间' , 'range_time');
		$this->setSearch('类型','select',CDict::$itemType);
		$this->setSearch('道具');
		
		$this->setFields(array('类型' , '道具ID','道具','数量','时间' ));
		$this->setSource('log' , 'prop_data',1);
		$this->displays();
	}
	public function prop_data(){
		
		$this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'eq' , 'source');
		$this->setCondition(2 , 'eq' , 'item_id');
		$this->setCondition(3 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(4 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('prop_data','prop_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}	
	//金钱流水
	public function money(){
		$this->setHeader('时间默认为今天');
		$this->setSearch('时间' , 'range_time');
		$this->setSearch('类型','select',CDict::$itemType);
		$this->setSearch('金钱类型','select',CDict::$money);
		$this->setFields(array('类型' ,'金钱类型','变动的铜钱','变动的元宝','变动的绑元','变动的积分','剩余的铜钱','剩余的元宝','剩余的绑元','剩余的积分', '时间' ));
		$this->setSource('log' , 'money_data',1);
		$this->displays();	
	}
	public function money_data(){
		$this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'eq' , 'coin_source');
		$this->setCondition(2 , 'eq' , 'money_type');
		$this->setCondition(3 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(4 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('money_data','money_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	
	//等级流水
	public function level(){
		$this->setHeader('时间默认为今天');
		$this->setSearch('日期' , 'range_time');
        $this->setSearch('等级');
	    $this->setFields(array('等级' , '时间' ));
		$this->setSource('log' , 'level_data',1);
		$this->displays();
	}
	//等级流水数据来源
	public function level_data(){
		$this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'eq' , 'level');
		$this->setCondition(2 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(3 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('level' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('level_data','level_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	//登录流水
	public function login(){
		$this->setHeader('时间默认为今天');
		$this->setSearch('日期' , 'range_time');
	    $this->setFields(array('等级' , 'IP','登录时间', '登出时间', '在线时长' ));
		$this->setSource('log' , 'login_data',1);
		$this->displays();
	}
	public function login_data(){
		$this->setCondition(0 , 'time' , 'start_time');
		$this->setCondition(1 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(2 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('start_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('login_data','login_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	
	//客户端流水
	public function client(){
		$this->setHeader('时间默认为今天');
		$this->setSearch('日期' , 'range_time');
	    $this->setFields(array('渠道' , '包',  '机型','网络类型','mac','udid','memory','时间'));
		$this->setSource('log' , 'client_data',1);
		$this->displays();
	}
	
	public function client_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'role',$_SESSION['roleid']);
		$this->setCondition(2 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('client_data','client_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	
	
	//任务流水
	public function task(){
		$this->setHeader('时间默认为今天');
		$this->setSearch('日期' , 'range_time');
        $this->setSearch('任务类型','select',array(''=>'-',1=>'主线任务',2=>'支线任务','5'=>'仙盟任务'));
		$this->setSearch('任务ID' );
		$this->setSearch('任务名称' );
	    $this->setFields(array('任务ID' ,'任务类型' , '任务名称','状态' ,'时间'));
		$this->setSource('log' , 'task_data',1);
		$this->displays();
	}
	public function task_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'type');
		$this->setCondition(2 , 'eq' , 'task_id');
		$this->setCondition(3 , 'eq' , 'task_name');
		$this->setCondition(4 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(5 , 'eq' , 'server', $_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('task_data','task_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
	
	//坐骑流水
	public function ride(){
		$this->setHeader('时间默认为今天');
		$this->setSearch('日期' , 'range_time');
        //$this->setSearch('等级');
	    $this->setFields(array('阶级' ,'等级' , '时间' ));
		$this->setSource('log' , 'ride_data',1);
		$this->displays();
	}
	
	public function ride_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(2 , 'eq' , 'server', $_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('ride_data','ride_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}

	/**
	 * [ce 战斗力流水]
	 * @return [type] [description]
	 */
	public function ce(){
		$this->setHeader('时间默认为今天');
		$this->setDataTableId('ce');
		$this->setSearch('时间' , 'range_time');
		$this->setFields(array('战斗力','时间' ));
		$this->setSource('log' , 'ce_data',1);
		$this->displays();
	}
	public function ce_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(2 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('ce_data','ce_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}

	/**
	 * [gem 宝石流水]
	 * @return [type] [description]
	 */
	public function gem(){
		$this->setHeader('时间默认为今天');
		$this->setDataTableId('gem');
		$this->setSearch('时间' , 'range_time');
		$this->setFields(array('部位','宝石','时间'));
		$this->setSource('log' , 'gem_data',1);
		$this->displays();
	}
	public function gem_data(){
		$this->setCondition(0 , 'time' , 'create_time');
		$this->setCondition(1 , 'eq' , 'role_id',$_SESSION['roleid']);
		$this->setCondition(2 , 'eq' , 'server',$_SESSION['server']);
		$this->setOrder('create_time' , 'desc' );
		$this->setLimit();
		$method = $this->setDataExport('gem_data','gem_export');
		$this->call('LogModel' , $method , $this->getConditions());
	}
}