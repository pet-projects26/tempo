<?php
!defined('IN_WEB') && exit('Access Denied');

class LoginController extends Controller
{

    //登录统计（菜单）
    public function record()
    {
        $header = '说明：<br>';
        $header .= '1. 平均登录次数 = 登录次数 / 登录人数 <br>';
        $header .= '2. 老玩家数 指当天有登录的非当天注册的人数<br>';
        $header .= '3. 活跃人数 指当天登录游戏时长总和大于等于30分钟的人数<br>';
        $header .= '4. 忠实人数 指连续三天有登录且三天登录在线时长总和大于等于5小时的人数<br>';
        $header .= '<span style="color:red;">5. 默认是显示最近七天的数据</span><br>';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        $this->setField('日期', 70);
        $this->setFields(array(
            '登录人数', '登录次数', '平均登录次数', '老玩家数', '老玩家平均在线时长', '活跃人数', '忠实人数'
        ));
        $this->setSource('login', 'record_data');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'time', 'time');
        $this->setCondition(1, 'scp', '', '', array(1, 0, 1, 1));
        $this->setOrder('time');
        $this->setLimit();
        $this->call('StatModel', 'login', $this->getConditions());
    }

    //当前在线（菜单）
    public function online()
    {
        $header = '说明：<br>';
        $header .= '只统计最近三天的在线情况 <br>';
        $this->setHeader($header);
        $this->setSearch('角色名');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('角色ID', 100);
        $this->setFields(array('角色名', '最后登录时间', '在线时长', 'ip'));
        $this->setSource('login', 'online_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function online_data()
    {
        $this->setCondition(0, 'like', 'role_name');
        $this->setCondition(1, 'scp');
        $this->setOrder('start_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('online_data', 'online_export');
        $this->call('LoginModel', $method, $this->getConditions());
    }
}