<?php
!defined('IN_WEB') && exit('Access Denied');

class LossController extends Controller{
    //流失统计(菜单)
    public function index(){
        $tabs = array(
            //array('title' => '流失统计' , 'url' => 'admin.php?&ctrl=loss&act=record'),
           // array('title' => '注册留存' , 'url' => 'admin.php?&ctrl=loss&act=reg_seven'),
           // array('title' => '创角留存' , 'url' => 'admin.php?&ctrl=loss&act=role_seven'),
			//array('title' => '帐号留存' , 'url' => 'admin.php?&ctrl=loss&act=account_seven'),
            array('title' => '等级流失' , 'url' => 'admin.php?&ctrl=loss&act=level_loss'),
            array('title' => '任务流失' , 'url' => 'admin.php?&ctrl=loss&act=task_loss'),
            //array('title' => '机型流失' , 'url' => 'admin.php?&ctrl=client&act=model_loss'),
            array('title' => '设备流失' , 'url' => 'admin.php?&ctrl=client&act=device_loss'),
            //array('title' => '节点流失' , 'url' => 'admin.php?ctrl=loss&act=node_loss')
			array('title' => '创角留存' , 'url' => 'admin.php?&ctrl=stat&act=remain&type=1'),
			array('title' => '注册留存' , 'url' => 'admin.php?&ctrl=stat&act=remain_account&type=2'),
			
        );
        $this->tabs($tabs);
    }

    //流失统计(子菜单)
    public function record(){
        $header  = '说明：<br>';
        $header .= '1. 1分，5分，10分，30分，1小时，3小时，5小时 指当天第一次连接游戏在各时间内后离开的玩家数，并且当天24点前没有再次登录游戏的玩家数<br>';
        $header .= '(例如 1分钟指首次登录游戏后,在线时长0-1分之内就离开游戏，且当天没有再次登录的玩家)<br>';
        $header .= '2. 流失人数 指到该日期为止连续3天或3天以上没登录游戏的玩家数<br>';
        $header .= '4. 总注册数 指所有创建账号成功的玩家数<br>';
        $header .= '3. 当天注册数 指当天创建账号的玩家数<br>';
        $header .= '5. 次日流失率 指当天注册第二天没有上线的玩家数 / 前一天注册的玩家数<br>';
        $header .= '6. 标准流失率 指 1 - (老玩家数 / 前一天为止的累计玩家数) <br>';
        $header .= '7. 三日流失率 指注册后连续三天内没有上线的玩家数 / 三天前总注册的玩家数<br>';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array(
            '流失人数' , '当天注册数' , '总注册数' , '5分钟' , '30分钟' , '1小时' , '3天' , '5天' , '7天' , '10天' , '30天' , '次日流失率' , '标准流失率' , '三日流失率'
        ));
        $this->setSource('loss' , 'record_data' , 1);
        $this->displays();
    }
    public function record_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('LossModel' , $method , $this->getConditions());
    }

    //创角留存（子菜单）
    public function role_seven(){
        $header  = '说明：<br>';
        $header .= '1. 表头为第N天的标题指第N天留存（例如2017-03-02的第2天指2017-03-03），数据格式是 人数(百分比)<br>';
        $header .= '2. 第N天留存率 = 该日期后的第N天有登录的角色数 / 第N天创角数';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array('创角数' , '第2天' , '第3天' , '第4天' , '第5天' , '第6天' , '第7天', '第8天' , '第9天' , '第10天' , '第11天' , '第12天' , '第13天' , '第14天' , '第15天' , '第16天' , '第17天' , '第18天' , '第19天' , '第20天' , '第21天' , '第22天' , '第23天' , '第24天' , '第25天' , '第26天' , '第27天' , '第28天' , '第29天' , '第30天' ));
        $this->setSource('loss' , 'role_seven_data' , 1);
        $this->displays();
    }
    public function role_seven_data(){
        $this->setCondition(0 , 'time' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('role_seven_data' , 'role_seven_export');
        $this->call('SevenremainModel' , $method , $this->getConditions());
    }

    //注册留存（子菜单）
    public function reg_seven(){
        $header  = '说明：<br>';
        $header .= '1. 表头为第N天的标题指第N天留存（例如2017-03-02的第2天指2017-03-03），数据格式是 人数(百分比)<br>';
        $header .= '2. 第N天留存率 = 该日期后的第N天有登录的账号数 / 第N天注册数';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array('注册数' , '第2天' , '第3天' , '第4天' , '第5天' , '第6天' , '第7天', '第8天' , '第9天' , '第10天' , '第11天' , '第12天' , '第13天' , '第14天' , '第15天' , '第16天' , '第17天' , '第18天' , '第19天' , '第20天' , '第21天' , '第22天' , '第23天' , '第24天' , '第25天' , '第26天' , '第27天' , '第28天' , '第29天' , '第30天' ));
        $this->setSource('loss' , 'reg_seven_data' , 1);
        $this->displays();
    }
    public function reg_seven_data(){
        $this->setCondition(0 , 'time' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('reg_seven_data' , 'reg_seven_export');
        $this->call('SevenremainModel' , $method , $this->getConditions());
    }

    //等级流失（子菜单）
    public function level_loss(){
        $header  = '说明：<br>';
        $header .= '1. 比例 = 等级人数 / 总人数 <br>';
        $header .= '2. 等级流失人数 指三天前该等级的到现在都没有登录的人数<br>';
        $header .= '3. 等级流失率 = 等级流失人数 / 三天前该等级人数 <br>';
        $header .= '4. 不选择日期则日期默认是今天';
        $this->setHeader($header);
        $this->setSearch('日期' , 'date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('等级' , 100);
        $this->setField('等级人数');
        $this->setField('比例');
        $this->setField('等级流失人数');
        $this->setField('等级流失率');
        $this->setLength(50);
        $this->setSource('loss' , 'level_loss_data' , 1);
        $this->smarty->assign('select','radio');
        $this->displays();
    }
    public function level_loss_data(){
        $this->setCondition(0 , 'eq' , "from_unixtime(create_time,'%Y-%m-%d')");
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time');
        $this->setLimit(50);
        $method = $this->setDataExport('level_loss_data' , 'level_loss_export');
        $this->call('LevelModel' , $method , $this->getConditions());
    }

    //任务流失（子菜单）
    public function task_loss(){
    	$header  = '说明：<br>';
    	$header .= '1. 角色完成率=完成任务人数/角色数<br>';
    	$header .= '2. 完成任务数 指完成交回任务领奖的人数<br>';
    	$header .= '3. 任务通过率=完成任务人数/接受任务人数 <br>';
    	$header .= '4. 任务接受率=接受任务/角色数 <br>';
    	$header .= '5. 流失率=1-任务接受率<br>';
    	$header .= '6. 不选择日期则默认是全部数据';
    	$this->setHeader($header);
    	$this->setSearch('日期' , 'range_time');
    	$this->setSearch('任务类型' , 'select' , CDict::$taskType);
    	$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
    	$this->setField('任务ID' , 120);
    	$this->setFields(array(
    			'任务类型' , '任务名称' ,
    			'角色数','接受任务','完成任务',
    			'角色完成率','任务通过率','任务接受率','流失率'
    	));
    	$this->smarty->assign('select','select');
    	$this->setSource('loss' , 'task_loss_data' , 1);
    	$this->displays();
    }
     
    public function task_loss_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'type');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    
    	$method = $this->setDataExport('task_loss_data' , 'task_loss_export');
    	$this->call('TaskModel' , $method , $this->getConditions());
    }

    //节点流失
    public function node_loss(){
        $header  = '说明：<br>';
        $header .= '1. 新增设备数 指该日期首次点击游戏图标的人数（新增导量数）<br>';
        $header .= '2. 节点流失率 = 1 - 当前节点人数 / 上一节点人数（百分比四舍五入保留1位小数）<br>';
        $header .= '3. 总流失率 = 1 - 当前节点人数 / 新增设备数（百分比四舍五入保留1位小数）<br>';
        $header .= '4. 成功进入注册页面（人数） 以及这之后的所有列的数据格式是（人数 / 节点流失率 / 总流失率）';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        //$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期');
        $this->setField('新增设备数');
        $this->setField('成功进入注册页面（人数）');
        $this->setField('注册完毕进入游戏创角页面（人数）');
        $this->setField('完成创建角色（人数）');
        $this->setField('进入加载页面（人数）');
        $this->setField('加载完成进入装逼副本（人数）');
        $this->setField('清完第一波小怪（人数）');
        $this->setField('打完敌对NPC（人数）');
        $this->setField('打完装逼副本第1波BOSS（人数）');
        $this->setField('打完装逼副本第2波小怪（人数）');
        $this->setField('打完装逼副本BOSS进入故事包装页（人数）');
        $this->setField('故事页播放完进入加载界面（人数）');
        $this->setField('进入新手村（人数）');
        $this->setSource('loss' , 'node_loss_data' , 1);
        $this->displays();
    }
    public function node_loss_data(){
        $this->setCondition(0 , 'time' , 'date');
        //$this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('node_loss_data' , 'node_loss_export');
        $this->call('NodedailyModel' , $method , $this->getConditions());
    }
}