<?php
!defined('IN_WEB') && exit('Access Denied');

class LtvController extends Controller{
	
	
	public function index(){
		$tabs2 = array(
				array('title' => '充值留存' , 'url' => 'admin.php?&ctrl=ltv&act=remain'),
				//array('title' => '创角留存' , 'url' => 'admin.php?&ctrl=stat&act=remain&type=1'),
				//array('title' => '注册留存' , 'url' => 'admin.php?&ctrl=stat&act=remain_account&type=2'),
                array('title' => '多日留存', 'url' => 'admin.php?&ctrl=ltv&act=newacc'),
                array('title' => '等级留存', 'url' => 'admin.php?&ctrl=ltv&act=level')
		);
		$this->tabs($tabs2);
	}
	
    //LTV统计（菜单）
    public function record(){
        $tabs = array(
            array('title' => '创角LTV' , 'url' => 'admin.phpltv?&ctrl=ltv&act=rolecreate'),
            array('title' => '注册LTV' , 'url' => 'admin.php?&ctrl=ltv&act=register')
        );
        $this->tabs($tabs);
    }

    public function rolecreate(){
        $header  = '说明：<br>';
        $header .= '1. 创角数 指当日创建角色数<br>';
        $header .= '2. 表头为N天 指该行日期作为开始日期算的第N天的日期(例如，日期是2016-02-03，1天指2016-02-03，2天是2016-02-04)<br>';
        $header .= '3. 客户周期总价值(创角)(简称LTV，即表头为N天的值的计算方式) = 当天新增创角到N天的累加充值  / 创角数 (四舍五入保留2位小数)';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 1 , 0 , 0));
        $this->setField('日期' , 70);
        $this->setField('创角数' , 80);
        $this->setFields(array('1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天'));
        $this->setSource('ltv' , 'rolecreate_data' , 1);
        $this->displays();
    }
    public function rolecreate_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('rolecreate_data' , 'rolecreate_export');
        $this->call('LtvModel' , $method , $this->getConditions());
    }

    public function register(){
        $header  = '说明：<br>';
        $header .= '1. 注册数 指当日注册帐号人数<br>';
        $header .= '2. 表头为N天 指该行日期作为开始日期算的第N天的日期(例如，日期是2016-02-03，1天指2016-02-03，2天是2016-02-04)<br>';
        $header .= '3. 客户周期总价值（注册）(简称LTV，即表头为N天的值的计算方式) = 当天注册帐号数N天的累加充值总额 / 注册帐号数 (四舍五入保留2位小数)';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 1 , 0 , 0));
        $this->setField('日期' , 70);
        $this->setField('注册数' , 80);
        $this->setFields(array('1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天'));
        $this->setSource('ltv' , 'register_data' , 1);
        $this->displays();
    }
    public function register_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('register_data' , 'register_export');
        $this->call('LtvModel' , $method , $this->getConditions());
    }

    //后续充值（菜单）
    public function follow(){
        $tabs = array(
            array('title' => '后续充值' , 'url' => 'admin.php?&ctrl=ltv&act=follow_all'),
            array('title' => '新增后续充值' , 'url' => 'admin.php?&ctrl=ltv&act=follow_new')
        );
        $this->tabs($tabs);
    }
    public function follow_all(){
        $header  = '说明：<br>';
        $header .= '1. 付费玩家的后续付费金额 指当日有充值的玩家在付费日后续(N)天的充值总额<br>';
        $header .= '2. 付费玩家的后续付费率 指当日有充值的玩家在付费日后续(N)天的付费率<br>';
        $header .= '3. 付费率 = 后续第(N)天的充值总额 / 充值总额 <br>';
        $header .= '4. 表头为天数的列的数据格式是： 付费玩家的后续付费金额(充值玩家的后续付费率)';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 1 , 0 , 0));
        $this->setField('日期' , 70);
        $this->setField('充值人数' , 80);
        $this->setField('充值总额' , 80);
        $this->setFields(array('1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天'));
        $this->setSource('ltv' , 'follow_all_data' , 1);
        $this->displays();
    }
    public function follow_all_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('follow_all_data' , 'follow_all_export');
        $this->call('FollowModel' , $method , $this->getConditions());
    }
    public function follow_new(){
        $header  = '说明：<br>';
        $header .= '1. 新增充值总额 指该日期新建账号的充值总额<br>';
        $header .= '3. 表头为天数的列的数据格式为 新增充值总额(新增充值人数)';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 1 , 0 , 0));
        $this->setField('日期' , 70);
        $this->setField('新增充值人数' , 80);
        $this->setField('新增充值总额' , 80);
        $this->setFields(array('1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天'));
        $this->setSource('ltv' , 'follow_new_data' , 1);
        $this->displays();
    }
    public function follow_new_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('follow_new_data' , 'follow_new_export');
        $this->call('FollownewModel' , $method , $this->getConditions());
    }

    //LTV（菜单）
    public function ltv()
    {
        $header = '说明：<br>';
        $header .= '新增用户数：该日新增且连接上游戏服务器的用户数。<br>';
        $header .= '各天数LTV：该日新增用户从当天累计至N天的充值总额 / 新增用户数 （四舍五入保留至小数点2位数）';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('日期', 100);
        $this->setField('服务器', 80);
        $this->setFields(array('新增用户数', '1天', '2天', '3天', '4天', '5天', '6天', '7天', '10天', '14天', '20天', '30天', '45天', '60天', '90天', '120天'));
        $this->setSource('ltv', 'ltv_data', 1);
        $this->displays();
    }

    public function ltv_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('LtvrecordModel', $method, $this->getConditions());
    }

    //充值留存（菜单）
    public function remain(){
        $header  = '说明：<br>';
        $header .= '新增付费用户数：该日新增且连接上游戏服务器并且有充值行为的用户数。<br>';
        $header .= '各天数留存：该日新增付费用户从当天至N天，每天的登录比例，登录比例 = 该批玩家当天登录的玩家数 / 该批玩家总数（四舍五入保留至小数点2位数）';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期', 100);
        $this->setField('服务器', 80);
        $this->setFields(array('新增付费用户数', '1天', '2天', '3天', '4天', '5天', '6天', '7天', '10天', '14天', '20天', '30天', '45天', '60天', '90天', '120天'));
        $this->setSource('ltv' , 'remain_data' , 1);
        $this->displays();
    }
    public function remain_data(){
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('RechargeremainModel', $method, $this->getConditions());
    }

    public function newacc(){
        $header  = '说明：<br>';
        $header .= '新增用户数：该日新增且连接上游戏服务器的用户数。<br>';
        $header .= '各天数留存：该日新增用户从当天至N天，每天的登录比例，登录比例 = 该批玩家当天登录的玩家数 / 该批玩家总数（四舍五入保留至小数点2位数）';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setField('服务器' , 80);
        $this->setFields(array('新增用户数','1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '10天',  '14天' , '20天', '30天', '45天', '60天', '90天', '120天'));
        $this->setSource('ltv' , 'newacc_data' , 1);
        $this->displays();
    }
    public function newacc_data(){
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('multiday_data' , 'multiday_export');
        $this->call('MultidayModel' , $method , $this->getConditions());
    }

    //等级留存
    public function level(){
        $header  = '说明：<br>';
        $header .= '新增用户数：该日新增且连接上游戏服务器的用户数。<br>';
        $header .= '等级：从1级开始，每1级都统计（达到人数为0的等级不统计）。<br>';
        $header .= '达到人数：≥该等级的玩家数。<br>';
        $header .= '通过人数： ＞该等级的玩家数。<br>';
        $header .= '停留人数: 该等级停留人数 <br>';
        $header .= '本级通过率： 通过人数 / 达到人数 *100%  <br>';
        $header .= '整体达成率： 达到人数 / 新增用户数  *100% <br>';
        $this->setHeader($header);
        $this->setSearch('日期' , 'date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期', 100);
        $this->setFields(array('服务器', '新增用户数', '等级', '达到人数', '通过人数', '停留人数', '本级通过率', '整体达成率'));
        $this->setSource('ltv' , 'level_data', 1);
        $this->displays();
    }

    public function level_data(){
        $this->setCondition(0 , 'eq' , "from_unixtime(create_time,'%Y-%m-%d')");
        $this->setCondition(1 , 'scp');
        $this->setOrder('level', 'asc');
        $this->setLimit();
        $method = $this->setDataExport('level_data' , 'level_export');
        $this->call('LevelremainModel' , $method , $this->getConditions());
    }
}