<?php
!defined('IN_WEB') && exit('Access Denied');

class MailController extends Controller
{
    private $mm;

    public function __construct()
    {
        parent::__construct();
        $this->mm = new MailModel();
        $this->db = new Model();
    }

    //发送邮件（菜单）
    public function manual()
    {
        $tabs = array(
            array('title' => '邮件列表', 'url' => 'admin.php?&ctrl=mail&act=mail_list'),
            array('title' => '发送邮件', 'url' => 'admin.php?&ctrl=mail&act=server_send_mail')
        );
        $this->tabs($tabs);
    }

    //发送邮件（子菜单）
    public function server_send_mail()
    {
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        //资源数组
        $this->smarty->assign('mailSource', CDict::$mailSource);

        $this->smarty->display('mail/manual.tpl');
    }

    //发送邮件（方法）
    public function server_send_mail_action()
    {

        $save = $this->getParam('save');

        $role_type = intval($this->getParam('role_type'));

        $send_type = intval($this->getParam('send_type'));

        $book_time = $this->getParam('book_time');

        //开服计划，允许不填写开服时间，为0
        if ($send_type == 1) {
            if ($book_time == '') {
                $json = array('msg' => '请选择预定发送时间', 'code' => 0);
                exit(json_encode($json));
            }

            if (strtotime($book_time) <= time()) {
                $json = array('msg' => '预定发送时间不能小于当前时间', 'code' => 0);
                exit(json_encode($json));
            }
        }

        if ($role_type == 0) {//多发邮件
            $channel_group = $this->getParam('channel_group');

            $server = $this->getParam('server');


            if ($channel_group == '' || $server == '') {
                die(json_encode(array('state' => false, 'code' => 200, 'msg' => '请勾选渠道组或者服务器')));
            }

            if (!empty($server[0])) {
                //$server = implode(',',$server);
            } else {

                if (empty($channel_group[0])) {//服务器和渠道组都是为空，则默认为全部渠道组下的全部服务器

                    $groupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

                    $serverRes = (new ChannelgroupModel())->getServerByTrait();
                    $server = array();
                    foreach ($serverRes as $k => $v) {

                        $server = array_merge($server, array_keys($v));

                    }
                    $server = array_unique($server);

                    $groups = implode(',', array_column($groupRes, 'id'));


                } else {
                    $channel_group = implode(',', $channel_group);
                    $sql = "select server_id from ny_server where group_id  in ($channel_group) and type !=2";
                    $server = $this->mm->query($sql);
                    $server = array_column($server, 'server_id');
                    $groups = $channel_group;

                }

                //$server = implode(',', $server) ;
            }

        } else {//单发角色

            $server = $this->getParam('server');

            if (empty($server)) {
                die(json_encode(array('state' => false, 'code' => 200, 'msg' => '请勾选服务器')));
            }

            //渠道组
            $sql = "select group_id from ny_server where server_id = '$server[0]' ";

            $group_id = $this->mm->query($sql);
            $groups = $group_id[0]['group_id'];

            //把角色名转换成角色id
            $role_ids = $this->getParam('role');

            $role = $this->getTextWrapContent($role_ids);  //如果role_type == 1 为role_id  2 为账号名

            $role_type_str = $role_type == 1 ? '角色id' : '账号名';

            $role_type_select = $role_type == 1 ? 'role_id' : 'account';

            $role_msg = '';
            $all_role_id = array();
            $all_role_name = [];
            foreach ($role as $key => $value) {
                if ($role_type == 1) $value = intval($value);

                //去单服获取role_id;
                //根据玩家id找到role_id
                $data = $this->call('RoleModel', 'getRoleInfoToType', array('type' => $role_type_select, 'value' => $value, 'fields' => ['role_id', 'name']), $server[0]);
                $role_id = $data[$server[0]]['role_id'];
                $role_name = $data[$server[0]]['name'];

                //根据获取的role_id获取role_name
                if (empty($role_id)) {

                    $role_msg .= $role_type_str . ':' . $value . " :角色信息查找不到<br/>";

                } else {
                    $all_role_id[] = $role_id;
                    $all_role_name[] = $role_name;
                }
            }

            if (!empty($role_msg)) {
                die(json_encode(array('state' => false, 'code' => 200, 'msg' => $role_msg)));
            }

        }

        if ($save) {

            $item_id = $this->getParam('item_id');

            $item_count = [];
            $allItem = [];
            if (!empty($item_id)) {

                $item_count = $this->getParam('item_count');

                $allItem = $this->getGoodsItem(0);

                if (empty($item_id) || !is_array($item_count) || !is_array($item_id) || empty($item_count) || count($item_id) != count($item_count)) {
                    $msg = '物品信息有误; 请重试';
                    die(json_encode(array('state' => false, 'code' => 200, 'msg' => $msg)));
                }

                foreach ($item_id as $k => $v) {
                    if (!in_array($v, $allItem)) {
                        $msg = '物品信息' . $v . '不是游戏物品; 请重试';
                        die(json_encode(array('state' => false, 'code' => 200, 'msg' => $msg)));
                    }

                    $v_count = (int)$item_count[$k];

                    if ($v_count <= 0) {
                        $msg = '物品' . $v . '数量不正常; 请重试';
                        die(json_encode(array('state' => false, 'code' => 200, 'msg' => $msg)));
                    }

                    $res = explode('/', $v);
                    $goods_id = isset($res[0]) ? $res[0] : $v;
                    $item[$k] = (int)trim($goods_id);
                }
            }

            $data = array(
                'groups' => $groups,
                'server' => $server,
                'role_id' => $role_type ? implode(',', $all_role_id) : 0,
                'role_name' => $role_type ? implode(',', $all_role_name) : 0,
                'sender' => $this->getParam('sender'),
                'title' => $this->getParam('title'),
                'content' => $this->getParam('content'),
                'item_id' => $item,
                'item_count' => $item_count,
                'source' => $this->getParam('source'),
                'send_type' => $send_type,
                'book_time' => strtotime($book_time)
            );

            (new MailModel())->manual_mail($data);
        }
    }

    /**
     * 邮件列表
     *
     */
    public function mail_list()
    {
        $this->setSearch('日期', 'range_date');
        $this->setSearch('角色名');
        // $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('ID');
        $this->setField('标题');
        $this->setField('渠道组');
        $this->setField('服');
        $this->setField('角色名称');
        $this->setField('货币');
        $this->setField('物品');
        $this->setField('邮件详情');
        $this->setField('状态');
        $this->setField('发送');
        $this->setField('发送结果');
        $this->setField('申请者');
        $this->setField('审批人');
        $this->setField('添加时间');
        $this->setField('是否定时');
        $this->setField('定时发送时间');

        $this->setSource('mail', 'mail_list_data', 1);
        $js = <<< END
            //物品详情
			function check(id){
                $.ajax({
                    url: 'admin.php?ctrl=mail&act=item_action',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json'
                }).done(function(data){
					
                    $.dialog({
                        title: data.title,
                        max: false,
                        min: false,
                        content: data.html
                    });
                });
            }
            //邮件详情
            function info(id){
                $.ajax({
                    url: 'admin.php?ctrl=mail&act=info_action',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json'
                }).done(function(data){
                    
                    $.dialog({
                        title: data.title,
                        max: false,
                        min: false,
                        width : 800,
                        content: data.html
                    });
                });
            }




		
            function editMail(type, id, obj) {
                if (type == 1) {
                    var url = 'admin.php?ctrl=mail&act=check';
                    var msg = '<button class="btn btn-success">已审核</button>';
                }else if (type == 3) {
                    var url = 'admin.php?ctrl=mail&act=refuse';
                    var msg = '<button class="btn btn-danger">拒绝</button>';
                } else if (type == 4) {
                    var url = 'admin.php?ctrl=mail&act=delete'
                } 
                else {
                    var url = 'admin.php?ctrl=mail&act=send';
                    var msg = '已发送';
                }

                var mark = $(obj).attr('mark');

                $(obj).attr('mark', 1);

                if (mark == 1) {
                    alert('请勿重复点击');
                    return false;
                } 

                $.post(
                    url,
                    {
                        id : id
                    },
                    function(data) {
                        data = eval('(' + data + ')');
                        code = data['code'];
                        if (code == 200) {

                            if(type ==1){
                                $(obj).parent().html(msg);
                            }
                            if(type ==3){
                              $(obj).parent().next("td").html('不允许发送');  

                            }
                            

                            if(type ==2){
                                $(obj).parent().next("td").html(data.msg);
                                $(obj).parent().html(msg);
                            }

                           
                            $.dialog.tips(data.msg);
                            \$dataTable.fnMultiFilter(getFilter());
                            

                        } else {
                            alert(data['msg']);
                        }
                        $(obj).removeAttr('mark');
                    }
                );
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    /**
     * 邮件列表数据
     *
     */
    public function mail_list_data()
    {
        $this->setCondition(0, 'date', 'create_time');
        $this->setCondition(1, 'eq', 'role_name');
        $this->setCondition(2, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('mail_list_data', 'mail_list_export');
        $this->call('MailModel', $method, $this->getConditions());
//         $this->call('MailModel', 'mail_list_data' , $this->getConditions());
    }

    /**
     * 邮件审核
     */
    public function check()
    {
        $id = $this->getParam('id');
        if (empty($id)) {
            die (json_encode(array('code' => 404, 'msg' => '非法参数')));
        }

        $msg = $this->mm->mail_check($id);

        if ($msg == 'ok') {
            die(json_encode(array('code' => 200, 'msg' => '审核成功')));
        } else {
            die(json_encode(array('code' => 500, 'msg' => $msg)));
        }
    }

    /**
     * 拒绝邮件
     */
    public function refuse()
    {

        $id = $this->getParam('id');
        if (empty($id)) {
            die (json_encode(array('code' => 404, 'msg' => '非法参数')));
        }

        $msg = $this->mm->mail_refuse($id);

        if ($msg == 'ok') {
            die(json_encode(array('code' => 200, 'msg' => '审核成功')));
        } else {
            die(json_encode(array('code' => 500, 'msg' => $msg)));
        }

    }

    //删除邮件
    public function delete()
    {

        $id = $this->getParam('id');
        if (empty($id)) {
            die (json_encode(array('code' => 404, 'msg' => '非法参数')));
        }

        $msg = $this->mm->mail_delete($id);

        if ($msg == 'ok') {
            die(json_encode(array('code' => 200, 'msg' => '删除成功')));
        } else {
            die(json_encode(array('code' => 500, 'msg' => $msg)));
        }
    }


    /**
     * 邮件发送
     */
    public function send()
    {
        $id = $this->getParam('id');
        if (empty($id)) {
            die (json_encode(array('code' => 404, 'msg' => '非法参数')));
        }

        $sql = "SELECT * FROM ny_mail WHERE id = {$id} LIMIT 1";

        $res = $this->db->query($sql);

        $server = explode(',', $res[0]['servers']);

        $mail = $res[0];

        $status = $mail['status'];

        if ($status == 0) {
            die (json_encode(array('code' => 404, 'msg' => '请先审核邮件')));
        }
        if ($status == 2) {
            die (json_encode(array('code' => 404, 'msg' => '不允许发送')));
        }

        if ($mail['send_type'] == 1) {
            die (json_encode(array('code' => 404, 'msg' => '该邮件是定时发送邮件')));
        }

        $attach = json_decode($res[0]['attach'], true);

        //@todo 

        $mail['role_id'] = $mail['role_id'] = 0 ? array(0) : explode(',', $mail['role_id']);//把角色id转成数组

        foreach ($mail['role_id'] as &$val) {
            $val = intval($val);
        }

        $msg = $this->mm->send_mail($server, $mail, $attach);

        $sql = "UPDATE ny_mail SET is_send = 1 , msg = '$msg', send_time = " . time() . " WHERE id = {$id} ";

        $this->db->query($sql);

        die(json_encode(array('code' => 200, 'msg' => $msg)));

    }

    public function server_mail_get_item_name()
    {
        $item_id = $this->getParam('itemId');

        //读取物品数据
        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = $this->mm->query($sql);

        $allitem = $this->call('GiftModel', 'getItem', array(), $server[0]['server_id']);//全部物品

        $allitem = $allitem[$server[0]['server_id']];

        if (!empty($allitem[$item_id])) {
            exit(json_encode(['error' => 0, 'itemName' => $allitem[$item_id]['name']]));
        }

        exit(json_encode(['error' => 1, 'msg' => '物品ID' . $item_id . '不存在,请重试!']));
    }

    //获取物品（方法）
    public function server_mail_get_item()
    {
        $item_id = $this->getParam('item');
        $item = file_get_contents(DATA_DIR . 'item_conf.json');
        $item = json_decode($item, true); //获取物品属性表
        $item = $item['root']['itemId'];
        $json = array();
        if (array_key_exists($item_id, $item)) {
            $json['name'] = $item[$item_id]['name'];
            isset($item[$item_id]['overlap']) && $json['overlap'] = $item[$item_id]['overlap'];
        } else {
            $json['name'] = '物品不存在';
        }
        echo json_encode($json);
    }

    public function item_action()
    {

        $id = $this->getParam('id');

        $item = $this->mm->getRow(array('attach'), array('id' => $id));
        $item = $item['attach'];
        $item = json_decode($item, true);
        $item = $item['item'];

        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = $this->mm->query($sql);

        $allitem = $this->call('GiftModel', 'getItem', array(), $server[0]['server_id']);//全部物品

        $allitem = $allitem[$server[0]['server_id']];

        $html = '';
        foreach ($item as $row) {

            $html .= '<span style="color:#228b22;">' . $allitem[trim($row[0])]['name'] . '</span> <span style="color:#ff0000;">' . $row[1] . '个</span><br>';
        }
        echo json_encode(array('title' => '邮件包含的物品', 'html' => $html));
    }

    public function info_action()
    {

        $id = $this->getParam('id');

        $info = $this->mm->getRow(array('title', 'sender', 'content'), array('id' => $id));

        $html .= '标题：' . $info['title'] . '<br><br>';
        $html .= '发件人：' . $info['sender'] . '<br><br>';
        $html .= '内容：<span style = "line-height:24px">' . $info['content'] . '</span><br>';

        echo json_encode(array('title' => '邮件详情', 'html' => $html));
    }

    public function getGoodsItem($output = 1)
    {

        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";

        $server = (new ServerModel())->query($sql);

        $goods = $this->call('GiftModel', 'getItem', array(), $server[0]['server_id']);//全部物品

        $goods = $goods[$server[0]['server_id']];

        $item = array();

        if (!empty($goods)) {
            foreach ($goods as $k => $v) {
                $item[] = $k . '/' . $v['name'];
            }
        }
        //print_r($AllItem);

        if ($output == 1) {
            die(json_encode($item));
        }

        return $item;

    }
}