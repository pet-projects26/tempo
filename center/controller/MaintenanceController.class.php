<?php
!defined('IN_WEB') && exit('Access Denied');

class MaintenanceController extends Controller{
    private $zm;

    public function __construct(){
        parent::__construct();
        $this->cgm = new ChannelgroupModel();
        $this->mm = new MaintenanceModel();
        $this->sm = new ServerModel();
    }

    
    public function index(){
        $tabs = array(
            array('title' => '维护列表', 'url' => 'admin.php?&ctrl=maintenance&act=record'),
            array('title' => '添加维护', 'url' => 'admin.php?&ctrl=maintenance&act=add'),
            array('title' => '编辑维护', 'url' => 'admin.php?&ctrl=maintenance&act=edit'),
        );
        $this->tabs($tabs);
    }

    public function record(){
        $groups = $this->cgm->getRows(array('id' , 'name'));

        $group = array();
        $group[''] = '全部';
        foreach($groups as $k=>$v){
            $group[$v['id']] = $v['name']; 
        }

        $this->setSearch('渠道组' , 'select' , $group);
        $this->setSearch('维护开始时间' , 'range_time');
        $this->setSearch('创建时间' , 'range_time');
        $this->setSearch('版本号');
        $this->setSearch('状态' , 'select' , array(''=>'全部','0'=>'未配置','1'=>'已配置' ,'2'=>'维护中' , '3' => '维护完成'));

        $this->setFields(array('编号' , '渠号' , '服务器' , '版本号' , '状态' , '维护开始时间' , '维护结束时间','创建时间'));
        $this->setField('操作' , '300');
        $this->setLength(100);
        $this->setSource('maintenance' , 'record_data');
        $js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=maintenance&act=edit&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=maintenance&act=edit&id=' + id);
            }
            function del(id){
                if(confirm("确定删除服务器吗？")){
                    $.ajax({
                        url: 'admin.php?ctrl=maintenance&act=del_action&id=' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: $(this).serialize()
                        }).done(function(data){
                            if(data ==1){
                                $.dialog.tips('删除成功');
                                \$tabs.tabs('load' , 0);
                            }else{
                                $.dialog.tips('删除失败，请重试');
                            }   
                    })
                }
            }

            function config(id){
                if(confirm("确定配置吗？")){

                    $.post('admin.php?ctrl=maintenance&act=config&id='+id,'' , function(data){
                        if(data ==1){
                            $.dialog.tips('配置成功');
                            \$tabs.tabs('load' , 0);
                        }else{
                            $.dialog.tips('配置失败，请重试');
                        }
                        
                    },'json');
                }
            }
           
END;
        $this->setJs($js);
        $this->displays();

    }

    public function record_data(){
        $this->setCondition(0 , 'eq' , 'group_num');
        $this->setCondition(1 , 'time' , 'starttime');
        $this->setCondition(2 , 'time' , 'createtime');
        $this->setCondition(3 , 'eq' , 'version');
        $this->setCondition(4, 'eq' , 'type');
        $this->setOrder('createtime');
        $this->setLimit(100);
        $this->mm->record_data($this->getConditions());
    }

    public function del_action(){
        $id = $this->getParam('id');
        if($id){
            $sql = "delete  from ny_server_maintenance where id = $id";

            $rs = $this->mm->query($sql);
            if($rs !== false){
            	(new LogAction())->logAdd("删除维护计划：{$id}");
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    public function add(){
        $servers = $this->sm->getServerByTrait();
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach($channelGroups as $key=>$val){
            $groups[$val['id']] = $val['name'];
        }
        $this->smarty->assign('servers',$servers);
        $this->smarty->assign('groups',$groups); 

        $this->smarty->display('maintenance/add.tpl');
    }

    public function add_action(){
       
        $group = $this->getParam('channel_group');
        $server = $this->getParam('server');
        $version = $this->getParam('version');
        $starttime = $this->getParam('starttime');
        $endtime = $this->getParam('endtime');

        if(empty($version)){
            die(json_encode( array('ret' =>'0','msg'=>'版本号不能为空'))) ;
        }

        if(empty($starttime)){
            die(json_encode(array('ret' =>'0','msg'=>'维护开始时间不能为空'))) ;
        }

        if(empty($endtime)){
            die(json_encode(array('ret' =>'0','msg'=>'维护结束时间不能为空')) );
        }

        if(strtotime($starttime) >= strtotime($endtime)){
            die(json_encode(array('ret' =>'0','msg'=>'维护结束时间不能小于维护开始时间')));
        }

        $data = array(
            'group_num' => !empty($group[0]) ? implode(',',$group) : '',
            'server_id'  => !empty($server[0]) ? implode(',',$server) : '',
            'version' => $version,
            'starttime' => strtotime($starttime),
            'endtime' => strtotime($endtime),
            'createtime' => time(),
        );

        $rs = $this->mm->add($data);

        if($rs){
        	(new LogAction())->logAdd("添加维护计划：{$rs}");
            //die('添加');exit;
            die(json_encode(array('ret' =>'1','msg'=>'添加成功')));
           
        }else{

            die(json_encode(array('ret' =>'0','msg'=>'添加失败')));
        }

    }

    public function check_version(){

        $group = $this->getParam('channel_group');
        $version = $this->getParam('version');
        if(empty($group[0])){
            $sql = "select name,version from ny_channel_group";
        }else{
            $group = implode("','",$group);
            $sql = "select name,version from ny_channel_group where id in ('$group')";
        }
        $rs = $this->mm->query($sql);
      
        $html='';
        foreach($rs as $k=> $v){
            $html .='<div style="width:200px;">'.$v['name'].':<span style="float:right">'.$v['version'].'</span></div>';
        }
       
        echo $html;
    }

    function edit(){

        $id = $this->getParam('id');

        if (empty($id)) {

            die('请选择要编辑的维护计划');
        }

        $data = $this->mm->getRow('*', array('id' => $id));
        if (empty($data)) {
           
            die('查询不到数据');
        }

        if(empty($data['group_num'])){
             $channelGroupId ='';
        }else{
            $channelGroupId = explode(',', $data['group_num']);
        }
        
        if(empty($data['server_id'])){
            $server_id = '';
        }else{
            $server_id = $data['server_id'];
            $server_id = explode(',', $server_id);
        }

        $servers = $this->sm->getServerByTrait();
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach($channelGroups as $key=>$val){
            $groups[$val['id']] = $val['name'];
        }
        $this->smarty->assign('servers',$servers);
        $this->smarty->assign('groups',$groups); 
        $this->smarty->assign('data',array('agent'=>$channelGroupId,'server'=>$server_id));
        $this->smarty->assign('version',$data['version']); 
        $this->smarty->assign('starttime',date('Y-m-d H:i',$data['starttime']));
        $this->smarty->assign('endtime',date('Y-m-d H:i',$data['endtime']));
        $this->smarty->assign('version',$data['version']);
        $this->smarty->assign('id',$data['id']);
        $this->smarty->display('maintenance/edit.tpl');

    }


    public function edit_action(){
       
        $group = $this->getParam('channel_group');
        $server = $this->getParam('server');
        $version = $this->getParam('version');
        $starttime = $this->getParam('starttime');
        $endtime = $this->getParam('endtime');
        $id = $this->getParam('id');


        if(empty($version)){
            die(json_encode( array('ret' =>'0','msg'=>'版本号不能为空'))) ;
        }

        if(empty($starttime)){
            die(json_encode(array('ret' =>'0','msg'=>'维护开始时间不能为空'))) ;
        }

        if(empty($endtime)){
            die(json_encode(array('ret' =>'0','msg'=>'维护结束时间不能为空')) );
        }

        if(strtotime($starttime) >= strtotime($endtime)){
            die(json_encode(array('ret' =>'0','msg'=>'维护结束时间不能小于维护开始时间')));
        }

        $data = array(
            'group_num' => !empty($group[0]) ? implode(',',$group) : '',
            'server_id'  => !empty($server[0]) ? implode(',',$server) : '',
            'version' => $version,
            'starttime' => strtotime($starttime),
            'endtime' => strtotime($endtime),
            'createtime' => time(),
        );

        $rs = $this->mm->update($data , array('id'=>$id));
       
        if($rs !== false){
        	(new LogAction())->logAdd("编辑维护计划：{$id}");
            die(json_encode(array('ret' =>'1','msg'=>'编辑成功')));
           
        }else{

            die(json_encode(array('ret' =>'0','msg'=>'编辑失败')));
        }

    }

    public function config(){
        $id = $this->getParam('id');

        if(empty($id)){
            echo '配置错误';
        }

        $rs = $this->mm->getRow('*' , array('id' => $id));

        $group_id = $rs['group_num'];
        if(empty($group_id) && empty($rs['server_id']) ){

            $group_name = "全部渠道组";

        }elseif (!empty($group_id) && empty($rs['server_id'])) {

            $sql = "select name from ny_channel_group where id in ($group_id)";
            $group = $this->mm->query($sql);
            $group_name  = implode(',' , array_column($group, 'name'));
        }elseif(!empty($rs['server_id'])){

            $server = explode(',',$rs['server_id']);
            $server = implode("','" , $server);
            $sql = "select g.name from ny_channel_group g left join ny_server s on g.id = s.group_id where s.server_id in ('$server') ";
            
            $group = $this->mm->query($sql);
            $group_name  = implode(',' , array_column($group, 'name'));
        }

//         $webhook = "https://oapi.dingtalk.com/robot/send?access_token=49e5417f91c3ce1e6d563e674babe0705bc414ea99e9acbcb96bc95127542843";
//         $message="类型：维护，渠道：".$group_name."，维护开始时间：".date('Y-m-d H:i' , $rs['starttime'])."维护结束时间：".date('Y-m-d H:i' , $rs['endtime'])."版本号：".$rs['version'];
        
//         $data = array ('msgtype' => 'text','text' => array ('content' => $message));
//         $data_string = json_encode($data);

//         $return = Helper::httpRequest($webhook ,$data_string,'post',5,5,true);
//         $return = json_decode($return , true); 
        
        if($this->mm->update(array('type' => 1) , array('id'=>$id))){
            echo 1;
        }else{
            echo 0;
        }
    }

}