<?php
!defined('IN_WEB') && exit('Access Denied');

class MallController extends Controller{

    protected $mallType;

    protected $defalutMallType;

    protected $defalutChildMallType;

    public function __construct()
    {
        parent::__construct();

        $server = (new ServerModel())->getLatestServer();
        //单服获取商城配置 包含分类
        $mallType = $this->call('MallRecordModel', 'getMallType', ['output' => 1], $server);

        $this->mallType = $mallType[$server];

        if (empty($this->mallType)) {
            echo 'mallType is empty ~';
            return false;
        }

        //获取第一个大类type和第一个小类type
        $defalutMallType = reset($this->mallType);

        $this->defalutMallType = $defalutMallType['mallType'];

        $defalutChildMallType = reset($defalutMallType['child']);

        $this->defalutChildMallType = $defalutChildMallType['mallType'];
    }

    public function index()
    {
        $tabs = array();

        foreach ($this->mallType as $key => $val) {
            array_push($tabs, ['title' => $val['mallName'], 'url' => 'admin.php?&ctrl=mall&act=child&mall_type='.$val['mallType']]);
        }

        $this->tabs($tabs);
    }

    public function child()
    {
        $mallType = $this->getParam('mall_type');

        $tabs = array();

        foreach ($this->mallType[$mallType]['child'] as $key => $val) {
            array_push($tabs, ['title' => $val['mallName'], 'url' => 'admin.php?&ctrl=mall&act=record&mall_type='.$mallType.'&child_mall_type='.$val['mallType']]);
        }

        $this->tabs($tabs, 'mall');
    }

    //每小时统计数据
    public function record()
    {
        $header = '说明：<br>';
        $header .= '商城货物名称：具体货物待统计。<br>';
        $header .= '消费金额：该服玩家当天在该货物上消费元宝数之和。<br>';
        $header .= '消费占比 ：消费金额 /商城日消费 *100% （小数点后两位，四舍五入）    商城日消费：当日该服全部玩家花费在商城上的总元宝数量。<br>';
        $header .= '消费人数：该服当天在商城该货物上进行过消费的玩家人数。<br>';
        $header .= '人数占比： 消费人数/ 商城总消费人数 *100% （小数点后两位，四舍五入）  商城总消费人数： 当日该服在商城进行过消费的玩家人数。<br>';

        $this->setHeader($header);

        $mall_type = $this->getParam('mall_type');
        $child_mall_type = $this->getParam('child_mall_type');

        $this->setSearch('日期' , 'date');
        $this->setSearch('服务器' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array('服务器', '商城货物名称', '消费金额', '消费占比', '消费人数', '人数占比', '货币'));
        $this->setSource('mall', 'record_data', 1, ['mall_type' => $mall_type, 'child_mall_type' => $child_mall_type]);
        $this->setLimit();
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function record_Data()
    {
        $this->setCondition(0 , 'eq' , "create_time");
        $this->setCondition(1 , 'scp');
        $this->setCondition(2, 'eq', 'mall_type', $this->getParam('mall_type', intval($this->defalutMallType)));
        $this->setCondition(3, 'eq', 'child_mall_type', $this->getParam('child_mall_type', $this->defalutChildMallType));

        $this->setOrder('product_id' , 'asc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('MallModel', $method, $this->getConditions());
    }
}