<?php
!defined('IN_WEB') && exit('Access Denied');

class MenuController extends Controller{
    private $ma;
    private $la;
    private $type;
    
    public function __construct(){
        parent::__construct();
        $this->ma = new MenuAction();
        $this->la = new LogAction();
        $this->type = trim($_REQUEST['type']);
        $this->smarty->assign('type' , $this->type);
    }
    
    public function main(){
        $this->smarty->display('menu/main.tpl');
    }
    
    public function index(){
        $this->smarty->display('menu/index.tpl');
    }

    public function items(){
        $menuTree = $this->ma->menuTree($this->type);
        $this->smarty->assign('menuTree' , $menuTree);
        $this->smarty->display('menu/items.tpl');
    }

    public function edit(){
        $id = intval($this->getParam('id'));
        $id && $menu = $this->ma->menuGetOne($id);
        $menuRoot = $this->ma->menuRoot($this->type);

        $this->smarty->assign('menu' , $menu);
        $this->smarty->assign('menuRoot' , $menuRoot);
        $this->smarty->display('menu/edit.tpl');
    }

    public function edit_submit(){
        $id = intval($this->getParam('id'));
        $data = array(
            'name' => trim($this->getParam('name')),
            'url' => trim($this->getParam('url')),
            'parent' => intval($this->getParam('parent')),
            'weight' => intval($this->getParam('weight')),
            'status' => 1
        );
        if($id){
            $data['id'] = $id;
            $result = $this->ma->menuUpdate($data);
            $result && $result = $id;
            $this->la->logAdd("编辑菜单:" . $this->getParam('name'));
        }
        else{
            $data['type'] = $this->type;
            $result = $this->ma->menuAdd($data);
            $this->la->logAdd("添加菜单:".$this->getParam('name'));
        }
        echo $result;
    }

    public function delete(){
        $id = intval($this->getParam('id'));
        $result = $this->ma->menuDelete($id);
        echo $result? 'success': 'fail';
        $this->la->logAdd('删除菜单，菜单id:' . $this->getParam('id'));
    }

    public function save_weight(){
        $menu_root_ids = $this->getParam('menu_root_ids');
        $menu_sub_ids = $this->getParam('menu_sub_ids');

        $result = $this->ma->save_weight($menu_root_ids , $menu_sub_ids);
        echo $result? 1: 0;
        $this->la->logAdd("保存菜单排序");
    }

    public function save_field(){
        $id = intval($this->getParam('id'));
        $field = trim($this->getParam('field'));
        $value = htmlspecialchars_decode(trim($this->getParam('value')));
        if($id <= 0){
            echo 'save fail';
            return;
        }
        $data = array('id' => $id , $field => $value);
        $this->ma->menuUpdate($data);
        echo $value;
        $this->la->logAdd('更新菜单字段:' . $field . '=>' . $value . '，菜单id:' . $id);
    }
}
