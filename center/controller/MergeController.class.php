<?php
!defined('IN_WEB') && exit('Access Denied');

class MergeController extends Controller{
    
    public function __construct(){
        parent::__construct();
        $this->m =  new MergeModel();
    }
    public function index(){
        $tabs = array(
            array('title' => '合服列表', 'url' => 'admin.php?&ctrl=merge&act=merge_list'),
            array('title' => '添加合服', 'url' => 'admin.php?&ctrl=merge&act=add_merge'),
            array('title' => '编辑合服', 'url' => 'admin.php?&ctrl=merge&act=edit_merge'),
           
        );
        $this->tabs($tabs);
    }
    /**
     * [merge_list 合服列表]
     */
    public function merge_list(){
        $this->setFields(array('ID','渠道组','母服','子服','合服时间','开服时间','状态','通知','创建时间' , '操作'));
        $this->setSource('merge' , 'merge_list_data');
        $js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=merge&act=edit_merge&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=merge&act=edit_merge&id=' + id);
            }
            function del(id){
                if(confirm('确定删除？')){
                    $.post('admin.php?ctrl=merge&act=del_action&id=' + id,'',function(data){
                        if(data.code ==1){
                         $.dialog.tips(data.msg);
                         \$tabs.tabs('load' , 0);
                        }else{
                           $.dialog.tips(data.msg); 
                        }
                        
                    },'json');
                }    
            }

            function _open(id){

                if(confirm('确定提前开服吗？')){
                    $.post('admin.php?ctrl=merge&act=open_server&id=' + id,'',function(data){
                        if(data.code ==1){
                         $.dialog.tips(data.msg);
                         \$tabs.tabs('load' , 0);
                        }else{
                           $.dialog.tips(data.msg); 
                        }
                        
                    },'json');
                }    
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    /**
     * [merge_list_data 列表数据源]
     * @return [type] [description]
     */
    public function merge_list_data(){
        $this->setOrder('m.create_time','desc');
        $this->setLimit();
        $this->call('MergeModel', 'merge_list_data' , $this->getConditions());
    }

    /**
     * [del_action 删除合服计划]
     * @return [type] [description]
     */
    public function del_action(){
        $id = $this->getParam('id');

        //删除的时候把服务器表的合服字段改成0
        $result = $this->m->getRow(array('parent','children') , array('id'=>$id));
        $server_id = $result['parent'].','.$result['children'];
        $server_id = explode(',',$server_id);
        $server_id = "'".implode("','" , $server_id)."'";
        $sql = "update ny_server set merge_status = 0 , merge_time = 0 where server_id in ($server_id)";
        $this->m->query($sql);

        $id && $rs =  $this->m->delete(array('id'=>$id));
        
        if($rs){
        	(new LogAction())->logAdd("删除合服计划：{$id}");
            exit(json_encode(array('code'=>1 , 'msg' => '删除成功')));
        }else{
            exit(json_encode(array('code'=>0 , 'msg' => '删除失败，请重试')));
        }
    }
    
    /**
     * [add_merge 添加合服列表]
     */
    public function add_merge(){
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();//全部服务器

        //查询已经添加合服计划，但是还没执行的服，即status = 0 
        $sql = "select parent ,children  from ny_merge where status  = 0";
        $rs = (new Model())->query($sql);
        //已经被勾选的服不再出现在子母服中
        foreach($rs as $k=>$v){
           foreach($server as $kk=>$vv){
            if(array_key_exists($v['parent'] , $vv)){
                unset($server[$kk][$v['parent']]);
            }
            $children = explode(',',$v['children']);
            if(is_array($children)){
                foreach($children as $vvv){
                    if(array_key_exists($vvv , $vv)){
                        unset($server[$kk][$vvv]);
                    }
                }   
            }else{
                if(array_key_exists($children , $vv)){
                    unset($server[$kk][$children]);
                }
            }
            
           }
        }
    
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        $this->smarty->display('merge/merge.tpl');  
    }

    /**
     * [save_merge 保存合服]
     * @return [type] [description]
     */
    public function save_merge(){
        
        $parent = $this->getParam('parent');
        $server = $this->getParam('server');
        $time = $this->getParam('time');
        $start_time = $this->getParam('start_time');

        if(empty($parent)){
             die (json_encode(array('code' => 200, 'msg' => '请选择母服')));
        }

        if(empty($server)){
             die (json_encode(array('code' => 200, 'msg' => '请选择子服')));
        }

        if(empty($time)){
             die (json_encode(array('code' => 200, 'msg' => '请选择合服时间')));
        }
        if(strtotime($time) <= time()){
             die (json_encode(array('code' => 200, 'msg' => '合服时间不能小于当前时间')));
        }

        if(empty($start_time)){
             die (json_encode(array('code' => 200, 'msg' => '请选择开服时间')));
        }
        if(strtotime($start_time) <= time()){
             die (json_encode(array('code' => 200, 'msg' => '开服时间不能小于当前时间')));
        }

        if(strtotime($start_time) <= strtotime($time)){
            die(json_encode(array('code' => 200, 'msg' => '开服时间不能小于合服时间')));
        }

        $data = array(
            'parent' => $parent[0],
            'children' => implode(',' , $server),
            'merge_time' => strtotime($time),
            'start_time' => strtotime($start_time),
            'create_time' => time(),
        );
        
        //判断否存在相对应的活动
        
        $servers = $server;
        $servers[] = $parent[0];


        $msg = $this->m->checkAct($servers, $time);

        if (!empty($msg)) {
            die(json_encode($msg));
        }
        

        $rs = $this->m->add($data);

        if($rs){
            //把服务器的合服标志改为1，合服时间改为添加的合服时间
            $server_id = $server;
            $server_id[] = $parent[0];
            $server_id = "'".implode("','" , $server_id)."'";

            $sql = "update ny_server set merge_status = 1 , merge_time = ".strtotime($time)." where server_id in ($server_id)";
            $this->m->query($sql);
            
            (new LogAction())->logAdd("添加合服计划：{$rs}");
            die (json_encode(array('code' => 400, 'msg' => '添加成功')));
        }else{
            die (json_encode(array('code' => 200, 'msg' => '添加失败，请重试')));
        }
    }

    public function edit_merge(){
        $id = $this->getParam('id');
        
        if(empty($id)){die('请选择编辑的合服计划');}
        $sql="select * from ny_merge where id =$id";
        $result=$this->m->query($sql);
        $result[0]['merge_time']=date('Y-m-d H:i',$result[0]['merge_time']);
        $result[0]['start_time']=date('Y-m-d H:i',$result[0]['start_time']);

        $this->smarty->assign('children' , explode(',', $result[0]['children']));

        $this->smarty->assign('data',$result[0]);
        

        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        
        //查询已经添加合服计划，但是还没执行的服，即status = 0 ,编辑的时候需要去掉本身
        $sql = "select parent ,children  from ny_merge where status  = 0 and id != $id";
        $rs = (new Model())->query($sql);
        //已经被勾选的服不再出现在子母服中
        foreach($rs as $k=>$v){
           foreach($server as $kk=>$vv){
            if(array_key_exists($v['parent'] , $vv)){
                unset($server[$kk][$v['parent']]);
            }
            $children = explode(',',$v['children']);
            if(is_array($children)){
                foreach($children as $vvv){
                    if(array_key_exists($vvv , $vv)){
                        unset($server[$kk][$vvv]);
                    }
                }   
            }else{
                if(array_key_exists($children , $vv)){
                    unset($server[$kk][$children]);
                }
            }
            
           }
        }
        
        
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        $this->smarty->display('merge/edit.tpl');
    }

    public function  save_edit(){
        $parent = $this->getParam('parent');
        $server = $this->getParam('server');
        $time = $this->getParam('time');
        $start_time = $this->getParam('start_time');
        $id = $this->getParam('id');

        if(empty($parent)){
             die (json_encode(array('code' => 200, 'msg' => '请选择母服')));
        }

        if(empty($server)){
             die (json_encode(array('code' => 200, 'msg' => '请选择子服')));
        }

        if(empty($time)){
             die (json_encode(array('code' => 200, 'msg' => '请选择合服时间')));
        }

        if(empty($start_time)){
             die (json_encode(array('code' => 200, 'msg' => '请选择开服时间')));
        }

        $data = array(
            'parent' => $parent[0],
            'children' => implode(',' , $server),
            'merge_time' => strtotime($time),
            'start_time' => strtotime($start_time),
            'create_time' => time(),
        );

        //判断是否存在相对应的活动
        $servers = $server;
        $servers[] = $parent[0];


        $msg = $this->m->checkAct($servers, $time);

        if (!empty($msg)) {
            die(json_encode($msg));
        }

        $res = $this->m->update($data,array('id'=> $id));

        if($res !== false){
            //把服务器的合服标志改为1，合服时间改为添加的合服时间
            $server_id = $server;
            $server_id[] = $parent[0];
            $server_id = "'".implode("','" , $server_id)."'";

            $sql = "update ny_server set merge_status = 1 , merge_time = ".strtotime($time)." where server_id in ($server_id)";
            $this->m->query($sql);
            (new LogAction())->logAdd("编辑合服计划：{$id}");
            die (json_encode(array('code' => 400, 'msg' => '更新成功')));
        }else{
            die (json_encode(array('code' => 200, 'msg' => '更新失败，请重试')));
        }
    }
    /**
     * [open_server 提前开服]
     * @return [type] [description]
     */
    public function open_server(){

        $id = $this->getParam('id');

        $rs = $this->m->getRow(array('children', 'parent'),array('id'=>$id));

        //把merge表的 notify 改成4 ， 开服时间改为当前时间 ，开启服务器
        
        $result = $this->m->update(array('notify' => 4 , 'start_time' => time()) , array('id'=>$id)); 
        
        if($result === false){

            $json  = array('msg' => '更新合服计划失败，请重试' , 'code' =>0);
            echo json_encode($json);exit;
        }

        //把server表的子母服的状态改成正常，即为5
        
        $server_id = $rs['children'] .','.$rs['parent'];
        $server_id = explode(',', $server_id);
        $servers =  $server_id ;
       
        $server_id = "'".implode("','" , $server_id)."'";

         //合服的服务器中出现有母服的情况
        $childrensql = "select server_id from ny_server where mom_server in ({$server_id})";

        $children =  $this->m->query($childrensql);
        
        if($children){

          foreach ($children as $k => &$v) {
            $v = "'{$v['server_id']}'";
          }
          
          $children = implode(',', $children);

          $server_id .= ','.$children; 
        }
       
        $sql = "update ny_server set status = 2 where server_id  in ($server_id)";
       
        $data  = $this->m->query($sql);


        //根据渠道组去更改服务器状态
        $gsql = "select distinct(group_id) from ny_server WHERE server_id IN ({$server_id}) ";
       
        $g = $this->m->query($gsql);
       
        $gid = array_column($g, 'group_id');
        $gid = implode(',', $gid);
        $updatesql = "SELECT server_id from (SELECT server_id,group_id,open_time FROM ny_server  where  status not in (0,-1)   and display = 1 and group_id in ($gid) ORDER BY open_time desc) b GROUP BY b.group_id ";
      
        $update = $this->m->query($updatesql);
        $server = array_column($update, 'server_id');

        $server = implode("','" , $server);
        $sql  = "update ny_server set status = '1' where server_id  in ('$server')";

        $this->m->query($sql);
       
        if($data === false){

            $json  = array('msg' => '更新服务器状态失败，请重试' , 'code' =>0);
            echo json_encode($json);exit;
        }

        //开启拍卖行
        
        $rs = (new ServerModel())->getServer($servers, '*');
  
        $params = array();

        $servers = array();
        foreach ($rs as $k2 => $r2) {
          $psId          = $r2['channel_num'];
          $sid           = $r2['num'];
          $sign          = $r2['server_id']; 
          $params[$sign] = array( 0 => array('pId' => $psId, 'sId' => $sid));
          $servers[$sign] = $r2;

        }

        //todo 发送交易组
        $url = Helper::v2();
        $res = Helper::rpc($url , $servers,'openTrade' , $params);

        $res = array_unique(array_values($res));

        if ( count($res) == 1 && $res[0] == 1) {

          $sql = "UPDATE ny_merge set notify = 5 where id = $id";
         
          $this->m->query($sql);

        }else{

            $json  = array('msg' => '开启拍卖行失败，请重试' , 'code' =>0);
            echo json_encode($json); exit;
        }

        $json  = array('msg' => '提前开服成功' , 'code' =>1);
        echo json_encode($json);
        (new LogAction())->logAdd("合服计划中提前开服：{$id}");

        /*if($result !== false && $data !== false){

            $json  = array('msg' => '提前开服成功' , 'code' =>1);
            echo json_encode($json);

        }else{

            $json  = array('msg' => '提前开服失败，请重试' , 'code' =>0);
            echo json_encode($json);
        }*/


    }

    
}