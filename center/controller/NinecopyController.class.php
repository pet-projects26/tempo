<?php
!defined('IN_WEB') && exit('Access Denied');

class NineCopyController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启九天之巅的人数。<br>';
        $header .= '参与人数：该日进行过九天之巅（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '登顶人数 = 该日全服登顶成功人数（通过第八层就算，不管有没有进入第九层）之和 <br>';
        $header .= '通关第X层人数比例 = 该日全服结算时，通关第X层的人数 / 参与人数 <br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '登顶人数', '通关第1层人数比例', '通关第2层人数比例', '通关第3层人数比例', '通关第4层人数比例', '通关第5层人数比例', '通关第6层人数比例', '通关第7层人数比例', '通关第8层人数比例'));
        $this->setSource('NineCopy', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('NineCopyModel', $method, $this->getConditions());
    }
}