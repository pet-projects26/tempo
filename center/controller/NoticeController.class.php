<?php
!defined('IN_WEB') && exit('Access Denied');

class NoticeController extends Controller{
    private $zm;
    private $nt;
    private $cgm;

    public function __construct(){
        parent::__construct();
		$this->nt = new NoticeModel();
		$this->cgm = new ChannelgroupModel();
        
    }

    //分区管理（菜单）
    public function index(){
        $tabs = array(
            array('title' => '公告列表', 'url' => 'admin.php?&ctrl=notice&act=record'),
            array('title' => '添加公告', 'url' => 'admin.php?&ctrl=notice&act=add'),
            array('title' => '编辑公告', 'url' => 'admin.php?&ctrl=notice&act=edit'),
        );
        $this->tabs($tabs);
    }
	
	public function record(){
        $this->setFields(array('ID', '渠道组', '服务器', '内容', '创建者', '发布', '操作'));
        $this->setSource('notice' , 'record_data');
		$js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=notice&act=edit&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=notice&act=edit&id=' + id);
            }
            function del(id){
				if(confirm('确定删除？')){
					$.post('admin.php?ctrl=notice&act=del&id=' + id,'',function(data){
						\$.dialog.tips(data.msg);
						if (data.code == 1) {
						    \$dataTable.fnMultiFilter(getFilter());
						}
					}, 'json');
				}
                
            }
			function notice(type, id){
			        
			    var str = type == 1 ? '同意并发布' : '拒绝发布';    
			
				if(confirm('确定'+str+'?')){
					$.post('admin.php?ctrl=notice&act=notice&id=' + id +'&type=' + type,'',function(data){
						\$.dialog.tips(data.msg);
						if (data.code == 1) {
						    \$dataTable.fnMultiFilter(getFilter());
						}
						
					},'json');
				}	
			}
			
			function content(id) {			    
			     $.ajax({
                    url: 'admin.php?ctrl=notice&act=content',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json'
                    }).done(function(data){                                  
                    if (data.code == 0) {                        
                        $.dialog.tips(data.msg);                      
                         return false;
                    }                        				
                    $.dialog({
                        title: data.title,
                        max: false,
                        min: false,
                        content: data.html
                    });
                    
                    });			  
			     }
END;
        $this->setJs($js);
        $this->displays();
	}
	
	
	public function record_data(){
		$this->setLimit();
        $this->call('NoticeModel' , 'record_data');
	}
	
	
	public function add(){

		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
		$this->smarty->display('notice/add.tpl');
	}
	
	public function add_action(){

        //$type = $this->getParam('type');
        $type = 1;
		//$notice_type = $this->getParam('notice_type');
        $contents = addslashes(htmlentities($this->getParam('contents')));;
		$group = $this->getParam('channel_group');
		$server = $this->getParam('server');
        //$sort = $this->getParam('sort');
        //$title = $this->getParam('title');
		$status = $this->getParam('status');
		$id = $this->getParam('id');
        $OperatingState = empty($id) ? 'add' : 'update';
		
		if($group[0] =='[]' || $group[0] == ''){
			
			$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
			$group = array_column($channelGroupRes, 'id');
			$group = implode(',', $group);

		}else{
			$group = implode(',', $group);	
		}

		if($server[0] =='[]' || $server[0] == ''){
			$server = '';
		}else{
			$server = implode(',', $server);	
		}
		
		$arr['type'] = $type;
		//$arr['notice_type'] = $notice_type;
		$arr['contents'] = $contents;
		$arr['groups'] = $group;
		$arr['servers'] = $server;
        //$arr['sort'] = $sort;
        //$arr['title'] = trim($title);
		
		if($status  == 'add'){
			$arr['create_time']  = time();	
		}
		
		$arr['admin']  = $_SESSION['username'];
		$arr['status'] = $status;
        $arr['OperatingState'] = $OperatingState;
		$arr['id'] = $id;

        $msg = '编辑成功:';
        if ($OperatingState == 'update') {
            //发送给后端
            $msg = $this->send($arr, $arr['contents']);
        }

		if($this->nt->add_notice($arr)){
            if ($OperatingState == 'add') {
				$msg ='添加成功';
            }
			$json = array('msg' => $msg , 'code' => 1);
		}
		else{
            if ($OperatingState == 'add') {
				$msg ='未添加成功，请再次尝试';
			}else{
				$msg ='未编辑成功，请再次尝试';
			} 
			$json = array('msg' => $msg , 'code' => 0);
		}
		echo json_encode($json);
	}
	
	public function edit(){

        $id = $this->getParam('id');
        //$type = $this -> getParam('type');
		$result = $this->nt->getRow('*',array('id' => $id));
		$groups =  array_filter(explode(',', $result['groups']));
		$servers =  array_filter(explode(',', $result['servers']));
		
		//服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        
        $server = (new ServerModel())->getServerByTrait();

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
        $this->smarty->assign('id' , $id);
        $this->smarty->assign('result' , $result);
        //$this->smarty->assign('type' , $type);
		$this->smarty->assign('data', array('agent' => $groups, 'server' => $servers));
		
		$this->smarty->display('notice/edit.tpl');		
	}

    public function content()
    {
        $id = $this->getParam('id');
        $notice = $this->nt->getRow(['contents', 'id'], ['id' => $id]);

        if (empty($notice)) {
            exit(json_encode(array('msg' => '参数错误', 'code' => 0)));
        }

        $return['title'] = '公告内容';
        $return['html'] = htmlspecialchars_decode(stripslashes($notice['contents']));

        $return['code'] = 1;

        die(json_encode($return));

    }

    public function notice()
    {
		$id = $this->getParam('id');
		$type = $this->getParam('type');

        $notice = $this->nt->getRow(['contents', 'servers', 'groups', 'status'], ['id' => $id]);

        if (empty($notice) || $notice['status'] != 0) {
            exit(json_encode(array('msg' => '参数错误', 'code' => 0)));
        }
        $msg = '';
		if($type ==1){//需要查看这个渠道组是否有存在已经发布了的，有就提示不给发布
            $msg = $this->send($notice, $notice['contents']);
        } else {
            $msg = '已拒绝发布';
        }

		if($this->nt->notice($id,$type)){
            exit(json_encode(array('msg' => $msg, 'code' => 1)));
		}else{
			exit(json_encode(array('msg' => '发布失败，请重试' ,'code' =>0)));		
		}

    }

    public function del()
    {
        $id = $this->getParam('id');

        $notice = $this->nt->getRow(['servers', 'groups', 'status'], ['id' => $id]);

        if (empty($notice)) {
            exit(json_encode(array('msg' => '参数错误', 'code' => 0)));
        }

        $msg = '删除成功:';
        if ($notice['status'] == 1) {
            $msg .= $this->send($notice, '');
        }

        if ($this->nt->notice($id, 3)) {
            exit(json_encode(array('msg' => '删除成功', 'code' => 1)));
        } else {
            exit(json_encode(array('msg' => '删除失败，请重试', 'code' => 0)));
        }
    }

    public function send($notice, $contents)
    {
        $server = explode(',', $notice['servers']);
        $channelGroup = explode(',', $notice['groups']);

        $serverList = (new GiftModel())->getServer($server, $channelGroup);

        $return = [];
        $return['code'] = 0;

        $instance = new ServerModel();
        $server_config = $instance->getServer(array_keys($serverList), ['s.server_id as server_id', 's.name as name', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port']);
        //判断服务器是否配置
        $serverIds = [];
        foreach ($server_config as $key => $msg) {
            if (empty($msg) || is_null($msg)) {
                array_push($serverIds, $key);
            }
        }
        $serRow = (new ServerModel())->getServer($serverIds, array('server_id', 'name'));
        $serInfo = array();
        foreach ($serRow as $tem) {
            $serInfo[$tem['server_id']] = $tem['name'];
        }
        $str = [];
        foreach ($serverIds as $sId) {
            array_push($str, $serInfo[$sId] . " : 服务器未配置<br/>");
        }
        if (!empty($str)) {
            $return['msg'] = implode('', $str);
            die(json_encode($return));
        }
        //发送给后端
        $sendInfo = [];
        $sendInfo['opcode'] = Pact::$UpdateNotice;

        if ($contents != '') {
            $contents = htmlspecialchars_decode(stripslashes($notice['contents']));
        }

        $sendInfo['str'] = [$contents];
        $msg = '';
        foreach ($server_config as $row) {
            $rs = $this->nt->socketCall($row['websocket_host'], $row['websocket_port'], 'giftCDKEY', $sendInfo);
            if ($rs === 0) {
                $msg .= $row['name'] . ': 发送成功';
            } else {
                $msg .= $row['name'] . ': 发送失败';
            }
        }

        return $msg;
    }

    /*
            //先查询已经发布的公告
            $notice = $this->nt->getRow('*' , array('status' => 1));
            if($notice){
                $result = $this->nt->getRow('groups',array('id' => $id));
                $group = $notice['groups'];
                $newgroup = $result['groups'];

                if($group == '' && $newgroup == ''){//已经发布的和即将发布的都为空，即两个都为全渠道组

                    exit(json_encode(array('msg' => 'ID'.$notice['id'].'已经发布了全渠道组,不能再发布全渠道组了' ,'code' =>-1)));

                }elseif($group == '' && $newgroup != ''){//已经发布的为全渠道组，即将发布的是某些渠道组，所以重复的是即将发布的渠道组

                    $name = $this->nt->getGroupName($newgroup);

                    exit(json_encode(array('msg' => 'ID'.$notice['id'].'已经发布了'.$name.'渠道组' ,'code' =>-1)));

                }elseif($group != '' && $newgroup == ''){//已经发布的是某些渠道组，即将发布的为全渠道组，所以重复的是已经发布的渠道组

                    $name = $this->nt->getGroupName($group);

                    exit(json_encode(array('msg' => 'ID'.$notice['id'].'已经发布了'.$name.'渠道组' ,'code' =>-1)));

                }elseif($group != '' && $newgroup != ''){//已经发布的和即将发布的都为某些渠道组，取两者的交集，即为重复的渠道组

                    $group = explode(',', $group);
                    $newgroup = explode(',', $newgroup);

                    $intersect = array_intersect($group,$newgroup);

                    if(!empty($intersect)){
                        $intersect = implode(',', $intersect);

                        $name = $this->nt->getGroupName($intersect);

                        exit(json_encode(array('msg' => 'ID'.$notice['id'].'已经发布了'.$name.'渠道组' ,'code' =>-1)));
                    }

                }

            }*/
}