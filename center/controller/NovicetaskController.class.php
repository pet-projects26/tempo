<?php
!defined('IN_WEB') && exit('Access Denied');

class NoviceTaskController extends Controller
{
    public function index()
    {
        $header = '说明：<br>';
        $header .= '新增用户数：该日新增且连接上游戏服务器的用户数。<br>';
        $header .= '任务ID：该任务的ID。  策划到时会提供一批需要统计的任务ID。<br>';
        $header .= '任务名称：该任务的名称。<br>';
        $header .= '领取人数：当天领取该任务的人数<br>';
        $header .= '完成人数：领取人数中，完成该任务的人数。<br>';
        $header .= '完成率： 完成人数 / 领取人数 （小数点后两位，四舍五入）<br>';
        $header .= '整体完成率： 完成人数 / 第一步任务领取人数 （小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '新增用户数', '任务ID', '任务名称', '领取人数', '完成人数', '完成率', '整体完成率'));
        $this->setSource('NoviceTask', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'eq', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'ASC', array(array('task_id', 'ASC')));
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('NoviceTaskModel', $method, $this->getConditions());
    }
}