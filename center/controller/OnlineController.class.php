<?php
!defined('IN_WEB') && exit('Access Denied');

class OnlineController extends Controller{
    //实时在线（菜单）
	public function real(){
        $this->smarty->assign('scp' , $this->getScp(array('showGroup' => 0 , 'showServer' => 1 , 'showChannel' => 0 , 'showPackage' => 0),array(),array(),array(),array(),1));
		$this->smarty->display('online/real.tpl');	
	}
	public function real_data(){
		$date=$this->getParam('date');
		$start_hour=$this->getParam('start_hour');
		$end_hour=$this->getParam('end_hour');
		$date=!empty($date)?$date:date('Y-M-d');
		$starttime=strtotime($date.' '.$start_hour.':00:00');
		$endtime=strtotime($date.' '.$end_hour.':00:00')-1;

		$starttime = $starttime * 1000;
		$endtime = $endtime * 1000;

		$conditions['WHERE']['create_time::BETWEEN'] = array($starttime , $endtime);
		$this->setConditions($conditions);
		$this->setCondition('scp' , 'scp');
		$this->setOrder('time','asc');
		$data = array();
		$this->setLimit();
        $conditions = $this->getConditions();
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0]; //只能选一个服
            unset($conditions['WHERE']['server::IN']);
            $data = $this->call('OnlineModel' , 'getRealData' , array('conditions' => $conditions) , $server_id);
			
           $data = $data[$server_id];
			
        }
		
        if(count($data)){
            foreach($data as $k => $row){
                $data['x'][] = $row['time'];
                $data['y'][0][] = $row['role_num'];
                $data['y'][1][] = $row['hook_num'];
            }
        }
        else{
            $data['x'] = array();
            $data['y'][0] = array();
            $data['y'][1] = array();
        }
        /*$options = array(
            'plotOptions' => array(
                'line' => array(
                    'lineWidth' => 1.5 ,
                    'fillOpacity' => 0.1,
                    'marker' => array('enabled' => false , 'states' => array('hover' => array('enabled' => true , 'radius' => 2)))
                ),
                'shadow' => false
            )
        );*/
		
        $size = array('x' => 120 , 'y' => 20);
        $title = array('main' => '实时在线' , 'x' => '时间' , 'y' => '角色数');
        $name = array('在线角色数' , '离线挂机角色数');
        $chart = init_highchart_line('real_chart' , $title , $data  , $name );
		
		
        $this->smarty->assign('real_chart' , $chart);
        krsort($data['x']);
        krsort($data['y'][0]);
        krsort($data['y'][1]);
		$this->smarty->assign('x' , $data['x']);
		$this->smarty->assign('y' , $data['y']);
		
		
        $this->smarty->display('online/real_chart_data.tpl');
	}
    //综合在线（菜单）
    public function multi(){
        $this->smarty->assign('start_time' , date('Y-m-d' , strtotime('last month'))); //默认显示30天的数据
        $this->smarty->assign('end_time' , date('Y-m-d'));
         $this->smarty->assign('scp' , $this->getScp(array('showGroup' => 0 , 'showServer' => 1 , 'showChannel' => 0 , 'showPackage' => 0),array(),array(),array(),array(),1));
        $this->smarty->display('online/multi_chart.tpl');
    }
    public function multi_data(){
        $conditions = array();
        $type = $this->getParam('type');
        switch($type){
            case 1:
                $conditions['WHERE']['create_time::>='] = strtotime($this->getParam('start_time'));
                $conditions['WHERE']['create_time::<='] = strtotime($this->getParam('end_time')) + 86400;
                break;
            case 2:
                $conditions['WHERE']['create_time::>='] = strtotime('-60 day');
                $conditions['WHERE']['create_time::<='] = time() + 86400;
                break;
            case 3:
                $conditions['WHERE']['create_time::>='] = strtotime('-90 day');
                $conditions['WHERE']['create_time::<='] = time() + 86400;
        }
        $this->setConditions($conditions);
        $this->setCondition('scp' , 'scp');
        $conditions = $this->getConditions();
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0]; //只能选一个服
            unset($conditions['WHERE']['server::IN']);
            $data = $this->call('OnlineModel' , 'getMultiData' , array('conditions' => $conditions) , $server_id);
			
            $data = $data[$server_id];
            $size = array('x' => 1 , 'y' => 20);
            $name = array('最大人数' , '平均人数' , '最小人数');
            $title = array('main' => '综合在线' , 'x' => '时间' , 'y' => '人数');
			
            $multi_chart = init_highchart_line('multi_chart' , $title , $data ,  $name);
			
            $this->smarty->assign('multi_data' , $data);
            $this->smarty->assign('x' , $data['x']);
            $this->smarty->assign('y' , $data['y']);
            $this->smarty->assign('multi_chart' , $multi_chart);
            $this->smarty->display('online/multi_chart_data.tpl');
        }
        else{
            echo '<div style="min-width:400px;height:20px;padding:10px 15px;margin:6px auto;border:solid 1px #ABABAB;border-radius:5px;">请选择服务器</div>';
        }
    }


    //实时在线（菜单）
    public function statistics(){
        $this->smarty->assign('scp' , $this->getScp(array('showGroup' => 0 , 'showServer' => 1 , 'showChannel' => 0 , 'showPackage' => 0),array(),array(),array(),array(),1));
        $this->smarty->display('online/statistics.tpl');
    }
    public function statistics_data(){
        $date=$this->getParam('date');
        $start_hour=$this->getParam('start_hour');
        $end_hour=$this->getParam('end_hour');
        $date=!empty($date)?$date:date('Y-M-d');
        $starttime=strtotime($date.' '.$start_hour.':00:00');
        $endtime=strtotime($date.' '.$end_hour.':00:00')-1;
        $starttime = $starttime * 1000;
        $endtime = $endtime * 1000;
        $conditions['WHERE']['create_time::BETWEEN'] = array($starttime , $endtime);
        $this->setConditions($conditions);
        $this->setCondition('scp' , 'scp');
        $this->setOrder('time','asc');
        $data = array();
        $this->setLimit();
        $conditions = $this->getConditions();
        if(!isset($conditions['WHERE']['server::IN'])){
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }
        $server_id = $conditions['WHERE']['server::IN'][0]; //只能选一个服
        unset($conditions['WHERE']['server::IN']);
        $data = $this->call('OnlineModel' , 'getRealData' , array('conditions' => $conditions) , $server_id);

        $data = $data[$server_id];

        if(count($data)){
            foreach($data as $k => $row){
                $data['x'][] = $row['time'];
                $data['y'][0][] = $row['role_num'];
            }
        }
        else{
            $data['x'] = array();
            $data['y'][0] = array();
        }
        /*$options = array(
            'plotOptions' => array(
                'line' => array(
                    'lineWidth' => 1.5 ,
                    'fillOpacity' => 0.1,
                    'marker' => array('enabled' => false , 'states' => array('hover' => array('enabled' => true , 'radius' => 2)))
                ),
                'shadow' => false
            )
        );*/

        $size = array('x' => 120 , 'y' => 20);
        $title = array('main' => '实时在线' , 'x' => '时间' , 'y' => '角色数');
        $name = array('在线角色数');
        $chart = init_highchart_line('real_chart' , $title , $data  , $name );


        $this->smarty->assign('real_chart' , $chart);
        krsort($data['x']);
        krsort($data['y'][0]);
        $this->smarty->assign('x' , $data['x']);
        $this->smarty->assign('y' , $data['y']);


        $this->smarty->display('online/statistics_chart_data.tpl');
    }
}
