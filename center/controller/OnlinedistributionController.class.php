<?php
!defined('IN_WEB') && exit('Access Denied');

class OnlinedistributionController extends Controller{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃玩家数：当天连接上游戏服务器的玩家数。<br>';
        $header .= '在线时间段的玩家占比：以分钟为单位，统计当天活跃玩家中，单人累计在线处于该段位的玩家数 / 活跃玩家数 *100%   （小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数','(0, 1]' , '(1, 5]' , '(5, 10]', '(10, 20]', '(20, 30]', '(30, 60]', '(60, 90]', '(90, 120]', '(120, 150]', '(150, 180]', '(180, 240]', '(240, 300]'));
        $this->setField('(300, 1440]' , 100);
        $this->setField('(1440, -]');
        $this->setSource('Onlinedistribution' , 'index_data' , 1);
        $this->setLimit('24');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');

        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('distribution_data' , 'distribution_export');
        $this->call('OnlinedistributionModel' , $method , $this->getConditions());
    }
}