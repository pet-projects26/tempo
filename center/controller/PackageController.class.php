<?php

class PackageController extends Controller
{
    private $cm;
    private $pm;

    public function __construct()
    {
        parent::__construct();
        $this->cgm = new ChannelgroupModel();
        $this->cm = new ChannelModel();
        $this->pm = new PackageModel();
    }

    //包管理（菜单）
    public function index()
    {
        $tabs = array(
            array('title' => '包列表', 'url' => 'admin.php?&ctrl=package&act=record'),
            array('title' => '添加包', 'url' => 'admin.php?&ctrl=package&act=add'),
            array('title' => '编辑包', 'url' => 'admin.php?&ctrl=package&act=edit'),
            array('title' => '包', 'url' => 'admin.php?&ctrl=package&act=package'),
            // array('title' => '批量提审','url'=>'admin.php?ctrl=review&act=add')

        );
        $this->tabs($tabs);
    }

    //包列表（子菜单）
    public function record()
    {

        $grouparr = $this->cgm->getChannelGroup(array(), array('id', 'name'));
        $group[''] = '-';
        foreach ($grouparr as $k => $v) {
            $group[$v['id']] = $v['name'];
        }

        $channelarr = $this->cm->getChannel(array(), array('channel_id', 'name'));
        $channel[''] = '-';
        foreach ($channelarr as $k => $v) {
            $channel[$v['channel_id']] = $v['name'];
        }

        $this->setSearch('渠道组', 'select', $group);
        $this->setSearch('渠道', 'select', $channel);
        $this->setSearch('包号');
        $this->setFields(array('包号', '包名', '游戏名', '游戏id', '渠道', '渠道组', '创建时间', 'URL', 'cdn链接', '平台', '操作'));
        $this->setSource('package', 'record_data');
        $js = <<< END
            function edit(pid){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=package&act=edit&pid=' + pid);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=package&act=edit&pid=' + pid);
            }
            function del(pid){
                $.dialog({
                    title: '',
                    content: '确定要删除吗？',
                    max: false,
                    min: false,
                    button : [{
                        name : '确定',
                        callback: function(){
                            $.ajax({
                                url: 'admin.php?ctrl=package&act=del_action&pid=' + pid,
                                type: 'POST',
                                dataType: 'JSON',
                                data: $(this).serialize()
                            }).done(function(data){
                                if(data ==1){
                                    $.dialog.tips('删除成功');
                            \$tabs.tabs('load' , 0);
                                }else{
                                    $.dialog.tips('删除失败');
                                }
    
                            })
                        }
                    },{ name : '取消' }]
                });   
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'eq', 'group_id');
        $this->setCondition(1, 'eq', 'channel_id');
        $this->setCondition(2, 'eq', 'package_id');
        $this->setLimit();
        $this->setOrder('create_time');

        $this->call('PackageModel', 'record_data', $this->getConditions());
    }

    //添加包（子菜单）
    public function add()
    {
//    	$rv_param3 = CDict::$rv_param3;
//    	$this->smarty->assign('rv_param3',$rv_param3);
//
//    	$rv_param2 = CDict::$rv_param2;
//    	$this->smarty->assign('rv_param2',$rv_param2);
//
//        $rv_name = CDict::$rv_name;
//        $rv_param = CDict::$rv_param;
//        $this->smarty->assign('rv_param',$rv_param);
//        $this->smarty->assign('rv_name',$rv_name);
        $this->smarty->assign('channels', $this->cm->getChannel(array(), array('channel_id', 'name')));
        $this->smarty->assign('groups', $this->cgm->getChannelGroup(array(), array('id', 'name')));
        $this->smarty->display('package/add.tpl');
    }

    public function add_action()
    {
        $package_id = $this->getParam('package_id');
        if ($package_id) {

            $channel_id = $this->getParam('channel_id');
            $group_id = $this->getParam('group_id');
            $package_name = $this->getParam('package_name');
            //$game_name = $this->getParam('game_name');
            $review_num = $this->getParam('review_num');
            $url = $this->getParam('url');
            //$cdn_url = $this->getParam('cdn_url');
            $platform = $this->getParam('platform');
//            $rv_param = $this->getParam('param');
//            $rv_param2 = $this->getParam('param2');
//            $rv_param3 = $this->getParam('param3');
            $test_ser_num = $this->getParam('test_ser_num');
//            $type = $this->getParam('type');
//            if ($type == 2) {
//            	$rv = $rv_param2;
//            }elseif ($type == 3){
//            	$rv = $rv_param3;
//            }else {
//            	$rv = $rv_param;
//            }
            $data = array(
                'channel_id' => $channel_id,
                'package_id' => $package_id,
                'group_id' => $group_id,
                'name' => $package_name,
                //'game_name' => $game_name,
                'review_num' => self::trimAll($review_num),
                'test_server_num' => self::trimAll($test_ser_num),
                'url' => self::trimAll($url),
                //'cdn_url' => self::trimAll($cdn_url),
                'create_time' => time(),
                'platform' => intval($platform),
                //'rv_param'=>json_encode($rv),
                //'type'=>$type,
            );

            if ($this->pm->getPackageUnique('package_id', $package_id)) {
                $json = array('msg' => '包号已存在', 'code' => 0);
            } else if ($this->pm->getPackageUnique('name', $package_name)) {
                $json = array('msg' => '包名已存在', 'code' => 0);
            } else if (!preg_match('/^[0-9]+$/', $package_id)) {
                $json = array('msg' => '包号格式不正确', 'code' => 0);
            } else if ($this->pm->addPackage($data)) {
                //查询游戏id和game_secret_key
                $game = $this->pm->getPackage($data['package_id'], ['p.package_id', 'c.game_id as game_id', 'c.game_secret_key as secret_key', 'c.game_abbreviation', 'c.cdn_url']);
                //修改redis
                $redisData = [
                    'package_id' => $data['package_id'],
                    'name' => $data['name'],
                    'cdn_url' => $data['cdn_url'],
                    'channel' => $data['channel_id'],
                    'group_id' => $data['group_id'],
                    'review_num' => $data['review_num'],
                    'game_id' => $game['game_id'],
                    'game_abbreviation' => $game['game_abbreviation'],
                    'secret_key' => $game['secret_key'],
                    'os' => $data['platform']
                ];
                (new ServerRedis(true))->setPackage(1, $redisData);
                $json = array('msg' => '添加成功', 'code' => 1);
            } else {
                $json = array('msg' => '未添加成功，请再次尝试', 'code' => 0);
            }
            echo json_encode($json);
            (new LogAction())->logAdd("添加包号：{$package_id}");
        }
    }

    //编辑包（子菜单）
    public function edit()
    {
        $package_id = $this->getParam('pid');
        $package = $this->pm->getPackage($package_id);
//        $rv_name = CDict::$rv_name;
//        $rv_param = CDict::$rv_param;
//
//        $rv_param2 = CDict::$rv_param2;
//
//        $rv_param3 = CDict::$rv_param3;


        if ($package) {

//        	if ($package['type'] == 2) {
//        		$package['rv_param'] && $this->smarty->assign('param2',json_decode($package['rv_param'],'true'));
//        	}elseif($package['type'] == 3){
//        		$package['rv_param'] && $this->smarty->assign('param3',json_decode($package['rv_param'],'true'));
//        	}else {
//        		$package['rv_param'] && $this->smarty->assign('param',json_decode($package['rv_param'],'true'));
//        	}

//        	$this->smarty->assign('rv_param3',$rv_param3);
//        	$this->smarty->assign('type',$package['type']);
//        	$this->smarty->assign('rv_param2',$rv_param2);
//            $this->smarty->assign('rv_param',$rv_param);
//            $this->smarty->assign('rv_name',$rv_name);
            $package['id'] && $this->smarty->assign('id', $package['id']);
            $package['channel_id'] && $this->smarty->assign('channel_id', $package['channel_id']);
            $package['group_id'] && $this->smarty->assign('group_id', $package['group_id']);
            $package['name'] && $this->smarty->assign('package_name', $package['name']);
            //$package['game_name'] && $this->smarty->assign('game_name1' , $package['game_name']);
            $package['review_num'] && $this->smarty->assign('review_num', $package['review_num']);
            $package['test_server_num'] = !empty($package['test_server_num']) ? $package['test_server_num'] : '';
            $this->smarty->assign('test_server_num', $package['test_server_num']);
            $package['url'] && $this->smarty->assign('url', $package['url']);
            //$package['cdn_url'] && $this->smarty->assign('cdn_url', $package['cdn_url']);
            $package['platform'] && $this->smarty->assign('platform', $package['platform']);

            $package_id && $this->smarty->assign('package_id', $package_id);
            $this->smarty->assign('channels', $this->cm->getChannel(array(), array('channel_id', 'name')));
            $this->smarty->assign('groups', $this->cgm->getChannelGroup(array(), array('id', 'name')));
        }
        $this->smarty->display('package/edit.tpl');
    }

    public function edit_action()
    {
        $package_id = $this->getParam('package_id');
        if ($package_id) {
            $old_package_id = $this->getParam('old_package_id');
            $channel_id = $this->getParam('channel_id');
            $package_name = $this->getParam('package_name');
            $group_id = $this->getParam('group_id');
            $game_name = $this->getParam('game_name');
            $review_num = $this->getParam('review_num');
            $url = $this->getParam('url');
            // $cdn_url = $this->getParam('cdn_url');
//            $rv_param = $this->getParam('param');
//            $rv_param2 = $this->getParam('param2');
//            $rv_param3 = $this->getParam('param3');
            $test_ser_num = $this->getParam('test_ser_num');

//            $type = $this->getParam('type');
//            if ($type == 2) {
//            	$rv = $rv_param2;
//            }elseif ($type == 3){
//            	$rv = $rv_param3;
//            }else {
//            	$rv = $rv_param;
//            }
            $data = array(
                'channel_id' => $channel_id,
                'package_id' => $package_id,
                'name' => $package_name,
                //'game_name' => $game_name,
                'group_id' => $group_id,
                'review_num' => self::trimAll($review_num),//去除前后及中间空格
                'test_server_num' => self::trimAll($test_ser_num),
                'url' => self::trimAll($url),
                // 'cdn_url' => self::trimAll($cdn_url)
                //'rv_param'=>json_encode($rv),
                //'type'=>$type?$type:0,
            );
            if ($this->pm->getPackageUnique('package_id', $package_id, $old_package_id)) {
                $json = array('msg' => '包号已存在', 'code' => 0);
            } else if ($this->pm->getPackageUnique('name', $package_name, $old_package_id)) {
                $json = array('msg' => '包名已存在', 'code' => 0);
            } else if (!preg_match('/^[0-9]+$/', $package_id)) {
                $json = array('msg' => '包号格式不正确', 'code' => 0);
            } else if ($this->pm->editPackage($data, $old_package_id)) {
                $json = array('msg' => '保存成功', 'code' => 1);

                (new ServerRedis(true))->setPackage(3, $data);
                (new LogAction())->logAdd("编辑包号：{$old_package_id} => {$package_id}");
            } else {
                $json = array('msg' => '没有任何改变', 'code' => 0);
            }

            echo json_encode($json);

        }
    }

    //删除包
    public function del_action()
    {
        $package_id = $this->getParam('pid');
        $package_id && $rs = $this->pm->delPackage($package_id);
        //修改redis
        $redisData = [
            'package_id' => $package_id,
        ];
        (new ServerRedis(true))->setPackage(3, $redisData);
        if ($rs > 0) {
            echo 1;
        } else {
            echo 0;
        }
        (new LogAction())->logAdd("删除包号：{$package_id}");
    }

    public function package()
    {
        //渠道组
        $allgroup = $this->cgm->getChannelGroup(array(), array('id', 'name'));

        $selectgroup[''] = '-';
        foreach ($allgroup as $k => $v) {
            $selectgroup[$v['id']] = $v['name'];
        }

        //包
        $package = $this->pm->getPackage(array(), array('group_id', 'name', 'channel_id'));

        $group = $this->cgm->getChannelGroup(array(), array('id', 'name'));

        foreach ($group as $k => $v) {

            foreach ($package as $kk => $vv) {
                $channel_id = $vv['channel_id'];
                $sql = "select name from ny_channel where channel_id = '$channel_id'";
                $channel = $this->cgm->query($sql);
                if ($v['id'] == $vv['group_id']) {
                    $group[$k]['package'][$kk] = $vv['name'] . '-' . $channel[0]['name'];
                }
            }
        }

        $this->smarty->assign('selectgroup', $selectgroup);
        $this->smarty->assign('group', $group);
        $this->smarty->display('package/package.tpl');
    }

    public function ajax_package()
    {
        $group_id = $this->getParam('group');
        $package_id = $this->getParam('package');

        if (empty($group_id)) {
            $group_id = array();
        }
        if (empty($package_id)) {
            $package_id = array();
        }

        if ($package_id) {

            $sql = "select group_id ,name , channel_id from ny_package where package_id = $package_id ";

            $package = $this->cgm->query($sql);

            $sql = "select id ,name from ny_channel_group where id = " . $package[0]['group_id'];

            $group = $this->cgm->query($sql);

        } else if ($group_id && $package == '') {

            $sql = "select id ,name from ny_channel_group where id =$group_id ";

            $group = $this->cgm->query($sql);

            $sql = "select group_id , name ,channel_id from ny_package where group_id = " . $group[0]['id'];

            $package = $this->cgm->query($sql);

        } else {

            $package = $this->pm->getPackage(array(), array('group_id', 'name', 'channel_id'));

            $group = $this->cgm->getChannelGroup(array(), array('id', 'name'));

        }

        foreach ($group as $k => $v) {

            foreach ($package as $kk => $vv) {
                $channel_id = $vv['channel_id'];
                $sql = "select name from ny_channel where channel_id = '$channel_id'";
                $channel = $this->cgm->query($sql);
                if ($v['id'] == $vv['group_id']) {
                    $group[$k]['package'][$kk] = $vv['name'] . '-' . $channel[0]['name'];
                }
            }
        }
        $html .= '<div class="ui-tabs-panel">';
        $html .= '<div >';
        foreach ($group as $k => $v) {

            $html .= '<div class="w-box-list ">';
            $html .= '<h5>' . $v['name'] . '</h5>';
            $html .= '<ul>';
            foreach ($v['package'] as $kk => $vv) {
                $html .= ' <li> <span class="name">' . $vv . '</span> </li>';
            }
            $html .= '</ul>';
            $html .= ' </div>';
        }
        $html .= ' </div>';
        $html .= ' </div>';


        echo $html;


    }

    private static function trimAll($str)
    {
        $str = trim($str);
        $qian = array("", " ", "\t", "\n", "\r");
        $hou = array("", "", "", "");
        return str_replace($qian, $hou, $str);

    }
}