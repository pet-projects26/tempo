<?php
!defined('IN_WEB') && exit('Access Denied');

class PerhourController extends Controller{

    public function index()
    {
        $tabs = array(
            array('title' => '每小时数据统计', 'url' => 'admin.php?ctrl=perHour&act=perHour'),
            array('title' => '节点时间统计', 'url' => 'admin.php?&ctrl=perHour&act=node_time'),
        );
        $this->tabs($tabs);

    }

    //每小时统计数据
    public function perHour()
    {
        $header = '说明：<br>';
        $header .= '新增跳转数：即单位时间内新增连接上游戏服务器人数。每个账号从到达选服界面成功则标记一次，标记后该账号在本服的后续跳转不再算为新增跳转。 <br>';
        $header .= '每一步人数：即单位时间内新增跳转人数中，达到该步骤则标记一次，该账号多次连接不做重复标记。*每一步人数的括号内为每一步的平均加载时间<br>';
        $header .= '每一步成功率（与上一步比）  =  每一步人数 /上一步人数  *100%     精确到小数点后两位<br>';
        $header .= '每一步成功率（与初始人数比）  =  每一步人数 /新增跳转数  *100%     精确到小数点后两位<br>';
        $header .= '备注：第一步的上一步人数为新增跳转人数。<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器', '平台新增跳转数', '加载类库', '加载类库成功率(初始)', '所有类库加载完成', '所有类库加载完成(上步)', '所有类库加载完成(初始)', /*'Web可用', 'Web可用率(上步)', 'Web可用率(初始)',*/
            '开始加载配置文件', '开始加载配置文件成功率(上步)', '开始加载配置文件成功率(初始)', '开始连接服务器', '开始连接服务器成功率(上步)', '开始连接服务器成功率(初始)', '连接服务器成功', '连接服务器成功成功率(上步)', '连接服务器成功成功率(初始)', '创角跳转', '创角跳转成功率(上步)', '创角跳转成功率(初始)', '开始创角', '开始创角成功率(上步)', '开始创角成功率(初始)', '创角成功', '创角成功成功率(上步)', '创角成功成功率(初始)', '登录成功', '登录成功成功率(上步)', '登录成功成功率(初始)', '登录失败', '开始进入场景', '开始进入场景成功率(上步)', '开始进入场景成功率(初始)', '开始加载通用资源', '开始加载通用资源成功率(上步)', '开始加载通用资源成功率(初始)', '通用资源加载完成', '通用资源加载完成成功率(上步)', '通用资源加载完成成功率(初始)', '进入场景', '进入场景成功率(上步)', '进入场景成功率(初始)'));
        $this->setSource('perHour' , 'perHour_data' , 1);
        $this->setLimit('24');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function perHour_data()
    {
        $this->setCondition(0, 'eq', "create_time");
        $this->setCondition(1 , 'scp');

        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('perHour_data' , 'perHour_export');
        $this->call('NodedailyhourModel' , $method , $this->getConditions());
    }

    public function node_time()
    {
        $header = '说明：<br>';
        $header .= '新增跳转数：即单位时间内新增连接上游戏服务器人数。每个账号从到达选服界面成功则标记一次，标记后该账号在本服的后续跳转不再算为新增跳转。 <br>';
        $header .= '每一步人数：即单位时间内新增跳转人数中，达到该步骤则标记一次，该账号多次连接不做重复标记。<br>';
        $header .= '每一步加载平均时间.<br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '平台新增跳转数', '加载类库', '平均时间->', '所有类库加载完成', '平均时间->',
            '开始加载配置文件', '平均时间->', '开始连接服务器', '平均时间->', '连接服务器成功', '平均时间->', '创角跳转', '平均时间->', '开始创角', '平均时间->', '创角成功', '平均时间->', '登录成功', '平均时间->', '登录失败', '开始进入场景', '平均时间->', '开始加载通用资源', '平均时间->', '通用资源加载完成', '平均时间->', '进入场景'));
        $this->setSource('perHour', 'node_time_data', 1);
        $this->setLimit('24');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function node_time_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();

        $method = $this->setDataExport('node_time_data', 'node_time_export');
        $this->call('NodedailyhourModel', $method, $this->getConditions());
    }

}