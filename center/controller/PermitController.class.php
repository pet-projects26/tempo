<?php
!defined('IN_WEB') && exit('Access Denied');

class PermitController extends Controller{
    private $pa;
    private $uga;
    private $la;
    private $pm;
    private $ma;

    public function __construct(){
        parent::__construct();
        $this->pa = new PermitAction();
        $this->uga = new UsergroupAction();
        $this->la = new LogAction();
        $this->pm = new PackageModel();
        $this->ma = new MenuAction();
    }

    public function index(){
        $tabs = array(
            array('title' => '央服权限', 'url' => 'admin.php?ctrl=permit&act=setting&type=center'),
            //array('title' => '单服权限', 'url' => 'admin.php?ctrl=permit&act=setting&type=single'),
            array('title' => '用户组权限', 'url' => 'admin.php?ctrl=permit&act=usergroup')
        );
        $this->tabs($tabs);
    }

    public function setting(){
        $type = trim($_GET['type']);
        if(empty($type)){
            echo 'type error';
            return;
        }
        $list = $this->pa->permitTree($type);

        $menuRoot = $this->ma->menuRoot('tree');
//        var_dump($menuRoot);

        $this->smarty->assign('menuRoot', $menuRoot);
        $this->smarty->assign('list', $list);
        $this->smarty->assign('type', $type);
        $this->smarty->display('permit/permit_list.tpl');
    }

    public function permit_add(){
        $group   = trim($_POST['group']);
        $name    = trim($_POST['name']);
        $type    = trim($_POST['type']);
        $modules = textareaStrToArr($_POST['modules']);
        if(empty($group)){
            echo '分组不能为空';
            return;
        }
        if(empty($name)){
            echo '权限名称不能为空';
            return;
        }
        if(empty($type)){
            echo '请刷新页面重新添加权限';
            return;
        }

        $groupData = (new MenuAction())->menuGetOne($group);

        if (empty($groupData)) {
            echo '该分组不存在, 请重试';
            return;
        }

        $groupName = $groupData['name'];

        $result = $this->pa->permitAdd($type, $group, $groupName, $name, $modules);
        echo $result ? '添加成功' : '添加失败';
        $this->la->logAdd('添加权限:' . $name);
    }

    public function permit_delete(){
        $pid = intval($_POST['pid']);
        $result = $this->pa->permitDelete($pid);
        echo $result ? 'success' : 'fail';
        $this->la->logAdd('删除权限:' . $pid);
    }

    public function save_field(){
        $pid = intval($_POST['id']);
        $field = trim($_POST['field']);
        $value = trim($_POST['value']);
        if($pid <= 0){
            echo 'pid error';
            return;
        }
        $return = $this->pa->permitUpdate($pid , $field , $value);
        if($field == "modules"){
            $value = str_replace('\n' , ' | ' , $value);
        }

        //修改权限时, 顺便更新用户组权限
        $this->uga->updateAllGroup();

        echo $return ? $value : 'save error';
        $this->la->logAdd('修改权限：' . $field . '=>' . $value . '，权限id:' . $pid);
    }

    public function get_module_textarea(){
        $pid = intval($_GET['id']);
        $field = trim($_GET['field']);
        if($pid <= 0 || $field != "modules"){
            echo "param error";
            return;
        }
        echo $this->pa->permitModulesTextarea($pid);
        return;
    }

    public function usergroup(){
        $groupid = intval($_GET['groupid']);
        if(!$groupid){
            $userChannel = array();
        }
        else{
            $usergroup = $this->uga->groupGetOne($groupid);
            $userChannel = $usergroup['channel'] ?: array();
        }
        $groupList = $this->uga->groupGetList();
        $centerPermitList = $this->pa->permitTree('center');
        $channelList = $this->pm->getChannelPackage(true);
        foreach($channelList as $k => $v){
            if($userChannel == 'all' || $userChannel[$k] == 'all'){
                $channelList[$k]['checked'] = true;
            }
            else{
                $channelList[$k]['checked'] = false;
            }
            $p = array();
            foreach($v['package'] as $v1){
                if($userChannel == 'all' || $userChannel[$k] == 'all' || (!empty($userChannel[$k]) && in_array($v1, $userChannel[$k], true))){
                    $p[$v1] = true;
                }
                else{
                    $p[$v1] = false;
                }
            }
            $channelList[$k]['package'] = $p;
        }
        $this->smarty->assign('usergroup' , $usergroup);
        $this->smarty->assign('groupList' , $groupList);
        $this->smarty->assign('centerPermitList' , $centerPermitList);
        $this->smarty->assign('channelList' , $channelList);
        $this->smarty->display('permit/usergroup.tpl');
        ob_flush();
    }

    public function group_permit_save(){
        parse_str(file_get_contents('php://input'), $_POST);
        $groupid = intval($_POST['groupid']);
        if (!$groupid) {
            echo '用户组id错误';

            return;
        }
        $type = trim($_POST['type']);
        if (in_array($type, array("center", "single"))) {
            $result = $this->uga->permitUpdate($groupid, $type, $_POST['permits'], isset($_POST['permit_all']));
        } elseif ($type == "server") {
            $data = array(
                'groupid' => $groupid,
                'agent'   => $_POST['agent'] ? serialize($_POST['agent']) : '',
                'server'  => $_POST['server'] ? serialize($_POST['server']) : '',
            );
            $result = $this->uga->usergroupUpdate($data);
        } elseif ($type == 'channel') {
            if ($_POST['all']) {
                $channel = 'all';
            } else {
                $channel = $this->getParam('channel', []);
                foreach ($channel as $k => $v) {
                    if (isset($_POST[$k])) {
                        $channel[$k] = 'all';
                    }
                }

                $channel = json_encode($channel);
            }

            $data = array(
                'groupid' => $groupid,
                'channel' => $channel,
            );
            $result = $this->uga->usergroupUpdate($data);
        }
        echo $result ? '修改成功' : '修改失败';
        $this->la->logAdd("修改用户组权限:(groupid:{$groupid}) {$type}");
    }

    public function update_all_group(){
        echo $this->uga->updateAllGroup();
        $this->la->logAdd("更新所有用户组权限");
    }

    public function permit_synchronize()
    {
        $bool = $this->pa->permitSynchronize();
        $this->la->logAdd('一键同步Menu权限');

        if ($bool) {
            echo json_encode(1);
        } else {
            echo json_encode(0);
        }
    }
}
