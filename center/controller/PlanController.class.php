<?php
!defined('IN_WEB') && exit('Access Denied');

class PlanController extends Controller{
    private $zm;

    public function __construct(){
        parent::__construct();
        $this->p = new PlanModel();
        $this->cgm = new ChannelgroupModel();
        $this->zm = new ZoneModel();
        $this->sm = new ServerModel();
        $this->scm = new ServerconfigModel();
    }
	
	public function index(){
		$tabs = array(
            array('title' => '开服列表', 'url' => 'admin.php?&ctrl=plan&act=record'),
            array('title' => '添加开服计划', 'url' => 'admin.php?&ctrl=plan&act=add'),
            array('title' => '编辑开服计划', 'url' => 'admin.php?&ctrl=plan&act=edit'),
        );
        $this->tabs($tabs);
	}

	/**
	 * [record 新服列表]
	 * @return [type] [description]
	 */
	public function record(){
		$this->setSearch('名称');
		$this->setSearch('标识符');
		$this->setSearch('创建时间','range_time');

		$this->setFields(array( '服号' , '渠号',  '名称' ,  '标识符' ,'配置' , '开服时间' ,'创建时间' ,'操作'));
        $this->setSource('plan' , 'record_data');
		$js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=plan&act=edit&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=plan&act=edit&id=' + id);
            }
            function del(id){
				if(confirm('确定删除？')){
					$.post('admin.php?ctrl=plan&act=del&id=' + id,'',function(data){
						if(data >0){

							$.dialog.tips('删除成功');
							\$tabs.tabs('load' , 0);

						}else{

							$.dialog.tips('删除失败，请重试');
						}
						
					});
				}
            }

            function config(id){
            	if(confirm('确定配置？')){
					$.post('admin.php?ctrl=plan&act=config&id=' + id,'',function(data){
						if(data ==0){
							$.dialog.tips('配置成功');
							\$tabs.tabs('load' , 0);
						}
						if(data ==1){
							$.dialog.tips('参数错误');
						}
						if(data ==2){
							$.dialog.tips('配置失败');
						}
					});
				}
            }
            
END;
        $this->setJs($js);
        $this->displays();

	}

	public function record_data(){
		$this->setCondition(0 , 'like' , 'name');
		$this->setCondition(1 , 'like' , 'server_id');
		$this->setCondition(2 , 'date' , 'create_time');
		$this->setOrder('create_time');
		$this->setLimit();
        $this->call('PlanModel' , 'record_data' , $this->getConditions());
	}
	/**
	 * [del 删除开服计划]
	 * @return [type] [description]
	 */
	public function del(){

		$id = $this->getParam('id');

		if($id){
			(new LogAction())->logAdd("删除开服计划：{$id}");
			echo $this->p->delete(array('id' => $id));	
		}	
	}
	/**
	 * [add 添加开服计划页面]
	 */
	public function add(){
		//渠道组
		$res = $this->cgm->getChannelGroupByTrait(array());
        $group = array();
        foreach ($res as $row) {
            $groupId = $row['id'];
            $group[$groupId] = $row;
        }

        //分区
        $zone = $this->zm->getZone(array());
        $this->smarty->assign('zone' , $zone);
        

		$this->smarty->assign('zone' , $zone);
        $this->smarty->assign('group' , $group);
		$this->smarty->display('plan/add.tpl');
	}
	/**
	 * [add_action 添加开服计划]
	 */
	public function add_action(){
		$group = $this->getParam('group');
		$num = $this->getParam('num');
		$server_id = $this->getParam('server_id');
		$name = $this->getParam('name');
		$zone = $this->getParam('zone');
		$open_time = $this->getParam('open_time');
		$review = $this->getParam('review');//提审
		$display_time = $this->getParam('display_time');//自动显示时间
		
		$tips = $this->getParam('tips');//未开服提醒

		$result = $this->p->getRow(array(),array('server_id' => $server_id));
		$rs = $this->sm->getRow(array(),array('server_id' => $server_id));

		if(!empty($result) || !empty($rs)){

            $json = array('msg' => '服务器标识已存在' , 'code' => 0);
            exit(json_encode($json));
        }

        $numres = $this->p->getRow(array(),array('num' => $num , 'group_id' => $group));
        
        $numrs  = $this->sm->getRow(array(),array('num' => $num , 'group_id' => $group));
        
        if(!empty($numres) || !empty($numrs)){

            $json = array('msg' => '该渠道组已存在此服号' , 'code' => 0);
            exit(json_encode($json));
        }


		//开服计划，允许不填写开服时间，为0
		if($open_time != 0) {
			if (strtotime($open_time) <= time()) {
				$json = array('msg' => '开服时间不能小于当前时间', 'code' => 0);
				exit(json_encode($json));
			}
		}
		$open_time = $open_time == 0?0:strtotime($open_time);
		$display_time = empty($display_time)?0:strtotime($display_time);

		if($display_time && $display_time > $open_time){
			$json = array('msg'=>'服务器显示时间不能大于开服时间','code'=>0);
			exit(json_encode($json));
		}

        $data = array(
        	'group_id' => $group,
        	'num' => $num,
        	'server_id' => $server_id,
        	'name' => $name,
        	'zone' => $zone,
			'review'=>(int)$review,
        	'open_time' => $open_time,
        	'create_time' => time(),
			'display_time'=>$display_time,
        	'tips'=>$tips,
        );

        
        
        if($this->p->add($data)){
        	(new LogAction())->logAdd("添加开服计划：{$server_id}");
        	$json = array('msg' => '添加成功' , 'code' => 200);
            exit(json_encode($json));
        	
        }else{

        	$json = array('msg' => '添加失败，请重试' , 'code' => 0);
            exit(json_encode($json));
        }

	}
	/**
	 * [edit 编辑开服计划]
	 * @return [type] [description]
	 */
	public function edit(){

		$id = $this->getParam('id');

		if(empty($id)){
			die('请选择要编辑的开服计划');
		}

		$rs = $this->p->getrow(array() , array('id' => $id));
		$rs['open_time'] = date('Y-m-d H:i:s' , $rs['open_time']);
		$rs['display_time'] = date('Y-m-d H:i:s',$rs['display_time']);
		//渠道组
		$res = $this->cgm->getChannelGroupByTrait(array());
        $group = array();
        foreach ($res as $row) {
            $groupId = $row['id'];
            $group[$groupId] = $row;
        }

        //分区
        $zone = $this->zm->getZone(array());
        $this->smarty->assign('zone' , $zone);
        
        $this->smarty->assign('rs' , $rs);
		$this->smarty->assign('zone' , $zone);
        $this->smarty->assign('group' , $group);
		$this->smarty->display('plan/edit.tpl');
	}

	/**
	 * [edit_action 编辑开服计划]
	 */
	public function edit_action(){
		$group = $this->getParam('group');
		$num = $this->getParam('num');
		$server_id = $this->getParam('server_id');
		$name = $this->getParam('name');
		$zone = $this->getParam('zone');
		$open_time = $this->getParam('open_time');
		$id = $this->getParam('id');
		$review = $this->getParam('review');
		
		$tips = $this->getParam('tips');//未开服提醒
		
		$sql ="select id from ny_server_plan where server_id = '$server_id' and id != $id";
		$result = $this->p->query($sql);

		$rs = $this->sm->getRow(array(),array('server_id' => $server_id));

		if(!empty($result) || !empty($rs)){

            $json = array('msg' => '服务器标已存在' , 'code' => 0);
            exit(json_encode($json));
        }       

        if(strtotime($open_time) <= time()){
        	$json = array('msg' => '开服时间不能小于当前时间' , 'code' => 0);
            exit(json_encode($json));
        }

        $data = array(
        	'group_id' => $group,
        	'num' => $num,
        	'server_id' => $server_id,
        	'name' => $name,
        	'zone' => $zone,
			'review'=>$review,
        	'open_time' => strtotime($open_time),
        	'create_time' => time(),
        	'tips'=>$tips,
        );
       
        if($this->p->update( $data ,array('id' => $id) ) !== false){
        	(new LogAction())->logAdd("编辑开服计划：{$server_id}");
        	$json = array('msg' => '编辑成功' , 'code' => 200);
            exit(json_encode($json));
        	
        }else{

        	$json = array('msg' => '编辑失败，请重试' , 'code' => 0);
            exit(json_encode($json));
        }
	}

	/**
	 * [config description]
	 * @return [type] [description]
	 */
	public function config(){
		$id = $this->getParam('id');
		if(empty($id)){
			exit(1);
			//exit(json_encode(array('msg' => '参数错误' , 'code' => 0)));
		}
		$fields = array('num' , 'group_id' ,'server_id' ,'name' , 'zone' ,'review','open_time','display_time','tips' );

		$data = $this->p->getRow($fields , array('id' => $id));
		$data['channel_num'] = $data['group_id'];
		$data['status'] = 0;//未开服
		$data['type'] = 1 ;//正常服
		$data['display'] = 0 ;//正常服
		$rs = $this->sm->add($data);

		$this->scm->add(array('server_id' => $data['server_id']));

		
		if($rs){

			//更新server_plan表的状态
			
			$this->p->update(array('is_config'=>1) , array('id' => $id));
			
			exit(2);
        	//$json = array('msg' => '配置成功' , 'code' => 200);
            //exit(json_encode($json));
        	
        }else{
        	exit(0);
        	//$json = array('msg' => '配置失败，请重试' , 'code' => 0);
            //exit(json_encode($json));
        }

	}


}