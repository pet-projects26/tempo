<?php

class PushController extends Controller{
    private $pm;
    private $am;

    public function __construct(){
        parent::__construct();
        $this->pm = new PushModel();
        $this->am = new AppModel();
    }

    //错误类型
    public static $errno = array(
        1000 => '系统内部错误',
        1001 => '只支持 HTTP Post 方法',
        1002 => '缺少了必须的参数',
        1003 => '参数值不合法',
        1004 => '验证失败',
        1005 => '消息体太大',
        1008 => 'app_key 参数非法',
        1011 => '没有满足条件的推送目标',
        1020 => '只支持 HTTPS 请求',
        1030 => '内部服务超时'
    );

    //消息推送（菜单）
    public function index(){
        $tabs = array(
            array('title' => '发送通知', 'url' => 'admin.php?ctrl=push&act=notification'),
            array('title' => '推送记录', 'url' => 'admin.php?ctrl=push&act=record'),
            array('title' => '应用列表', 'url' => 'admin.php?ctrl=app&act=record'),
            array('title' => '添加应用', 'url' => 'admin.php?ctrl=app&act=add'),
            array('title' => '编辑应用', 'url' => 'admin.php?ctrl=app&act=edit')
        );
        $this->tabs($tabs);
    }

    //推送记录（子菜单）
    public function record(){
        $this->setSearch('平台' , 'select' , CDict::$platform);
        $this->setSearch('日期' , 'range_date');
        $this->setField('发送时间' , 150);
        $this->setFields(array('内容' , 'IOS成功数' , 'Android成功数' , '错误信息'));
        $this->setSource('push' , 'record_data' , 1);
        $this->displays();
    }
    public function record_data(){
        $this->setCondition(0 , 'eq' , 'type');
        $this->setCondition(1 , 'between' , 'create_time' , implode('|' , array(strtotime('-1 month') , time())));
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->getParam('export') ? 'record_export' : 'record_data';
        $this->call('PushModel' , $method , $this->getConditions());
    }

    //发送通知（子菜单）
    public function notification(){
        if($this->getParam('submit')){
            $app = $this->getParam('app');
            $content = $this->getParam('content');
            echo $this->push(array('app' => $app , 'content' => $content , 'type' => 'notification'));
            //(new LogAction())->logAdd('发送通知');
        }
        else{
            unset(CDict::$platform[0]);
            $apps = $this->am->getApp(array() , array('app_id' , 'name' , 'platform'));
            $this->smarty->assign('apps' , $apps);
            $this->smarty->assign('platform' , CDict::$platform);
            $this->smarty->display('push/notification.tpl');
        }
    }

    public function push($notice){
        $content = json_decode($notice['content'] , true);
        !$content && exit(json_encode(array('msg' => '未填写推送内容')));
        $data = array();
        $platform = $content['platform'];
        $platform_flag = 0;
        if($platform == 'all'){
            $data['platform'] = 0;
        }
        else if(is_array($platform)){
            in_array('android' , $platform) && $platform_flag = 1;
            in_array('ios' , $platform) && $platform_flag += 2;
            $data['platform'] = $platform_flag == 3 ? 0 : $platform_flag;
        }
        if($notice['type'] == 'notification'){
            if(empty($content['notification']['android'])){
                unset($content['notification']['android']);
            }
            if(empty($content['notification']['ios'])){
                unset($content['notification']['ios']);
            }
            $data['type'] = 0;
        }

        if(isset($content['cron'])){
            $data['cron'] = 1;
            $data['cron_time'] = trim($content['cron']);
            unset($content['cron']);
        }
        else{
            $data['cron'] = 0;
        }
        $data['app'] = $notice['app'];
        $data['object'] = json_encode($content);
        $data['create_time'] = time();
        $data['last_modify_time'] = $data['create_time'];
        $id = $this->pm->addPush($data);
        if(!$data['cron'] && $id){
            return $this->send($id);
        }
        echo json_encode(array('msg' => '已添加'));
    }

    public function send($id){
        $rs = $this->pm->getPush($id);
        if(!$rs){
            return $id . ' not exists';
        }
        $app = $this->am->getSecret($rs['app']);
        $jpush = new JPush();
        $jpush->app_key = $app['app_id'];
        $jpush->master_secret = $app['secret'];
        $result = json_decode($jpush->send($rs['object']) , true);
        $msg_id = isset($result['msg_id']) ? $result['msg_id'] : 'null';
        $errno  = 0;
        $errstr = '';
        if(isset($result['error'])){
            $errno  = $result['error']['code'];
            $errstr = $result['error']['message'];
        }
        $data = array(
            'msg_id' => $msg_id,
            'errno' => $errno,
            'errstr' => $errstr,
            'cron' => $rs['cron'] ? 2 : 0,
            'last_modify_time' => time()
        );
        $this->pm->editPush($data , $id);
        return $errno ? self::$errno[$errno] . '(' . $errstr . ')' : '成功';
    }

    //定时推送
    public function cron(){
        $rs = $this->pm->getCron();
        foreach($rs as $row){
            $this->send($row['id']);
        }
    }

    //收到消息的用户数
    public function received(){
        $rs = $this->pm->getReceived();
        $app_msg_ids = array();
        foreach($rs as $row){
            $app_msg_ids[$row['app']][] = $row['msg_id'];
        }
        if(!$app_msg_ids){
            return false;
        }
        $jpush = new JPush();
        foreach($app_msg_ids as $app => $msg_ids){
            $rs = $this->am->getSecret($app);
            $jpush->app_key = $rs['app_id'];
            $jpush->master_secret = $rs['secret'];
            $result = array();
            if(count($msg_ids) <= 100){
                $result = $jpush->report($msg_ids);
            }
            else{
                $chunk = array_chunk($msg_ids , 100);
                foreach($chunk as $c){
                    $result += $jpush->report($c);
                }
            }
            if(!$result){
                return false;
            }
            print_r($result);
            foreach($result as $k => $v){
                $data = array(
                    'android_received' => $v['android'],
                    'ios_received' => $v['ios']
                );
                $this->pm->editPush($data , '' , $k , $app);
            }
        }
    }
}
