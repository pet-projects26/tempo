<?php
!defined('IN_WEB') && exit('Access Denied');

class RankController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->sm = new ServerModel();
    }

    public function record()
    {

        $servers = $this->sm->getServerByTrait();
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach ($channelGroups as $key => $val) {
            $groups[$val['id']] = $val['name'];
        }

        $this->smarty->assign('servers', $servers);
        $this->smarty->assign('groups', $groups);

        $this->smarty->assign('rankType', CDict::$rankType);

        $this->smarty->display('rank/record.tpl');
    }

    public function record_data()
    {
        $server = $this->getParam('server');
        $type = $this->getParam('type');

        //根据role_id获取信息 redis 从单服获取
        $data = $this->call('RankRedis', 'getTypeRankData', array('type' => $type), $server);

        $data = $data[$server];

        if ($data['errno']) {
            echo json_encode($data);
            exit;
        }

        $contents = '';

        foreach ($data as $k => $v) {
            $rank = $k + 1;
            $v['tm'] = date('Y-m-d H:i:s', $v['tm'] / 1000);
            $contents .= ' <tr><td>' . $v['name'] . '</td><td>' . $v['vip'] . '</td><td>' . $v['vipExp'] . '</td><td>' . $rank . '</td><td>' . $v['param'] . '</td><td>' . $v['tm'] . '</td></tr>';
        }

        echo json_encode($contents);
    }

    /*
    public function getRecord_data($server,$type,$time){
        $conditions=array();
        $ServerModel=new ServerModel();
        $conditions['WHERE']['type']=$type;
        $conditions['WHERE']["FROM_UNIXTIME(date,'%Y-%m-%d %H')"]= date('Y-m-d H',$time);
        $conditions['Extends']['LIMIT']=100;
        $conditions['Extends']['ORDER']=array('rank#asc');
        $result=$ServerModel->call('Rank','getRankData',array('conditions' => $conditions),$server);
        $result = $result[$server];
        $contents = '';
        foreach($result as $k=>$v){
            $contents.=' <tr><td>'.$v['name'].'</td><td>'.$v['rank'].'</td><td>'.$v['contents'].'</td></tr>';
        }
        return  $contents;
    }
    public function getdata(){
        $server=$this->getParam('server');
        $time=$this->getParam('time');
        $type=$this->getParam('type');

        $conditions=array();
        $ServerModel=new ServerModel();
        $conditions['WHERE']['type']=$type;
        $conditions['WHERE']["FROM_UNIXTIME(date,'%Y-%m-%d %H')"]= date('Y-m-d H',$time);
        $conditions['Extends']['LIMIT']=100;
        $conditions['Extends']['ORDER']=array('rank#asc');
        $result=$ServerModel->call('Rank','getRankData',array('conditions' => $conditions),$server);
        $result = $result[$server];
        $contents = '';
        foreach($result as $k=>$v){
            $contents.=' <tr><td>'.$v['name'].'</td><td>'.$v['rank'].'</td><td>'.$v['contents'].'</td></tr>';
        }
        echo  json_encode($contents);
    }*/

    public function getRankType()
    {
        echo json_encode(CDict::$rankType);
    }
}

;