<?php
!defined('IN_WEB') && exit('Access Denied');

class RanklistController extends Controller{
	
	
	public function index(){
		$tabs2 = array(
				array('title' => '等级排行' , 'url' => 'admin.php?&ctrl=ranklist&act=record&type=0'),
				array('title' => '战力排行' , 'url' => 'admin.php?&ctrl=ranklist&act=record&type=1'),
				array('title' => '付费排行' , 'url' => 'admin.php?&ctrl=ranklist&act=record&type=2'),
		);
		$this->tabs($tabs2);
	}

    public function record()
    {
        $type = $this->getParam('type');

        $header = '说明：<br>';
        $header .= '名次：由1开始后推，统计前100名 <br>';
        $header .= '等级：按截止到所选日期，该服全部玩家的等级进行排序，从大到小排，同级别则先达到则靠前排。<br>';
        $header .= '玩家名：该名次的玩家名称，含服务器标识<br>';
        $header .= '仙盟：该玩家的仙盟名称<br>';
        $header .= '战力：该玩家的战力<br>';
        $header .= '充值金额：该玩家已充值的金额（人民币）<br>';
        $header .= '剩余元宝：该玩家身上的元宝数<br>';
        $header .= '离线时长：该玩家的离线时长，以天为单位。<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));

        switch ($type) {
            case 0:
                $this->setFields(array('名次', '等级' , '玩家名' , '仙盟' , '战力', '充值金额', '剩余元宝', '离线时长', '统计时间', '服务器'));
                break;
            case 1:
                $this->setFields(array('名次', '战力' , '玩家名' , '仙盟' , '等级', '充值金额', '剩余元宝', '离线时长', '统计时间', '服务器'));
                break;
            case 2:
                $this->setFields(array('名次', '充值金额' , '玩家名' , '仙盟' , '战力', '等级', '剩余元宝', '离线时长', '统计时间', '服务器'));
                break;
            default:
                $type = 0;
                $this->setFields(array('名次', '等级' , '玩家名' , '仙盟' , '战力', '充值金额', '剩余元宝', '离线时长', '统计时间', '服务器'));
                break;
        }

        $this->setSource('Ranklist' , 'record_data' , 1, ['type' => $type]);
        $this->setLimit('24');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'eq', "create_time");
        $this->setCondition(1 , 'scp');
        $this->setCondition(2 , 'eq' , 'type' ,$this->getParam('type'));
//        $this->setOrder('time' , 'asc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('RanklistModel' , $method , $this->getConditions());
    }

}