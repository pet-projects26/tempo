
<?php

!defined('IN_WEB') && exit('Access Denied');

class RegisterController extends Controller{
    //注册数据（菜单）
    public function index(){
        $tabs = array(
            //array('title' => '设备数' , 'url' => 'admin.php?&ctrl=client&act=device'),
           array('title' => '注册数' , 'url' => 'admin.php?&ctrl=register&act=record'),
            array('title' => '创角数' , 'url' => 'admin.php?&ctrl=stat&act=create'),
            //array('title' => '创角数' , 'url' => 'admin.php?&ctrl=role&act=create'),
            array('title' => '创角数分析' , 'url' => 'admin.php?&ctrl=role&act=create_analysis'),
			
			
        );
        $this->tabs($tabs);
    }

    //注册数（菜单）
    public function record(){
		$header = '1.注册数 指当天创号成功的玩家数量(包括提审服,不包括老玩家,一个账号只计算一次)';
		$this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 1 , 0 , 0));
        $this->setField('日期');
        $this->setField('注册数');
		$this->setDataTableId('register');
        $this->setSource('register' , 'record_data' , 1);
		$js = <<< END
   			$(function () {
				$('#register tr').die().live("click", function () {
					if (\$register.fnIsOpen(this))
						\$register.fnClose(this)
					else {
						var elem = \$(this).find('.data')
						var postData = new Object
						postData.date = elem.attr('data-date')
						postData.channel = elem.attr('data-channel')
						var tr = this
						$.post('admin.php?ctrl=register&act=regDetail', postData, function (data) {
						   \$register.fnOpen(tr, data)
						})
					}
				})
			})

END;
		$this->setJs($js);
        $this->displays();
    }
    public function record_data(){
        $this->setCondition(0 , 'date' , 'unix_timestamp(date)');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date','desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('RegistercountModel' , $method , $this->getConditions());
    }
	//获取当前日期注册数的详细信息
	public function regDetail(){
        $m = new RegistercountModel();
		if(!empty($_POST['channel'])){
			$channel=$_POST['channel'];
			$channel=explode(',',$channel);
			$channel="'" . implode("','",$channel) . "'";	
		}else{
			$channel='';	
		}
        $data = $m->getDetail($_POST['date'],$channel);
        $this->smarty->assign('data', $data);
        echo $this->smarty->fetch('online/reg_detail.tpl');
    }
}