<?php
!defined('IN_WEB')&&exit('Access Denied');

/** 提审 **/
class ReviewController extends Controller{

    private static $_instance = null;

    public static function get_instance(){
        if(is_null(self::$_instance)){
            self::$_instance = new ChannelgroupModel();
        }
        return self::$_instance;
    }

    /**
     * 菜单
    */
    public function index(){

        $tabs = array(
            array('title'=>'渠道版本号','url'=>'admin.php?&ctrl=review&act=lst'),
            //array('title'=>'编辑渠道','url'=>'admin.php?&ctrl=review&act=edit'),
        );
        $this->tabs($tabs);
    }

    /**
     * 列表，显示所有已经提审的渠道
     * @version 2017-09-18
    */
    public function lst(){
       
        $this->setSearch('渠道组名称','input');
        $this->setFields(array('渠道组ID','名称','版本号','提审服版本号'));//'提审日起',
        $this->setSource('review','get_list',0);
        $this->setOrder();
       
        $this->displays();
    }
    public function get_list(){
        
        $this->setCondition(0,'like','name');
        $this->setLimit();
        $this->call('ChannelgroupModel','get_list',$this->getConditions());
    }

    /**
     * 获取所有渠道组
     * @version 2017-09-18
    */
    public function add(){
        $channel_id = $this->getParam('id');
        $channel_id = intval($channel_id);
        $model = self::get_instance();
        if($channel_id){
            $version = $model->getChannelGroup($channel_id,array('version'));
            $version = array_shift($version);
            $this->smarty->assign('channel_id',$channel_id);
            $this->smarty->assign('version',$version);
        }
        $data = $model->getChannelGroupByTrait(array(),array('id','name','version'));

        $channelGroupRow = array();
        foreach ($data as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $group_id = array();
        foreach($data as $val){
            array_push($group_id,$val['id']);
        }
        $rv_name = CDict::$rv_name;
        $rv_param = CDict::$rv_param;
        $this->smarty->assign('rv_param',$rv_param);
        $this->smarty->assign('rv_name',$rv_name);
        $packageData = (new PackageModel())->getPackagesByChannelGroup($group_id,array('p.group_id','p.game_name','g.name','p.channel_id','p.package_id','p.review_num'));
        if(empty($packageData)) die('未找到合适的IOS包');
        $this->smarty->assign('groups',$channelGroupRow);
        $this->smarty->assign('packages',$packageData);
        //$this->smarty->assign('data',$data);
        $this->smarty->display('channelgroup/review.tpl');
    }

    //填写属于渠道的版本号，用于跨服和维护
    public function add_review(){
        $id = $this->getParam('id');
        $model = self::get_instance();
        $data = $model->getChannelGroupByTrait(array(),array('id','name','version'));
        $channelGroupRow = array();
        foreach ($data as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        if(!empty($id)){
            $conditions['WHERE']['id'] = $id;
            $res = $model->getRow(array('version'),$conditions['WHERE']);
            $this->smarty->assign('edit_id',$id);
            $this->smarty->assign('version',$res['version']);
        }
        $this->smarty->assign('groups',$channelGroupRow);
        $this->smarty->display('channelgroup/review_ser.tpl');
    }

    public function add_review_handel(){
        $group_id = $this->getParam('channel_group');//得到渠道组
        $review_num = $this->getParam('review_num');//
        empty($group_id[0]) && $group_id = array();
        empty($package_id[0]) && $package_id = array();
        $data = array(
            'group_id'=>$group_id,
            'review_num'=>self::trimAll($review_num)
        );

        $model = self::get_instance();
        $res = $model->set_serType($data);

        if($res>0){
            exit(json_encode(['code'=>1,'msg'=>'编辑成功'],JSON_UNESCAPED_UNICODE));
        }else{
            exit(json_encode(['code'=>0,'msg'=>'编辑失败'],JSON_UNESCAPED_UNICODE));
        }
    }

    public function edit_review(){

    }

    /**
     * 编辑渠道组，设置为提审
     * @version 2017-09-18
    */
    public function add_handel(){
        $group_id = $this->getParam('channel_group');//得到渠道组
        $package_id = $this->getParam('package_id');//得到包号
        $review_num = $this->getParam('review_num');//提审版本号
        $param = $this->getParam('param');
        empty($group_id[0]) && $group_id = array();
        empty($package_id[0]) && $package_id = array();
        $data = array(
            'group_id'=>$group_id,
            'package_id'=>$package_id,
            'review_num'=>self::trimAll($review_num),
            'rv_param'=>$param,
        );
        $model = self::get_instance();
        $res = $model->set_rvtype($data);

        if($res>0){
            exit(json_encode(['code'=>1,'msg'=>'编辑成功'],JSON_UNESCAPED_UNICODE));
        }else{
            exit(json_encode(['code'=>0,'msg'=>'编辑失败'],JSON_UNESCAPED_UNICODE));
        }
    }

    private static function trimAll($str){
        $str = trim($str);
        $qian = array(""," ","\t","\n","\r");
        $hou = array("","","","");
        return str_replace($qian,$hou,$str);

    }
}
?>