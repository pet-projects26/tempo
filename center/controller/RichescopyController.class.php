<?php
!defined('IN_WEB') && exit('Access Denied');

class RichesCopyController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启天降财宝的人数。<br>';
        $header .= '参与人数：该日进行过天降财宝（采集数≥1）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均采集次数 = 该日全服每个玩家采集数之和 / 参与人数   （确定到小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '人均采集次数'));
        $this->setSource('RichesCopy', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('RichesCopyModel', $method, $this->getConditions());
    }
}