<?php
!defined('IN_WEB') && exit('Access Denied');

class RoleController extends Controller
{
    private $ro;

    public function __construct()
    {
        parent::__construct();
        $this->variableAction = new VariableAction();
        //日志记录
        $this->logAction = new LogAction();
        $this->ro = new RoleModel();
        $this->sm = new ServerModel();
    }

    //角色统计（菜单）
    public function index()
    {
        $tabs = array(
            array('title' => '角色列表', 'url' => 'admin.php?&ctrl=role&act=record'),
            array('title' => '基本信息', 'url' => 'admin.php?&ctrl=role&act=attr&type=1'),
            array('title' => '流水查询', 'url' => 'admin.php?&ctrl=role&act=attr&type=2'),
            array('title' => '背包', 'url' => 'admin.php?&ctrl=role&act=attr&type=3'),
            array('title' => '职业统计', 'url' => 'admin.php?&ctrl=role&act=career'),
            array('title' => '等级统计', 'url' => 'admin.php?&ctrl=role&act=level')
        );
        $this->tabs($tabs);
    }

    //角色列表（子菜单）
    public function record()
    {
        $this->setSearch('角色名');
        $this->setSearch('角色ID');
        $this->setSearch('账号');
        $this->setSearch('包号');
        $this->setSearch('创号时间', 'range_time');
        $career = array('' => '全部') + CDict::$career[1] + CDict::$career[2];
        $this->setSearch('职业', 'select', $career);
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('角色ID', 100);
        $this->setField('角色名', 120);
        $this->setField('账号');
        $this->setField('包号');
        $this->setField('职业');
        $this->setField('等级', 60);
        $this->setField('元宝', 100);
        $this->setField('创号时间', 200);
        $this->setField('最后登录时间', 200);
        $this->setField('最后退出时间', 200);
        $this->setField('ip', 200);
        $this->setField('操作', 250);

        $this->setSource('role', 'record_data', 1);
        $js = <<< END
		
         function attr(type , id){
            $('#tabs').tabs('url' , 1 , 'admin.php?ctrl=role&act=attr&type=1&id=' + id);
            $('#tabs').tabs('url' , 2 , 'admin.php?ctrl=role&act=attr&type=2&id=' + id);
            $('#tabs').tabs('url' , 3 , 'admin.php?ctrl=role&act=attr&type=3&id=' + id);
        	$('#tabs').tabs('select' , 1);
        }
END;
        $this->smarty->assign('select', 'radio');

        $this->setJs($js);
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'like', 'name');
        $this->setCondition(1, 'eq', 'role_id');
        $this->setCondition(2, 'eq', 'account');
        $this->setCondition(3, 'eq', 'package');
        // $this->setCondition(3 , 'between' , 'level');
        $this->setCondition(4, 'time', 'create_time');
        $this->setCondition(5, 'eq', 'career');
        $scp = $this->getCookie('scp');
        $scp = json_decode($scp, true);
        if ($scp[2]) {
            $this->setCondition(7, 'eq', 'server::IN', $scp[2][0]);
            $_SESSION['server'] = $scp[2][0];
        } else {
            $this->setCondition(7, 'scp');
        }
        $this->setOrder();
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export2');
        $this->call('RoledailyModel', $method, $this->getConditions());
//         $this->call('RoledailyModel' , 'record_data' , $this->getConditions());
    }

    public function attr()
    {

        $type = $this->getParam('type');
        $id = $this->getParam('id');
        $_SESSION['roleid'] = $id;
        empty($id) && die('请选择查询的角色列表中的查看按钮');
        if ($type && $id) {
            switch ($type) {
                case 1:
                    $this->baseinfo();
                    break;
                case 2:
                    $this->log();
                    break;
                case 3:
                    $this->bag();
                    break;
            }
        }
    }

    //基本信息
    public function baseinfo()
    {

        $id = $_SESSION['roleid'];
        $server = $_SESSION['server'];
        if (!$id) {
            echo '请选择查询的角色列表中的查看按钮';
            exit;
        }
        $info = $this->call('RoleModel', 'baseinfo', array('id' => $id), $server);


        $info = $info[$server];

        if (isset($info['rename'])) {
            $info['rename'] = implode(",", $info['rename']);
        }

        $info['create_tm'] = date('Y-m-d H:i:s', $info['create_tm']);

        $occ = $info['occ'];//职业
        $career = CDict::$career;
        $i = substr($occ, 0, 1);
        $info['occ'] = $career[$i][$occ];

        //客户端信息
        $client = new ClientModel();
        $clientinfo = $client->getClientByrole($id);
        $info['channel'] = $clientinfo['channel'];//渠道
        $info['package'] = $clientinfo['package'];//包

        //身上装备的信息
        $equip_sql = "select item_id ,part from ny_wear_equip where role_id = $id and item_id > 0 group by part order by create_time desc";
        //身上装备的宝石信息
        $gem_sql = "select item_id ,part from ny_wear_gem where role_id = $id  group by part order by create_time desc";
        //坐骑魂石
        $ride_stone_sql = "select count(*) as count ,item_id from ny_consume_produce where item_id in (3240020010001 ,3240030010002 , 3240040010003 , 3240040010004 , 3240050010005 , 3240050010006 ,3240050010007 ) and role_id  = $id group by item_id ";

        //法器魂石
        $multiplier_stone_sql = "select count(*) as count ,item_id from ny_consume_produce where item_id in (3230030010001 , 3230040010001 , 3230050010001 ) and role_id  = $id group by item_id ";

        $data = array(
            'equip' => $equip_sql,
            'ride' => $ride_stone_sql,
            'multiplier' => $multiplier_stone_sql,
            'gem' => $gem_sql,
        );

        $arr = array('query' => array('data' => $data));
        $res = (new ServerconfigModel())->makeHttpParams(array("$server"), $arr);

        $urls = $res['urls'];
        $param = $res['params'];
        $res = Helper::rolling_curl($urls, $param);

        $data = json_decode($res[$server], true);

        $data = $data['data']['query'];

        $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";
        $servera = $this->sm->query($sql);

        $AllItem = $this->call('GiftModel', 'getItem', array(), $servera[0]['server_id']);//全部物品

        $item = $AllItem[$servera[0]['server_id']];
        //print_r($item);exit;
        //装备
        if (!empty($data['equip'])) {
            foreach ($data['equip'] as $k => $v) {
                $item_id = trim($v['item_id']);
                $equip[$v['part']] = $item[$item_id];
            }
        }
        //坐骑魂石
        if (!empty($data['ride'])) {
            $gongji = 0;
            $shengming = 0;
            $fangyu = 0;
            foreach ($data['ride'] as $k => $v) {
                switch ($v['item_id']) {
                    case '3240020010001':
                        $gongji += 30 * $v['count'];
                        $fangyu += 10 * $v['count'];
                        $shengming += 420 * $v['count'];
                        break;
                    case '3240030010002':
                        $gongji += 75 * $v['count'];
                        $fangyu += 20 * $v['count'];
                        $shengming += 1110 * $v['count'];
                        break;
                    case '3240040010003':
                        $gongji += 90 * $v['count'];
                        $fangyu += 25 * $v['count'];
                        $shengming += 1320 * $v['count'];
                        break;
                    case '3240040010004':
                        $gongji += 105 * $v['count'];
                        $fangyu += 30 * $v['count'];
                        $shengming += 1580 * $v['count'];
                        break;
                    case '3240050010005':
                        $gongji += 265 * $v['count'];
                        $fangyu += 70 * $v['count'];
                        $shengming += 3940 * $v['count'];
                        break;
                    case '3240050010006':
                        $gongji += 450 * $v['count'];
                        $fangyu += 115 * $v['count'];
                        $shengming += 6750 * $v['count'];
                        break;
                    case '3240050010007':
                        $gongji += 450 * $v['count'];
                        $fangyu += 90 * $v['count'];
                        $shengming += 7200 * $v['count'];
                        break;

                    default:
                        # code...
                        break;
                }

            }
            $ride = '攻击力+' . $gongji . '  防御+' . $fangyu . '  生命+' . $shengming;

        } else {
            $ride = '';
        }

        //法器魂石
        if (!empty($data['multiplier'])) {
            $gongji = 0;
            $shengming = 0;
            $fangyu = 0;
            foreach ($data['ride'] as $k => $v) {
                switch ($v['item_id']) {
                    case '3230030010001':
                        $gongji += 50 * $v['count'];
                        $fangyu += 10 * $v['count'];
                        $shengming += 800 * $v['count'];
                        break;
                    case '3230040010001':
                        $gongji += 250 * $v['count'];
                        $fangyu += 50 * $v['count'];
                        $shengming += 4000 * $v['count'];
                        break;
                    case '3230050010001':
                        $gongji += 600 * $v['count'];
                        $fangyu += 120 * $v['count'];
                        $shengming += 9600 * $v['count'];
                        break;
                    default:
                        # code...
                        break;
                }

            }
            $multiplier = '攻击力+' . $gongji . '  防御+' . $fangyu . '  生命+' . $shengming;

        } else {
            $multiplier = '';
        }

        //装备宝石信息
        if ($data['gem']) {
            foreach ($data['gem'] as $k => $v) {
                $gem = json_decode($v['item_id'], true);
                foreach ($gem as $key => $value) {
                    $gem[$key] = $item[trim($value)];
                }
                $gemdetail[$v['part']] = implode(',', $gem);

            }
        }

        $this->smarty->assign('equip', $equip);
        $this->smarty->assign('ride', $ride);
        $this->smarty->assign('multiplier', $multiplier);
        $this->smarty->assign('gemdetail', $gemdetail);
        $this->smarty->assign('info', $info);
        $this->smarty->display('role/baseinfo.tpl');
    }

    //流水
    public function log()
    {
        $this->setDataTableId('log');
        $id = $_SESSION['roleid'];
        if (!$id) {
            echo '请选择查询的角色';
            exit;
        }
        $tabs = array(
            array('title' => '装备强化流水', 'url' => 'admin.php?&ctrl=log&act=equip'),
            array('title' => '装备替换流水', 'url' => 'admin.php?&ctrl=log&act=equip_replace'),
            array('title' => '道具流水', 'url' => 'admin.php?&ctrl=log&act=prop'),
            array('title' => '金钱流水', 'url' => 'admin.php?&ctrl=log&act=money'),
            array('title' => '等级流水', 'url' => 'admin.php?&ctrl=log&act=level'),
            array('title' => '登录流水', 'url' => 'admin.php?&ctrl=log&act=login'),
            array('title' => '客户端流水', 'url' => 'admin.php?&ctrl=log&act=client'),
            array('title' => '任务流水', 'url' => 'admin.php?&ctrl=log&act=task'),
            array('title' => '坐骑流水', 'url' => 'admin.php?&ctrl=log&act=ride'),
            array('title' => '战斗力流水', 'url' => 'admin.php?&ctrl=log&act=ce'),
            array('title' => '宝石流水', 'url' => 'admin.php?&ctrl=log&act=gem'),
        );
        $this->tabs($tabs, 'log');
    }

    public function bag()
    {
        $id = $_SESSION['roleid'];
        $server = $_SESSION['server'];
        if (!$id) {
            echo '请选择查询的角色';
            exit;
        }
        $this->call('LogModel', 'getItem', array());
        $info = $this->call('RoleModel', 'bag', array('id' => $id), $server);
        $info = $info[$server];
        $memache = Util::memcacheConn();
        $item = $memache->get($server . 'item');
        foreach ($info as $k => $v) {
            $item_id = $v['item_id'];
            $info[$k]['item'] = $item["$item_id"];
        }
        $this->smarty->assign('info', $info);
        $this->smarty->display('role/bag.tpl');

    }

    //职业统计（子菜单）
    public function career()
    {
        $header = '说明：<br>';
        $header .= '1. 总人数 指角色总数量<br>';
        $header .= '2. 刀剑师 跟 羽翎师 两列的值的格式是：人数（比例）<br>';
        $header .= '3. 比例  = 刀剑师或羽翎师人数 / 总人数 (百分比四舍五入保留1位小数)';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('日期', 70);
        $this->setField('刀剑师');
        $this->setField('羽翎师');
        $this->setField('总人数');
        $this->setSource('role', 'career_data', 1);
        $this->displays();
    }

    public function career_data()
    {
        $this->setCondition(0, 'date', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('career_data', 'career_export');
        $this->call('RoledailyModel', $method, $this->getConditions());
    }

    //等级统计（菜单）
    public function level()
    {
        $header = '说明：<br>';
        $header .= '1. 总人数 指角色总数量<br>';
        $header .= '2. 比例 = 等级人数 / 总人数 (百分比四舍五入保留1位小数)<br>';
        $this->setHeader($header);
        $this->setSearch('日期', 'date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('等级', 70);
        $this->setField('人数');
        $this->setField('总人数');
        $this->setField('比例');
        $this->setSource('role', 'level_data');
        $this->displays();
    }

    public function level_data()
    {
        $this->setCondition(0, 'eq', "from_unixtime(create_time,'%Y-%m-%d')");
        $this->setCondition(1, 'scp');
        $this->setOrder('level');
        $this->setLimit();
        $method = $this->setDataExport('level_data', 'level_export');
        $this->call('RoledailyModel', $method, $this->getConditions());
    }

    //创角数（菜单）
    public function create()
    {

        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('日期', 70);
        $this->setField('创角数');
        $this->setField('创角设备数');
        $this->setDataTableId('create');
        $this->setSource('role', 'create_data', 1);

        $js = <<< END
   			$(function () {
				$('#create tr').die().live("click", function () {
					if (\$create.fnIsOpen(this))
						\$create.fnClose(this)
					else {
						var elem = \$(this).find('.data')
						var postData = new Object
						postData.date = elem.attr('data-date')
						postData.server = elem.attr('data-server')
						var tr = this
						$.post('admin.php?ctrl=role&act=rolecreate_detail', postData, function (data) {
						   \$create.fnOpen(tr, data)
						})
					}
				})
			})

END;
        $this->setJs($js);
        $this->displays();
    }

    public function create_data()
    {
        $default = $this->getServerPackage(0, 0, 1, 0);
        $this->setCondition(0, 'date', 'unix_timestamp(date)');
        $this->setCondition(1, 'scp', '', $default);
        $this->setOrder('date', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('create_data', 'create_export');
        $this->call('RolecreateModel', $method, $this->getConditions());
    }

    //获取当前日期创角数和创角设备数的详细信息
    function rolecreate_detail()
    {

        $m = new RolecreateModel();
        $data = $m->getDetail($_POST['date'], $_POST['server']);
        $this->smarty->assign('data', $data);
        echo $this->smarty->fetch('online/rolecreate_detail.tpl');
    }

    //创角数分析（子菜单）
    public function create_analysis()
    {
        $header = '说明：<br>';
        $header .= '1. 非重复创角数 = 非重复创角数 指当天注册角色帐号去重<br>';
        $header .= '2. 非重复创角数所占比率 = 非重复创角数 / 角色总数 (百分比四舍五入保留1位小数)<br>';
        $header .= '3. 重复创角数 = 角色总数 - 非重复创角数 <br>';
        $header .= '4. 重复创角数所占比率 = 重复创角数 / 角色总数 (百分比四舍五入保留1位小数)';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        $this->setFields(array('日期', '总创角数', '非重复创角数', '非重复创角数比例', '重复创角数', '重复创角数比例'));
        $this->setSource('role', 'create_analysis_data');
        $this->displays();
    }

    public function create_analysis_data()
    {
        $this->setCondition(0, 'date', 'time');
        $this->setCondition(1, 'scp', '', '', array(1, 0, 1, 1));
        $this->setOrder('time');
        $this->setLimit();
        $this->call('StatModel', 'create_analysis_data', $this->getConditions());
    }

    public function manual_logout()
    {

        $servers = $this->sm->getServerByTrait();
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach ($channelGroups as $key => $val) {
            $groups[$val['id']] = $val['name'];
        }

        $this->smarty->assign('servers', $servers);
        $this->smarty->assign('groups', $groups);
        $this->smarty->display('role/logout.tpl');
    }

    public function manual_logout_action()
    {
        $server = $this->getParam('server');
        $save = $this->getParam('save');

//        $sql = "select server_id , ip,gm_port from ny_server_config where  server_id = '$server'";
//        $serverarr = $this->sm->query($sql);

        if ($save) {
            $data = array(
                'role_id' => $this->getParam('role_id'),
                'server' => $server
            );
            $this->ro->manual_logout($data);
        }
    }

}