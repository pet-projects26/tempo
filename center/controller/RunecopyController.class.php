<?php
!defined('IN_WEB') && exit('Access Denied');

class RuneCopyController extends Controller{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启远古符阵玩法的玩家人数（备注：开启条件待定，已上线前版本为准）。<br>';
        $header .= '参与人数：该日进行过远古符阵挑战玩法（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均挑战次数 = 该日全服每个玩家挑战远古符阵次数之和（成功才算） / 参与人数   （确定到小数点后两位，四舍五入）<br>';
        $header .= '领取每日奖励占比 = 该日全服领取远古符阵每日奖励的人数 / 该日全服远古符阵已经通关第1关的玩家数    （确定到小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '参与人数' , '参与度', '人均挑战次数', '领取每日奖励占比'));
        $this->setSource('RuneCopy' , 'index_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('RuneCopyModel' , $method , $this->getConditions());
    }
}