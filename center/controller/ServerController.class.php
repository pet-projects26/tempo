<?php
!defined('IN_WEB') && exit('Access Denied');

class ServerController extends Controller
{
    private $sm;
    private $cm;
    private $zm;
    private $scm;
    private $cgm;
    private $db;

    public function __construct()
    {
        parent::__construct();
        $this->sm = new ServerModel();
        $this->cm = new ChannelModel();
        $this->zm = new ZoneModel();
        $this->scm = new ServerconfigModel();
        $this->cgm = new ChannelgroupModel();

    }

    //服务器管理（菜单）
    public function index()
    {
        $tabs = array(
            array('title' => '服务器列表', 'url' => 'admin.php?&ctrl=server&act=record'),
            //array('title' => '添加服务器', 'url' => 'admin.php?&ctrl=server&act=add'),
            array('title' => '编辑服务器', 'url' => 'admin.php?&ctrl=server&act=edit'),
            array('title' => '刷新服务器缓存', 'url' => 'admin.php?&ctrl=server&act=del_redis_server'),
            array('title' => '刷新cdn缓存', 'url' => 'admin.php?&ctrl=server&act=refresh_cdn'),
            array('title' => '批量同步服务器配置', 'url' => 'admin.php?&ctrl=server&act=batch_config'),
            /*array('title' => '开通渠道', 'url' => 'admin.php?&ctrl=server&act=channel_open'),
            array('title' => '批量设置服务器状态', 'url' => 'admin.php?&ctrl=server&act=status_edit'),
			
			array('title' => '服务器', 'url' => 'admin.php?&ctrl=server&act=server')*/
            //todo====合服
        );
        $this->tabs($tabs);
    }

    //服务器列表（子菜单）
    public function record()
    {
        /*$rsc = $this->cm->getChannel(array() , array('channel_id' , 'name'));
        $channel = array();
        $channel[0] = '全部';
        foreach($rsc as $k => $row){
            $channel[$row['channel_id']] = $row['name'];
        }*/
        $groups = $this->cgm->getRows(array('id', 'name'));

        $group = array();
        $group[''] = '全部';
        foreach ($groups as $k => $v) {
            $group[$v['id']] = $v['name'];
        }

        $this->setSearch('渠道组', 'select', $group);
        $this->setSearch('开服时间', 'range_time');
        $serverStatus = CDict::$serverStatus;
        $this->setSearch('状态', 'select', $serverStatus);
        $this->setSearch('名称');
        $this->setField('编号', '80');
        $this->setField('渠号', '81');
        $this->setFields(array('名称', '标识', '分区', '状态', '开服时间', '开服天数', '显示状态', '合服标识'));
        $this->setField('操作', '300');
        $this->setLength(100);
        $this->setSource('server', 'record_data');
        $js = <<< END
            function edit(sid){
                \$tabs.tabs('select' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=server&act=edit&sid=' + sid);
                \$tabs.tabs('load' , 1);
                \$tabs.tabs('url' , 1 , 'admin.php?ctrl=server&act=edit&sid=' + sid);
            }
            function del(sid){
                if(confirm("确定删除服务器吗？"))
                {
                     $.ajax({
                        url: 'admin.php?ctrl=server&act=del_action&sid=' + sid,
                        type: 'POST',
                        dataType: 'JSON',
                        data: $(this).serialize()
                        }).done(function(data){
                            if(data ==1){
                                $.dialog.tips('删除成功');
                                \$tabs.tabs('load' , 0);
                            }else{
                                $.dialog.tips('删除失败，请重试');
                            }
                            
                        })
                    }
 
            }
            $("#check-all").bind('click',function(){
                if($(this).prop('checked')){
                    $(".select-server").attr('checked',true);
                }else{
                    $(".select-server").removeAttr('checked');
                }
            })
            $(".edit-all").click(function(){
                //显示服务器配置modal
            })
            function channel_open(sid, gid){
                \$tabs.tabs('select' , 3);
                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=server&act=channel_open&sid=' + sid + '&gid=' + gid);
                \$tabs.tabs('load' , 3);
                \$tabs.tabs('url' , 3 , 'admin.php?ctrl=server&act=channel_open&sid=' + sid + '&gid=' + gid);
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    //服务器列表（数据）
    public function record_data()
    {
        $this->setCondition(0, 'eq', 'channel_num');
        $this->setCondition(1, 'time', 's.open_time');
        $this->setCondition(2, 'eq', 's.status');
        $this->setCondition(3, 'like', 's.name');
        $this->setOrder('s.id');
        $this->setLimit(100);
        $this->sm->record_data($this->getConditions());
    }

    //添加服务器（子菜单）
    public function add()
    {
        $zone = $this->zm->getZone(array());
        $this->smarty->assign('zone', $zone);
        unset(CDict::$serverStatus['-2']);
        $this->smarty->assign('status', CDict::$serverStatus);
        $this->smarty->display('server/add.tpl');
    }

    public function add_action()
    {
        $server_id = $this->getParam('server_id');
        if ($server_id) {
            $num = $this->getParam('num');
            $name = $this->getParam('name');
            $type = $this->getParam('type');
            $zone = $this->getParam('zone');
            $status = $this->getParam('status');
            $ip = $this->getParam('ip');
            $domain = $this->getParam('domain');
            $api_url = $this->getParam('api_url');
            $open_time = $this->getParam('open_time');
            $close_time = $this->getParam('close_time');
            $sort = $this->getParam('sort');
            $display = $this->getParam('display');
            $tips = $this->getParam('tips');
            $max_online = $this->getParam('max_online');
            $mongo_host = $this->getParam('mongo_host');
            $mongo_port = $this->getParam('mongo_port');
            $mongo_user = $this->getParam('mongo_user');
            $mongo_passwd = $this->getParam('mongo_passwd');
            $mongo_db = $this->getParam('mongo_db');

            $redis_host = $this->getParam('redis_host');
            $redis_port = $this->getParam('redis_port');
            $redis_passwd = $this->getParam('redis_passwd');
            $redis_prefix = $this->getParam('redis_prefix');
            $redis_db = $this->getParam('redis_db');

            $websocket_host = $this->getParam('websocket_host');
            $websocket_port = $this->getParam('websocket_port');

            $mysql_host = $this->getParam('mysql_host');
            $mysql_port = $this->getParam('mysql_port');
            $mysql_user = $this->getParam('mysql_user');
            $mysql_passwd = $this->getParam('mysql_passwd');
            $mysql_db = $this->getParam('mysql_db');
            $mysql_prefix = $this->getParam('mysql_prefix');
            $login_host = $this->getParam('login_host');
            $login_port = $this->getParam('login_port');
            $gm_port = $this->getParam('gm_port');
            $mdkey = $this->getParam('mdkey');

            $server_data = array();
            $server_id && $server_data['server_id'] = $server_id;
            $num && $server_data['num'] = $num;
            $name && $server_data['name'] = $name;
            $server_data['type'] = $type;
            $server_data['zone'] = $zone;
            $server_data['status'] = $status;
            $open_time && $server_data['open_time'] = strtotime($open_time);
            $close_time && $server_data['close_time'] = strtotime($close_time);
            $sort && $server_data['sort'] = $sort;
            $server_data['display'] = $display;
            $tips && $server_data['tips'] = $tips;
            $max_online && $server_data['max_online'] = $max_online;
            $server_data['create_time'] = time();

            $server_config_data = array();
            $server_id && $server_config_data['server_id'] = $server_id;
            $ip && $server_config_data['ip'] = $ip;
            $domain && $server_config_data['domain'] = $domain;
            $api_url && $server_config_data['api_url'] = $api_url;
            $mongo_host && $server_config_data['mongo_host'] = $mongo_host;
            $mongo_port && $server_config_data['mongo_port'] = $mongo_port;
            $mongo_user && $server_config_data['mongo_user'] = $mongo_user;
            $mongo_passwd && $server_config_data['mongo_passwd'] = $mongo_passwd;
            $mongo_db && $server_config_data['mongo_db'] = $mongo_db;

            $redis_host && $server_config_data['redis_host'] = $redis_host;
            $redis_port && $server_config_data['redis_port'] = $redis_port;
            $redis_passwd && $server_config_data['redis_passwd'] = $redis_passwd;
            $redis_prefix && $server_config_data['redis_prefix'] = $redis_prefix;
            $redis_db && $server_config_data['redis_db'] = $redis_db;

            $websocket_host && $server_config_data['websocket_host'] = $websocket_host;
            $websocket_port && $server_config_data['websocket_port'] = $websocket_port;

            $mysql_host && $server_config_data['mysql_host'] = $mysql_host;
            $mysql_port && $server_config_data['mysql_port'] = $mysql_port;
            $mysql_user && $server_config_data['mysql_user'] = $mysql_user;
            $mysql_passwd && $server_config_data['mysql_passwd'] = $mysql_passwd;
            $mysql_db && $server_config_data['mysql_db'] = $mysql_db;
            $mysql_prefix && $server_config_data['mysql_prefix'] = $mysql_prefix;
            $login_host && $server_config_data['login_host'] = $login_host;
            $login_port && $server_config_data['login_port'] = $login_port;
            $gm_port && $server_config_data['gm_port'] = $gm_port;
            $mdkey && $server_config_data['mdkey'] = $mdkey;

            if ($this->sm->getServer($server_id)) {
                $json = array('msg' => '服务器标识已存在', 'code' => 0);
                exit(json_encode($json));
            }
            if (!preg_match('/^[0-9a-zA-Z_\-]+$/', $server_id)) {
                $json = array('msg' => '服务器标识格式不正确', 'code' => 0);
                exit(json_encode($json));
            }
            if (!$server_data && !$server_config_data) {
                $json = array('msg' => '没有填写任何内容', 'code' => 0);
                exit(json_encode($json));
            }

            $server_flag = 0;
            $server_config_flag = 0;
            if (!$server_data || $this->sm->addServer($server_data)) {
                $server_flag = 1;
            }

            if (!$server_config_data || $this->scm->addConfig($server_config_data)) {
                $server_config_flag = 1;
            }

            if ($server_flag && $server_config_flag) {
                $json = array('msg' => '保存成功', 'code' => 1);
                if ($api_url && url_exists($api_url)) {
                    $config = array();
                    $mongo_host && $config['mongo_host'] = $mongo_host;
                    $mongo_port && $config['mongo_port'] = $mongo_port;
                    $mongo_user && $config['mongo_user'] = $mongo_user;
                    $mongo_passwd && $config['mongo_passwd'] = $mongo_passwd;
                    $mongo_db && $config['mongo_db'] = $mongo_db;

                    $redis_host && $config['redis_host'] = $redis_host;
                    $redis_port && $config['redis_port'] = $redis_port;
                    $redis_passwd && $config['redis_passwd'] = $redis_passwd;
                    $redis_prefix && $config['redis_prefix'] = $redis_prefix;
                    $redis_db && $config['redis_db'] = $redis_db;

                    $websocket_host && $config['websocket_host'] = $websocket_host;
                    $websocket_port && $config['websocket_port'] = $websocket_port;

                    $mysql_host && $config['mysql_host'] = $mysql_host;
                    $mysql_port && $config['mysql_port'] = $mysql_port;
                    $mysql_user && $config['mysql_user'] = $mysql_user;
                    $mysql_passwd && $config['mysql_passwd'] = $mysql_passwd;
                    $mysql_db && $config['mysql_db'] = $mysql_db;
                    $mysql_prefix && $config['mysql_prefix'] = $mysql_prefix;
                    $gm_port && $config['gm_port'] = $gm_port;
                    $mdkey && $config['mdkey'] = $mdkey;
                    $open_time && $config['open_time'] = strtotime($open_time);
                    $close_time && $config['close_time'] = strtotime($close_time);
                    @$this->call('ServerconfigController', 'setConfig', array('config' => $config), $server_id); //添加单服本地数据库的配置文件
                }
            } else {
                $json = array('msg' => '保存未成功，请再次尝试', 'code' => 0);
            }
            echo json_encode($json);
        }
    }

    //编辑服务器（子菜单）
    public function edit()
    {
        $server_id = $this->getParam('sid');
        if ($server_id) {
            $server = $this->sm->getServer($server_id);
            $server['open_time'] = $server['open_time'] ? date('Y-m-d H:i', $server['open_time']) : '';
            $server['close_time'] = $server['close_time'] ? date('Y-m-d H:i', $server['close_time']) : '';
            $server['display_time'] = $server['display_time'] ? date('Y-m-d H:i', $server['display_time']) : '';
            $server_config = $this->scm->getConfig($server_id);
            $this->smarty->assign('server', $server);
            $this->smarty->assign('server_config', $server_config);
        }
        $zone = $this->zm->getZone(array());
        $this->smarty->assign('zone', $zone);
        unset(CDict::$serverStatus['-2']);
        $this->smarty->assign('status', CDict::$serverStatus);
        $this->smarty->display('server/edit.tpl');
    }

    public function edit_action()
    {
        $server_id = $this->getParam('server_id');
        if ($server_id) {

            $old_server_id = $this->getParam('old_server_id');

            $name = $this->getParam('name');
            //$type = $this->getParam('type');
            $zone = $this->getParam('zone');
            $status = $this->getParam('status');
            $ip = $this->getParam('ip');
            $domain = $this->getParam('domain');
            $api_url = $this->getParam('api_url');
            $configs_path = self::trimAll($this->getParam('configs_path'));
            $bin_path = self::trimAll($this->getParam('bin_path'));
            $open_time = $this->getParam('open_time');
            $close_time = $this->getParam('close_time');
            $sort = $this->getParam('sort');
            $display = $this->getParam('display');
            $tips = $this->getParam('tips');
            $max_online = $this->getParam('max_online');
            $mongo_host = $this->getParam('mongo_host');
            $mongo_port = $this->getParam('mongo_port');
            $mongo_user = $this->getParam('mongo_user');
            $mongo_passwd = $this->getParam('mongo_passwd');
            $mongo_db = $this->getParam('mongo_db');

            $redis_host = $this->getParam('redis_host');
            $redis_port = $this->getParam('redis_port');
            $redis_passwd = $this->getParam('redis_passwd');
            $redis_prefix = $this->getParam('redis_prefix');
            $redis_db = $this->getParam('redis_db');

            $websocket_host = $this->getParam('websocket_host');
            $websocket_port = $this->getParam('websocket_port');

            $mysql_host = $this->getParam('mysql_host');
            $mysql_port = $this->getParam('mysql_port');
            $mysql_user = $this->getParam('mysql_user');
            $mysql_passwd = $this->getParam('mysql_passwd');
            $mysql_db = $this->getParam('mysql_db');
            $mysql_prefix = $this->getParam('mysql_prefix');
            $login_host = $this->getParam('login_host');
            $login_port = $this->getParam('login_port');
            $gm_port = $this->getParam('gm_port');
            $mdkey = $this->getParam('mdkey');
            $review = $this->getParam('review');
            $channel_rv = $this->getParam('channel_rv');
            $display_time = $this->getParam('display_time');
            $data = array();
            $server_data = array();//server
            $server_id && $server_data['server_id'] = $server_id;
            $name && $server_data['name'] = $name;
            //$server_data['type'] = $type;
            $server_data['zone'] = $zone;
            $server_data['status'] = $status;
            $open_time && $server_data['open_time'] = strtotime($open_time);
            $close_time && $server_data['close_time'] = strtotime($close_time);
            $sort && $server_data['sort'] = $sort;
            $server_data['display'] = $display;
            $tips && $server_data['tips'] = $tips;
            $max_online && $server_data['max_online'] = $max_online;
            !is_null($review) && $server_data['review'] = (int)$review;//提审状态标识
            $display_time && $server_data['display_time'] = strtotime($display_time);//服务器自动显示时间
            $server = $this->sm->getServer($old_server_id);
            foreach ($server as $k => $v) {
                if (array_key_exists($k, $server_data) && $v != $server_data[$k]) {
                    $data[$k] = $server_data[$k];
                }
            }
            $server_data = $data;
            $data = array();
            $server_config_data = array();//server_config
            $server_id && $server_config_data['server_id'] = $server_id;
            $ip && $server_config_data['ip'] = $ip;
            $domain && $server_config_data['domain'] = $domain;
            $api_url && $server_config_data['api_url'] = $api_url;
            $configs_path && $server_config_data['configs_path'] = $configs_path;
            $bin_path && $server_config_data['bin_path'] = $bin_path;
            $mongo_host && $server_config_data['mongo_host'] = $mongo_host;
            $mongo_port && $server_config_data['mongo_port'] = $mongo_port;
            $mongo_user && $server_config_data['mongo_user'] = $mongo_user;
            $mongo_passwd && $server_config_data['mongo_passwd'] = $mongo_passwd;
            $mongo_db && $server_config_data['mongo_db'] = $mongo_db;

            $redis_host && $server_config_data['redis_host'] = $redis_host;
            $redis_port && $server_config_data['redis_port'] = $redis_port;
            $redis_passwd && $server_config_data['redis_passwd'] = $redis_passwd;
            $redis_prefix && $server_config_data['redis_prefix'] = $redis_prefix;
            $redis_db && $server_config_data['redis_db'] = $redis_db;

            $websocket_host && $server_config_data['websocket_host'] = $websocket_host;
            $websocket_port && $server_config_data['websocket_port'] = $websocket_port;

            $mysql_host && $server_config_data['mysql_host'] = $mysql_host;
            $mysql_port && $server_config_data['mysql_port'] = $mysql_port;
            $mysql_user && $server_config_data['mysql_user'] = $mysql_user;
            $mysql_passwd && $server_config_data['mysql_passwd'] = $mysql_passwd;
            $mysql_db && $server_config_data['mysql_db'] = $mysql_db;
            $mysql_prefix && $server_config_data['mysql_prefix'] = $mysql_prefix;
            $login_host && $server_config_data['login_host'] = $login_host;
            $login_port && $server_config_data['login_port'] = $login_port;
            $gm_port && $server_config_data['gm_port'] = $gm_port;
            $mdkey && $server_config_data['mdkey'] = $mdkey;
            isset($channel_rv) && $server_config_data['channel_rv'] = $channel_rv;
            $server_config = $this->scm->getConfig($old_server_id);
            foreach ($server_config as $k => $v) {
                if (array_key_exists($k, $server_config_data) && $v != $server_config_data[$k]) {
                    $data[$k] = $server_config_data[$k];
                }
            }
            $server_config_data = $data;

            if (!preg_match('/^[0-9a-zA-Z_\-]+$/', $server_id)) {
                $json = array('msg' => '服务器标识格式不正确', 'code' => 0);
                exit(json_encode($json));
            }
            if (!$server_data && !$server_config_data) {
                $json = array('msg' => '没有任何改变', 'code' => 0);
                exit(json_encode($json));
            }

            $server_flag = 0;
            $server_config_flag = 0;
            if (!$server_data || $this->sm->editServer($server_data, $old_server_id)) {
                $server_flag = 1;
            }
            if (!$server_config_data || $this->scm->editConfig($server_config_data, $old_server_id)) {
                $server_config_flag = 1;
            }

            if ($server_flag && $server_config_flag) {
                $json = array('msg' => '保存成功', 'code' => 1);
                //更改serverRedis
                (new ServerRedis(true))->setServer($old_server_id, $server_id);

                if ($api_url && url_exists($api_url)) {
                    $config = array();
                    $configs_path && $config['configs_path'] = $configs_path;

                    $mongo_host && $config['mongo_host'] = $mongo_host;
                    $mongo_port && $config['mongo_port'] = $mongo_port;
                    $mongo_user && $config['mongo_user'] = $mongo_user;
                    $mongo_passwd && $config['mongo_passwd'] = $mongo_passwd;
                    $mongo_db && $config['mongo_db'] = $mongo_db;

                    $redis_host && $config['redis_host'] = $redis_host;
                    $redis_port && $config['redis_port'] = $redis_port;
                    $redis_passwd && $config['redis_passwd'] = $redis_passwd;
                    $redis_prefix && $config['redis_prefix'] = $redis_prefix;
                    $redis_db && $config['redis_db'] = $redis_db;

                    $websocket_host && $config['websocket_host'] = $websocket_host;
                    $websocket_port && $config['websocket_port'] = $websocket_port;

                    $mysql_host && $config['mysql_host'] = $mysql_host;
                    $mysql_port && $config['mysql_port'] = $mysql_port;
                    $mysql_user && $config['mysql_user'] = $mysql_user;
                    $mysql_passwd && $config['mysql_passwd'] = $mysql_passwd;
                    $mysql_db && $config['mysql_db'] = $mysql_db;
                    $mysql_prefix && $config['mysql_prefix'] = $mysql_prefix;
                    $gm_port && $config['gm_port'] = $gm_port;
                    $mdkey && $config['mdkey'] = $mdkey;
                    $open_time && $config['open_time'] = strtotime($open_time);
                    $close_time && $config['close_time'] = strtotime($close_time);
                    $channel_rv && $config['channel_rv'] = trim($channel_rv);//去除空格


                    @$this->call('ServerconfigController', 'setConfig', array('config' => $config), $server_id); //添加单服本地数据库的配置文件


                }
                (new LogAction())->logAdd("编辑服务器：{$server_id}");
            } else {
                $json = array('msg' => '没有任何改变', 'code' => 0);
            }
            echo json_encode($json);
        }
    }

    /**
     * 开服
     * @return [type] [description]
     */
    public function channel_open()
    {
        $server_id = $this->getParam('sid');
        $gid = $this->getParam('gid');

        $this->smarty->assign('id', $gid);

        if (empty($server_id)) {
            echo '请选择服务器';
            die();
        }


        $res = $this->cgm->getChannelGroupByTrait(array());
        $group = array();
        foreach ($res as $row) {
            $groupId = $row['id'];
            $group[$groupId] = $row;
        }

        // $rsc = $this->cm->getChannel(array() , array('channel_id' , 'name'));
        // $channel = array();
        // foreach($rsc as $row){
        //     $channel[$row['channel_id']] = $row['name'];
        // }

        // foreach($group as $k => $row){

        //     if(!empty($row['server'])){ //把已开通的渠道标红显示
        //         $row['server'] = json_decode($row['server'] , true);
        //         if(in_array($server_id , $row['server'])){
        //             $group[$k]['name'] = '<span style="color:#ff0000;">' . $group[$k]['name'] . '</span>';
        //             $this->smarty->assign('id' , $group[$k]['id']);
        //         }
        //     }

        //     $row['channel_package'] = json_decode($row['channel_package'] , true);
        //     $channel_package = array();
        //     if($row['channel_package']){
        //         foreach($row['channel_package'] as $channel_id => $package){
        //             $channel_package[$channel[$channel_id] . '(' . $channel_id . ')'] = implode(' , ' , $package);
        //         }
        //     }
        //     $group[$k]['channel_package'] = $channel_package;
        // }
        //
        $this->db = new Model();

        $sql = "SELECT group_id, pack.channel_id, package_id, cha.name  FROM ny_package AS pack 
                    LEFT JOIN ny_channel AS cha
                ON pack.`channel_id` = cha.`channel_id` ";

        $rows = $this->db->query($sql);

        $package = array();
        foreach ($rows as $row) {
            $groupId = $row['group_id'];

            $key = $row['name'] . "(" . $row['channel_id'] . ")";
            $packageId = $row['package_id'];

            $package[$groupId][$key][$packageId] = $packageId;
        }

        $this->smarty->assign('package', $package);
        $this->smarty->assign('group', $group);
        $this->smarty->assign('server_id', $server_id);

        $this->smarty->display('server/channel_open.tpl');
    }

    public function del_action()
    {
        $server_id = $this->getParam('sid');
        if ($server_id) {
            $sm = $this->sm->delServer($server_id);
            $scm = $this->scm->delConfig($server_id);

            if ($sm) {
                (new LogAction())->logAdd("删除服务器：{$server_id}");
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //批量设置服务器状态
    public function status_edit()
    {
        $serverStatus = CDict::$serverStatus;
        $serverStatus[0] = '未选择';
        $server = $this->sm->getServer(array(), array('server_id', 'name', 'status'));
        $this->smarty->assign('status', $serverStatus);

        //服务器，渠道组
        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait(array());

        $channelGroupRow = array();

        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }

        $server = (new ServerModel())->getServerByTrait2();

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);

        $this->smarty->display('server/status_edit.tpl');
    }

    public function status_edit_action()
    {

        //渠道组，服务器
        $platforms = array_filter($_POST['channel_group']);

        $servers = array_filter($_POST['server']);

        $server = (new ServerModel)->getServerByServerAndChannelGroup2($servers, $platforms);

        $server = array_keys($server);

        if ($server) {
            $status = $this->getParam('status');
            if ($this->sm->editServer(array('status' => $status), $server)) {
                $json = array('msg' => '保存成功', 'code' => 1);
            } else {
                $json = array('msg' => '没有任何改变', 'code' => 0);
            }
        } else {
            $json = array('msg' => '未选择服务器', 'code' => 0);
        }
        echo json_encode($json);
    }

    public function server()
    {

        $group = $this->cgm->getChannelGroup(array(), array('id', 'name'));
        $channel = $this->cm->getChannel(array(), array('num', 'name'));
        $serverStatus = CDict::$serverStatus;

        array_unshift($group, array('id' => '', 'name' => '-'));

        array_unshift($channel, array('num' => '', 'name' => '-'));

        $sql = "select s.num,s.channel_num,s.name,s.server_id,s.zone,sc.mdkey,s.status,s.open_time,s.display,s.group_id from ny_server as s left join ny_server_config as  sc on s.server_id = sc.server_id";

        $rs = $this->sm->query($sql);

        $rss = $this->getserver1($rs);

        $this->smarty->assign('rss', $rss);
        $this->smarty->assign('serverStatus', $serverStatus);
        $this->smarty->assign('group', $group);
        $this->smarty->assign('channel', $channel);
        $this->smarty->display('server/server.tpl');
    }

    public function getserver1($rs)
    {
        $rsz = $this->zm->getZone(array());
        $zone = array();
        foreach ($rsz as $row) {
            $zone[$row['id']] = $row['name'];
        }

        $rss = array();
        $serverStatus = CDict::$serverStatus;
        foreach ($rs as $k => $row) {
            $gid = $row['group_id'];

            $sql = "select name from ny_channel_group where id = " . $row['group_id'];
            $group_id = $this->zm->query($sql);
            $row['group_id'] = $group_id[0]['name'];

            $open_time = $row['open_time'] ? ceil((time() - $row['open_time']) / 86400) : 0;

            $rss[$row['group_id']][$k]['num'] = $row['num'];

            $rss[$row['group_id']][$k]['channel_num'] = $row['channel_num'];

            $rss[$row['group_id']][$k]['name'] = $row['name'];

            $rss[$row['group_id']][$k]['server_id'] = $row['server_id'];

            $rss[$row['group_id']][$k]['zone'] = $row['zone'] ? array_key_exists($row['zone'], $zone) ? $zone[$row['zone']] : '分区已删除' : '未选择分区';

            $rss[$row['group_id']][$k]['mdkey'] = $row['mdkey'] ? $row['mdkey'] : '无';

            $rss[$row['group_id']][$k]['status'] = $serverStatus[$row['status']];

            $rss[$row['group_id']][$k]['open_time'] = $row['open_time'] ? date('Y-m-d H:i:s', $row['open_time']) : '无';

            $rss[$row['group_id']][$k]['day'] = $open_time > 0 ? '第' . $open_time . '天' : '未开服';

            $rss[$row['group_id']][$k]['display'] = $row['display'] ? '<span style="color:#228b22;">显示</span>' : '<span style="color:#ff0000;">不显示</span>';

            $rss[$row['group_id']][$k]['caozuo'] = '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['server_id'] . '\')">' .
                '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['server_id'] . '\')">' .
                '<input type="button" class="gbutton" value="开通渠道" onclick="channel_open(\'' . $row['server_id'] . '\', \'' . $gid . '\')">';

        }
        return $rss;
    }

    public function ajax_server()
    {

        $group_id = $this->getParam('group');
        $channel_num = $this->getParam('channel');
        $start_time = $this->getParam('start_time');
        $end_time = $this->getParam('end_time');
        $serverStatus = $this->getParam('serverStatus');
        $name = $this->getParam('name');

        $where = 'where 1';
        if (!empty($group_id)) {
            $where .= " and group_id = $group_id";
        }
        if (!empty($channel_num)) {
            $where .= " and channel_num = $channel_num";
        }
        if (!empty($start_time)) {
            $where .= " and open_time >=" . strtotime($start_time);
        }
        if (!empty($end_time)) {
            $where .= " and open_time <=" . strtotime($end_time);
        }
        if ($serverStatus != -2) {
            $where .= " and status = $serverStatus";
        }
        if (!empty($name)) {
            $where .= " and name = $name";
        }

        $sql = "select s.num,s.channel_num,s.name,s.server_id,s.zone,sc.mdkey,s.status,s.open_time,s.display,s.group_id from ny_server as s left join ny_server_config as  sc on s.server_id = sc.server_id $where";

        $rs = $this->sm->query($sql);

        $rss = $this->getserver1($rs);

        $html = '';

        foreach ($rss as $k => $v) {
            $html .= '<div class="contents" >';
            $html .= '<h5>' . $k . '</h5>';
            $html .= '<table cellpadding="0" cellspacing="0" border="1" class="table_list dataTable" id="DataTables_Table_1" 	aria-describedby="DataTables_Table_1_info" >';
            $html .= '<thead>';
            $html .= '<tr height="44px" role="row">';
            $html .= '<th width="80" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="编号" style="width: 74px;">编号</th>
					  <th width="81" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="渠号" style="width: 75px;">渠号</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="名称" style="width: 111px;">名称</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="标识" style="width: 111px;">标识</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="分区" style="width: 111px;">分区</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="认证密钥" style="width: 111px;">认证密钥</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="状态" style="width: 111px;">状态</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="开服时间" style="width: 112px;">开服时间</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="开服天数" style="width: 112px;">开服天数</th>
					  <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="显示状态" style="width: 112px;">显示状态</th>
					  <th width="300" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="操作" style="width: 277px;">操作</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= ' <tbody role="alert" aria-live="polite" aria-relevant="all">';
            foreach ($v as $key => $val) {
                $html .= '<tr class="odd">';
                $html .= '<td class=" sorting_1">' . $val['num'] . '</td>';
                $html .= '<td class="">' . $val['channel_num'] . '</td>';
                $html .= '<td class="">' . $val['name'] . '</td>';
                $html .= '<td class="">' . $val['server_id'] . '</td>';
                $html .= '<td class="">' . $val['zone'] . '</td>';
                $html .= '<td class="">' . $val['mdkey'] . '</td>';
                $html .= '<td class="">' . $val['status'] . '</td>';
                $html .= '<td class="">' . $val['open_time'] . '</td>';
                $html .= '<td class="">' . $val['day'] . '</td>';
                $html .= '<td class="">' . $val['display'] . '</td>';
                $html .= '<td class="">' . $val['caozuo'] . '</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
        }
        echo $html;

    }

    public function del_redis_server()
    {
        $this->smarty->display('server/del_redis_server.tpl');
    }

    public function del_redis_server_action()
    {
        $save = $this->getParam('save');

        if ($save) {

            $ServerRedis = new ServerRedis(true);
            $ServerRedis->delServerKey();

            (new LogAction())->logAdd("刷新redis服务器缓存");
            exit(json_encode(['state' => 1, 'msg' => '刷新成功']));
        }

        exit(json_encode(['state' => 0, 'msg' => '参数有误']));
    }

    public function refresh_cdn()
    {
        $this->smarty->display('server/refresh_cdn.tpl');
    }

    public function refresh_cdn_action()
    {

        $refreshType = (int)$this->getParam('refreshType');
        $dirType = (int)$this->getParam('dirType');

        $returnArr = [
            'code' => 4000,
            'message' => '参数错误'
        ];

        if (!$refreshType || ($refreshType == 2 && !$dirType)) {
            echo json_encode($returnArr);
            die();
        }

        $args = $this->getParam('str');

        if (empty($args)) {
            echo json_encode($returnArr);
            die();
        }

        $TencentCdn = new TencentCdn();

        if ($refreshType == 1) {
            $returnArr = $TencentCdn->RefreshCdnUrl($args);
        } else if ($refreshType == 2) {
            $returnArr = $TencentCdn->RefreshCdnDir($args, $dirType);
        }

        if ($returnArr['code'] == 0) {
            $returnArr['message'] = '刷新成功';
        }

        echo json_encode($returnArr);
        die();
    }

    public function batch_config()
    {
        $servers = $this->sm->getServerByTrait([],true);
        $channelGroups = $this->sm->getChannelGroupByTrait();

        $groups = array();
        foreach($channelGroups as $key=>$val){
            $groups[$val['id']] = $val['name'];
        }
        foreach($groups as $key=>$v){
            if(!in_array($key,array_keys($servers))){
                unset($groups[$key]);
            }
        }

        $this->smarty->assign('servers',$servers);
        $this->smarty->assign('groups',$groups);

        $this->smarty->display('server/batch_config.tpl');
    }

    public function batch_config_action()
    {
        $save = $this->getParam('save');

        if ($save) {

            $server = $this->getParam('server');
            $channelGroup = $this->getParam('channel_group');

            $server = empty($server[0]) ? array() : $server;
            $channelGroup =  empty($channelGroup[0])?array():$channelGroup;

            $server_id = array_unique(array_filter($server));
            $channel_group = array_unique(array_filter($channelGroup));
            $serverids =  $this->sm->getServerByServerAndChannelGroup($server_id, $channel_group, '1', 1);

            $fileds = [
                's.server_id as server_id',
                'sc.configs_path as configs_path', 'sc.redis_host as redis_host', 'sc.redis_port as redis_port', 'sc.redis_passwd as redis_passwd',
                'sc.redis_prefix as redis_prefix', 'sc.redis_db as redis_db', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port', 'sc.mysql_host as mysql_host', 'sc.mysql_port as mysql_port', 'sc.mysql_user as mysql_user', 'sc.mysql_passwd as mysql_passwd', 'sc.mysql_db as mysql_db',
                'sc.mysql_prefix as mysql_prefix', 'sc.gm_port as gm_port', 'sc.mdkey as mdkey', 's.open_time as open_time', 's.close_time as close_time', 'sc.channel_rv as channel_rv'
            ];

            if (empty($serverids)) {
                exit(
                json_encode([
                    'code' => 400,
                    'msg' => '服务器id为空'
                ])
                );
            }
            $serveridList = array_keys($serverids);

            $servers_config = $this->sm->getServer($serveridList, $fileds);

            if (!empty($servers_config)) {

                foreach ($servers_config as $row) {

                    $server_id = $row['server_id'];

                    unset($row['server_id']);

                    $this->call('ServerconfigController', 'setConfig', array('config' => $row), $server_id); //添加单服本地数据库的配置文件

                }
            } else {
                exit(
                    json_encode([
                        'code' => 400,
                        'msg' => '配置为空'
                    ])
                );
            }
            exit(
            json_encode([
                'code' => 200,
                'msg' => '操作成功'
            ])
            );


        }
        exit(
        json_encode([
            'code' => 400,
            'msg' => '参数有误'
        ])
        );

    }


    private static function trimAll($str)
    {
        $str = trim($str);
        $qian = array("", " ", "\t", "\n", "\r");
        $hou = array("", "", "", "");
        return str_replace($qian, $hou, $str);

    }
}
