<?php
!defined('IN_WEB') && exit('Access Denied');

class ServerAutoPlanController extends Controller
{
    private $ServerAutoPlanModel;
    private $cgm;
    private $sm;

    public function __construct(){
        parent::__construct();
        $this->cgm = new ChannelgroupModel();
        $this->ServerAutoPlanModel = new ServerAutoPlanModel();
        $this->sm = new ServerModel();
    }

    public function index(){
        $tabs = array(
            array('title' => '计划列表', 'url' => 'admin.php?&ctrl=serverAutoPlan&act=record'),
            array('title' => '添加计划', 'url' => 'admin.php?&ctrl=serverAutoPlan&act=add'),
            array('title' => '编辑计划', 'url' => 'admin.php?&ctrl=serverAutoPlan&act=edit'),
        );
        $this->tabs($tabs);
    }

    public function record(){
        $groups = $this->cgm->getRows(array('id' , 'name'));

        $group = array();
        $group[''] = '全部';
        foreach($groups as $k=>$v){
            $group[$v['id']] = $v['name'];
        }

        $this->setSearch('渠道组' , 'select' , $group);
        $this->setSearch('创建时间' , 'range_time');
        $this->setSearch('状态' , 'select' , array(''=>'全部','0'=>'未配置','1'=>'已配置' ,'2'=>'已废弃'));

        $this->setFields(array('编号' , '渠号' , '说明', '状态' , '类型' , '创角数', '付费人数', '创建时间'));
        $this->setField('操作' , '300');
        $this->setLength(100);
        $this->setSource('serverAutoPlan' , 'record_data');
        $js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=serverAutoPlan&act=edit&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=serverAutoPlan&act=edit&id=' + id);
            }

            function config(status, id){
                if(confirm("确定配置吗？")){

                    $.post('admin.php?ctrl=serverAutoPlan&act=config&status='+status+'&id='+id,'' , function(data){
                        if(data ==1){
                            $.dialog.tips('配置成功');
                            \$tabs.tabs('load' , 0);
                        }else{
                            $.dialog.tips('配置失败，请重试');
                        }
                        
                    },'json');
                }
            }
           
END;
        $this->setJs($js);
        $this->displays();

    }

    public function record_data(){
        $this->setCondition(0 , 'eq' , 'group_id');
        $this->setCondition(2 , 'time' , 'create_time');
        $this->setCondition(4, 'eq' , 'type');
        $this->setOrder('create_time');
        $this->setLimit(100);
        $this->ServerAutoPlanModel->record_data($this->getConditions());
    }

    public function add(){

        $this->smarty->assign('AutoPlanType', CDict::$AutoPlanType);
        $this->smarty->assign('groups', $this->cgm->getChannelGroup(array(), array('id', 'name')));
        $this->smarty->display('serverAutoPlan/add.tpl');
    }

    public function add_action(){

        $group_id = $this->getParam('group_id');
        $name = $this->getParam('name');
        $type = (int)$this->getParam('type');
        $create_role_num = (int)$this->getParam('create_role_num');
        $pay_num = (int)$this->getParam('pay_num');

        if (empty($group_id)) {
            die(json_encode(array('ret' =>'0','msg'=>'请选择渠道组')));
        }

        if (empty($name)) {
            die(json_encode(array('ret' =>'0','msg'=>'请填写说明')));
        }

        if ($type === 0) {
            die(json_encode(array('ret' =>'0','msg'=>'请选择自动开服类型')));
        }

        if ($type === 1) {
            if ($create_role_num === 0 && $pay_num === 0) {
                die(json_encode(array('ret' =>'0','msg'=>'填写错误')));
            }
        }

        if ($type === 2) {
            if ($create_role_num === 0 || $pay_num === 0) {
                die(json_encode(array('ret' =>'0','msg'=>'填写错误')));
            }
        }

        $data = array(
            'group_id' => $group_id,
            'name'  => $name,
            'type' => $type,
            'create_role_num' => $create_role_num,
            'pay_num' => $pay_num,
            'status' => 0,
            'create_time' => time(),
        );

        $rs = $this->ServerAutoPlanModel->add($data);

        if($rs){
            (new LogAction())->logAdd("添加自动开服计划：{$rs}");
            //die('添加');exit;
            die(json_encode(array('ret' =>'1','msg'=>'添加成功')));

        }else{

            die(json_encode(array('ret' =>'0','msg'=>'添加失败')));
        }

    }

    function edit(){

        $id = $this->getParam('id');

        if (empty($id)) {

            die('请选择要编辑的自动开服计划');
        }

        $data = $this->ServerAutoPlanModel->getRow(['id', 'group_id', 'name', 'type', 'status', 'create_time', 'create_role_num', 'pay_num'], array('id' => $id));
        if (empty($data)) {

            die('查询不到数据');
        }

        $this->smarty->assign('AutoPlanType', CDict::$AutoPlanType);
        $this->smarty->assign('groups', $this->cgm->getChannelGroup(array(), array('id', 'name')));
        $this->smarty->assign('group_id',$data['group_id']);
        $this->smarty->assign('name', $data['name']);
        $this->smarty->assign('type', $data['type']);
        $this->smarty->assign('create_role_num',$data['create_role_num']);
        $this->smarty->assign('pay_num',$data['pay_num']);
        $this->smarty->assign('id',$data['id']);
        $this->smarty->assign('status', $data['status']);
        $this->smarty->display('serverAutoPlan/edit.tpl');
    }


    public function edit_action(){

        $group_id = $this->getParam('group_id');
        $name = $this->getParam('name');
        $type = (int)$this->getParam('type');
        $create_role_num = (int)$this->getParam('create_role_num');
        $pay_num = (int)$this->getParam('pay_num');
        $id = $this->getParam('id');
        $status = $this->getParam('status');

        if (empty($group_id)) {
            die(json_encode(array('ret' =>'0','msg'=>'请选择渠道组')));
        }

        if (empty($name)) {
            die(json_encode(array('ret' =>'0','msg'=>'请填写说明')));
        }

        if ($type === 0) {
            die(json_encode(array('ret' =>'0','msg'=>'请选择自动开服类型')));
        }

        if ($type === 1) {
            if ($create_role_num === 0 && $pay_num === 0) {
                die(json_encode(array('ret' =>'0','msg'=>'填写错误')));
            }
        }

        if ($type === 2) {
            if ($create_role_num === 0 || $pay_num === 0) {
                die(json_encode(array('ret' =>'0','msg'=>'填写错误')));
            }
        }

        $data = array(
            'group_id' => $group_id,
            'name'  => $name,
            'type' => $type,
            'create_role_num' => $create_role_num,
            'pay_num' => $pay_num,
            'status' => $status,
            'create_time' => time(),
        );

        $rs = $this->ServerAutoPlanModel->update($data , array('id'=>$id));

        if($rs){
            (new LogAction())->logAdd("编辑自动开服计划：{$id}");
            die(json_encode(array('ret' =>'1','msg'=>'编辑成功')));

        }else{

            die(json_encode(array('ret' =>'0','msg'=>'编辑失败')));
        }

    }

    public function config(){
        $id = $this->getParam('id');
        $status = $this->getParam('status');

        if(empty($id)){
            echo 0;die;
        }

        $rs = $this->ServerAutoPlanModel->getRow(['id', 'status'] , array('id' => $id));

        if (empty($rs)) {
            echo 0;die;
        }

        if ($status == $rs['status']) {
            echo 0;die;
        }

        if ($status != 3) {
            if($this->ServerAutoPlanModel->update(array('status' => $status) , array('id'=>$id))){
                (new LogAction())->logAdd("配置自动开服计划：{$id}");
                echo 1;
            }else{
                echo 0;
            }
        } else {
            if($this->ServerAutoPlanModel->delete(array('id'=>$id))){
                (new LogAction())->logAdd("删除自动开服计划：{$id}");
                echo 1;
            }else{
                echo 0;
            }
        }

    }


}