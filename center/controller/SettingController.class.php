<?php
!defined('IN_WEB') && exit('Access Denied');

class SettingController extends Controller{
    private $sm;

    public function __construct(){
        parent::__construct();
        $this->variableAction = new VariableAction();
        //日志记录
        $this->logAction = new LogAction();
        $this->sm = new SettingModel();
        $this->smc = new ServerconfigModel();
    }
    
    
    function index(){
        $tabs = array(
            array('title' => '登录白名单', 'url' => 'admin.php?ctrl=setting&act=login_white_ip'),
        );
        $this->tabs($tabs);
    }
    
    function login_white_ip(){
        $ip_arr = $this->variableAction->variableGet("login_white_ip");
        $this->smarty->assign("ip_text", implode("\n", $ip_arr));
        $this->smarty->display("setting/login_white_ip.tpl");
    }
    
    function login_white_ip_save(){
        $this->logAction->logAdd("更新登录ip白名单");
        $ip_arr = textareaStrToArr($_POST['ip_text']);
        $return = $this->variableAction->variableSet("login_white_ip", $ip_arr);
        echo $return? "设置成功": "设置失败";
    }

    //热更新（菜单）
    function hupdate(){
        $tabs = array(
            array('title' => '热更新', 'url' => 'admin.php?ctrl=setting&act=hotupdate'),
            array('title' => '热更新记录', 'url' => 'admin.php?ctrl=setting&act=hotupdate_log')
        );
        $this->tabs($tabs);
    }

    //热更新（子菜单）
    public function hotupdate()
    {
        $servers = $this->sm->getServerByTrait([],true);
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach($channelGroups as $key=>$val){
            $groups[$val['id']] = $val['name'];
        }
        foreach($groups as $key=>$v){
            if(!in_array($key,array_keys($servers))){
                unset($groups[$key]);
            }
        }

        $this->smarty->assign('hotUpdateType', Pact::$HotUpdateType);
        $this->smarty->assign('hotUpdateOperate', Pact::$HotUpdateOperate);
        $this->smarty->assign('servers',$servers);
        $this->smarty->assign('groups',$groups);
        $this->smarty->display('setting/hupdate.tpl');
    }
    public function hupdate_action()
    {
        $save = $this->getParam('save');
        $type = $this->getParam('type');
        //发送格式
        //type Bg_Hotupdate = [number, Array<string>];
        //type Bg_ReloadCfg = [number];
        if ($save && $type) {
            $server = $this->getParam('server');
            $channelGroup = $this->getParam('channel_group');
            $data = array(
                'str' => $this->getParam('str'),
                'netinfo' => $this->getParam('netinfo'),
                'type' => $type,
                'channel_group' =>empty($channelGroup[0])?array():$channelGroup,
                'server' => empty($server[0]) ? array() : $server,
            );

            $this->logAction->logAdd("热更新服务器:" . json_encode($data));
            (new HoTUpdateModel())->hupdate($data);
        }
    }

    public function hotupdate_log()
    {
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(1 , 0 , 1 , 0));
        $this->setFields(array('渠道组' , '服务器' , '操作类型' , '协议', '操作人', 'ip', '发送的数据', '时间'));
        $this->setSource(' Setting' , 'hotupdate_log_data');
        $this->setLimit('100');
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function hotupdate_log_data()
    {
        $this->setCondition(0 , 'time' , "a.create_time");
        $this->setCondition(1 , 'scp');
        $this->setOrder('a.create_time', 'desc');
        $this->setLimit();
        $method = 'record_data';
        //$this->call('HotUpdateModel' , $method , $this->getConditions());
        (new HoTUpdateModel())->$method($this->getConditions());
    }

    publiC function websocketConfig()
    {
        $server = $this->getParam('server');

        $server_config = $this->smc->getConfig($server, ['server_id', 'websocket_host', 'websocket_port']);

        $data = [];

        foreach($server_config as $key => $value) {
            $data[$value['server_id']]['websocket_host'] = $value['websocket_host'];
            $data[$value['server_id']]['websocket_port'] = $value['websocket_port'];
        }

        echo json_encode($data);
    }

    //GM指令
    public function gmcmd()
    {
        $servers = $this->sm->getServerByTrait([],true);
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach($channelGroups as $key=>$val){
            $groups[$val['id']] = $val['name'];
        }
        $this->smarty->assign('servers',$servers);
        $this->smarty->assign('groups',$groups);
        $this->smarty->display('setting/gmcmd.tpl');
    }

    public function gmcmd_action()
    {
        $save = $this->getParam('save');
        if($save){
            $server = $this->getParam('server');
            $roleIdArr = [];

            $type = $this->getParam('type');

            if ($type == 0) {
                exit(json_encode(['msg' => '类型错误']));
            }

            if (!Pact::$GMcommandPact) {
                exit(json_encode(['msg' => '代码中未填写GM协议, 请联系后台管理员']));
            }

            if ($type == 1) {
                $role_id = $this->getParam('role_id');
                array_push($roleIdArr, (int)$role_id);
            } else {
                $role_id_start = $this->getParam('role_id_start');
                $role_id_end = $this->getParam('role_id_end');

                if ($role_id_start > $role_id_end) {
                    exit(json_encode(['msg' => '角色id区间有误']));
                }

                for ($role = $role_id_start; $role < $role_id_end + 1; $role++) {
                    array_push($roleIdArr, (int)$role);
                }
            }

            foreach ($roleIdArr as $role) {
                $data = array(
                    'cmd' => $this->getParam('cmd'),
                    'server' => $server,
                    'role_id' => $role,
                    'pact' => Pact::$GMcommandPact
                );
                $this->logAction->logAdd("发送GM指令:" . json_encode($data));
                $this->sm->gmcmd($data);
            }
        }
    }

    //一键清除央服统计数据和单服数据
    public function cleansql()
    {
        $servers = $this->sm->getServerByTrait([], true);
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach ($channelGroups as $key => $val) {
            $groups[$val['id']] = $val['name'];
        }
        foreach ($groups as $key => $v) {
            if (!in_array($key, array_keys($servers))) {
                unset($groups[$key]);
            }
        }

        $this->smarty->assign('servers', $servers);
        $this->smarty->assign('groups', $groups);
        $this->smarty->display('setting/cleansql.tpl');
    }


    public function cleansql_action()
    {
        $save = $this->getParam('save');

        if ($save) {

            $server = $this->getParam('server');
            $this->logAction->logAdd("清除服务器数据库: server:" . $server);
            $this->sm->cleansql($server);

        }
    }
}