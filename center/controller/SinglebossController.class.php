<?php
!defined('IN_WEB') && exit('Access Denied');

class SingleBossController extends Controller{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日成功连接上游戏服务器并且≥50级的人数。<br>';
        $header .= '参与人数：该日进行过单人BOSS挑战玩法（无论成功或失败，含扫荡）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '50级人均参与次数 = 该日全服挑战50级单人BOSS挑战玩法的总次数（成功才算，含扫荡） / 该日挑战50级单人BOSS玩法的玩家数（成功才算，每个玩家ID只算一次，需去重）   （确定到小数点后两位，四舍五入）<br>';
        $header .= '后续1转、2转…7转依次类推。<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '参与人数' , '参与度', '50级人均参与次数', '1转人均参与次数', '2转人均参与次数', '3转人均参与次数', '4转人均参与次数', '5转人均参与次数', '6转人均参与次数', '7转人均参与次数'));
        $this->setSource('SingleBoss' , 'index_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('single_data' , 'single_export');
        $this->call('SingleBossModel' , $method , $this->getConditions());
    }
}