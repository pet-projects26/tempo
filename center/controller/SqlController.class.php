<?php
!defined('IN_WEB') && exit('Access Denied');

class SqlController extends Controller{
	public function __construct(){
        parent::__construct();
        $this->sc = new  ServerconfigModel();
        
    }
	
   //根据sql语句查询数据
	public function getData(){
		//渠道组
		$server = (new ServerModel())->getServerByTrait([],true);
		//服务器，渠道组
		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

		$channelGroupRow = array();
		foreach ($channelGroupRes as $row) {
			$group_id = $row['id'];
			$channelGroupRow[$group_id] = $row['name'];
		}

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
		$this->smarty->display('server/data.tpl');
	}
	
	public function data(){
		
		$sql = $_POST['sql'];
		$sql = str_replace(';','' ,$sql);
		$server = $_POST['server'];
		
		$limit=strstr($sql,'limit');
		
		
		if(stripos($sql , 'delete') !==false || stripos($sql , 'drop') !==false || stripos($sql , 'truncate') !==false || stripos($sql , 'update') !==false){
			exit(json_encode(array('code' => 400 , 'msg' => '不允许修改表数据')));
		}
		
		if(stripos($sql,'select') !== false && stripos($sql , 'limit') === false){
			$sql .= ' limit 20';
		}
		
		$arr  = array('query' => array('data' => array($sql)));
	 
		$res  = (new ServerconfigModel())->makeHttpParams(array($server), $arr);
  		
		$urls = $res['urls'];
		
		$params = $res['params'];
		
		$res = Helper::rolling_curl($urls, $params);
		

		
		
		//foreach($server as $k=>$v){
			
			$data = json_decode($res[$server],true);
			
			if(!is_array($data)){
				
				exit(json_encode(array('code' => 400 , 'msg' => json_encode($res))));
			}
		
			$data = $data['data']['query'][0];
		//}
		if($data){
			foreach($data[0] as $k=>$v){
				$key[] = $k;
			}
			$html ='<div style="padding:5px">';
			$html .= '<table cellpadding="0" cellspacing="0" border="0" class="table_list dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">';
			$html .= '<thead>';
			$html .= '<tr>';
			foreach($key as $k => $v){
				$html .= ' <th rowspan="1" colspan="1">'.$v.'</th>';	
			}
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			
			foreach($data as $k=>$v){
				if($k %2 == 0){
					$html .= '<tr class="odd">';	
				}else{
					$html .= '<tr class="even">';	
				}
				foreach($key as $kk =>$vv){
					$html .= '<td>'.$v[$vv].'</td>';	
				}
				$html .= '</tr> ';
			}
			
			$html .= ' </tbody>';
			$html .= ' </table>';
			$html .= '</div>';
			$html .= '';
			$html .= '';
			
			exit(json_encode(array('code' => 200 , 'msg' => $html)));
				
		}else{
			exit(json_encode(array('code' => 400 , 'msg' => '查询不到数据')));
			
		}	
		
	}
	
	public function activity(){

		//渠道组
		$server = (new ServerModel())->getServerByTrait();
		//服务器，渠道组
		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();

		$channelGroupRow = array();
		foreach ($channelGroupRes as $row) {
			$group_id = $row['id'];
			$channelGroupRow[$group_id] = $row['name'];
		}

        $this->smarty->assign('groups', $channelGroupRow);
        $this->smarty->assign('servers', $server);
		$this->smarty->display('sql/activity.tpl');	
	}
	
	public function activity_data(){
		//根据传递的服务器标识获取活动参数
		$server = $_POST['server'];
		$activity =$this->sc->call('Sql' , 'getData' , array() ,  $server);
		
		$activity = $activity[$server];

		if($activity){
			$activityType = CDict::$activityType;
			$i = 0;
			foreach($activity as $k=>$v){
				$arr[$i]['id'] = $v['_id'];
				$arr[$i]['aid'] = $v['aId'];
				$arr[$i]['activityType'] = $activityType[$v['aId']];
				$arr[$i]['name'] = $v['name'];
				$arr[$i]['tmType'] = $v['tmType'];
				$arr[$i]['start'] = !empty($v['tm']['start']) ? date('Y-m-d H:i:s' , $v['tm']['start'] ) : '';
				$arr[$i]['close'] = !empty($v['tm']['close']) ? date('Y-m-d H:i:s' , $v['tm']['close'] ) : '';
				$arr[$i]['state'] = $v['state'] ==0  ? '开启' : '关闭';
				$arr[$i]['openLvl'] = $v['openLvl'];

				$v = json_encode($v);
				//替换掉NumberLong转成的value
				$v = str_replace('{"value":"','' , $v);
				$v = str_replace('"}','', $v);

				$arr[$i]['all'] = $v;
				$i++;
			}

			//构造页面
			$html = '';
			$html ='<div style="padding:5px">';
			$html .= '<table cellpadding="0" cellspacing="0" border="0" class="table_list dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">';
			$html .= '<thead>';
			$html .= '<tr>';
			//表头
			$html .= ' <th rowspan="1" colspan="1">ID</th>';
			$html .= ' <th rowspan="1" colspan="1">活动类型</th>';
			$html .= ' <th rowspan="1" colspan="1">活动类型</th>';
			$html .= ' <th rowspan="1" colspan="1">活动名称</th>';
			$html .= ' <th rowspan="1" colspan="1">时间类型</th>';	
			$html .= ' <th rowspan="1" colspan="1">开始时间</th>';	
			$html .= ' <th rowspan="1" colspan="1">结束时间</th>';	
			$html .= ' <th rowspan="1" colspan="1">状态</th>';
			$html .= ' <th rowspan="1" colspan="1">开启等级</th>';
			$html .= ' <th rowspan="1" colspan="1">查看详情</th>';

			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			
			foreach($arr as $k=>$v){
				if($k %2 == 0){
					$html .= '<tr class="odd">';	
				}else{
					$html .= '<tr class="even">';	
				}
				
				$html .= '<td>'.$v['id'].'</td>';
				$html .= '<td>'.$v['aid'].'</td>';
				$html .= '<td>'.$v['activityType'].'</td>';
				$html .= '<td>'.$v['name'].'</td>';
				$html .= '<td>'.$v['tmType'].'</td>';
				$html .= '<td>'.$v['start'].'</td>';
				$html .= '<td>'.$v['close'].'</td>';
				$html .= '<td>'.$v['state'].'</td>';
				$html .= '<td>'.$v['openLvl'].'</td>';	

				$html .= '<td> <button style="margin-bottom:5px;" class="gbutton" onclick="_view(\''.$v['id'].'\')">查看详情</button></td>';
				
				$html .= '</tr> ';

				$html .= '<span class = "'.$v['id'].'" style="display:none;">'.$v['all'].'</span>';
			}
			
			$html .= ' </tbody>';
			$html .= ' </table>';
			$html .= '</div>';
			$html .= '';
			
			exit(json_encode(array('code' => 200 , 'msg' => $html)));

		}else{
			exit(json_encode(array('code' => 400 , 'msg' => '查询不到数据')));
		}
			
	} 

	public function csv(){
		
		$this->smarty->display('sql/csv.tpl');	

	}
	public function uploadCsv(){
		
		if ($_FILES["file"]["error"] > 0){  
			die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "错误: " . $_FILES["file"]["error"] . "<br />")));
		}else{ 
			$filename = $_FILES["file"]["name"];
			$suffix = substr($filename, strrpos($filename, '.') + 1);
			$filetype = array('csv' ,'xlsx' , 'xls');
			
			if(!in_array($suffix , $filetype)){

				die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "只允许上传Excel文件")));
			}else if($_FILES['file']['size'] > 20*1024*1024){
				
				die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "文件大小不允许超过20M")));
			}else{
				if(strtolower($suffix) == 'csv'){
					$handle = fopen($_FILES['file']['tmp_name'],"r");
					$csv = array();
					while ($row = fgetcsv($handle)) {
						foreach($row as $k=>$v){
							$csv[$k][] = iconv('gbk','utf-8',$v);
						}
					}
				}else{
					$csv = Util::readExcel($_FILES["file"]["tmp_name"]);
				}
				echo json_encode(array('data' => json_encode($csv) , 'status' => 1 , 'e' => "文件上传成功"));
				
			}
		}
			
	}

	
}