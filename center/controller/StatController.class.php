<?php
!defined('IN_WEB') && exit('Access Denied');

class StatController extends Controller{
    public function __construct(){
        parent::__construct();
    }

    public function remain_account(){

        $type = $this->getParam('type');

        $header = '说明：<br>';
        $header = '1. 注册数:是包括老玩家的(不包括提审服)<br>';
        $header .= '2. 表头为第N天的标题指第N天留存（例如2017-03-02的第2天指2017-03-03），数据格式是 人数(百分比)<br>';
        $header .= '3. 第N天留存率 = 该日期后的第N天有登录的注册数 / 第N天注册数';
        $this->setHeader($header);
        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        $this->setField('日期', 100);
        $this->setFields(array('注册数', '第2天', '第3天', '第4天', '第5天', '第6天', '第7天', '第8天', '第9天', '第10天', '第11天', '第12天', '第13天', '第14天', '第15天', '第16天', '第17天', '第18天', '第19天', '第20天', '第21天', '第22天', '第23天', '第24天', '第25天', '第26天', '第27天', '第28天', '第29天', '第30天'));
        $this->setSource('stat', 'remain_account_data', 1, array('type' => $type));
        $this->displays();
    }

    public function remain_account_data(){
        $this->setCondition(0, 'time', 'time');
        $this->setCondition(1, 'scp', '', '', array(1, 0, 1, 1));
        $this->setCondition(2, 'eq', 'type', $this->getParam('type'));
        $this->setOrder('time');
        $this->setLimit();

        $method = $this->setDataExport('remain_account_data', 'remain_account_export');
        $this->call('StatModel', $method, $this->getConditions());
    }

    //每日充值（菜单）
    public function daily(){

        $header  = '说明：<br>';
        $header .= '1. 新增充值人数：当天新增玩家并在当天进行充值的角色数 <br>';
        $header .= '2. 充值人数：当天充值的人数<br>';
        $header .= '3. 新增充值金额： 新增充值人数当天充值的金额<br>';
        $header .= '4. 登录ARPU = 充值总额 / 登录人数 <br>';
        $header .= '5. 新增充值ARPU = 新增充值总额 / 充值人数 <br>';
        $header .= '6. ARPU = 充值总额 / 充值人数 <br>';
        $header .= '7. 注册ARPU = 充值总额 / 注册人数<br> ';
        $header .= '8. 首冲人数：当天第一次充值的人数<br> ';


        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        $this->setFields(array('日期' , '新增充值人数' ,'新增充值总额' , '首充人数' , '首充金额' , '充值总额' , '充值人数' , '充值次数' ,'新增充值ARPU' , 'ARPU' ,'登录ARPU', ' 注册ARPU ' ));
        $this->setSource('stat' , 'daily_data' , 1);
        $this->displays();
    }

    public function daily_data(){
        $this->setCondition(0 , 'date' , 'time');
        $this->setCondition(1 , 'scp' ,'','',array(1,0,1,1));
        $this->setOrder('time');
        $this->setLimit();

        $method = $this->setDataExport('daily_data' , 'daily_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    public function ltv_index()
    {
        $tabs = array(
            array('title' => '创角LTV', 'url' => 'admin.php?&ctrl=stat&act=rolecreate'),
            array('title' => '注册LTV', 'url' => 'admin.php?&ctrl=stat&act=register'),
        );
        $this->tabs($tabs);
    }

    public function rolecreate()
    {
        $header  = '说明：<br>';
        $header .= '1. 创角数 指当日创建角色数<br>';
        $header .= '2. 表头为N天 指该行日期作为开始日期算的第N天的日期(例如，日期是2016-02-03，1天指2016-02-03，2天是2016-02-04)<br>';
        $header .= '3. 客户周期总价值(创角)(简称LTV，即表头为N天的值的计算方式) = 当天新增创角到N天的累加充值  / 创角数 (四舍五入保留2位小数)';
        $this->setHeader($header);
        //$starttime = date('Y-m-d' , strtotime('-1 week'));
        //$endtime = date ('Y-m-d' , time() );
        $this->setSearch('日期' , 'range_date');//, array($starttime,$endtime)
        $this->setSearch('' , 'scp' , array(1 , 0 , 1 , 1));
        $this->setField('日期' , 70);
        $this->setField('创角数' , 80);
        $this->setFields(array('1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天'));
        $this->setSource('stat' , 'rolecreate_data' , 1);
        $this->displays();
    }
    public function rolecreate_data(){

        $this->setCondition(0 , 'date' , 'time');
        $this->setCondition(1 , 'scp' ,'','',array(1,0,1,1));
        $this->setCondition(2, 'eq', 'type', 1);
        $this->setOrder('time');
        $this->setLimit();
        $method = $this->setDataExport('ltv_data' , 'ltv_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    public function register(){
        $header  = '说明：<br>';
        $header .= '1. 注册数 指当日注册帐号人数<br>';
        $header .= '2. 表头为N天 指该行日期作为开始日期算的第N天的日期(例如，日期是2016-02-03，1天指2016-02-03，2天是2016-02-04)<br>';
        $header .= '3. 客户周期总价值（注册）(简称LTV，即表头为N天的值的计算方式) = 当天注册帐号数N天的累加充值总额 / 注册帐号数 (四舍五入保留2位小数)';
        $this->setHeader($header);
        //$starttime = date('Y-m-d' , strtotime('-1 week'));
        //$endtime = date ('Y-m-d' , time());
        $this->setSearch('日期' , 'range_date');//, array($starttime,$endtime)
        $this->setSearch('' , 'scp' , array(1 , 0 , 1 , 1));
        $this->setField('日期' , 70);
        $this->setField('注册数' , 80);
        $this->setFields(array('1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天'));
        $this->setSource('stat' , 'register_data' , 1,array('type'=>2));
        $this->displays();
    }
    public function register_data(){
        $this->setCondition(0 , 'date' , 'time');
        $this->setCondition(1 , 'scp','','',array(1,0,1,1));
        $this->setCondition(2, 'eq', 'type', 2);
        $this->setOrder('time');
        $this->setLimit();
        $method = $this->setDataExport('ltv_data' , 'ltv_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    public function remain()
    {

        $type = $this->getParam('type');

        $header = '说明：<br>';
        $header .= '1. 表头为第N天的标题指第N天留存（例如2017-03-02的第2天指2017-03-03），数据格式是 人数(百分比)<br>';
        $header .= '2. 第N天留存率 = 该日期后的第N天有登录的角色数 / 第N天创角数';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(1 , 0 , 1 , 1));
        $this->setField('日期' , 100);
        $this->setFields(array('创角数' , '第2天' , '第3天' , '第4天' , '第5天' , '第6天' , '第7天', '第8天' , '第9天' , '第10天' , '第11天' , '第12天' , '第13天' , '第14天' , '第15天' , '第16天' , '第17天' , '第18天' , '第19天' , '第20天' , '第21天' , '第22天' , '第23天' , '第24天' , '第25天' , '第26天' , '第27天' , '第28天' , '第29天' , '第30天' ));
        $this->setSource('stat' , 'remain_data' , 1,array('type'=>$type));
        $this->displays();
    }

    public function remain_data()
    {
        $this->setCondition(0 , 'time' , 'time');
        $this->setCondition(1 , 'scp','','',array(1,0,1,1));
        $this->setCondition(2, 'eq', 'type', $this->getParam('type'));
        $this->setOrder('time');
        $this->setLimit();

        $method = $this->setDataExport('remain_data' , 'remain_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    public function summary_index()
    {
        $tabs = array(
            array('title' => '角色数据汇总', 'url' => 'admin.php?&ctrl=stat&act=summary_role'),
            //array('title' => '帐号数据汇总', 'url' => 'admin.php?&ctrl=stat&act=summary_account'),
        );
        $this->tabs($tabs);
    }

    public function summary_role()
    {
        $header = '说明：<br>';
        $header .= '1. <span style="color:red">注册数 指成功创角的帐号数（未创角的账号数无法统计入内)</span><br>';
        $header .= '2. 创角数 指当天创建角色成功的数量<br>';
        $header .= '3. 登录数 指当天登录游戏的角色数量<br>';
        $header .= '4. 活跃数 指当天登录游戏时长总和大于等于30分钟的角色数量<br>';
        $header .= '5. 老角色数 指当天有登录的非当天创建的角色数量<br>';
        $header .= '6. 充值人数 指当天充值成功的角色数量<br>';
        $header .= '7. 登录付费率 = 充值人数 / 登录数<br>';
        $header .= '8. 付费AP = 充值金额 / 充值人数<br>';
        $header .= '9. 新充值人数 指当日创建的且当日有进行充值的角色数<br>';
        $header .= '10. 新充值金额 指当日创建的且当日有进行充值的角色的充值金额<br>';
        $header .= '11. 新增付费率 = 新充值人数 / 创角数<br>';
        $header .= '12. 新充值AP = 新充值金额 / 新充值人数<br>';
        $header .= '13. 老充值人数 = 指当天充值成功的老角色数<br>';
        $header .= '14. 老充值金额 = 指当天充值成功的老角色的总充值金额<br>';
        $header .= '15. 老付费率 = 老充值人数 / 老角色数<br>';
        $header .= '16. 老充值AP = 老充值金额 / 老充值人数<br>';
        $header .= '17. 登录AP = 充值金额 / 登录数<br>';
        $header .= '18. 次留和3留 指次日留存率和三日留存率 (之后的7留 ，15留 ，30留同理)<br>';
        $header .= '19. 三日留存率 = 当天有登录且在三天前也有登录的角色数量 / 总角色数量 <br> ';
        $header .= '20. 注册AP = 新增充值金额 / 注册数 <br>';
        $header .= '21. 付费率 = 充值人数/创角数 <br>';
        $header .= '22. 活跃AP = 活跃充值金额/活跃充值人数 <br>';
        $header .= '<span style="color:red;">23. 默认是显示最近七天的数据</span><br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(1, 0, 1 , 1));
        $this->setField('日期' , 100);
        $this->setFields(array(
            '注册数' , '创角数' , '登录数' , '活跃数' , '活跃AP', '老角色数' , '充值人数' , '充值金额' , '登录付费率' , '付费AP' , '新充值人数' ,
            '新充值金额' , '新增付费率' , '新充值AP' , '老充值人数' , '老充值金额' , '老付费率' , '老充值AP' , '登录AP' , '注册AP' ,
            '最高在线数' ,  '次留' , '3留' , '4留' , '5留' , '6留' , '7留' , '15留' , '30留'
        ));
        //$this->smarty->assign('select', 'radio');
        $this->setSource('stat' , 'summary_role_data' , 1);
        $this->displays();
    }

    public function summary_role_data()
    {
        $this->setCondition(0, 'date', 's.time');
        $this->setCondition(1 , 'scp','','',array(1,0,1,1));
        $this->setOrder('s.time');
        $this->setLimit();
        $method = $this->setDataExport('summary_role_data' , 'summary_role_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    public function summary_account()
    {

        $header = '说明：<br>';
        $header .= '1. 注册数 指当天创号成功的玩家数量<br>';
        $header .= '2. 登录数 指当天登录游戏的角色数量<br>';
        $header .= '3. 老角色数 指当天有登录的非当天创建的角色数量<br>';
        $header .= '4. 充值人数 指当天充值成功的角色数量<br>';
        $header .= '5. 登录付费率 = 充值人数 / 登录数<br>';
        $header .= '6. 付费AP = 充值金额 / 充值人数<br>';
        $header .= '7. 新充值人数 指当日创建的且当日有进行充值的角色数<br>';
        $header .= '8. 新充值金额 指当日创建的且当日有进行充值的角色的充值金额<br>';
        $header .= '9. 新增付费率 = 新充值人数 /注册数<br>';
        $header .= '10. 新充值AP = 新充值金额 / 新充值人数<br>';
        $header .= '11. 老充值人数 = 指当天充值成功的老角色数<br>';
        $header .= '12. 老充值金额 = 指当天充值成功的老角色的总充值金额<br>';
        $header .= '13. 老付费率 = 老充值人数 / 老角色数<br>';
        $header .= '14. 老充值AP = 老充值金额 / 老充值人数<br>';
        $header .= '15. 登录AP = 充值金额 / 登录数<br>';
        $header .= '16. 注册AP = 充值金额 / 注册数 ';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(1, 0 , 1 , 1));
        $this->setField('日期' , 100);
        $this->setFields(array(
            '注册数' , '登录数' ,  '老角色数' , '充值人数' , '充值金额' , '登录付费率' , '付费AP' , '新充值人数' ,
            '新充值金额', '新增付费率', '新充值AP', '老充值人数', '老充值金额', '老付费率', '老充值AP', '登录AP', '注册AP'
        ));

        $this->setSource('stat' , 'summary_account_data' , 1);
        $this->displays();
    }

    public function summary_account_data()
    {
        $this->setCondition(0, 'date', 'time');
        $this->setCondition(1 , 'scp' ,'','',array(1,0,1,1));
        $this->setOrder('time');
        $this->setLimit();
        $method = $this->setDataExport('summary_account_data' , 'summary_account_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    public function activity(){
        $activity = array(
            71 => '充值投资',
            102 => '苍穹云购',
            108 => '幸运鉴宝',
            114 => '砸蛋',
            109 => '燃放烟花',
            111 => '全民嗨',
            20 => 'vip购买',
        );

        $header  = '1.消耗占比 = 消耗到该活动的元宝数/总消耗元宝<br>';
        // $header .= '2.库存消耗占比 = 消耗到该活动的元宝/库存元宝<br>';
        $header .= '2.消耗人数占比 = 参与该活动的角色数/参与消耗角色数<br>';


        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('类型' , 'select',$activity);
        $this->setSearch('' , 'scp' , array(1, 0 , 1 , 1));
        $this->setFields(array(
            '日期', '总消耗元宝', '总消耗角色数', '充值元宝', '充值角色数', '库存元宝', '该活动消耗的元宝', '参与该活动的角色数', '消耗占比',  '消耗人数占比'
        ));
        $this->setSource('stat' , 'activity_data' , 1);
        $this->displays();
    }

    public function activity_data(){
        $this->setCondition(0 , 'date' , 'time');
        $this->setCondition(1,'eq' , 'type' , '71');
        $this->setCondition(2 , 'scp','','',array(1,0,1,1));
        $this->setOrder('time');
        $this->setLimit();
        $method = $this->setDataExport('activity_data' , 'activity_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    //创角数（菜单）
    public function create(){
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(1 , 0 , 1 ,1));
        $this->setField('日期' , 70);
        $this->setField('创角数');
        $this->setField('创角设备数');
        $this->setDataTableId('create');
        $this->setSource('stat' , 'create_data' , 1);

        $js = <<< END
   			$(function () {
				$('#create tr').die().live("click", function () {
					if (\$create.fnIsOpen(this))
						\$create.fnClose(this)
					else {
						var elem = \$(this).find('.data')
						var postData = new Object
						postData.date = elem.attr('data-date')
						postData.server = elem.attr('data-server')
                        postData.package = elem.attr('data-package')
						var tr = this
						$.post('admin.php?ctrl=stat&act=rolecreate_detail', postData, function (data) {
						   \$create.fnOpen(tr, data)
						})
					}
				})
			})

END;
        $this->setJs($js);
        $this->displays();
    }
    public function create_data(){
        $this->setCondition(0 , 'date' , 'unix_timestamp(time)');
        $this->setCondition(1 , 'scp', '','', array(1,0,1,1));
        $this->setOrder('time','desc');
        $this->setLimit();
        $method = $this->setDataExport('create_data' , 'create_export');
        $this->call('StatModel' , $method , $this->getConditions());
    }

    //获取当前日期创角数和创角设备数的详细信息
    function rolecreate_detail()
    {

        $m = new StatModel();
        $data = $m->getDetail($_POST['date'], $_POST['server'], $_POST['package']);
        $this->smarty->assign('data', $data);
        echo $this->smarty->fetch('online/rolecreate_detail.tpl');
    }




}