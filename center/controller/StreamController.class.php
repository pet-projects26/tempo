<?php
!defined('IN_WEB') && exit('Access Denied');

class StreamController extends Controller{

    public function __construct(){
        parent::__construct();
        set_time_limit(0); //执行时间无限
        ini_set('memory_limit', '-1'); //内存无限
    }

    public function set()
    {
        set_time_limit(0); //执行时间无限
        ini_set('memory_limit', '-1'); //内存无限
    }
    
    public function index(){

    	$tabs = array(
            // array('title' => '汇总流水', 'url' => 'admin.php?&ctrl=stream&act=summary'),
            // array('title' => '装备强化流水', 'url' => 'admin.php?&ctrl=stream&act=equip'),
            //array('title' => '装备替换流水', 'url' => 'admin.php?&ctrl=stream&act=equip_replace'),
            array('title' => '道具流水', 'url' => 'admin.php?&ctrl=stream&act=prop'),
            array('title' => '货币流水', 'url' => 'admin.php?&ctrl=stream&act=money'),
            array('title' => '等级流水', 'url' => 'admin.php?&ctrl=stream&act=level'),
            array('title' => '登录流水', 'url' => 'admin.php?&ctrl=stream&act=login'),
            array('title' => '邮箱流水', 'url' => 'admin.php?&ctrl=stream&act=email'),
            //array('title' => '客户端流水', 'url' => 'admin.php?&ctrl=stream&act=client'),
            // array('title' => '任务流水', 'url' => 'admin.php?&ctrl=stream&act=task'),
            //array('title' => '仙器流水', 'url' => 'admin.php?&ctrl=stream&act=ride'),
            //array('title' => '战斗力流水', 'url' => 'admin.php?&ctrl=stream&act=ce'),
            //array('title' => '宝石流水', 'url' => 'admin.php?&ctrl=stream&act=gem')
    	);
    	$this->tabs($tabs, 'log');
    }

    //汇总流水
    public function summary()
    {
        $this->set();
        $header = '说明：<br>';
        $header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
        $header .= '目前汇总了 物品消耗流水, 金钱流水, 登录流水, 等级流水 <br>';
        $this->setHeader($header);
        $this->setSearch('时间', 'range_time');
        $this->setSearch('角色ID');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setFields(array('角色ID', '角色名', '流水内容', '时间'));
        $this->smarty->assign('select', 'select');
        $this->setSource('stream', 'summary_data');
        $this->displays();
    }

    public function summary_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'eq', 'role_id');
        $this->setCondition(2, 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $this->call('StreamModel', 'summary_data', $this->getConditions());
    }


    //装备强化流水
    public function equip(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('部位','等级','时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'equip_data' , 1);
    	$this->displays();
    }
    
    public function equip_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('equip_data' , 'equip_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //装备替换流水
    public function equip_replace(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('装备');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('类型','装备id','装备','时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'equip_replace_data' , 1);
    	$this->displays();
    }
    
	public function equip_replace_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'eq' , 'item_id');
    	$this->setCondition(3 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('equip_replace_data' , 'equip_replace_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //道具流水
    public function prop(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('类型','select',CDict::$itemSource);
    	$this->setSearch('消耗类型', 'select', CDict::$itemConsumeType);
    	$this->setSearch('道具');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('类型','道具id','道具','数量','时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'prop_data' , 1);
    	$this->displays();
    }
    
	public function prop_data(){
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'eq' , 'role_id');
        $this->setCondition(2 , 'eq' , 'source');
        $this->setCondition(3, 'eq', 'type');
        $this->setCondition(4 , 'eq' , 'item_id');
    	$this->setCondition(5 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('prop_data' , 'prop_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //金钱流水
    public function money(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('类型','select',CDict::$itemSource);
        $this->setSearch('消耗类型', 'select', CDict::$itemConsumeType);
		$this->setSearch('金钱类型','select',CDict::$money);
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
        $this->setFields(array('类型', '货币类型', '数量', '该货币类型余额', '时间', '角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'money_data' , 1);
    	$this->displays();
    }
    
	public function money_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'eq' , 'coin_source');
        $this->setCondition(3, 'eq', 'type');
    	$this->setCondition(4 , 'eq' , 'money_type');
    	$this->setCondition(5 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('money_data' , 'money_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //等级流水
    public function level(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('等级' , '时间' ,'角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'level_data' , 1);
    	$this->displays();
    }
    
	public function level_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('level');
    	$this->setLimit();
    	$method = $this->setDataExport('level_data' , 'level_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //登录流水
    public function login(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('等级' , 'IP','登录时间', '登出时间','角色id', '在线时长'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'login_data' , 1);
    	$this->displays();
    }
    
	public function login_data(){
    	$this->setCondition(0 , 'time' , 'start_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('start_time');
    	$this->setLimit();
    	$method = $this->setDataExport('login_data' , 'login_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }

    //邮件流水
    public function email(){
        $this->set();
        $header  = '说明：<br>';
        $header .= '1.请选择服务器,2.时间默认是今天的,如果选择的时间范围操作一周,将只显示一周的数据 3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
        $this->setHeader($header);
        $this->setSearch('时间' , 'range_time');
        $this->setSearch('角色ID');
        $this->setSearch('操作类型','select',CDict::$itemEmail);
        $this->setSearch('邮箱名');
        //$this->setSearch('邮箱名');
        $this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
        $this->setFields(array('类型','邮件id','邮件名','时间','角色id'));
        $this->smarty->assign('select','select');
        $this->setSource('stream' , 'email_data' , 1);
        $this->displays();
    }

    public function email_data(){
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'eq' , 'role_id');
        $this->setCondition(2 , 'eq' , 'type');
        $this->setCondition(3 , 'eq' , 'email_name');
        //$this->setCondition(4 , 'eq' , 'email_name');
        $this->setCondition(4 , 'scp');
        $this->setOrder('create_time');
        $this->setLimit();
        $method = $this->setDataExport('email_data' , 'email_export');
        $this->call('StreamModel' , $method , $this->getConditions());
    }

    //客户端流水
    public function client(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('渠道' , '包',  '机型','网络类型','mac','udid','memory','时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'client_data' , 1);
    	$this->displays();
    }
    
	public function client_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('client_data' , 'client_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //任务流水
    public function task(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('任务类型','select',array(''=>'-',1=>'主线任务',2=>'支线任务','5'=>'仙盟任务'));
    	$this->setSearch('任务ID' );
    	$this->setSearch('任务名称' );
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('任务ID' ,'任务类型' , '任务名称','状态' ,'时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'task_data' , 1);
    	$this->displays();
    }
    
	public function task_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'eq' , 'type');
    	$this->setCondition(3 , 'eq' , 'task_id');
    	$this->setCondition(4 , 'eq' , 'task_name');
    	$this->setCondition(5 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('task_data' , 'task_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //坐骑流水
    public function ride(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('阶级' ,'等级' , '时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'ride_data' , 1);
    	$this->displays();
    }
    
    public function ride_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('ride_data' , 'ride_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //战斗力流水
    public function ce(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('战斗力','时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'ce_data' , 1);
    	$this->displays();
    }
    
    public function ce_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('ce_data' , 'ce_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
    //宝石流水
    public function gem(){
        $this->set();
    	$header  = '说明：<br>';
    	$header .= '1.请选择服务器,2.时间默认是今天的,3.注意不要同时选择太多服,防止数据量过多,导致后台跨掉 <br>';
    	$this->setHeader($header);
    	$this->setSearch('时间' , 'range_time');
    	$this->setSearch('角色ID');
    	$this->setSearch('' , 'scp' , array(0 ,0, 1, 0));
    	$this->setFields(array('部位','宝石','时间','角色id'));
    	$this->smarty->assign('select','select');
    	$this->setSource('stream' , 'gem_data' , 1);
    	$this->displays();
    }
    
    public function gem_data(){
    	$this->setCondition(0 , 'time' , 'create_time');
    	$this->setCondition(1 , 'eq' , 'role_id');
    	$this->setCondition(2 , 'scp');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('gem_data' , 'gem_export');
    	$this->call('StreamModel' , $method , $this->getConditions());
    }
}