<?php
!defined('IN_WEB') && exit('Access Denied');

class SummaryController extends Controller
{

    public function summary()
    {

        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(1, 0, 1, 1));
        $this->setField('日期', 100);

        $this->setFields(array('新增用户', '老用户', '活跃用户', '设备激活数', '平均在线', '最高在线', '付费人数', '总充值金额', '付费ARPU', '活跃付费率', '新用户付费人数', '新用户付费金额', '新用户ARPU', '新用户付费率', '老用户付费人数', '老用户付费金额', '老用户ARPU', '老用户付费率'));

        $this->setSource('Summary', 'summary_data', 1);
        $this->displays();
    }

    public function summary_data()
    {
        $this->setCondition(0, 'date', 's.time');
        $this->setCondition(1, 'scp', '', '', array(1, 0, 1, 1));
        $this->setOrder('s.time');
        $this->setLimit();
        $method = $this->setDataExport('summary_data', 'summary_export');
        $this->call('SummaryModel', $method, $this->getConditions());
    }

    public function monthly_income()
    {

        $this->setSearch('日期', 'range_date');
        $this->setSearch('', 'scp', array(0, 0, 1, 1));
        $this->setField('日期', 100);

        $this->setFields(array('渠道', '服', '充值金额'));
        $this->setSource('Summary', 'monthly_income_data', 1);
        $this->displays();
    }

    public function monthly_income_data()
    {
        $this->setCondition(0, 'date', 's.time');
        $this->setCondition(1, 'scp', '', '', array(0, 0, 1, 1));
        $this->setOrder('s.time');
        $this->setLimit();
        $method = $this->setDataExport('monthly_income_data', 'monthly_income_export');
        $this->call('SummaryModel', $method, $this->getConditions());
    }

    public function single_summary()
    {

        $this->setSearch('', 'scp', array(0, 0, 1, 1));
        $this->setFields(array('服', '总创角', '总充值人数', '总付费金额', '总付费率', '总付费ARPU', '上月充值金额', '本月充值金额', '昨天充值金额', '今天充值金额'));
        $this->setSource('Summary', 'single_summary_data', 1);
        $this->displays();
    }

    public function single_summary_data()
    {
        $this->setCondition(1, 'scp', '', '', array(0, 0, 1, 1));
        $this->setLimit();
        $method = $this->setDataExport('single_summary_data', 'single_summary_export');
        $this->call('SummaryModel', $method, $this->getConditions());
    }


}