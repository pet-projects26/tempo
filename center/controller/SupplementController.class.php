<?php
!defined('IN_WEB') && exit('Access Denied');

class SupplementController extends Controller{
	public function __construct(){
        parent::__construct();
        $this->o = new  OrderModel();
    }
    
    public function budan(){
    	$tabs = array(
    			array('title' => '补单' , 'url' => 'admin.php?&ctrl=supplement&act=getFailOrder'),
    	);
    	$this->tabs($tabs);
    }
    
    //获取所有status=3的订单
    public function getFailOrder(){
    	//设置搜索项，按照页面显示的顺序设置
    	$this->setSearch('角色名');
    	$this->setSearch('订单号');
    	$this->setSearch('第三方订单号');
    	$this->setSearch('账号');
    	$this->setFields(array('渠道' , '区服' , '包号' , '第三方订单号'));
    	 
    	$this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
    	//设置显示项，按照页面显示的顺序设置
    	$this->setField('订单号' , 190);
    	$this->setFields(array('账号' , '角色名' , '金额' , '元宝')); //如果显示项只需要设置名称，则可使用setFields方法批量设置显示项
    	$this->setField('时间' , 180);
    	$this->setField('操作');
    	 
    	$js = <<< END
        function logup(id)
        {
            if ( confirm('是否确定要进行补单?')) {
                        \$.ajax({
                            url: 'admin.php?ctrl=supplement&act=setFailOrder',
                            type: 'POST',
                            dataType: 'JSON',
                            data:{id:id}
                        })
                        .done(function (response) {
                            if (response.state == false) {
                                \$tabs.tabs( "load" ,0);
                            } else {
                                \$.dialog.alert(response.msg);
    							 \$tabs.tabs( "load" ,0);
                            }
                        })
                    }
        }
END;
    	//设置获取页面数据的类方法
    	$this->setSource('Supplement' , 'getFailOrder_data' , 1);
    	$this->setJs($js);
    	//显示页面
    	$this->displays();
    }
    
    public function setFailOrder(){
    	$id = $this->getParam('id');
    	if ($id) {
    		include_once('../api/core/BaseController.php');
    		$contoller = new BaseController();
    		$one  =	$this->o->getRow(array('order_num','corder_num','role_name','money','account','role_id','role_level','role_career','gold','item_id','server','create_time') , array('status' => 3,'id'=>$id));
    
    		if ($one) {
    			$rs =  $contoller->sendGoods($one['server'],$one);
    			if($rs == 1){
    				$this->o->update(array('status' => 1) , array('order_num' => $one['order_num']));
    				$json = array('state' => true,'msg'   => '补单成功');
    			}else {
    				$json = array('state' => false,'msg'   => '补单失败');
    			}
    			 
    		}else {
    			$json = array('state' => false,'msg'   => '没有记录');
    		}
    		exit(json_encode($json));
    	}
    }
    
    public function getFailOrder_data(){
    	//设置搜索条件
    	$this->setCondition(0 , 'like' , 'role_name');
    	$this->setCondition(1 , 'eq' , 'order_num');
    	$this->setCondition(2 , 'eq' ,'corder_num');
    	$this->setCondition(3 , 'eq' , 'account');
    	$this->setCondition(4 , 'scp');
    	//$this->setCondition(7 , 'eq','order',1);
    	//设置搜索排序
    	$this->setOrder('o.id');
    	//设置搜索条数限制
    	$this->setLimit();
    	$method = $this->setDataExport('getFailOrder_data' , 'getFailOrder_export');
    	$this->call('ChargeModel' , $method , $this->getConditions());
    }

    public function index(){

    	$this->smarty->display('supplement/index.tpl');
    }

    public function recode(){

    	include_once('../api/core/BaseController.php');

    	$contoller = new BaseController();

    	$rs  =	$this->o->getRows(array('order_num','corder_num','role_name','money','account','role_id','role_level','role_career','gold','item_id','server') , array('status' => 3));

        if(!empty($rs)){
            
            foreach ($rs as $key => $value) {
                $rs =  $contoller->sendGoods($value['server'],$value);
                if($rs == 1){
                    $this->o->update(array('status' => 1) , array('order_num' => $value['order_num']));
                } 
            } 
             echo  '补单完成';
        }else{
             echo  '不存在适合补单条件的订单';
        }
       
	}

 }

   