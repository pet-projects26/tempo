<?php
!defined('IN_WEB') && exit('Access Denied');

class SwimmingCopyController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启昆仑瑶池的人数。<br>';
        $header .= '参与人数：该日进行过昆仑瑶池（进场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均沐浴时长 = 该日全服所有玩家沐浴时长之和 / 参与人数   （确定到小数点后两位，四舍五入）<br>';
        $header .= '捡肥皂人数：该日进行过捡肥皂的玩家数。<br>';
        $header .= '捡肥皂参与度 = 捡肥皂人数 / 参与人数 *100%   （确定到小数点后两位，四舍五入）<br>';
        $header .= '人均捡肥皂次数  = 该日全服所有玩家捡肥皂的次数之和 / 捡肥皂人数 <br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '人均沐浴时长', '捡肥皂人数', '捡肥皂参与度', '人均捡肥皂次数'));
        $this->setSource('SwimmingCopy', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('SwimmingCopyModel', $method, $this->getConditions());
    }
}