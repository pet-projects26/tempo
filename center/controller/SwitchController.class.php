<?php
!defined('IN_WEB') && exit('Access Denied');

class SwitchController extends Controller{

    //功能开关（菜单）
    public function index(){
        $tabs = array(
            array('title' => '开关列表', 'url' => 'admin.php?&ctrl=switch&act=server_switch_record'),
            array('title' => '添加开关', 'url' => 'admin.php?&ctrl=switch&act=server_switch_edit')
        );
        $this->tabs($tabs);
    }

    //开关列表（子菜单）
    public function server_switch_record(){
        $data = array();
        $data['other_data']['header']  = '';
        $data['search_fields'] = array();
        $data['show_fields'] = array(
            array('title' => '功能名称'),
            array('title' => '值'),
            array('title' => '状态'),
            array('title' => '操作')
        );
        $data['ajax_source'] = 'admin.php?&ctrl=switch&act=server_switch_record_data';
        $data['other_data']['js'] = <<< END
        function edit(id){
            \$tabs.tabs('select' , type);
            \$tabs.tabs('url' , type , 'admin.php?ctrl=switch&act=server_switch_edit&id=' + id);
            \$tabs.tabs('load' , type);
            \$tabs.tabs('url' , type , 'admin.php?ctrl=switch&act=server_switch_edit&id=' + id);
        }
        function del(id){
            $.ajax({
                url: 'admin.php?ctrl=switch&act=server_switch_del&id=' + id,
                type: 'GET',
                dataType: 'JSON'
            }).done(function(data){
                $.dialog.tips(data.msg);
            })
        }
        function change(id){
            $.ajax({
                url: 'admin.php?ctrl=switch&act=server_switch_change&id=' + id,
                type: 'GET',
                dataType: 'JSON'
            }).done(function(data){
                $.dialog.tips(data.msg);
            })
        }
END;
        $this->displays($data);
    }

    //开关列表（数据）
    public function server_switch_record_data(){
        echo (new SwitchModel())->server_switch_record_data();
    }

    //添加和编辑开关（子菜单）
    public function server_switch_edit(){

    }

    //删除开关（方法）
    public function server_switch_del(){

    }

    //开启或关闭开关（方法）
    public function server_switch_change(){

    }
}