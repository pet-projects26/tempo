<?php
!defined('IN_WEB') && exit('Access Denied');

class TalkController extends Controller{
   
    public function __construct(){
        $this->s = new ServerModel();
        parent::__construct();
        
    }

    public function index(){
    	$tabs = array(
    			array('title' => '聊天监控', 'url' => 'admin.php?&ctrl=talk&act=talklist'),
    			array('title' => '敏感字列表', 'url' => 'admin.php?&ctrl=talk&act=sensitlist'),
    			array('title' => '敏感字设置', 'url' => 'admin.php?&ctrl=talk&act=setsensit'),
    			array('title' => '编辑敏感字', 'url' => 'admin.php?&ctrl=talk&act=editsensit'),
    			array('title' => '导入敏感字', 'url' => 'admin.php?&ctrl=talk&act=import'),
//     			array('title' => '禁言设置列表', 'url' => 'admin.php?&ctrl=talk&act=bantalkList'),
//     			array('title' => '禁言设置', 'url' => 'admin.php?&ctrl=talk&act=bantalk'),
//     			array('title' => '编辑禁言设置', 'url' => 'admin.php?&ctrl=talk&act=editbantalk'),
    	);
    	$this->tabs($tabs, 'log');
    }
    
    
    
    public function bantalk(){
    	//服务器，渠道组
    	$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
    	$channelGroupRow = array();
    
    	foreach ($channelGroupRes as $row) {
    		$group_id = $row['id'];
    		$channelGroupRow[$group_id] = $row['name'];
    	}
    	$server = (new ServerModel())->getServerByTrait();
    	$this->smarty->assign('groups', $channelGroupRow);
    	//     	$this->smarty->assign('servers', $server);
    
    	$this->smarty->display('talk/bantalk.tpl');
    }
    
    public function bantalk_action(){
    	 
    	$channel_group = $this->getParam('channel_group');
    	$time = $this->getParam('time');
    	$m = new BantalkModel();
    	foreach ($channel_group as $k=>$v) {
    		$sql    = " REPLACE INTO ny_bantalk (`channel_group`,`time`) VALUES ('{$v}','{$time}') ";
    		$result = $m->query($sql);
    	}
    	 
    	if($result){
    		$json = array('msg' => '添加成功' , 'code' => 1);
    	}else{
    		$json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
    	}
    	echo json_encode($json);
    }
    
    public function bantalkList(){
    	$this->setFields(array('渠道组id', '禁言时间(秒)' , '操作'));
    	$this->setSource('Talk' , 'bantalkList_data');
    	$js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 6);
                \$tabs.tabs('url' ,6, 'admin.php?ctrl=talk&act=editbantalk&id=' + id);
                \$tabs.tabs('load' , 6);
                \$tabs.tabs('url' , 6, 'admin.php?ctrl=talk&act=editbantalk&id=' + id);
            }
            function del(id){
                if(confirm('确定删除？')){
                $.ajax({
                    url: 'admin.php?ctrl=talk&act=delbantalk',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{'id':id}
                }).done(function(obj){
					if(obj){
						\$tabs.tabs('load' , 4);
				    }
    
                })
    
                }
            }
END;
    	$this->setJs($js);
    	$this->displays();
    }
    
    public function bantalkList_data(){
    	$this->setLimit();
    	$this->call('BantalkModel' , 'bantalkList_data' , $this->getConditions());
    }
    
    public function delbantalk(){
    	$id = $this->getParam('id');
    	$m = new  BantalkModel();
    	echo $m->delete(['channel_group' => $id]);
    }
    
    public function editbantalk(){
    	$id = $this->getParam('id');
    	if ($id) {
    
    		$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
    		$channelGroupRow = array();
    		foreach ($channelGroupRes as $row) {
    			$group_id = $row['id'];
    			$channelGroupRow[$group_id] = $row['name'];
    		}
    		$server = (new ServerModel())->getServerByTrait();
    		$this->smarty->assign('groups', $channelGroupRow);
    
    		$m = new BantalkModel();
    		$data = $m->getRow("*",['channel_group' => $id]);
    		if($data){
    			$this->smarty->assign('data' , $data);
    		}
    		$this->smarty->display('talk/editbantalk.tpl');
    	}else {
    		echo '请选择需要编辑的禁言设置';
    	}
    }
    
    
    public function import() {
    	$this->smarty->display('talk/tpl_tail.tpl');
    }
    
    public function uploadCsv(){
    	
    	if ($_FILES["file"]["error"] > 0){
    		die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "错误: " . $_FILES["file"]["error"] . "<br />")));
    	}else{
    	
    		$filename = $_FILES["file"]["name"];
    		$suffix = substr($filename, strrpos($filename, '.') + 1);
    		$filetype = array('csv' ,'xlsx' , 'xls');
    		if(!in_array($suffix , $filetype)){
    
    			die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "只允许上传Excel文件")));
    		}else if($_FILES['file']['size'] > 20*1024*1024){
    
    			die(json_encode(array('data'=>'' ,  'status' =>0 , 'e' => "文件大小不允许超过20M")));
    		}else{
    			if(strtolower($suffix) == 'csv'){
    				$handle = fopen($_FILES['file']['tmp_name'],"r");
    				$csv = array();
    				while ($row = fgetcsv($handle)) {
    					foreach($row as $k=>$v){
    						$csv[$k][] = iconv('gbk','utf-8',$v);
    					}
    				}
    			}else{
    				$csv = Util::readExcel($_FILES["file"]["tmp_name"]);
    			}
    			
    			if($csv){
    				unset($csv[1]);
    				$rs = array();
    				//上传表头
    				$info = array(0=>'name');
    
    				foreach ($csv as $k => $v) {
    					foreach ($v as $key => $value) {
    						if ($value) {
    							$rs[$k][$info[$key]] =substr($value,0,200);
    						}
    					}
    					$rs[$k]['create_time'] = time();
    				}
    			
    				$result = $this->multInsert('ny_sensit',$rs);
    				if($result !== false){
    					$return = array('status' => 1 , 'msg' => '添加成功');
    				}else{
    					$return = array('status' => 0 , 'msg' => '添加失败，请检查文件');
    				}
    
    			}else{
    
    				$return = array('status' => 0 , 'msg' => '请检查文件是否为空');
    			}
    
    			exit(json_encode($return));
    
    		}
    	}
    
    }
    
    /**
     * 分批次插入
     * @return [type] [description]
     */
    public static function multInsert($table, $data) {
    	$tmp = $data;
    	$keys = array_keys(array_pop($tmp));
    
    	$len = count($data);
    
    	$length = 1000;
    
    	$times = ceil($len / $length);
    
    	foreach ($keys as &$v) {
    		$v = "`{$v}`";
    	}
    	unset($v);
    
    
    	$dbh = new Model();
    
    	$insertId = 0;
    
    	//防止单次插入过多，分批次插入
    	for ($i = 0; $i < $times; $i++) {
    		$sql = "INSERT INTO {$table} (". implode(',', $keys).") values ";
    
    		$offset = ($i * $length);
    		$tmp = array_slice($data, $offset, $length);
    
    		$r2 = array();
    		foreach ($tmp as $k => $row) {
    
    			foreach ($row as $k2 => &$v2) {
    				$v2 = "'{$v2}'";
    			}
    
    			$r2[$k] = "(" . implode(',', $row) . ")";
    		}
    
    		$sql .= implode(',', $r2);
    
    		try {
    			$dbh->beginTransaction();
    
    			$res = $dbh->query($sql);
    
    			$insertId = $dbh->lastInsertId();
    
    			$dbh->commit();
    
    		} catch (Exception $e) {
    			$dbh->rollBack();
    		}
    
    	}
    	//end for
    
    	return $insertId;
    
    }
    
    public function sensitlist(){
    	$this->setSearch('名称');
    	$this->setFields(array('名称' ,'说明', '创建时间' , '操作'));
    	$this->setSource('talk' , 'sensit_data',1);
    	$js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 3);
                \$tabs.tabs('url' , 3, 'admin.php?ctrl=talk&act=editsensit&id=' + id);
                \$tabs.tabs('load' , 3);
                \$tabs.tabs('url' , 3, 'admin.php?ctrl=talk&act=editsensit&id=' + id);
            }
            function del(id){
                if(confirm('确定删除？')){
                $.ajax({
                    url: 'admin.php?ctrl=talk&act=delsensit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{'id':id}
                }).done(function(obj){
					if(obj){
						\$tabs.tabs('load' , 1);
				    }
    
                })
    
                }
            }
END;
    	$this->setJs($js);
    	$this->displays();
    }
    
    public function sensit_data(){
    	$this->setCondition(0 , 'like' , 'name');
    	$this->setOrder('create_time');
    	$this->setLimit();
    	$method = $this->setDataExport('sensit_data' , 'sensit_export');
    	$this->call('SensitModel' , $method , $this->getConditions());
    }
    
    public function editsensit(){
    	$id = $this->getParam('id');
    	if ($id) {
    		$m = new SensitModel();
    		$data = $m->getRow("*",['id' => $id]);
    		if($data){
    			$this->smarty->assign('data' , $data);
    		}
    		$this->smarty->display('talk/edit.tpl');
    	}else {
    		echo '请选择需要编辑的敏感规则';
    	}
    }
    
    public function setsensit(){
    	$this->smarty->display('talk/add.tpl');
    }
    
    public function add_action(){
    	$name = $this->getParam('name');
    	$content = $this->getParam('content');
    	$data = array(
    			'name' => $name,
    			'content' => $content,
    			'create_time' => time()
    	);
    	$m = new SensitModel();
    	if($m->add($data)){
    		$json = array('msg' => '添加成功' , 'code' => 1);
    	}else{
    		$json = array('msg' => '未添加成功，请再次尝试' , 'code' => 0);
    	}
    	echo json_encode($json);
    }
    
    public function editsensit_action(){
    	$id = $this->getParam('id');
    	if ($id) {
    		$name = $this->getParam('name');
    		$content = $this->getParam('content');
    		$data = array(
    				'name' => $name,
    				'content' => $content,
    		);
    		$where['id'] = $id;
    		$m = new SensitModel();
    		if($m->update($data, $where)){
    			$json = array('msg' => '更新成功' , 'code' => 1);
    		}else{
    			$json = array('msg' => '更新失败，请再次尝试' , 'code' => 0);
    		}
    		echo json_encode($json);
    	}
    }
    
    public function delsensit_action(){
    	$id = $this->getParam('id');
    	$m = new SensitModel();
    	echo $m->delete(['id' => $id]);
    }
    
    /**
     * [index description]
     * @return [type] [description]
     */
    public function talklist(){
    	//服务器，渠道组
    	$channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait();
    
    	$channelGroupRow = array();
    
    	foreach ($channelGroupRes as $row) {
    		$group_id = $row['id'];
    		$channelGroupRow[$group_id] = $row['name'];
    	}
    
    	$server = (new ServerModel())->getServerByTrait();
    
    	$this->smarty->assign('groups', $channelGroupRow);
    	$this->smarty->assign('servers', $server);
    
    	$this->smarty->display('talk/index.tpl');
    }
    
    public function data_action(){
    	//     	ini_set("display_errors","On");
    	//     	error_reporting(E_ALL);
    	$server = $this->getParam('server');
    	 
    	$role_name  =  trim($this->getParam('role_name'));
    	
    	$keyword  =  trim($this->getParam('keyword'));
    	
    	$type  =  $this->getParam('type');
    	
    	$where = [] ;
    	
    	$export = $this->getParam('export');
    	if ($export == 1) {
    		$server = explode(",",$server);
    	}
    	
    	
    
//     	if(!empty($role_name)){
    	
//     		$where .= ' and role_name = '.$role_name;
//     	}
    	
//     	if(!empty($keyword)){
    	
//     		$where .= " and content like '%$keyword%'";
//     	}
    	
//     	if(!empty($type)){
    	
//     		$where .= 'type = '.$type;
//     	}else{
//     		$where .=' type in (1,2,3,4,11)';
//     	}

    	$role_name && $where[] = "  role_name =  '{$role_name}' ";
    	$keyword && $where[] = "  content like '%$keyword%' ";
    	if (!empty($type)) {
    		$where[] = "  type =  '{$type}' ";
    	}else {
    		$where[] = "  type in (1,2,3,4,11,9,10)";
    	}
    	$where = $where ?  implode('and' , $where) : '';
    	
    	$sql = "select t.* ,r.package,r.account from ny_talk t LEFT JOIN ny_role r on r.name = t.role_name where $where  order by t.create_time desc limit 1000";
    	$arr  = array('query' => array('data' => array($sql)));
    	$res  = (new ServerconfigModel())->makeHttpParams($server, $arr);
    	$urls = $res['urls'];
    	$param = $res['params'];
    	$res = Helper::rolling_curl($urls, $param);
    	
    	$rs = array();
    	
    	foreach ($server as  $value) {
    	
    		$data = json_decode($res[$value],true);
    		$data = $data['data']['query'][0];
    	
    		if(empty($data)){
    			continue;
    		}
    	
    		$package  = array_column($data, 'package');
    			
    		$package = array_unique($package);
    		$package  = implode(',' ,$package);
    		$package =  rtrim($package,',');
    		$sql = "select c.`name` , p.package_id from ny_channel c LEFT JOIN ny_package p  on  p.channel_id = c.channel_id WHERE p.package_id in ($package) ";
    	
    		$package  =  $this->s->query($sql);
    	
    		foreach ($data as $k => $v) {
    	
    			$data[$k]['server'] = $value;
    			foreach ($package as  $p) {
    				 
    				if($p['package_id'] == $v['package']){
    					$data[$k]['channel'] = $p['name'];
    				}
    			}
    		}
    	
    		$rs = array_merge($rs , $data);
    	}
    	
    	if ($export == 1) {
    		
    		$result[] = array(
    				'服务器', '包号','角色名','账号','聊天内容','类型','等级','时间'
    		);
    		foreach($rs as $k=>$row){
    			$result[$k+1]['server'] = $row['server'];
    			$result[$k+1]['package'] = $row['package'];
    			$result[$k+1]['role_name'] = $row['role_name'];
    			$result[$k+1]['account'] = $row['account'];
    			$result[$k+1]['content'] = $row['content'];
    			$result[$k+1]['type'] = $row['type'];
    			$result[$k+1]['role_level'] = $row['server'];
    			$result[$k+1]['create_time'] = date("Y-m-d H:i:s",$row['create_time']); ;
    		}
    		 
    		Util::exportExcel($result , '聊天信息（' . date('Y年m月d日H时i分s秒') . '）');
    	}else {
    		echo json_encode($rs);
    	}
    }
  
    /**
     * [ban 转换封禁需要的数据]
     * @return [type] [description]
     */
    public function ban(){

        $server = $this->getParam('server');
        $type = $this->getParam('type');
        $role_name = $this->getParam('role_name');
        

        $sql = "select account , name, last_login_ip ,mac from ny_role where name = '$role_name'";
       
        $arr  = array('query' => array('data' => array($sql)));
        $res  = (new ServerconfigModel())->makeHttpParams(array($server), $arr);
        $urls = $res['urls'];
        $param = $res['params'];
        $res = Helper::rolling_curl($urls, $param);

        $rs = array();

        $data = json_decode($res[$server],true);
        $data = $data['data']['query'][0][0];

        switch ($type) {
            case '1':
                $content = $data['account'];
                break;
           
            case '3':
                $content = $data['last_login_ip'];
                break;
            case '4':
                $content = $data['mac'];
                break;
            case '2':
            case '5':
                $content = $data['name'];
                break;
            default:
                break;
        }

        //查询服务器rpc需要的ip和端口
        //$sql ="select ip ,gm_port from ny_server_config where  server_id   = '$server' ";
        //$rs = $this->s->query($sql);
        echo json_encode(array('content' =>$content, 'server' => $server));//.':'.$rs[0]['ip'].':'.$rs[0]['gm_port']
    }
}