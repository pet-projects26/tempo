<?php
!defined('IN_WEB') && exit('Access Denied');

class ThreerealmsBossController extends Controller{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日成功连接上游戏服务器并且开启三界BOSS的玩家人数（备注：开启条件待定，已上线前版本为准）。<br>';
        $header .= '参与人数：该日进行过三界BOSS挑战玩法（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均挑战次数 = 该日全服每个玩家挑战三界BOSS次数之和（有奖励即算一次，不需要去重） / 参与人数   （确定到小数点后两位，四舍五入）<br>';
        $header .= '鼓舞人数占比 = 在三界BOSS玩法中进行过鼓舞的玩家数（需去重） / 参与人数 *100%（精确到小数点后两位，四舍五入）<br>';
        $header .= '金币鼓舞人数占比  =  在三界BOSS玩法中进行过金币鼓舞的玩家数（需去重） / 在三界BOSS玩法中进行过鼓舞的人数 *100%（精确到小数点后两位，四舍五入）<br>';
        $header .= '元宝鼓舞人数占比  =  在三界BOSS玩法中进行过元宝鼓舞的玩家数（需去重） / 在三界BOSS玩法中进行过鼓舞的人数 *100%（精确到小数点后两位，四舍五入）<br>';
        $header .= '人均金币鼓舞次数 = 该日本服在三界BOSS玩法中进行金币鼓舞的总次数 / 在三界BOSS玩法中进行过金币鼓舞的玩家数 （需去重） （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均元宝鼓舞次数 = 该日本服在三界BOSS玩法中进行元宝鼓舞的总次数 / 在三界BOSS玩法中进行过元宝鼓舞的玩家数 （需去重） （精确到小数点后两位，四舍五入）<br>';
        $header .= '涅槃复活人数占比 = 该日本服的三界BOSS玩法中进行过涅槃复活的玩家数（去重） / 参与人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均涅槃复活次数 = 该日本服在三界BOSS玩法中进行涅槃复活的总次数 / 在三界BOSS玩法中进行过涅槃复活的玩家数 （需去重） （精确到小数点后两位，四舍五入）<br>';
        $header .= '购买次数玩家占比 = 该日本服购买三界BOSS次数的玩家数（去重） / 该日本服进行过三界BOSS挑战玩法（有奖励即算，需去重）的玩家数 * 100% <br>';
        $header .= '人均购买次数 = 该日本服购买三界BOSS总次数 / 该日本服购买三界BOSS次数的玩家数（去重）  精确到小数点后两位，四舍五入 <br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '人均挑战次数', '鼓舞人数占比', '金币鼓舞人数占比', '元宝鼓舞人数占比', '人均金币鼓舞次数', '人均元宝鼓舞次数', '涅槃复活人数占比', '人均涅槃复活次数', '购买次数玩家占比', '人均购买次数'));
        $this->setSource('ThreerealmsBoss' , 'index_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('boss_data' , 'boss_export');
        $this->call('ThreerealmsBossModel' , $method , $this->getConditions());
    }
}