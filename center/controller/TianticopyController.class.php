<?php
!defined('IN_WEB') && exit('Access Denied');

class TiantiCopyController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启天梯斗法的人数。<br>';
        $header .= '参与人数：该日进行过天梯斗法（参与场数≥1）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '人均参与场数 = 该日全服玩家参与场数之和   / 参与人数 *100%<br>';
        $header .= '购买次数人数：该日进行过购买天梯斗法次数的玩家人数。<br>';
        $header .= '购买参与度 = 购买次数人数 / 参与人数 *100%<br>';
        $header .= '人均购买次数 = 该日全服玩家购买天梯斗法次数总和 / 购买次数人数 <br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '参与人数', '参与度', '人均参与场数', '购买次数人数', '购买参与度', '人均购买次数'));
        $this->setSource('TiantiCopy', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('TiantiCopyModel', $method, $this->getConditions());
    }
}