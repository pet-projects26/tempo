<?php
!defined('IN_WEB') && exit('Access Denied');

class TooldealController extends Controller{
  
	private $file = "/tmp/udid_deadline.txt";
    public function __construct(){
        parent::__construct();
    }

    //分区管理（菜单）
    public function index(){
        $tabs = array(
            array('title' => '设置udid到期时间', 'url' => 'admin.php?&ctrl=tooldeal&act=settime'),
        );
        $this->tabs($tabs);
    }
	
	public function settime(){
		$time = file_get_contents($this->file);
		$this->smarty->assign('time',$time);
		$this->smarty->display('tooldeal/add.tpl');
	}
	
	public function add_data(){
		$time = $_REQUEST['time'];
		file_put_contents($this->file, (int)$time);
		exit('true');
	}
	
	
}



















