<?php
!defined('IN_WEB') && exit('Access Denied');

class TotalController extends Controller{
	public function index(){
		$tabs = array(
            array('title' => '角色财富统计', 'url' => 'admin.php?&ctrl=total&act=rolefortune'),
            array('title' => '账号财富统计', 'url' => 'admin.php?&ctrl=total&act=accountfortune'),
        );
        $this->tabs($tabs);	
	}

    //数据汇总（菜单）
    public function sum(){
        $tabs = array(
            array('title' => '角色数据汇总', 'url' => 'admin.php?&ctrl=total&act=record'),
            array('title' => '账号数据汇总', 'url' => 'admin.php?&ctrl=total&act=account_record'),
        );
        $this->tabs($tabs);
    }

    public function record(){
        $header  = '说明：<br>';
        $header .= '1. 注册数 指当天创号成功的玩家数量<br>';
        $header .= '2. 创角数 指当天创建角色成功的数量<br>';
        $header .= '3. 登录数 指当天登录游戏的角色数量<br>';
        $header .= '4. 活跃数 指当天登录游戏时长总和大于等于30分钟的角色数量<br>';
        $header .= '5. 老角色数 指当天有登录的非当天创建的角色数量<br>';
        $header .= '6. 充值人数 指当天充值成功的角色数量<br>';
        $header .= '7. 登录付费率 = 充值人数 / 登录数<br>';
        $header .= '8. 总AP = 充值金额 / 充值人数<br>';
        $header .= '9. 新充值人数 指当日创建的且当日有进行充值的角色数<br>';
        $header .= '10. 新充值金额 指当日创建的且当日有进行充值的角色的充值金额<br>';
        $header .= '11. 新增付费率 = 新充值人数 / 创角数<br>';
        $header .= '12. 新充值AP = 新充值金额 / 新充值人数<br>';
        $header .= '13. 老充值人数 = 指当天充值成功的老角色数<br>';
        $header .= '14. 老充值金额 = 指当天充值成功的老角色的总充值金额<br>';
        $header .= '15. 老付费率 = 老充值人数 / 老角色数<br>';
        $header .= '16. 老充值AP = 老充值金额 / 老充值人数<br>';
        $header .= '17. 登录AP = 充值金额 / 登录数<br>';
        $header .= '18. 次留和3留 指次日留存率和三日留存率 (之后的7留 ，15留 ，30留同理)<br>';
        $header .= '19. 三日留存率 = 当天有登录且在三天前也有登录的角色数量 / 总角色数量 ';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array(
            '注册数' , '创角数' , '登录数' , '活跃数' , '老角色数' , '充值人数' , '充值金额' , '登录付费率' , '总AP' , '新充值人数' ,
            '新充值金额' , '新增付费率' , '新充值AP' , '老充值人数' , '老充值金额' , '老付费率' , '老充值AP' , '登录AP' ,
            '最高在线数' , '平均在线数' , '次留' , '3留' , '4留' , '5留' , '6留' , '7留' , '15留' , '30留'
        ));
        $this->setSource('total' , 'record_data' , 1);
        $this->displays();
    }
    public function record_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('record_data' , 'record_export');
        $this->call('TotaldailyModel' , $method , $this->getConditions());
    }

    //账号付费（菜单）
    public function account_record(){
        $header  = '说明：<br>';
        $header .= '1. 注册数 指当天创号成功的账号数量<br>';
        $header .= '2. 登录数 指当天有登录的账号数量<br>';
        $header .= '3. 老玩家数 指当天有登录的非当天注册的账号数量<br>';
        $header .= '4. 充值人数 指当天有充值的玩家数量<br>';
        $header .= '5. 登录付费率 = 充值人数 / 登录数<br>';
        $header .= '6. 总AP = 充值金额 / 充值人数<br>';
        $header .= '7. 新充值人数 指当天注册的并且当天有充值的账号数量<br>';
        $header .= '8. 新充值金额 指当天注册的账号在当天的充值金额<br>';
        $header .= '9. 新增付费率 = 新付费金额 / 新充值人数<br>';
        $header .= '10. 新充值AP = 新付费金额 / 注册数<br>';
        $header .= '11. 老充值人数 = 指当天充值成功的老玩家数<br>';
        $header .= '12. 老充值金额 = 指当天充值成功的老玩家的总充值金额<br>';
        $header .= '13. 老付费率 = 老充值人数 / 老玩家数<br>';
        $header .= '14. 老充值AP = 老充值金额 / 老充值人数<br>';
        $header .= '15. 注册AP = 充值金额 / 注册数';
        $this->setHeader($header);
        $this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('日期' , 100);
        $this->setFields(array(
            '注册数' , '登录数' , '老玩家数' , '充值人数' , '充值金额' , '登录付费率' , '总AP' , '新充值人数' ,
            '新充值金额' , '新增付费率' , '新充值AP' , '老充值人数' , '老充值金额' , '老付费率' , '老充值AP' , '注册AP'
        ));
        $this->setSource('total' , 'account_record_data' , 1);
        $this->displays();
    }

    //账号付费（菜单）
    public function account_record_data(){
        $this->setCondition(0 , 'date' , 'date');
        $this->setCondition(1 , 'scp');
        $this->setOrder('date');
        $this->setLimit();
        $method = $this->setDataExport('account_record_data' , 'account_record_export');
        $this->call('TotaldailyModel' , $method , $this->getConditions());
    }

    //角色财富统计（菜单）
    public function rolefortune(){
		$this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setFields(array('范围' , '人数' , '金额' , '玩家总数占比' , ' 充值人数占比'));
        $this->setSource('total' , 'role_fortune_data' , 1);
        $this->smarty->assign('select', 'radio');
        $this->displays();
    }
    public function role_fortune_data(){
		$this->setCondition(0 , 'date','create_time');
        $this->setCondition(1 , 'scp');
        $method = $this->setDataExport('role_fortune_data' , 'role_fortune_export');
        $this->call('TotalModel' , $method , $this->getConditions());
    }
	//账号财富统计（菜单）
    public function accountfortune(){
		$this->setSearch('日期' , 'range_date');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setFields(array('范围' , '人数' , '金额' , '玩家总数占比' , '充值人数占比'));
        $this->setSource('total' , 'account_fortune_data' , 1);
        $this->smarty->assign('select', 'radio');
        $this->displays();
    }
    public function account_fortune_data(){
		$this->setCondition(0 , 'date','create_time');
        $this->setCondition(1 , 'scp');
        $method = $this->setDataExport('account_fortune_data' , 'account_fortune_export');
        $this->call('TotalModel' , $method , $this->getConditions());
    }
}