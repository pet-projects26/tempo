<?php
!defined('IN_WEB') && exit('Access Denied');

class UserController extends Controller{
    private $ua;
    private $la;
    
    public function __construct(){
        parent::__construct();
        $this->ua = new UserAction();
        $this->la  = new LogAction();
    }

    public function index(){
        $tabs = array(
            array('title' => '用户列表', 'url' => 'admin.php?ctrl=user&act=items'),
            array('title' => '添加用户', 'url' => 'admin.php?ctrl=user&act=edit')
        );
        $this->tabs($tabs);
    }

    public function edit(){
        $expiration = date('Y-m-d' , strtotime('+1 week'));
        $this->smarty->assign('expiration' , $expiration);
        $groupNames = $this->ua->usergroupGetNames();
        $this->smarty->assign('groupNames' , $groupNames);
        $this->smarty->display('user/edit.tpl');
    }

    public function items(){
        $mChannel   = new ChannelModel();
        $list       = $this->ua->userGetList();
        $groupNames = $this->ua->usergroupGetNames();
        $this->smarty->assign('groupNames' , json_encode($groupNames));
        $this->smarty->assign('list' , $list);
        $this->smarty->assign('channel' , json_encode(array('-1' => '-') + $mChannel->getFieldAssoc(array('channel_id' , 'name'))));
        $this->smarty->display('user/items.tpl');
    }

    public function delete(){
        $uid = intval($this->getParam('uid'));
        if($uid){
            $result = $this->ua->userDelete($uid);
            echo $result ? 'success' : '删除失败';
            $this->la->logAdd('删除用户，uid：' . $uid);
        }
        else{
            echo '错误的参数';
        }
    }
    public function add_submit(){
        $groupid = filter_input(INPUT_POST,'groupid');
        self::checkChannel($groupid);
        $fields = array(
            'username' => '填写用户名',
            'groupid'  => '选择用户组',
            'name'     => '填写姓名'
        );
        $cp = check_post(array_keys($fields));
        if(!($cp === '1')){
            echo $fields[$cp];
            return;
        }
        $this->la->logAdd('添加用户:' . $this->getParam('username'));
        $result = $this->ua->addUser($_POST);
        switch($result['code']){
            case 1:
                $msg = '添加用户成功，密码为：' . $result['msg'];
                break;
            case -1:
                $msg = '添加失败';
                break;
            case -2:
                $msg = '已存在的用户名';
                break;
            case -3:
                $msg = '两次输入密码不相同';
                break;
            case -5:
                $msg = '有效期需在今天之后';
                break;
            case -6:
                $msg = '用户名只能为小写字母数字和下划线3-14位';
                break;
            default:
                $msg = '未知错误';
        }
        echo $msg;
    }

    public function save_field(){
        $uid   = intval($this->getParam('id'));
        $field = trim($this->getParam('field'));
        $value = trim($this->getParam('value'));
        self::checkChannel($value);
        if($uid <= 0){
            echo 'save fail';
            return;
        }
        $data = array(
            'uid'  => $uid,
            $field => $value
        );
        $this->ua->userUpdate($data);
        if($field == 'groupid'){
            $groupNames = $this->ua->usergroupGetNames();
            $value      = $groupNames[$value];
        }
        echo $value;
        $this->la->logAdd("修改用户:{$field}=>{$value},uid:{$uid}");
    }

    public function delay_account(){
        $days       = 30;
        $uid        = intval($this->getParam('uid'));
        $expiration = date('Y-m-d', strtotime($this->getParam('expiration_format')) + 86400 * $days);
        $this->ua->delayAccount($uid, $days);
        echo $expiration;
        $this->la->logAdd('账户续期，uid：' . $uid);
    }

    public function reset_password(){
        $uid = intval($this->getParam('uid'));
        $this->la->logAdd('重置用户密码，uid：' . $uid);
        if($uid){
            echo $this->ua->resetPassword($uid);
        } else {
            echo 'error';
        }

    }

    public function clear_login_err(){
        $username = trim($this->getParam('username'));
        $this->la->logAdd('清空用户错误登录次数，username：' . $username);
        if($username){
            $this->ua->clearLoginErr($username);
            echo '成功';
        }
        else{
            echo 'error';
        }
    }

    public function change_password_page(){
        $this->smarty->display('user/change_password.tpl');
    }

    public function change_password(){
        $submit = $this->getParam('submit');
        if(isset($submit)){
            $this->la->logAdd('修改密码');
            $result = $this->ua->changePassword($_POST);
            switch($result['code']){
                case 1:
                    $msg = 'success';
                    break;
                case -1:
                    $msg = '修改密码失败';
                    break;
                case -2:
                    $msg = '原始密码错误';
                    break;
                case -3:
                    $msg = '两次输入密码不相同';
                    break;
                case -4:
                    $msg = '不能修改，您在' . $result['msg'] . '使用过该密码';
                    break;
                case -11:
                    $msg = '密码长度需要大于' . PASSWORD_LENGTH . '位字符';
                    break;
                case -12:
                    $msg = '密码必须同时包含数字和字母';
                    break;
                default:
                    $msg = '未知错误';
            }
            echo $msg;
        }
        else{
            echo '严重错误';
        }
    }

    /**
     * @param int $user_group
    */
    private static function checkChannel($user_group){

        if(empty($user_group))exit('用户组不存在');
        $model = new Model();
        $sql = "select channel from ny_user_group where groupid = '".$user_group."'";
        $row = $model->query($sql);
        $msg = array_shift($row);
        $msg['channel'] != 'all' && $tc = json_decode($msg['channel'],true);
        if($msg['channel'] != 'all' && empty($tc)){
            echo '<script language="JavaScript">alert("该用户组未勾选渠道！")</script>';
            exit('点击修改');
        }
    }
}
