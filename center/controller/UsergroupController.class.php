<?php
class UsergroupController extends Controller{
    private $uga;
    private $la;

    public function __construct(){
        parent::__construct();
        $this->uga = new UsergroupAction();
        $this->la = new LogAction();
    }

    public function index(){
        $this->smarty->display('usergroup/index.tpl');
    }

    public function groups(){
        $groupList = $this->uga->groupGetList();
        $this->smarty->assign('groupList', $groupList);
        $this->smarty->display('usergroup/usergroup.tpl');
    }

    public function edit(){
        $groupid = intval($_GET['groupid']);
        $groupid && $usergroup = $this->uga->groupGetOne($groupid);
        $this->smarty->assign('usergroup', $usergroup);
        $this->smarty->display('usergroup/edit.tpl');
    }

    public function delete(){
        $groupid = intval($_POST['groupid']);
        $groupid && $result = $this->uga->groupDelete($groupid);
        echo $result? 'success': 'fail';
        $this->la->logAdd("删除用户组，groupid：{$groupid}");
    }

    public function edit_submit(){
        if(empty($_POST['name'])){
            echo '名称必须填写';
            return;
        }
        //处理ip白名单
        $allow_ip = textareaStrToArr($_POST['allow_ip']);
        //默认数据
        $data = array(
            'name' => trim($_POST['name']),
            'description' => trim($_POST['description']),
            'allow_ip' => serialize($allow_ip),
        );
        //复制用户组权限
        $copy_usergroup = intval($_POST['copy_usergroup']);
        if($copy_usergroup){
            $tmp_data = $this->uga->groupGetOne($copy_usergroup);
            if($tmp_data){
                $data['center_permit'] = $tmp_data['center_permit'];
                $data['single_permit'] = $tmp_data['single_permit'];
                $data['center_modules'] = $tmp_data['center_modules'];
                $data['single_modules'] = $tmp_data['single_modules'];
            }
        }
        $groupid = intval($_POST['groupid']);
        if($groupid){
            $data['groupid'] = $groupid;
            $result = $this->uga->usergroupUpdate($data);
            echo $result ? '修改成功': '修改失败';
            $this->la->logAdd('修改用户组：' . $data['name']);
        }
        else{
            $result = $this->uga->usergroupAdd($data);
            echo $result ? '添加成功': '添加失败';
            $this->la->logAdd('添加用户组：' . $data['name']);
        }
    }
}
