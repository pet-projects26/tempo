<?php
!defined('IN_WEB') && exit('Access Denied');

class UserinfoController extends Controller
{

    private $sm;

    public function __construct()
    {
        parent::__construct();
        $this->sm = new SettingModel();
    }

    public function index()
    {

        $servers = $this->sm->getServerByTrait([], true);
        $channelGroups = $this->sm->getChannelGroupByTrait();
        $groups = array();
        foreach ($channelGroups as $key => $val) {
            $groups[$val['id']] = $val['name'];
        }
        $this->smarty->assign('servers', $servers);
        $this->smarty->assign('groups', $groups);

        $this->smarty->display('userinfo/index.tpl');
    }

    public function index_data()
    {
        $search_type = $this->getParam('search_type');
        $server = $this->getParam('server');
//
        switch ($search_type) {
            case '1':
                $account = trim($this->getParam('account'));
                //根据玩家账号找到其role_id
                $data = $this->call('RoleModel', 'getRoleIdToType', array('type' => 'account', 'value' => $account), $server);
                $role_id = $data[$server];
                break;
            case '2':
                $role_id = trim($this->getParam('role_id'));
                //根据玩家id找到role_id
                $data = $this->call('RoleModel', 'getRoleIdToType', array('type' => 'role_id', 'value' => $role_id), $server);
                $role_id = $data[$server];
                break;
            case '3':
                $role_name = trim($this->getParam('role_name'));
                //根据角色名找到其role_id
                $data = $this->call('RoleModel', 'getRoleIdToType', array('type' => 'name', 'value' => $role_name), $server);
                $role_id = $data[$server];
                break;
            default:
                $role_id = 0;
                break;
        }
//        $role_id = 2147484101;

//        如果在这role_id = 0; 说明参数有误
        if (!$role_id) exit(json_encode(['errno' => 2, 'error' => "role_id is empty"]));

        //根据role_id获取信息 redis 从单服获取
        $data = $this->call('RoleRedis', 'getRoleInfo', array('role_id' => $role_id), $server);

        $data = $data[$server];

        if ($data['error']) exit(json_encode($data));

        if (empty($data)) exit(json_encode(['errno' => 2, 'error' => 'data is error']));

        $data['role_info']['eraLvleraNum'] = $data['role_info']['eraLvl'] . '转' . $data['role_info']['eraNum'] . '重';

        if ($data['role_equip']['EquipGrids']) {
            //装备宝石数组改为字符串
            foreach ($data['role_equip']['EquipGrids'] as $equip_key => $equip_val) {
                //装备名字符串
                $data['role_equip']['EquipGrids'][$equip_key]['itemName_str'] = $data['role_equip']['EquipGrids'][$equip_key]['item']['itemId'] != 0 ? $data['role_equip']['EquipGrids'][$equip_key]['item']['itemName'] . '(' . $data['role_equip']['EquipGrids'][$equip_key]['item']['grade'] . '品' . $data['role_equip']['EquipGrids'][$equip_key]['item']['class'] . '阶' . $data['role_equip']['EquipGrids'][$equip_key]['item']['star'] . '星)' : '无';
                //仙石字符串
                $data['role_equip']['EquipGrids'][$equip_key]['gems_str'] = implode('、', $data['role_equip']['EquipGrids'][$equip_key]['gems']);
                //强化字符串
                $data['role_equip']['EquipGrids'][$equip_key]['strongLvl_str'] = $data['role_equip']['EquipGrids'][$equip_key]['strongLvl'] > 0 ? '+' . $data['role_equip']['EquipGrids'][$equip_key]['strongLvl'] : 0;
            }
            unset($equip_key);
            unset($equip_val);
        }

        $data['role_pet']['rankStar'] = $data['role_pet']['rank'] . '阶' . $data['role_pet']['stars'] . '星';

        $data['role_ride']['rankStar'] = $data['role_ride']['rank'] . '阶' . $data['role_ride']['stars'] . '星';

        //角色法宝排序修改
        // 原本 $data['role_amulet'] -> refineList -> 一个个法宝 改为 $data['role_amulet']-> 4大类对应下标 -> typeName + refineList refineList里按等级从大到小排序

        if (isset($data['role_amulet']['refineList'])) {

            $role_amulet = $data['role_amulet']['refineList'];

            $role_amulet_new = [];

            foreach ($role_amulet as $key => $val) {
                if (!isset($role_amulet_new[$val['type']])) {
                    $role_amulet_new[$val['type']]['type'] = $val['type'];
                    $role_amulet_new[$val['type']]['typeName'] = $val['typeName'];
                }
                $role_amulet_new[$val['type']]['refineList'][] = $val;

                if (count($role_amulet_new[$val['type']]['refineList']) > 1) {
                    $levelVal = [];
                    //直接排序
                    foreach ($role_amulet_new[$val['type']]['refineList'] as $k => $v) {
                        $levelVal[$k] = $v['level'];
                    }
                    array_multisort($levelVal, SORT_DESC, $role_amulet_new[$val['type']]['refineList']);
                }
            }

            unset($data['role_amulet']['refineList']);
            unset($key);
            unset($val);
            $data['role_amulet']['typeList'] = $role_amulet_new;
        }

        //符文
        if (!empty($data['role_rune']['slot'])) {
            foreach ($data['role_rune']['slot'] as $key => $val) {

                $itemName = $val['itemId'] ? $val['itemName'] . ' Lv.' . $val['level'] : '无';

                $data['role_rune']['slot'][$key]['itemName'] = $itemName;

            }
            unset($key);
            unset($val);
        }

        $role_xianfu_illBookInfo_new = [];
        $role_xianfu_illBookInfo = $data['role_xianfu_illBookInfo'];

        //灵兽图鉴分类
        if (!empty($role_xianfu_illBookInfo)) {

            foreach ($role_xianfu_illBookInfo as $key => $val) {
                if (!isset($role_xianfu_illBookInfo_new[$val['quality']])) {
                    $role_xianfu_illBookInfo_new[$val['quality']]['quality'] = $val['quality'];
                    $role_xianfu_illBookInfo_new[$val['quality']]['qualityName'] = $val['qualityName'];
                }
                $role_xianfu_illBookInfo_new[$val['quality']]['list'][] = $val;

                if (count($role_xianfu_illBookInfo_new[$val['quality']]['list']) > 1) {
                    $levelVal = [];
                    //直接排序
                    foreach ($role_xianfu_illBookInfo_new[$val['quality']]['list'] as $k => $v) {
                        $levelVal[$k] = $v['level'];
                    }
                    array_multisort($levelVal, SORT_DESC, $role_xianfu_illBookInfo_new[$val['quality']]['list']);
                }
            }
            unset($key);
            unset($val);

            ksort($role_xianfu_illBookInfo_new);

            $data['role_xianfu_illBookInfo'] = $role_xianfu_illBookInfo_new;
        }

        if (isset($data['role_xianfu_fengShuiInfo']['decorate'])) {

            $decorate = $data['role_xianfu_fengShuiInfo']['decorate'];

            $decorate_new = [];
            if (!empty($decorate)) {
                foreach ($decorate as $key => $val) {
                    if (!isset($decorate_new[$val['type']])) {
                        $decorate_new[$val['type']]['type'] = $val['type'];
                        $decorate_new[$val['type']]['typeName'] = $val['typeName'];
                    }
                    $decorate_new[$val['type']]['list'][] = $val;
                }
                unset($key);
                unset($val);

                ksort($decorate_new);

                $data['role_xianfu_fengShuiInfo']['decorate'] = $decorate_new;
            }
        }
        echo json_encode($data);
    }

    public function login_account()
    {
        define('API', true);

        require_once ROOT . '/../api/core/BaseController.php';
        require_once ROOT . '/../api/core/BaseCheckSession.php';
        require_once ROOT . '/../api/conf/Game.php';
        require_once ROOT . '/../api/controller/_gameController.php';

        $package = $this->getParam('package');
        $account = $this->getParam('account');

        $account = substr($account, strpos($account, '_') + 1);

        $returnData = (new _gameController())->getTestServer($package, $account, 0);

        if ($returnData['errCode'] === 200) {

            $data = $returnData['data'];
            $server = $data['s'];

            $urlQuery = array(
                'account' => $data['user']['openId'],
                'uid' => $data['user']['openId'],
                'channel' => $data['channel'],
                'package' => $data['package'],
                'mac' => 0,
                'cdn' => $data['cdn'],
                'rvtype' => 0,
                'newregister' => 0,
                'sign' => $data['sign']
            );

            $serverSelect = array(
                'server' => $server['server_id'],
                'name' => $server['name'],
                'server_num' => (int)$server['server_num'],
                'channel_num' => (int)$server['channel_num'],
                'server_addr' => $server['server_addr'],
                'server_port' => (int)$server['server_port'],
                'status' => (int)$server['status'],
                'tick' => (int)$server['tick'],
                'sign' => $server['sign'],
                'package' => (int)$data['package'],
                'cdn' => $server['cdn']
            );


            $urlQuery['selected_server'] = $serverSelect;

            $returnData['urlQuery'] = $urlQuery;

            //记录日志
            (new LogAction())->logAdd("登陆玩家账号:package:{$package} | account: {$data['user']['openId']}");

        }

        echo json_encode($returnData);
    }

    public function ajaxGetSingleRoleName()
    {
        $server_id =  trim($this->getParam('server_id'));

        $returnData = [
            'errno' => 400,
        ];

        if (!$server_id) {
            echo json_encode($returnData);
            exit;
        }

        $data = $this->call('RoleModel', 'getSingleRoleName', array('output' => 1), $server_id);

        $singleRoleName = $data[$server_id];

        if (empty($singleRoleName)) {
            echo json_encode($returnData);
            exit;
        }

        $returnData['errno'] = 200;
        $returnData['data'] = $singleRoleName;

        echo json_encode($returnData);
    }

}