<?php
!defined('IN_WEB') && exit('Access Denied');

/***
 * 内网版本控制器 by wang
 * Class VersionController
 */
class VersionController extends Controller
{
    //定义版本控制数组
    private static $versionControl = array(
        0 => [
            'name' => '内网服务器',
            'path' => "/root/server/publish/tool/",
            'startJsName' => 'start.js',
            'stopJsName' => 'stop.js',
            'watchJsName' => 'watch.js',
            'svnUpShell' => "/svnup.sh",
            'logName' => 'localhost'
        ],
        1 => [
            'name' => '测试服务器',
            'path' => "/root/server_test/publish/tool/",
            'startJsName' => 'start.js',
            'stopJsName' => 'stop.js',
            'watchJsName' => 'watch.js',
            'svnUpShell' => "/svnup_test.sh",
            'logName' => 'test_mingming'
        ],
        2 => [
            'name' => 'cross服务器',
            'path' => "/root/server_cross/publish/tool/",
            'startJsName' => 'start_cross.js',
            'stopJsName' => 'stop_cross.js',
            'watchJsName' => 'watch_cross.js',
            'svnUpShell' => "/svnup_cross.sh",
            'logName' => 'cross'
        ],
        3 => [
            'name' => 'cross_t服务器',
            'path' => "/root/server_cross_t/publish/tool/",
            'startJsName' => 'start_cross_t.js',
            'stopJsName' => 'stop_cross_t.js',
            'watchJsName' => 'watch_cross_t.js',
            'svnUpShell' => "/svnup_cross_t.sh",
            'logName' => 'cross_t'
        ],
        4 => [
            'name' => 'shuzhi服务器',
            'path' => "/root/server_shuzhi/publish/tool/",
            'startJsName' => 'start.js',
            'stopJsName' => 'stop.js',
            'watchJsName' => 'watch.js',
            'svnUpShell' => "/svnup_shuzhi.sh",
            'logName' => 'shuzhi'
        ],
        5 => [
            'name' => '内网版署服务器',
            'path' => "/root/server_banshu/publish/tool/",
            'startJsName' => 'start.js',
            'stopJsName' => 'stop.js',
            'watchJsName' => 'watch.js',
            'svnUpShell' => "/svnup_banshu.sh",
            'logName' => 'banshu'
        ],
        6 => [
            'name' => '肖廉扬服务器',
            'path' => "/root/server_yan/publish/tool/",
            'startJsName' => 'start.js',
            'stopJsName' => 'stop.js',
            'watchJsName' => 'watch.js',
            'svnUpShell' => "/svnup_yan.sh",
            'logName' => 'test_yan'
        ],
        7 => [
            'name' => '测试服务器cross',
            'path' => "/root/server_test_cross/publish/tool/",
            'startJsName' => 'start_cross.js',
            'stopJsName' => 'stop_cross.js',
            'watchJsName' => 'watch_cross.js',
            'svnUpShell' => "/svnup_test_cross.sh",
            'logName' => 'test_mingming_cross'
        ],
        8 => [
            'name' => '预览服务器',
            'path' => "/root/server_client/publish/tool/",
            'startJsName' => 'start.js',
            'stopJsName' => 'stop.js',
            'watchJsName' => 'watch.js',
            'svnUpShell' => "/svnup_client.sh",
            'logName' => 'client'
        ]
    );

    //svnUp脚本路径定义
    private $svnUpShellPath = ROOT . '/versionSh';

    private $versionType = 0;

    private $resArr = [];

    public function __clone()
    {
        // TODO: Implement __clone() method.
        die('???');
    }

    public function index()
    {
        $node_status = 2;

        if ($this->watchSh()) $node_status = 1;

        $versionType = [];
        foreach (self::$versionControl as $key => $val) {
            $versionType[$key] = $val['name'];
        }
        $this->smarty->assign('node_status', $node_status);
        $this->smarty->assign('versionType', $versionType);
        $this->smarty->display('version/index.tpl');
    }

    public function action()
    {

        $this->typeVerification();

        $type = intval($_POST['type']);

        $this->resArr['status'] = '2';
        $this->resArr['title'] = '参数错误';

        if (!$type) $this->returnJson();

        switch ($type) {
            case 1:
                $this->startSh();
                break;
            case 2;
                if ($this->watchSh()) {
                    $this->stopSh();
                }
                break;
            case 3:
                //test SVN up
                $this->updateSvn();
                break;
            default:
                $this->returnJson();
                break;
        }

        $status = 1;
        $title = '启动成功';

        if ($type == 2) $title = '关闭成功';

        if ($type == 3) $title = '更新成功';

        $message = $title;

        $this->returnJson($status, $title, $message);
    }

    //关闭脚本
    public function stopSh()
    {
        $type = $this->versionType;

        $stopSh = self::$versionControl[$type]['path'] . self::$versionControl[$type]['stopJsName'];

        if (!file_exists($stopSh)) {
            $this->returnJson(2, '文件错误', '请检查关闭js是否存在');
        };

        //执行关闭脚本
        exec("sudo node " . $stopSh);
    }

    //启动脚本
    public function startSh()
    {
        $type = $this->versionType;

        $startSh = self::$versionControl[$type]['path'] . self::$versionControl[$type]['startJsName'];

        if (!file_exists($startSh)) {
            $this->returnJson(2, '文件错误', '请检查启动js是否存在');
        }
//
        /*
                if ($this->watchSh()) {
                    $this->stopSh();
                }
        */
        //执行启动脚本
        exec("sudo node " . $startSh, $res);
    }

    //ajax查询服务进程
    public function ajaxWatch()
    {
        $this->typeVerification();

        $this->resArr['node_status'] = 2;
        if ($this->watchSh()) {
            $this->resArr['node_status'] = 1;
        }

        echo json_encode($this->resArr);
    }

    //查看当前进程
    public function watchSh()
    {
        $type = $this->versionType;

        $watchSh = self::$versionControl[$type]['path'] . self::$versionControl[$type]['watchJsName'];

        if (!file_exists($watchSh)) {
            $this->returnJson(2, '文件错误', '请检查查看进程脚本是否存在');
        }

        exec('node ' . $watchSh, $res);
        return $res;
    }

    //更新SVN
    public function updateSvn()
    {
        $type = $this->versionType;

        $svnupSh = $this->svnUpShellPath . self::$versionControl[$type]['svnUpShell'];

        if (!file_exists($svnupSh)) {
            $this->returnJson(2, '文件错误', '请检查SVNUP脚本是否存在');
        }

        exec('sudo sh ' . $svnupSh, $res, $status);

        Helper::log($res, self::$versionControl[$type]['logName'], 'svnup', 'version');

        //更新结果 - - 不知道怎么判断成功或失败
        $message = implode("\n", $res);
        $this->returnJson($status, '更新结果', $message);
    }

    //验证服务状态
    public function typeVerification()
    {
        $this->versionType = intval($_POST['versionType']);

        if (!array_key_exists($this->versionType, self::$versionControl)) {
            $this->resArr['status'] = '2';
            $this->resArr['title'] = '参数错误';

            $this->returnJson();
        }
    }

    public function returnJson($status = '', $title = '', $message = '')
    {
        if ($status != '') $this->resArr['status'] = $status;

        if ($message != '') $this->resArr['message'] = $message;

        if ($title != '') $this->resArr['title'] = $title;

        $this->resArr['node_status'] = 2;
        //当前服务器状态
        if ($this->watchSh()) {
            $this->resArr['node_status'] = 1;
        }

        exit(json_encode($this->resArr));
    }
}