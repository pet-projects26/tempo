<?php
!defined('IN_WEB') && exit('Access Denied');

class VipController extends Controller{

    //VIP统计（菜单）
    public function index(){
        $tabs = array(
            array('title' => 'VIP信息', 'url' => 'admin.php?&ctrl=vip&act=record'),
            array('title' => 'VIP统计', 'url' => 'admin.php?&ctrl=vip&act=total'),
        );
        $this->tabs($tabs);
    }

    /*
        public function record(){
            $this->setSearch('VIP等级' , 'select' , CDict::$vip);
            $this->setSearch('角色名');
            $this->setSearch('领取时间' , 'range_time');
            $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
            $this->setFields(array('VIP等级' , '角色名' , '领取时间' , '过期时间'));
            $this->setSource('vip' , 'record_data' , 1);
            $this->smarty->assign('select', 'radio');
            $this->displays();
        }
        public function record_data(){
            $this->setCondition(0 , 'eq' , 'v.level');
            $this->setCondition(1 , 'like' , 'rd.name');
            $this->setCondition(2 , 'time' , 'rd.create_time');
            $this->setCondition(3 , 'scp');
            $this->setLimit();
            $method = $this->setDataExport('record_data' , 'record_export');
            $this->call('VipModel' , $method , $this->getConditions());
        }

        public function total(){
            $header  = '说明：<br>';
            $header .= '1. 占总VIP人数比例 = 该VIP等级的人数 / VIP总人数 <br>';
            $header .= '2. 占总人数比例 = 该VIP等级的人数 / 游戏总人数 ';
            $this->setHeader($header);
            $this->setSearch('持续时间' , 'select' , array('' => '全部' , 30 => '30天' , 90 => '90天' , 180 => '180天'));
            $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
            $this->setFields(array('VIP等级' , '人数' , '占总VIP人数比例' , '占总人数比例'));
            $this->setSource('vip' , 'total_data' , 1);
            $this->smarty->assign('select', 'radio');
            $this->displays();
        }
        public function total_data(){
            $this->setCondition(0 , 'eq' , 'day');
            $this->setCondition(1 , 'scp');
            $this->setOrder();
            $this->setLimit();
            $method = $this->setDataExport('total_data' , 'total_export');
            $this->call('VipModel' , $method , $this->getConditions());
        }
        */

    public function record()
    {

        $header = '说明：<br>';
        $header .= '付费用户数：从开服至截止到所选时间为止，总付费用户数。 <br>';
        $header .= 'VIP玩家数：从开服至截止到所选时间为止，各档次VIP玩家人数。 ';
        $this->setHeader($header);
        $this->setSearch('选择时间', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setFields(array('日期', '服务器', '付费用户数'));
        $this->setFields(CDict::$vip);
        $this->setSource('vip', 'record_data', 1);
//        $this->smarty->assign('select', 'radio');
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('VipModel', $method, $this->getConditions());
    }
}
