<?php
!defined('IN_WEB') && exit('Access Denied');

/** VIP公告 */
class VipnoticeController extends Controller{
    private $vipnoticeModel = null;

    public function __construct(){
        parent::__construct();
        $this->vipnoticeModel = new VipnoticeModel();
    }
    /**
     * 导航栏
    */
    public function index(){
        $tab = array(
            array('title'=>'公告列表','url'=>'admin.php?&ctrl=vipnotice&act=record'),
            array('title'=>'公告新增','url'=>'admin.php?&ctrl=vipnotice&act=add'),
            array('title'=>'公告编辑','url'=>'admin.php?&ctrl=vipnotice&act=edit'),
        );
        $this->tabs($tab);
    }

    /**
     * 显示列表
    */
    public function record(){
        $header = "";
        $channelGroupModel = new ChannelgroupModel();
        $channelGroupMsg = $channelGroupModel->getChannelGroupByTrait();
        $group = [];
        $group[] = '全部';
        foreach($channelGroupMsg as $key=>$val){
            $group[$val['id']] = $val['name'];
        }
        $this->setSearch('渠道组','select',$group);
        $this->setSearch('创建时间');
        $this->setFields(array('ID(自增)','渠道组','邮件标题','内容','发送','状态','发送结果','申请者','添加时间','操作'));
        $this->setSource('vipnotice','record_data');
        $js = <<< END
            //查看内容
            function view(row_id){
                //code for view massage
                if(!row_id){
                   return false;
                }
                $.ajax({
                    type:'post',
                    url:'admin.php?ctrl=vipnotice&act=view',
                    dataType: 'json',
                    data:{
                        'id':row_id
                    }
                }).done(function(data){
                    $.dialog({
                         'title':data.title,
                         'max':false,
                         'min':false,
                         'content':data.html
                    })
                });
            }
            //同意
            function agreed(row_id,type){
                if(!row_id) return false;
                if(confirm('确认修改状态?')){
                    $.ajax({
                        type:'post',
                        url:'admin.php?ctrl=vipnotice&act=agreed',
                        data:{
                            status:type,
                            id:row_id
                        },
                        dataType:'json'
                    }).done(function(data){
                        $.dialog.tips(data.msg);
                    })
                }
            }
            //发送
            function send(row_id){
                //code for send notice
                console.log(row_id);
                if(!row_id) return false;
               if(confirm('确定发送至对应渠道组？')){
                    $.ajax({
                        type:'post',
                        url:'admin.php?ctrl=vipnotice&act=noticeUp',
                        data:{
                            id:row_id
                        },
                        dataType:'json',
                        beforeSend:function(data){
                            $(".send").attr({disabled:'disabled'})
                        },
                        success:function(data){
                            //console.log(data)
                            $.dialog({
                            'title':data.title,
                            'max':false,
                            'width':false,
                            'content':data.html
                        });
                        },
                        complete:function(data){
                            $(".send").removeAttr('disabled');
                        }
                    })
               }
            }
            //下架
            function down(row_id){
                if(!row_id) return false;
               if(confirm('确定发送至对应渠道组？')){
                    $.ajax({
                        type:'post',
                        url:'admin.php?ctrl=vipnotice&act=noticeDown',
                        data:{
                            id:row_id
                        },
                        dataType:'json',
                        beforeSend:function(data){
                            $(".down-btn").attr({disabled:'disabled'})
                        },
                        success:function(data){
                            //console.log(data)
                            $.dialog({
                            'title':data.title,
                            'max':false,
                            'width':false,
                            'content':data.html
                        });
                        },
                        complete:function(data){
                            $(".down-btn").removeAttr('disabled');
                        }
                    })
               }
            }
            //编辑
            function edit(row_id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=vipnotice&act=edit&id=' + row_id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=vipnotice&act=edit&id=' + row_id);
            }
            //详情
            function more(row_id){
                if(!row_id) return false;
                $.ajax({
                    type:'post',
                    url:'admin.php?ctrl=vipnotice&act=more',
                    dataType:'json',
                    data:{
                        id:row_id
                    },
                }).done(function(data){
                    $.dialog({
                        'title':data.title,
                        'max':false,
                        'width':false,
                        'content':data.html
                    });
                })
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    public function record_data(){
        $this->setCondition(0,'eq','group_id');
        $this->setCondition(1,'date','create_time');
        //$this->setOrder('id');
        $this->setLimit();
        $this->call('VipnoticeModel','record_data',$this->getConditions());
    }

    /**
     * 添加公告界面
    */
    public function add(){
        $channelGroupModel = new ChannelgroupModel();
        $channelGroup = $channelGroupModel->getChannelGroupByTrait();
        $channelGroupRow = array();
        foreach ($channelGroup as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $server = (new ServerModel())->getServerByTrait();
        $this->smarty->assign('groups',$channelGroupRow);
        $this->smarty->assign('servers',$server);
        $this->smarty->display('vipnotice/add.tpl');
    }
    public function add_action(){
        //error_log(json_encode($_POST,JSON_UNESCAPED_UNICODE)."\n\r",3,'server.log');exit;
        $type = $this->getParam('type');
        $first_level = $this->getParam('first_level');
        $mail_title = $this->getParam('mail_title');
        $content = $this->getParam('content');
        $connect = $this->getParam('connect');
        $group_id = $this->getParam('channel_group');
        $serverIds = $this->getParam('server');
        $id = $this->getParam('id');

        empty($connect) && $connect = array();//初始化数据类型
        //判断推送方式
        if(empty($type)){
            die(json_encode(['title'=>406,'html'=>'请选择推送方式']));
        }
        //判断充值档次
        if(empty($first_level)){
            die(json_encode(['title'=>406,'html'=>'请填写开启档次']));
        }
        //判断推送信息
        if(in_array('2',$type)){
            if(empty($connect))die(json_encode(['title'=>406,'html'=>'请填写联系方式']));
        }else{
            if(empty($mail_title) || empty($content))die(json_encode(['title'=>406,'html'=>'请填写邮件标题和内容']));
        }
        //判断渠道组
        if(empty($group_id))die(json_encode(['title'=>406,'html'=>'请选择渠道组']));
        $row = $this->vipnoticeModel->getRow('*',array('group_id'=>$group_id));
        if(!$id){
            !empty($row) && die(json_encode(['title'=>406,'html'=>'该渠道组存在，请使用编辑']));
        }
        //只有在编辑的时候才启动数据校验
        $datas = $_POST;
        $signRecord = self::getDataSign($datas);
        if($id) {
            $sign = $this->vipnoticeModel->getRow(array('sign'),array('id'=>$id));
            if (($sign['sign'] == $signRecord) || empty($sign)) {
                die(json_encode(['title' => 304, 'html' => '没有任何改变']));
            }
        }
        $merch = '/[(\r)|(\r\n)|(\n)]/';
        foreach($content as &$msg){
            $msg = preg_replace($merch,'X22#',$msg);
        }
        $serverIds = empty($serverIds[0])?'':join(',',$serverIds);
        $data = array(
            'group_id'=>$group_id,
            'server'=>$serverIds,
            'type'=>join(',',$type),//find)_in_set
            'title'=>json_encode($mail_title,JSON_UNESCAPED_UNICODE),//防止unicode编码
            'level'=>json_encode($first_level),
            'connect'=>json_encode($connect,JSON_UNESCAPED_UNICODE),
            'content'=>json_encode($content,JSON_UNESCAPED_UNICODE),
            'manage'=>'GM',
            'status'=>2,//状态默认为拒绝
            'sign'=>$signRecord,
            'last_modified_time'=>time(),
        );
        if(empty($id)) $data['create_time'] = time();
        $status = false;
        $int = [];
        foreach($data as $key=>$value){
            if(empty($value)) continue;
            array_push($int,$key.'='.'\''.$value.'\'');//防止被转义
        }
        $param  = join(',',$int);
        if(!empty($id)){
            $sql = "update ny_vipnotice set ".$param.' where id='.$id.'';
            $res = $this->vipnoticeModel->query($sql);
            //$res = $this->vipnoticeModel->update($data,array('id' => $id));
            if($res) $status = true;
            $datas['id'] = $id;
            $datas['emailSender'] = $data['manage'];//发件人
        }else{
            $sql = "insert into ny_vipnotice set ".$param;
            $res = $this->vipnoticeModel->query($sql);
            //$res = $this->vipnoticeModel->add($data);
            if($res) $status = true;
            $id = $res;//最后写入的id
            $datas['id'] = $id;
            $datas['emailSender'] = $data['manage'];//发件人
        }
        $row = array();
        if($status){
            //更新mongo
            #code for mongo retur
            $datas['server'] = $serverIds;
            $row = $this->vipnoticeModel->syncPush($datas);
        }
        if($res){
            $mongoBack = '';
            foreach($row as $msg){
                $backStatus = explode('/',$msg);

                if($backStatus[0] == 1)//表示成功
                {
                    $mongoBack .= '<span style="color:#36ca95;font-weight: 400">'.$backStatus[1].'</span><br/>';
                }else{
                    $mongoBack .= '<span style="color:#ca1b36;font-weight: 400">'.$backStatus[1].'</span><br/>';
                }
            }
            empty($mongoBack) && $mongoBack = '更新至mongo库失败';
            $out = array('title'=>'操作结果','html'=>'保存成功<br/>'.$mongoBack);
        }else{
            $out = array('title'=>'操作结果','html'=>'保存失败<br/>');
        }
        die(json_encode($out));
    }

    /**
     * 编辑公告
    */
    public function edit(){
        $id = $this->getParam('id');//method get
        if(empty($id))exit();
        $channelGroupModel = new ChannelgroupModel();
        $res = $this->vipnoticeModel->getVipnotice(array(),$id);
        $type = explode(',',$res['type']);
        $title = json_decode($res['title'],true);
        $connect = json_decode($res['connect'],true);
        $content = json_decode($res['content'],true);
        $level = json_decode($res['level'],true);
        foreach($content as &$msg){
            $msg = str_replace('X22#X22#',"\n",$msg);
        }
        $row = array(
            'type'=>$type,
            'title'=>$title,
            'connect'=>$connect,
            'content'=>$content,
            'level'=>$level,
        );
        function get_last_key($params){
            krsort($params['arr']);//数组{"1":"50","2":"100","3":"200"}
            $params['arr'] = array_keys($params['arr']);
            $lastKey = isset($params['arr'][0])?intval($params['arr'][0])+1:1;
            return $lastKey;
        }
        $this->smarty->register_function('getLastKey','get_last_key');
        $channelGroup = $channelGroupModel->getChannelGroupByTrait();
        $server = (new ServerModel())->getServerByTrait();
        $ser = explode(',',$res['server']);
        $ser = array_filter($ser);
        $channelGroupRow = array();
        foreach ($channelGroup as $cInfo) {
            $group_id = $cInfo['id'];
            $channelGroupRow[$group_id] = $cInfo['name'];
        }
        $this->smarty->assign('groups',$channelGroupRow);
        $this->smarty->assign('servers',$server);
        $this->smarty->assign('id',$res['id']);
        $this->smarty->assign('data',array('agent'=>array($res['group_id']),'server'=>$ser));
        $this->smarty->assign('row',$row);
        $this->smarty->display('vipnotice/edit.tpl');
    }

    /**
     * 查看内容
    */
    public function view(){
        $id = $this->getParam('id');
        if(!is_numeric($id) || empty($id)){
            $out = array(
                'title'=>'警告！',
                'html'=>'非法操作'
            );
            die(json_encode($out));
        }
        $row = $this->vipnoticeModel->getVipnotice(array('type','title','connect','content','level'),$id);
        foreach($row as $key=>&$msg){
            $row[$key] = json_decode($msg);
            if($key == 'content'){
                $msg = str_replace('X22#X22#'," ",$msg);
            }
        }
        $typeType = $this->vipnoticeModel->type;
        $type = '';
        foreach($row['type'] as $v){
            $type .= $typeType[$v]."<br/>";
        }
        $info = '';
        foreach($row['connect'] as $key=>$val){
            $info .= '标题：'.$row['title'][$key].' 档次：'.$row['level'][$key]."<br/>"." 邮件内容：".$row['content'][$key]."<br/>"." 联系方式：".$row['connect'][$key]."<br/>";
        }
        $html = $type.$info;
        exit(json_encode(array('title'=>'内容','html'=>$html)));
    }
    /**
     * 查看详情
    */
    public function more(){
        $id = $this->getParam('id');
        if(empty($id)) die(json_encode(['title'=>'警告','html'=>'id不存在，查询详情失败']));
        $dataRow = $this->vipnoticeModel->getVipnotice(array('back_massage','status'),$id);
        $back_massage = json_decode($dataRow['back_massage'],true);
        $server_id = array_filter(array_keys($back_massage));
        $serverInfo = (new ServerModel())->getServerByServerAndChannelGroup($server_id,array());
        $back = '';
        if($dataRow['status'] == 3) {
            //已经发布
            foreach ($back_massage as $sid => $msg) {
                $backStatus = explode('/', $msg);

                if ($backStatus[0] == 1)//表示成功
                {
                    $back .= '<span style="color:#36ca95;font-weight: 400">' . $serverInfo[$sid] . ':'.$backStatus[1] . '</span><br/>';
                } else {
                    $back .= '<span style="color:#ca1b36;font-weight: 400">' . $serverInfo[$sid] . ':' . (empty($backStatus[1]) ? $msg : $backStatus[1]) . '</span><br/>';
                }
            }
        }else if($dataRow['status'] == 1){
            //已经同步至mongo
            foreach($back_massage as $sid =>$msg){
                if($msg == 1){
                    #success
                    $back .= '<span style="color:#36ca95;font-weight: 400">' . $serverInfo[$sid] . ': 写入数据库成功，同步mongo成功'.'</span><br/>';
                }else{
                    $back .= '<span style="color:#ca1b36;font-weight: 400">' . $serverInfo[$sid] . ': 写入数据库成功，同步mongo失败'.'</span><br/>';
                }
            }
        }
        die(json_encode(['title'=>'操作详情','html'=>$back]));
    }

    /**
     * 推送信息
     * @mark 写入mongoDB
     * @mark 发送协议
    */
    public function noticeUp(){
        $id = $this->getParam('id');
        if(empty($id)){
            die(json_encode(['msg'=>'非法操作']));
        }
        $dataRow = $this->vipnoticeModel->getVipnotice(array(),$id);
        if(empty($dataRow)) die(json_encode(['msg'=>'该记录不存在']));
        $dataRow['handel_type'] = 'up';
        $server = explode(',',$dataRow['server']);
        $res = $this->vipnoticeModel->upVipNotice($dataRow);
        //error_log(json_encode($res)."\n\r",3,'server.log');
        $backMsg = array(
            '0'=>'更新失败',
            '2'=>'非法操作',
            '1'=>'更新成功',
            '-3'=>'服务器连接失败'
        );
        $serverInfo = (new ServerModel())->getServerByServerAndChannelGroup($server,array($dataRow['group_id']));
        //print_r($serverInfo);exit;
        $html = [];
        foreach($res as $sid=>$ret){
            $vs = isset($backMsg[$ret])?$backMsg[$ret]:$ret;
            $serName = $serverInfo[$sid];
            $temp = $serName." : ".$vs;
            $html[] = $temp;
        }
        $html = join("<br/>",$html);
        die(json_encode(['title'=>'操作结果','html'=>$html]));
        #code
    }
    /**
     * 下架公告
     * @mark 发送协议
    */
    public function noticeDown(){
        $id = $this->getParam('id');

        if(empty($id))die('id不存在，查询详情失败');

        $dataRow = $this->vipnoticeModel->getVipnotice(array(),$id);
        if(empty($dataRow)) die('该记录不存在');
        $servers = $dataRow['server'];
        $dataRow['handel_type'] = 'down';
        $res = $this->vipnoticeModel->upVipNotice($dataRow);
        //error_log(json_encode($res)."\n\r",3,'server.log');
        $backMsg = array(
            '0'=>'下架失败',
            '2'=>'非法操作',
            '1'=>'下架成功',
            '-3'=>'服务器连接失败'
        );
        $serverRes = (new ServerModel())->getServerByServerAndChannelGroup($servers,array());
        $html = [];
        foreach($res as $sid=>$ret){
            $vs = isset($backMsg[$ret])?$backMsg[$ret]:$ret;
            $serName = $serverRes[$sid];
            $temp = $serName." : ".$vs;
            $html[] = $temp;
        }
        $html = join("<br/>",$html);
        die(json_encode(['title'=>'操作结果','html'=>$html]));
    }

    /**
     * 同意/拒绝
     * @mark 脑残操作
    */
    public function agreed(){
        $id = $this->getParam('id');
        $status = $this->getParam('type');

        if(empty($id) || !in_array($status,array(1,2))){
            exit(json_encode(array('code'=>404,'msg'=>'非法操作')));
        }
        $data = array(
            'last_modified_time'=>time(),
            'status'=>$status
        );
        $res = $this->vipnoticeModel->update($data,array('id'=>$id));
        if($res){
            exit(json_encode(['code'=>202,'msg'=>'修改成功']));
        }else{
            exit(json_encode(['code'=>505,'msg'=>'修改失败']));
        }
    }

    /**
     * 校验数据是否有更新
     * @param array $data
     * @return boolean
    */
    private function getDataSign(array $data){
        if(empty($data)) return md5('');
        $connect = $data['connect'];
        $first_level = $data['first_level'];
        $mail_title = $data['mail_title'];
        $content = $data['content'];
        $type = $data['type'];
        $server = empty($data['server'][0])?array():$data['server'];
        #code for merge data
        /**--加密数据，生成对比依据-*/
        $signArr = [];
        foreach($connect as $key=>$val){
            $str = @$first_level[$key].trim(@$mail_title[$key]).trim(@$content[$key]).trim(@$connect[$key]);
            $str = strtolower(md5($str));
            array_push($signArr,$str);
        }
        $sign = strtoupper(md5(join('',$signArr).join(',',$type).join(',',$server)));//校验sign
        /**----------------------*/
        return $sign;
    }
}
?>