<?php
!defined('IN_WEB') && exit('Access Denied');

class WhiteController extends Controller{

    public function __construct(){
        parent::__construct();
    }

    //白名单（菜单）
    public function index(){
        $tabs = array(
            array('title' => 'IP白名单列表', 'url' => 'admin.php?&ctrl=white&act=record'),
            array('title' => '添加IP白名单', 'url' => 'admin.php?&ctrl=white&act=edit')
        );
        $this->tabs($tabs);
    }

    public function record(){
        $type = CDict::$banType;
        $type[0] = '全部';
        ksort($type);
        $this->setSearch('IP');
        $this->setSearch('时间' , 'range_time');
        $this->setFields(array('IP' , '服务器' , '管理员' , '创建时间' , '操作'));
        $this->setSource('white' , 'record_data');
        $this->setLength(100);
        $js = <<< END
            function del(id){
                $.ajax({
                    url: 'admin.php?ctrl=white&act=del_action&id=' + id,
                    type: 'GET'
                }).done(function(){
                    \$tabs.tabs('load' , 0);
                })
            }
END;
        $this->setJs($js);
        $this->displays();
    }
    public function record_data(){
        $this->setCondition(0 , 'eq' , 'ip');
        $this->setCondition(1 , 'eq' , 'create_name');
        $this->setOrder();
        $this->setLimit(100);
        $this->call('WhiteModel' , 'record_data' , $this->getConditions());
    }

    public function edit(){
        $server = (new WhiteModel())->getGm(array('s.server_id' , 's.name'));
        $this->smarty->assign('server' , $server);
        $this->smarty->display('white/edit.tpl');
    }

    public function edit_action(){
        $server = $this->getParam('server');
        $ip = $this->getParam('ip');
        if($server && $ip){
            $data = array();
            $ip = explode(',' , $ip);
            $create_time = time();
            $user = $_SESSION['username'];
            foreach($ip as $k => $v){
                $v = trim($v);
                if(!preg_match('/^\d+\.\d+\.\d+\.\d+$/' , $v)){
                    exit(json_encode(array('msg' => 'IP格式存在错误')));
                }
                else{
                    $data[$k]['ip'] = $v;
                    $data[$k]['server'] = $server;
                    $data[$k]['user'] = $user;
                    $data[$k]['create_time'] = $create_time;
                }
            }
            if((new WhiteModel())->multiAdd($data)){
				$wm = new WhiteModel();
				$wm->getWhiteIps(1);
                $json = array('msg' => '添加成功');
            }
            else{
                $json = array('msg' => '添加遇到错误');
            }
            echo json_encode($json);
        }
    }
    public function del_action(){
        $id = $this->getParam('id');
        $wm = new WhiteModel();
        $wm->delete(array('id' => $id));
        $wm->getWhiteIps(1);
    }
}