<?php
!defined('IN_WEB') && exit('Access Denied');

class WhiteipController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    //白名单（菜单）
    public function index()
    {
        $tabs = array(
            array('title' => 'IP白名单列表', 'url' => 'admin.php?&ctrl=whiteip&act=record'),
            array('title' => '添加IP白名单', 'url' => 'admin.php?&ctrl=whiteip&act=edit')
        );
        $this->tabs($tabs);
    }

    public function record()
    {
        $this->setSearch('IP');
        $this->setFields(array('IP', '状态', '说明', '创建时间', '操作'));
        $this->setSource('whiteip', 'record_data');
        $this->setLength(100);
        $js = <<< END
            function del(id){
                if(confirm('确定删除此白名单？')){
                    $.ajax({
                        url: 'admin.php?ctrl=whiteip&act=del_action&id=' + id,
                        type: 'GET'
                    }).done(function(){
                        \$tabs.tabs('load' , 0);
                    })
                }
            }
			
			function change(id,type){
                $.ajax({
                    url: 'admin.php?ctrl=whiteip&act=change_action&id=' + id + '&type=' + type,
                    type: 'GET'
                }).done(function(){
                    \$tabs.tabs('load' , 0);
                })
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    public function record_data()
    {
        $this->setCondition(0, 'eq', 'ip');
        $this->setOrder();
        $this->setLimit(100);
        $this->call('WhiteipModel', 'record_data', $this->getConditions());
    }

    public function edit()
    {

        $this->smarty->display('whiteip/edit.tpl');
    }

    public function edit_action()
    {

        $ip = $this->getParam('ip');
        $contents = $this->getParam('contents');
        if ($ip) {
            $data = array();
            $ip = explode("\n", $ip);

            foreach ($ip as $key => $value) {
                $ip[$key] = trim($value);
            }
            $ip = array_unique(array_filter($ip));
            $create_time = time();

            foreach ($ip as $k => $v) {
                $v = trim($v);
                if (!preg_match('/^\d+\.\d+\.\d+\.\d+$/', $v)) {
                    exit(json_encode(array('msg' => 'IP格式存在错误')));
                } else {
                    $data[$k]['ip'] = $v;
                    $data[$k]['contents'] = $contents;
                    $data[$k]['create_time'] = $create_time;
                }
            }

            if ((new WhiteipModel())->multiAdd($data)) {
                $serverRedis = new ServerRedis(true);
                $serverRedis->updateWhiteIp();
                $json = array('msg' => '添加成功');
            } else {
                $json = array('msg' => '添加遇到错误');
            }
            echo json_encode($json);
        }
    }

    public function del_action()
    {
        $id = $this->getParam('id');
        $wm = new WhiteipModel();
        $row = $wm->getRow(['ip'], ['id' => $id]);
        $ip = $row['ip'];
        $wm->delete(array('id' => $id));
        $serverRedis = new ServerRedis(true);
        $serverRedis->delWhiteIp($ip);
    }

    public function change_action()
    {
        $id = $this->getParam('id');
        $type = $this->getParam('type');
        if ($type == 0) {
            $type = 1;
        } elseif ($type == 1) {
            $type = 0;
        }
        $wm = new WhiteipModel();
        $wm->update(array('type' => $type), array('id' => $id));

        $row = $wm->getRow(['ip'], ['id' => $id]);

        $ip = $row['ip'];

        $serverRedis = new ServerRedis(true);
        if ($type == 1) {
            $serverRedis->delWhiteIp($ip);
        } else {
            $serverRedis->addWhiteIp($ip);
        }

    }
}