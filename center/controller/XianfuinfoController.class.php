<?php
!defined('IN_WEB') && exit('Access Denied');

class XianfuInfoController extends Controller
{

    public function index()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日开始连接游戏服务器并且开启护送仙女的人数。<br>';
        $header .= '汇灵参与度 =  该日收获汇灵阵次数≥1的玩家数 / 活跃人数 *100%<br>';
        $header .= '聚宝参与度 =  该日收获聚宝盆次数≥1的玩家数 / 活跃人数 *100%<br>';
        $header .= '炼制参与度 =  该日进行过炼制≥1（任意一种炼制都算）的玩家数 / 活跃人数 *100%<br>';
        $header .= '人均炼制次数 = 该日全服所有玩家进行炼制的次数总和（三种炉子的炼制次数都要加上） / 该日进行过炼制≥1的玩家数 *100%<br>';
        $header .= '游历参与度 =  该日进行过游历≥1（任意一个灵兽任意一种游历都算）的玩家数 / 活跃人数 *100% <br>';
        $header .= '人均游历次数 = 该日全服所有玩家进行游历的次数总和（四种灵兽的游历次数都要加上） / 该日进行过游历≥1的玩家数 *100%<br>';
        $header .= '游历罗盘参与度 = 该日全服【四海八荒】及【九州三界】类的游历中使用罗盘的游历次数 / 该日全服【四海八荒】及【九州三界】的游历总次数 *100% <br>';
        $header .= '人均罗盘次数 = 该日全服【四海八荒】及【九州三界】类的游历中使用罗盘的游历次数 / 该日全服使用罗盘的玩家人数   <br>';
        $header .= '游历立即结束参与度 = 该日全服游历中立即结束的游历次数 / 该日全服的游历总次数 *100%<br>';
        $header .= '人均立即结束次数 = 该日全服游历中立即结束的游历次数 / 该日全服进行过立即结束游历的人数  <br>';

        $header .= '仙府任务参与度1 = 该日仙府任务活跃度≥20 的玩家数 / 活跃人数 *100% <br>';
        $header .= '仙府任务参与度2 = 该日仙府任务活跃度≥60 的玩家数 / 活跃人数 *100%  <br>';
        $header .= '仙府任务参与度3 = 该日仙府任务活跃度≥100 的玩家数 / 活跃人数 *100% <br>';
        $header .= '仙府任务参与度4 = 该日仙府任务活跃度≥150 的玩家数 / 活跃人数 *100% <br>';

        $this->setHeader($header);
        $this->setSearch('日期', 'range_time');
        $this->setSearch('', 'scp', array(0, 0, 1, 0));
        $this->setField('时间', 100);
        $this->setFields(array('服务器', '活跃人数', '汇灵参与度', '聚宝参与度', '炼制参与度', '人均炼制次数', '游历参与度', '人均游历次数', '游历罗盘参与度', '人均罗盘次数', '游历立即结束参与度', '人均立即结束次数', '仙府任务参与度1', '仙府任务参与度2', '仙府任务参与度3', '仙府任务参与度4'));
        $this->setSource('XianfuInfo', 'index_data', 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function index_data()
    {
        $this->setCondition(0, 'time', 'create_time');
        $this->setCondition(1, 'scp');
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('record_data', 'record_export');
        $this->call('XianfuInfoModel', $method, $this->getConditions());
    }
}