<?php
!defined('IN_WEB') && exit('Access Denied');

class ZoneController extends Controller{
    private $zm;

    public function __construct(){
        parent::__construct();
        $this->zm = new ZoneModel();
    }

    //分区管理（菜单）
    public function index(){
        $tabs = array(
            array('title' => '分区列表', 'url' => 'admin.php?&ctrl=zone&act=record'),
            array('title' => '添加分区', 'url' => 'admin.php?&ctrl=zone&act=add'),
            array('title' => '编辑分区', 'url' => 'admin.php?&ctrl=zone&act=edit'),
        );
        $this->tabs($tabs);
    }

    //分区列表（子菜单）
    public function record(){
        $this->setFields(array('ID' , '渠道组', '名称' , '排序', '分区备注', 'cdn链接', '创建时间' , '操作'));
        $this->setSource('zone' , 'record_data');
        $js = <<< END
            function edit(id){
                \$tabs.tabs('select' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=zone&act=edit&id=' + id);
                \$tabs.tabs('load' , 2);
                \$tabs.tabs('url' , 2 , 'admin.php?ctrl=zone&act=edit&id=' + id);
            }
            function del(id){
                $.ajax({
                    url: 'admin.php?ctrl=zone&act=del_action&id=' + id,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(){
                    \$tabs.tabs('load' , 0);
                })
            }
END;
        $this->setJs($js);
        $this->displays();
    }

    //分区列表（数据）
    public function record_data(){
        $this->setLimit();
        $this->call('ZoneModel' , 'record_data' , $this->getConditions());
    }

    //添加分区（子菜单）
    public function add(){
        $this->smarty->assign('groups', (new ChannelgroupModel())->getChannelGroup(array(), array('id', 'name')));
        $this->smarty->display('zone/add.tpl');
    }

    public function add_action(){
        $name = trim($this->getParam('name'));
        $group_id = (int)$this->getParam('group_id');
        $remark = trim($this->getParam('remark'));
        $cdn_url = self::trimAll($this->getParam('cdn_url'));
        $sort = (int)$this->getParam('sort');

        if(!$name){
            $json = array('msg' => '没有填写分区名称' , 'code' => 0);
            exit;
        }

        if (!$group_id) {
            $json = array('msg' => '没有填写渠道组' , 'code' => 0);
            exit;
        }

        if (!$remark) {
            $json = array('msg' => '没有填写分区说明', 'code' => 0);
            exit;
        }

        if (!$cdn_url) {
            $json = array('msg' => '没有填写cdn链接', 'code' => 0);
            exit;
        }

        if (!$sort) {
            $json = array('msg' => '没有填写分区排序', 'code' => 0);
            exit;
        }

        $time = time();

        $data = array('name' => $name , 'group_id' => $group_id, 'remark' => $remark, 'cdn_url' => $cdn_url, 'sort' => $sort, 'create_time' => $time);
        $insertId = $this->zm->addZone($data);
        $json = $insertId ? array('msg' => '添加成功', 'code' => 1) : array('msg' => '分区名称已存在', 'code' => 0);

        $redisData = ['id' => $insertId, 'group_id' => $group_id, 'name' => $name, 'sort' => $sort, 'cdn_url' => $cdn_url];
        (new ServerRedis(true))->setZone(1, $redisData);

        (new LogAction())->logAdd("新增分区：{$insertId} => {$name} ");


        echo json_encode($json);
    }

    //编辑分区（子菜单）
    public function edit(){
        $id = $this->getParam('id');
        $zone = $this->zm->getZone($id);
        $id && $this->smarty->assign('id' , $id);
        $this->smarty->assign('name' , $zone['name']);
        $this->smarty->assign('group_id', $zone['group_id']);
        $this->smarty->assign('remark', $zone['remark']);
        $this->smarty->assign('cdn_url', $zone['cdn_url']);
        $this->smarty->assign('sort', $zone['sort']);

        $this->smarty->assign('groups', (new ChannelgroupModel())->getChannelGroup(array(), array('id', 'name')));
        $this->smarty->display('zone/edit.tpl');
    }

    public function edit_action(){
        $id = $this->getParam('id');
        $name = trim($this->getParam('name'));
        $group_id = (int)$this->getParam('group_id');
        $remark = trim($this->getParam('remark'));
        $cdn_url = self::trimAll($this->getParam('cdn_url'));
        $sort = (int)$this->getParam('sort');

        if($id){
        if(!$name){
            $json = array('msg' => '没有填写分区名称' , 'code' => 0);
            exit;
        }

        if (!$group_id) {
            $json = array('msg' => '没有填写渠道组' , 'code' => 0);
            exit;
        }

        if (!$remark) {
            $json = array('msg' => '没有填写分区说明', 'code' => 0);
            exit;
        }

        if (!$cdn_url) {
            $json = array('msg' => '没有填写cdn链接', 'code' => 0);
            exit;
        }

        if (!$sort) {
            $json = array('msg' => '没有填写分区排序', 'code' => 0);
            exit;
        }

        $data = array('name' => $name , 'group_id' => $group_id, 'remark' => $remark, 'cdn_url' => $cdn_url, 'sort' => $sort);

        if($this->zm->editZone($data , $id)){
            $json = array('msg' => '保存成功' , 'code' => 1);
            $redisData = ['id' => $id, 'name' => $name];
            (new ServerRedis(true))->setZone(3);
            (new LogAction())->logAdd("修改分区：{$id} => {$name} ");
        }
        else{
            $json = array('msg' => '分区名称没有改变' , 'code' => 0);
        }
        echo json_encode($json);
        }
    }

    public function del_action(){
        $id = $this->getParam('id');
        $id && $this->zm->delZone($id);

        $json = ['msg' => '删除失败', 'code' => 0];

        if ($id) {
            $this->zm->delZone($id);
            $redisData = ['id' => $id];
            (new ServerRedis(true))->setZone(3, $redisData);
            $json = ['msg' => '删除成功', 'code' => 1];

            (new LogAction())->logAdd("删除分区：{$id}");
        }

        echo json_encode($json);
    }

    private static function trimAll($str)
    {
        $str = trim($str);
        $qian = array("", " ", "\t", "\n", "\r");
        $hou = array("", "", "", "");
        return str_replace($qian, $hou, $str);

    }
}