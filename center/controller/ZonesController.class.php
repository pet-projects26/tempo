<?php
!defined('IN_WEB') && exit('Access Denied');

class ZonesController extends Controller{

    public function zones1()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日成功连接上游戏服务器并且开启天关玩法的玩家人数（备注：开启条件待定，已上线前版本为准）。<br>';
        $header .= '参与人数：该日进行过天关挑战玩法（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均挑战次数 = 该日全服每个玩家挑战天关次数之和（成功才算） / 参与人数   （确定到小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '参与人数' , '参与度', '人均挑战次数'));
        $this->setSource('Zones' , 'zones1_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function zones1_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setCondition(2 , 'eq' , 'zones_type' ,1);
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('zones_data' , 'zones_export');
        $this->call('ZonesModel' , $method , $this->getConditions());
    }

    public function zones2()
    {
        $header = '说明：<br>';
        $header .= '活跃人数：该日成功连接上游戏服务器并且开启大荒古塔玩法的玩家人数（备注：开启条件待定，已上线前版本为准）。<br>';
        $header .= '参与人数：该日进行过大荒古塔挑战玩法（进入场景即算）的玩家数（每个玩家ID只算一次，需去重）<br>';
        $header .= '参与度 = 参与人数 /活跃人数 *100% （精确到小数点后两位，四舍五入）<br>';
        $header .= '人均挑战次数 = 该日全服每个玩家挑战大荒古塔次数之和（成功才算） / 参与人数   （确定到小数点后两位，四舍五入）<br>';

        $this->setHeader($header);
        $this->setSearch('日期' , 'range_time');
        $this->setSearch('' , 'scp' , array(0 , 0 , 1 , 0));
        $this->setField('时间' , 100);
        $this->setFields(array('服务器' , '活跃人数' , '参与人数' , '参与度', '人均挑战次数'));
        $this->setSource('Zones' , 'zones2_data' , 1);
        $this->smarty->assign('select', 'select');
        $this->displays();
    }

    public function zones2_data()
    {
        $this->setCondition(0 , 'time' , 'create_time');
        $this->setCondition(1 , 'scp');
        $this->setCondition(2 , 'eq' , 'zones_type' ,2);
        $this->setOrder('create_time', 'desc');
        $this->setLimit();
        $method = $this->setDataExport('zones_data' , 'zones_export');
        $this->call('ZonesModel' , $method , $this->getConditions());
    }
}