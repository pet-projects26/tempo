<?php


class ActiveAccountListPlanController extends CronController
{

    public function run(array $argv = null)
    {

        $NodeRedis = (new NodeRedis());

        $num = 600;
        $msg = '';

        $isMonth = cronBase::checkIsMonth();

        if ($isMonth) {
            $num = 0;
        }

        $info = $NodeRedis->rPopActiveAccountList($num);

        if (!empty($info) && !empty($info['list']) && !empty($info['hashKey'])) {
            $List = $info['list'];

            $HashKeySet = $info['hashKey'];

            $ActiveAccountModel = new ActiveAccountModel();

            $data = [];

            foreach($List as $row) {
                //执行逻辑
                $data[] = $row;
            }

            if (!empty($data)) {
                if ($ActiveAccountModel->multiAdd($data)) {
                    //插入成功则删除所有hashKey
                    $NodeRedis->delHashKey($HashKeySet);
                    $msg = 'success';
                } else {
                    $NodeRedis->rPushAccountNodeList($HashKeySet);
                    $msg = 'error';
                }
            } else {
                $msg = 'data is empty ~';
            }
        }
/*
        //分月表
        if ($isMonth == 1) {
            (new SplitSqlUtil('active_account'))->checkRenameTable();
        }
*/
        echo $msg;
    }
}