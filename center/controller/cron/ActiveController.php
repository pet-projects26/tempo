<?php

/**
 * 活跃统计
 * 每天凌晨跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class ActiveController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Active', 'stat', $serList, $start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Active = new ActiveModel();
        $Node = new NodeModel();
        $ActiveAccount = new ActiveAccountModel();

        $data = [];

        $serIdList = array_keys($serList);
        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_active', $serIdList, $time);

        foreach ($serList as $key => $value) {

            $dat = [];

            //获取活跃玩家数与活跃玩家
            $active_data = $ActiveAccount->getActiveAccount($start_time, $end_time, $value['server_id']);

            $dat['active_num'] = !empty($active_data) ? count($active_data) : 0;
            $dat['server'] = $value['server_id'];
            $dat['create_time'] = $time;

            //获取活跃用户的登录次数和登录时长
            if ($dat['active_num'] > 0) {

                $active_acc_login_num = $Active->call('Login', 'getLogTimesByDate_json', array('date' => $date, 'accounts' => $active_data), $value['server_id']);

                $active_acc_login_longtime_arr = $Active->call('Login', 'getOlTimeByDate_json', array('start_time' => $start_time, 'end_time' => $end_time, 'accounts' => $active_data), $value['server_id']);

                $active_acc_login_longtime = 0;

                if ($active_acc_login_longtime_arr[$value['server_id']]) {
                    foreach ($active_acc_login_longtime_arr[$value['server_id']] as $v) {
                        $active_acc_login_longtime += $v;
                    }
                }
                if (!$active_acc_login_num[$value['server_id']]) $active_acc_login_num[$value['server_id']] = 0;
            } else {
                $active_acc_login_num[$value['server_id']] = 0;
                $active_acc_login_longtime = 0;
            }

            $dat['login_num'] = $active_acc_login_num[$value['server_id']];

            //人均登录次数
            $dat['avg_login_num'] = $dat['login_num'] ? round($dat['login_num'] / $dat['active_num'], 2) : 0;
            $dat['avg_login_longtime'] = $active_acc_login_longtime ? round($active_acc_login_longtime / $dat['active_num'], 2) : 0; // 人均在线时长

            //获取新增玩家数与新增玩家
            $new_active_data = $Node->getNewAccount($start_time, $end_time, $value['server_id']);

            $dat['new_acc_num'] = count($new_active_data);

            //获取新增用户的登录次数和登录时长
            if ($new_active_data) {

                $new_active_acc_login_num_res = $Active->call('Login', 'getLogTimesByDate_json', array('date' => $date, 'accounts' => $new_active_data), $value['server_id']);

                $new_active_acc_login_num = $new_active_acc_login_num_res[$value['server_id']] ? $new_active_acc_login_num_res[$value['server_id']] : 0;

                $new_active_acc_login_longtime_arr = $Active->call('Login', 'getOlTimeByDate_json', array('start_time' => $start_time, 'end_time' => $end_time, 'accounts' => $new_active_data), $value['server_id']);

                $new_active_acc_login_longtime = 0;

                if ($new_active_acc_login_longtime_arr[$value['server_id']]) {
                    foreach ($new_active_acc_login_longtime_arr[$value['server_id']] as $v) {
                        $new_active_acc_login_longtime += $v;
                    }
                }
            } else {
                $new_active_acc_login_num = 0;
                $new_active_acc_login_longtime = 0;
            }

            //新增用户人均登录次数
            $dat['new_acc_avg_login_num'] = $new_active_acc_login_num ? round($new_active_acc_login_num / $dat['new_acc_num'], 2) : 0;
            $dat['new_acc_avg_login_longtime'] = $new_active_acc_login_longtime ? round($new_active_acc_login_longtime / $dat['new_acc_num'], 2) : 0; // 新增用户人均在线时长

            //获取老玩家
//          $old_acc_data = $Node->getAccountConnectionServerData($start_time, $end_time, $value['server_id'], 0);

            //老玩家定义修改为 非本日新增创角玩家
            $old_acc_data = $Node->call('Role', 'getOldAccounts_json', array('time' => $start_time), $value['server_id']);
            $old_acc_data = $old_acc_data[$value['server_id']];

            if ($old_acc_data) {
                //查询node表看看该批老玩家有没有连接游戏服务器
                $old_acc_data = $ActiveAccount->getActiveAccount($start_time, $end_time, $value['server_id'], '', $old_acc_data);
            }

            $dat['old_acc_num'] = count($old_acc_data);

            if ($old_acc_data) {

                $old_acc_login_num_res = $Active->call('Login', 'getLogTimesByDate_json', array('date' => $date, 'accounts' => $old_acc_data), $value['server_id']);

                $old_acc_login_num = $old_acc_login_num_res[$value['server_id']] ? $old_acc_login_num_res[$value['server_id']] : 0;

                $old_acc_login_longtime_arr = $Active->call('Login', 'getOlTimeByDate_json', array('start_time' => $start_time, 'end_time' => $end_time, 'accounts' => $old_acc_data), $value['server_id']);

                $old_acc_login_longtime = 0;

                if ($old_acc_login_longtime_arr[$value['server_id']]) {
                    foreach ($old_acc_login_longtime_arr[$value['server_id']] as $v) {
                        $old_acc_login_longtime += $v;
                    }
                }
            } else {
                $old_acc_login_num = 0;
                $old_acc_login_longtime = 0;
            }

            //老玩家人均登录次数
            $dat['old_acc_avg_login_num'] = $old_acc_login_longtime ? round($old_acc_login_num / $dat['old_acc_num'], 2) : 0;
            $dat['old_acc_avg_login_longtime'] = $old_acc_login_longtime ? round($old_acc_login_longtime / $dat['old_acc_num'], 2) : 0; // 老玩家人均在线时长

            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($Active->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty ~';
        }

        return $msg;
    }
}