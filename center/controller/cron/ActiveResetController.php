<?php

/**
 * 23点59分重置活跃玩家
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class ActiveResetController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('ActiveReset', 'stat', $serList, $start, $end);
        }
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $ActiveAccount = new ActiveAccountModel();

        $data = [];

        foreach ($serList as $key => $value) {

            $server_id = $value['server_id'];

            //查出时间内的活跃账号
            //获取活跃玩家数与活跃玩家
            $active_data = $ActiveAccount->getDataOnServer($start_time + 1, $end_time, $value['server_id']);

            if (empty($active_data)) continue;

            $account = array_keys($active_data);

            //获取单服当前在线的玩家  ny_login star_time 在今天之内且end_time = 0
            //ny_role
            $rs = $ActiveAccount->call('Login', 'getTodayNotLogoutRole', ['start_time' => $start_time, 'end_time' => $end_time, 'account' => $account], $server_id);

            $roleData = $rs[$server_id];

            if (empty($roleData)) continue;

            foreach ($roleData as $row) {
                $dat = [];
                $dat['package'] = $active_data[$row['account']]['package'];
                $dat['channel'] = $active_data[$row['account']]['channel'];
                $dat['mac'] = $active_data[$row['account']]['mac'];
                $dat['ip'] = $active_data[$row['account']]['ip'];
                $dat['server'] = $server_id;
                $dat['create_time'] = strtotime(date('Ymd', $start_time)) + 86400;

                array_push($data, $dat);
            }

        }

        if (!empty($data)) {
            if ($ActiveAccount->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}