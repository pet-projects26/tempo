<?php

/**
 * BOSS之家统计
 * 每天凌晨5点跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class BosshomeController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 19200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $start += 18000; //每天5点 到隔天凌晨5点
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Bosshome', 'stat', $serList, $start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $Bosshome = new BosshomeModel();
        $ActiveAccount = new ActiveAccountModel();

        $data = [];

        //删除今天的数据
        $serIdList = array_keys($serList);
        CronBase::delDataByServerCreateTime('ny_boss_home', $serIdList, $start_time);

        foreach ($serList as $key => $value) {

            $format_start = CronBase::openTimeForStartTime($value['open_time'], $start_time);

            //获取参与人数
            $join = $Bosshome->call('Bosshome', 'getJoinNumByRangtime', array('start_time' => $format_start, 'end_time' => $end_time), $value['server_id']);

            $join_num = $join[$value['server_id']] ? $join[$value['server_id']] : 0;

            if ($join_num == 0) continue;

            //获取活跃玩家数与活跃玩家
            $active_data = $ActiveAccount->getActiveAccount($format_start, $end_time, $value['server_id']);

            if ($active_data) {
                //查看玩家boss之家功能是否开启
                $filterActive = $Bosshome->call('Bosshome', 'getActionOpenToAccount', array('reId' => CDict::$bossHome['reId'], 'accounts' => $active_data, 'time' => $format_start), $value['server_id']);
                $active_num = $filterActive[$value['server_id']] ? $filterActive[$value['server_id']] : 0;
            } else {
                $active_num = 0;
            }

            if ($active_num == 0) continue;

            foreach (CDict::$bossHomeLayer as $layer => $zone) {

                $dat = [];

                //获取数据
                $res = $Bosshome->call('Bosshome', 'getDataByRangtime', array('layer' => $layer, 'start_time' => $format_start, 'end_time' => $end_time), $value['server_id']);

                if (empty($res[$value['server_id']])) {
                    $dat['layer_join_num'] = 0;
                    $dat['boss_die_num'] = 0;
                    $dat['free_num'] = 0;
                    $dat['free_count'] = 0;
                    $dat['pay_num'] = 0;
                    $dat['pay_count'] = 0;
                } else {
                    $dat = $res[$value['server_id']];
                }

                $dat['server'] = $value['server_id'];

                $dat['create_time'] = $start_time;

                $dat['layer'] = $layer;

                $dat['join_num'] = $join_num;

                $dat['active_num'] = $active_num;

                array_push($data, $dat);
            }
        }

        if (!empty($data)) {
            if ($Bosshome->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty ~';
        }

        return $msg;
    }
}