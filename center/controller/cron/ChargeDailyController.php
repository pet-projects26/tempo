<?php
/**
 * 每日充值
 * 新增充值人数：当天新增玩家并在当天进行充值的角色数
 * 充值人数：当天充值的人数
 * 新增充值金额：新增充值人数当天充值的金额
 * 登录ARPU: 充值总额 / 登录人数
 * 新增充值APRU：  新增充值总额 / 充值人数
 * arppu＝充值总额／充值人数
 * 注册ARPU： 充值总额 / 注册人数
 * 
 * @author  lai <[<email address>]>
 * @date(format)
 * 
 */
class ChargeDailyController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
  	public function run(array $argv = null){
  		$serList = CronBase::getServerList();

  		if (empty($argv)) {
  			$time = time();
  			$start_time = strtotime(date('Y-m-d'));

  			//当天零点的时候会重跑昨天的数据，
  			if ($time - $start_time < 1200) {
  				$start_time = $start_time - 86400;
  			}

  			$end_time = $start_time;
  		} elseif (!empty($argv[0]) && !isset($argv[1])) {
  			
  			$start_time = strtotime($argv[0]);
  			$end_time = strtotime(date('Y-m-d'));
  		
  		} elseif (!empty($argv[0]) && !empty($argv[1])) {
  			
  			$start_time = strtotime($argv[0]);
  			$end_time = strtotime($argv[1]);
  		}

  		if ($start_time > $end_time) {
  			die ('start time > end time');
  		}

  		$num = ceil(($end_time - $start_time) / 86400) + 1;

  		//执行命令
  		for ($i = 0; $i < $num; $i++) {
  			$start = $start_time + $i * 86400;
  			$end = $start + 86399;

  			// $serList = array(array('server_id' => 248));
  			//单线程执行
  			foreach ($serList as $server) {
  				$server_id = $server['server_id'];
  				$msg = CronBase::runmultiprocess('ChargeDaily', 'stat', $server_id, $start, $end);
  				echo $msg;
  			}
  		}

  		die();
	}

	/**
	 * [统计]
	 * @param  [string] $server_id  [服ID]
	 * @param  [int] $start_time [开始时间]
	 * @param  [int] $end_time   [结束时间]
	 * @return []             [数据库的更新]
	 */
	public function stat($server_id, $start_time, $end_time) {
		//充值金额与充值人数，充值次数
		$chargeSql = "SELECT 
					  SUM(money) AS money,
					  COUNT(DISTINCT ord.role_id) AS charge_num,
					  COUNT(ord.`id`) AS charge_times,
					  package 
					FROM
					  ny_order AS `ord` 
					  LEFT JOIN ny_role AS role 
					    ON ord.`role_id` = role.role_id 
					WHERE ord.`create_time` BETWEEN {$start_time} 
					  AND {$end_time} 
					GROUP BY package  ";





		//注册人数
		$regSql = "SELECT COUNT(role_id) AS reg_num, package FROM ny_role 
						WHERE create_time  BETWEEN {$start_time} AND {$end_time} GROUP BY package ";

		//登录人数
		$loginSql = "SELECT COUNT(DISTINCT login.role_id) AS login_num, package FROM ny_role AS role
						LEFT JOIN ny_login AS login ON role.role_id = login.role_id
					WHERE login.start_time BETWEEN {$start_time} AND  {$end_time} GROUP BY package ";


		//新增充值人数, 新增充值金额
		$newChargeSql = "SELECT COUNT(DISTINCT ord.role_id) AS  new_charge_num, SUM(`money`) AS new_charge_money, package FROM ny_role AS role 
							LEFT JOIN ny_order AS ord 
						ON  role.role_id  = ord.role_id 
							WHERE  role.create_time BETWEEN {$start_time} AND  {$end_time}
						AND ord.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY  package  ";
        $data = array(
        	'charge' => $chargeSql, 
        	'reg' => $regSql, 
        	'login' => $loginSql,
        	'newCharge' => $newChargeSql,
        );

		$arr  = array('query' => array('data' => $data));



        $res  = CronBase::makeHttpParams($server_id, $arr);


        $urls = $res['urls'];
        $params = $res['params'];

        $post_string = http_build_query($params);

        $res = Helper::httpRequest($urls, $post_string);

        $result = json_decode($res, true);

        if (empty($result)) {
        	die($res);
        }


        $code = $result['code'];

        if ($code != 1) {
        	die($res);
        }

        $data = $result['data']['query'];

        //充值人数，充值金额
        $chargeRes = CronBase::formatArrByPackage($data['charge']);

        //注册人数
        $regRes = CronBase::formatArrByPackage($data['reg']);


        //登录人数
       	$loginRes = CronBase::formatArrByPackage($data['login']);

       	//新增充值人数
       	$newChargeRes = CronBase::formatArrByPackage($data['newCharge']);

        $insert = CronBase::mergeArray($chargeRes,  $regRes);

        $insert = CronBase::mergeArray($insert,  $loginRes);
        $insert = CronBase::mergeArray($insert,  $newChargeRes);

        $m = new Model('charge_daily');

        $m->setNewPrefix('stat_');

        $m->setAlias('stat_charge_daily');

        CronBase::delDataByServer('stat_charge_daily', $server_id, $start_time);

        foreach ($insert as &$row) {
        	$row['server'] = $server_id;
        	$row['time'] = $start_time;

        	$insertId = $m->add($row);
        }

        $msg = PHP_EOL. '服：'. $server_id .' '. date('y-m-d', $start_time). ' success'.PHP_EOL;
        return $msg;
	}
}