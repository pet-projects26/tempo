<?php
/**
 * 充值分布 只能跑一天的数据 20180309
 * @author  william <[<email address>]>
 * @date(format) 2018-02-27
 * 
 */
class ChargeDistributeController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
  	public function run(array $argv = null){
  		$start_time                     = isset($argv[0]) ? strtotime($argv[0]) : strtotime(date('Y-m-d',time()));
        $end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()))+86400;
  		if ($start_time > $end_time) {
  			die ('start time > end time');
  		}
  		//执行命令
  		$msg = $this->stat( $start_time, $end_time);
  		echo $msg;
  		die();
	}

	/**
	 * [统计]
	 * @param  [string] $server_id  [服ID]
	 * @param  [int] $start_time [开始时间]
	 * @param  [int] $end_time   [结束时间]
	 * @return []             [数据库的更新]
	 */
	public function stat($start_time, $end_time) {
		$time = date('Y-m-d',$start_time);
		//日期,充值账号数,服务器
		$chargeSql = "SELECT FROM_UNIXTIME(create_time,'%Y-%m-%d') AS days,
				COUNT(DISTINCT account) as chargecount,`server`
				FROM ny_order WHERE 
				create_time BETWEEN {$start_time} AND {$end_time} AND `status` = 1  GROUP BY `server`";
		$goldSql = "SELECT FROM_UNIXTIME(create_time,'%Y-%m-%d') AS days,
				SUM(gold) AS sum_gold ,`server`
				FROM ny_order WHERE 
				create_time BETWEEN {$start_time} AND {$end_time} AND `status` = 1  GROUP BY `server`,account";

        $m = new Model();
		$chargeData = $m->query($chargeSql);
		$goldData = $m->query($goldSql);
		if (is_array($chargeData)) {
			foreach ($chargeData as $key=>$val) {
				$range1 = $range2 = $range3 = $range4 = $range5 = $range6 = $range7 = $range8 = $range9 = 0;
				$sqlData[$key]['server'] = $val['server'];
				$sqlData[$key]['chargenum'] = $val['chargecount'];
				$sqlData[$key]['create_time'] = $val['days'];
				foreach ($goldData as $k => $v) {
					if ($val['server'] == $v['server']) {
						if ($v['sum_gold'] < 100) $range1 += 1;
						elseif ($v['sum_gold'] >= 100 && $v['sum_gold']  < 200) $range2 += 1;
						elseif ($v['sum_gold'] >= 200 && $v['sum_gold']  < 500) $range3 += 1;
						elseif ($v['sum_gold'] >= 500 && $v['sum_gold']  < 1000) $range4 += 1;
						elseif ($v['sum_gold'] >= 1000 && $v['sum_gold']  < 2000) $range5 += 1;
						elseif ($v['sum_gold'] >= 2000 && $v['sum_gold']  < 5000) $range6 += 1;
						elseif ($v['sum_gold'] >= 5000 && $v['sum_gold']  < 10000) $range7 += 1;
						elseif ($v['sum_gold'] >= 10000 && $v['sum_gold']  < 20000) $range8 += 1;
						elseif ($v['sum_gold'] >= 20000 ) $range9 += 1;
					}
				}
				$sqlData[$key]['range1'] = $range1;
				$sqlData[$key]['range2'] = $range2;
				$sqlData[$key]['range3'] = $range3;
				$sqlData[$key]['range4'] = $range4;
				$sqlData[$key]['range5'] = $range5;
				$sqlData[$key]['range6'] = $range6;
				$sqlData[$key]['range7'] = $range7;
				$sqlData[$key]['range8'] = $range8;
				$sqlData[$key]['range9'] = $range9;
			}
		}
		
		$sql = "DELETE FROM ny_charge_distribute WHERE create_time = '{$time}'";
		$m->query($sql);
		$chargeModel = new Model('charge_distribute');
		if ($chargeModel->multiAdd($sqlData)) {
			$msg = 'success';
		}else {
			$msg = 'error';
		}
        return $msg;
	}
}