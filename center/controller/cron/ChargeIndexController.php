<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/4/30
 * Time: 17:29
 */

class ChargeIndexController extends CronController
{
    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {
            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $end_time = strtotime(date('Ymd', $start_time)) + 86399;

        //执行命令
        $msg = $this->stat($start_time, $end_time);
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($start_time, $end_time)
    {

        $serList = CronBase::getServerList();

        $Index = new ChargeIndexModel();

        $data = [];
        foreach ($serList as $key => $value) {

            //把原本的删了
            CronBase::delDataByTime('ny_charge_index', $start_time);

            $res = $Index->call('ChargeIndex', 'getDataByRangtime', array('start_time' => $start_time, 'end_time' => $end_time), $value['server_id']);

            $res = $res[$value['server_id']];

            if (!empty($res)) {

                foreach ($res as $v) {
                    $dat = $v;
                    $dat['server'] = $value['server_id'];
                    $dat['time'] = $start_time;

                    array_push($data, $dat);
                }
            }
        }

        if (!empty($data)) {
            if ($Index->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty ~';
        }

        return $msg;
    }
}