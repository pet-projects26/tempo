<?php

/**
 * 充值等级
 * 新增充值人数：当天新增玩家并在当天进行充值的角色数
 * 充值人数：当天充值的人数
 * 新增充值金额：新增充值人数当天充值的金额
 * 登录ARPU: 充值总额 / 登录人数
 * 新增充值APRU：  新增充值总额 / 充值人数
 * arppu＝充值总额／充值人数
 * 注册ARPU： 充值总额 / 注册人数
 *
 * @author  lai <[<email address>]>
 * @date(format)
 *
 */
class ChargeLvController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        $serList = CronBase::getServerList();

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据，
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;

        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //单线程执行
            $msg = CronBase::runmultiprocess('ChargeLv', 'stat', $serList, $start, $end);
            // }
        }

        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {
        //玩家最大等级
        $roleLvSql = "SELECT MAX(role_level) AS lv FROM ny_login  ";

        //充值最大等级
        $chargeLvSql = "SELECT MAX(role_level) AS lv FROM ny_order ";

        //充值金额与充值人数，充值次数
        $chargeSql = "SELECT 
					  SUM(money) AS money,
					  COUNT(DISTINCT ord.role_id) AS charge_num,
					  COUNT(ord.`id`) AS charge_times,
            ord.role_level  AS lv,
					  package 
					FROM
					  ny_order AS `ord` 
					  LEFT JOIN ny_role AS role 
					    ON ord.`role_id` = role.role_id 
					WHERE ord.`create_time` BETWEEN {$start_time} 
					  AND {$end_time} 
					GROUP BY package, lv ";

        $yesterday_start = $start_time - 86400;
        $yesterday_end = $yesterday_start + 86399;

        $yesterdaySql = "SELECT 
            SUM(money) AS money,
            COUNT(DISTINCT ord.role_id) AS charge_num,
            COUNT(ord.`id`) AS charge_times,
            ord.role_level  AS lv,
            package 
          FROM
            ny_order AS `ord` 
            LEFT JOIN ny_role AS role 
              ON ord.`role_id` = role.role_id 
          WHERE ord.`create_time` BETWEEN {$yesterday_start} 
            AND {$yesterday_end} 
          GROUP BY package, lv ";

        //登录人数
        $loginSql = "SELECT COUNT(DISTINCT login.role_id) AS login_num, package, login.role_level AS lv  FROM ny_role AS role
						LEFT JOIN ny_login AS login ON role.role_id = login.role_id
					WHERE login.start_time BETWEEN {$start_time} AND  {$end_time} GROUP BY package, lv  ";

        $data = array(
            'roleLv' => $roleLvSql,
            'chargeLv' => $chargeLvSql,
            'charge' => $chargeSql,
            'login' => $loginSql,
            'yesterday' => $yesterdaySql
        );

        $arr = array('query' => array('data' => $data));

        $servers = array_keys($serList);

        $servers = array('248');

        $res = CronBase::makeHttpParams($servers, $arr);


        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);


        $m = new Model('charge_lv');

        $m->setNewPrefix('stat_');

        $m->setAlias('stat_charge_lv');


        //并发请求，获取返回结果
        foreach ($result as $server_id => $json) {
            $res = json_decode($json, true);

            if (!is_array($res)) {
                echo PHP_EOL . $server_id . ' == ' . PHP_EOL;

                print_r($json);

                echo PHP_EOL;

                continue;
            }

            $code = $res['code'];
            if ($code != 1) {
                continue;
            }

            $data = $res['data']['query'];

            //角色等级
            $roleLv = $data['roleLv'];

            //充值等级
            $chargeLv = $data['chargeLv'];

            //昨天充值
            $yesterday = $data['yesterday'];

            $maxLv = max($roleLv[0]['lv'], $chargeLv[0]['lv']);

            $maxLv = 50;

            $step = ceil($maxLv / 10);

            $tmp = array();
            for ($i = 1; $i <= $step; $i++) {
                $tmp[$i] = array('login_num' => 0, 'charge_num' => 0, 'charge_times' => 0, 'money' => 0, 'step' => $i);
            }


            //充值金额和充值人数，充值次数
            $charge = $data['charge'];

            //登录人数
            $login = $data['login'];

            $insert = array();
            foreach ($login as $row) {
                $lv = $row['lv'];
                $step = self::findStep($lv);
                $package = $row['package'];

                $login_num = $row['login_num'];

                $total = isset($insert[$step][$package]['login_num']) ? $insert[$step][$package]['login_num'] : 0;

                $insert[$step][$package]['login_num'] = $total + $login_num;
            }


            foreach ($charge as $row) {
                $lv = $row['lv'];
                $package = $row['package'];

                $step = self::findStep($lv);

                $money = isset($insert[$step][$package]['money']) ? $insert[$step][$package]['money'] : 0;
                $charge_times = isset($insert[$step][$package]['charge_times']) ? $insert[$step][$package]['charge_times'] : 0;
                $charge_num = isset($insert[$step][$package]['charge_num']) ? $insert[$step][$package]['charge_num'] : 0;


                $insert[$step][$package]['money'] = $money + $row['money'];
                $insert[$step][$package]['charge_times'] = $charge_times + $row['charge_times'];
                $insert[$step][$package]['charge_num'] = $charge_num + $row['charge_num'];
            }

            foreach ($yesterday as $row) {
                $lv = $row['lv'];
                $package = $row['package'];

                $step = self::findStep($lv);

                $yesterday_money = isset($insert[$step][$package]['yesterday_money']) ? $insert[$step][$package]['yesterday_money'] : 0;
                $insert[$step][$package]['yesterday_money'] = $row['yesterday_money'] + $yesterday_money;
            }

            CronBase::delDataByServer('stat_charge_lv', $server_id, $start_time);
            foreach ($insert as $step => $row) {
                foreach ($row as $package => $rr) {
                    $rr['step'] = $step;
                    $rr['package'] = $package;
                    $rr['server'] = $server_id;
                    $rr['time'] = $start_time;

                    $m->add($rr);
                }
                # code...
            }

            $msg = PHP_EOL . '服：' . $server_id . ' ' . date('y-m-d', $start_time) . ' success' . PHP_EOL;

            echo $msg;
            //end foreach
        }

        //end function
    }

    /**
     * [查找玩家等级] 从 0 到 9为一个区间，以10个单位为一个区间
     * @param  [int] $lv [等级]
     * @return [int]     [区间]
     */
    private static function findStep($lv)
    {
        $lv = $lv + 1;
        $step = ceil($lv / 10);
        return $step;
    }
}