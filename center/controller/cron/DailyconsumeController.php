<?php

/**
 * 每日消费统计
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class DailyconsumeController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Dailyconsume', 'stat', $serList, $start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Consume = new DailyconsumeModel();
        $ActiveAccount = new ActiveAccountModel();

        $data = [];

        $serIdList = array_keys($serList);
        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_daily_consume', $serIdList, $time);

        foreach ($serList as $key => $value) {

            //获取今天的活跃人数
            //获取活跃玩家数与活跃玩家
            $active_data = $ActiveAccount->getActiveAccount($start_time, $end_time, $value['server_id']);

            $dat = [];

            //去单服获取今天的统计数据
            $resData = $Consume->call('Payment','getDailyConsume', ['date' => $date], $value['server_id']);

            $rs = $resData[$value['server_id']];

            if (empty($rs)) {
                $dat['consume_gold'] = 0;
                $dat['consume_num'] = 0;
                $dat['produce_gold'] = 0;
                $dat['charge_produce_gold'] = 0;
                $dat['free_produce_gold'] = 0;
                $dat['gold_total_stock'] = 0;
            } else {
                $dat = $rs;
            }

            $dat['active_num'] = !empty($active_data) ? count($active_data) : 0;

            $dat['server'] = $value['server_id'];

            $dat['create_time'] = $time;

            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($Consume->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}