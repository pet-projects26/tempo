<?php

/**
 * 每日充值统计
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class DailyrechargeController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Dailyrecharge', 'stat', $serList, $start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Recharge = new DailyrechargeModel();
        $Node = new NodeModel();
        $ActiveAccount = new ActiveAccountModel();

        $data = [];

        $serIdList = array_keys($serList);
        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_daily_recharge', $serIdList, $time);

        foreach ($serList as $key => $value) {

            $dat = [];
            //获取当天活跃用户
            $active_data = $ActiveAccount->getActiveAccount($start_time, $end_time, $value['server_id']);

            $dat['active_num'] = !empty($active_data) ? count($active_data) : 0;
            $dat['recharge_num'] = 0;
            $dat['recharge_money'] = 0;
            $dat['new_active_num'] = 0;
            $dat['new_recharge_num'] = 0;
            $dat['new_recharge_money'] = 0;
            $dat['old_user_login_num'] = 0;
            $dat['old_user_recharge_num'] = 0;
            $dat['old_user_recharge_money'] = 0;
            $dat['first_recharge'] = 0;
            $dat['server'] = $value['server_id'];
            $dat['create_time'] = $time;

            if ($dat['active_num'] > 0) {
                //获取当天充值总金额和充值总人数
                $rechargeData = $Recharge->call('Charge', 'getChargeDataByDate', ['date' => $date, 'accounts' => []], $value['server_id']);
                $rechargeData = $rechargeData[$value['server_id']];
                $dat['recharge_num'] = $rechargeData['num'];
                $dat['recharge_money'] = $rechargeData['money'];

                //获取当天新增跳转用户
                $new_account = $Node->getNewAccount($start_time, $end_time, $value['server_id']);
                $dat['new_active_num'] = count($new_account);

                if ($dat['new_active_num'] > 0) {
                    $newData = $Recharge->call('Charge', 'getChargeDataByDate', ['date' => $date, 'accounts' => $new_account], $value['server_id']);
                    $newData = $newData[$value['server_id']];
                    $dat['new_recharge_num'] = $newData['num'];
                    $dat['new_recharge_money'] = $newData['money'];
                }

                //老玩家定义修改为 非本日新增创角玩家
                $old_acc_data = $Node->call('Role', 'getOldAccounts_json', array('time' => $start_time), $value['server_id']);
                $old_acc_data = $old_acc_data[$value['server_id']];

                if ($old_acc_data) {
                    //查询node表看看该批老玩家有没有连接游戏服务器
                    $old_acc_data = $ActiveAccount->getActiveAccount($start_time, $end_time, $value['server_id'], '', $old_acc_data);
                }

                if ($old_acc_data) {
                    //过滤未上线老玩家
                    $old_acc_data = $Node->call('Login', 'getLoginAccountByDate', array('date' => $date, 'accounts' => $old_acc_data, 'old' => 0, 'output' => 1), $value['server_id']);
                    $old_acc_data = $old_acc_data[$value['server_id']];

                    $dat['old_user_login_num'] = count($old_acc_data);
                }

                if ($dat['old_user_login_num'] > 0) {
                    $oldData = $Recharge->call('Charge', 'getChargeDataByDate', ['date' => $date, 'accounts' => $old_acc_data], $value['server_id']);
                    $oldData = $oldData[$value['server_id']];
                    $dat['old_user_recharge_num'] = $oldData['num'];
                    $dat['old_user_recharge_money'] = $oldData['money'];
                }

                //获取首充人数
                $firstData = $Recharge->call('Charge', 'getFirstChargeNum', ['date' => $date], $value['server_id']);

                $dat['first_recharge'] = $firstData[$value['server_id']] ? $firstData[$value['server_id']] : 0;
            }

            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($Recharge->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}