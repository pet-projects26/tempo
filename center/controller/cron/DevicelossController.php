<?php
class DevicelossController extends CronController{
    private $dl;
    private $cl;
	private $p;
    public function __construct(){
        $this->dl = new DevicelossModel();
        //$this->cl = new ClientModel();
		$this->m = new MacModel();
		$this->p= new PackageModel();
    }
    public function run(array $argv = null){
		$result=$this->p->getPackages();//获取所有的渠道 包详情
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
		foreach($result as $k=>$v){
			$channel=$v['channel_id'];
			$package=$v['package_id'];
			$deviceloss = $this->dl->getDevicelossByDate($date,$channel,$package);
			$data = array('date' => $time,'channel'=>$channel,'package'=>$package);
			if(!$deviceloss){
				$device = $this->m->getNewDeviceByDate($date,$channel,$package); //新增设备数
				$data['new_num'] = count($device);
				$this->dl->setDeviceloss($data);
			}
		}
		$this->updateDeviceloss();
    }

    public function updateDeviceloss(){
		$result=$this->p->getPackages();//获取所有的渠道 包详情
        $time = strtotime(date('Y-m-d')) - 86400;        //获取昨天日期的时间戳
        $days = array('two' => 1 , 'three' => 2 , 'seven' => 6 , 'fifteen' => 14 , 'thirty' => 29); //第N天,two就是第2天
		foreach($result as $key=>$val){
			$channel=$val['channel_id'];
			$package=$val['package_id'];
			foreach($days as $k => $v){
				$old_date = date('Y-m-d' , $time - $v * 86400);  //获取昨天的前N天的日期
				$deviceloss = $this->dl->getDevicelossByDate($old_date,$channel,$package);
				if($deviceloss && $deviceloss['new_num']){
					$device = $this->m->getNewDeviceByDate($old_date,$channel,$package);
					$old_loss = $this->m->getDevicelossNumByDate(date('Y-m-d' , $time) , $device,$channel,$package); //获取第N天设备流失
					$data = array($k =>  $old_loss);
				}
				else{
					$data = array($k => '0');
				}
				$data['channel']=$channel;
				$data['package']=$package;
				$this->dl->setDeviceloss($data , $old_date);
			}	
		}
        
    }
}