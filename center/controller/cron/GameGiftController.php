<?php

/**
 * 定时去同步竞换码
 */
class GameGiftController extends CronController
{

    private $db;

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        $this->db = new Model();

        //查找需要同步的礼包码
        $sql = "SELECT * FROM  ny_gift WHERE agent = '' OR  server = '[]'";

        $result = $this->db->query($sql);

        print_r($result);

    }

    /**
     * [同步兑换码] 说明，全渠道
     * @version 2017-09-29
     * @param array $serverMsg
     */
    public static function syncCode($serverMsg)
    {
        if (empty($serverMsg)) {
            return true;
        }
        $agent = array_keys($serverMsg);

        $sql = "select `id`,`agent`,`server`,`type`,`open_time`,`end_time`,`item` from ny_gift where (`agent` IN ('".join('\',\'',$agent)."') or `agent`='') && `server` = '[]'";
        //得到新开服数据
        $model = new Model();
        $res = $model->query($sql);
        empty($res) && $res = array();
        if(empty($res)) exit;
        $data = [];
        //得到渠道下所有全服的兑换包
        foreach ($res as $giftInfo) {
            $key = $giftInfo['agent'] ? $giftInfo['agent'] : 'all';
            $data[$key][] = array(
                'server' => $giftInfo['server'],
                'id' => $giftInfo['id'],
                'type' => $giftInfo['type'],
                'open_time' => $giftInfo['open_time'],
                'end_time' => $giftInfo['end_time'],
                'item' => $giftInfo['item']
            );
        }
        //构造兑换码数据
        $send = [];
        foreach ($data as $g_id => $val) {
            //获取每一组
            $exchange = array();
            $key = 0;//自增标识键
            foreach($val as $mcs){
                $fileDir = ROOT . '/export/gift/' . $mcs['id'] . '.txt';
                $codeField = trim(@file_get_contents($fileDir));
                $tmp = json_decode($codeField, true);
                if(empty($tmp)) continue;
                $code = array();
                foreach ($tmp as $row) {
                    foreach ($row as $r) {
                        $code[] = $r;
                    }
                }
                foreach ($code as $k => $code_number) {
                    $exchange[$key] = array(
                        'code' => $code_number,
                        'itemList' => $mcs['item'],
                        'activeId' => (int)$mcs['id'],
                        'startTm' => (int)$mcs['open_time'],
                        'endTm' => (int)$mcs['end_time'],
                        'type' => (int)$mcs['type'],
                        'flag' => 0
                    );
                    $key++;
                }
            }
            $send[$g_id] = $exchange;//如果为全渠道情况下$gid = 'all'
        }
        //发送数据
        $backMsg = [];
        foreach ($serverMsg as $group_id => $servers) {
            if(array_key_exists('all',$send)){
                $serverid = (new ServerModel())->getServerByServerAndChannelGroup(array(),array());
                self::roll_send('all',array_keys($serverid),$send);
                unset($send['all']);
                continue;
            }
            $ret = self::roll_send($group_id,array_values($servers),$send);
            array_push($backMsg,json_encode($ret));
        }
        Helper::log($backMsg,'syncGiftCode');//记录每次同步日志
    }


    /**
     * 批量发货接口
     * @param string $group_id
     * @param array $server_id
     * @param array $send_data
     * @return array $rs
    */
    private static function roll_send($group_id,$server_id,$send_data){

        $instance = new ServerconfigModel();
        $urls = $instance->getApiUrl($server_id, true);
        $vars = array('document' => $send_data[$group_id]);//二维
        $postData = array(
            'r' => 'call_method',
            'class' => 'GiftModel',
            'method' => 'addGift',
            'params' => json_encode($vars)
        );
        //并发执行代码同步
        $rs = Helper::rolling_curl($urls, $postData);
        return $rs;
    }

}