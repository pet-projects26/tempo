<?php
/**
 * 赫德删档操作补偿
 */
class GameOrderController extends CronController
{

    private $db;

    /**
     * [初始化]
     */
    public function __construct()
    {
        $this->db = new Model();
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){

        $server = array('lianyun0001','lianyun0002');
        
        $hedeyinghesql = "select * from ny_order_return where type = 0 and server = 'lianyun0001'";//赫德硬核专服
        $hedesidasql = "select * from ny_order_return where type = 0 and server = 'lianyun0002'";//赫德四大专服

        $lianyun0001 = $this->db->query($hedeyinghesql); 
        $lianyun0002 = $this->db->query($hedesidasql);

        $hedeyingheaccount = array_column($lianyun0001, 'account');
        $hedesidaaccount = array_column($lianyun0002, 'account');

        //根据帐号在单服查询roleid
        $hedeyingheaccount = "'".implode("','" , $hedeyingheaccount)."'";
        $hedesidaaccount = "'".implode("','" , $hedesidaaccount)."'";

        $hedeyingheaccountsql = " select account,role_id ,name , min(create_time) from ny_role where account in(
                                $hedeyingheaccount) GROUP BY account ";
       
        $hedesidaaccountsql = " select account,role_id ,name,min(create_time) from ny_role where account in(
                            $hedesidaaccount) GROUP BY account";

        $data = array(
            'hedeyinghesingle' => $hedeyingheaccountsql,
            'hedesidasingle' => $hedesidaaccountsql,
        );

        $arr  = array('query' => array('data' => $data));
        $res  = (new ServerconfigModel())->makeHttpParams($server, $arr);
            
        $urls = $res['urls'];
        $param = $res['params'];
        $res = Helper::rolling_curl($urls, $param);

        $res = self::getDatas($res);
        
        $cc = array();

        foreach($res as $kk=> $val){
          
            foreach ($val as $key => $value) {    
                foreach ($value as $k => $v) {
        
                   $cc[$v['account']]['role_id'] = $v['role_id'];
                   $cc[$v['account']]['name'] = $v['name'];
                   $cc[$v['account']]['server'] = $kk;

                }
            }
        }
        
        $result = array();
        foreach($server as $k=>$v){
           
          foreach($$v as $kk => $vv){
           if(array_key_exists($vv['account'] , $cc)  && $v == $cc[$vv['account']]['server']){
                $result[$v][$vv['account']]['role_id'] = $cc[$vv['account']]['role_id'];
                $result[$v][$vv['account']]['gold'] = $vv['money']*20;
                $result[$v][$vv['account']]['name'] = $cc[$vv['account']]['name'];
            }
          }
        }

       
        if(empty($result)){
            die();
        }

        $str = "亲爱的玩家：
          您好，非常感谢您对我们游戏的支持和喜爱，现为您发放测试期间的充值福利返还，请您查收。祝您游戏愉快!";

        foreach ($server as  $value) {

            $data = array( 'sender' => 'GM', 'title' => '充值福利' , 'content' => $str , 'attach' => $result[$value]);

            $url = Helper::v2();
            $res = Helper::rpc($url , array($value => 1),'return_smail' , $data);
           
            if($res[$value] == 1){

                //拿到发送了元宝的账号，更新表的type，输出成功
                $account = array_keys($result[$value]);
                $account = "'".implode("','", $account)."'";
                $sql = "update  ny_order_return set type = 1  where account in ($account)  AND server ='{$value}' ";
                $res = $this->db->query($sql);
              
                unset($data);
                //添加邮件
                foreach($result[$value] as $k => $v){

                    $email = array(
                        'servers' => $value,
                        'admin' => 'GM',
                        'role_id' => $v['role_id'],
                        'role_name' => $v['name'],
                        'sender' => 'GM',
                        'title' => '充值福利',
                        'content' =>  '亲爱的玩家：
          您好，非常感谢您对我们游戏的支持和喜爱，现为您发放测试期间的充值福利返还，请您查收。祝您游戏愉快!',
                        'attach' => json_encode(array('gold' => $v['gold'])),
                        'create_time' => time(),
                        'is_send' => 1,
                        'status' =>1,
                        'msg' =>$value.':发送成功',
                    );

                    $data[] = $email;
                    unset($email);
                }
        
                if ($data) {
                	(new MailModel())->multiAdd($data);
                }

                if($res !== false){
                    echo '成功';
                }else{
                    echo '失败'; 
                }
            }
        }

    }

    /**
     * 格式化返回数据
     * @param array $arr
     * @return array $tmp
    */
    public static function getDatas($arr) {
        $tmp = array();
        foreach ($arr as $server_id => $json) {
            $res = json_decode($json, true);

            if (!is_array($res)) {
                echo PHP_EOL . $server_id . ' == ' .PHP_EOL;
                print_r($json);
                echo PHP_EOL;
                continue;
            }
            $code = $res['code'];

            if ($code != 1) {
                continue;
            }
            $data = $res['data']['query'];
            $tmp[$server_id] = $data;
        }
        return $tmp;
    }

}