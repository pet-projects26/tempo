<?php

class GameServerAutoOpenPlanController extends CronController{
    private $db;

    /**
     * [初始化]
     */
    public function __construct() {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        Helper::log('run', 'gameServerAutoOpenPlan', 'run', 'server');

        $time = strtotime(date('y-m-d H:i:00'));

        //防止定时任务过多，前几分钟的服未开的重开
        $start_time = $time- 5 * 60;
        $end_time = time() + 30;

        //查询所有自动开服计划
        $ServerAutoPlanModel = new ServerAutoPlanModel();

        $res = $ServerAutoPlanModel->getRows(['group_id', 'name', 'status', 'type', 'create_role_num', 'pay_num'], ['status' => 1]);

        if (empty($res)) {
            Helper::log('no plan', 'gameServerAutoOpenPlan', 'run', 'server');
            die;
        }

        $ServerRedis = new ServerRedis();

        $data = [];

        foreach ($res as $row) {

            //查询该渠道下的当前导量入口入口服, 即最新服
            $server_id = $ServerRedis->getNewServerId($row['group_id'],0);

            if ($server_id == '') {
                Helper::log($row['name'].' no new server', 'gameServerAutoOpenPlan', 'run', 'server');
                continue;
            }

            $isOpen = false;
            $where = '';

            //判断开服计划类型
            if ($row['type'] == 1 || $row['type'] == 2) {
                //查询该服的创角数和付费数
                $info = $ServerAutoPlanModel->call('Instantdata', 'getServerRealData', array('output' => 1), $server_id);
                $info = $info[$server_id];

                Helper::log($info, 'gameServerAutoOpenPlan', $row['name'].'info', 'server');
                if(empty($info)) {
                    Helper::log($row['name'].' no info', 'gameServerAutoOpenPlan', 'run', 'server');
                    continue;
                }

                $create_role_num = $info['role_num'] ? $info['role_num'] : 0;
                $pay_num = $info['pay_num'] ? $info['pay_num'] : 0;

                if ($row['type'] == 1) {
                    //或
                    if ($create_role_num >= $row['create_role_num'] || $pay_num >= $row['pay_num']) {
                        $isOpen = true;
                    }
                } else if ($row['type'] == 2) {
                    //且
                    if ($create_role_num >= $row['create_role_num'] && $pay_num >= $row['pay_num']) {
                        $isOpen = true;
                    }
                }
            } else if ($row['type'] == 3) {
                $isOpen = true;
                $where = " AND open_time BETWEEN {$start_time} AND {$end_time} ";
            }


            if ($isOpen) {
                //查询下个服
                $sql = "SELECT `group_id`, `server_id`, `open_time` FROM ny_server WHERE status  = 0 AND type = 1 AND group_id = '".$row['group_id']."' ".$where." order by num asc";
                $rs = $ServerAutoPlanModel->fetchOne($sql);

                if (!$rs) {
                    if ($row['type'] == 1 || $row['type'] == 2) {
                        require_once ROOT . '/../api/core/BaseController.php';
                        require_once ROOT . '/../api/controller/_serverEmailController.php';
                        $str = "九天互娱后台自动开服:  渠道组:".$row['group_id']."中的服务器id为".$server_id."服到达开启下一个服的条件; 后台没有配置服务器; 请通知相关运维人员配置";
                        //发邮件通知管理员
                        //(new _serverEmailController())->run($str, 0);
                    }
                    Helper::log($row['name'].' no server', 'gameServerAutoOpenPlan', 'run', 'server');
                    continue;
                }

                //如果有 则开服
                $group_id = $row['group_id'];
                $data[$row['group_id']][] = $rs['server_id'];

                $sql = "UPDATE ny_server SET status = 1 WHERE status  = 2 AND display = 1 AND group_id = '{$group_id}' and server_id = '{$server_id}'";
                $ServerAutoPlanModel->query($sql);

                //将服务器设置为新服
                $sql = "UPDATE ny_server SET status = 2, display = 1, open_time = '".$time."' WHERE server_id = '{$rs['server_id']}'";
                $ServerAutoPlanModel->query($sql);
                Helper::log($sql, 'gameServerAutoOpenPlan', $row['name'].' sql', 'server');
            }
        }

        if (empty($data)) {
            NoticeModel::syncNotice($data); //同步公告

            GiftModel::syncCode($data);//同步兑换码操作，需要考虑到服务器承载量

            //执行成功后清楚redis服务器缓存
            (new ServerRedis(true))->delServerKey();
        }

        die('success');
    }
}