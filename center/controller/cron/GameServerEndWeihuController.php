<?php

/**
 * 维护关服功能
 * 开服条件：
 * 1、根据维护表里面的状态为1的
 * 2、维护开始时间到了
 */
class GameServerEndWeihuController extends CronController
{
    private $db;

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        Helper::log('run', 'GameServerEndWeihu', 'run', 'server');
        //防止定时任务过多，前几分钟的服未执行的重新执行
        $start_time = strtotime(date('y-m-d H:i:00')) - 5 * 60;
        $end_time = time() + 30;

        $sql = "SELECT id , group_num , server_id FROM ny_server_maintenance WHERE  type = 2 AND  endtime BETWEEN {$start_time} AND {$end_time}";

        $this->db = new Model();

        $res = $this->db->query($sql);

        if (!empty($res)) {
            foreach ($res as $k => $v) {
                $id = $v['id'];
                if (!empty($v['server_id'])) {
                    $group = $v['group_num'];
                    $server_id = implode("','", explode(',', $v['server_id']));

                    $sql = "update ny_server set status = 1 where server_id  in ('$server_id')";

                } else if (empty($v['server_id']) && !empty($v['group_num'])) {

                    $group = $v['group_num'];

                    $sql = "update ny_server set status = 1 where group_id  in ($group) and status = '-1'  and display = 1 ";

                } else {

                    $sql = "update ny_server set status = 1  where status = '-1'  and display = 1";

                }
                $this->db->query($sql);

                $group = explode(',', $group);

                //根据groupid去更新每个渠道组中创建时间最大的服为新服，即status = 2
                if (!empty($group)) {
                    foreach ($group as $g) {
                        $updatesql = "SELECT server_id from ny_server where  status != 0  and display = 1 and group_id = '{$g}' ORDER BY open_time desc limit 1 ";

                        $update = $this->db->query($updatesql);
                        $server = $update[0]['server_id'];
                        if ($server) {
                            $sql = "update ny_server set status = 2 where server_id  in ('$server')";

                            $this->db->query($sql);
                        }
                    }
                }

            }
            //$id = $v['id'];
            $sql = "update ny_server_maintenance set type= 3 where id = $id";
            $this->db->query($sql);

            //执行成功后清楚redis服务器缓存
            (new ServerRedis(true))->delServerKey();
        }
        die('执行完毕');
    }

}