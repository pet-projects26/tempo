<?php
/**
 * 合服
 */
class GameServerMergeController extends CronController{
    private $db;
    
    /**
     * [初始化]
     */
    public function __construct() {
      $this->db = new Model();
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null) {
     
      $mail = $this->sendMail();

      $exchange = $this->offSale();

      $notice  = $this->notice();

      $open = $this->openServer();

      $openTrade = $this->openTrade();
    }

    /**
     * 发送邮件 24小时前发送邮件
     * ffc828 黄
     * ff1400 红
     * ff00f5 紫
     * @return [type] [description]
     */
    public function sendMail() {

      $time24 = time() + 86400;

      $sql = "SELECT * FROM ny_merge WHERE status = 0 AND notify = 0 AND merge_time  < {$time24} ";

      //echo $sql;

      $res = $this->db->query($sql);

      if (empty($res)) {
        return true;
      }

      //检测合服列表
      foreach ($res as $k => $row) {
        $parent   = $row['parent'];
        $children = $row['children'];

        $servers = $parent . ',' . $children;

        $servers = explode(',', $servers);

        $rs = (new ServerModel())->getServer($servers);

        $str = array();

        foreach ($rs as $k2 => $r2) {
          $str[] = $r2['name'];
        }

        $str = implode(',', $str);

        $merge_time = date('Y年m月d日H时i分', $row['merge_time']);
        $start_time  = date('Y年m月d日H时i分', $row['start_time']);


        $str = "亲爱的玩家：
        为给大家营造更好的游戏氛围，[41ff00]{$str}[-]将于[ffc828]{$merge_time} - {$start_time}[-]进行合服。合服完毕后，大家可以通过原入口登录，重名玩家将免费获得改名卡。合服后会开启限时精彩活动，期待您的参与！
      为避免造成经济损失，合服12小时前会关闭交易所。您上架的物品会通过邮件返回，请您注意查收。";


        $data = array('role_id' => 0 , 'sender' => 'GM', 'title' => '合服邮件' , 'content' => $str , 'attach' => array());
        $url = Helper::v2();

        print_r($url);

        $res = Helper::rpc($url , array_flip($servers) ,'smail' , $data);

        var_dump($res);

        $sql = "UPDATE ny_merge set notify = 1  where id = {$row['id']} ";

        $this->db->query($sql);

      }

    }

    //交易所下架
    public function offSale() {
      $time  = time() + 43200;

      $sql = "SELECT * FROM ny_merge WHERE status = 0 AND notify = 1 and merge_time < {$time} ";

      $res = $this->db->query($sql);

      if (empty($res)) {
          return true;
      }

       //检测合服列表
      foreach ($res as $k => $row) {
        $parent   = $row['parent'];
        $children = $row['children'];

        $servers = $parent . ',' . $children;

        $servers = explode(',', $servers);

        $rs = (new ServerModel())->getServer($servers, '*');

        $params = array();

        $servers = array();
        foreach ($rs as $k2 => $r2) {
          $psId          = $r2['channel_num'];
          $sid           = $r2['num'];
          $sign          = $r2['server_id']; 
          $params[$sign] = array( 0 => array('pId' => $psId, 'sId' => $sid));
          $servers[$sign] = $r2;

        }

        
        //todo 发送交易组
        $url = Helper::v2();
        $res = Helper::rpc($url , $servers,'closeTrade' , $params);

        if (array_unique($rs) && count($rs) == 1 && $rs[0] == 1 || true) {
          $sql = "UPDATE ny_merge set notify = 2 where id = '{$row['id']}'";
          $this->db->query($sql);
        }

      }
      //end foreach 

    }



    //公告的推送，当到达合服时间，进行推送，并把相关服改为测试状态
    public function notice() {
      $time = time() - 30;

      $sql = "SELECT * FROM ny_merge WHERE status = 0 AND notify = 2 AND merge_time < {$time}";
      
      $res = $this->db->query($sql);

      if (empty($res)) {
        return true;
      }

      $title = "合服公告";

      //检测合服列表
      foreach ($res as $k => $row) {
        $parent   = $row['parent'];
        $children = $row['children'];

        $allServers = $parent . ',' . $children;

        $servers = explode(',', $allServers);

        $rs = (new ServerModel())->getServer($servers);

        $str = array();

        foreach ($rs as $k2 => $r2) {
          $str[] = $r2['name'];
        }

        $str = implode(',', $str);

        $start_time  = date('Y年m月d日H时i分', $row['start_time']);

        $content = "亲爱的玩家：
        [41ff00]{$str}[-]于 [ffc828]{$start_time}[-] 合服完毕。现在连续登录[ff1400]7天[-]即可获得[ff00f5]限量版坐骑时装[-]及[ffc828]超炫称号[-]，充值更能获得[ff00f5]精美时装[-]及[ffc828]极品红装[-]。更多惊喜跨服活动等你参与！现在就上线体验吧！




                                                                 官方运营团队";

        $groupserver = implode("','" , $servers);
        $sql = "SELECT DISTINCT(group_id) from ny_server where server_id in ('$groupserver') ;";
        $group = (new ServerModel())->query($sql);
        $group = array_column($group, 'group_id');
        $groups = implode(',', $group);
       
        $data = array(
          'groups' => $groups,
          'servers' => $allServers,
          'title' => $title,
          'contents'=> $content,
          'start_time' => time(),
          'end_time' => time() + 86400 * 7,
          'create_time' => time(),
          'status' => 1,
          'admin' => '后台自动',
          'type' => '3',
        );  

        //添加公告(如果需要合服公告，就把下面这行开启)
        //(new NoticeModel())->add($data);

        //@todo 关闭服务器
        
        $allServers  = explode(',', $allServers);

        foreach ($allServers as $k => &$v) {
          $v = "'{$v}'";
        }

        unset($v);

        $allServers = implode(',', $allServers);

        //合服的服务器中出现有母服的情况
        $childrensql = "select server_id from ny_server where mom_server in ({$allServers})";

        $children =  $this->db->query($childrensql);
        
        if($children){

          foreach ($children as $k => &$v) {
            $v = "'{$v['server_id']}'";
          }
          
          $children = implode(',', $children);

          $allServers .= ','.$children; 

        }

        $sql = "UPDATE ny_server SET `status` = -1 WHERE server_id IN ({$allServers}) ";

        $this->db->query($sql);

        $sql = "UPDATE ny_merge set notify = 3  where id = {$row['id']} ";

        $this->db->query($sql);

      }

      //end foreach 

      return 'success';
    }

    /**
     * 自动开服
     * @return [type] [description]
     */
    public function openServer() {

      $time = time() - 30;
      $sql = "SELECT * FROM ny_merge WHERE status = 1 AND notify = 3 AND start_time < {$time}";
    
      $res = $this->db->query($sql);

      if (empty($res)) {
          return true;
      }

       //检测合服列表
      foreach ($res as $k => $row) {
        $parent   = $row['parent'];
        $children = $row['children'];

        $servers = $parent . ',' . $children;

        $servers = explode(',', $servers);

        $server = $servers;

        foreach ($servers as &$v) {
          $v = "'{$v}'";
        }
        unset($v);

        $allServers = implode(',', $servers);

        //合服的服务器中出现有母服的情况
        $childrensql = "select server_id from ny_server where mom_server in ({$allServers})";

        $children =  $this->db->query($childrensql);
        
        if($children){

          foreach ($children as $k => &$v) {
            $v = "'{$v['server_id']}'";
          }
          
          $children = implode(',', $children);

          $allServers .= ','.$children; 
        }

        //根据渠道组去更改服务器状态
        
        $sql =  "UPDATE ny_server SET `status` = 2 WHERE server_id IN ($allServers);";
        
        $this->db->query($sql);

        $gsql = "select distinct(group_id) from ny_server WHERE server_id IN ({$allServers}) ";

        $g = $this->db->query($gsql);
        $gid = array_column($g, 'group_id');
        $gid = implode(',', $gid);

        $sql = "UPDATE ny_server SET `status` = 2 WHERE server_id IN ( select * from (select server_id from ny_server where status not in (0,-1) and display = 1 and group_id in ($gid) ) a ) ";
      
        $this->db->query($sql);
        //根据渠道组去更改服务器状态
        /*$gsql = "select distinct(group_id) from ny_server WHERE server_id IN ({$allServers}) ";

        $g = $this->db->query($gsql);
        $gid = array_column($g, 'group_id');
        $gid = implode(',', $gid);*/
        $updatesql = "SELECT server_id from (SELECT server_id,group_id,open_time FROM ny_server  where  status not in (0,-1) and server_id not like '%9998' and display = 1 and group_id in ($gid) ORDER BY open_time desc) b GROUP BY b.group_id ";
         
        $update = $this->db->query($updatesql);
        $server = array_column($update, 'server_id');

        $server = implode("','" , $server);
        $sql  = "update ny_server set status = '1' where server_id  in ('$server')";

        $this->db->query($sql);
        
        $sql = "UPDATE ny_merge set notify = 4  where id = {$row['id']} ";

        $this->db->query($sql);

        $rs = (new ServerModel())->getServer($parent, '*');

        $params = array();

        $servers = array();
        if(!empty($rs)){
          //foreach ($rs as $k2 => $r2) {
            $psId          = $rs['channel_num'];
            $sid           = $rs['num'];
            $sign          = $rs['server_id']; 
            $params[$sign] = array( 0 => array('pId' => $psId, 'sId' => $sid));
            $servers[$sign] = $rs;

          //}
        }
        
        //todo 发送交易组
        $url = Helper::v2();
        $res = Helper::rpc($url , $servers,'openTrade' , $params);

        $res = array_unique(array_values($res));

        if ( count($res) == 1 && $res[0] == 1) {
          $sql = "UPDATE ny_merge set notify = 5 where id = '{$row['id']}'";
          $this->db->query($sql);
        }

      }
      //end foreach 

      return 'success';

    }

    /**
     * [openTrade 开启拍卖行]
     * @return [type] [description]
     */
    public function openTrade(){
       

      $sql = "SELECT id , parent,children FROM ny_merge WHERE  notify = 4 ";

      $result = $this->db->query($sql);

      $server = array();

      $servers = '';
      if(!empty($result)){
        foreach($result as $k=>$v){

          $id[] = $v['id']; 

          $servers .= $v['parent'];
        }

        $id = implode(',', $id);

        $server = array_filter(explode(',', $servers));

        $rs = (new ServerModel())->getServer($server, '*');

        $params = array();

        $servers = array();
        foreach ($rs as $k2 => $r2) {
          $psId          = $r2['channel_num'];
          $sid           = $r2['num'];
          $sign          = $r2['server_id']; 
          $params[$sign] = array( 0 => array('pId' => $psId, 'sId' => $sid));
          $servers[$sign] = $r2;

        }

        //todo 发送交易组
        $url = Helper::v2();
        $res = Helper::rpc($url , $servers,'openTrade' , $params);

        $res = array_unique(array_values($res));

        if ( count($res) == 1 && $res[0] == 1) {
          $sql = "UPDATE ny_merge set notify = 5 where id in  ($id)";
         
          $this->db->query($sql);
        }
      }

      return 'success';

  }

  
}