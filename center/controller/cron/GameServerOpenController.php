<?php
/**
 * 开服自动化功能
 * 开服条件：
 * 1、服的类型是：正常服
 * 2、状态是示开服
 * 3、开服时间已到
 * 4、该服已选好渠道组
 */
class GameServerOpenController extends CronController{
    private $db;
    
    /**
     * [初始化]
     */
    public function __construct() {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        Helper::log('run', 'gameServerOpen', 'run', 'server');
        //防止定时任务过多，前几分钟的服未开的重开
        $start_time = strtotime(date('y-m-d H:i:00')) - 5 * 60;
        $end_time = time() + 30;
        $this->db = new Model();
        //自动显示
        $sql = "SELECT `server_id` FROM ny_server where display = 0 and display_time BETWEEN {$start_time} AND {$end_time}";
        $serverIds = [];
        $ret = $this->db->query($sql);
        empty($ret) && $ret = array();
        foreach($ret as $msg){
            array_push($serverIds,$msg['server_id']);
        }
        if(!empty($serverIds)){
            $serverIds = array_filter($serverIds);//去空
            $upd = "UPDATE ny_server SET display=1 where server_id IN('".join('\',\'',$serverIds)."')";
            $this->db->query($upd);
            Helper::log($upd,'gameServerOpen','update_display');
        }
        /*
        //自动开服
        $sql = "SELECT `group_id`, `server_id` FROM ny_server WHERE status  = 0 AND type = 1 AND group_id != 0  AND open_time BETWEEN {$start_time} AND {$end_time}";
        $res = $this->db->query($sql);
        if (empty($res)) {
            die ('no Server');
        }

        $data = [];
        foreach ($res as $row) {
            //将之前的新服
            $group_id = $row['group_id'];
            $data[$row['group_id']][] = $row['server_id'];

            $sql = "UPDATE ny_server SET status = 1 WHERE status  = 2 AND display = 1 AND group_id = '{$group_id}' and open_time < {$start_time} ";

            $this->db->query($sql);

            //将服务器设置为新服
            $sql = "UPDATE ny_server SET status = 2, display = 1 WHERE server_id = '{$row['server_id']}'";

            $this->db->query($sql);
            Helper::log($sql, 'gameServerOpen', 'sql', 'server');

            //@todo 添加新服公告
            //公告方面因为暂时没有新版，所以先不自动添加
            //$this->addNotice($row['server_id']);
        }

        GiftModel::syncCode($data);//同步兑换码操作，需要考虑到服务器承载量
        */

        //执行成功后清楚redis服务器缓存
        (new ServerRedis(true))->delServerKey();

        die('success');
    }

    /**
     * 添加活动公告
     * @param [array] $server [服务器标识符]
     */
    private function addNotice($server) {
      $start_time = time();
      $end_time  = time() + 86400 * 7;
      $sql = "UPDATE ny_notice SET status = 0, admin = '后台自动' WHERE `servers` = '{$server}' 
        AND type = 0 AND start_time < {$start_time} AND end_time >  {$end_time} AND status = 1  ";

      $this->db = new Model();

      //先修改原有新服公告

      $this->db->query($sql);

      $title = "新服公告";

      $content = "[ff7800]欢迎回来！[-]

[e6e6e6]各位仙友：

      千年盛典尽在今日仙途！即日开启超多惊喜新服活动~
      每日登录均可领取1888大奖！

      每日充值100元宝，更能获得神级羽毛！炫酷时装！[-]
      
      对游戏有疑问或反馈请加客服Q群658664647，客服美眉等你来撩！

[41ff00]更多精彩活动！即刻登录体验！[-] ";

        $this->db = new Model('notice');

        $data = array(
            'groups' => '',
            'servers' => $server,
            'contents' => $content,
            'title' => $title,
            'type' => 0,
            'create_time' => time(),
            'start_time' => $start_time,
            'end_time' => $end_time,
            'admin' => '后台自动',
            'sort' => 99,
            'status' => 1,
        );


        $insertId = $this->db->add($data);

        return $insertId;

    }



	
}