<?php

/**
 * 维护关服功能
 * 开服条件：
 * 1、根据维护表里面的状态为1的
 * 2、维护开始时间到了
 */
class GameServerStartWeihuController extends CronController
{
    private $db;

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        Helper::log('run', 'gameServerStartWeihu', 'run', 'server');
        //防止定时任务过多，前几分钟的服未执行的重新执行
        $start_time = strtotime(date('y-m-d H:i:00')) - 5 * 60;
        $end_time = time() + 30;
        $sql = "SELECT id, group_num , server_id FROM ny_server_maintenance WHERE  type = 1 AND  starttime BETWEEN {$start_time} AND {$end_time}";
        $this->db = new Model();
        $res = $this->db->query($sql);
        if (!empty($res)) {
            foreach ($res as $k => $v) {
                if (!empty($v['server_id'])) {
                    $server_id = implode("','", explode(',', $v['server_id']));
                    $sql = "update ny_server set status = '-1' where server_id  in ('$server_id')";

                } else if (empty($v['server_id']) && !empty($v['group_num'])) {
                    $group = $v['group_num'];
                    //维护的服务器需要剔除掉提审服
                    $sql = "update ny_server set status = '-1' where group_id  in ($group) and status != 0  and display = 1 and server_id not like '%9999' and server_id not like '%9998' ";
                } else {
                    //维护的服务器需要剔除掉提审服
                    $sql = "update ny_server set status = '-1' where  status != 0  and display = 1 and server_id not like '%9999' and server_id not like '%9998' ";
                }
                $this->db->query($sql);
                $id = $v['id'];
                $sql = "update ny_server_maintenance set type= 2 where id = $id";
                $this->db->query($sql);
            }
            //执行成功后清楚redis服务器缓存
            (new ServerRedis(true))->delServerKey();
        }
        die('执行完毕');
    }

}