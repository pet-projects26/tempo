<?php
/**
 * 同步激活码的计划任务
 * every day for am 3:00
 */
class GameSyncGiftController extends CronController{

    private $db;

    /**
     * 构造方法 初始化
    */
    public function __construct(){
        date_default_timezone_set('PRC'); //设置中国时区
    }

    /**
     * 计划任务开始执行的
     * @mark 判断每天新开的服务器
     * @param array $argv
    */
    public function run(array $argv = null){
        //获取当天需要开服的服务器
        $serverModel = new ServerModel();
        $t = time();
        $start_time = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));  //当天开始时间
        $end_time = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t)); //当天结束时间
        $conditions = array();

        $conditions['WHERE']['open_time::BETWEEN'] = array($start_time,$end_time);
        $fields = array('server_id','open_time','group_id');
        //得到当天需要开服的服务器
        $row = $serverModel->getRows($fields,$conditions['WHERE']);
        $serverIds = array();
        foreach($row as $key=>$serInfo){
            !empty($serInfo['server_id']) && array_push($serverIds,$serInfo['server_id']);
        }
        //得到当天需要开服的服务器相关配置
        if(empty($serverIds)){
            //Helper::log('没有服务器需要进行同步',__CLASS__);
            exit();
        }
        $sql = "select server_id from ny_server_config where server_id in('".join('\',\'',$serverIds)."')
                    and api_url != ''
                    and mongo_host != ''
                    and mongo_port != ''
                    and mongo_user != ''
                    and mongo_passwd != ''
                    and mongo_db !='' ";
        $configRow = $serverModel->query($sql);
        empty($configRow) && $configRow = array();
        $configSerIds = array();
        foreach($configRow as $v){
            !empty($v['server_id']) && array_push($configSerIds,$v['server_id']);
        }
        $data = array();
        foreach($row as $value){
            if(empty($value['group_id']))continue;

            if(!empty($value['server_id']) && in_array($value['server_id'],$configSerIds)){
                $data[$value['group_id']][] = $value['server_id'];
            }
        }
        if(empty($data))exit;
        GameGiftController::syncCode($data);
    }
}
?>