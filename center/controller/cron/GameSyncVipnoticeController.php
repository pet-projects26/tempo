<?php
/** 定时同步&&发送rpc协议通知游戏服务端 **/
class GameSyncVipnoticeController extends CronController{

    private $vipNoticeModel;
    public function __construct(){
        date_default_timezone_set('PRC'); //设置中国时区
        set_time_limit(30*60);//设计
        $this->vipNoticeModel = new VipnoticeModel();
    }
    /**
     * 持续监听，每33分钟进行同步一次
     * @param array $arr
     */
    final public function run(array $arr=null){
        //获取当前至30分钟内有开服的服务器
        $serverModel = new ServerModel();
        $now_time = time();
        $min33ago = time()-60*33;//33分钟前
        //$min33ago = '1513909533';
        //$now_time = '1513909899';
        $pushServer = self::getServerId(array($min33ago,$now_time));
        $group_id = array_filter(array_keys($pushServer));
        //获取需要被同步的渠道组下大R公告内容
        $conditions = [];
        $conditions['WHERE']['group_id::IN'] = $group_id;
        $fields = array('id','group_id','type','title','connect','content','level','status');
        $row = $this->vipNoticeModel->getRows($fields,$conditions['WHERE']);
        $data = $backMsg = $backInfo = [];
        foreach($row as $msg){
            if(empty($msg['group_id']))continue;
            $data[$msg['group_id']] = $msg;
        }
        $group_id = !empty($data)?array_keys($data):array();
        //遍历所有存在新服&&有提交大R信息的渠道组
        foreach($group_id as $key=>$gid){
            #code for sync data to mongo with rolling_curl
            if(!isset($pushServer[$gid]) || empty($data[$gid]['level']))continue;//不存在的
            $goldLevel = json_decode($data[$gid]['level'],true);
            $contactArr = !empty($data[$gid]['connect'])?json_decode($data[$gid]['connect'],true):[];
            $emailTitleArr = !empty($data[$gid]['title'])?json_decode($data[$gid]['title'],true,JSON_UNESCAPED_UNICODE):[];
            $emailContentArr = !empty($data[$gid]['content'])?json_decode($data[$gid]['content'],true,JSON_UNESCAPED_UNICODE):[];
            $list = [];//道具信息
            foreach($emailContentArr as &$msg){
                $msg = str_replace('X22#X22#','\n',$msg);//解析数据
            }
            foreach($goldLevel as $k=>$val){
                $content = '会员公告';
                $contact = isset($contactArr[$k])?$contactArr[$k]:'';
                $emailTitle = isset($emailTitleArr[$k])?$emailTitleArr[$k]:'';
                $emailContent = isset($emailContentArr[$k])?$emailContentArr[$k]:'';

                $itemList = [];
                $gold = intval($val);
                $temp = array(
                    'content'=>$content,
                    'contact'=>$contact,
                    'gold'   =>$gold,
                    'emailTitle'=>$emailTitle,
                    'emailSender'=>'GM',
                    'emailContent'=>$emailContent,
                    'itemList'=>$itemList
                );
                $list[] = $temp;
            }#end foreach
            $pushData['_id'] = 1;
            $pushData['updateTm'] = time();
            $pushData['list'] = $list;
            $type = explode(',',$data[$gid]['type']);
            $upType = array_intersect(array('1','2'),$type);
            empty(array_diff(array('1','2'),$type)) && $upType=[3];
            $pushData['type'] = array_shift($upType);
            $servers = $pushServer[$gid];
            $serverMsg = $serverModel->getServerByServerAndChannelGroup($servers,array($gid));

            if(empty($serverMsg)) continue;//未能找到合适的服务器
            $server = array_keys($serverMsg);
            //$server = ['open1'];
            $param = Helper::mongoInt64($pushData);
            $postData = array(
                'r' => 'call_method',
                'method' => 'addNotice',//方法名(新增&&更新)
                'class' => 'VipnoticeModel',//单服类)
                'params' => json_encode(array('document' => $param)),
            );
            //查找服务器的api Url
            $instance  = new ServerconfigModel();
            $urls = $instance->getApiUrl($server, true);
            $rs = Helper::rolling_curl($urls, $postData);

            //收集返回的信息
            foreach($rs as $sid=>$val){
                $backInfo[$sid]['info'] = $val;
                $backInfo[$sid]['type'] = $pushData['type'];
            }
            array_push($backMsg,$rs);
        }#end foreach
        error_log("run: ".json_encode($backMsg)."\n\r",3,'server.log');//写入mongo后，再拾取记录中的数据，对游戏内进行up操作
        #code for send up or down massage to game
        self::AsyncUpNotice($backInfo);//异步更新
    }



    /**
     * 获取时间内服务器数据
     * @param array $times
     * @return array $serverId
    */
    private static function getServerId($times){
        $serverModel = new ServerModel();
        $start_time = $times[0];
        $end_time = $times[1];
        /*print date('Y-m-d H:i:s',$start_time)."\n";
        print date('Y-m-d H:i:s',$end_time);*/
        $conditions['WHERE']['open_time::>='] = $start_time;
        $conditions['WHERE']['open_time::<='] = $end_time;
        $fields = array('server_id','open_time','group_id');
        //得到当天需要开服的服务器
        $row = $serverModel->getRows($fields,$conditions['WHERE']);
        $serverIds = array();
        foreach($row as $key=>$serInfo){
            !empty($serInfo['server_id']) && array_push($serverIds,$serInfo['server_id']);
        }
        //得到当天需要开服的服务器相关配置
        if(empty($serverIds)){
            //Helper::log('没有服务器需要进行同步',__CLASS__);
            exit();
        }
        $sql = "select server_id from ny_server_config where server_id in('".join('\',\'',$serverIds)."')
                    and api_url != ''
                    and mongo_host != ''
                    and mongo_port != ''
                    and mongo_user != ''
                    and mongo_passwd != ''
                    and mongo_db !='' ";
        $configRow = $serverModel->query($sql);
        empty($configRow) && $configRow = array();
        $configSerIds = array();
        foreach($configRow as $v){
            !empty($v['server_id']) && array_push($configSerIds,$v['server_id']);
        }
        $data = array();
        foreach($row as $value){
            if(empty($value['group_id']))continue;
            if(!empty($value['server_id']) && in_array($value['server_id'],$configSerIds)){//得到mongo已经配置完成的服务器
                $data[$value['group_id']][] = $value['server_id'];
            }
        }
        return $data;
    }

    /**
     * @mark 更新游戏信息
     * @param array $data
    */
    private static function AsyncUpNotice($data){
        if(empty($data)) exit();
        //只有返回状态$data[sid][info] == 1才会进行更新游戏
        $upServer = [];
        foreach($data as $key=>$val){
            if(!is_numeric($val['info'] && $val['info'] != 1))continue;
            array_push($upServer,$key);//得到成功的服务器id
        }
        if(empty($upServer)) exit();
        $url = Helper::v2();
        $backMsg = [];
        foreach($upServer as $val){
            $type = $data[$val]['type'];
            $upType = array_intersect(array('1','2'),$type);
            empty(array_diff(array('1','2'),$type)) && $upType=[3];
            $upType = array_shift($upType);

            $serId[$val] = 1;
            $res = Helper::rpc($url,$serId,'upRechMember',array('type'=>$upType,'handel_type'=>'up'));
            $backMsg = array(
                '0'=>'发布失败',
                '2'=>'非法操作',
                '1'=>'发布成功'
            );
            foreach($res as $key=>&$v){
                $v = isset($backMsg[$v])?$v.'/'.$backMsg[$v]:'未知错误';
            }
            array_push($backMsg,$res);
        }
        error_log("AsyncUpNotice: ".json_encode($backMsg)."\n\r",3,'server.log');
    }
}
?>