<?php

/**
 * 等级留存
 * 每天凌晨跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class LevelRemainController extends CronController
{

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('LevelRemain', 'stat', $serList, $start, $end);

            //$msg .= $this->stat($start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {
        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Level = new LevelremainModel();

        $Node = new NodeModel();

        $data = [];

        $serIdList = array_keys($serList);

        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_level_remain', $serIdList, $time);

        foreach ($serList as $key => $value) {

            //获取新增用户数
            $newAccount = $Node->getNewAccount($start_time, $end_time, $value['server_id']);

            if (empty($newAccount)) continue;

            $rs = $Level->call('Level', 'getDataByRangtime', array('end_time' => $end_time, 'accounts' => $newAccount), $value['server_id']);

            $rs = $rs[$value['server_id']];

            if (empty($rs)) continue;

            foreach ($rs as $v) {
                $dat = [];
                $dat = $v;
                $dat['server'] = $value['server_id'];
                $dat['create_time'] = $time;
                $dat['new_num'] = count($newAccount);

                array_push($data, $dat);
            }
        }

        if (!empty($data)) {
            if ($Level->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}