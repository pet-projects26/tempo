<?php

/**
 * ltv
 * 每天凌晨跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class LtvrecordController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Ltvrecord', 'stat', $serList, $start, $end);
        }
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {
        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Ltv = new LtvrecordModel();
        $Node = new NodeModel();

        $data = [];

        $serIdList = array_keys($serList);
        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_ltv_record', $serIdList, $time);

        foreach ($serList as $key => $value) {

            //定义时间留存数组
            $timeArr = [2 => 'two_day', 3 => 'three_day', 4 => 'four_day', 5 => 'five_day', 6 => 'six_day', 7 => 'seven_day', 8 => 'eight_day', 9 => 'nine_day', 10 => 'ten_day', 14 => 'fourteen_day', 20 => 'twenty_day', 30 => 'thirty_day', 45 => 'forty_five_day', 60 => 'sixty_day', 90 => 'ninety_day', 120 => 'one_hundred_twenty_day'];

            //获取今天新增用户
            $new_acc_data = $Node->getNewAccount($start_time, $end_time, $value['server_id']);

            //insert
            $dat = [];

            $dat['server'] = $value['server_id'];

            $dat['create_time'] = $time;

            $dat['new_account'] = !empty($new_acc_data) ? count($new_acc_data) : 0;

            $dat['one_day'] = '0(0%)';

            if ($dat['new_account'] > 0) {
                $one_day = $Ltv->call('Charge', 'getChargeDataByDate', array('date' => $date, 'accounts' => $new_acc_data), $value['server_id']);

                $one_day = $one_day[$value['server_id']];

                $one_day_value = $one_day['money'];

                $dat['one_day'] = $one_day_value . '(' . round(floatval($one_day_value) / $dat['new_account'], 2) . ')';
            }

            foreach ($timeArr as $field) {
                $dat[$field] = '';
            }
            unset($field);

            array_push($data, $dat);

            //update
            foreach ($timeArr as $day => $field) {
//              $dattime = strtotime('-'.$day.' day', $date);
                $day_time = $day * 86400;

                $dattime = $time - $day_time + 86400;

                $thatDay = date('Y-m-d', $dattime);

                $dattime = strtotime($thatDay);

                $dattime_end_time = $dattime + 86399;

                //查看当天的数据是否存在 不存在则continue;
                $record = $Ltv->getRowData($thatDay, $value['server_id']);

                if (empty($record)) continue;

                $Updatedata = [];

                $Updatedata[$field] = '0(0%)';

                if ($record['new_account'] > 0) {
                    //查看当天的新增用户
                    $that_day_new_acc = $Node->getNewAccount($dattime, $dattime_end_time, $value['server_id']);

                    $Updatedata = [];

                    $fieldValue = $Ltv->call('Charge', 'getPayingUserMoney', array('date' => date('Y-m-d H:i:s', $end_time), 'accounts' => $that_day_new_acc), $value['server_id']);

                    $fieldValue = $fieldValue[$value['server_id']];

                    $Updatedata[$field] = $fieldValue . '(' . round(floatval($fieldValue) / $record['new_account'], 2) . ')';
                }

                $Ltv->setRemain($Updatedata, $thatDay, $value['server_id']);
            }

        }

        if (!empty($data)) {
            if ($Ltv->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}