<?php

/**
 * 多人boss统计
 * 每天凌晨5点跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class ManyBossController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 19200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $start += 18000; //每天5点 到隔天凌晨5点
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('ManyBoss', 'stat', $serList, $start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $Many = new ManyBossModel();
        $ActiveAccount = new ActiveAccountModel();

        $data = [];

        //删除今天的数据
        $serIdList = array_keys($serList);

        CronBase::delDataByServerCreateTime('ny_many_boss', $serIdList, $start_time);

        foreach ($serList as $key => $value) {

            $dat = [];

            $format_start = CronBase::openTimeForStartTime($value['open_time'], $start_time);

            //获取活跃玩家数与活跃玩家
            $active_data = $ActiveAccount->getActiveAccount($format_start, $end_time, $value['server_id']);
            $active_num = 0;
            //获取活跃用户的登录次数和登录时长
            if ($active_data) {
                $active_num = $Many->call('ManyBossJoin', 'getActionOpenToAccount', array('reId' => CDict::$manyBoss['reId'], 'accounts' => $active_data, 'time' => $format_start), $value['server_id']);
                $active_num = $active_num[$value['server_id']] ? $active_num[$value['server_id']] : 0;
            } else {
                $active_num = 0;
            }

            if ($active_num == 0) continue;

            //查询boss信息表
            $info = $Many->call('ManyBossInfo', 'getDataByRangtime', array('start_time' => $format_start, 'end_time' => $end_time), $value['server_id']);

            if (empty($info[$value['server_id']])) {
                //$dat['challenge_num'] = 0;
                $dat['gold_inspire_count'] = 0;
                $dat['ingots_inspire_count'] = 0;
                $dat['inspire_num'] = 0;
                $dat['gold_inspire_num'] = 0;
                $dat['ingots_inspire_num'] = 0;
            } else {
                $dat = $info[$value['server_id']];
            }

            //查询boss奖励表查询挑战人数
            $reward = $Many->call('ManyBossReward', 'getDataByRangtime', array('start_time' => $format_start, 'end_time' => $end_time), $value['server_id']);

            $reward = $reward[$value['server_id']];

            if (empty($reward)) {
                $dat['challenge_count'] = 0;
                $dat['challenge_num'] = 0;
            } else {
                $dat['challenge_count'] = $reward['challenge_count'] ? $reward['challenge_count'] : 0;
                $dat['challenge_num'] = $reward['challenge_num'] ? $reward['challenge_num'] : 0;
            }

            //获取参与人数 使用卷轴人数 和 总次数
            $res = $Many->call('ManyBossJoin', 'getJoinNumByRangtime', array('start_time' => $format_start, 'end_time' => $end_time), $value['server_id']);

            $res = $res[$value['server_id']];

            if (empty($res)) {
                $dat['join_num'] = 0;
                $dat['add_num_role'] = 0;
                $dat['add_num_sum'] = 0;
            } else {
                $dat['join_num'] = $res['join_num'];
                $dat['add_num_role'] = $res['add_num_role'];
                $dat['add_num_sum'] = $res['add_num_sum'];
            }

            $dat['server'] = $value['server_id'];

            $dat['create_time'] = $start_time;

            $dat['active_num'] = $active_num;

            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($Many->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty ~';
        }

        return $msg;
    }
}