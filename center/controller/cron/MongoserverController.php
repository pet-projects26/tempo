<?php
/**
 * 把服务器列表同步到mongo
 * @author Administrator
 *
 */
class MongoserverController extends CronController{
    

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
      
        $db = new  Model();
        $package = $db->query('SELECT package_id FROM ny_package');
        $mongo = Util::mongoConn(SERVER_MONGO_HOST, SERVER_MONGO_PORT, SERVER_MONGO_USER, SERVER_MONGO_PASSWD,SERVER_MONGO_DB);
        $result = [];
        foreach ($package as $k => $v) {
            $package_id = $v['package_id'];
            if ($package_id) {
                $sql = "SELECT s.open_time,sc.domain,s.sort,s.max_online,s.`name`,s.server_id,s.num,s.channel_num,sc.login_host,sc.login_port,s.`status`,s.zone,s.tips,s.mom_server,s.display,s.review,p.package_id,p.channel_id,p.review_num,p.rv_param,p.type  FROM ny_server s
LEFT JOIN ny_server_config sc ON s.server_id = sc.server_id 
LEFT JOIN ny_package p ON s.group_id = p.group_id
WHERE package_id = '$package_id'
ORDER BY s.num DESC";
                $data = $db->query($sql);
                if (is_array($data)) {
                    try {
                        $mongo->package_server_list->update(['package' => $package_id], ['$set' => ['data' => $data, 'update_time' => time()]], ['upsert' => 1]);
                        $result = ['status' => 1, 'msg' => 'success'];
                    } catch (Exception $e) {
                        $result = ['status' => 0, 'msg' => $e->getMessage()];
                    }
                }
                echo json_encode($result);
            }
           Helper::log($package_id . '......' . json_encode($result),  'server_sync_mongo_result', 'info','center');
        }
    }
}