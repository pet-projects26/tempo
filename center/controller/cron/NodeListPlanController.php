<?php


class NodeListPlanController extends CronController
{

    public function run(array $argv = null)
    {

        $NodeRedis = (new NodeRedis());

        $num = 600;
        $msg = '';

        $isMonth = cronBase::checkIsMonth();

        if ($isMonth) {
            $num = 0;
        }

        $info = $NodeRedis->rPopAccountNodeList($num);


        if (!empty($info) && !empty($info['list']) && !empty($info['hashKey'])) {
            $List = $info['list'];

            $HashKeySet = $info['hashKey'];

            $Node = new NodeModel();
            $NewServerAccountRedis = new NewServerAccountRedis();

            $data = [];
            foreach ($List as $rows) {

                ksort($rows);
                $current = current($rows);
                $channel = $current['channel'];
                $package = $current['package'];
                $server = $current['server'];
                $account = $current['account'];
                $ip = $current['ip'];
                $create_time = $current['create_time'];

                $is_first = 0;
                $is_roll = 0;
                $is_create = 0;
                $is_loadover = 0;
                $role_id = 0;
                $is_create_redis = $NewServerAccountRedis->selectAccountStatus($channel, $package, $server, $account, 'is_create');
                $is_loadover_redis = $NewServerAccountRedis->selectAccountStatus($channel, $package, $server, $account, 'is_loadover');

                if ($is_create_redis && $is_loadover_redis) {
                    continue;
                }

                foreach ($rows as $key => $row) {
                    if ($is_create_redis) {
                        //如果已经创角   去除大于创角之前的节点
                        if($key <= CDict::$recordStep['RoleCreated']) {
                            continue;
                        }
                    }

                    if ($row['role_id'] != '' || $row['status'] == CDict::$recordStep['RoleCreated']) {
                        $is_create = 1;
                        $role_id = $row['role_id'];
                    }

                    $row['first'] = 0;
                    //执行逻辑
                    //查询是否为first
                    if ($row['status'] == CDict::$recordStep['StartLoadLib']) {
                        $row['first'] = 1;
                        $is_first = 1;
                    }

                    if ($row['status'] == CDict::$recordStep['EnterScene']) {
                        $is_loadover = 1;
                    }

                    //查询玩家是否为滚服玩家 如果是要新增为本服的新增用户
                    if ($is_first == 0 && $row['status'] == CDict::$recordStep['StartLoadConfig'] && $NewServerAccountRedis->Model->isRollServerAccount($row['server'], $row['account']) && !$NewServerAccountRedis->selectAccountIsFirst($row['channel'], $row['package'] ,$row['server'], $row['account'])) {
                        $is_roll = 1;
                        //新增 1 2
                        $row_1 = $row;
                        $row_1['status'] = CDict::$recordStep['StartLoadLib'];
                        $row_1['first'] = 1;
                        $data[] = $row_1;
                        $row_2 = $row;
                        $row_2['status'] = CDict::$recordStep['LoadedAllLib'];
                        $row_2['first'] = 0;
                        $data[] = $row_2;
                    }

                    $data[] = $row;
                }

                $is_loadover = ($is_create == 1 || $is_create_redis == 1) && $is_loadover == 1 ? 1 : 0;

                //根据参数判断是新增redis还是update redis
                if ($is_first || $is_roll) {

                    $redisInfo = [
                        'channel' => $channel,
                        'package' => $package,
                        'server' => $server,
                        'account' => $account,
                        'ip' => $ip,
                        'is_create' => $is_create,
                        'is_loadover' => $is_loadover,
                        'role_id' => $role_id,
                        'is_roll' => $is_roll,
                        'create_time' => $create_time
                    ];

                    $NewServerAccountRedis->addNewServerAccountKey($redisInfo['channel'], $redisInfo['package'], $redisInfo['server'], $redisInfo['account'], $redisInfo);
                } else if ($is_create == 1 || $is_loadover == 1) {

                    $fidlesValues = [
                        'role_id' => $role_id,
                        'is_create' => $is_create,
                        'is_loadover' => $is_loadover
                    ];

                    $NewServerAccountRedis->updateNewServerAccountField($channel, $package, $server, $account, $fidlesValues);
                }
            }

            if (!empty($data)) {
                if ($Node->multiAdd($data)) {
                    //插入成功则删除所有hashKey
                    $NodeRedis->delHashKey($HashKeySet);
                    $msg .= 'success';
                } else {
                    $NodeRedis->rPushAccountNodeList($HashKeySet);
                    $msg .= 'error';
                }
            } else {
                $msg .= 'data is empty ~';
            }
        }

        /*
        //分月表
        if ($isMonth == 1) {
            (new SplitSqlUtil('node'))->checkRenameTable();
        }
*/
        echo $msg;
    }
}