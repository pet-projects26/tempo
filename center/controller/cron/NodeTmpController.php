<?php


class NodeTmpController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    public function run(array $argv = null)
    {

        cronBase::checkIsMonth();
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $NodeModel = new NodeModel();

        $info = $NodeModel->getRows(['channel', 'package', 'server', 'account'], ['first' => 1, 'status' => 1]);

        if (!empty($info)) {
            $NewServerAccountRedis = new NewServerAccountRedis();

            $multiAdd = [];

            foreach ($info as $row) {

                $list = $NodeModel->getRows(['channel', 'package', 'server', 'mac', 'ip', 'account', 'role_id', 'status', 'first', 'create_time'], $row, ['GROUP' => 'status', 'ORDER' => ['status#asc']]);


                $current = current($list);

                $channel = $current['channel'];
                $package = $current['package'];
                $server = $current['server'];
                $account = $current['account'];
                $ip = $current['ip'];
                $create_time = $current['create_time'];
                $role_id = 0;

                $is_loadover = 0;
                $is_create = 0;
                $is_roll = 0;


                foreach ($list as $v) {
                    if ($v['status'] == CDict::$recordStep['RoleCreated']) {
                        $is_create = 1;
                        $role_id = $v['role_id'];
                    }

                    if ($v['status'] == CDict::$recordStep['EnterScene']) {
                        $is_loadover = 1;
                    }
                }

                $redisInfo = [
                    'channel' => $channel,
                    'package' => $package,
                    'server' => $server,
                    'account' => $account,
                    'ip' => $ip,
                    'is_create' => $is_create,
                    'is_loadover' => $is_loadover,
                    'role_id' => $role_id,
                    'is_roll' => $is_roll,
                    'create_time' => $create_time
                ];

                $rs = $NewServerAccountRedis->multiAddNewServerAccountKey($redisInfo['channel'], $redisInfo['package'], $redisInfo['server'], $redisInfo['account'], $redisInfo);
                if ($rs) {
                    $multiAdd[] = $redisInfo;
                }
            }

            if (!empty($multiAdd)) {

                //循环插入mysql
                $addRows = array_chunk($multiAdd, 1000);

                foreach ($addRows as $rows) {
                    $NewServerAccountRedis->Model->multiAdd($rows);
                }
            }
        }
    }

}