<?php
class NodedailyController extends CronController{
    private $no;
    private $nd;

    public function __construct(){
        $this->no = new NodeModel();
        $this->nd = new NodedailyModel();
        $this->cl = new ClientModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
        $nodeloss = $this->nd->getNodedailyByDate($date);
        if(!$nodeloss){
            $data = array('date' => $time);
            $data['device_num'] = $this->cl->getNewDeviceByDate($date); //新增设备数
            $status_num = $this->no->getNodeBydate($date , $data['device_num']); //各个节点的人数
            $prev_node_num = 0;
            foreach($status_num as $k => $v){
                if($data['device_num']){
                    $node_num = $v ? $v : 0;
                    $node_tloss = sprintf("%.2f" , (1 - $node_num / $data['device_num']) * 100) .'%';
                    $node_loss = ($k == 'one') ? $node_tloss : $prev_node_num ? sprintf("%.2f" , (1 - $node_num / $prev_node_num) * 100) . '%' : '100%';
                    $data[$k] = $node_num . '/' . $node_loss . '/' . $node_tloss;
                    $prev_node_num = $v;
                }
                else{
                    $data[$k] = '0%/0%/0%';
                }
            }
            $this->nd->setNodedaily($data);
        }
    }
}