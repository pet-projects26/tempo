<?php

class NodedailyhourController extends CronController
{

    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d H:00:00', $time));
            //每小时的05分跑上一个小时的数据
            if ($time - $start_time < 305) {
                $start_time = $start_time - 3600;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d H:00:00'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 3600) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 3600;
            $end = $start + 3599;

            //并发执行
            $msg .= CronBase::runmultiprocess('Nodedailyhour', 'stat', $serList, $start, $end);
            //$msg .= $this->stat($start, $end);
        }
        echo $msg;
    }

    public function stat($serList, $start_time, $end_time)
    {
        $date = date('Y-m-d H:00:00', $start_time);
        $time = strtotime($date);

        $data = [];

        $Node = new NodeModel();

        $NodeDailyHour = new NodedailyhourModel();

        $statusArr = $NodeDailyHour->statusArr;

        $serIdList = array_keys($serList);
        //删除这小时数据
        CronBase::delDataByServerCreateTime('ny_node_daily_hour', $serIdList, $time);

        foreach ($serList as $key => $row) {

            $new_acc = $Node->getNewAccount($start_time, $end_time, $row['server_id']);

            $dat = [];

            foreach ($statusArr as $v) {
                $dat[$v] = 0;
                if ($v != 'one') {
                    $dat[$v . '_load_time'] = 0;
                }
            }

            //获取新增跳转之后的新手用户数
            if (!empty($new_acc)) {
                //获取新增用户的登录成功数
                $dat['new_active'] = count($new_acc);

                $account = $new_acc;

                $newNumStep = CDict::$newNumStep;

                foreach ($newNumStep as $v) {
                    $res = $Node->getAccountStatusServerLoadTime($start_time, $end_time, $row['server_id'], $v, $account);

                    if (empty($res['account']) && $v != 9) break;

                    $dat[$statusArr[$v - 1]] = count($res['account']);
                    if ($v != 1 && $v != 9) {
                        $dat[$statusArr[$v - 1] . '_load_time'] = $res['load_time'];
                    }
                    //9是登录失败
                    if ($v != 9) {
                        $account = $res['account'];
                    }
                }
            } else {
                $dat['new_active'] = 0;
            }
            $dat['server'] = $row['server_id'];
            $dat['create_time'] = $time;
            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($NodeDailyHour->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}