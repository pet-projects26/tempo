<?php

/**
 *
 * 新手任务留存统计
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class NoviceTaskController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('NoviceTask', 'stat', $serList, $start, $end);
        }
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Task = new NoviceTaskModel();

        $Node = new NodeModel();

        $data = [];

        $serIdList = array_keys($serList);

        foreach ($serList as $key => $value) {

            //查询今天是否有新增用户数
            $new_acc_data = $Node->getNewAccount($start_time, $end_time, $value['server_id']);

            if (empty($new_acc_data)) continue;

            //去单服获取今天的统计数据
            $resData = $Task->call('Task', 'getDataByRangtime', ['start_time' => $start_time, 'end_time' => $end_time, 'accounts' => $new_acc_data], $value['server_id']);

            $resData = $resData[$value['server_id']];

            if (empty($resData)) continue;

            foreach ($resData as $k => $v) {

                $dat = [];

                //过滤task_id m1110 以上的 任务
                if ((int)substr($v['task_id'], 1) > 1110) {
                    continue;
                }
                $dat = $v;
                $dat['active_num'] = count($new_acc_data);
                $dat['server'] = $value['server_id'];
                $dat['create_time'] = $time;

                array_push($data, $dat);
            }
        }

        if (!empty($data)) {
            //删除今日数据
            CronBase::delDataByServerCreateTime('ny_novice_task', $serIdList, $time);
            if ($Task->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}