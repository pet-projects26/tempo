<?php

/**
 * 在线分布
 * 每天凌晨跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class OnlineDistributionController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('OnlineDistribution', 'stat', $serList, $start, $end);
        }
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $online = new OnlinedistributionModel();
        $ActiveAccount = new ActiveAccountModel();

        $serIdList = array_keys($serList);

        $data = [];

        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_online_distribution', $serIdList, $time);

        foreach ($serList as $key => $value) {

            $server = $value['server_id'];
            //查询今天是否有活跃人数
            $active_data = $ActiveAccount->getActiveAccount($start_time, $end_time, $server);

            if (empty($active_data)) continue;

            $timeArr = [0, 1, 5, 10, 20, 30, 60, 90, 120, 150, 180, 240, 300, 1440];

            //获取活跃玩家的在线时间
            $acc_longtime_arr = $online->call('Login', 'getOlTimeByDate_json', ['start_time' => $start_time, 'end_time' => $end_time, 'accounts' => $active_data], $server);

            $acc_longtime_arr = $acc_longtime_arr[$server];

            if (empty($acc_longtime_arr)) continue;

            $dat = [
                '(0,1]' => 0,
                '(1,5]' => 0,
                '(5,10]' => 0,
                '(10,20]' => 0,
                '(20,30]' => 0,
                '(30,60]' => 0,
                '(60,90]' => 0,
                '(90,120]' => 0,
                '(120,150]' => 0,
                '(150,180]' => 0,
                '(180,240]' => 0,
                '(240,300]' => 0,
                '(300,1440]' => 0,
                '(1440,-]' => 0
            ];

            foreach ($acc_longtime_arr as $longtime) {
                $min = round((float)$longtime / 60, 2);
                foreach ($timeArr as $k => $v) {
                    if ($min <= 1440 && $min > $v && $min <= $timeArr[$k + 1]) {
                        $dat['(' . $v . ',' . $timeArr[$k + 1] . ']'] += 1;
                    }
                }
                if ($min > 1440) {
                    $dat['(1440,-]'] += 1;
                }
            }

            $dat['active_num'] = count($active_data);

            //把未登录的人数加到(0,1]区间
            if ($dat['active_num'] - count($acc_longtime_arr) > 0) {
                $dat['(0,1]'] += $dat['active_num'] - count($acc_longtime_arr);
            }

            $dat['server'] = $server;
            $dat['create_time'] = $time;

            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($online->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}