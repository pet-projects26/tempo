<?php
/**
 * 晚上23点到0点59:59之间不执行
 * 根据注册数达到2000人,自动开启同个渠道组的下个服
 * 20180507
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 目前只支持一个渠道组的服务器
 */
class OpenserverController extends CronController{
	
	private   $regnum = 1000;//总注册数
	private   $group_id = '5';//渠道组
	private   $filterTimeArr = [23,0,1,2,3,4,5,6,7,8,9];//不在这些时刻才执行脚本
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
    	
    	$h = date("H",time());
    	if (!in_array($h,$this->filterTimeArr)) {
    		$group_id = $this->group_id;
    		$sql = "SELECT `server_id`, `name`,group_id,open_time FROM ny_server WHERE `type` = 1 AND `status` NOT IN (-1, 0) and server_id not like '%9999'   and server_id not like '%9998'  AND group_id IN({$group_id})  ORDER BY open_time DESC";
    		$m = new Model();
    		$dqServer = $m->query($sql);
    		$dangqian = [];
    		$group_idArr = explode(",",$group_id);
    		foreach ($group_idArr as $key=>$val) {
    			foreach ($dqServer as $k=>$v) {
    				if ($val == $v['group_id']) {
    					$dangqian[] = $v;
    					break;
    				}
    			}
    		}
    		
    		file_put_contents('/tmp/Openserver.txt','['.date('Y-m-d H:i:s',time()).']'.'当前服:'.json_encode($dangqian)."\n\r",FILE_APPEND);
    		if ($dangqian != array()) {
    			//查找注册数是否大于2000
    			foreach ($dangqian as $k=>$v) {
    				$rs = $m->call('AccountData' , 'getAllRegister' , [] , $v['server_id']);
    				
    				file_put_contents('/tmp/Openserver.txt','['.date('Y-m-d H:i:s',time()).']'.'当前服的注册数据:'.json_encode($rs)."\n\r",FILE_APPEND);
    				if ($rs && $rs[$v['server_id']]['count'] > $this->regnum) {
    					//查找最近未开服的服,后台有配置就改变状态status和display
    					$sql = "SELECT s.`server_id`, `name`,group_id,open_time,sc.mysql_db FROM ny_server s LEFT JOIN ny_server_config sc ON s.server_id = sc.server_id WHERE `type` = 1 AND `status` IN (0) and s.server_id not like '%9999'  and s.server_id not like '%9998'  AND group_id IN({$v['group_id']})  ORDER BY open_time ASC LIMIT 1";
    					$nopen = $m->query($sql);
    					
    					file_put_contents('/tmp/Openserver.txt','['.date('Y-m-d H:i:s',time()).']'.'接下来要开启的新服:'.json_encode($nopen)."\n\r",FILE_APPEND);
    					if ($nopen && $nopen[0]['server_id'] && $nopen[0]['group_id']) {
    						$conditions['db'] = $nopen[0]['mysql_db'];
    						$conditions['open_time'] = date('Ymd',time());
    						$shell = $m->call('AccountData' , 'execShellOpenServer' ,['conditions'=>$conditions], $nopen[0]['server_id']);
    						
    						if ($shell[$nopen[0]['server_id']] === 0) {
    							//找到时修改状态和显示
    							//修改当前的服的状态为流畅
    							$sql = "UPDATE ny_server SET `status` = 2 WHERE server_id = '{$v['server_id']}' AND group_id = '{$v['group_id']}' LIMIT 1";
    							$m->query($sql);
    							$open_time = time();
    							//新的服的状态为新服
    							$sql = "UPDATE ny_server SET `status` = 1,display = 1,open_time ={$open_time},display_time = {$open_time}  WHERE server_id = '{$nopen[0]['server_id']}' AND group_id = '{$nopen[0]['group_id']}' LIMIT 1";
    							$m->query($sql);
    							echo 'success'.PHP_EOL;
    							file_put_contents('/tmp/Openserver.txt','['.date('Y-m-d H:i:s',time()).']'.'执行结果:success'."\n\r",FILE_APPEND);
    						}
    				
    					}
    				}
    			}
    		}
    	}	

  }

}