<?php

class RegistercountController extends CronController{
    private $ro;
	private $r;

    public function __construct(){
		
        $this->rc = new RegistercountModel();
		$this->r = new RegisterModel();
    }
  	public function run(array $argv = null){
		$H=date("H");
		if($H==0){
			$H=23;
			$day=date("Y-m-d",strtotime("-1 day"));
		}else{
			$H=$H-1;
			$day=date("Y-m-d");	
		}
		$nowtime= strtotime(date("$day $H:00:00"));
		$starttime=$nowtime;
		$endtime=$nowtime+3599;
		
		$channelsql="select channel_id from ny_channel";
		$channel=$this->r->query($channelsql);
		
		foreach($channel as $key=>$val){//根据所有渠道名称去搜索数据
			$sql="select count(1) as count,channel  from ny_register where create_time>=$starttime and create_time<=$endtime and channel='".$val['channel_id']."'";		
			$result=$this->r->query($sql);
			if($result[0]['count']!=0){
				foreach($result as $k=>$v){
					
					$sql="select countlist,count from ny_register_count where date='$day' and channel='".$v['channel']."'";
					$res=$this->r->query($sql);
					if($res){
						
						//update
						$count=$res[0]['count']+$v['count'];//总数
						$countlist=json_decode($res[0]['countlist'],true);
						$countlist[$H]=$v['count'];
						$countlist=json_encode($countlist);
						$updatesql="update ny_register_count set countlist='$countlist',count=$count where date='$day' and channel='".$v['channel']."'";
						//echo $updatesql;
						$this->r->query($updatesql);
					}else{
						
						//insert
						$countlist=array(
							$H=>$v['count'],
						);
						$count=$v['count'];
						$countlist=json_encode($countlist);
						
						
						$data=array();
						
						$addsql="insert into ny_register_count values(0,'$day','$countlist','".$v['channel']."',$count) ";
						//echo $addsql;
						$this->r->query($addsql);
					}
				}	
			}else{//查不到数据
				$sql="select countlist from ny_register_count where date='$day' and channel='".$val['channel_id']."'";
				$res=$this->r->query($sql);
					if($res){
						//update
						$countlist=json_decode($res[0]['countlist'],true);
						$countlist[$H]=0;
						$countlist=json_encode($countlist);
						$updatesql="update ny_register_count set countlist='$countlist' where date='$day' and channel='".$val['channel_id']."'";
						
						$this->r->query($updatesql);
					}else{
						
						//insert
						$countlist=array(
							$H=>0,
						);
						$count=0;
						$countlist=json_encode($countlist);
						$addsql="insert into ny_register_count values(0,'$day','$countlist','".$val['channel_id']."',$count) ";
						//echo $addsql;
						$this->r->query($addsql);
					}
			}
		}
	}
	
}