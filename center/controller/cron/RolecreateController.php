<?php

class RolecreateController extends CronController{
    private $ro;
	private $r;

    public function __construct(){
		$this->r = new RolecreateModel();
    }

  	public function run(array $argv = null){
		$H=date("H");
		if($H==0){
			$H=23;
			$day=date("Y-m-d",strtotime("-1 day"));
		}else{
			$H=$H-1;
			$day=date("Y-m-d");	
		}
		$nowtime= strtotime(date("$day $H:00:00"));
		$starttime=$nowtime;
		$endtime=$nowtime+3599;
		$jintian=strtotime(date("$day 00:00:00"));
		$serversql="select server_id from ny_server ";
		$server=$this->r->query($serversql);
 	
		$mac=array();
		foreach($server as $key=>$val){//根据所有服务器名称去搜索数据 node表步骤3代表创建角色了
			$server_id=$val['server_id'];
			$sql="select count(1) as rolecount,server from ny_node where status=3 and  create_time>=$starttime and create_time<=$endtime and server='".$val['server_id']."'";//查询创角数量
			
			$allmacsql="select DISTINCT(mac) as mac from ny_node where status=3 and  create_time>=$jintian and create_time<$starttime and server='".$val['server_id']."'";//计算今天到当前时间少一个小时的所有mac
			$macsql="select DISTINCT(mac) as mac from ny_node where status=3 and  create_time>=$starttime and create_time<=$endtime and server='".$val['server_id']."'";//计算这个小时的mac
			$allmac=$this->r->query($allmacsql);
			$mac=$this->r->query($macsql);
			$one=array();
			$two=array();
			foreach($allmac as $k=>$v){
				$one[]=$v['mac'];	
			}
			foreach($mac as $k=>$v){
				$two[]=$v['mac'];	
			}
			$diff=array_diff($two,$one);//两个数组的差集就是当前这个小时的mac设备数
			$mac[$server_id]=count($diff);

			$result=$this->r->query($sql);
			if($result[0]['rolecount'] !=0){
				foreach($result as $k=>$v){
					$sql="select rolelist,rolecount,accountlist,accountcount from ny_rolecreat where date='$day' and server='".$v['server']."'";
					
					$res=$this->r->query($sql);
					$server=$v['server'];
					if($res){
						//update
						$rolecount=$res[0]['rolecount']+$v['rolecount'];//创角数
						
						$accountcount=$res[0]['accountcount']+$mac[$server];//设备数
						$rolelist=json_decode($res[0]['rolelist'],true);
						$accountlist=json_decode($res[0]['accountlist'],true);
						
						$accountlist[$H]=$mac[$server];
						$rolelist[$H]=$v['rolecount'];
						$rolelist=json_encode($rolelist);
						$accountlist=json_encode($accountlist);
						$updatesql="update ny_rolecreat set rolecount='$rolecount',accountcount='$accountcount',accountlist='$accountlist',rolelist='$rolelist' where date='$day' and server='".$v['server']."'";

						$this->r->query($updatesql);
					}else{
						//insert
						$accountlist=array(
							
							$H=>$mac[$server],
						);
						$rolelist=array(
							$H=>$v['rolecount'],
						);
						$rolecount=$v['rolecount'];
						
						$accountcount=$mac[$server];
						$accountlist=json_encode($accountlist);
						$rolelist=json_encode($rolelist);
						
						$addsql="insert into ny_rolecreat values(0,'$day','$rolecount','$rolelist','$accountcount','$accountlist','".$v['server']."') ";
						$this->r->query($addsql);
					}
				}	
			}else{//查不到数据
				$sql="select accountlist,rolelist,accountcount,rolecount from ny_rolecreat where date='$day' and server='".$val['server_id']."'";
				$res=$this->r->query($sql);
					if($res){
						//update
						$accountlist=json_decode($res[0]['accountlist'],true);
						$accountlist[$H]=0;
						
						$rolelist=json_decode($res[0]['rolelist'],true);
						$rolelist[$H]=0;
						
						$accountlist=json_encode($accountlist);
						$rolelist=json_encode($rolelist);
						$updatesql="update ny_rolecreat set rolelist='$rolelist',accountlist='$accountlist' where date='$day' and server='".$val['server_id']."'";
						$this->r->query($updatesql);
					}else{
						//insert
						$accountlist=array(
							$H=>0,
						);
						$rolelist=array(
							$H=>0,
						);
						$accountcount=0;
						$rolecount=0;
						$accountlist=json_encode($accountlist);
						$rolelist=json_encode($rolelist);
						$addsql="insert into ny_rolecreat values(0,'$day','$rolecount','$rolelist','$accountcount','$accountlist','".$val['server_id']."') ";
						$this->r->query($addsql);
					}
			}
		}
		
	}
}