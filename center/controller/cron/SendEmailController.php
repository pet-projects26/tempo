<?php
/**
 * 定时邮件发送
 * @author  wt1ao <[<email address>]>
 * @date(format)
 * 
 */
class SendEmailController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){

        $time = time();
        $start_time = strtotime(date('Y-m-d H:i'));

        $end_time = $start_time + 59;

        //执行命令
        $msg = $this->stat($start_time, $end_time);
        echo $msg;
        die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time) {

  	  $nowTime = time();

  	  //获取该时间区间内的待发送的邮件
  	  $mailModel = new MailModel();
  	  $bookEmail = $mailModel->getBookEmail($start_time, $end_time);

  	  if ($bookEmail) {
  	      foreach($bookEmail as $key => $val) {
  	          $val['attach'] = json_decode($val['attach'], true);
  	          //如果审核状态为0
              if ($val['status'] == 1) {
                  //发送
                  $server = explode(',',$val['servers']);

                  $val['role_id'] = $val['role_id'] == 0 ? array(intval(0)) : explode(',' , $val['role_id']);//把角色id转成数组

                  foreach($val['role_id'] as &$v) {
                      $v = intval($v);
                  }

                  $msg = $mailModel->send_mail($server, $val, $val['attach']);

                  $mailModel->update(array('msg' => $msg, 'is_send' => 1,'send_time' => $nowTime) , array('id'=>$val['id']));

              } else {
                  //不发送 update该条信息的msg 改为超时未审核未发送
                  $updateMsg = '超时未审核未发送';

                  $msg = $mailModel->update(array('msg' => $updateMsg, 'is_send' => 3) , array('id' => $val['id']));
              }
          }
      } else {
  	      $msg = date('Y-m-d H:i:s', $nowTime).'没有待发送邮件';
      }

//  	  $msg = '';

      return $msg;
  }
}