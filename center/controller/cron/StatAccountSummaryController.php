<?php
/*
* 1. 注册数 指当天创号成功的账号数量
* 2. 登录数 指当天有登录的账号数量
* 3. 老玩家数 指当天有登录的非当天注册的账号数量
* 4. 充值人数 指当天有充值的玩家数量
* 5. 登录付费率 = 充值人数 / 登录数
* 6. 总AP = 充值金额 / 充值人数
* 7. 新充值人数 指当天注册的并且当天有充值的账号数量
* 8. 新充值金额 指当天注册的账号在当天的充值金额
* 9. 新增付费率 = 新付费金额 / 新充值人数
* 10. 新充值AP = 新付费金额 / 注册数
* 11. 老充值人数 = 指当天充值成功的老玩家数
* 12. 老充值金额 = 指当天充值成功的老玩家的总充值金额
* 13. 老付费率 = 老充值人数 / 老玩家数
* 14. 老充值AP = 老充值金额 / 老充值人数
* 15. 注册AP = 充值金额 / 注册数
*/
class statAccountSummaryController extends CronController{

    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
      $serList = CronBase::getServerList();

      if (empty($argv)) {
        $time = time();
        $start_time = strtotime(date('Y-m-d'));

        $start_time = $start_time - 86400; //今天跑昨天的数据

        $end_time = $start_time;
      } elseif (!empty($argv[0]) && !isset($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime(date('Y-m-d'));
      
      } elseif (!empty($argv[0]) && !empty($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime($argv[1]);
      }

      if ($start_time > $end_time) {
        die ('start time > end time');
      }

      $num = ceil(($end_time - $start_time) / 86400) + 1;

      //执行命令
      for ($i = 0; $i < $num; $i++) {
        $start = $start_time + $i * 86400;
        $end = $start + 86399;
        
        //并发执行
        $msg = CronBase::runmultiprocess('statAccountSummary', 'stat', $serList, $start, $end);
      }

      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($serList, $start_time, $end_time) {
      //帐号注册
      $regSql = "SELECT COUNT(DISTINCT account) AS reg_num, channel, package FROM ny_register WHERE  create_time BETWEEN {$start_time} AND {$end_time} GROUP BY channel, package ";

      $db = new Model();

      $regRes = $db->query($regSql);

      //帐号登录
      $loginSql = "SELECT COUNT(DISTINCT account) AS login_num, channel, package  FROM  log_account 
              WHERE create_time BETWEEN {$start_time} AND {$end_time} GROUP BY package, channel";


      $loginRes = $db->query($loginSql);

     


      //老玩家
      $oldSql = "SELECT COUNT(DISTINCT reg.account) AS old_num, reg.channel, reg.package  FROM ny_register 
              AS reg LEFT JOIN  log_account AS log WHERE reg.create_time > {$start_time} AND log.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY reg.channel, reg.package";

      //充值金额，充值次数
      $chargeSql = "SELECT SUM(money) AS charge_money,  COUNT(DISTINCT account ) AS charge_num, package, channel 
                FROM ny_order  WHERE status != 0 
              AND create_time BETWEEN {$start_time} AND  {$end_time} GROUP BY package, channel";

      $chargeRes = $db->query($chargeSql);

      //新玩家充值金额
      $newChargeSql = "SELECT SUM(money) AS new_money, COUNT(reg.account) AS new_charge_num FROM ny_order AS charge 
                LEFT JOIN ny_register AS reg ON reg.account = charge.account WHERE charge.status != 0 AND  
              charge.create_time BETWEEN {$start_time} AND {$end_time} AND reg.create_time BETWEEN {$start_time} AND {$end_time} ";

      $newChargeRes = $db->query($newChargeSql);

      //老玩家充值金额
      $oldChargeSql = "SELECT SUM(money) AS old_money, COUNT(reg.account) AS old_charge_num FROM ny_order AS charge 
                LEFT JOIN ny_register AS reg ON reg.account = charge.account WHERE charge.status != 0 AND
              reg.create_time < {$start_time} AND charge.create_time BETWEEN {$start_time} AND  {$end_time} GROUP BY reg.package, reg.channel ";

      $oldChargeRes = $db->query($oldChargeSql);

  }
}