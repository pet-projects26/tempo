<?php
/**
 * 
 */
class StatActivityController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
      $serList = CronBase::getServerList();

      if (empty($argv)) {
        $time = time();
        $start_time = strtotime(date('Y-m-d'));

        //当天零点的时候会重跑昨天的数据，
        if ($time - $start_time < 1200) {
          $start_time = $start_time - 86400;
        }

        $end_time = $start_time;
      } elseif (!empty($argv[0]) && !isset($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime(date('Y-m-d'));
      
      } elseif (!empty($argv[0]) && !empty($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime($argv[1]);
      }

      if ($start_time > $end_time) {
        die ('start time > end time');
      }

      $num = ceil(($end_time - $start_time) / 86400) + 1;

      //执行命令
      for ($i = 0; $i < $num; $i++) {
        $start = $start_time + $i * 86400;
        $end = $start + 86399;
        
        //并发执行
        $msg = CronBase::runmultiprocess('statActivity', 'stat', $serList, $start, $end);
      }

      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($serList, $start_time, $end_time) {
      // 充值投资 71
      // 苍穹云购参与率 102
      // 幸运鉴定参与率 108
      // 砸蛋活动参与率 114
      // 燃放烟花 109
      // 全民嗨活动 111
      // vip购买 20
      //该类型下总消耗的元宝
      
      $WHERE = " 1 AND coin_source IN (71, 102, 108, 114, 109, 111, 20) AND log.type = 0";
      $totalSql = "SELECT COUNT(DISTINCT role.role_id) AS total_consume_num, SUM(log.gold) AS total_consume_gold, package ,psId
                   FROM ny_payment AS log 
                LEFT JOIN ny_role AS role ON role.role_id = log.role_id WHERE log.type = 0 AND log.gold > 0 AND log.create_time 
                    BETWEEN {$start_time} AND  {$end_time} GROUP BY package ,psId ";


      //充值人数，充值元宝
      $chargeSql = "SELECT COUNT(DISTINCT charge.role_id) AS charge_num, SUM(charge.gold) AS charge_gold, package,psId FROM ny_order AS charge
                      LEFT JOIN ny_role AS role ON charge.role_id = role.role_id WHERE charge.gold != 180 AND charge.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY package,psId";
      //查找出库存
      $sql = "SELECT * FROM (
                        SELECT * FROM ny_payment WHERE  create_time BETWEEN {$start_time} AND  {$end_time} 
                        AND bag_gold > 0 ORDER BY create_time DESC 
                      ) AS tmp1 GROUP BY  tmp1.role_id ";

      $inventorySql = "SELECT  SUM(pay.bag_gold) AS inventory_gold, package,psId 
                      FROM ({$sql}) AS pay  LEFT JOIN ny_role AS role
                        ON role.role_id = pay.role_id GROUP BY package,psId";
      

      //每个类型
      $singleSql = "SELECT COUNT(DISTINCT role.role_id) AS single_consume_num, SUM(log.gold) AS single_consume_gold, package,coin_source ,psId
                   FROM ny_payment AS log 
                LEFT JOIN ny_role AS role ON role.role_id = log.role_id WHERE {$WHERE} AND log.create_time 
                    BETWEEN {$start_time} AND  {$end_time} GROUP BY package, coin_source  ,psId";

      $data = array(
        'total' => $totalSql,
        'charge' => $chargeSql,
        'inventory' => $inventorySql,
        'single' => $singleSql
      );

      $arr  = array('query' => array('data' => $data));
      $servers = array_keys($serList);

      $res  = CronBase::makeHttpParams($servers, $arr);

      $urls = $res['urls'];
      $params = $res['params'];
      $result = Helper::rolling_curl($urls, $params);


      $result = CronBase::getData($result);


      $types = array(71, 102, 108, 114, 109, 111, 20);

      foreach ($result as $server_id => $data) {

        //消耗
        $totalRes = CronBase:: makeKey($data['total'] ,'package' ,  'psId');

        $packageRes = array();
        $psIds = array();
        
       
        foreach ($totalRes as $key => $value) {
          $packageRes[] = $value['package'];
          $psIds[] = $value['psId'];
        }
        
        

        //充值人数，充值金额
        $chargeRes = CronBase:: makeKey($data['charge'] , 'package' ,  'psId');


        //库存人数
        $inventoryRes = CronBase:: makeKey($data['inventory'] , 'package' ,  'psId');

        $total =  CronBase::mergeData($totalRes, $chargeRes, $inventoryRes);


        $singleRes = array();

        foreach ($data['single'] as $key => $row) {
          $coin_source = $row['coin_source'];
          $psId        = $row['psId'];
          $package     = $row['package'];

          $singleRes[$coin_source][$package .'-' . $psId] = $row;
        }


        foreach ($types as $type) {
          foreach ($packageRes as $package) {
            foreach ($psIds as  $psId) {

              $key  =$package .'-' . $psId;

              if (!isset($singleRes[$type][$key])) {
                $row = array();
          
                $row = array(
                  'single_consume_num'  => 0,
                  'single_consume_gold' => 0,
                  'coin_source'         => $type, 
                  'package'             => $package,
                  'psId'                => $psId,
                );
              $singleRes[$type][$key] = $row;
              }
            }
            //$singleRes[$type] = $row;
          }
        }

        

        foreach ($singleRes as $type => &$row) {
          
          foreach ($row as $k2 => &$r2) {
            $r2 = isset($total[$k2]) ? array_merge($r2, $total[$k2]) : $r2;
          }

          unset($r2);
        }
        unset($row);

        if(!empty($singleRes)){
         
          CronBase::delDataByServer('stat_activity', $psIds,$packageRes, $start_time);
        }
        $rs = array();
        foreach ($singleRes as $type =>  $rows) {
          foreach ($rows as &$row) {
            
              $server = CronBase::serverBypsId($row['psId']);
             
              $row['server'] = !empty($server) ? $server : $server_id;
              $row['time'] = $start_time;
              $row['type'] = $row['coin_source'];
              unset($row['coin_source']);
              unset($row['psId']);

              $rs[] = $row;
              //$insertId = $m->add($v);
            }
           
          
          unset($row);
        }

        unset($row);

        CronBase::insertData('stat_activity' , $rs);


        $msg = PHP_EOL. '服：'. $server_id .' '. date('y-m-d', $start_time). ' success'.PHP_EOL;

        echo $msg;
      }
  }
}