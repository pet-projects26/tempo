<?php

/**
 * 每日充值
 * 新增充值人数：当天新增玩家并在当天进行充值的角色数
 * 充值人数：当天充值的人数
 * 新增充值金额：新增充值人数当天充值的金额
 * 登录ARPU: 充值总额 / 登录人数
 * 新增充值APRU：  新增充值总额 / 充值人数
 * arppu＝充值总额／充值人数
 * 注册ARPU： 充值总额 / 注册人数
 *
 * @author  lai <[<email address>]>
 * @date(format)
 *
 */
class StatChargeDailyController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        $serList = CronBase::getServerList();

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据，
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;

        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg = CronBase::runmultiprocess('statChargeDaily', 'stat', $serList, $start, $end);
        }

        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        //充值金额与充值人数，充值次数
        $chargeSql = "SELECT 
              SUM(money) AS money,
              COUNT(DISTINCT ord.role_id) AS charge_num,
              COUNT(ord.`id`) AS charge_times,
              package ,psId
            FROM
              ny_order AS `ord` 
              LEFT JOIN ny_role AS role 
                ON ord.`role_id` = role.role_id 
            WHERE ord.`create_time` BETWEEN {$start_time} 
              AND {$end_time} 
            GROUP BY package ,psId  ";

        //注册人数
        $regSql = "SELECT COUNT(role_id) AS reg_num, package ,psId FROM ny_role 
              WHERE create_time  BETWEEN {$start_time} AND {$end_time} GROUP BY package ,psId";


        //登录人数
        $loginSql = "SELECT COUNT(DISTINCT login.role_id) AS login_num, package ,psId FROM ny_role AS role
              LEFT JOIN ny_login AS login ON role.role_id = login.role_id
            WHERE login.start_time BETWEEN {$start_time} AND  {$end_time} GROUP BY package ,psId ";


        //新增充值人数, 新增充值金额
        $newChargeSql = "SELECT COUNT(DISTINCT ord.role_id) AS  new_charge_num, SUM(`money`) AS new_charge_money, package ,psId FROM ny_role AS role 
            LEFT JOIN ny_order AS ord 
          ON  role.role_id  = ord.role_id 
            WHERE  role.create_time BETWEEN {$start_time} AND  {$end_time}
          AND ord.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY  package ,psId ";


        //当天第一次充值的人数
        $firstChargeSql = "SELECT COUNT(DISTINCT ord.role_id) AS first_charge_num, sum(`money`) AS first_charge_money, package ,psId FROM ny_role AS role
            LEFT JOIN  (
                SELECT * FROM (SELECT * FROM ny_order ORDER BY create_time ASC ) AS tmp GROUP BY role_id 
                      ) AS ord 
            ON role.role_id  = ord.role_id WHERE ord.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY role.package ,psId ";

        $data = array(
            'charge' => $chargeSql,
            'reg' => $regSql,
            'login' => $loginSql,
            'newCharge' => $newChargeSql,
            'firstCharge' => $firstChargeSql,
        );

        $arr = array('query' => array('data' => $data));
        $servers = array_keys($serList);

        $res = CronBase::makeHttpParams($servers, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);

        $result = CronBase::getData($result);

        foreach ($result as $server_id => $data) {
            //充值人数，充值金额

            $chargeRes = CronBase:: makeKey($data['charge'], 'package', 'psId');

            //注册人数

            $regRes = CronBase:: makeKey($data['reg'], 'package', 'psId');

            //登录人数
            $loginRes = CronBase:: makeKey($data['login'], 'package', 'psId');

            if (!empty($loginRes)) {
                $rs = CronBase::getPackagepsId($loginRes);
                $packageRes = $rs['packageRes'];
                $psIds = $rs['psIds'];
            } else {
                continue;
            }

            //新增充值人数
            $newChargeRes = CronBase:: makeKey($data['newCharge'], 'package', 'psId');

            //首充人数
            $firstChargeRes = CronBase:: makeKey($data['firstCharge'], 'package', 'psId');

            $insert = CronBase::mergeData($chargeRes, $regRes, $loginRes, $newChargeRes, $firstChargeRes);

            if (!empty($insert)) {

                if (!empty($psIds)) {

                    CronBase::delDataByServer('stat_charge_daily', $psIds, $packageRes, $start_time);
                }
                $data = array();

                foreach ($insert as &$row) {

                    $server = CronBase::serverBypsId($row['psId']);
                    $row['server'] = !empty($server) ? $server : $server_id;
                    $row['time'] = $start_time;
                    unset($row['psId']);
                    $data[] = $row;
                }

                unset($row);

                CronBase::insertData('stat_charge_daily', $data);
            }

            $msg = PHP_EOL . '服：' . $server_id . ' ' . date('y-m-d', $start_time) . ' success' . PHP_EOL;

            echo $msg;
        }
    }
}