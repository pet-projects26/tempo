<?php
//use PHPMailer\PHPMailer\PHPMailer;

/**
 * 道具监控(四个小时更新)
 * 20180530
 * @author  william <[<email address>]>
 * @date(format)
 * 
 */
class StatItemCheckController extends CronController{
	
	private $itemcheck_result;
	private $m;
	private $Host = 'smtp.163.com'; //smtp服务器的名称
  	private $Port = 25;
  	private $SMTPAuth = TRUE; //启用smtp认证
  	private $Username = 'wyandmzy2@163.com'; //你的邮箱名
  	private $Password = 'wy198973' ; //邮箱密码(邮箱授权码)
  	private $From = 'wyandmzy2@163.com'; //发件人地址（也就是你的邮箱地址）
  	private $FromName = '系统'; //发件人姓名
    
    /**
     * [初始化]
     */
    public function __construct(){
    	$this->itemcheck_result = new Model('itemcheck_result');
    	$this->m = new Model();
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
    	
      $start_time                     = isset($argv[0]) ? strtotime($argv[0]) : time() - 14400;
      $end_time                       = isset($argv[1]) ? strtotime($argv[1]) : time();
      	
      if ($start_time > $end_time) {
      	die ('start time > end time');
      }
      //执行命令
      $msg = $this->stat($start_time, $end_time,1);//产出
//       $msg = $this->stat($start_time, $end_time,0);//消耗
      echo $msg;
      die();
  }

  /**
   * [统计]
   * 
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @param  [int] $type  [类型] 1产出0消耗
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time,$type) {
  	
  	  $sqlitem = "SELECT bind_item_id AS id FROM ny_item_config";
  	  $itemidAll = $this->m->query($sqlitem);
  	  
  	  $sql = "SELECT * FROM ny_item_check";
      $one = $this->m->query($sql);
      
      
      $server = [];
      if ($one != array()) {
      	 if ($one[0]['server']) {
      	 	$server = explode(",",$one[0]['server']);
      	 }elseif ($one[0]['group']){
      	 	$sql = "SELECT `server_id`, `name`,`group_id` FROM ny_server WHERE `type` = 1 AND group_id IN ({$one[0]['group']}) AND `status` NOT IN (-1, 0) and server_id not like '%9999'  and server_id not like '%9998'   ORDER BY  id ASC ";
      	 	$serverData = $this->m->query($sql);
      	 	foreach ($serverData as $value) {
      	 		$server[] = $value['server_id'];
      	 	}
      	 }
      	 
      	 $white = json_decode($one[0]['white'],true);
      	 
      	 if ($itemidAll != array()) {
      	 	 foreach ($itemidAll as $k=>$v) {
      	 	 	$itemidAll[$k]['count'] = 1000;//道具数量
      	 	 }
      	 }
      	 
      	 $item = $itemidAll;
      	 $gold = json_decode($one[0]['gold'],true);
      	 foreach ($item as $value) {
      	 	 $itemData[] = $value['id'];
      	 }
      	 
      	 $conditions['item_id'] = implode(",",$itemData);
      	 $conditions['start_time'] = $start_time;
      	 $conditions['end_time'] = $end_time;
      	 $conditions['type'] = $type;//1产出0消耗
      	 
      	 //检测道具
      	 
      	 $create_time = time();
      	 
      	 $sql = "DELETE FROM ny_itemcheck_result WHERE create_time = '{$create_time}'";//也可以使用replace into
      	 $this->m->query($sql);
      	 foreach ($server as $key=>$value) {
      	 	$data = [];
      	 	$rs_Item = $this->m->call('ItemCheck' , 'getItem' , array('conditions' => $conditions) , $value);
      	 	
      	 	if ($rs_Item[$value] != array() && $rs_Item) {
      	 		foreach ($rs_Item[$value] as $k=>$v) {
      	 			
      	 			foreach ($item as $kk=>$vv) {
      	 				if ($v['item_id'] == $vv['id']) {
      	 					
      	 					if ($v['item_num'] > $vv['count'] ) {
      	 						$data[$k]['server'] = $value;
      	 						$data[$k]['role_id'] =  $v['role_id'];
      	 						$data[$k]['role_name'] = $v['role_name'];
      	 						$data[$k]['item_id'] = $v['item_id'];
      	 						$data[$k]['count'] = $v['item_num'];
      	 						$data[$k]['get_source'] = $v['get_source'];
      	 						$data[$k]['coin_type'] = 3;//道具
      	 						$data[$k]['coin'] = 0;
      	 						$data[$k]['type'] = $conditions['type'];
      	 						$data[$k]['create_time'] = $create_time;
      	 						$data[$k]['check_count'] = $vv['count'];
      	 					}else {
      	 						unset($rs_Item[$value][$k]);
      	 					}
      	 				}
      	 			}
      	 		   
      	 		}
      	 		
      	 		if ($data != array()) {
      	 			$data = array_values($data);
      	 			foreach ($data as $k=>$v) {
      	 				$data[$k]['dayu_count'] = count($data);
      	 			}
      	 			
      	 			if ($this->itemcheck_result->multiAdd($data)) {
      	 				$msg = $value.',item success'."\n";
      	 			}else {
      	 				$msg = $value.',item error'."\n";
      	 			}
      	 			echo $msg;
      	 		}
      	 		
      	 	}
      	 	
      	 }
      	
      	 //检测元宝
      	 foreach ($server as $key=>$value) {
      	 	$data = [];
      	 	$rs_Gold = $this->m->call('ItemCheck' , 'getGold' , array('conditions' => $conditions) , $value);
      	 	if ($rs_Gold[$value] != array() && $rs_Gold) {
      	 		foreach ($rs_Gold[$value] as $k=>$v) {
      	 			
      	 			if ($v['gold'] > @$gold[0]['gold'] && isset($gold[0]['gold'])) {
      	 				
      	 				$data[$k]['server'] = $value;
      	 				$data[$k]['role_id'] =  $v['role_id'];
      	 				$data[$k]['role_name'] = $v['role_name'];
      	 				$data[$k]['item_id'] = 0;
      	 				$data[$k]['count'] = 0;
      	 				$data[$k]['get_source'] = $v['get_source'];
      	 				$data[$k]['coin_type'] = 1;//元宝
      	 				$data[$k]['coin'] = $v['gold'];
      	 				$data[$k]['type'] = $conditions['type'];
      	 				$data[$k]['create_time'] = $create_time;
      	 				$data[$k]['check_count'] = $gold[0]['gold'];
      	 			}else {
      	 				unset($rs_Gold[$value][$k]);
      	 			}
      	 		}
      	 		
      	 		if ($data != array()) {
      	 			$data = array_values($data);
      	 			foreach ($data as $k=>$v) {
      	 				$data[$k]['dayu_count'] = count($data);
      	 			}
      	 			 
      	 			if ($this->itemcheck_result->multiAdd($data)) {
      	 				$msg = $value.',gold success'."\n";
      	 			}else {
      	 				$msg = $value.',gold error'."\n";
      	 			}
      	 			echo $msg;
      	 		}
      	 	}
      	 }
   
      	 //检测内部元宝
      	 foreach ($server as $key=>$value) {
      	 	$data = [];
      	 	$rs_NeibuGold = $this->m->call('ItemCheck' , 'getNeibuGold' , array('conditions' => $conditions) , $value);
      	 	if ($rs_NeibuGold[$value] != array() && $rs_NeibuGold) {
      	 		foreach ($rs_NeibuGold[$value] as $k=>$v) {
      	 			
      	 			if ($v['gold'] > @$gold[2]['gold'] && isset($gold[2]['gold'])) {
      	 				
      	 				$data[$k]['server'] = $value;
      	 				$data[$k]['role_id'] =  $v['role_id'];
      	 				$data[$k]['role_name'] = $v['role_name'];
      	 				$data[$k]['item_id'] = 0;
      	 				$data[$k]['count'] = 0;
      	 				$data[$k]['get_source'] = $v['get_source'];
      	 				$data[$k]['coin_type'] = 4;//内部元宝
      	 				$data[$k]['coin'] = $v['gold'];
      	 				$data[$k]['type'] = $conditions['type'];
      	 				$data[$k]['create_time'] = $create_time;
      	 				$data[$k]['check_count'] = $gold[2]['gold'];
      	 			}else {
      	 				unset($rs_NeibuGold[$value][$k]);
      	 			}
      	 		}
      	 		
      	 		if ($data != array()) {
      	 			$data = array_values($data);
      	 			foreach ($data as $k=>$v) {
      	 				$data[$k]['dayu_count'] = count($data);
      	 			}
      	 			
      	 			if ($this->itemcheck_result->multiAdd($data)) {
      	 				$msg = $value.',NeibuGold success'."\n";
      	 			}else {
      	 				$msg = $value.',NeibuGold error'."\n";
      	 			}
      	 			echo $msg;
      	 		}
      	 	}
      	 }
      	 
//       	 die;
      	 //检测邦元
      	 foreach ($server as $key=>$value) {
      	 	$data = [];
      	 	$rs_BindGold= $this->m->call('ItemCheck' , 'getBindGold' , array('conditions' => $conditions) , $value);
      	 	
      	 	if ($rs_BindGold[$value] != array() && $rs_BindGold) {
      	 		foreach ($rs_BindGold[$value] as $k=>$v) {
      	 			
      	 			if ($v['bind_gold'] > @$gold[1]['gold'] && isset($gold[1]['gold'])) {
      	 			
      	 				$data[$k]['server'] = $value;
      	 				$data[$k]['role_id'] =  $v['role_id'];
      	 				$data[$k]['role_name'] = $v['role_name'];
      	 				$data[$k]['item_id'] = 0;
      	 				$data[$k]['count'] = 0;
      	 				$data[$k]['get_source'] = $v['get_source'];
      	 				$data[$k]['coin_type'] = 2;//绑元
      	 				$data[$k]['coin'] = $v['bind_gold'];
      	 				$data[$k]['type'] = $conditions['type'];
      	 				$data[$k]['create_time'] = $create_time;
      	 				$data[$k]['check_count'] = $gold[1]['gold'];
      	 			}else {
      	 				unset($rs_BindGold[$value][$k]);
      	 			}
      	 		}
      	 		if ($data != array()) {
      	 			$data = array_values($data);
      	 			foreach ($data as $k=>$v) {
      	 				$data[$k]['dayu_count'] = count($data);
      	 			}
      	 			 
      	 			if ($this->itemcheck_result->multiAdd($data)) {
      	 				$msg = $value.',bindgold success'."\n";
      	 			}else {
      	 				$msg = $value.',bindgold error'."\n";
      	 			}
      	 			echo $msg;
      	 		}
      	 		
      	 	}
      	 	
      	 }
      
      	 $this->postMail($one,$create_time);
      	 echo "\n";
      	 return '--------------success----------------';
      }
  }
  
  public function postMail($one,$create_time){
  	 require(ROOT."includes/class.phpmailer.php");
  	 require(ROOT."includes/class.smtp.php");
  	 
  	 $sql = "SELECT * FROM ny_itemcheck_result WHERE create_time = '{$create_time}' ORDER BY server";
  	 $checkresult = $this->m->query($sql);
  	 $mail1 = explode(",",$one[0]['mail']);
  	 $title = $one[0]['title'];
  	 
  	 
  	 $mail = new PHPMailer(true); //实例化
  	 $mail->SMTPDebug = 1;
  	 $mail->IsSMTP(); // 启用SMTP
  	 $mail->Host = $this->Host; //smtp服务器的名称（这里以QQ邮箱为例）
  	 $mail->Port = $this->Port;
  	 $mail->SMTPAuth = $this->SMTPAuth; //启用smtp认证
  	 $mail->Username = $this->Username; //你的邮箱名
  	 $mail->Password = $this->Password; //邮箱密码(邮箱授权码)
  	 $mail->From = $this->From; //发件人地址（也就是你的邮箱地址）
  	 $mail->FromName = $this->FromName; //发件人姓名
  	 $mail->WordWrap = 50; //设置每行字符长度
  	 $mail->IsHTML(TRUE); // 是否HTML格式邮件
  	 $mail->CharSet= 'utf-8'; //设置邮件编码
  	 $mail->Subject = $title; //邮件主题
  	 $mail->AltBody = "这个是不支持HTML显示的邮件内容"; //邮件正文不支持HTML的备用显示
  	 
  	 
  	 

  	 if ($one[0]['white']) {
  	 	$white = json_decode($one[0]['white'],true);
  	 }
  	 
  	 $coin_type2 = ['1'=>'元宝','2'=>'绑元','4'=>'内部元宝'];
  	 $mailcontent = $contentStr = '';
  	 
  	 //白名单
  	 $roleArr =  $whiteArr = [];
  	 if ($white != array()) {
  	 	foreach ($white as $k=>$v) {
  	 		$roleArr = explode(",",$v['role']);
  	 		foreach ($roleArr as $value) {
  	 			$whiteArr[] = [$v['server'],$value];
  	 		}
  	 	}
  	 }

  	 $sql = "select server_id from ny_server where status = 2 and display = 1 limit 1";
  	 $ser = $this->m->query($sql);
  	 $AllItem = $this->m->call('Gift' , 'getItem' , array() , $ser[0]['server_id']);//全部物品
  	 $goods = $AllItem[$ser[0]['server_id']];
  	 
  	 if ($checkresult != array()) {
  	 	 foreach ($checkresult as $k=>$v) {
  	 	 	//剔除白名单
  	 	 	if ($whiteArr != array()) {
  	 	 		foreach ($whiteArr as $kk=>$vv) {
  	 	 			if ($v['server']== $vv[0] && $v['role_name']== $vv[1]) {
  	 	 				unset($checkresult[$k]);
  	 	 			}
  	 	 		}
  	 	 	}
  	 	 	 
  	 	 }
  	 }
  	 
  	 $checkresult = array_values($checkresult);
  	 if ($checkresult != array()) {
  	 	
  	 	 foreach ($checkresult as $k=>$v) {
  	 	 	$content = $one[0]['content'];
  	 	 	$contentStr = str_replace('{$server}','<span style="color:red;">'.$v['server'].'</span>',$content);
  	 	 	$contentStr = str_replace('{$role_name}','<span style="color:red;">'.$v['role_name'].'</span>',$contentStr);
  	 	 	$contentStr = str_replace('{$time}','<span style="color:red;">'.date("Y-m-d H:i:s",$v['create_time']).'</span>',$contentStr);
  	 	 	if ($v['coin_type'] != 3) {
  	 	 		$contentStr = str_replace('{$type}','<span style="color:red;">'.$coin_type2[$v['coin_type']].'</span>',$contentStr);
  	 	 		$mailcontent .= str_replace('{$count}','<span style="color:red;">'.$v['coin'].'</span>',$contentStr)."<br/>";
  	 	 	}else {
  	 	 		$name = isset($goods[$v['item_id']]) ? $goods[$v['item_id']] : '';
  	 	 		$coin_type  = $v['item_id'] . '/'.$name;
  	 	 		$contentStr = str_replace('{$type}','<span style="color:red;">'.$coin_type.'</span>',$contentStr);
  	 	 		$mailcontent .= str_replace('{$count}','<span style="color:red;">'.$v['count'].'</span>',$contentStr)."<br/>";
  	 	 	}
  	 	 	 
  	 	 }
  	 	
  	 	 $mail->Body = $mailcontent;
  	 	
  	 	 foreach ($mail1 as $to) {
  	 	 	 $mail->AddAddress($to);
  	 	 }
  	 	 $stat =  $mail->Send();
  	 	 echo $stat;
  	 	 file_put_contents('/tmp/statItemCheck.txt', json_encode($mail1).'result:'.$stat.',时间:'.date('Y-m-d H:i:s',$create_time) ."\n\r",FILE_APPEND);
  	 }
  	 
  	
  }
  
}

