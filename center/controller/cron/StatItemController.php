<?php
/** 20180319
* 1. 产生元宝 指产生元宝的数量
* 2. 参与产生数 指参与产生的人数
* 3. 消耗元宝 指消耗元宝的数量
* 4. 参与消耗数 指参与消耗的人数
* 5. 滞留率 = 1-总消耗/总产出 (保留2位小数)
*/
class StatItemController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
  	public function run(array $argv = null){
  		$serList = CronBase::getServerList();

  		if (empty($argv)) {
  			$time = time();
  			$start_time = strtotime(date('Y-m-d'));

  			//当天零点的时候会重跑昨天的数据，
  			if ($time - $start_time < 1200) {
  				$start_time = $start_time - 86400;
  			}

  			$end_time = $start_time;
  		} elseif (!empty($argv[0]) && !isset($argv[1])) {
  			
  			$start_time = strtotime($argv[0]);
  			$end_time = strtotime(date('Y-m-d'));
  		
  		} elseif (!empty($argv[0]) && !empty($argv[1])) {
  			
  			$start_time = strtotime($argv[0]);
  			$end_time = strtotime($argv[1]);
  		}

  		if ($start_time > $end_time) {
  			die ('start time > end time');
  		}

  		$num = ceil(($end_time - $start_time) / 86400) + 1;

  		//执行命令
  		for ($i = 0; $i < $num; $i++) {
  			$start = $start_time + $i * 86400;
  			$end = $start + 86399;
  			
        //并发执行
        $msg = CronBase::runmultiprocess('statItem', 'stat', $serList, $start, $end);
  		}

  		die();
	}

	/**
	 * [统计]
	 * @param  [string] $server_id  [服ID]
	 * @param  [int] $start_time [开始时间]
	 * @param  [int] $end_time   [结束时间]
	 * @return []             [数据库的更新]
	 */
	public function stat($serList, $start_time, $end_time) {
    
    $types  = array(
      1 => 'gold',
      2 => 'bind_gold',
      3 => 'copper',
      5 => 'sorce',
    );
    foreach ($types as $type => $coin) {
      self::statCoin($serList, $start_time, $end_time, $type, $coin);
    }

    self::statGoods($serList, $start_time, $end_time);
  }

  /**
   * 统计货币
   * @param  [type] $serList    [description]
   * @param  [type] $start_time [description]
   * @param  [type] $end_time   [description]
   * @return [type]             [description]
   */
  public function statCoin($serList, $start_time, $end_time, $type, $coin) {
    //消耗
    $useSql = "SELECT COUNT(DISTINCT role.role_id) AS use_role_num, sum(log.`{$coin}`) AS use_num, package ,psId,GROUP_CONCAT(log.`{$coin}`) AS use_coin,GROUP_CONCAT(log.coin_source) AS use_coin_source
                FROM ny_payment AS log
              LEFT JOIN ny_role AS role ON log.role_id = role.role_id 
                WHERE log.`{$coin}`  > 0 AND log.create_time BETWEEN {$start_time} AND {$end_time} AND log.type = 0
              GROUP BY package ,psId ";

    //产出
    $getSql = "SELECT COUNT(DISTINCT role.role_id) AS get_role_num, sum(log.`{$coin}`) AS get_num, package ,psId
                FROM ny_payment AS log
              LEFT JOIN ny_role AS role ON log.role_id = role.role_id 
                WHERE log.`{$coin}`  > 0 AND log.create_time BETWEEN {$start_time} AND {$end_time} AND log.type = 1
              GROUP BY package ,psId ";

    //总消耗
    $totalUseSql = "SELECT sum(log.`{$coin}`) AS total_use, package ,psId
                FROM ny_payment AS log
              LEFT JOIN ny_role AS role ON log.role_id = role.role_id 
                WHERE log.`{$coin}`  > 0 AND log.create_time < {$end_time} AND log.type = 0
              GROUP BY package ,psId";

    //总产出
    $totalGetSql = "SELECT sum(log.`{$coin}`) AS total_get, package ,psId
                FROM ny_payment AS log
              LEFT JOIN ny_role AS role ON log.role_id = role.role_id 
                WHERE log.`{$coin}`  > 0 AND log.create_time < {$end_time} AND log.type = 1
              GROUP BY package ,psId";

    $data = array(
      'use' => $useSql,
      'get' => $getSql,
      'totalUse' => $totalUseSql,
      'totalGet' => $totalGetSql,
      // 'inventory' => $inventorySql,
    );

    $arr  = array('query' => array('data' => $data));
    $servers = array_keys($serList);

    $res  = CronBase::makeHttpParams($servers, $arr);

    $urls = $res['urls'];
    $params = $res['params'];
    $result = Helper::rolling_curl($urls, $params);

    $result = CronBase::getData($result);

    foreach ($result as $server_id => $data) {
      //消耗
      $useRes = CronBase:: makeKey($data['use'] , 'package' ,  'psId');
	    
      //产出 
      $getRes = CronBase:: makeKey($data['get'] ,  'package' ,  'psId');

      //总消耗
      $totalUseRes = CronBase:: makeKey($data['totalUse'] ,  'package' ,  'psId');

      if(!empty($totalUseRes)){

        $rs = CronBase::getPackagepsId($totalUseRes);
        $packageRes = $rs['packageRes'];
        $psIds = $rs['psIds'];
      }else{
          continue;
      }

      //总产出
      $totalGetRes = CronBase:: makeKey($data['totalGet'] ,  'package' ,  'psId');
     
      $insert = CronBase::mergeData($useRes, $getRes, $totalUseRes, $totalGetRes);
      
	    if(!empty($insert)){
        if(!empty($totalUseRes)){

           CronBase::delDataByServer('stat_item', $psIds,$packageRes, $start_time , 'type ='.$type);
        }
        $data = array();
  		  foreach ($insert as $k => $row) {
    			$server = CronBase::serverBypsId($row['psId']);
    			$row['type'] = $type;
    			$row['server'] = !empty($server) ? $server : $server_id;
    			$row['time'] = $start_time;
    			$inventory = $row['total_get'] - $row['total_use'];
    	
    			$row['inventory'] = $inventory > 0 ? $inventory : 0;
    			unset($row['psId']);
          		$data[] = $row;
    		
  		  }

  	    CronBase::insertData('stat_item' , $data);
  		  
	  }
      $msg = PHP_EOL. '服：'. $server_id .'-' . $type .':'. date('y-m-d', $start_time). ' success'.PHP_EOL;
    
      echo $msg;
    }
  } 

  /**
   * 统计物品
   * @param  [type] $serList    [description]
   * @param  [type] $start_time [description]
   * @param  [type] $end_time   [description]
   * @return [type]             [description]
   */
  public function statGoods($serList, $start_time, $end_time) {
     //道具使用消费
      $consumeSql = "SELECT SUM(item_num) AS use_num, COUNT(DISTINCT log.role_id) AS use_role_num, package ,psId ,GROUP_CONCAT(item_id) AS use_item_id,GROUP_CONCAT(source) AS use_source   
                  FROM ny_consume_produce AS log 
                LEFT JOIN ny_role AS role  ON log.role_id = role.role_id 
                  WHERE  log.create_time BETWEEN {$start_time} AND {$end_time} AND log.type = 0 
                GROUP BY package,psId";
 
      //道具生产情况
      $produceSql = "SELECT SUM(item_num) AS get_num, COUNT(DISTINCT log.role_id) AS get_role_num, package ,psId ,GROUP_CONCAT(item_id) AS get_item_id,GROUP_CONCAT(source) AS get_source
                  FROM ny_consume_produce AS log 
                LEFT JOIN ny_role AS role  ON log.role_id = role.role_id 
                  WHERE  log.create_time BETWEEN {$start_time} AND {$end_time} AND log.type = 1 
                GROUP BY package,psId";

      //道具总产出
      $totalProduceSql =  "SELECT SUM(item_num) AS total_get, package,psId FROM ny_consume_produce AS log 
                      LEFT JOIN  ny_role AS role
                        ON log.role_id = role.role_id 
                      WHERE log.type = 1 AND log.create_time < {$end_time} GROUP BY package,psId";

      $totalConsumeSql = "SELECT SUM(item_num) AS total_use, package ,psId FROM ny_consume_produce AS log 
                      LEFT JOIN  ny_role AS role
                        ON log.role_id = role.role_id 
                      WHERE log.type = 0 AND log.create_time < {$end_time} GROUP BY package,psId";

      $data = array(
        'use' => $consumeSql,
        'get' => $produceSql,

        'totalUse' => $totalConsumeSql,
        'totalGet' => $totalProduceSql,
      );

      $arr  = array('query' => array('data' => $data));
      
      $servers = array_keys($serList);
      
      $res  = CronBase::makeHttpParams($servers, $arr);

      $urls = $res['urls'];
      $params = $res['params'];
      $result = Helper::rolling_curl($urls, $params);

      $result = CronBase::getData($result);
      
      foreach ($result as $server_id => $data) {
        //消耗
        $useRes = CronBase:: makeKey($data['use'] ,  'package' ,  'psId');
        //产出
        $getRes = CronBase:: makeKey($data['get'] ,  'package' ,  'psId');
        
        
        if(!empty($useRes)){
          $rs = CronBase::getPackagepsId($useRes);
          $packageRes = $rs['packageRes'];
          $psIds= $rs['psIds'];
        }elseif (!empty($getRes)){
        	$rs = CronBase::getPackagepsId($getRes);
        	$packageRes = $rs['packageRes'];
        	$psIds= $rs['psIds'];
        }
       

        //总消耗
        $totalUseRes = CronBase:: makeKey($data['totalUse'] , 'package' ,  'psId');
        //总产出
        $totalGetRes = CronBase:: makeKey($data['totalGet'] , 'package' ,  'psId');

        $insert = CronBase::mergeData($useRes, $getRes, $totalUseRes, $totalGetRes);
        
        $type = 4;
		    if(!empty($insert)){
		          if(!empty($useRes) || !empty($getRes) ){
		            CronBase::delDataByServer('stat_item', $psIds,$packageRes, $start_time , 'type =4');
		          }
				   $data = array();
	    		   foreach ($insert as $k => $row) {
	    			  $server = CronBase::serverBypsId($row['psId']);
	    			  $row['type'] = 4;
	    			  $row['inventory'] = (($row['total_get'] - $row['total_use']) > 0) ? $row['total_get'] - $row['total_use'] : 0;
	    			  $row['server'] = !empty($server) ? $server : $server_id;
	    			  $row['time'] = $start_time;
	    			  unset($row['psId']);
	            	  $data[] = $row;
	    		}
	    		
		        CronBase::insertData('stat_item' , $data);
			    $msg = PHP_EOL. '服：'. $server_id .'-'. $type .':'. date('y-m-d', $start_time). ' success'.PHP_EOL;
		    }

      }
  }

  //end Class

}