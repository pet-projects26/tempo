<?php
/**
 * 等级留存
 * 每天凌晨跑一次
 * @author  wt1ao <[<email address>]>
 * @date(format)
 * 
 */
class StatLevelController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){

        $time = time();
        $start_time = strtotime(date('Y-m-d'));

        //当天零点的时候会重跑昨天的数据，
        if ($time - $start_time < 1200) {
            $start_time = $start_time - 86400;
        }

        $end_time = strtotime(date('Ymd', $start_time)) + 86399;

        //执行命令
        $msg = $this->stat( $start_time, $end_time);
        echo $msg;
        die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time) {
     
  	  $time = date('Y-m-d',$start_time);
      
      $serList = CronBase::getServerList();
      $serList = array_values($serList);
      
      $loginCondition['end_time'] = $end_time;

      $levelModel = new Model('level');
      $levelModel->setNewPrefix('stat_');

      $data = [];
      foreach ($serList as $key=>$value) {
          //当天用户的等级信息
          $rs = $levelModel->call('Roledaily' , 'getLevelDataCron' , array('conditions' => '', 'start_time' => $start_time) , $value['server_id']);

          if (empty($rs[$value['server_id']])) break;

          foreach($rs[$value['server_id']] as $k => $v) {
              $v['server_id'] = $value['server_id'];
              $v['create_time'] = $start_time;

              //查询两天前该等级人数获取滞留人数
              $where = [];
              $where['level::='] = $v['level'];
              $where['create_time::='] = strtotime('-2 day',$start_time);
              $where['server_id::='] = $value['server_id'];

              $stay = $levelModel->getRow('level_count', $where, []);

              $v['stay_num'] = $stay['level_count'];

              array_push($data, $v);
          }
      }

      if ($levelModel->multiAdd($data)) {
      	$msg = 'success';
      }else {
      	$msg = 'error';
      }
      return $msg;
  }
}