<?php
/**
 * 1. 平均登录次数 = 登录次数 / 登录人数 
 * 2. 老玩家数 指当天有登录的非当天注册的人数
 * 3. 活跃人数 指当天登录游戏时长总和大于等于30分钟的人数
 * 4. 忠实人数 指连续三天有登录且三天登录在线时长总和大于等于5小时的人数
 * 
 * 
 */
class StatLoginController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
      $serList = CronBase::getServerList();

     if (empty($argv)) {
        $time = time();
        $start_time = strtotime(date('Y-m-d'));

        //当天零点的时候会重跑昨天的数据，
        if ($time - $start_time < 1200) {
          $start_time = $start_time - 86400;
        }

        $end_time = $start_time;
      } elseif (!empty($argv[0]) && !isset($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime(date('Y-m-d'));
      
      } elseif (!empty($argv[0]) && !empty($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime($argv[1]);
      }

      if ($start_time > $end_time) {
        die ('start time > end time');
      }

      $num = ceil(($end_time - $start_time) / 86400) + 1;

      //执行命令
      for ($i = 0; $i < $num; $i++) {
        $start = $start_time + $i * 86400;
        $end = $start + 86399;
        
        //并发执行
        $msg = CronBase::runmultiprocess('statLogin', 'stat', $serList, $start, $end);
      }

      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($serList, $start_time, $end_time) {
   
      //登录人数
      $loginSql = "SELECT COUNT(DISTINCT login.role_id) AS login_num, COUNT(*) AS login_times, package ,psId FROM ny_role AS role
              LEFT JOIN ny_login AS login ON role.role_id = login.role_id
            WHERE login.start_time BETWEEN {$start_time} AND  {$end_time} GROUP BY package ,psId";


      //老玩家数，当天登陆，当天之前注册的玩家 
      $oldSql = "SELECT COUNT(DISTINCT role.role_id) AS old_num, package ,psId FROM ny_role AS role 
                  LEFT JOIN ny_login AS login 
                ON role.role_id = login.role_id
                  WHERE role.create_time < {$start_time} AND login.start_time BETWEEN {$start_time} AND {$end_time}
                GROUP BY package ,psId";

      //老玩家在线时长 
      $oldOnlineSql = "SELECT SUM(end_time - start_time) AS old_online_time, package ,psId  FROM ny_role AS role 
                  LEFT JOIN ny_login AS login 
                ON role.role_id = login.role_id
                  WHERE role.create_time < {$start_time} AND login.start_time BETWEEN {$start_time} AND {$end_time} 
                AND start_time < end_time
                GROUP BY package ,psId ";

      //活跃人数, 当天登录游戏的时长大于30分钟的才算为活跃
      $sql = "SELECT SUM(end_time - start_time ) AS num , role_id   FROM ny_login 
                WHERE start_time BETWEEN {$start_time}  AND {$end_time} 
              AND end_time > start_time  GROUP BY role_id HAVING num >= 1800 ";

      $activeSql = "SELECT COUNT(DISTINCT tmp.role_id) AS `active_num`,package ,psId FROM ny_role AS role  
                      LEFT JOIN ({$sql}) AS tmp ON role.role_id = tmp.role_id GROUP BY package ,psId  "; 

      //忠实人数，指连续三天有登录且登录在线时长大于等于5小时的人数
      $start_time3 = $start_time - 2 * 86400;


      //在线时长大于5小时的要的玩家
      $time =  5 * 60 * 60;

      $sql = "SELECT role_id FROM ( SELECT role_id, SUM(end_time - start_time) AS time 
                FROM ny_login WHERE start_time BETWEEN {$start_time3} AND {$end_time}
              AND start_time < end_time 
                GROUP BY role_id HAVING `time` >= {$time} ) AS tmp ";

      //查找出三天连接登录的玩家
      $fansUidSql = "SELECT DISTINCT uid
                FROM (SELECT MAX(DATE)-MIN(DATE) LESS,uid
                      FROM (SELECT (DATE-rn) diff, uid, DATE, rn
                            FROM (SELECT @wy:=@wy+1 rn, uid,
                                         DATEDIFF(login_time,'1971-01-01') DATE,login_time
                                  FROM (SELECT FROM_UNIXTIME(`start_time`, '%Y-%m-%d') AS  login_time, role_id AS uid FROM ny_login 
                                        WHERE start_time >= {$start_time3} AND
                                              start_time < {$end_time} AND  role_id IN ({$sql})
                                        GROUP BY role_id, DATE(login_time)
                                        ORDER BY role_id , DATE(login_time)
                                       )X 
                                 )X, (SELECT @yw= 0) AS t 
                           )X
                       GROUP BY diff,uid
                      )X
                WHERE LESS >= 2";


      $fansSql = "SELECT COUNT(*) AS fans_num, package ,psId FROM ny_role WHERE role_id in ($fansUidSql) GROUP BY package,psId ";


      $data = array(
        'login' => $loginSql,
        'old' => $oldSql,
        'active' => $activeSql,
        'fans' => $fansSql,
        'oldOnline' => $oldOnlineSql,
      );

      $arr  = array('query' => array('data' => $data));
      $servers = array_keys($serList);

      $res  = CronBase::makeHttpParams($servers, $arr);

      $urls = $res['urls'];
      $params = $res['params'];
      $result = Helper::rolling_curl($urls, $params);

      $result = CronBase::getData($result);

      foreach ($result as $server_id => $data) {
        //登录人数
        $loginRes = CronBase:: makeKey($data['login'] ,  'package' ,  'psId');
		    
        if(!empty($loginRes)){
          $rs = CronBase::getPackagepsId($loginRes);
          $packageRes = $rs['packageRes'];
          $psIds= $rs['psIds'];
        }else{
          continue;
        }
    		
        //老玩家人数
        $oldRes = CronBase:: makeKey($data['old'] , 'package' ,  'psId');

        //活跃人数
        $activeRes = CronBase:: makeKey($data['active'] ,  'package' ,  'psId');

        //忠实玩家
        $fansRes = CronBase:: makeKey($data['fans'] , 'package' ,  'psId');

        //老玩家充值
        $oldOnline = CronBase:: makeKey($data['oldOnline'] , 'package' ,  'psId');
        
        $insert = CronBase::mergeData($loginRes,  $oldRes, $activeRes, $fansRes, $oldOnline);


        if(!empty($insert)){
          if(!empty($loginRes)){
			       CronBase::delDataByServer('stat_login', $psIds,$packageRes, $start_time);
		      }
          $data = array();
          foreach ($insert as &$row) {
            $server = CronBase::serverBypsId($row['psId']);
            $row['server'] = !empty($server) ? $server : $server_id;
            $row['time'] = $start_time;
            unset($row['psId']);
            //$insertId = $m->add($row);
            $data[] = $row;
          }

          unset($row);
          CronBase::insertData('stat_login' , $data);
        }

        $msg = PHP_EOL. '服：'. $server_id .' '. date('y-m-d', $start_time). ' success'.PHP_EOL;

        echo $msg;
      }
  }
}