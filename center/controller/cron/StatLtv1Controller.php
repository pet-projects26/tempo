<?php
/**
 * 充值等级
 * 新增充值人数：当天新增玩家并在当天进行充值的角色数
 * 充值人数：当天充值的人数
 * 新增充值金额：新增充值人数当天充值的金额
 * 登录ARPU: 充值总额 / 登录人数
 * 新增充值APRU：  新增充值总额 / 充值人数
 * arppu＝充值总额／充值人数
 * 注册ARPU： 充值总额 / 注册人数
 * 
 * @author  lai <[<email address>]>
 * @date(format)
 * 
 */
class StatLtv1Controller extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
  	public function run(array $argv = null){
  		//$serList = CronBase::getServerList();
  		$serList['lianyun0001']['server_id'] = 'lianyun0001';
  		$serList['lianyun0002']['server_id'] = 'lianyun0002';
  		if (empty($argv)) {
  			$time = time();
  			$start_time = strtotime(date('Y-m-d'));

  			//当天零点的时候会重跑昨天的数据，
  			if ($time - $start_time < 1200) {
  				$start_time = $start_time - 86400;
  			}

  			$end_time = $start_time;
  		} elseif (!empty($argv[0]) && !isset($argv[1])) {
  			
  			$start_time = strtotime($argv[0]);
  			$end_time = strtotime(date('Y-m-d'));
  		
  		} elseif (!empty($argv[0]) && !empty($argv[1])) {
  			
  			$start_time = strtotime($argv[0]);
  			$end_time = strtotime($argv[1]);
  		}

  		if ($start_time > $end_time) {
  			die ('start time > end time');
  		}

  		$num = ceil(($end_time - $start_time) / 86400) + 1;

  		//执行命令
  		for ($i = 0; $i < $num; $i++) {
  			$start = $start_time + $i * 86400;
  			$end = $start + 86399;

  			//并发执行
  			$msg = CronBase::runmultiprocess('statLtv', 'stat', $serList, $start, $end);
        //
  		}

  		die();
	}

	/**
	 * [统计]
	 * @param  [string] $server_id  [服ID]
	 * @param  [int] $start_time [开始时间]
	 * @param  [int] $end_time   [结束时间]
	 * @return []             [数据库的更新]
	 */
	public function stat($serList, $start_time, $end_time) {
  
    //注册人数
    $regSql = "SELECT COUNT(role_id) AS num, package , psId FROM ny_role 
            WHERE create_time  BETWEEN {$start_time} AND {$end_time} GROUP BY package , psId ";

    //帐号注册数
    $accountSql = "SELECT COUNT(1) AS num, package , psId
                    FROM  (
                        SELECT * FROM (SELECT * FROM ny_role ORDER BY  create_time ASC ) AS t GROUP BY account  
                    ) AS tmp
                WHERE  `create_time`  BETWEEN  {$start_time} AND {$end_time} GROUP BY package , psId  ";
    
    $data = array(
      'role' => $regSql,
      'account' => $accountSql,
    );

		$arr  = array('query' => array('data' => $data));

    $servers = array_keys($serList);

    $res  = CronBase::makeHttpParams($servers, $arr);

    $urls = $res['urls'];
    $params = $res['params'];
    $result = Helper::rolling_curl($urls, $params);

    $result = CronBase::getData($result);

    //并发请求，获取返回结果
    foreach ($result as $server_id => $data) {
        //角色数
        $role = $data['role'];
        //帐号数
        $account = $data['account'];
		    
        if(!empty($role)){
          $rs = CronBase::getPackagepsId($role);
          $packageRes = $rs['packageRes'];
          $psIds= $rs['psIds'];
          CronBase::delDataByServer('stat_ltv', $psIds,$packageRes, $start_time); 
        }else{
          continue;
        }
  			
  		  $data = array();
        foreach ($role as $row) {
          $server = CronBase::serverBypsId($row['psId']);
          $row['time'] = $start_time;
          $row['type'] = 1;
          $row['server'] = !empty($server) ? $server : $server_id;
          unset($row['psId']);
          //$m->add($row);
          $data[] = $row;
        }
	
        foreach ($account as $key => $row) {
          $server = CronBase::serverBypsId($row['psId']);
          $row['time'] = $start_time;
          $row['type'] = 2;
          $row['server'] = !empty($server) ? $server : $server_id;
          unset($row['psId']);
          $data[] = $row;
        }
        CronBase::insertData('stat_ltv' , $data);
        $msg = PHP_EOL. '服：'. $server_id .' '. date('y-m-d', $start_time). ' success'.PHP_EOL;

        echo $msg;
        //end foreach
    }
    
    self::updateLtv($start_time, $serList);

  //end function 
	}

  /**
   * 更新ltv值 
   * @param int $time 当前统计时间
   *
   */
  private function updateLtv($time, $serList){
      $days = array(
          'one' => 1 , 'two' => 2 , 'three' => 3 , 'four' => 4 , 'five' => 5 , 'six' => 6 ,
          'seven' => 7 , 'fourteen' => 14 , 'thirty' => 30 , 'sixty' => 60 , 'ninety' => 90
      ); //第N天

      $m = new Model();

      foreach($days as $kk => $v){
        $num  = $v - 1;

        $reg_start = $time - $num * 86400; 
        $reg_end  = $reg_start + 86399;

        $charge_start = $reg_start;
        $charge_end = $time + 86399;
        
        //角色
        $roleSql = "SELECT SUM(money) AS money, role.package ,role.psId  FROM ny_order AS charge  
                    LEFT JOIN ny_role AS role ON charge.role_id = role.role_id 
                WHERE role.create_time BETWEEN {$reg_start} AND {$reg_end} 
                    AND charge.create_time BETWEEN {$charge_start} AND {$charge_end} GROUP BY role.package ,role.psId ";

        //帐号
        $accountSql = "SELECT SUM(money) AS money, tmp.package , tmp.psId FROM ny_order AS charge  
                    LEFT JOIN (
                        SELECT * FROM (SELECT * FROM ny_role ORDER BY  create_time ASC ) AS t GROUP BY account  
                    ) AS tmp 
                    ON charge.account = tmp.account 
                WHERE tmp.create_time BETWEEN {$reg_start} AND {$reg_end} 
                    AND charge.create_time BETWEEN {$charge_start} AND {$charge_end} GROUP BY tmp.package, tmp.psId";

        $data = array(
          'role' => $roleSql,
          'account' => $accountSql,
        );

        $arr  = array('query' => array('data' => $data));

        $servers = array_keys($serList);

        $res  = CronBase::makeHttpParams($servers, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);

        $result = CronBase::getData($result);
       
        foreach ($result as $server_id => $data) {
          $role = $data['role'];
          $account = $data['account'];

          foreach ($role as $row) {
            $server = CronBase::serverBypsId($row['psId']);
            $money = doubleval($row['money']);
            $package = $row['package'];

            $col = 'day'.$v;

            $sql = "UPDATE stat_ltv set `{$col}` = '{$money}' WHERE `server` = '{$server}' AND `package` = '{$package}' AND `time` = {$reg_start} AND  `type` = 1";

            $m->query($sql);
          }

          foreach ($account as $row) {
            $server = CronBase::serverBypsId($row['psId']);
            $money = doubleval($row['money']);
            $package = $row['package'];

            $col = 'day'.$v;
            $sql = "UPDATE stat_ltv set `{$col}` = '{$money}' WHERE `server` = '{$server}' AND `package` = '{$package}' AND `time` = {$reg_start} AND  `type` = 2";

            $m->query($sql);
          }

        }

        echo PHP_EOL .'==='. $v . '==='. PHP_EOL;
        sleep(1);
        //end foreach
      }

      return true;
  }
}