<?php
/**
 * 新增玩家(五分钟更新)
 * 只跑一天的数据的 20180309
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class StatNewAccountController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
    	
      $start_time                     = isset($argv[0]) ? strtotime($argv[0]) : strtotime(date('Y-m-d',time()));
      $end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()))+86400;
    	
      if ($start_time > $end_time) {
      	die ('start time > end time');
      }
      //执行命令
      $msg = $this->stat( $start_time, $end_time);
      echo $msg;
      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time) {
     
  	  $time = date('Y-m-d',$start_time);
      $udidSql = "SELECT COUNT(DISTINCT udid) num,`server`
FROM ny_client WHERE create_time >= {$start_time} AND create_time < {$end_time} GROUP BY `server`";
      $m = new Model();
      $udidData = $m->query($udidSql);
      foreach ($udidData as $key=>$val) {
      	$udidData1[$val['server']] = $val;
      }
      
      $serList = CronBase::getServerList();
      $serList = array_values($serList);
      $conditions['start_time'] = $start_time;
      $conditions['end_time'] = $end_time;
      foreach ($serList as $key=>$value) {
      	$rs = $m->call('AccountData' , 'getNewAccount' , array('conditions' => $conditions) , $value['server_id']);
      	
      	$data[$key]['time'] = $time;
      	$data[$key]['server'] = $value['server_id'];
      	$data[$key]['account'] =  $rs[$value['server_id']][1]?$rs[$value['server_id']][1]:0;
      	$data[$key]['udid'] = $udidData1[$value['server_id']]['num']?$udidData1[$value['server_id']]['num']:0;
      	
      }
      $sql = "DELETE FROM ny_new_account WHERE time = '{$time}'";
      $m->query($sql);
      $chargeModel = new Model('new_account');
      if ($chargeModel->multiAdd($data)) {
      	$msg = 'success';
      }else {
      	$msg = 'error';
      }
      return $msg;
  }
}