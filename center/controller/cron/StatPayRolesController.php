<?php
/**
 * 付费率(五分钟更新)
 * 只跑一天的数据的 20180309
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class StatPayRolesController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
    	
      $start_time                     = isset($argv[0]) ? strtotime($argv[0]) : strtotime(date('Y-m-d',time()));
      $end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()))+86400;
    	
      if ($start_time > $end_time) {
      	die ('start time > end time');
      }
      //执行命令
      $msg = $this->stat( $start_time, $end_time);
      echo $msg;
      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time) {
     
  	  $time = date('Y-m-d',$start_time);
      $m = new Model();
      
      $serList = CronBase::getServerList();
      $serList = array_values($serList);
      
      $conditions['start_time'] = $start_time;
      $conditions['end_time'] = $end_time;
      $week['start_time'] = $end_time - 7 * 60 * 60 * 24;//往前7天
      $week['end_time'] = $end_time;
      $month['start_time'] = $end_time - 30 * 60 * 60 * 24;//往前30天
      $month['end_time'] = $end_time;
      
      foreach ($serList as $key=>$value) {
      	//日付费率
      	$rs = $m->call('AccountData' , 'getPayRoles' , array('conditions' => $conditions) , $value['server_id']);
      	$data[$key]['time'] = $time;
      	$data[$key]['server'] = $value['server_id'];
      	$data[$key]['payRoles_1'] =  $rs[$value['server_id']][0]?$rs[$value['server_id']][0]:0;
      	$data[$key]['loginRoles_1'] = $rs[$value['server_id']][1]?$rs[$value['server_id']][1]:0;      	
      	//周付费率
      	$rsWeek = $m->call('AccountData' , 'getPayRoles' , array('conditions' => $week) , $value['server_id']);
      	$data[$key]['payRoles_7'] =  $rsWeek[$value['server_id']][0]?$rsWeek[$value['server_id']][0]:0;
      	$data[$key]['loginRoles_7'] = $rsWeek[$value['server_id']][1]?$rsWeek[$value['server_id']][1]:0;
      	//月付费率
      	$rsMonth = $m->call('AccountData' , 'getPayRoles' , array('conditions' => $month) , $value['server_id']);
      	$data[$key]['payRoles_30'] =  $rsMonth[$value['server_id']][0]?$rsMonth[$value['server_id']][0]:0;
      	$data[$key]['loginRoles_30'] = $rsMonth[$value['server_id']][1]?$rsMonth[$value['server_id']][1]:0;
      }
      
      $sql = "DELETE FROM ny_payroles WHERE time = '{$time}'";//也可以使用replace into
      $m->query($sql);
      $payRolesModel = new Model('payroles');
      if ($payRolesModel->multiAdd($data)) {
      	$msg = 'success';
      }else {
      	$msg = 'error';
      }
      return $msg;
  }
}