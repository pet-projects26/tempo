<?php
/**
 * 留存统计
 * 当天注册在第2天登录算为2留
 * 当天注册在第3天登录算为3留
 * 
 * @author  lai <[<email address>]>
 * @date(format)
 * 
 */
class StatRemainController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }
    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
      file_put_contents('/tmp/roleRemain.txt', date('Y-m-d H:i:s').'run StatRemain'."#\n\r",FILE_APPEND);
      $serList = CronBase::getServerList();
      
      if (empty($argv)) {
        $time = time();
        $start_time = strtotime(date('Y-m-d'));

       //当天零点的时候会重跑昨天的数据，
        if ($time - $start_time < 1200) {
          $start_time = $start_time - 86400;
        }

        $end_time = $start_time;
      } elseif (!empty($argv[0]) && !isset($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime(date('Y-m-d'));
      
      } elseif (!empty($argv[0]) && !empty($argv[1])) {
        
        $start_time = strtotime($argv[0]);
        $end_time = strtotime($argv[1]);
      }

      if ($start_time > $end_time) {
        die ('start time > end time');
      }

      $num = ceil(($end_time - $start_time) / 86400) + 1;

      //执行命令
      for ($i = 0; $i < $num; $i++) {
        $start = $start_time + $i * 86400;
        $end = $start + 86399;

        //单线程执行
          $msg = CronBase::runmultiprocess('statRemain', 'stat', $serList, $start, $end);
        //
      }

      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($serList, $start_time, $end_time) {
    
    //注册人数
    $regSql = "SELECT COUNT(role_id) AS reg_num, package ,psId FROM ny_role 
            WHERE create_time  BETWEEN {$start_time} AND {$end_time} GROUP BY package ,psId ";

    $data = array(
      'role' => $regSql,
    );

    $arr  = array('query' => array('data' => $data));

    $servers = array_keys($serList);

    $res  = CronBase::makeHttpParams($servers, $arr);

    $urls = $res['urls'];
    $params = $res['params'];
    $result = Helper::rolling_curl($urls, $params);

    //并发请求，获取返回结果
    foreach ($result as $server_id => $json) {
        $res = json_decode($json, true);

        if (!is_array($res)) {
          echo PHP_EOL . $server_id . ' == ' .PHP_EOL;

          continue;
        }

        $code = $res['code'];
        if ($code != 1) {
          continue;
        }

        $data = $res['data']['query'];
        //角色数
        $role = $data['role'];

        if(!empty($role)){
          $rs = CronBase::getPackagepsId($role);
          $packageRes = $rs['packageRes'];
          $psIds= $rs['psIds'];
          CronBase::delDataByServer('stat_remain', $psIds,$packageRes, $start_time);   
        }else{
          continue;
        }
    		
        $type = 1;
        //帐号数
        
        $data = array();
        foreach ($role as $row) {
          $server = CronBase::serverBypsId($row['psId']);
          $row['time'] = $start_time;
          $row['server'] = !empty($server) ? $server : $server_id;
          $row['type'] = $type;
          unset($row['psId']);
          //$m->add($row);
          $data[] = $row;
        }
        CronBase::insertData('stat_remain' , $data);
        $msg = PHP_EOL. '服：'. $server_id .' '. date('y-m-d', $start_time). ' success'.PHP_EOL;

        echo $msg;
    //end foreach
    }


    self::updateLogin($start_time, $serList);

  //end function 
  }

  /**
   * 更新ltv值 
   * @param int $time 当前统计时间
   *
   */
  private function updateLogin($time, $serList){
      $days = range(2, 30);
      $m = new Model();

      foreach($days as $kk => $v){
        $num  = $v - 1;

        $reg_start = $time - $num * 86400;  //n天前注册
        $reg_end  = $reg_start + 86399;

        $login_start = $time; //今天登录
        $login_end = $login_start + 86399;
        
        //角色
        $roleSql = "SELECT COUNT(DISTINCT role.role_id ) AS num, role.package ,role.psId FROM ny_role AS role 
                    LEFT JOIN ny_login AS login ON role.role_id = login.role_id 
                WHERE role.create_time BETWEEN {$reg_start} AND {$reg_end} 
                    AND login.start_time BETWEEN {$login_start} AND {$login_end} GROUP BY role.package ,role.psId";

        $data = array(
          'role' => $roleSql,
        );

        $arr  = array('query' => array('data' => $data));

        $servers = array_keys($serList);

        $res  = CronBase::makeHttpParams($servers, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);

        $result = CronBase::getData($result);


        foreach ($result as $server_id => $data) {
          $role = $data['role'];

          foreach ($role as $row) {
            $server = CronBase::serverBypsId($row['psId']);
            $num = intval($row['num']);
            $package = $row['package'];

            $col = 'login_'.$v;

            $sql = "UPDATE stat_remain set `{$col}` = '{$num}' WHERE `server` = '{$server}' AND `package` = '{$package}' AND `time` = {$reg_start} AND  `type` = 1";

            $m->query($sql);
          }
        }

        echo PHP_EOL .'=== day'. $v . '=== update  success '. PHP_EOL;
        sleep(2);
        //end foreach
      }

      return true;
  }
}