<?php

/*说明：
 * 1. 创角数 指当天创建角色成功的数量
 * 2. 创角设备数 指当天创建角色成功的设备的数量
*/

class StatRoleController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {

        $serList = CronBase::getServerList();

        if (empty($argv)) {
            $H = date("H");
            if ($H == 0) {
                $H = 23;
                $day = date("Y-m-d", strtotime("-1 day"));
            } else {

                $day = date("Y-m-d");
            }

            $time = time();


            $start_time = strtotime(date("$day $H:00:00"));

            if ($time - $start_time < 600) {
                $H = $H - 1;
                $start_time = strtotime(date("$day $H:00:00"));
            }

            $end_time = $start_time + 3600;

        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d H:00:00', time())) + 3600;

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]) + 86400;
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }
        $num = ceil(($end_time - $start_time) / 3600);

        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 3600;
            $end = $start + 3599;
            //并发执行
            $msg = CronBase::runmultiprocess('statRole', 'stat', $serList, $start, $end);
        }
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        //注册人数
        $regSql = "SELECT COUNT(role_id) AS reg_num ,  package ,psId FROM ny_role 
              WHERE create_time  BETWEEN {$start_time} AND {$end_time} GROUP BY package ,psId ";


        $macSql = "SELECT count(mac) as mac_num , package ,psId from 
                (select * from (select  mac , package ,create_time ,psId  from ny_role where mac !=0  order by create_time asc) as tmp group by mac ) as tmp2 
                where create_time  BETWEEN {$start_time} AND {$end_time}  GROUP BY  package ,psId";

        $data = array(
            'reg' => $regSql,
            'mac' => $macSql,
        );

        $arr = array('query' => array('data' => $data));
        $servers = array_keys($serList);

        $res = CronBase::makeHttpParams($servers, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);

        $result = CronBase::getData($result);

        foreach ($result as $server_id => $data) {
            $regRes = array();
            //创角数
            $regRes = CronBase:: makeKey($data['reg'], 'package', 'psId');


            if (!empty($regRes)) {
                $rs = CronBase::getPackagepsId($regRes);
                $packageRes = $rs['packageRes'];
                $psIds = $rs['psIds'];
            } else {
                continue;
            }

            //创角设备数
            $macRes = array();

            $macRes = CronBase:: makeKey($data['mac'], 'package', 'psId');

            $insert = CronBase::mergeData($regRes, $macRes);

            if (!empty($insert)) {

                CronBase::delDataByServer('stat_role', $psIds, $packageRes, $start_time);
                $data = array();
                foreach ($insert as &$row) {
                    $server = CronBase::serverBypsId($row['psId']);
                    $row['server'] = !empty($server) ? $server : $server_id;
                    $row['time'] = $start_time;
                    unset($row['psId']);
                    //$insertId = $m->add($row);
                    $data[] = $row;
                }

                unset($row);
                CronBase::insertData('stat_role', $data);
                $msg = PHP_EOL . '服：' . $server_id . ' ' . date('y-m-d H:i:s', $start_time) . ' success' . PHP_EOL;

                echo $msg;
            }

        }
    }
}

?>