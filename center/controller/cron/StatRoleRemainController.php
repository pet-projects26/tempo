<?php
/**
 * 角色留存(一小时更新) 今天登录的今天的留存会变的
 * 只跑一天的数据的 20180309
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class StatRoleRemainController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
      file_put_contents('/tmp/roleRemain.txt', date('Y-m-d H:i:s').'run StatRoleRemain'."#\n\r",FILE_APPEND);
      $start_time                     = isset($argv[0]) ? strtotime($argv[0]) : strtotime(date('Y-m-d',time()));
      $end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()))+86400;
    	
      if ($start_time > $end_time) {
      	die ('start time > end time');
      }
      //执行命令
      $msg = $this->stat( $start_time, $end_time);
      echo $msg;
      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time) {
     
  	  $end   = $start_time - 86400 * 30;
      $m = new Model();
      $serList = CronBase::getServerList();
      foreach ($serList as $value) {
      	$conditions['start_time'] = $start_time;
      	$conditions['end_time'] = $end_time;
      		//今天新增的角色
      		$rtoday = $m->call('AccountData' , 'getCreateRoleid' , array('conditions' => $conditions) , $value['server_id']);
      		$newRole = $rtoday[$value['server_id']];
      		
//       		file_put_contents('/tmp/roleRemain.txt', json_encode($conditions)."#\n\r",FILE_APPEND);
      		//今天的登录角色
      		$rLogin = $m->call('AccountData' , 'getLoginRole' , array('conditions' => $conditions) , $value['server_id']);
      		$LoginRole = $rLogin[$value['server_id']];
      		
      		for ($i = ($start_time - 86400); $i >= $end; $i -= 86400) {
      			
      			//只保留 次留,7留,30留
      			if (!in_array($i,[$start_time - 86400,$start_time - 86400*6,$start_time - 86400*29])) {
      				continue;
      			}
      			
      			//昨天新创建的角色
      			$conditions['start_time'] = $i;
      			$conditions['end_time'] = $i + 86400;
      			$ryestoday = $m->call('AccountData' , 'getCreateRoleid' , array('conditions' => $conditions) , $value['server_id']);
      			$yestodayRole = $ryestoday[$value['server_id']];
      			
      			$date                       = date('Y-m-d', $i);
      			$sql                        = "SELECT * FROM ny_server_remain WHERE  server = '{$value['server_id']}' AND `created` = '{$date}' LIMIT 1 ";
//       			file_put_contents('/tmp/roleRemain.txt', json_encode($sql)."#1\n\r",FILE_APPEND);
      			$remainData                 = $m->query($sql);
      			if ($remainData != array()) {
      				$remainData                 = $remainData[0];
      				$remainData['data']         = json_decode($remainData['data'], true);
      				if (is_array($LoginRole) && is_array($yestodayRole)) {
      					$loginNum                   = count(array_intersect($LoginRole, $yestodayRole));
      				}else {
      					$loginNum = 0;
      				}
      				$remainData['data'][$start_time] = intval($loginNum);
      				$server = $remainData['server'];
      				$role = $remainData['role'];
      				$data = json_encode($remainData['data']);
      				$time = $remainData['created'];
      				$sql                        = "REPLACE INTO ny_server_remain (`server`,`role`,`data`,`created`) VALUE ('{$server}','{$role}','{$data}','{$time}')";
      				
//       				file_put_contents('/tmp/roleRemain.txt', json_encode($sql)."#2\n\r",FILE_APPEND);
      				$m->query($sql);
      			}
      			
      		}
//       		$data = [$value['server_id'], count($newRole),json_encode([]), date('Y-m-d', $start_time)];
      		$server_id = $value['server_id'];
      		$newRoleCount = count($newRole);
      		$data = json_encode([]);
      		$time = date('Y-m-d', $start_time);
      		$sql  = "REPLACE INTO ny_server_remain (`server`,`role`,`data`,`created`) VALUE ('{$server_id}','{$newRoleCount}','{$data}','{$time}')";
//       		file_put_contents('/tmp/roleRemain.txt', json_encode($sql)."#3\n\r",FILE_APPEND);
      		$m->query($sql);
      }
  }
  
  
}