<?php

/*说明：
 * 1. 注册数 指当天创号成功的玩家数量
 * 2. 创角数 指当天创建角色成功的数量
 * 3. 登录数 指当天登录游戏的角色数量
 * 4. 活跃数 指当天登录游戏时长总和大于等于30分钟的角色数量
 * 5. 老角色数 指当天有登录的非当天创建的角色数量
 * 6. 充值人数 指当天充值成功的角色数量
 * 7. 登录付费率 = 充值人数 / 登录数
 * 8. 付费AP = 充值金额 / 充值人数
 * 9. 新充值人数 指当日创建的且当日有进行充值的角色数
 * 10. 新充值金额 指当日创建的且当日有进行充值的角色的充值金额
 * 11. 新增付费率 = 新充值人数 / 创角数
 * 12. 新充值AP = 新充值金额 / 新充值人数
 * 13. 老充值人数 = 指当天充值成功的老角色数
 * 14. 老充值金额 = 指当天充值成功的老角色的总充值金额
 * 15. 老付费率 = 老充值人数 / 老角色数
 * 16. 老充值AP = 老充值金额 / 老充值人数
 * 17. 登录AP = 充值金额 / 登录数
 * 18. 次留和3留 指次日留存率和三日留存率 (之后的7留 ，15留 ，30留同理)
 * 19. 三日留存率 = 当天有登录且在三天前也有登录的角色数量 / 总角色数量
 * 20. 注册AP = 充值金额 / 注册数
 */

class StatSummaryController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        $serList = CronBase::getServerList();

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据，
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;

        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg = CronBase::runmultiprocess('statSummary', 'stat', $serList, $start, $end);
        }

        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        //帐号注册数
        $accountSql = "SELECT COUNT(*) AS acc_reg_num, package ,psId FROM (SELECT * FROM
            (SELECT * FROM ny_role ORDER BY create_time ASC ) 
              AS tmp GROUP BY account) AS tmp2
            WHERE create_time BETWEEN {$start_time}  AND {$end_time} GROUP BY  package ,psId ";


        //充值金额与充值人数，充值次数
        $chargeSql = "SELECT
              SUM(money) AS charge_money,
              COUNT(DISTINCT ord.role_id) AS charge_num,
              COUNT(ord.`id`) AS charge_times,
              package ,psId
            FROM
              ny_order AS `ord` 
              LEFT JOIN ny_role AS role 
                ON ord.`role_id` = role.role_id 
            WHERE ord.`create_time` BETWEEN {$start_time} 
              AND {$end_time} 
            GROUP BY package ,psId ";


        //注册人数
        $regSql = "SELECT COUNT(role_id) AS reg_num, COUNT(DISTINCT account) as reg_no_distinct_num, package ,psId FROM ny_role
              WHERE create_time  BETWEEN {$start_time} AND {$end_time} GROUP BY package ,psId ";

        //登录人数
        $loginSql = "SELECT COUNT(DISTINCT login.role_id) AS login_num, package ,psId FROM ny_role AS role
              LEFT JOIN ny_login AS login ON role.role_id = login.role_id
            WHERE login.start_time BETWEEN {$start_time} AND  {$end_time} GROUP BY package ,psId ";


        //新增充值人数, 新增充值金额
        $newChargeSql = "SELECT COUNT(DISTINCT ord.role_id) AS  new_charge_num, SUM(`money`) AS new_charge_money, package ,psId FROM ny_role AS role
            LEFT JOIN ny_order AS ord 
          ON  role.role_id  = ord.role_id 
            WHERE  role.create_time BETWEEN {$start_time} AND  {$end_time}
          AND ord.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY  package ,psId ";


        //老玩家数，当天登陆，当天之前注册的玩家
        $oldSql = "SELECT COUNT(DISTINCT role.role_id) AS old_num, package ,psId FROM ny_role AS role
                  LEFT JOIN ny_login AS login 
                ON role.role_id = login.role_id
                  WHERE role.create_time < {$start_time} AND login.start_time BETWEEN {$start_time} AND {$end_time}
                GROUP BY package ,psId ";


        //活跃人数, 当天登录游戏的时长大于30分钟的才算为活跃
        $sql = "SELECT SUM(end_time - start_time ) AS num , role_id   FROM ny_login
                WHERE start_time BETWEEN {$start_time}  AND {$end_time} 
              AND end_time > start_time  GROUP BY role_id HAVING num >= 1800 ";

        $activeSql = "SELECT COUNT(DISTINCT tmp.role_id) AS `active_num`, package ,psId FROM ny_role AS role
                      LEFT JOIN ({$sql}) AS tmp ON role.role_id = tmp.role_id group by package ,psId";

        //活跃充值
        $activeChargeSql = "SELECT SUM(money) AS active_charge_money, COUNT(DISTINCT charge.role_id) AS active_charge_num, package ,psId FROM
        ny_order AS charge LEFT JOIN ny_role AS role ON charge.role_id = role.role_id LEFT JOIN ({$sql}) AS tmp ON role.role_id = tmp.role_id
        WHERE charge.create_time > {$start_time} AND charge.create_time < {$end_time}
        GROUP BY role.package ,psId ";

        //老玩充值数
        /*$oldChargeSql = "SELECT SUM(money) AS old_charge_money, COUNT(DISTINCT charge.role_id) AS old_charge_num, package ,psId FROM
                        ny_order AS charge LEFT JOIN ny_role AS role
                      ON charge.role_id = role.role_id
                        INNER JOIN
                      (SELECT * FROM ny_login
                        WHERE start_time > {$start_time} AND start_time < {$end_time}
                        group by role_id
                      ) AS tmp ON tmp.role_id = charge.role_id
                     WHERE charge.create_time > {$start_time} AND charge.create_time < {$end_time}
                      AND role.create_time < {$start_time} GROUP BY role.package ,psId ";*/
        //新版老玩家充值（算入长时间在线充值人数）

        $oldChargeSql = "SELECT SUM(money) AS old_charge_money, COUNT(DISTINCT charge.role_id) AS old_charge_num, package ,psId FROM
                        ny_order AS charge LEFT JOIN ny_role AS role
                      ON charge.role_id = role.role_id
                     WHERE charge.create_time > {$start_time} AND charge.create_time < {$end_time}
                      AND role.create_time < {$start_time} GROUP BY role.package ,psId ";
        //最大在线，平均在线
        $sqlOnline = "SELECT MAX(role_num) AS max_online_num, AVG(role_num) AS avg_online_num , psId
                      FROM ny_online_hook WHERE  create_time > {$start_time} AND create_time < {$end_time}  group by psId";

        $data = array(
            'account' => $accountSql,
            'charge' => $chargeSql,
            'reg' => $regSql,
            'login' => $loginSql,
            'newCharge' => $newChargeSql,
            'old' => $oldSql,
            'active' => $activeSql,
            'oldCharge' => $oldChargeSql,
            'online' => $sqlOnline,
            'activeCharge' => $activeChargeSql,
        );

        $arr = array('query' => array('data' => $data));
        $servers = array_keys($serList);

        $res = CronBase::makeHttpParams($servers, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);

        $result = CronBase::getData($result);

        CronBase::delDataByTime('stat_summary', $start_time);//重跑前 删除当天数据，确保数据正确性（通过时间预防累计）
        foreach ($result as $server_id => $data) {
            //帐号注册数
            $accountRes = CronBase:: makeKey($data['account'], 'package', 'psId');

            //充值人数，充值金额
            $chargeRes = CronBase:: makeKey($data['charge'], 'package', 'psId');

            //注册人数
            $regRes = CronBase:: makeKey($data['reg'], 'package', 'psId');

            //登录人数
            $loginRes = CronBase:: makeKey($data['login'], 'package', 'psId');
            $num = array(
                '1' => array($loginRes, count($loginRes)),
                '2' => array($chargeRes, count($chargeRes)),
                '3' => array($accountRes, count($accountRes)),
                '4' => array($regRes, count($regRes)),
            );
            $len = count($num);
            for ($i = 1; $i < $len; $i++) {
                for ($k = 0; $k < $len - $i; $k++) {
                    if ($num[$i][1] < $num[$k + 1][1]) {
                        $temp = $num[$i];
                        $num[$i] = $num[$i + 1];
                        $num[$i + 1] = $temp;
                    }
                }
            }
            $req = array_shift($num);
            if (!empty($req[0])) {
                $rs = CronBase::getPackagepsId($req[0]);
                $packageRes = $rs['packageRes'];
                $psIds = $rs['psIds'];
            } else {
                continue;
            }

            //新增充值人数
            $newChargeRes = CronBase:: makeKey($data['newCharge'], 'package', 'psId');

            //老玩家人数
            $oldRes = CronBase:: makeKey($data['old'], 'package', 'psId');

            //活跃人数
            $activeRes = CronBase:: makeKey($data['active'], 'package', 'psId');

            //老玩家充值人数
            $oldChargeRes = CronBase:: makeKey($data['oldCharge'], 'package', 'psId');


            //活跃充值
            $activeChargeRes = CronBase:: makeKey($data['activeCharge'], 'package', 'psId');

            //在线, 由于在线不存在包名，所以只记录包号为0
            foreach ($data['online'] as $key => &$row) {
                $row['package'] = 0;
            }

            unset($row);

            $onlineRes = CronBase:: makeKey($data['online'], 'package', 'psId');

            $insert = CronBase::mergeData($accountRes, $chargeRes, $regRes, $loginRes, $newChargeRes, $oldRes, $activeRes, $oldChargeRes, $onlineRes,$activeChargeRes);
            if (!empty($insert)) {
                //CronBase::delDataByServer('stat_summary', $psIds, $packageRes, $start_time);

                $data = array();

                foreach ($insert as $key=>$row) {
                    $server = CronBase::serverBypsId($row['psId']);
                    $package = $row['package'];
                    if(empty($server)){
                        unset($insert[$key]);
                        continue;
                    }
                    $insert[$key]['server'] = $server;
                    $insert[$key]['time'] = $start_time;
                    unset($insert[$key]['psId']);
                    $data[$key] = $insert[$key];
                    //$insertId = $m->add($row);
                }
                //unset($row);
                CronBase::insertData('stat_summary', $data);
            }

            $msg = PHP_EOL . '服：' . $server_id . ' ' . date('y-m-d', $start_time) . ' success' . PHP_EOL;

            echo $msg;
        }
    }
}