<?php
/**
 * 聊天监控(实时更新)
 * 一分钟的数据的 20180517
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class StatTalkController extends CronController{
	
	private $file = "/controller/talk.csv";
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
      $start_time                     = isset($argv[0]) ? strtotime($argv[0]) : time() - 60;
      $end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()))+86400;
    	
      if ($start_time > $end_time) {
      	die ('start time > end time');
      }
      //执行命令
      $msg = $this->stat( $start_time, $end_time);
      echo $msg;
      die();
  }
  

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time, $end_time) {
  	
 
  	$handle = fopen(dirname(dirname(dirname(__FILE__))).$this->file,"r");
  	
  	$csv = array();
  	while (!!$row = fgetcsv($handle)) {
  		foreach($row as $k=>$v){
  			$csv[$k][] = iconv('gbk','utf-8',$v);
  		}
  	}
      $m = new Model();
      
      $serList = CronBase::getServerList();
      $serList = array_values($serList);
     
      
      $conditions['start_time'] = $start_time;
      $conditions['end_time'] = $end_time;
      
      foreach ($serList as $key=>$value) {
      	$rs = $m->call('TalkData' , 'getTalk' , array('conditions' => $conditions) , $value['server_id']);
      	if ($rs[$value['server_id']] && $rs[$value['server_id']] != array()) {
      		foreach ($rs[$value['server_id']] as $k => $v) {
      			if ($csv&$csv[0] != array()) {
      				foreach ($csv[0] as $kk=>$vv) {
      					if (preg_match("/$vv/", $v['content'],$str_Ar)) {
      						$this->ban( $value['server_id'],$v['role_name'],2,0,2);
      					}
      				}
      			}
      		}
      	}
      	
      }
     
  }
  
  public function ban($server,$content,$type,$time,$reason){
  	file_put_contents('/tmp/bantalkrole.txt', '['.date("Y-m-d H:i:s",  time() - 60).']'.$server.'-'.$content."\n\r",FILE_APPEND);
  	$n = new BanModel();
  	$n->banTalk($server, $content, $type, $time, $reason);
  }
}