<?php

class SummaryController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据，
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Summary', 'stat', $serList, $start, $end);

        }
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {
        $start_time_millisecond = $start_time * 1000;
        $end_time_millisecond = $end_time * 1000;

        //帐号注册数
        $accountSql = "SELECT COUNT(*) AS acc_reg_num, package ,psId FROM (SELECT * FROM
            (SELECT * FROM ny_role ORDER BY create_time ASC ) 
              AS tmp GROUP BY account) AS tmp2
            WHERE create_time BETWEEN {$start_time_millisecond}  AND {$end_time_millisecond} GROUP BY  package ,psId ";


        //充值金额与充值人数，充值次数
        $chargeSql = "SELECT
              SUM(money) AS charge_money,
              COUNT(DISTINCT ord.role_id) AS charge_num,
              COUNT(ord.`id`) AS charge_times,
              package ,psId
            FROM
              ny_order AS `ord` 
              LEFT JOIN ny_role AS role 
                ON ord.`role_id` = role.role_id 
            WHERE ord.`create_time` BETWEEN {$start_time} 
              AND {$end_time} 
            GROUP BY package ,psId ";


        //注册人数
        $regSql = "SELECT COUNT(role_id) AS reg_num, COUNT(DISTINCT account) as reg_no_distinct_num, package ,psId FROM ny_role
              WHERE create_time  BETWEEN {$start_time_millisecond} AND {$end_time_millisecond} GROUP BY package ,psId ";

        //登录人数
        $loginSql = "SELECT COUNT(DISTINCT login.role_id) AS login_num, package ,psId FROM ny_role AS role
              LEFT JOIN ny_login AS login ON role.role_id = login.role_id
            WHERE login.start_time BETWEEN {$start_time_millisecond} AND  {$end_time_millisecond} GROUP BY package ,psId ";


        //新增充值人数, 新增充值金额
        $newChargeSql = "SELECT COUNT(DISTINCT ord.role_id) AS  new_charge_num, SUM(`money`) AS new_charge_money, package ,psId FROM ny_role AS role
            LEFT JOIN ny_order AS ord 
          ON  role.role_id  = ord.role_id 
            WHERE  role.create_time BETWEEN {$start_time_millisecond} AND  {$end_time_millisecond}
          AND ord.create_time BETWEEN {$start_time} AND {$end_time} GROUP BY  package ,psId ";


        //老玩家数，当天登陆，当天之前注册的玩家
        $oldSql = "SELECT COUNT(DISTINCT role.role_id) AS old_num, package ,psId FROM ny_role AS role
                  LEFT JOIN ny_login AS login 
                ON role.role_id = login.role_id
                  WHERE role.create_time < {$start_time_millisecond} AND login.start_time BETWEEN {$start_time_millisecond} AND {$end_time_millisecond}
                GROUP BY package ,psId ";

        //老玩充值数
        $oldChargeSql = "SELECT SUM(money) AS old_charge_money, COUNT(DISTINCT charge.role_id) AS old_charge_num, package ,psId FROM
                        ny_order AS charge LEFT JOIN ny_role AS role 
                      ON charge.role_id = role.role_id 
                        INNER JOIN 
                      (SELECT * FROM ny_login 
                        WHERE start_time > {$start_time_millisecond} AND start_time < {$end_time_millisecond}
                        group by role_id
                      ) AS tmp ON tmp.role_id = charge.role_id 
                     WHERE charge.create_time > {$start_time} AND charge.create_time < {$end_time} 
                      AND role.create_time < {$start_time_millisecond} GROUP BY role.package ,psId ";

        //最大在线，平均在线
        $sqlOnline = "SELECT MAX(role_num) AS max_online_num, AVG(role_num) AS avg_online_num , psId
                      FROM ny_online_hook WHERE  create_time > {$start_time_millisecond} AND create_time < {$end_time_millisecond}  group by psId";

        $data = array(
            'account' => $accountSql,
            'charge' => $chargeSql,
            'reg' => $regSql,
            'login' => $loginSql,
            'newCharge' => $newChargeSql,
            'old' => $oldSql,
            // 'active' => $activeSql,
            'oldCharge' => $oldChargeSql,
            'online' => $sqlOnline,
            // 'activeCharge' => $activeChargeSql,
        );

        $arr = array('query' => array('data' => $data));
        $servers = array_keys($serList);

        $res = CronBase::makeHttpParams($servers, $arr);

        $urls = $res['urls'];
        $params = $res['params'];
        $result = Helper::rolling_curl($urls, $params);

        $result = CronBase::getData($result);

        CronBase::delDataByTime('ny_summary', $start_time);//重跑前 删除当天数据，确保数据正确性（通过时间预防累计）
        foreach ($result as $server_id => $data) {

            //帐号注册数
            $accountRes = CronBase:: makeKey($data['account'], 'package', 'psId');

            //充值人数，充值金额
            $chargeRes = CronBase:: makeKey($data['charge'], 'package', 'psId');

            //注册人数
            $regRes = CronBase:: makeKey($data['reg'], 'package', 'psId');

            //登录人数
            $loginRes = CronBase:: makeKey($data['login'], 'package', 'psId');
            /*
            $num = array(
                '1' => array($loginRes, count($loginRes)),
                '2' => array($chargeRes, count($chargeRes)),
                '3' => array($accountRes, count($accountRes)),
                '4' => array($regRes, count($regRes)),
            );
            $len = count($num);
            for ($i = 1; $i < $len; $i++) {
                for ($k = 0; $k < $len - $i; $k++) {
                    if ($num[$i][1] < $num[$k + 1][1]) {
                        $temp = $num[$i];
                        $num[$i] = $num[$i + 1];
                        $num[$i + 1] = $temp;
                    }
                }
            }

            $req = array_shift($num);
            if (!empty($req[0])) {
                $rs = CronBase::getPackagepsId($req[0]);
                $packageRes = $rs['packageRes'];
                $psIds = $rs['psIds'];
            } else {
                continue;
            }*/

            //新增充值人数
            $newChargeRes = CronBase:: makeKey($data['newCharge'], 'package', 'psId');

            //老玩家人数
            $oldRes = CronBase:: makeKey($data['old'], 'package', 'psId');

            //老玩家充值人数
            $oldChargeRes = CronBase:: makeKey($data['oldCharge'], 'package', 'psId');

            //在线, 由于在线不存在包名，所以只记录包号为0
            foreach ($data['online'] as $key => &$row) {
                $row['package'] = 0;
            }

            unset($row);

            $onlineRes = CronBase:: makeKey($data['online'], 'package', 'psId');

            $insert = CronBase::mergeData($accountRes, $chargeRes, $regRes, $loginRes, $newChargeRes, $oldRes, /*$activeRes,*/
                $oldChargeRes, $onlineRes/*,$activeChargeRes*/);
            if (!empty($insert)) {

                $data = array();

                $Node = new NodeModel();

                $psIds = [];
                $packageRes = [];

                foreach ($insert as $v) {
                    $psIds[] = $v['psId'];
                    $packageRes[] = $v['package'];
                }

                //CronBase::delDataByServer('ny_summary', $psIds, $packageRes, $start_time);

                foreach ($insert as $key => $row) {
                    $server = CronBase::serverBypsId($row['psId']);
                    $server = $server ? $server : $server_id;
                    $package = $row['package'];
                    if (empty($server)) {
                        unset($insert[$key]);
                        continue;
                    }

                    $insert[$key]['server'] = $server;
                    $insert[$key]['time'] = $start_time;

                    //计算活跃用户 and 活跃充值
                    $activeData = (new ActiveAccountModel())->getActiveAccount($start_time, $end_time, $server, $package);

                    $insert[$key]['active_num'] = $package ? count($activeData) : 0;
                    $insert[$key]['active_charge_num'] = 0;
                    $insert[$key]['active_charge_money'] = 0;

                    if ($insert[$key]['active_num']) {
                        //获取活跃充值数
                        $activeRes = $Node->call('Charge', 'getChargeDataByDatePackage', ['date' => date('Y-m-d', $start_time), 'package' => $package, 'accounts' => $activeData], $server);
                        $activeRes = $activeRes[$server];

                        $insert[$key]['active_charge_num'] = $activeRes['active_charge_num'] ? $activeRes['active_charge_num'] : 0;
                        $insert[$key]['active_charge_money'] = $activeRes['active_charge_money'] ? $activeRes['active_charge_money'] : 0;
                    }

                    if ($package) {
                        //计算新增用户数 新增活跃跳转
                        $newData = $Node->getNewAccount($start_time, $end_time, $server, $package);
                        $insert[$key]['new_num'] = count($newData);
                    } else {
                        $insert[$key]['new_num'] = 0;
                    }

                    unset($insert[$key]['psId']);
                    $data[$key] = $insert[$key];
                    //$insertId = $m->add($row);
                }
                //unset($row);
                CronBase::insertData('ny_summary', $data);
            }

            $msg = PHP_EOL . '服：' . $server_id . ' ' . date('y-m-d', $start_time) . ' success' . PHP_EOL;

            echo $msg;
        }
    }
}