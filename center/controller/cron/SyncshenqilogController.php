<?php
/**
 * 同步数据到神奇的后台
 *  20180522
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class SyncshenqilogController extends CronController{
	
	private $group_id = "7,13,15,19,20,21,22,23,24,25,26,27,29,30";//渠道组id
	
	private $url = "http://106.14.183.134/data/data/reportMultiterm";
	private $gameid = '180713';
	private $gamekey = 'yhlq_sdu3rr80dp';
	private $num = 500;//一次最多发送500条记录,大于500条需要多次发送
	
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
    	
      $time                     = isset($argv[0]) ? date('Ymd',strtotime($argv[0])): date('Ymd',strtotime('yesterday'));
    	
      //执行命令
      $msg = $this->stat( $time);
      echo $msg;
      die();

  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($time) {
  	  
      $m = new Model();
      $sql = "SELECT `server_id`, `group_id` , `num` FROM ny_server WHERE `type` = 1 AND `status` NOT IN (-1, 0) and server_id not like '%9999'  and server_id not like '%9998'  AND group_id IN ($this->group_id)   ORDER BY  id ASC ";
      $serList = $m->query($sql);
      foreach ($serList as $key=>$value) {
      	  $conditions['time']  = $time;
      	  $conditions['group_id']  = $value['group_id'];
      	  $conditions['num']  = $value['num'];
      	  $rs = $m->call('Shenqilog' , 'postLog' , array('conditions' => $conditions) , $value['server_id']);
      	  if ($rs[$value['server_id']]) {
      	  	  $this->httpPost($rs[$value['server_id']],$value['server_id'],$value['group_id'].'_'.$value['num']);
      	  }
//       	  file_put_contents('/tmp/shenqiresult.txt', '时间:'.date("Y-m-d H:i:s",time()).',结果:'.json_encode($rs).',日期:'.$time."\n\r",FILE_APPEND);
      }
     
      return 'success';
  }
  
  private function httpPost($activeArr = [],$server = '',$areaid = ''){
  	$post['gameid'] = $this->gameid;
  	$post['resin'] = '1.0.0';
  	$post['areaid'] = $areaid;
  	$Arr = array_chunk($activeArr, $this->num, true);
  	foreach ($Arr as $value) {
  		$post['data'] = json_encode(array_values($value));
  		$post['md5'] = md5($this->gamekey.$post['data']);
  		$num = 1;
  		do{
  			$return = Helper::httpRequest($this->url,http_build_query($post));
  			file_put_contents('/tmp/shenqilog.txt', '时间:'.date("Y-m-d H:i:s",time()).',服务器:'.$server.',个数:'.count(array_values($value)).',retrun:'.$return.',num:'.$num."\n",FILE_APPEND);
  			$reJson = json_decode($return,true);
  			if($reJson['code'] === 0){
  				break;//跳出持续监听
  			}
  			sleep(3);
  			$num++;
  		}while($num<=5);
  	}
  	
  	file_put_contents('/tmp/shenqilog.txt', '时间:'.date("Y-m-d H:i:s",time()).',服务器:'.$server.',总的个数:'.count($activeArr)."\n",FILE_APPEND);
  
  }

}