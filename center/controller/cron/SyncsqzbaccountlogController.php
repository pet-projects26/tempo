<?php
/**
 * 同步设备,账号登录数据到神奇的后台
 *  20180522
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class SyncsqzbaccountlogController extends CronController{
	private $url = "http://106.14.183.134/data/data/reportMultiterm";
	private $gameid = '180713';
	private $gamekey = 'yhlq_sdu3rr80dp';
	private $num = 500;//一次最多发送500条记录,大于500条需要多次发送
	private $appid = 123123;
	private $channel = "'ludashianzhuo1','huiyaoanzhuo1','ludashiios1','ludashiios2','ludashiios3','ludashiios4','ludashiios5','huiyaoios1','huiyaoios2','huiyaoios3','huiyaoios4','huiyaoios5','huiyaoios6','huowuios1','huowuios2','huowuios3','huowuios4','huowuios5','huowuios6','huowuios7','juliios1','lingyouios1','zhiyouios1','zhiyouios2','zhiyouios3','zhiyouios4','xuegaoios1','xuegaoios2','xuegaoios3','letangios1','letangios2','letangios3','baoyuad1','baoyuyy1','yuyuead1','yuyueyy1'";
	private $iosChannel = ['ludashiios1','ludashiios2','ludashiios3','ludashiios4','ludashiios5','huiyaoios1','huiyaoios2','huiyaoios3','huiyaoios4','huiyaoios5','huiyaoios6','huiyaoios1','huowuios1','huowuios2','huowuios3','huowuios4','huowuios5','huowuios6','huowuios7','juliios1','lingyouios1','zhiyouios1','zhiyouios2','zhiyouios3','zhiyouios4','xuegaoios1','xuegaoios2','xuegaoios3','letangios1','letangios2','letangios3','baoyuyy1','yuyueyy1'];
	private $andChannel = ['huiyaoanzhuo1','ludashianzhuo1','baoyuad1','yuyuead1'];
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
    	
    	$start_time                     = isset($argv[0]) ? strtotime($argv[0]) : strtotime('yesterday');
    	$end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()));
    	 
    	if ($start_time > $end_time) {
    		die ('start time > end time');
    	}    	
	      //执行命令
	      $msg = $this->stat( $start_time ,$end_time);
	      echo $msg;
	      die();
  }

  /**
   * [统计]
   * @param  [string] $server_id  [服ID]
   * @param  [int] $start_time [开始时间]
   * @param  [int] $end_time   [结束时间]
   * @return []             [数据库的更新]
   */
  public function stat($start_time ,$end_time) {
  	  $time = time();
  	  
      $m = new Model();
      //激活
      $sql = "SELECT * FROM ny_active_device WHERE ts >= '{$start_time}' AND ts < '{$end_time}' AND 
      channel IN ($this->channel) 
       ORDER BY ts ASC";
      $list = $m->query($sql);
      
      if ($list != array()) {
      	 foreach ($list as $k=>$v) {
      	 	
      	 	$metrics = [
	      	 	'fr' => $v['fr'],
	      	 	'brand' => $v['brand'],
	      	 	'mac' => $v['mac'],
	      	 	'ip' => $v['ip'],
	      	 	'net' => $v['net'],
	      	 	'imsi' => $v['imsi'],
	      	 	'os' => $v['platform'] == 1?'android':'ios',
	      	 	'res' => $v['res'],
	      	 	'model' => $v['model'],
      	 	];
      	 	
      	 	$deviceInfo = [
	      	 	'ts' => $v['ts'] * 1000,
	      	 	'deviceId' => $v['deviceId'],
	      	 	'ch' => $v['channel'],
	      	 	'bindIds' => (object)[],
	      	 	'metrics' => $metrics,
      	 	];
      	 	
      	 	 $active = [
      	 	 	'ts' =>  $time * 1000,
      	 	 	'appId' => $this->appid,
      	 	 	'event' => 'activation',
      	 	 	'params' => json_encode((object)[]),
      	 	 	'deviceInfo' => json_encode($deviceInfo),
      	 	 ];
      	 	$activeArr[] =  $this->getString($active);
//       	 	 file_put_contents('/tmp/sqzbaccount.txt', $this->getString($active[$k])."\n",FILE_APPEND);
      	 }
      	 
      	 $this->httpPost($activeArr,'active');
      	 
      }
      //启动
      $sql = "SELECT * FROM ny_startup_device WHERE ts >= '{$start_time}' AND ts < '{$end_time}' AND 
      channel IN ($this->channel) 
      ORDER BY ts ASC";
      $startup = $m->query($sql);
      if ($startup != array()) {
      	 foreach ($startup as $k=>$v) {
      	 	$metrics = [
	      	 	'fr' => $v['fr'],
	      	 	'brand' => $v['brand'],
	      	 	'mac' => $v['mac'],
	      	 	'ip' => $v['ip'],
	      	 	'net' => $v['net'],
	      	 	'imsi' => $v['imsi'],
	      	 	'os' => $v['platform'] == 1?'android':'ios',
	      	 	'res' => $v['res'],
	      	 	'model' => $v['model'],
      	 	];
      	 	
      	 	$deviceInfo = [
	      	 	'ts' => $v['ts'] * 1000,
	      	 	'deviceId' => $v['deviceId'],
	      	 	'ch' => $v['channel'],
	      	 	'bindIds' => (object)[],
	      	 	'metrics' => $metrics,
      	 	];
      	 	
      	 	
      	 	 $startup1 = [
      	 	 	'ts' =>  $time * 1000,
      	 	 	'appId' => $this->appid,
      	 	 	'event' => 'startup',
      	 	 	'params' => json_encode((object)[]),
      	 	 	'deviceInfo' => json_encode($deviceInfo),
      	 	 ];
      	 	 $startupArr[] =  $this->getString($startup1);
//       	 	 file_put_contents('/tmp/sqzbaccount.txt', $this->getString($startupArr[$k])."\n",FILE_APPEND);
      	 }
      	 
      	 $this->httpPost($startupArr,'startup');
      	 
      }
      //创建账号
      $sql = "SELECT * FROM ny_register WHERE create_time >= '{$start_time}' AND create_time < '{$end_time}' AND 
      channel IN ($this->channel) 
      ORDER BY create_time ASC";
      $create = $m->query($sql);
      
      if ($create != array()) {
      	 foreach ($create as $k=>$v) {
      	 	
      	 	if (in_array($v['channel'],$this->andChannel)) {
      	 		$os = 'android';
      	 	}elseif (in_array($v['channel'],$this->iosChannel)){
      	 		$os = 'ios';
      	 	}else {
      	 		$os = '';
      	 	}
      	 	
      	 	$metrics = [
	      	 	'fr' => '',
	      	 	'brand' => '',
	      	 	'mac' => '',
	      	 	'ip' => '',
	      	 	'net' => '',
	      	 	'imsi' => '',
	      	 	'os' => $os,
	      	 	'res' => '',
	      	 	'model' => '',
      	 	];
      	 	list(,$uid) = explode("_", $v['account']);
      	 	$bindIds = [
      	 		'userType' => '',
      	 		'accountId' => $uid,
      	 	];
      	 	
      	 	$deviceInfo = [
	      	 	'ts' => $v['create_time'] * 1000,
	      	 	'deviceId' => '',
	      	 	'ch' => $v['channel'],
	      	 	'bindIds' => $bindIds,
	      	 	'metrics' => $metrics,
      	 	];
      	 	
      	 	
      	 	 $create1 = [
      	 	 	'ts' =>  $time * 1000,
      	 	 	'appId' => $this->appid,
      	 	 	'event' => 'user.create',
      	 	 	'params' => json_encode((object)[]),
      	 	 	'deviceInfo' => json_encode($deviceInfo),
      	 	 ];
      	 	 
      	 	 $createArr[] =  $this->getString($create1);
//       	 	 file_put_contents('/tmp/sqzbaccount.txt', $this->getString($createArr[$k])."\n",FILE_APPEND);
      	 }
      	 
      	 $this->httpPost($createArr,'create');
      	 
      }
      //账号登录
      $sql = "SELECT * FROM log_account WHERE create_time >= '{$start_time}' AND create_time < '{$end_time}' AND 
      channel IN ($this->channel) 
      ORDER BY create_time ASC";
      $online = $m->query($sql);
      
      if ($online != array()) {
      	 foreach ($online as $k=>$v) {
      	 	
      	 	if (in_array($v['channel'],$this->andChannel)) {
      	 		$os = 'android';
      	 	}elseif (in_array($v['channel'],$this->iosChannel)){
      	 		$os = 'ios';
      	 	}else {
      	 		$os = '';
      	 	}
      	 	
      	 	$metrics = [
	      	 	'fr' => '',
	      	 	'brand' => '',
	      	 	'mac' => '',
	      	 	'ip' => $v['ip'],
	      	 	'net' => '',
	      	 	'imsi' => '',
	      	 	'os' => $os,
	      	 	'res' => '',
	      	 	'model' => '',
      	 	];
      	 	list(,$uid) = explode("_", $v['account']);
      	 	$bindIds = [
      	 		'userType' => '',
      	 		'accountId' => $uid,
      	 	];
      	 	
      	 	$deviceInfo = [
	      	 	'ts' => $v['create_time'] * 1000,
	      	 	'deviceId' => '',
	      	 	'ch' => $v['channel'],
	      	 	'bindIds' => $bindIds,
	      	 	'metrics' => $metrics,
      	 	];
      	 	
      	 	
      	 	 $online1 = [
      	 	 	'ts' =>  $time * 1000,
      	 	 	'appId' => $this->appid,
      	 	 	'event' => 'user.online',
      	 	 	'params' => json_encode((object)[]),
      	 	 	'deviceInfo' => json_encode($deviceInfo),
      	 	 ];
      	 	 
      	 	 $onlineArr[] =  $this->getString($online1);

      	 }
      	 
      	 $this->httpPost($onlineArr,'online');
      	 
      }
    
      echo 'success';

     
  }
  
  private function array_unset_tt($arr,$key){
  	//建立一个目标数组
  	$res = array();
  	foreach ($arr as $value) {
  		//查看有没有重复项
  		if(isset($res[$value[$key]])){
  			//有：销毁
  			unset($value[$key]);
  		}else{
  			$res[$value[$key]] = $value;
  		}
  	}
  	return $res;
  }
  
  private function getString($params){
  	
  	foreach ($params as $key=>$item) {
  		$newArr[] = $key.'='.$item;
  	}
  	 
  	$stringA = implode("`", $newArr);
  	return $stringA;
  }
  
  private function httpPost($activeArr = [],$type = ''){
  	$post['gameid'] = $this->gameid;
  	$post['resin'] = '1.0.0';
  	$post['areaid'] = '0';
  	$Arr = array_chunk($activeArr, $this->num, true);
  	foreach ($Arr as $value) {
  		$post['data'] = json_encode(array_values($value));
  		$post['md5'] = md5($this->gamekey.$post['data']);
  		//$return = Helper::httpRequest($this->url,http_build_query($post));
  		$num = 1;
  		do{
  			$return = Helper::httpRequest($this->url,http_build_query($post));
  			file_put_contents('/tmp/sqzbaccount.txt', '时间:'.date("Y-m-d H:i:s",time()).',类型:'.$type.',个数:'.count(array_values($value)).',retrun:'.$return.',num:'.$num."\n",FILE_APPEND);
  			$reJson = json_decode($return,true);
  			if($reJson['code'] === 0){
  				break;//跳出持续监听
  			}
  			sleep(3);
  			$num++;
  		}while($num<=5);
  		
  	}
  	
  	file_put_contents('/tmp/sqzbaccount.txt', '时间:'.date("Y-m-d H:i:s",time()).',类型:'.$type.',总的个数:'.count($activeArr)."\n",FILE_APPEND);
  	
  }
  
 
}