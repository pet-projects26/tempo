<?php
/**
 * 角色留存(一小时更新) 今天登录的今天的留存会变的
 * 只跑一天的数据的 20180309
 * @author  wiiliam <[<email address>]>
 * @date(format)
 * 
 */
class TestController extends CronController{
    
    /**
     * [初始化]
     */
    public function __construct(){
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null){
     
      $start_time                     = isset($argv[0]) ? strtotime($argv[0]) : strtotime(date('Y-m-d',time()));
      $end_time                       = isset($argv[0]) ? strtotime($argv[0])+86400 : strtotime(date('Y-m-d',time()))+86400;
      file_put_contents('/tmp/roleRemain.txt', $start_time."#\n\r",FILE_APPEND);
      file_put_contents('/tmp/roleRemain.txt', $end_time."#\n\r",FILE_APPEND);
     
  }

}