<?php

/**
 * Vip统计
 * @author  wt1ao <[<email address>]>
 * @date(format)
 *
 */
class VipController extends CronController
{

    /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $serList = CronBase::getServerList();

        if (empty($serList)) {
            die("server list is empty");
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('Vip', 'stat', $serList, $start, $end);
        }
        echo $msg;
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     * @return []             [数据库的更新]
     */
    public function stat($serList, $start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);

        $time = strtotime($date);

        $Vip = new VipModel();

        $data = [];

        $serIdList = array_keys($serList);
        //删除今日数据
        CronBase::delDataByServerCreateTime('ny_vip', $serIdList, $time);

        foreach ($serList as $key => $value) {

            $dat = [];

            //获取付费用户
            $payingUserNum = $Vip->call('Charge', 'getPayingUserNum', ['date' => date('Y-m-d H:i:s', $end_time)], $value['server_id']);

            $dat['paying_user_num'] = $payingUserNum[$value['server_id']];

            //获取vip数量
            $vipData = $Vip->call('Role', 'getVipCount', [], $value['server_id']);

            $vipData = $vipData[$value['server_id']];

            $vipMsg = [];

            $vip = CDict::$vip;

            if (!empty($vipData)) {
                foreach ($vipData as $k => $v) {
                    if (array_key_exists($v['vip_level'], $vip)) {
                        $vipMsg[$v['vip_level']] = $v['count'];
                    }
                }
            }

            foreach ($vip as $i => $j) {
                if (!array_key_exists($i, $vipMsg)) {
                    $vipMsg[$i] = 0;
                }
            }

            ksort($vipMsg);

            $dat['vip_msg'] = json_encode(array_combine($vip, $vipMsg));

            $dat['server'] = $value['server_id'];

            $dat['create_time'] = $time;

            array_push($data, $dat);
        }

        if (!empty($data)) {
            if ($Vip->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty';
        }

        return $msg;
    }
}