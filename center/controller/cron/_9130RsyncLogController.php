<?php

/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/2/13
 * Time: 15:39
 */
class _9130RsyncLogController extends CronController
{
     /**
     * [初始化]
     */
    public function __construct()
    {
    }

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {
/*
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $num = ceil(($end_time - $start_time) / 86400) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
            $end = $start + 86399;

            //并发执行
            $msg .= CronBase::runmultiprocess('_9130RsyncLog', 'stat', $start, $end);
        }
        */
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 8400) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time + 86399;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = $start_time + 86399;
        }

        $msg = $this->stat($start_time, $end_time);
        echo $msg;
        die();
    }

    /**
     * [统计]
     * @param  [string] $server_id  [服ID]
     * @param  [int] $start_time [开始时间]
     * @param  [int] $end_time   [结束时间]
     */
    public function stat($start_time, $end_time)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $_9130_channel_game_abbreviation = array(
           // 118 => 'jzxjca',
            99 => 'jzxjc',
        );

        $games = $_9130_channel_game_abbreviation;

        foreach ($games as $game_id => $game) {

            //获取所有包 返回数组 key package value ['package', 'channel']
            $packageList = cronBase::getPackageList($game);

            $packageIdList = array_keys($packageList);

            $tmp = '';
            foreach ($packageIdList as $v) {
                $tmp .= $v . ',';
            }
            $packageIdList = rtrim($tmp, ',');

            $serverList = cronBase::getServerListByPackage($packageIdList, $end_time);

            if (empty($serverList)) die();

            $date = date('Y-m-d', $start_time);

            $nowTime = time();

//            //然后断开客户端连接
//            if (function_exists("fastcgi_finish_request")) {
//                fastcgi_finish_request();//使用CGI，断开与客户端连接，服务器继续执行
//                set_time_limit(2 * 60);//拟定脚本执行时间为5分钟
//            }

            //下面开始获取日志数据
            //日志接入整体说明
            //日志名称	日志用途与说明
            //createrole 	创建角色日志，统计游戏的玩家新增相关数据
            //rolelogin	角色登录日志，统计游戏的玩家活跃、留存相关数据
            //rolelogout	角色登出日志，统计游戏的玩家活跃相关数据
            //levelup	角色升级日志，统计玩家的等级相关数据
            //addcash	充值日志，统计游戏的收入相关数据
            //addyuanbao	货币增加日志，统计游戏的货币产出相关数据
            //costyuanbao	货币消耗日志，统计游戏的货币消耗相关数据
            //shoptrade	商城日志，统计游戏的商城销售相关数据
            //gainitem	物品获得日志，统计游戏的核心物品产出数据
            //loseitem	物品消耗日志，统计游戏的核心物品消耗数据
            //chardata	角色快照日志，统计玩家生存状态相关数据

            $logText = '';

            $Model = new Model();

            $ModelClass = '_9130';

            foreach ($serverList as $key => $row) {
                $serverId = $row['server_id'];
                $packageInfo = $row['packageInfo'];
                //$platform = $row['platform'];
                //$os = $row['os'];

                //创建角色日志
                $createRoleData = $Model->call($ModelClass, 'getCreateRole', ['start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);
                $createRoleData = $createRoleData[$serverId];
                $createRole = '';
                if (!empty($createRoleData)) {
                    //from	服务器id
                    //userid	账号id
                    //roleid	角色id
                    //account	角色名
                    //platform	渠道id
                    //os	平台id
                    //ip	ip地址
                    foreach ($createRoleData as $k => $v) {

                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $createRole .= date('Y-m-d H:i:s', $v['create_time']) . ' createrole'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].'platform:'.$platform.':os='.$os.':ip='.$v['ip'];

                    }
                    unset($k);
                    unset($v);

                    $logText .= $createRole;
                }
                //角色登录登出

                /* 角色基本信息 */
                //from	服务器id
                //userid	账号id
                //roleid	角色id
                //account	角色名
                //lev	级别
                //platform	渠道id
                //mac	设备地址
                //os	平台id
                //totalcash	总充值金额

                //登录字段   角色基本信息 + ip
                $roleLoginData = $Model->call($ModelClass, 'getRoleLogin', ['type' => 1, 'start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $roleLoginData = $roleLoginData[$serverId];
                $roleLogin = '';
                if (!empty($roleLoginData)) {

                    foreach ($roleLoginData as $k => $v) {

                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $totalcash= $v['totalcash'] * 100;

                        $roleLogin .= date('Y-m-d H:i:s', $v['start_time']). ' rolelogin'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':ip='.$v['ip'];
                    }
                    unset($k);
                    unset($v);

                    $logText .= $roleLogin;
                }

                //登出字段   角色基本信息 + time  /在线时长
                $roleLogoutData = $Model->call($ModelClass, 'getRoleLogin', ['type' => 2, 'start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $roleLogoutData = $roleLogoutData[$serverId];

                $roleLogout = '';

                if (!empty($roleLogoutData)) {

                    foreach ($roleLogoutData as $k => $v) {

                        if (!$v['end_time']) $v['end_time'] = $end_time;

                        $v['start_time'] /= 1000;
                        strlen($v['end_time']) == 13 && $v['end_time'] /= 1000;

                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $loginTime = $v['end_time'] - $v['start_time'];
                        $totalcash= $v['totalcash'] * 100;
                        $roleLogout .= date('Y-m-d H:i:s', $v['end_time']). ' rolelogout'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':time='.$loginTime;
                    }
                    unset($k);
                    unset($v);

//                    $roleLogout = self::formatData('rolelogout', $roleLogout, 'end_time');

                    $logText .= $roleLogout;
                }

                //角色升级
                $levelUpData = $Model->call($ModelClass, 'getRoleLevUp', ['start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $levelUpData = $levelUpData[$serverId];
                $levelUp = '';
                if (!empty($levelUpData)) {

                    foreach ($levelUpData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $totalcash= $v['totalcash'] * 100;
                        $levelUp .= date('Y-m-d H:i:s', $v['create_time']). ' levelup'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':viplev='.$v['viplev'];

                    }
                    unset($k);
                    unset($v);

                    //$levelUp = self::formatData('levelup', $levelUp, 'create_time');
                    $logText .= $levelUp;
                }

                //充值日志 addcash  //待确认
                $addCashData = $Model->call($ModelClass, 'getChargeData', ['start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $addCashData = $addCashData[$serverId];

                if (!empty($addCashData)) {

                    $addCash = '';
                    $addYuanBaoTmp = '';
                    $costYuanBaoTmp = '';

                    foreach ($addCashData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        if ($v['yuanbao'] == 0) {
                            $v['yuanbao'] = $v['cash'] * 100; //购买月卡或物品直接RMB*100

                            $totalcash = $v['totalcash'] * 100;
                            $addYuanBaoTmp .= date('Y-m-d H:i:s', $v['create_time']). ' addyuanbao'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':typeinfo=44:yuanbao='.$v['yuanbao'].':type=1:totalnum='.$v['yuanbao'];

                            $totalcash = $v['totalcash'] * 100;
                            $costYuanBaoTmp .= date('Y-m-d H:i:s', $v['create_time']). ' costyuanbao'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':typeinfo=44:itemcount=0:yuanbao='.$v['yuanbao'].':yuanbao1=0:totalnum='.$v['yuanbao'];
                        }


                        $totalcash= $v['totalcash'] * 100;
                        $addCash .= date('Y-m-d H:i:s', $v['create_time']). ' addcash'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':cash='.$v['cash'].':yuanbao='.$v['yuanbao'].':id1='.$v['id1'].':id2='.$v['id2'].':ip='.$v['ip'];
                    }
                    unset($k);
                    unset($v);

                   // $addCash = self::formatData('addcash', $addCash, 'create_time');

                    $logText .= $addCash;
                }

                //货币增加日志
                //typeinfo	本次货币增加的途径
                //yuanbao	本次增加的元宝
                //type	货币类型
                $addYuanBaoData = $Model->call($ModelClass, 'getRoleYuanBao', ['type' => 1, 'start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $addYuanBaoData = $addYuanBaoData[$serverId];
                $addYuanBao = '';
                if (!empty($addYuanBaoData)) {

                    foreach ($addYuanBaoData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $totalcash= $v['totalcash'] * 100;
                        $addYuanBao .= date('Y-m-d H:i:s', $v['create_time']). ' addyuanbao'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':typeinfo='.$v['typeinfo'].':yuanbao='.$v['yuanbao'].':type='.$v['money_type'].':totalnum='.$v['totalnum'];
                        //addyuanbao:from=101:userid=323523:roleid=6b4715084:account=xx:lev=20:platform=25pp:mac=DC2B61739C2:os=1:totalcash=1000:typeinfo=2:yuanbao=100:type=1
                    }
                    unset($k);
                    unset($v);
                    //$addYuanBao = self::formatData('addyuanbao', $addYuanBao, 'create_time');
                    $logText .= $addYuanBao;

                    if ($addYuanBaoTmp != '') {
                        $logText .= $addYuanBaoTmp;
                    }
                }

                //货币消耗日志
                $costYuanBaoData = $Model->call($ModelClass, 'getRoleYuanBao', ['type' => 0, 'start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $costYuanBaoData = $costYuanBaoData[$serverId];
                $costYuanBao = '';

                if (!empty($costYuanBaoData)) {

                    foreach ($costYuanBaoData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }
                        $totalcash = $v['totalcash'] * 100;
                        $costYuanBao .= date('Y-m-d H:i:s', $v['create_time']). ' costyuanbao'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':totalcash='.$totalcash.':typeinfo='.$v['typeinfo'].':itemcount=0:yuanbao='.$v['yuanbao'].':yuanbao1=0:totalnum='.$v['totalnum'];
                    }
                    unset($k);
                    unset($v);

                    $logText .= $costYuanBao;

                    if ($costYuanBaoTmp != '') {
                        $logText .= $costYuanBaoTmp;
                    }
                }

                //商城消费日志
                $shopTradeData = $Model->call($ModelClass, 'getShopTrade', ['start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $shopTradeData = $shopTradeData[$serverId];

                $shopTrade = '';
                if (!empty($shopTradeData)) {

                    foreach ($shopTradeData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $item_count = $v['item_num'] * $v['buy_num'];
                        $shopTrade .= date('Y-m-d H:i:s', $v['create_time']). ' shoptrade'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':item_id='.$v['item_id'].':item_type=0:item_count='.$item_count.':cash_type='.$v['cash_type'].':cash_need='.$v['cash_need'].':order_id=0:cash_need1=0';
                    }
                    unset($k);
                    unset($v);

                    $logText .= $shopTrade;
                }

                //物品产出日志
                //from	同前
                //userid	同前
                //roleid	同前
                //account	同前
                //lev	同前
                //platform	同前
                //mac	同前
                //os		同前
                //itemid	物品id
                //count	物品数量
                //guid	物品唯一号
                //reason	原因
                //hint	预留字段
                //type	类型id
                $gainItemData = $Model->call($ModelClass, 'getRoleGainLoseItemData', ['type' => 1, 'start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $gainItemData = $gainItemData[$serverId];
                $gainItem = '';
                if (!empty($gainItemData)) {
                    foreach ($gainItemData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $gainItem .= date('Y-m-d H:i:s', $v['create_time']). ' gainitem'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':itemid='.$v['itemid'].':count='.$v['count'].':guid=0:reason='.$v['reason'].':type=0';
                    }
                    unset($k);
                    unset($v);

                  //  $gainItem = self::formatData('gainitem', $gainItem, 'create_time');

                    $logText .= $gainItem;
                }
                //物品消耗日志
                $loseItemData = $Model->call($ModelClass, 'getRoleGainLoseItemData', ['type' => 0, 'start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);
                $loseItemData = $loseItemData[$serverId];
                $loseItem = '';
                if (!empty($loseItemData)) {

                    $loseItem = [];

                    foreach ($loseItemData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $loseItem .= date('Y-m-d H:i:s', $v['create_time']). ' loseitem'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':lev='.$v['lev'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':itemid='.$v['itemid'].':count='.$v['count'].':guid=0:reason='.$v['reason'].':type=0';
                    }
                    unset($k);
                    unset($v);

                    //$loseItem = self::formatData('loseitem', $loseItem, 'create_time');

                    $logText .= $loseItem;
                }


                //角色快照日志
                //createtime	角色创建时间
                //lastlogintime	最近登录时间
                //totalonlinetime	总在线时长
                //dayonlinetime	当日在线时长
                //lev	角色等级
                //viplev	角色VIP等级
                //exp	经验数值
                //fight	战斗力数值
                //totalcash	累计付费金额
                //yuanbaoowned	剩余元宝数值
                //jinbiowned	剩余金币数值
                $charData = $Model->call($ModelClass, 'getRoleCharData', ['start_time' => $start_time, 'end_time' => $end_time, 'package' => $packageInfo], $serverId);

                $charData = $charData[$serverId];
                $char = '';
                if (!empty($charData)) {
                    foreach ($charData as $k => $v) {
                        $platform = 0;
                        $os = 0;

                        if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                            $platform = $packageList[$v['package']]['channel'];
                            $os = $packageList[$v['package']]['os'];
                        }

                        $totalcash = $v['totalcash'] * 100;
                        $gainItem .= $date. ' chardata'.':from='.$serverId.':userid='.$v['userid'].':roleid='.$v['roleid'].':account='.$v['account'].':platform='.$platform.':mac='.$v['mac'].':os='.$os.':createtime='.$v['create_time'].':lastlogintime='.$v['lastlogintime'].':totalonlinetime='.$v['totalonlinetime'].':dayonlinetime='.$v['dayonlinetime'].':lev='.$v['lev'].':viplev='.$v['viplev'].':exp=0:fight='.$v['fight'].':totalcash='.$totalcash.':yuanbaoowned='.$v['yuanbaoowned'].':jinbiowned='.$v['jinbiowned'];
                    }
                    unset($k);
                    unset($v);

                   // $char = self::formatData('chardata', $char, 'time');

                    $logText .= $char;
                }

            }

            if ($logText != '') {
                //创建日志文件
                //每天生成一个文件。
                //文件名：项目ID_数据日期（2016-12-11）.txt.gz 项目id 99
                //文件校验：项目ID_数据日期（2016-12-11）.txt.gz.md5
                //备份要求：日志采用rsync方式进行日志传输，上传至指定服务器，凌晨上传生成的前一天日志即可。
                //注意事项：字段值不能包含分隔符:和=

                $dateFormat = date('Y-m-d', $start_time);

                $fileName = $game_id.'_' . $dateFormat . '.txt';

                $fileDir = '/tmp/_9130RsyncLog/' . $fileName;

                file_put_contents($fileDir, $logText);
            }
        }
    }
}