<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/3/20
 * Time: 15:25
 *  9130平台聊天监控
 * 接口说明：游戏方在游戏服务端将聊天消息（世界频道、公会频道、私聊频道等）通过http的方式发送到平台消息收集服务器。请考虑平台服务不可用，接口超时的情况，请设置一个比较短的连接超时时间，避免影响游戏业务。
 * 接口调用：http://msgapi.sh9130.com/
 * 传参方式：GET
 * game    游戏简称，游戏方自定    是    zzcq
 * server    游戏服ID，封号与禁言接口透传此ID    是    1001
 * platform_uid    平台帐号，登录接口时平台方传给游戏的帐号    是    3023112
 * rolename    角色名    是    神奇小子
 * roleid    角色ID，封号与禁言接口透传此ID    是    1000021
 * role_level    角色等级    是    30
 * user_ip    用户IP    是    112.12.1.21
 * content    消息正文    是    聊天内容
 * chat_type    聊天频道    是    1
 * channel_id    渠道id，如悦玩、37    是    32
 * ext    扩展参数    否
 */

class _9130TalkSendController extends CronController
{
    public function run(array $argv = null)
    {

        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d H:i:00', $time));
            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 5) {
                $start_time = $start_time - 60;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d H:i:00'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $num = ceil(($end_time - $start_time) / 60) + 1;
        $msg = '';
        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 60;
            $end = $start + 59;

            //并发执行
            $msg .= CronBase::runmultiprocess('_9130TalkSend', 'stat', $start, $end);
            //$msg .= $this->stat($start, $end);
        }
        die();
    }

    public function stat($start_time, $end_time)
    {

        $_9130_channel_game_abbreviation = array(
            'jzxjca',
            'jzxjc',
        );

        $Model = new TalkModel();
/*
        $isMonth = cronBase::checkIsMonth();

        if ($isMonth) {
            (new SplitSqlUtil('talk'))->checkRenameTable();
        }
*/
        //获取所有9130渠道的服务器
        //获取所有包 返回数组 key package value ['package', 'channel']
        $packageList = cronBase::getPackageList($_9130_channel_game_abbreviation);

        $packageIdList = array_keys($packageList);

        $talkData = $Model->getPackageListTalkData($packageIdList, $start_time, $end_time);

        $talkSendUrl = 'http://msgapi.sh9130.com/';

        foreach ($talkData as $v) {

            $server = CronBase::serverBypsId($v['psId']);

            if (!$server) continue;

            if ($v['package'] && array_key_exists($v['package'], $packageList)) {
                $channel = $packageList[$v['package']]['channel'];
            } else {
                $channel = '9130';
            }

            $getData = [
                'game' => $packageList[$v['package']]['game_abbreviation'] ? $packageList[$v['package']]['game_abbreviation'] : 'jzxjc',
                'server' => $server,
                'platform_uid' => str_replace($channel . '_', '', $v['platform_uid']),
                'rolename' => $v['rolename'],
                'roleid' => $v['roleid'],
                'role_level' => $v['role_level'],
                'user_ip' => $v['user_ip'],
                'content' => $v['content'],
                'chat_type' => $v['chat_type'],
                'channel_id' => $channel,
                'ext' => json_encode(['package' => $v['package']]) //数据里放入包号
            ];
            $sendStr = @http_build_query($getData);
            Helper::log($sendStr, '_9130TalkSend', 'sendTalk', 'server');
            Helper::httpRequest($talkSendUrl, $sendStr, 'get', 1, 1);
        }

    }
}