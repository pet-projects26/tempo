<?php


class _playerCompensateController extends CronController
{

    /**
     * [run description]
     * @param  array|null $argv [description]
     * @return [type]           [description]
     */
    public function run(array $argv = null)
    {

        if (empty($argv)) {
            die('请输入时间');
        }

        if (empty($argv[0]) || empty($argv[1])) {
            die('请输入开始时间与结束时间');
        }

        if (!empty($argv[0]) && !empty($argv[1])) {
            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        //定义补偿档位
        $indexSet = array(
            1 => ['超值月卡', [[94150001, 280], [90140001, 1000]]],
            2 => ['至尊特权', [[94150001, 880], [90140001, 1000]]],
            6 => ['无限手套', [[94150001, 980], [90140001, 1000]]],
            31 => ['心灵原石', [[94150001, 680], [90140001, 1000]]],
            32 => ['时间原石', [[94150001, 680], [90140001, 1000]]],
            33 => ['灵魂原石', [[94150001, 680], [90140001, 1000]]],
            34 => ['空间原石', [[94150001, 680], [90140001, 1000]]],
            35 => ['力量原石', [[94150001, 680], [90140001, 1000]]],
            36 => ['现实原石', [[94150001, 680], [90140001, 1000]]]
        );

        //查询范围时间内的充值订单
        $Order = new OrderModel();

        $conditions = array();
       // $conditions['WHERE']['channel::IN'] = ['fante', '9130'];
        $conditions['WHERE']['channel::IN'] = ['local'];
        $conditions['WHERE']['item_id::IN'] = array_keys($indexSet);
        $conditions['WHERE']['status'] = 1;

        $conditions['WHERE']['create_time::>='] = $start_time;
        $conditions['WHERE']['create_time::<='] = $end_time;

        $fileds = ['channel', 'server', 'item_id', 'role_id'];

        $rs = $Order->getRows($fileds, $conditions['WHERE']);

        if (!empty($rs)) {

            $server_config = (new ServerconfigModel())->getConfig([], ['server_id', 'websocket_host', 'websocket_port']);

            $serverConf = [];

            foreach ($server_config as $s) {
                $serverConf[$s['server_id']] = $s;
            }

            foreach ($rs as $row) {

                $item_name = $indexSet[$row['item_id']][0];
                $items = $indexSet[$row['item_id']][1];

                $title = "购买【".$item_name."】仙玉补偿通知";
                $content = "亲爱的上仙，您之前购买的【".$item_name."】未获得对应仙玉，现给予补偿，并送上薄礼一份，望笑纳~";

                $data = array($items, $title, $content, [$row['role_id']]);

                $info = ['opcode' => Pact::$MailSendOperate, 'str' => $data];

                $server_id = $row['server'];
                $websocket_host = $serverConf[$server_id]['websocket_host'];
                $websocket_port = $serverConf[$server_id]['websocket_port'];

                $rs = $Order->socketCall($websocket_host, $websocket_port, 'sendMail', $info);

                $msg = $row['channel'].'_'.$row['server'].': 订单号:'.$row['order_num'];

                if ($rs === 0) {
                    $msg .= '发送补偿成功';
                } else {
                    $msg .= '发送补偿失败';
                }

                Helper::log($msg, '_playerCompensate', 'run', 'server');
            }
        }
    }
}