#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#9130同步日志
run _9130RsyncLog $1

#文件.txt生成好后压缩成gz格式再 生成 .txt.gz.md5 文件 最后推给服务器
#文件在 /tmp/_9130RsyncLog/99_Y-m-d.txt
#先压缩成gz格式再md5加密
#最后推给 rsync服务器
today='';
if [ -z $1 ] ; then
   today=`date +%Y-%m-%d`;
fi

today=$1;

file="99_${today}.txt";

dir="/tmp/_9130RsyncLog/";

if [ ! -d "$dir" ]; then
    mkdir ${dir};
fi


cd ${dir};

#如果文件不存在
if [ ! -f "$file" ]; then
    echo "file ${file} empty";
    exit 1;
fi

#压缩文件
gzip ${file};
file=${file}.gz;
filemd5=${file}.md5;
#生成MD5校验
md5sum ${file} >${filemd5};

#rsync 推送给服务器
#rsync -avz $file $filemd5 99@up.bi.sh9130.cn::99/ --password-file=/etc/rsync.pass

