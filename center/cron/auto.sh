#!/bin/bash
source /etc/profile

# 定时执行php的脚本
work_dir=$(dirname "$0");


function run()
{


	cd  `dirname $0`

	# 每小时的第N分钟执行php(0-59)
	# 例：run_by_min auto_log_reg_statis.php 3

	# 指定小时、分钟执行php

	#ltv
	#run_by_min statLtv 12 

	
	



	##################################################

	#每小时执行一次
	#run_by_hour statItem 2
}



########## 下面代码不需要改动 ##########

# 所有每分要执行的脚本
function run_by_min
{
	sleep 1
	# 当前分钟数
	min=`date +%M`
	
	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}
	
	#弹出第参数
	shift  
	#弹出参数
	shift 

	# 每t分钟执行一次
	b=$(( $min % $t ))
	
	if [ ${b} -eq 0 ] ; then 
	
		ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
		if [ $? -ne 0 ]; then
			php cron.php $file $@;
		fi
		
		#/usr/local/php/bin/php $file
	fi
}

# 所有每小时要执行的脚本
function run_by_hour
{
	sleep 1
	# 当前小时数
	hour=`date +%H`

	min=`date +%M`

	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}

	shift
	shift

	# 每小时的第2分钟执行
	b=$(( $hour % $t ))
	if [ ${b} -eq 0 -a ${min} -eq 2 ] ; then 
	
		ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
		if [ $? -ne 0 ]; then
			php cron.php $file $@;
		fi
		
		#/usr/local/php/bin/php $file
	fi
}

# 定点定时执行
function run_by_time
{
	sleep 1
	# 当前小时数
	hour=`date +%H`
	# 当前分钟数
	min=`date +%M`

	# 传入的变量 file为要执行的文件 h 为小时 m 分钟
	local file=${1}
	local h=${2}
	local m=${3}

	shift
	shift
	shift

	if [ ${hour} -eq ${h} -a ${min} -eq ${m} ] ; then 
		ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
		if [ $? -ne 0 ]; then
			php cron.php $file $@;
		fi
	fi
}




run
