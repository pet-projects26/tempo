<?php
//计划任务入口文件
//php cron.php foo 会执行 ./controller/cron/FooController.php

//PHP_SAPI != 'cli' && exit();
ini_set('display_errors' , 'On');
error_reporting(E_ALL ^ E_STRICT);
//
/*
$argv[1] = 'NodeListPlan';

$argv[2] = '2019-07-12';
$argv[3] = '20190730';
*/

date_default_timezone_set('Asia/Shanghai');
define('IN_WEB' , true);
define('ROOT' , __DIR__ . '/../');
define('SOCKET', ROOT . '/../socket/');
require_once ROOT . '/includes/db.php';

function autoload($className){
    @require_once "$className.php";
}

spl_autoload_register('autoload');
$dirs = array(
    get_include_path(),
    ROOT . '/controller/cron',
    ROOT . '/model',
    ROOT . '/includes',
    ROOT . '/redis'
);
require_once ROOT . '/cron/cronBase.php';

set_include_path(implode(PATH_SEPARATOR , $dirs));

if (empty($argv[1])) {
    exit('no file to exec');
}

$params = count($argv) > 2 ? array_slice($argv , 2) : null;

$className = ucfirst($argv[1]) . 'Controller';

if (!class_exists($className)) {
	exit("{$className} not exists ");
}

//注册类
$rc = new ReflectionClass($className);

if (!$rc->isSubclassOf('CronController')) {
	exit(' 不是 CronController 的子类');
}

$rc->newInstance()->run($params);

//判断是否base64加密
function is_base64($str)
{
//这里多了个纯字母和纯数字的正则判断
    if (@preg_match('/^[0-9]*$/', $str) || @preg_match('/^[a-zA-Z]*$/', $str)) {
        return false;
    } elseif (is_utf8(base64_decode($str)) && base64_decode($str) != '') {
        return true;
    }
    return false;
}

//判断否为UTF-8编码
function is_utf8($str)
{
    $len = strlen($str);
    for ($i = 0; $i < $len; $i++) {
        $c = ord($str[$i]);
        if ($c > 128) {
            if (($c > 247)) {
                return false;
            } elseif ($c > 239) {
                $bytes = 4;
            } elseif ($c > 223) {
                $bytes = 3;
            } elseif ($c > 191) {
                $bytes = 2;
            } else {
                return false;
            }
            if (($i + $bytes) > $len) {
                return false;
            }
            while ($bytes > 1) {
                $i++;
                $b = ord($str[$i]);
                if ($b < 128 || $b > 191) {
                    return false;
                }
                $bytes--;
            }
        }
    }
    return true;
}
