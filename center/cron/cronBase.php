<?php

/**
 * 用于计划任务，执行相对方法
 */
class CronBase
{
    private static $db;

    /**
     * [获取要执行的服务器列表]
     * @return [type] [description]
     */
    public static function getServerList()
    {
        // 只统计已开启的服，并且撇除9999,9998结尾的ios提审服

        $sql = "SELECT `server_id`, `name`, `open_time`  FROM ny_server WHERE `type` = 1 AND `status` NOT IN (0) and server_id not like '%9999'  and server_id not like '%9998' and review !=1  ORDER BY  id ASC ";

        self::$db = new Model();

        $res = self::$db->query($sql);

        if (empty($res)) {
            return array();
        }

        $tmp = array();

        foreach ($res as $row) {
            $server_id = $row['server_id'];
            $tmp[$server_id] = $row;
        }

        return $tmp;
    }


    public static function getPackageList($channel_game_name)
    {

        if (is_array($channel_game_name) && !empty($channel_game_name)) {
            $tmp = '';
            foreach ($channel_game_name as $v) {
                $tmp .= $v . "','";
            }
            $channel_game_name = rtrim($tmp, "','");
        }

        $sql = "select p.package_id as package, c.channel_id as channel, p.platform as os, c.game_abbreviation as game_abbreviation, c.game_id as game_id from ny_package as p left join ny_channel as c on p.channel_id = c.channel_id where c.`game_abbreviation` in ('" . $channel_game_name . "')";

        if (self::$db == null) {
            self::$db = new Model();
        }

        $rs = self::$db->query($sql);

        $tmp = array();

        foreach ($rs as $row) {
            $package = $row['package'];
            $tmp[$package] = $row;
        }

        return $tmp;
    }

    public static function getServerListByPackage($package, $time)
    {
        if (is_array($package) && !empty($package)) {
            $tmp = '';
            foreach ($package as $v) {
                $tmp .= $v . ',';
            }
            $package = rtrim($tmp, ',');
        }

        $sql = "select s.server_id as server_id, s.name as `server_name`, p.package_id as package from ny_server as s left join ny_package as p on s.group_id = p.group_id where p.package_id in ($package) and s.status not in (0) and s.review != 1 and s.open_time <= ".$time;

        if (self::$db == null) {
            self::$db = new Model();
        }

        $res = self::$db->query($sql);

        if (empty($res)) {
            return array();
        }

        $tmp = array();

        foreach ($res as $row) {
            $server_id = $row['server_id'];
            $tmp[$server_id] = $row;
        }

        foreach ($res as $k => $v) {
            if (array_key_exists($v['server_id'], $tmp)) {
                $tmp[$v['server_id']]['packageInfo'][] = $v['package'];
            }
        }

        return $tmp;
    }

    /**
     * [生成http 相对应的参数]
     * @param  [string]  $server_id  [服务器标识符]
     * @param  array $data [传入相对应的参数]
     * @param  boolean $encryption [是否需要相对应加密]
     * @return [array] 返回加密后的数据
     */
    public static function makeRowHttpParams($server_id, $data = array(), $encryption = true)
    {
        $sql = "SELECT server_id, api_url, mdkey FROM ny_server_config WHERE server_id = '{$server_id}' LIMIT 1";
        self::$db = new Model();

        $row = self::$db->fetchOne($sql);

        if (empty($row)) {
            return array();
        }

        $time = time();
        $server_id = $row['server_id'];

        $url = $row['api_url'];
        $mdkey = $row['mdkey'];
        $url = str_replace('index.php', 'api.php', $url);

        $json = json_encode($data);

        if ($encryption == true) {
            $md5 = Helper::authcode($json, 'ENCODE');
        } else {
            $md5 = $json;
        }

        $post = array('unixtime' => $time, 'data' => $md5);
        $post['sign'] = Helper::checkSign($post, $mdkey);

        return array('urls' => $url, 'params' => $post);
    }

    /**
     * [生成http请求参数]
     * @param  [array | string ] $server_id [description]
     * @return [type]            [description]
     */
    public static function makeHttpParams($servers, $data = array(), $encryption = true)
    {

        if (empty($servers)) {
            return array();
        }

        self::$db = new Model();


        foreach ($servers as &$v) {
            $v = "'{$v}'";
        }

        $str = implode(',', $servers);

        $sql = "SELECT server_id, api_url, mdkey FROM ny_server_config WHERE server_id IN ({$str}) ";
        self::$db = new Model();

        $rows = self::$db->query($sql);

        if (empty($rows)) {
            return array();
        }

        $time = time();

        $params = array();

        $urls = array();

        foreach ($rows as $key => $row) {
            $server_id = $row['server_id'];

            $url = $row['api_url'];
            $mdkey = $row['mdkey'];
            $url = str_replace('index.php', 'api.php', $url);

            $urls[$server_id] = $url;

            $json = json_encode($data);

            if ($encryption == true) {
                $md5 = Helper::authcode($json, 'ENCODE');
            } else {
                $md5 = $json;
            }

            $post = array('unixtime' => $time, 'data' => $md5);

            $post['s'] = $row['server_id'];

            $post['sign'] = Helper::checkSign($post, $mdkey);

            $params[$server_id] = $post;

        }

        return array('urls' => $urls, 'params' => $params);
    }

    /**
     * [单进程调用方法]
     * @param  [string] $className [类名]
     * @param  [string] $method    [方法]
     * @return [type]            [description]
     */
    public static function runmultiprocess($className, $method)
    {
        $className = ucfirst($className);
        $className = $className . 'Controller';

        $args = func_get_args(); //可变参数
        $args = array_slice($args, 2);

        return call_user_func_array(array($className, $method), $args);
    }

    /**
     * [返回一个数组，以包名作为key]
     * @param  [array] $arr [数组]
     * @return [type]      [description]
     */
    public static function formatArrByPackage($arr)
    {
        if (empty($arr)) {
            return $arr;
        }

        $tmp = array();

        foreach ($arr as $key => $row) {
            $package = $row['package'];

            $tmp[$package] = $row;
        }

        return $tmp;
    }

    /**
     * 合并两个数组
     *
     * @param array $arr1
     * @param string $key1
     * @param array $arr2
     * @param string $key
     */
    public static function mergeArray($arr1, $arr2, $key = 'package')
    {
        $tmp1 = $tmp2 = array();

        if (empty($arr1) && !empty($arr2)) {
            return $arr2;
        }

        if (!empty($arr1) && empty($arr2)) {
            return $arr1;
        }

        if (empty($arr1) && empty($arr2)) {
            return array();
        }

        $mKey1 = array_keys($arr1);

        $mKey2 = array_keys($arr2);

        $mKey = array_merge($mKey1, $mKey2);

        $mKey = array_unique($mKey);

        //将数组赋值给一个临时变量
        $tmp1 = $arr1;
        $tmp2 = $arr2;

        $def1 = array_shift($tmp1);
        $def2 = array_shift($tmp2);

        foreach ($def1 as $k => &$v) {
            $v = 0;
        }

        unset($v);

        foreach ($def2 as $k => &$v) {
            $v = 0;
        }

        unset($v);

        $arr = array();
        foreach ($mKey as $v) {
            if (isset($arr1[$v]) && isset($arr2[$v])) {
                $arr[$v] = array_merge($arr1[$v], $arr2[$v]);

            } elseif (isset($arr1[$v]) && !isset($arr2[$v])) {

                $arr[$v] = array_merge($def2, $arr1[$v]);

            } elseif (!isset($arr1[$v]) && isset($arr2[$v])) {
                $arr[$v] = array_merge($def1, $arr2[$v]);
            }

        }

        return $arr;
    }

    /**
     * 合并N个数组
     * @return [type] [description]
     */
    public static function mergeData()
    {
        $argv = func_get_args();

        $data = array();
        foreach ($argv as $k => $row) {

            $data = self::mergeArray($data, $row);
        }

        return $data;
    }

    /**
     * [删除数据通过]
     * @param  [type] $table     [description]
     * @param  [type] $server_id [description]
     * @return [type]            [description]
     */
    public static function delDataByServer($table, $psIds, $packageRes, $time, $where = '1')
    {
        self::$db = new Model();

        foreach ($psIds as $value) {

            $server[] = cronBase::serverBypsId($value);
        }

        $server = "'" . implode("','", $server) . "'";

        foreach ($packageRes as $pack) {
            $package[] = $pack;
        }

        $package = "'" . implode("','", $package) . "'";

        $sql = "SELECT id FROM `{$table}`  WHERE {$where} AND `server` IN ($server) AND `package` IN ($package) AND `time` = {$time} ";

        $res = self::$db->query($sql);

        if (empty($res)) {
            return true;
        }

        $ids = array();
        foreach ($res as &$row) {
            $ids[] = $row['id'];
        }

        $str = implode(',', $ids);

        $sql = "DELETE FROM `{$table}` WHERE id IN ({$str}) ";

        return self::$db->query($sql);

    }


    /**
     * [删除数据通过]
     * @param  [type] $table     [description]
     * @param string $start_time
     * @return [type]            [description]
     */
    public static function delDataByTime($table, $start_time)
    {
        self::$db = new Model();

        $sql = "DELETE FROM `{$table}` WHERE `time`={$start_time}";
        $res = self::$db->query($sql);
        return $res;
    }

    /**
     * [删除数据通过]
     * @param  [type] $table     [description]
     * @param string $start_time
     * @return [type]            [description]
     */
    public static function delDataByCreateTime($table, $server, $start_time)
    {
        self::$db = new Model();

        $sql = "DELETE FROM `{$table}` WHERE `server` = '{$server}' AND `create_time`={$start_time}";
        $res = self::$db->query($sql);
        return $res;
    }

    /**
     * 返回CURL的合法参数
     * @param  [Array] $arr [数组]
     * @return [type]      [description]
     */
    public static function getData($arr)
    {
        $tmp = array();
        foreach ($arr as $server_id => $json) {
            $res = json_decode($json, true);

            if (!is_array($res)) {
                echo PHP_EOL . $server_id . ' == ' . PHP_EOL;

                print_r($json);

                echo PHP_EOL;

                continue;
            }

            $code = $res['code'];

            if ($code != 1) {
                continue;
            }

            $data = $res['data']['query'];

            $tmp[$server_id] = $data;
        }

        return $tmp;
    }

    /**
     * 根据key合并两个数组
     *
     * @param array $arr1
     * @param string $key1
     * @param array $arr2
     * @param string $key2
     */
    public static function mergeArrayByKey($arr1, $arr2, $key1 = 'package', $key2 = 'channel')
    {
        $all = array();
        $result = array();
        $rs = array();
        foreach ($arr1 as $v) {
            $all[] = $v;
        }
        foreach ($arr2 as $v) {
            $all[] = $v;
        }

        foreach ($all as $key => $info) {
            $package = $info[$key1];
            $channel = $info[$key2];
            $result[$package][$channel][] = $info;
        }

        foreach ($result as $k => $v) {

            foreach ($v as $kk => $vv) {

                if (isset($result[$k][$kk][0]) && isset($result[$k][$kk][1])) {

                    $rs[] = array_merge($result[$k][$kk][0], $result[$k][$kk][1]);

                } elseif (isset($result[$k][$kk][0])) {

                    $rs[] = $result[$k][$kk][0];

                } else {

                    $rs[] = $result[$k][$kk][1];

                }

            }
        }

        return $rs;

    }

    /**
     * [serverBypsId 把psid转成server_id]
     * @param  [int] $psId [description]
     * @return [string]       [description]
     */
    public static function serverBypsId($psId)
    {
        $channel_num = Pact::channelNum($psId);
        $num = Pact::serverNum($psId);
        $sql = "select server_id from ny_server where channel_num = $channel_num and num = $num ";
        $server = (new Model())->query($sql);
        if (!empty($server)) {
            $server = $server[0]['server_id'];
        } else {
            $server = '';
        }
        return $server;
    }

    /**
     * [mergeDataBykeys 根据两个key合并多个数组]
     * @return [array] [合并完的数组]
     */
    public static function mergeDataBykeys()
    {
        $argv = func_get_args();

        $data = array();
        foreach ($argv as $k => $row) {

            $data = self::mergeArrayByKey($data, $row, 'package', 'psId');
        }

        return $data;
    }

    /**
     * 格式化数据，保证所有的key在一起
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function formatInsertDataAccKeys($data)
    {
        if (empty($data)) {
            return array();
        }

        $res = array();

        foreach ($data as $row) {
            $keys = array_keys($row);
            ksort($keys);

            $k = md5(implode(',', $keys));

            $res[$k][] = $row;
        }

        return $res;
    }

    /**
     * 分批次插入
     * @return [type] [description]
     */
    public static function multInsert($table, $data)
    {
        $tmp = $data;
        $keys = array_keys(array_pop($tmp));

        $len = count($data);

        $length = 1000;

        $times = ceil($len / $length);

        foreach ($keys as &$v) {
            $v = "`{$v}`";
        }
        unset($v);


        $dbh = new Model();

        $insertId = 0;

        //防止单次插入过多，分批次插入
        for ($i = 0; $i < $times; $i++) {
            $sql = "INSERT INTO {$table} (" . implode(',', $keys) . ") values ";

            $offset = ($i * $length);
            $tmp = array_slice($data, $offset, $length);

            $r2 = array();
            foreach ($tmp as $k => $row) {

                foreach ($row as $k2 => &$v2) {
                    $v2 = "'{$v2}'";
                }

                $r2[$k] = "(" . implode(',', $row) . ")";
            }

            $sql .= implode(',', $r2);

            try {
                $dbh->beginTransaction();

                $res = $dbh->query($sql);

                $insertId = $dbh->lastInsertId();

                $dbh->commit();

            } catch (Exception $e) {
                $dbh->rollBack();
            }

        }
        //end for

        return $insertId;

    }

    /**
     * 批量插入
     * @param  [type] $table [description]
     * @param  [type] $data  [description]
     * @return [type]        [description]
     */
    public static function insertData($table, $data)
    {
        if (empty($data)) {
            return $data;
        }

        $data = self::formatInsertDataAccKeys($data);

        foreach ($data as $k => $row) {
            $insertId = self::multInsert($table, $row);
        }

        return true;
    }

    /**
     * [makeKey 构造key]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public static function makeKey($data)
    {
        $args = func_get_args();
        array_shift($args);
        $rs = array();
        foreach ($data as $row) {
            $keys = array();
            foreach ($args as $value) {

                $keys[] = $row[$value];
            }

            $key = implode('-', $keys);

            $rs[$key] = $row;
        }

        return $rs;

    }

    /**
     * [getPackagepsId 把数组转成包号和psId]
     * @param  [type] $array [description]
     * @return [type]        [description]
     */
    public static function getPackagepsId($array)
    {
        $packageRes = array();
        $psIds = array();
        foreach ($array as $key => $value) {
            !in_array($value['package'], $packageRes) && $packageRes[] = $value['package'];
            !in_array($value['psId'], $psIds) && $psIds[] = $value['psId'];
        }
        $packageRes[] = 0;

        return array('packageRes' => $packageRes, 'psIds' => $psIds);
    }

    /**
     * [删除数据通过]
     * @param  [type] $table     [description]
     * @param  [type] $server_id [description]
     * @return [type]            [description]
     */
    public static function delDataByServer1($table, $server_id, $time, $where = '1')
    {
        self::$db = new Model();

        $sql = "SELECT id FROM `{$table}`  WHERE {$where} AND `server` = '{$server_id}' AND `time` = {$time} ";

        $res = self::$db->query($sql);

        if (empty($res)) {
            return true;
        }

        $ids = array();
        foreach ($res as &$row) {
            $ids[] = $row['id'];
        }

        $str = implode(',', $ids);

        $sql = "DELETE FROM `{$table}` WHERE id IN ({$str}) ";

        return self::$db->query($sql);

    }

    /**
     * [删除数据通过]
     * @param  [type] $table     [description]
     * @param  [type] $server_id [description]
     * @return [type]            [description]
     */
    public static function delDataByServerCreateTime($table, $server_id, $time, $where = '1')
    {
        self::$db = new Model();

        if (is_array($server_id)) {
            foreach ($server_id as $value) {

                $server[] = $value;
            }
        } else {
            $server[] = $server_id;
        }

        $server = "'" . implode("','", $server) . "'";

        $sql = "SELECT id FROM `{$table}`  WHERE {$where} AND `server` IN ({$server}) AND `create_time` = {$time} ";

        $res = self::$db->query($sql);

        if (empty($res)) {
            return true;
        }

        $ids = array();
        foreach ($res as &$row) {
            $ids[] = $row['id'];
        }

        $str = implode(',', $ids);

        $sql = "DELETE FROM `{$table}` WHERE id IN ({$str}) ";

        return self::$db->query($sql);

    }

    /***
     * 判断服务器的开服时间
     */
    public static function openTimeForStartTime($open_time, $start_time)
    {
        if (!$open_time) return $start_time;

        if (strtotime(date('Ymd', $open_time)) == strtotime(date('Ymd', $start_time))) {
            return $open_time;
        }

        return $start_time;
    }

    //判断是否为每月的第一分钟
    public static function checkIsMonth()
    {
        $today = date('d');
        $hour = date('H');
        $m = date('i');
        $isMonth = 0;

        if ((int)$today == 1 && (int)$hour == 0 && (int)$m == 1) {
            $isMonth = 1;
        }

        return $isMonth;
    }
}
