#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#重置活跃人数
run activeReset

#副本 / 天关系统 大荒古塔
run zones

#远古符阵
run runeCopy

#商城消费统计
run mall



