#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0")

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#VIP分布
run vip

#每日充值
run dailyrecharge

#数据汇总
run summary

#排行榜
run rankList

#充值档位统计
run chargeIndex

#在线分布
run onlineDistribution
