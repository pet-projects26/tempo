#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");
function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#run registercount;

#run rolecreate;

#玩家留存 20180314
#run StatRoleRemain 

#留存
#run statRemain
#账号留存
#run StatAccountRemain


#run statActivity

#run statItem
#run statLtv

#node节点
run Nodedailyhour

#新手任务留存
#run noviceTask

#新手引导留存
#run guide

#多日留存
run multiday

#充值留存
run rechargeremain

#LTV
run ltvrecord

#等级留存
run levelRemain

#活跃统计
run active


#消费项目统计
run consumer

#每日消费
run dailyconsume


