#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0")

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

# 同步服务器列表到中央服 20180314
#run Mongoserver

# 定时开服
run gameServerOpen

# 自动开服计划
run gameServerAutoOpenPlan

#定时维护
run gameServerEndWeihu
run gameServerStartWeihu

#合服脚本
#run gameServerMerge

#删档检查
#run GameOrder

#定时邮箱
run SendEmail

#定时发送9130聊天内容
run _9130TalkSend