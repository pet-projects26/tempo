source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#设备流失
run deviceloss;

#区服充值汇总
run chargetotal;

#重新跑昨天数据
run ChargeDistribute yesterday
run StatNewAccount yesterday
run StatHyRoles yesterday
run StatPayRoles yesterday
run StatRoleRemain yesterday


#神奇日志
run Syncshenqilog
run Syncsqzbaccountlog