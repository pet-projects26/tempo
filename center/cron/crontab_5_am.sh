#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#单人boss
run singleBoss

#多人boss
run manyBoss

#三界boss
run threerealmsBoss

#boss之家
run bosshome

#每日试炼
run dailytrial

#多人副本 / 幽冥鬼境
run teamCopy

#天降财宝
run richesCopy

#昆仑瑶池
run swimmingCopy

#云梦秘境
run cloudlandCopy

#护送仙女
run fairyInfo

#九天之巅
run nineCopy

#天梯斗法
run tiantiCopy

#仙府洞天
run xianfuInfo

#奇遇系统
run adventureInfo