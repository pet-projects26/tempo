source /etc/profile
work_dir=$(dirname "$0")

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

# 每日充值统计
run statChargeDaily
	
#数据汇总
run statSummary  

#登录统计
run statLogin 

#创角数统计
run statRole 

# 同步充值分布 20180314
run ChargeDistribute 

# 同步新增玩家数据 20180314
run StatNewAccount 

# 同步活跃玩家数据 20180314
run StatHyRoles 

# 同步付费率 20180314
run StatPayRoles 

# 注册数达到一定数目自动开下一个服 20180601
#run Openserver

