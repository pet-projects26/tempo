#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");
function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}
#活跃统计
run active $1 $2

#在线分布
run onlineDistribution $1 $2

#多日留存
run multiday $1 $2


#充值留存
run rechargeremain $1 $2

#LTV
run ltvrecord $1 $2

#VIP分布
run vip $1 $2

#每日充值
run dailyrecharge $1 $2

#商城消费统计
run mall $1 $2

#每日消费
run dailyconsume $1 $2

#新手任务留存
run noviceTask $1 $2

#新手引导留存
run guide $1 $2

#等级留存
run levelRemain $1 $2

#排行榜
run rankList $1 $2

#充值档位统计
run chargeIndex $1 $2

#数据汇总
run summary $1 $2

#单人boss
run singleBoss $1 $2

#多人boss
run manyBoss $1 $2

#三界boss
run threerealmsBoss $1 $2

#副本 / 天关系统 大荒古塔
run zones $1 $2

#boss之家
run bosshome $1 $2

#每日试炼
run dailytrial $1 $2

#多人副本 / 幽冥鬼境
run teamCopy $1 $2

#远古符阵
run runeCopy $1 $2

#天降财宝
run richesCopy $1 $2

#昆仑瑶池
run swimmingCopy $1 $2

#云梦秘境
run cloudlandCopy $1 $2

#护送仙女
run fairyInfo $1 $2

#九天之巅
run nineCopy $1 $2

#天梯斗法
run tiantiCopy $1 $2

#仙府洞天
run xianfuInfo $1 $2

#奇遇系统
run adventureInfo $1 $2
