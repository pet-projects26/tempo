#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");
function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}
#重置活跃人数
#run activeReset

#活跃统计
#run active

#在线分布
#run onlineDistribution

#多日留存
#run multiday

#充值留存
#run rechargeremain

#LTV
#run ltvrecord $1

#VIP分布
#run vip

#每日充值
#run dailyrecharge

#商城消费统计
#run mall $1

#每日消费
#run dailyconsume $1

#node节点
#run Nodedailyhour

#单人boss
#run singleBoss $1

#多人boss
#run manyBoss $1

#三界boss
#run threerealmsBoss $1

#副本 / 天关系统 大荒古塔
#run zones $1

#boss之家
#run bosshome $1

#每日试炼
#run dailytrial $1

#多人副本 / 幽冥鬼境
#run teamcopy $1

#远古符阵
#run runecopy $1

#等级留存
#run levelRemain $1

#排行榜
#run rankList $1

#天降财宝
#run richesCopy $1

#昆仑瑶池
#run swimmingCopy $1

#云梦秘境
#run cloudlandCopy $1

#护送仙女
#run fairyInfo $1

#九天之巅
#run nineCopy $1

#天梯斗法
#run tiantiCopy $1

#仙府洞天
#run xianfuInfo $1

#奇遇系统
#run adventureInfo $1

#数据汇总

#转移活跃
run NodeTmp $1 $2