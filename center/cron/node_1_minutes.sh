#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0")

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#活跃玩家出队
run activeAccountListPlan

#node节点出队
run nodeListPlan

