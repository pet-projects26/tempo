<?php
!defined('IN_WEB') && exit('Access Denied');
(defined('NAME_PHP_SAPI') && NAME_PHP_SAPI == 'cli' && substr(php_sapi_name() , 0 , 3) != 'cli') && exit('403');

date_default_timezone_set("Asia/Shanghai");
spl_autoload_register('__autoload');
spl_autoload_register('autoload');
set_error_handler('error_handler');//设置错误处理和异常处理的方法,写入日志
set_exception_handler('exception_handler');
ini_set('mongo.long_as_object' , 1);
session_start();
header('Content-type: text/html;charset=UTF-8');
require_once ROOT . '/includes/Define.php';
require_once ROOT . '/includes/Database.php';
require_once ROOT . '/library/Highchart.php';
require_once ROOT . '/includes/Helper.php';
require_once ROOT . '/library/String.class.php';
require_once ROOT . '/library/Matrix.php';
//require_once ROOT . '/includes/redisServer.php';

function print_page(){
    require_once ROOT . '/includes/Controller.php';
    require_once ROOT . '/includes/Router.php';
}

function _addslashes($string){
    if (is_array($string)) {
        $keys = array_keys($string);
        foreach ($keys as $key) {
            $val = $string[$key];
            unset($string[$key]);
            $string[addslashes($key)] = _addslashes($val);
        }
    }
    else{
        $string = addslashes($string);
    }
    return $string;
}

function __autoload($className){
    $info      = _class_name_info($className);
    $classPath = ROOT . "/{$info['folder']}/{$className}.class.php";
    if (is_file($classPath)) {
        require_once $classPath;
    }
}

function autoload($className){
	
    if(strpos($className , 'PHPExcel_') === 0){
        return;
    }
    if(strpos($className , '\\')){
        return;
    }
    $path = array(
        get_include_path(),
        ROOT . '/model',
        ROOT . '/redis',
        ROOT . '/includes',
        ROOT . '/library/PHPExcel',
        ROOT . '/library/PHPExcel/PHPExcel/IOFactory.php',
        ROOT . '/library'
		
    );
    set_include_path(implode(PATH_SEPARATOR, $path));
    require_once "$className.php";
	
}

function error_handler($errno , $errstr , $errfile , $errline){
    if($errno == E_NOTICE || $errno == E_DEPRECATED){
        return true;
    }
    $errorDir = Util::logDir() . '/error/';
    if(!is_dir($errorDir)){
        mkdir($errorDir);
    }
    $msg  = '[' . date('Y-m-d H:i:s') . '] ' . "$errfile line $errline: $errstr" . PHP_EOL;
    $file = $errorDir . date('Ym') . '.log';
    file_put_contents($file, $msg, FILE_APPEND);
    return is_dev() ? false : true;
}

function exception_handler(Exception $e){
    $msg    = $e->getMessage();
    $output = 'Exception occured';
    echo is_dev() ? $output . ": $msg\n" : $output;
    $trace = $e->getTraceAsString();
    $msg   = '[' . date('Y-m-d H:i:s') . "] Exception: {$msg}\nStack trace:\n$trace\n";
    $errorDir = Util::logDir() . '/error/';
    if(!is_dir($errorDir)){
        mkdir($errorDir);
    }
    $file = $errorDir . date('Ym') . '.log';
    file_put_contents($file , $msg , FILE_APPEND);
}

function write_error_log($message, $type = 'error'){
    $time    = date('Y-m-d H:i:s');
    $message = "[ {$time} ] {$message}" . PHP_EOL;
    $dir = LOG_DIR . $type;
    if(!file_exists($dir)){
        xmkdir($dir);
    }
    $file = $dir . '/' . date("Y-m") . '.center.log';
    error_log($message, 3, $file);
}

function _error_levels(){
    $types = array(E_ERROR => array('Error'), E_WARNING           => array('Warning'),
        E_PARSE                => array('Parse error'), E_NOTICE      => array('Notice'),
        E_CORE_ERROR           => array('Core error'), E_CORE_WARNING => array('Core warning'),
        E_COMPILE_ERROR        => array('Compile error'),
        E_COMPILE_WARNING      => array('Compile warning'),
        E_USER_ERROR           => array('User error'), E_USER_WARNING => array('User warning'),
        E_USER_NOTICE          => array('User notice'), E_STRICT      => array('Strict warning'),
        E_RECOVERABLE_ERROR    => array('Recoverable fatal error'));
    // E_DEPRECATED and E_USER_DEPRECATED were added in PHP 5.3.0.
    if (defined('E_DEPRECATED')) {
        $types[E_DEPRECATED]      = array('Deprecated function');
        $types[E_USER_DEPRECATED] = array('User deprecated function');
    }
    return $types;
}

function _class_name_info($className){
    $strlen  = strlen($className);
    $tmp_arr = $info = array();
    if($strlen > 0){
        $tmp = '';
        for($i = 0; $i < $strlen; $i++){
            $tmpChar = $className[$i];
            if($tmpChar == strtoupper($tmpChar)){
                $tmp && $tmp_arr[] = $tmp;
                $tmp = $tmpChar;
            }
            else{
                $tmp .= $tmpChar;
            }
        }
        $info['folder']   = strtolower($tmp);
        $info['filename'] = $className;
    }
    return $info;
}
/**
 * [random 返回随机的字符串]
 * @param  [type]  $length  [生成长度]
 * @param  integer $numeric []
 * @return [type]           [是否纯数字]
 */
function random($length , $numeric = 0){
    if($numeric){
        $hash = sprintf('%0' . $length . 'd' , mt_rand(0 , pow(10 , $length) - 1));
    }
    else{
        $hash  = '';
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $max   = strlen($chars) - 1;
        for($i = 0; $i < $length; $i++){
            $hash .= $chars[mt_rand(0 , $max)];
        }
    }
    return $hash;
}
/**
 * [check_post 去掉POST数据的空格]
 * @param  array  $fields [description]
 * @return [type]         [description]
 */
function check_post($fields = array()){
    if($fields){
        foreach($fields as $f){
            $value = trim($_POST[$f]);
            if($value == ''){
                return $f;
            }
        }
    }
    return '1';
}

function show_403_page($content = null){
    // header('HTTP/1.1 403 No permission');
    echo $content ? $content : '你没有权限访问该页面';
    exit();
}

function show_404_page($content = null){
//     header('HTTP/1.1 404 Not Found');
    //     header("status: 404 Not Found");
    echo $content ? $content : 'page not found';
    exit();
}

function check_ip($ip){
    if(!preg_match("/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/", $ip)){
        return false;
    }
    $ip_arr = explode('.', $ip);
    foreach($ip_arr as $num){
        if($num > 255){
            return false;
        }
    }
    return true;
}

//获取用户ip
function get_ip(){
    if ($ip = getenv('HTTP_CLIENT_IP'));
    elseif($ip = getenv('HTTP_X_FORWARDED_FOR'));
    elseif($ip = getenv('HTTP_X_FORWARDED'));
    elseif($ip = getenv('HTTP_FORWARDED_FOR'));
    elseif($ip = getenv('HTTP_FORWARDED'));
    else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

//判断用户是否有权限
function has_purview($ctrl, $act){
    $userAction = new UserAction();
    return $userAction->hasPurview($ctrl, $act);
}

function variable_get($name){
    global $variableAction;
    !$variableAction && $variableAction = new VariableAction();
    return $variableAction->variableGet($name);
}

function variable_set($name , $value){
    global $variableAction;
    !$variableAction && $variableAction = new VariableAction();

    return $variableAction->variableSet($name, $value);
}

function variable_delete($name){
    global $variableAction;
    !$variableAction && $variableAction = new VariableAction();

    return $variableAction->variableDelete($name);
}

function notice_set($str){
    global $userAction;
    !$userAction && $userAction = new UserAction();
    $notice                     = $userAction->userSessionGet('notice');
    $notice[]                   = $str;
    $userAction->userSessionSet('notice' , $notice);
}

function notice_get(){
    global $userAction;
    !$userAction && $userAction = new UserAction();
    return array_unique($userAction->userSessionGet('notice'));
}

function check_plain($text){
    static $php525;
    if(!isset($php525)){
        $php525 = version_compare(PHP_VERSION , '5.2.5' , '>=');
    }
    if($php525){
        return htmlspecialchars($text , ENT_QUOTES , 'UTF-8');
    }
    return (preg_match('/^./us' , $text) == 1) ? htmlspecialchars($text , ENT_QUOTES , 'UTF-8') : '';
}

function xmkdir($dir){
    if (!file_exists($dir)) {
        $result = mkdir($dir , 0777 , true);
        $cmd    = "chown -R www:www {$dir}";
        pclose(popen($cmd , "w"));
        return $result;
    }
    else{
        return true;
    }
}

//参数意义分别是显示图表的div id(重要，请参考前台模版内容) ，显示数据，标题，X轴名字，Y轴名字，X步长
function init_highchart($render , $title = array() , array $data = array() , array $size = array() , array $name = array() , array $options = array()){
    empty($title) && $title = array('main' => '' , 'sub' => '' , 'x' => '' , 'y' => '');//主标题，副标题，x轴标题，y轴标题
    empty($name) && $name = array('x' => '');
    empty($size) && $size = array('x' => 1 , 'y' => 0);
    empty($option) && $option = array(
        'title' => array('text' => $title['main'] ? $title['main'] : ''),
        'subtitle' => array('text' => $title['sub'] ? $title['sub'] : ''),
        'chart' => array('type' => 'line' , 'renderTo' => $render),
        'xAxis' => array('title' => array('text' => $title['x']) , 'tickInterval' => $size['x'] ? $size['x'] : 1),
        'yAxis' => array('title' => array('text' => $title['y']) , 'min' => 0)
    );
    $size['y'] && $option['yAxis']['tickInterval'] = $size['y'];
    foreach($name as $k => $v){
        
        $option['series'][$k]['name'] = $v;
    }
    foreach($options as $k => $v){
        $option[$k] = $v;
    }

    foreach($data['x'] as $v){
        $option['xAxis']['categories'][] = $v;
    }
    foreach($data['y'] as $k => $row){
        foreach($row as $v){
            $option['series'][$k]['data'][] = $v;
        }
    }


    $chart = new Highchart();

    foreach($option as $k => $v){
        $chart->offsetSet($k , $v);
    }
	
    return '<script type="text/javascript" src="style/js/highcharts/highcharts.js"><script language="javascript">' . $chart->render($render) . '</script>';//返回生成highchart对象的js
}


//参数意义分别是显示图表的div id(重要，请参考前台模版内容) ，显示数据，标题，X轴名字，Y轴名字，X步长
function init_highchart_pie($render,$title,$arr) {
    $option = array(
        'title' => array('text' => $title),
        'chart' => array('type' => 'pie' , 'renderTo' => $render,'plotBackgroundColor'=> NULL,'plotBorderWidth'=>NULL,'plotShadow'=>false),
		'tooltip'=>array('headerFormat'=>'{series.name}<br>','pointFormat'=>'{point.name}: <b>{point.percentage:.1f}%</b>'),
		'plotOptions'=>array('pie'=>array(
			'allowPointSelect'=>true,
			'cursor'=>'pointer',
			'dataLabels'=>array(
				'enabled'=>true,
				'format'=>'<b>{point.name}</b>: {point.percentage:.1f}%',
				'style'=>array(
					'color'=> (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
				),
			
			),
		
		))
		
    );

    $chart = new Highchart();
	
	$option['series'][0]['type'] = 'pie';
	$option['series'][0]['name'] = '消耗数量';
	$option['series'][0]['data'] = $arr;
	
    foreach($option as $k => $v){
        $chart->offsetSet($k , $v);
    }
    return '<script language="javascript">' . $chart->render($render) . '</script>';//返回生成highchart对象的js
}



//参数意义分别是显示图表的div id(重要，请参考前台模版内容) ，显示数据，标题，X轴名字，Y轴名字，X步长
function init_highchart_line($render,$title,$data,$name) {
	
    $option = array(
        'title' => array('text' => $title['main'],'x' => -20),
		'subtitle' => array('text' => $title['sub'] ? $title['sub'] : '' ,'x' => -20),
		'xAxis' => array('categories' => ''),
		'yAxis' => array('title' => array('text' => $title['y']),'plotLines' => array( array('value' => 0 , 'width' => 1 , 'color' => '#808080' ))),
		'legend' => array( 'layout' => 'vertical', 'align' => 'right', 'verticalAlign' => 'middle', 'borderWidth' => 0),
    );
	foreach($data['x'] as $v){
        $option['xAxis']['categories'][] = $v;
    }
    $chart = new Highchart();
	
	foreach($data['y'] as $k => $row){
		$option['series'][$k]['name'] = $name[$k];
		$option['series'][$k]['data'] = $row;
	}
	
			
    foreach($option as $k => $v){
        $chart->offsetSet($k , $v);
    }
    return '<script language="javascript">' . $chart->render($render) . '</script>';//返回生成highchart对象的js
}






function textareaStrToArr($str){
    $str = trim($str);
    $str = str_replace(';', ',', $str);
    $str = str_replace('；', ',', $str);
    $str = str_replace('，', ',', $str);
    $str = str_replace(',', "\n", $str);
    $arr = explode("\n", $str);
    $data = array();
    //防止在windows系统下数组中的元素出现\r的情况
    foreach($arr as $tmp){
        trim($tmp) && $data[] = trim($tmp);
    }
    return $data;
}

/**
 * [chinese_substr 中文字符串截取]
 * @param  [type]  $text   [待处理字符串]
 * @param  [type]  $start  [截取开始位置]
 * @param  [type]  $length [截取长度]
 * @param  integer $ex     [description]
 * @return [type]          [description]
 */
function chinese_substr($text, $start, $length = null, $ex = 2){
    $strlen = strlen($text);
    $bytes = 0;
    if($start > 0){
        $bytes = -1;
        $chars = -1;
        while($bytes < $strlen && $chars < $start){
            $bytes++;
            $c = ord($text[$bytes]);
            if($c < 0x80 || $c >= 0xC0){
                if($c >= 0xC0){
                    $chars += $ex;
                }
                else{
                    $chars++;
                }
            }
        }
    }
    elseif($start < 0){
        $start = abs($start);
        $bytes = $strlen;
        $chars = 0;
        while($bytes > 0 && $chars < $start){
            $bytes--;
            $c = ord($text[$bytes]);
            if($c < 0x80 || $c >= 0xC0){
                if($c >= 0xC0){
                    $chars += $ex;
                }
                else{
                    $chars++;
                }
            }
        }
    }
    $istart = $bytes;
    if($length === null){
        $bytes = $strlen - 1;
    }
    elseif($length > 0){
        $bytes = $istart;
        $chars = 0;
        while($bytes < $strlen && $chars < $length){
            $bytes++;
            $c = ord($text[$bytes]);
            if($c < 0x80 || $c >= 0xC0){
                if($c >= 0xC0){
                    $chars += $ex;
                }
                else{
                    $chars++;
                }
            }
        }
        $bytes--;
    }
    elseif($length < 0){
        $length = abs($length);
        $bytes  = $strlen - 1;
        $chars  = 0;
        while($bytes >= 0 && $chars < $length){
            $c = ord($text[$bytes]);
            if($c < 0x80 || $c >= 0xC0){
                if($c >= 0xC0){
                    $chars += $ex;
                }
                else{
                    $chars++;
                }
            }
            $bytes--;
        }
    }
    $iend = $bytes;
    return substr($text , $istart , max(0 , $iend - $istart + 1));
}

//随机生成用户指定个数的字符串
function create_rand_code($num = 6){
    $ascii_number = '';
    for($i = 0; $i < $num; $i++){
        $number = rand(0, 2);
        switch($number){
            case 0:
                $rand_number = rand(48, 57);
                break; //数字
            case 1:
                $rand_number = rand(65, 90);
                break; //大写字母
            case 2:
                $rand_number = rand(97, 122);
                break; //小写字母
        }
        $ascii        = sprintf("%c", $rand_number);
        $ascii_number = $ascii_number . $ascii;
    }
    return $ascii_number;
}

/**
 * 加密解密方法
 */
function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0){
    $ckey_length = 8; // 随机密钥长度 取值 0-32;
    // 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。
    // 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方
    // 当此值为 0 时，则不产生随机密钥
    $key  = md5($key ? $key : '~x*$c0!2.gI0(x&j32');
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';
    $cryptkey   = $keya . md5($keya . $keyc);
    $key_length = strlen($cryptkey);
    $string        = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
    $string_length = strlen($string);
    $result = '';
    $box    = range(0, 255);
    $rndkey = array();
    for($i = 0; $i <= 255; $i++){
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }
    for($j = $i = 0; $i < 256; $i++){
        $j       = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp     = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }
    for($a = $j = $i = 0; $i < $string_length; $i++){
        $a       = ($a + 1) % 256;
        $j       = ($j + $box[$a]) % 256;
        $tmp     = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }
    if($operation == 'DECODE'){
        if((substr($result , 0 , 10) == 0 || substr($result , 0 , 10) - time() > 0) && substr($result , 10 , 16) == substr(md5(substr($result , 26) . $keyb) , 0 , 16)){
            return substr($result , 26);
        }
        else{
            return '';
        }
    }
    else{
        return $keyc . str_replace('=' , '' , base64_encode($result));
    }
}

/**
 * [datePage 根据日期显示页码数量]
 * @param  string  $start [开始日期]
 * @param  string  $end   [结束日期]
 * @param  integer $page  [当前页码]
 * @param  integer $num   [显示的数量]
 * @param  integer $sort  [排序 1 倒序 0 顺序]
 * @param  integer $actual  [实际总数]
 * @return [type]         [description]
 */
function datePage($start, $end, $page = 1, $num = 10, $sort = 1, $actual = null){
    if(is_numeric($start) && is_numeric($end)){
        $type = true;
    }
    else{
        $start = strtotime($start);
        $end   = strtotime($end);
    }
    $result = array();
    $total  = ceil(($end - $start) / 86400) + 1;
    if($sort){
        $result[] = $actual && $actual < $total ? $actual : $total;
        $result[] = ($end - $page * $num * 86400 + 86400);
        $result[] = ($end - ($page - 1) * $num * 86400);
    }
    else{
        $result[] = $actual && $actual < $total ? $actual : $total;
        $result[] = $start + ($page - 1) * $num * 86400 + 86400;
        $result[] = $start + $page * $num * 86400;
    }
    $result[1] = $result[1] < $start ? $start : $result[1];
    $result[2] = $result[2] > $end ? $end : $result[2];
    if(!$type){
        $result[1] = date('Y-m-d', $result[1]);
        $result[2] = date('Y-m-d', $result[2]);
    }
    return $result;
}

/**
 * [convert 获取内存使用量]
 * @param  [type] $size [数组]
 * @return [type]       [description]
 */
function Mem(){
    $size = memory_get_usage();
    $unit = ['B', 'K', 'M', 'G', 'T', 'P'];
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function getClientIp($type = 0, $adv = false){
    $type = $type ? 1 : 0;
    static $ip = null;
    if(null !== $ip){
        return $ip[$type];
    }
    if($adv){
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) {
                unset($arr[$pos]);
            }
            $ip = trim($arr[0]);
        }
        elseif(isset($_SERVER['HTTP_CLIENT_IP'])){
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif(isset($_SERVER['REMOTE_ADDR'])){
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    }
    elseif(isset($_SERVER['REMOTE_ADDR'])){
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u", ip2long($ip));
    $ip = $long ? [$ip, $long] : ['0.0.0.0', 0];
    return $ip[$type];
}

/**
 * 获取ip对应 国家/省份/城市
 * @param null $ip
 * @param int $format 0：市；1：市/省；2：市/省/国家
 * @return array|bool
 */
function GetIpCity($ip = null,$format=0){
    if(empty($ip)) return false;
    $res = @file_get_contents('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip=' . $ip);
    if(empty($res)){ return false; }
    $jsonMatches = array();
    preg_match('#\{.+?\}#', $res, $jsonMatches);
    if(!isset($jsonMatches[0])){
        return false;
    }
    $json = json_decode($jsonMatches[0], true);
    if(isset($json['ret']) && $json['ret'] == 1){
        $return = [$json['city']];
        if($format > 0){
            array_push($return,$json['province']);
            if($format > 1){
                array_push($return,$json['country']);
            }
        }
    }
    else{
        return false;
    }
    return $return;
}

function url_exists($url){
    $handle = curl_init($url);
    curl_setopt($handle , CURLOPT_RETURNTRANSFER , TRUE);
    curl_setopt($handle , CURLOPT_CONNECTTIMEOUT , 10);
    curl_exec($handle);
    $httpCode = curl_getinfo($handle , CURLINFO_HTTP_CODE);
    curl_close($handle);
    return ($httpCode == 404) ? false : true;
}