
<?php
!defined('IN_WEB') && exit('Access Denied');

class Controller{
    public $smarty;
    public $method;
    private $data;
    private $conditions;

    use tOperate;//引入切面
    use tChannelGroupServerAccess;
    
    public function __construct(){
        $this->method = $_SERVER['REQUEST_METHOD'];

        require_once ROOT . '/library/smarty/Smarty.class.php';
        $smarty                  = new Smarty();
        $smarty->debugging       = false;
        $smarty->template_dir    = ROOT . '/templates'; //设置模板目录
        $smarty->compile_dir     = ROOT . '/templates_c'; //设置编译目录
        $smarty->left_delimiter  = '<{';
        $smarty->right_delimiter = '}>';
        $smarty->php_handling    = SMARTY_PHP_ALLOW;
        $this->smarty            = $smarty;

        $this->userAction = new UserAction();
        $_username        = $this->userAction->userSessionGet('username');

        tOperate::tHandel($_SESSION,dirname(ROOT).'/logs/center/operate/',true);

        $this->smarty->assign('_username' , $_username);
        $this->smarty->assign('web_name' , WEB_NAME);
        $this->smarty->assign('game_name' , GAME_NAME);
        $this->smarty->assign('login_url' , LOGIN_URL);
        $this->setConditions();
        $this->setData();
        $this->setHeader('' , 1);
        $this->setSearchHeader('' , 1);
        $this->setSearchFooter('' , 1);
        //$this->setJs('' , 1);
        //$this->setJs('' , 3);
    }
    
    public function displays($output = true , $header = 0){
        !$this->validSearch($this->data['search_fields']) && exit('search fields error');
        !$this->validOther($this->data['other_data']) && exit('other data error');
        ($output == false) && ob_start();
        if(isset($this->data['other_data']['header']) && $this->data['other_data']['header'] != ''){
            if($header ==0){
                $this->data['other_data']['header'] = '<div style="padding:10px 10px 20px;">' . $this->data['other_data']['header'] . '</div>';
            }else{
                $this->data['other_data']['header'] = '<div>' . $this->data['other_data']['header'] . '</div>';
            }
           
        }
        $aoColumns = $this->renderAoColumns($this->data['show_fields']);
        $this->smarty->assign('search_fields' , $this->data['search_fields']);
        $this->smarty->assign('show_fields', $this->data['show_fields']);
        $this->smarty->assign('ajax_source', $this->data['ajax_source']);
        $this->smarty->assign('other_data', $this->data['other_data']);
        $this->smarty->assign('aoColumns', $aoColumns);
        $this->smarty->display('common/data_tables.tpl');
        if($output == false){
            $rs = ob_get_contents();
            ob_end_clean();
            return $rs;
        }
    }
   
    public function renderAoColumns($show_fields){
        $aoColumns = array();
        foreach($show_fields as $item){
            if(isset($item['sortable']) && $item['sortable'] == true){
                $aoColumns[] = '{ "asSorting" : [ "desc", "asc" ] }';
            }
            elseif(isset($item['class']) && $item['class'] == true){
                $aoColumns[] = '{ "sClass" : "details-control"}';
            }
            else{
                $aoColumns[] = '{ "bSortable" : 0}';
            }
        }
        return implode(',' , $aoColumns);
    }

    private function validOther(&$other_data){
        !$other_data['iDisplayLength'] && $other_data['iDisplayLength'] = 100; //iDisplayLength是每页条数，没有设置则每页10条
        $other_data['sortCol'] = intval($other_data['sortCol']);
        !$other_data['sortDir'] && $other_data['sortDir'] = 'desc';
        !$other_data['id'] && $other_data['id'] = 'dataTable';
        if(!isset($other_data['bSort'])){
            $other_data['bSort'] = 'true';
        }
        else{
            $other_data['bSort'] = $other_data['bSort'] ? 'true' : 'false';
        }
        return true;
    }

    private function validSearch(&$search_fields){
        
        if(!$search_fields){
            return true;
        }

        foreach($search_fields as $k => $field){
            empty($field['type']) && $field['type'] = 'input';
            $types = array('input' , 'checkbox' , 'date' , 'range' , 'select' , 'range_date' , 'range_time' , 'selects' , 'scp');
            //类型限制
            if(!in_array($field['type'], $types)){
                return false;
            }
            //如果是下拉菜单类型，必须定义下拉菜单的值
            if($field['type'] == 'select' && empty($field['value'])){
                return false;
            }
            else if($field['type'] == 'scp'){
                $show = array();
                if($field['value']){
                    $show['showGroup'] = $field['value'][0];
                    $show['showServer'] = $field['value'][2];
                    $show['showChannel'] = $field['value'][1];
                    $show['showPackage'] = $field['value'][3];
                }
                $field['value'] = '<input type="button" id="filter" class="gbutton" value="筛选">';
                
                $this->smarty->assign('scp' , $this->getScp($show));
            }
            //默认宽度
            !$field['width'] && $field['width'] = 120;
            $search_fields[$k] = $field;
        }
        return true;
    }

    public function tabs($tabs , $tabs_id = 'tabs' , $other_data = array()){
        $new_tabs = array();
        foreach($tabs as $row){
            $ctrl= '';$act= '';
            $parse_url = parse_url($row['url']);
            $query = $parse_url['query'];
            parse_str($query);
            $ctrl = empty($ctrl) ? 'frame' : $ctrl;
            $act = empty($act) ? 'index' : $act;
            $hasPurview = has_purview($ctrl, $act);
            $hasPurview === 1 && $new_tabs[] = $row;
        }
		
        if($new_tabs){
            $this->smarty->assign('id' , $tabs_id);
            $this->smarty->assign('tabs' , $new_tabs);
            $this->smarty->assign('other_data' , $other_data);
            $this->smarty->display('common/tabs.tpl');
        }
        else{
            echo 'no tabs';
        }
    }

    public function setData($data = array()){
        $this->data = $data;
    }

    public function getData(){
        return $this->data;
    }

    public function setHeader($header , $type = 0){
        if($type){
            $this->data['other_data']['header'] = $header;
        }
        else{
            $this->data['other_data']['header'] .= $header;
        }
    }

    public function setSearchHeader($header , $type = 0){
        if($type){
            $this->data['other_data']['search_header'] = $header;
        }
        else{
            $this->data['other_data']['search_header'] .= $header;
        }
    }

    public function setSearchFooter($footer , $type = 0){
        if($type){
            $this->data['other_data']['search_footer'] = $footer;
        }
        else{
            $this->data['other_data']['search_footer'] .= $footer;
        }
    }

    public function setSearch($title , $type = 'input' , $value = '' , $width = 120 , $class = ''){
        $this->data['search_fields'][] = array('title' => $title , 'type' => $type , 'value' => $value , 'width' => $width , 'class' => $class);
    }

    public function setButton($position , $value , $id = '' , $class = ''){
        if($position == 'header'){
            $this->setSearchHeader('<input type="button" value="' . $value . '" id="' . $id . '" class="gbutton ' . $class . '" style="margin-right:14px;">');
        }
        else if($position = 'footer'){
            $this->setSearchFooter('<input type="button" value="' . $value . '" id="' . $id . '" class="gbutton ' . $class . '" style="margin-right:14px;">');
        }
    }

    //$width单位是px
    public function setField($title , $width = 120){
        $this->data['show_fields'][] = array('title' => $title , 'width' => $width);
    }

    //当只需要设置标题，宽度可以统一设置时，可使用该方法批量设置项
    public function setFields($fields , $width = 120){
        if(is_array($fields)){
            foreach($fields as $title){
                $this->data['show_fields'][] = array('title' => $title , 'width' => $width);
            }
        }
        else{
            return false;
        }
    }

    //$params是附加查询参数
    public function setSource($controller , $action , $export = 0 , array $params = array()){
        $query = '';
        if($params){
            foreach($params as $k => $v){
                $query .= '&' . $k . '=' . $v;
            }
        }
        $this->data['ajax_source'] = 'admin.php?&ctrl=' . $controller . '&act=' . $action . $query;
        $export && $this->setExport($controller , $action);
    }

    public function setDataTableId($id){
        $this->data['other_data']['id'] = $id;
    }

    public function setJs($js = '' , $type = 0){
        if($type == 1){
            $this->data['other_data']['js'] = $js;
        }
        else if($type == 2){
            $js = $this->smarty->fetch($js);
            $this->data['other_data']['jsf'] .= '<script type="text/javascript">' . $js . '</script>';
        }
        else if($type == 3){
            $js = $this->smarty->fetch($js);
            $this->data['other_data']['jsf'] = '<script type="text/javascript">' . $js . '</script>';
        }
        else{
            $this->data['other_data']['js'] .= $js;
        }
		
    }

    public function setExport($controller , $action){
        $this->setSearchFooter('<button id="export" class="gbutton" onclick="_export()">导出</button>');
        $js = <<< END
            function _export(){
                var url = 'admin.php?ctrl=$controller&act=$action&export=1';
                window.open(url);
            }
END;
        $this->setJs($js);
    }

    public function setConditions($conditions = array()){
        $this->conditions = $conditions;
    }

    public function getConditions(){
        return $this->conditions;
    }

    //$num是获取值的标识，$type是预设的conditions类型，$field是对应的数据库字段名，$default是默认赋值 , $scp是scp的赋值
    public function setCondition($num , $type , $field = '' , $default = '' , $scp =array()){

        $types = array(
            'gs', //大于..小于..
            'gts', //大于等于..小于..
            'gst', //大于..小于等于..
            'gtst', //大于等于..小于等于..
            'time', //时间
            'date', //日期
            'between', //between关键字
            'eq', //等于
            'in', //in关键字
            'scp', //渠道组，服务器，渠道，包筛选
			'like', //模糊查询
            'neq',//不等于
        );
        if(!in_array($type , $types)){
            return false;
        }
        $conditions = $this->getConditions();
        $num = is_numeric($num) ? 'sSearch_' . $num : $num;
        $num = $this->getParam($num);
        $data = (empty($num) && $default) ? $default : $num; //如果输入为空，则data等于默认赋值

        switch($type){
            case 'eq':
                $data && $conditions['WHERE'][$field] = trim($data);
                $data === 0 && $conditions['WHERE'][$field] = 0;
                $data === '0' && $conditions['WHERE'][$field] = '0';
                break;
            case 'scp':

                $scp = $this->parseScp($data,$scp);
               
                //$scp['group'] && $conditions['WHERE']['group::IN'] = $scp['group'];
                $scp['server'] && $conditions['WHERE']['server::IN'] = $scp['server'];
                $scp['channel'] && $conditions['WHERE']['channel::IN'] = $scp['channel'];
                $scp['package'] && $conditions['WHERE']['package::IN'] = $scp['package'];
                break;
            case 'time':
                $data = explode('|' , $data);
                $s = trim($data[0]);
                $s && $conditions['WHERE'][$field . '::>='] = strtotime($s);
                $e = trim($data[1]);
                $e && $conditions['WHERE'][$field . '::<='] = strtotime($e)+59;
                break;
            case 'date':
                $data = explode('|' , $data);
                $s = trim($data[0]);
                $s && $conditions['WHERE'][$field . '::>='] = strtotime($s);
                $e = trim($data[1]);
                $e && $conditions['WHERE'][$field . '::<='] = strtotime($e) + 86400 - 1;
                break;
            case 'between':
                $data = explode('|' , $data);

                if (!$data[0] && !$data[1]) {
                    break;
                }

                $s = trim($data[0]) ? intval($data[0]) : 0;
                $e = trim($data[1]) ? intval($data[1]) : 1000000000;
                $conditions['WHERE'][$field . '::BETWEEN'] = array($s , $e);
                break;
            case 'in':
                $data && $conditions['WHERE'][$field . '::IN'] = $data;
                break;
			case 'like':
                $data && $conditions['WHERE'][$field . '::like'] = "%$data%";
                break;
            case 'gs':
                $data = explode('|' , $data);
                $s = trim($data[0]);
                $s && $conditions['WHERE'][$field . '::>'] = intval($s);
                $e = trim($data[1]);
                $e && $conditions['WHERE'][$field . '::<'] = intval($e);
                break;
            case 'gts':
                $data = explode('|' , $data);
                $s = trim($data[0]);
                $s && $conditions['WHERE'][$field . '::>='] = intval($s);
                $e = trim($data[1]);
                $e && $conditions['WHERE'][$field . '::<'] = intval($e);
                break;
            case 'gst':
                $data = explode('|' , $data);
                $s = trim($data[0]);
                $s && $conditions['WHERE'][$field . '::>'] = intval($s);
                $e = trim($data[1]);
                $e && $conditions['WHERE'][$field . '::<='] = intval($e);
                break;
            case 'gtst':
                $data = explode('|' , $data);
                $s = trim($data[0]);
                $s && $conditions['WHERE'][$field . '::>='] = intval($s);
                $e = trim($data[1]);
                $e && $conditions['WHERE'][$field . '::<='] = intval($e);
                break;
            case 'eqtime':
                $data && $conditions['WHERE'][$field] = strtotime(date('ymd',$data));
                break;
        }

        $this->setConditions($conditions);
    }

    //格式：$orders = array($num => array($order_field , $order_type),...);
    public function setOrder($default = 'id' , $sort = '' , $orders = array()){
        $conditions = $this->getConditions();
        $sort_type = trim($this->getParam('sSortDir_0'));
        $sort_type = $sort ? $sort : ($sort_type ? $sort_type : 'desc');
        $sort_field = trim($this->getParam('iSortCol_0'));
        if($orders && array_key_exists($sort_field , $orders)){
            $sort_type = isset($orders[$sort_field][1]) ? $orders[$sort_field][1] : $sort_type;
            $conditions['Extends']['ORDER'] = array($orders[$sort_field][0] . '#' . $sort_type);
        }
        else{
            $conditions['Extends']['ORDER'] = array($default . '#' . $sort_type);
        }
        $this->setConditions($conditions);
    }

    //$limit用于设置显示的每页条数
    public function setLength($limit){
        $this->data['other_data']['iDisplayLength'] = $limit;
    }

    //$limit用于设置查询的每页条数
    public function setLimit($limit = 0 , $skip = true){
        $conditions = $this->getConditions();
        $limit = $limit ? $limit : intval($this->getParam('iDisplayLength'));
        if($skip){
            $skip = intval($this->getParam('iDisplayStart'));
            $conditions['Extends']['LIMIT'] = array($skip , $limit);
        }
        else{
            $conditions['Extends']['LIMIT'] = $limit;
        }
        $this->setConditions($conditions);
    }

    public function setDataExport($data_method , $export_method){
        if($this->getParam('export')){
            $this->setConditions($_SESSION['conditions']);
            $method = $export_method;
        }
        else{
            $_SESSION['conditions'] = $this->getConditions();
            $method = $data_method;
        }
        return $method;
    }

    //获取POST和GET
    public function getParam($name , $default = ''){
        return isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $default);
    }

    //获取COOKIE
    public function getCookie($name , $default = ''){
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : $default;
    }

    //获取SESSION
    public function getSession($name , $default = ''){
        return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
    }

    public function fetch($url , array $params){
        if(!$url){
            return false;
        }
        $params['unixtime'] = time();
        $params['sign'] = md5(CDict::$apiKey['centerSingle'] . $params['unixtime']);
        return Util::getCurl($url, $params);
    }

    //获取单服地址
    public function apiUrl($server_id){
        $server_id = is_array($server_id) ? $server_id : array($server_id);
        $server = (new ServerconfigModel())->getApiUrl($server_id);
        if($server){
            $api_url = array();
            foreach($server as $row){
                $api_url[$row['server_id']] = $row['api_url'];
            }
            return $api_url;
        }
        else{
            return null;
        }
    }

    //调用单服方法
    public function remoteCall($class , $method , array $params = array() , $api_url = '', $server_id = ''){
        return Util::remoteCall($api_url , $class , $method , $params, null, $server_id);
    }

    //调用单服或者本地方法
    public function call($class , $method , array $params = array() , $server_id = ''){
        if($server_id){
            $api_url = $this->apiUrl($server_id);


            $rs = $api_url ? $this->remoteCall($class , $method , $params , $api_url, $server_id) : false;

            if($rs){
                foreach($rs as $k => $row){ //$k是服务器id ，$row是该服务器id的返回内容
                  
                    $rs[$k] = json_decode($row , true);
                }
            }
            return $rs;
        }
        else{
            return (new $class())->$method($params);
        }
    }

    //合并使用call方法返回多个服务器的数据
    //对$call_result有格式要求：array('server_id1' => array(array() , 0) , 'server_id2' => ...) ， 数组server_id1的第一个值是返回的数据 , 第二个值是数据的条数
    public function callDataMerge($call_result , $server_add = false){
        $data = array();$num = 0;
        $server = (new ServerModel())->getServerNameByMmc();
        foreach($call_result as $k => $v){
            foreach($v[0] as $row){
                $server_add && $row['call_server_id'] = $server[$k]; //如果$server_add为true则把server_id添加到每一条数据里面
                $data[] = $row;
            }
            $num += $v[1];
        }
        return array($data , $num);
    }

    /**
     * 渠道组，服务器，渠道，包的 
     * @param  array  $show    [description]
     * @param  array  $group   [description]
     * @param  array  $server  [description]
     * @param  array  $channel [description]
     * @param  array  $package [description]
	 * @param  int    $radio   [description]
     * @return [type]          [description]
     */
    public function getScp($show = array() , $group = array() , $server = array() , $channel = array() , $package = array() , $radio = 0 ){
        
        $ChannelgroupModel = new ChannelgroupModel();

        empty($show) && $show = array('showGroup' => 1 , 'showServer' => 1 , 'showChannel' => 1 , 'showPackage' => 1);
        
        $list = array();
        $list['group'] = array();
        $list['server'] = array();
        $list['channel'] = array();
        $list['package'] = array();

        //根据用户的渠道权限来判断可以看到的渠道组，渠道，服务器，包

        if($_SESSION['group_channel'] == 'all') {//全部权限
            $groupRes = (new ChannelgroupModel())->getChannelGroup($group , array('id' , 'name'));
        }else{
          
            $keys = array_keys($_SESSION['group_channel']);

            $channelid = "'" . implode("','" , $keys) . "'";
            $sql = "select distinct(group_id) from ny_package where channel_id in ($channelid)";

            $g = $ChannelgroupModel -> query($sql);
            $g = array_column($g, 'group_id');
            
            $groupRes = (new ChannelgroupModel())->getChannelGroup($g , array('id' , 'name'));

        }

		foreach($groupRes as $k=>$v){
			$group[$v['id']]['name'] = $v['name'];//渠道组
			$group[$v['id']]['id'] = $v['id'];
		}
      
		$groupid = array_column($groupRes,'id');
		$groupid = implode(',' , $groupid);
		
		//渠道
        if($_SESSION['group_channel'] == 'all') {//全部权限

		    $channelsql = "select c.name,c.channel_id,p.group_id from ny_package p left join ny_channel c on c.channel_id = p.channel_id where p.group_id in ($groupid) ";
            
            $packagesql = "select package_id , p.`name` ,p.`game_name` ,group_id , c.`name` as channel_name from ny_package p LEFT JOIN ny_channel c ON c.channel_id = p.channel_id where group_id in ($groupid)  order by p.create_time desc"; 

        }  else{
             $channelsql = "select c.name,c.channel_id,p.group_id from ny_package p left join ny_channel c on c.channel_id = p.channel_id where c.channel_id in ($channelid)"; 
             
             $packagesql = "select package_id , p.`name` ,p.`game_name` ,group_id , c.`name` as channel_name from ny_package p LEFT JOIN ny_channel c ON c.channel_id = p.channel_id where group_id in ($groupid) and c.channel_id in ($channelid)  order by p.create_time desc";
        }
		//服务器
		$serversql = "select server_id , name ,group_id , num,`mom_server`  from ny_server where group_id in ($groupid) order by num desc";

		$c = $ChannelgroupModel->query($channelsql);
		$s = $ChannelgroupModel->query($serversql);
		$p = $ChannelgroupModel->query($packagesql);
		
		//渠道
		foreach($c as $k=>$v){
			if(!isset($channel[$v['group_id']][$v['channel_id']])){
				$channel[$v['group_id']][$v['channel_id']]['name'] = $v['name'];
				$channel[$v['group_id']][$v['channel_id']]['channel_id'] = $v['channel_id']; 
                $channel[$v['group_id']][$v['channel_id']]['group_id'] = $v['group_id']; 
			}
		}
		//服务器
        $momServerArr = [];
        foreach($s as $k=>$info){
            !empty($info['mom_server']) && $momServerArr[$info['group_id']][$info['server_id']] = array('mom_server'=>$info['mom_server'],'num'=>$info['num']);
        }
		foreach($s as $k=>$v){
			$server[$v['group_id']] [$v['server_id']]['name'] =  $v['name']; 
			$server[$v['group_id']] [$v['server_id']]['server_id'] =  $v['server_id']; 
            $server[$v['group_id']] [$v['server_id']]['group_id'] =  $v['group_id']; 
			$server[$v['group_id']] [$v['server_id']]['num'] =  $v['num'];
		}
		//包号
		foreach($p as $k =>$v){
			$package[$v['group_id']][$v['package_id']]['name'] = $v['name'] ;
			$package[$v['group_id']][$v['package_id']]['package_id'] = $v['package_id'] ;
            $package[$v['group_id']][$v['package_id']]['group_id'] = $v['group_id'] ;
            $package[$v['group_id']][$v['package_id']]['channel_name'] = $v['channel_name'] ;
            $package[$v['group_id']][$v['package_id']]['game_name'] = $v['game_name'] ;
		}

        $list['group'] = $group;
        $list['server'] = $server;
        $list['channel'] = $channel;
        $list['package'] = $package;

        //如果radio为1的话，则只能单选
        if($radio == 1){
            $this->smarty->assign('select', 'radio');
        }
        $this->smarty->assign('showGroup' , $show['showGroup']);
        $this->smarty->assign('showServer' , $show['showServer']);
        $this->smarty->assign('showChannel' , $show['showChannel']);
        $this->smarty->assign('showPackage' , $show['showPackage']);
        $this->smarty->assign('list' , $list);
        return $this->smarty->fetch('common/scp.tpl');

    }

    /**
     * [渠道组，服务器，渠道，包]
     * @param  [string] $data [json]
     * @param  [string] $arr [array]
     * @return [type]       [description]
     */
    public function parseScp($data , $arr){//0渠道组，2服务器，1渠道，3包号
        $ChannelgroupModel = new ChannelgroupModel();

        $data = json_decode($data , true); 
        $scp = array();

        $group = $data[0] ? $data[0] : array();
        $server = $data[2] ? $data[2] : array();
        $channel = $data[1] ? $data[1] : array();
        $package = $data[3] ? $data[3] : array();
		
        $group_server = array();
        $group_package = array();
        $channel_server = array();
        $channel_package = array();

        if(!empty($group)){
            $group = implode(',',$group);
            $serversql = "select server_id from ny_server where group_id in ($group) ";//服务器
            $packagesql = "select package_id from ny_package where group_id in ($group)"; //包

            $group_server = $ChannelgroupModel->query($serversql);
            $group_server = array_column($group_server, 'server_id') ; //渠道组转成的服务器

            $group_package = $ChannelgroupModel->query($packagesql);
            $group_package = array_column($group_package, 'package_id') ; //渠道组转成的包
        }

        if(!empty($channel)){
            $channelid = "'" . implode("','" , $channel) . "'";
            $sql = "select distinct(group_id) from ny_package where channel_id in ($channelid)";

            $channel = $ChannelgroupModel->query($sql);
            $channel = array_column($channel, 'group_id');
            $channel = implode(',', $channel);

            $serversql = "select server_id from ny_server where group_id in ($channel) ";//服务器
            $packagesql = "select package_id from ny_package where group_id in ($channel)"; //包

            $channel_server = $ChannelgroupModel->query($serversql);
            $channel_server = array_column($channel_server, 'server_id') ; //渠道转成的服务器

            $channel_package = $ChannelgroupModel->query($packagesql);
            $channel_package = array_column($channel_package, 'package_id') ; //渠道转成的包   
        }

       //根据三个数组的值来判断服务器和包
       
       if(!empty($group)){

            if(!empty($channel)){

                if(!empty($server)){
                    $scp['server'] = array_intersect($group_server , $channel_server ,$server);
                }else{
                    $scp['server'] = array_intersect($group_server , $channel_server );
                }

                if(!empty($package)){
                    $scp['package'] = array_intersect($group_package , $channel_package ,$package);
                }else{
                    $scp['package'] = array_intersect($group_package , $channel_package);
                }
            }else{
                if(!empty($server)){
                    $scp['server'] = array_intersect($group_server , $server);
                }else{
                    $scp['server'] = $group_server;
                }

                if(!empty($package)){
                    $scp['package'] = array_intersect($group_package , $package);
                }else{
                    $scp['package'] = $group_package;
                }
            }

       }else{

            if(!empty($channel)){

                if(!empty($server)){
                    $scp['server'] = array_intersect($channel_server ,$server);
                }else{
                    $scp['server'] = $channel_server;
                }

                if(!empty($package)){
                     $scp['package'] = array_intersect($channel_package ,$package);
                }else{
                    $scp['package'] = $channel_package;
                }
            }else{
                if(!empty($server)){
                    $scp['server'] = $server;
                }else{
                    $scp['server'] = '';
                }

                if(!empty($package)){
                     $scp['package'] = $package;
                }else{
                    $scp['package'] = '';
                }
            }
       }

       //如果都为空并且该方法需要转换，则添加该用户的权限可以看到的服务器和包，避免看到所有的数据
       if(empty($scp['package']) &&  empty($scp['server']) && $arr[2] ==1 && $arr[3] == 1 ){
           $scp =  $this->permit();
       }

	   if(!empty($scp['package']) && empty($package)){
	   	 $scp['package'][] = 0;
	   }

       //做个特殊处理,下面这些方法不用进行转换
       
       $ctrl = $this->getParam('ctrl');
       $act = $this->getParam('act');
       $method = $ctrl.'_'.$act;

       if($method == 'register_record_data'){
            unset($scp);
            $scp['channel'] = $data[1] ? $data[1] : '';
       }

       if($method == 'client_device_loss_data'){
            unset($scp);
            $scp['channel'] = $data[1] ? $data[1] : '';
            $scp['package'] = $data[3] ? $data[3] : '';
       }

       return $scp;
    }

    public function getServer(){
        $server = (new ServerModel())->getServer(array() , array('server_id' , 'name'));
        $this->smarty->assign('checkboxServer' , $server);
        return $this->smarty->fetch('common/server.tpl');
    }

    public function getChannel(){
        $channel = (new ChannelModel())->getChannel(array() , array('channel_id' , 'name'));
        $this->smarty->assign('checkboxChannel' , $channel);
        return $this->smarty->fetch('common/channel.tpl');
    }

    public function getPackage(){
        $package = (new PackageModel())->getPackage(array() , array('channel_id' , 'package_id' , 'name'));
        $this->smarty->assign('checkboxPackage' , $package);
        return $this->smarty->fetch('common/package.tpl');
    }

    public function getChannelServer(){
        $servers = (new ServerModel())->getServerByTrait();

        $channelGroupRes = (new ChannelgroupModel())->getChannelGroupByTrait(array());

        $channelGroupRow = array();
        foreach ($channelGroupRes as $row) {
            $group_id = $row['id'];
            $channelGroupRow[$group_id] = $row['name'];
        }
        $this->smarty->assign('groups',$channelGroupRow);
        $this->smarty->assign('servers',$servers);
        return $this->smarty->fetch('../plugin/channelGroup_server.tpl');
    }
	//
    /**
     * [getServerPackage 根据权限获取渠道组服务器渠道包的信息]
     * @param  integer $showgroup   [是否返回渠道组]
     * @param  integer $showserver  [是否返回服务器]
     * @param  integer $showchannel [是否返回渠道]
     * @param  integer $showpackage [是否返回包]
     * @return [varchar]  $data     [返回信息的json格式]
     */
	public function getServerPackage($showgroup =0 ,$showserver = 0 , $showchannel = 0 ,$showpackage = 0){
		$serverarr = array();
		$packagearr = array();
		$channelarr = array();
		$grouparr = array();
		if($_SESSION['group_channel'] != 'all'){
			
		    foreach($_SESSION['group_channel'] as $c => $p){
				$gs = (new ServerModel())->getServerByChannelPackage($c , '' , '' , '' , array('s.server_id' , 's.name',  's.num') , true);
				foreach($gs as $g => $s){
					
					$g = explode('_' , $g);
                    $groupId = $g[0];
					$grouparr[] = $groupId;
					
					foreach($s as $row){
						$server_id = $row['server_id'];
						$serverarr[] = $server_id;
					}
				}
				if($p == 'all'){
					
					$rs = (new PackageModel())->getPackageByChannel($c , array('package_id'));

					foreach($rs as $row){
						$packagearr[] = $row['package_id'];
					}
				}
				
				$list = (new ChannelModel())->getChannel($c , array('channel_id' , 'name'));
				$channelarr[] =  $list['channel_id'];
			}
		}
		$data = array();
		
		if($showgroup == 1){
			$data[0] = $grouparr;	
		}else{
			$data[0] =array();	
		}
		
		if($showserver == 1){
			$data[2] = $serverarr;	
		}else{
			$data[2] =array();	
		}
		
		if($showchannel == 1){
			$data[1] = $channelarr;	
		}else{
			$data[1] =array();	
		}
		
		if($showpackage == 1){
			$data[3] = $packagearr;	
		}else{
			$data[3] =array();	
		}
		
		return json_encode($data);
	}
    //默认的时候的权限
    public function permit(){
        $ChannelgroupModel = new ChannelgroupModel();
        $list = array();
        $list['group'] = array();
        $list['server'] = array();
        $list['channel'] = array();
        $list['package'] = array();

        //根据用户的渠道权限来判断可以看到的渠道组，渠道，服务器，包
       
        if($_SESSION['group_channel'] == 'all') {//全部权限
            $groupRes = (new ChannelgroupModel())->getChannelGroup(array() , array('id' , 'name'));

        }else{
          
            $keys = array_keys($_SESSION['group_channel']);

            $channelid = "'" . implode("','" , $keys) . "'";

            $sql = "select distinct(group_id) from ny_package where channel_id in ($channelid)";

            $g = $ChannelgroupModel -> query($sql);

            $g = array_column($g, 'group_id');
            
            $groupRes = (new ChannelgroupModel())->getChannelGroup($g , array('id' , 'name'));
           
        }
       
      
        $groupid = array_column($groupRes,'id');
       
        $groupid = implode(',' , $groupid);
       
        //服务器
        $serversql = "select server_id , name ,group_id , num  from ny_server where group_id in ($groupid) order by create_time desc";

        if($_SESSION['group_channel'] != 'all'){
            $where = " and channel_id in ($channelid)";
        }else{
            $where  = '';
        }

        //包号
        $packagesql = "select package_id , name ,group_id from ny_package where group_id in ($groupid)  $where  order by create_time desc";  

       
        $s = $ChannelgroupModel->query($serversql);
        $p = $ChannelgroupModel->query($packagesql);
        
        //服务器 
        foreach($s as $k=>$v){
            $server[] = $v['server_id'];
        }
        //包号
        foreach($p as $k =>$v){
            $package[] = $v['package_id'];
        }

        $list['server'] = $server;
      
        $list['package'] = $package;

        return $list;
    }

    public function getTextWrapContent($content)
    {
        if (!$content || is_array($content)) return false;

        $order= array("\r\n","\n","\r");
        $content = str_replace($order,',', $content);
        $content = explode(",", trim($content));
        $content = array_unique($content); //去重

        return $content;
    }

    /**
     * 设置modal弹出框
     * @version 2017-09-07
     * @author xty
     * @param string $html
    */
    public function setModal($html){

    }

    /**
     * [生产订单号]
     * @param  [int] $role_id [角色ID]
     * @return [type]          [description]
     */
    public function makeOrder($role_id)
    {
        $len = strlen($role_id);
        if ($len < 4) {
            $num = 4 - $len;
            $str = '';
            for ($i = 0; $i < $num; $i++) {
                $str .= 0;
            }
            $str .= $role_id;
        } else {
            $str = substr($role_id, -4);
        }

        //防止科学记数法出现
        $order_num = date('ymdHis') . 'E' . $str . rand(1000, 9999);


        return $order_num;
    }
}
