<?php

abstract class CronController{
    abstract public function run(array $argv = null);

    public function __destruct(){
        echo PHP_EOL . '[' . date('Y-m-d H:i:s') . '] finish ' . get_called_class() . PHP_EOL;
    }
}