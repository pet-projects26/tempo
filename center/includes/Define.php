<?php
!defined('IN_WEB') && exit('Access Denied');

//开发模式
define('ENVIRONMENT' , 'DEV');
define('WEB_NAME' , '管理后台');
define('GAME_NAME', '九州仙剑传');
define('SALT' , 'uiR5DSIPv2');
define('LANGUAGE' , 'zh-hans');
define('TIMEZONE' , 'Asia/Shanghai');
define('MENU_STATUS_SHOW' , 1);  //菜单显示
define('MENU_STATUS_HIDDEN' , 2); //菜单隐藏
define('USER_STATUS_NORMAL' , 1); //用户状态：正常
define('USER_STATUS_FORBID' , 2); //用户状态：封号
define('PASSWORD_LENGTH' , 8);//密码长度
define('LOGIN_URL' , 'admin.php?ctrl=index&act=login');//登录页面链接
define('LOG_TYPE_ACCESS' , 'access');//日志类型：访问日志
define('LOG_TYPE_CUSTOM' , 'custom');//日志类型：自定义日志
define('LOG_TYPE_WRONG_PASSWORD' , 'wrong_password');//日志类型：密码错误
define('DEFAULT_PAGE_COUNT' , 10);//默认每页显示数据量
define('DEFAULT_LANGUAGE' , 'zh-hans');
//默认语言
define('MENU_TYPE_CENTER' , 'tree'); //央服菜单类型
define('MENU_TYPE_SINGLE' , 'leaf'); //单服菜单类型
define('LOG_DIR' , ROOT . '/runtime/log/');
define('RPC' , ROOT . '/../rpc/');
define('SOCKET', ROOT . '/../socket/');
global $__global;
//不需要权限的页面
$__global['noPermissionPage'] = array('index_verification_code' , 'index_login' ,'index_user_status');
//用户登录成功但不需要后台设置权限的页面
$__global['mustPassPage'] = array(
    'index_index',
    'index_item_info',
    'index_setChannelDisplay',
);
require_once __DIR__ . '/db.php';

//检查是否在开发环境下
function is_dev(){
    if(defined('ENVIRONMENT') && ENVIRONMENT == 'DEV'){
        return true;
    }
    return false;
}
