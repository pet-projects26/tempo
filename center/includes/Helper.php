<?php
!defined('IN_WEB') && exit('Access Denied');

/**
 * 主要是一些Common的方法
 * @date 2017-06-07
 * @author [赖伟杰] <[<laiwejiemu@qq.com>]>
 */
class Helper
{

    /**
     * [生成玩家帐号]
     * @param  [string] $account [玩家帐号]
     * @param  [string] $channel [渠道]
     * @return [string]          [返回玩家帐号]
     */
    public static function makeAccount($account, $channel)
    {
        if ($channel == 'huasheng') {
            return $account;
        }

        return $channel . '_' . $account;
    }

    /**
     * 获取IP
     *
     * @return string
     */
    public static function get_ip()
    {
        if (isset ($_SERVER ['HTTP_CLIENT_IP'])) {
            $hdr_ip = stripslashes($_SERVER ['HTTP_CLIENT_IP']);
        } else {
            if (isset ($_SERVER ['HTTP_X_FORWARDED_FOR'])) {
                $hdr_ip = stripslashes($_SERVER ['HTTP_X_FORWARDED_FOR']);
            } else {
                $hdr_ip = stripslashes($_SERVER ['REMOTE_ADDR']);
            }
        }
        return $hdr_ip;
    }

    /**
     * json 输出
     *
     * @param $ret
     * @param array $data_array
     * @param bool $is_jsonp
     * @param bool $is_die
     * @param string $charset
     */
    public static function output_json($ret, $data_array = array(), $is_jsonp = false, $is_die = true, $charset = 'UTF-8')
    {
        $result = '';
        $callback = '';
        $data = array(
            'errcode' => $ret,
            'data' => $data_array
        );
        if ($is_jsonp) {
            $callback = $is_jsonp;
            $result = $callback . '(' . json_encode($data) . ')';
        } else {
            $result = json_encode($data);
            @header('Content-type: application/json; charset=' . $charset);
        }

        echo $result;

        if ($is_die) {
            die();
        }
    }

    /**
     * 字符编码检测转换
     *
     * @param $string
     * @param string $charset
     * @return string
     */
    function protect_encoding($string, $charset = "UTF-8")
    {
        $encode = mb_detect_encoding($string, array('GB2312', 'GBK', 'UTF-8'));
        if (in_array($encode, array('GB2312', 'GBK'))) {
            $string = @iconv('GBK', 'UTF-8//IGNORE', $string);
        }
        return $string;
    }


    /**
     *
     * 导出cvs格式
     * @param string $filename
     * @param array $header
     * @param array $bodyData
     * @param array $array_text
     * @param int $flag
     */
    public static function toCsv($filename, $header, $bodyData, $arrayText = array(), $flag = 1)
    {
        ini_set('memory_limit', '2048M');
        // 输出Excel文件头，可把user.csv换成你要的文件名
        if (!headers_sent()) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
            header('Cache-Control: max-age=0');
        }
        // 打开PHP文件句柄，php://output 表示直接输出到浏览器 
        $fp = fopen('php://output', 'a');
        foreach ($header as $i => $v) {
            // CSV的Excel支持GBK编码，一定要转换，否则乱码 
            $header[$i] = iconv('utf-8', 'gbk', $v);
        }
        // 将数据通过fputcsv写到文件句柄   
        if ($flag == 1) {
            fputcsv($fp, $header);
        }
        // 逐行取出数据，不浪费内存 
        if (!empty($bodyData) && is_array($bodyData)) {
            foreach ($bodyData as $row) {
                foreach ($row as $i => $v) {
                    $row[$i] = iconv('utf-8', "gbk//IGNORE", $v);
                    if (in_array($i, $arrayText)) {
                        $row[$i] = "\t" . $row[$i];
                    }
                }
                fputcsv($fp, $row);
            }
        }
    }

    /**
     *
     * 导出csv
     * @param string $filename
     * @param array $bodyData
     * @param bool $csv
     * @param array $arrayText
     * @param int $flag
     */
    public static function formatCSV($filename, $bodyData, $csv = false, $header = array(), $arrayText = array(), $flag = 1)
    {
        if ((!isset($_GET['csv'])) && ($csv !== true)) {
            return false;
        }

        if (empty($header)) {
            if (!isset($_GET['header']) || empty($_GET['header'])) {
                return false;
            }
            $h = trim($_GET['header']);
            $header = explode(',', $h);
        }

        self::toCsv($filename, $header, $bodyData);
        die();

    }

    /**
     *
     * @param array $arr 要排序的数组
     * @param array $key 根据该key进行排序
     */
    public static function sortArrByKey($arr, $key)
    {
        if (!is_array($arr) || empty($arr)) {
            return $arr;
        }

        if (!is_array($arr) || empty($arr)) {
            return $arr;
        }

        $tmp = array();
        foreach ($key as $k) {
            foreach ($arr as $k2 => $row) {
                $tmp[$k2][$k] = $row[$k];
            }
        }

        return $tmp;

    }


    /**
     * 检测是否按顺序输入
     * @param array $model
     */
    public static function arrayEnterBySort($model)
    {
        reset($model);
        $tmp = $model;
        while (list($key, $value) = each($model)) {
            unset($tmp[$key]);
            if (empty($value)) {
                $filter = array_filter($tmp); //删除数组中所有false的值
                if (!empty($filter)) {
                    return false;
                }
            }
        }
        return true;
    }


    public static function urlEnCodeArr($vars)
    {
        return is_array($vars) ? array_map(array(__CLASS__, __FUNCTION__), $vars) : urlencode($vars);
    }

    /**
     *
     * 输入出中文的json
     * @param array $vars
     */
    public static function chineseJson($vars)
    {
        $vars = self::urlEnCodeArr($vars);
        return urldecode(json_encode($vars));
    }

    /**
     *
     * 对数据进行base64_endcode
     * @param array|string $vars
     * @return Ambigous <multitype:, string>
     */
    public static function base64Encode($vars)
    {
        return is_array($vars) ? array_map(array(__CLASS__, __FUNCTION__), $vars) : base64_encode($vars);
    }

    /**
     *
     * 对数据进行base64_decode
     * @param array|string $vars
     */
    public static function base64Decode($vars)
    {
        return is_array($vars) ? array_map(array(__CLASS__, __FUNCTION__), $vars) : base64_decode($vars);
    }


    /**
     * 添加魔鬼符号
     * @param unknown_type $vars
     */
    public static function addQuotes($vars)
    {
        return is_array($vars) ? array_map(array(__CLASS__, __FUNCTION__), $vars) : addslashes($vars);
    }

    /**
     * 去除魔术符号
     * @param unknown_type $vars
     */
    public static function stripQuotes($vars)
    {
        return is_array($vars) ? array_map(array(__CLASS__, __FUNCTION__), $vars) : stripslashes($vars);
    }


    /**
     *
     * http请求
     * @param string $url
     * @param string $post_string
     * @param string $method
     * @param int $connectTimeout
     * @param int $readTimeout
     */
    public static function httpRequest($url, $post_string, $method = "post", $connectTimeout = 5, $readTimeout = 5, $header = FALSE)
    {
        $method = strtolower($method);
        if ($method == "get") {
            $url = $url . "?" . $post_string;
        }

        if (function_exists('curl_init')) {
            $timeout = $connectTimeout + $readTimeout;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connectTimeout);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            if ($method == "post") {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
                if ($header) {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json; charset=utf-8 ',
                            'Content-Length: ' . strlen($post_string))
                    );
                }
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'API PHP5 Client (curl) ' . phpversion());
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);

            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($result === false) {
                //error_log('c=' . @$_GET['c'] . '&a=' . @$_GET['a'] . '&url=' . @$url . @curl_error($ch) . "\n", 3, 'curl_errors.log');
            }

            curl_close($ch);
        } else {
            $result = self::socketPost($url, $post_string, $method, $connectTimeout, $readTimeout);
        }
        return $result;
    }

    /**
     *
     * @param string $url
     * @param json $data_string
     */
    public static function httpPostJson($url, $data_string)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        // 默认不输出
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Content-Length: ' . strlen($data_string))
        );

        $resultData = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        var_dump(123);

        echo $url;

        var_dump($code);

        if ($code !== 200) {
            $resultData = FALSE;
        }

        curl_close($ch);
        return $resultData;
    }


    /**
     * Socket请求
     */
    public static function socketPost($url, $post_string, $method = "post", $connectTimeout = 1, $readTimeout = 2)
    {
        $urlInfo = parse_url($url);
        $urlInfo["path"] = ($urlInfo["path"] == "" ? "/" : $urlInfo["path"]);
        $urlInfo["port"] = !isset($urlInfo["port"]) ? 80 : $urlInfo["port"];
        $urlInfo["port"] = ($urlInfo["port"] == "" ? 80 : $urlInfo["port"]);
        $hostIp = gethostbyname($urlInfo["host"]);

        $urlInfo["request"] = $urlInfo["path"] .
            (empty($urlInfo["query"]) ? "" : "?" . $urlInfo["query"]) .
            (empty($urlInfo["fragment"]) ? "" : "#" . $urlInfo["fragment"]);

        $fsock = fsockopen($hostIp, $urlInfo["port"], $errno, $errstr, $connectTimeout);
        if (false == $fsock) {
            return false;
        }
        if ($method == "get") {
            $post_string = "";
        }
        /* begin send data */
        $in = "POST " . $urlInfo["request"] . " HTTP/1.0\r\n";
        $in .= "Accept: */*\r\n";
        $in .= "User-Agent: API PHP5 Client (non-curl)\r\n";
        $in .= "Host: " . $urlInfo["host"] . "\r\n";
        $in .= "Content-type: application/x-www-form-urlencoded\r\n";
        $in .= "Content-Length: " . strlen($post_string) . "\r\n";
        $in .= "Connection: Close\r\n\r\n";
        $in .= $post_string . "\r\n\r\n";

        stream_set_timeout($fsock, $readTimeout);
        if (!fwrite($fsock, $in, strlen($in))) {
            fclose($fsock);
            return false;
        }
        unset($in);

        //process response
        $out = "";
        stream_set_timeout($fsock, $readTimeout);
        while ($buff = fgets($fsock, 4096)) {
            $out .= $buff;
        }
        //finish socket
        fclose($fsock);
        $pos = strpos($out, "\r\n\r\n");
        $head = substr($out, 0, $pos);      //http head
        $status = substr($head, 0, strpos($head, "\r\n"));      //http status line
        $body = substr($out, $pos + 4, strlen($out) - ($pos + 4));      //page body
        if (preg_match("/^HTTP\/\d\.\d\s([\d]+)\s.*$/", $status, $matches)) {
            if (intval($matches[1]) / 100 == 2) {//return http get body
                return $body;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function result($arr, $key = '')
    {
        if (empty($arr)) {
            return $arr;
        }
        if (empty($key)) {
            return $arr;
        }
        $tmp = array();
        foreach ($arr as $k => $r) {
            $tmp[$r[$key]] = $r;
        }
        return $tmp;
    }


    /**
     * 写入日志
     * @param $content 内容
     * @param $filename 文件名
     * @return 写入日志
     */
    public static function log($content, $filename, $type = 'info', $path = 'api', $isName = true)
    {
        $logPath = dirname(dirname(dirname(__FILE__))) . '/logs/' . $path . '/';
        if (!is_dir($logPath)) {
            mkdir($logPath, '0777');
        }
        if (is_array($content) || is_object($content)) $content = json_encode($content);

        if ($isName) {
            $log_file = $logPath . $filename . "-" . date("Ymd") . ".log";
            // $log_file = $logPath .  date("Ymd") . ".log";

        } else {
            $log_file = $logPath . $filename . "-" . ".log";
        }

        $date = date("Y-m-d H:i:s");

        $strlen = file_put_contents($log_file, $date . " [$type] " . $content . "\n", FILE_APPEND);

        @chmod($log_file, 0777);

        return $strlen;
    }


    /**
     * 下载文件在浏览器中
     * @param string $filename
     * @param string $content
     */
    public static function downLoadFile($filename, $content)
    {
        $ua = $_SERVER["HTTP_USER_AGENT"];
        $encoded_filename = urlencode($filename);
        $encoded_filename = str_replace("+", "%20", $encoded_filename);


        header("Content-Type: application/octet-stream");
        if (preg_match("/MSIE/", $_SERVER['HTTP_USER_AGENT'])) {
            header('Content-Disposition:  attachment; filename="' . $encoded_filename . '"');
        } elseif (preg_match("/Firefox/", $_SERVER['HTTP_USER_AGENT'])) {
            header('Content-Disposition: attachment; filename*="' . $filename . '"');
        } else {
            header('Content-Disposition: attachment; filename="' . $filename . '"');
        }

        echo $content;
        die();
    }

    /**
     *
     * 并发去请求url
     * @param array $urls
     * @param int $delay
     */
    public static function rolling_curl($urls, $postData, $method = 'post', $delay = 0.5, $header = false)
    {
        $queue = curl_multi_init();
        $map = array();

        $keys = array();

        foreach ($urls as $k => $url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_NOSIGNAL, true);

            $post_string = isset($postData[$k]) ? $postData[$k] : $postData;

            if (is_array($post_string)) {
                $post_string = http_build_query($post_string);
            }

            if ($method == "post") {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
                if ($header) {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Expect:',
                            'Content-Type: application/json; charset=utf-8 ',
                            'Content-Length: ' . strlen($post_string))
                    );
                }
            }
            curl_multi_add_handle($queue, $ch);

            $map[(string)$ch] = $url;

            $keys[(string)$ch] = $k;

        }


        $responses = $return = array();
        do {
            while (($code = curl_multi_exec($queue, $active)) == CURLM_CALL_MULTI_PERFORM) ;

            if ($code != CURLM_OK) {
                break;
            }


            // a request was just completed -- find out which one
            while ($done = curl_multi_info_read($queue)) {

                // get the info and content returned on the request
                $info = curl_getinfo($done['handle']);

                $error = curl_error($done['handle']);

//              $results = callback(curl_multi_getcontent($done['handle']), $delay);
                $responses[$map[(string)$done['handle']]] = compact('info', 'error', 'results');

                //get content 

                $k = $keys[(string)$done['handle']];

                $return[$k] = curl_multi_getcontent($done['handle']);

                // remove the curl handle that just completed
                curl_multi_remove_handle($queue, $done['handle']);
                curl_close($done['handle']);
            }

            // Block for data in / output; error handling is done by curl_multi_exec
            if ($active > 0) {
                curl_multi_select($queue, $delay);
            }

        } while ($active);

        curl_multi_close($queue);


        return $return;
    }


    /**
     * [用于加密，解密码]
     * @param  [string]  $string    [字符串]
     * @param  string $operation [方法]
     * @param  string $key [key]
     * @param  integer $expiry [过期时间]
     * @return [type]             [description]
     */
    public static function authcode($string, $operation = 'DECODE', $key = 'eXoyfGhansCg', $expiry = 0)
    {
        // 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙   
        $ckey_length = 10;

        // 密匙   
        $key = md5($key);

        // 密匙a会参与加解密   
        $keya = md5(substr($key, 0, 16));
        // 密匙b会用来做数据完整性验证   
        $keyb = md5(substr($key, 16, 16));
        // 密匙c用于变化生成的密文   
        $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';
        // 参与运算的密匙   
        $cryptkey = $keya . md5($keya . $keyc);
        $key_length = strlen($cryptkey);
        // 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)， 
        //解密时会通过这个密匙验证数据完整性   
        // 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确   
        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
        $string_length = strlen($string);
        $result = '';
        $box = range(0, 255);
        $rndkey = array();
        // 产生密匙簿   
        for ($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }
        // 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上对并不会增加密文的强度   
        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        // 核心加解密部分   
        for ($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            // 从密匙簿得出密匙进行异或，再转成字符   
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if ($operation == 'DECODE') {
            // 验证数据有效性，请看未加密明文的格式   
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            // 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因   
            // 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码   
            return $keyc . str_replace('=', '', base64_encode($result));
        }
    }

    /**
     * 较验
     * @param  [array] $params [数组]
     * @param  [string] $sign  [要较验的值]
     * @param  [string] $key   [key]
     * @return [bool]        [description]
     */
    public static function checkSign($params, $key, $sign = false, $separator = '|')
    {
        ksort($params);

        unset($params['sign']);

        $str = "";
        foreach ($params as $key => $value) {
            $str .= $key . '=' . $value . $separator;
        }

        $str .= $key;

        if ($sign == false) {
            return md5($str);
        }

        $mySign = md5($str);

        if ($mySign == $sign) {
            return true;
        }

        return false;
    }

    /**
     * [获取错误码]
     * @param  int $code [错是误码]
     * @param  string|enum $type [类型]
     * @return 错误码信息
     */
    public static function getCode($code = '', $type = 'json')
    {
        $path = dirname(dirname(dirname(__FILE__)));
        $config = include rtrim($path, '/') . '/api/conf/Code.php';
        if (empty($code)) {
            $vars = $config;
        } else {
            $vars = isset($config[$code]) ? $config[$code] : array();
        }

        if ($type == 'json') {
            return self::chineseJson($vars);
        }

        return $vars;
    }

    /**
     * 生成uuid
     * @return [type] [description]
     */
    public static function uuid()
    {
        $charid = md5(uniqid(mt_rand(), true));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);
        return $uuid;
    }

    /**
     * [将数字转为mongo 64位]
     * @param  [array] $vars [数组]
     * @return [array]       [返回数组]
     */
    public static function mongoInt64($vars)
    {
        return $vars;
        // if (is_array($vars)) {
        //     foreach ($vars as $key  => &$row ) {
        //         if (is_array($row)) {
        //             return array_map(array(__CLASS__, __FUNCTION__), $vars);
        //         } else {
        //             if (is_numeric($row) && strlen($row) >= 12) {
        //                 $row = new MongoInt64($row);
        //             }
        //         }
        //     }
        // }
        // unset($row);

        // return $vars;
    }

    /**
     * 获取版本2的路径
     * @param  string $c [控制器]
     * @param  string $m [方法]
     * @return [type]    [description]
     */
    public static function v2($c = 'index', $m = 'index')
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $server_name = $_SERVER['HTTP_HOST'];
        } else {
            $file = dirname(dirname(ROOT)) . '/includes/Config.php';
            $config = include $file;
            $server_name = $config['DOMAIN'];
        }

        if ($server_name == 'dpyh.center.net' || $server_name == 'localhost') {
            $server_name = "http://localhost/dev/html/v2/index.php/";
        } else {
            //
            $server_name = 'http://' . $server_name . "/v2/index.php/";
        }

        $server_name = $server_name . $c . '/' . $m;

        return $server_name;
    }

    /**
     * rpc通知
     * @param  [string] $url    [服务器]
     * @param  [array] $rs     [要更新的服]
     * @param  [string] $method [执行的活动]
     * @param  [array]  $param  [参数]
     * @return [type]         [description]
     */
    public static function rpc($url, $rs, $method, $param = array())
    {
        $urls = $postData = array();

        foreach ($rs as $sign => $code) {
            $urls[$sign] = $url . '?sign=' . $sign;

            $post = isset($param[$sign]) ? $param[$sign] : $param;

            $postData[$sign] = array(
                'tk' => md5($sign . 'Y0FG2GR8'),
                'method' => $method,
                'param' => $post,
            );
        }

        return self::rolling_curl($urls, $postData);
    }

    public static function psIdByServerid($server)
    {

        if (is_array($server)) {
            $server = "'" . implode("','", $server) . "'";
        } else if (is_string($server)) {
            $server = '\'' . $server . '\'';
        }

        $sql = "select server_id, channel_num ,num from ny_server where server_id in ($server)";

        $result = (new Model())->query($sql);

        $rs = array();

        foreach ($result as $k => $v) {
            //$psId = (($v['channel_num'] + 1) * 100000) + $v['num'];
            $psId = Pact::psId($v['channel_num'], $v['num']);

            $rs[$v['server_id']] = $psId;
            unset($psId);
        }

        return $rs;

    }

    /**
     * 分服分包数据数据显示提示
     * <a id="tb" class="tag"  href="http://www.17sucai.com" tips="17素材网是一个专门分享jquery资源、jquery插件、jquery特效的一个网站。">17素材网-测试上边</a>
     * @param array $data
     * @param string $sign
     * @param closure $value
     */
    public static function setTips($data, $sign, $value = null)
    {
        $span = '';
        $sum_num = 0;
        $title = '';
        if (empty($data)) {
            return $span;
        }
        //比率
        if (is_null($sign) && !is_null($value)) {
            $msg = $value($sign);
            $sum_num = $msg['sum_num'];
            $title = $msg['title'];
        } else {
            foreach ($data as $key => $val) {
                $sum_num += $val[$sign];
                $title .= $key . ":" . $val[$sign] . '<br/>';
            }
        }
        //$span = '<span data-html="true" title="Popover title" data-container="body" data-toggle="popover" data-placement="bottom" data-content="'.$title.'">'.$sum_num.'</span>';
        $span = '<span class="tb_tips_popover tag" tips="' . $title . '">' . $sum_num . '<span>';
        return $span;
    }
    //end
}