<?php

class JPush{
    public $app_key = '';
    public $master_secret = '';
    const PUSH_URL = 'https://api.jpush.cn/v3/push';
    const REPORT_URL = 'https://report.jpush.cn/v3/received';
    public function send($content){
        (!$content || !is_string($content)) && exit('expect a non-empty string');
        $auth_string = base64_encode($this->app_key . ':' . $this->master_secret);
        $header = array('Authorization: Basic ' . $auth_string);
        $rc = new RollingCurl();
        $rc->headers = $header;
        $rc->post(self::PUSH_URL , $content);
        return $rc->execute();
    }

    public function report($msg_ids){
        !is_array($msg_ids) && $msg_ids = array($msg_ids);
        $msg_ids = implode(',' , $msg_ids);
        $url = self::REPORT_URL . '?msg_ids=' . $msg_ids;
        $auth_string = base64_encode($this->app_key . ':' . $this->master_secret);
        $header = array('Authorization: Basic ' . $auth_string);
        $rc = new RollingCurl();
        $rc->headers = $header;
        $rc->get($url);
        $rs = json_decode($rc->execute() , true);
        $result = array();
        if(!$rs || isset($rs['error'])){
            return $result;
        }
        foreach($rs as $v){
            $result[$v['msg_id']] = array(
                'android' => $v['android_received'] === null ? 'null' : $v['android_received'],
                'ios' => $v['ios_apns_sent'] === null ? 'null' : $v['ios_apns_sent']
            );
        }
        return $result;
    }
}
