<?php
/**
 * MyISAM分表类  (弃用 腾讯云不支持MyISAM引擎
 * Created by PhpStorm.
 * User: w
 * Date: 2019/5/20
 * Time: 13:56
 */

class MergeSqlUtil extends Model
{
    private $newTableName = '';
    private $FatherTableName = '';
    private $incNumber = 0;
    private $UNIONSTR = '';

    public function __construct($table)
    {
        parent::__construct($table);
        $this->alias = 't';
    }

    public function createNewMergeTable($number = 200000)
    {
        $number = (int)$number;

        if (!is_numeric($number) || $number < 0) {
            $number = 200000;
        }

        $tableName = $this->prefix . $this->tableName;

        $this->FatherTableName = $tableName;

        $allSonTables = [];

        $theLastTable = '';

        //查询分表数量
        $sql = "SHOW TABLES LIKE '" . $tableName . "_son_%'";
        $res = $this->query($sql);

        if (empty($res)) {
            $this->newTableName = $tableName . '_son_1';
            $this->incNumber = 1;
            $this->UNIONSTR = '`' . $this->newTableName . '`';
            $this->create_table($this->FatherTableName, $this->newTableName, $this->incNumber, $this->UNIONSTR);

            return $this->newTableName;
        }

        foreach ($res as $key => $val) {
            $data = $val["Tables_in_" . MYSQL_DB . " ({$tableName}_son_%)"];
            $arr = explode($tableName . '_son_', $data);
            if (count($arr) > 0) {
                if (is_numeric($arr[1])) {
                    $allSonTables[$key] = $data;
                    $theLastTable = $val["Tables_in_" . MYSQL_DB . " ({$tableName}_son_%)"];
                }
            }
        }

        //查询总表是否正常 不正常先创建总表
        if (!$this->query("SHOW TABLES LIKE '" . $this->FatherTableName . "'") && !empty($allSonTables)) {
            $UNIONSTR = '`' . implode('`,`', $allSonTables) . '`';
            $this->create_father_table($this->FatherTableName, $UNIONSTR);
        }


        $countSql = "select count(*) as count from $theLastTable";

        $theLastTableCount = $this->fetchOne($countSql);

        $theLastTableCount = $theLastTableCount['count'];

        if ($theLastTableCount && $theLastTableCount >= $number) {
            //建新子表
            //获取自增id
            $sql = "select max(id) as inc_number from " . $theLastTable;

            $inc = $this->fetchOne($sql);

            $this->incNumber = $inc['inc_number'] ? (int)$inc['inc_number'] + 1 : 1;

            //获取新表名
            $newCode = substr(strrchr($theLastTable, '_'), 1);
            $newCode += 1;
            $this->newTableName = $tableName . '_son_' . $newCode;
            $allSonTables[] = $this->newTableName;
            //获取新的UNION
            $this->UNIONSTR = '`' . implode('`,`', $allSonTables) . '`';

            $this->create_table($this->FatherTableName, $this->newTableName, $this->incNumber, $this->UNIONSTR);
        } else {
            $this->newTableName = $theLastTable;
        }

        return $this->newTableName;
    }

    private function create_table($tableName, $newTableName, $incNumber, $UNIONSTR)
    {
        try {
            //建新表
            $sonFunction = "create_son_table_of_" . $this->tableName;

            $fatherFunction = "create_table_of_" . $this->tableName;

            if (method_exists('MergeSqlUtil', $sonFunction) && method_exists('MergeSqlUtil', $fatherFunction)) {
                //子表
                $this->$sonFunction($newTableName, $incNumber);
                //总表
                $this->$fatherFunction($tableName, $UNIONSTR);
            } else {
                throw new Exception("类MergeSqlUtil中" . $sonFunction . "方法或者" . $fatherFunction . "方法不存在");
            }

        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    private function create_father_table($tableName, $UNIONSTR)
    {
        try {

            $fatherFunction = "create_table_of_" . $this->tableName;

            if (method_exists('MergeSqlUtil', $fatherFunction)) {
                //总表
                $this->$fatherFunction($tableName, $UNIONSTR);
            } else {
                throw new Exception("类MergeSqlUtil中" . $fatherFunction . "方法不存在");
            }

        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    private function addMergerTableUNION($tableName, $UNIONSTR)
    {
        $sql = "ALTER TABLE $tableName ENGINE=MRG_MYISAM UNION=($UNIONSTR) INSERT_METHOD=LAST;";
        $this->query($sql);
    }

    private function create_table_of_node($tableName, $UNIONSTR)
    {
        if ($this->query("SHOW TABLES LIKE '" . $tableName . "'")) {
            $this->addMergerTableUNION($tableName, $UNIONSTR);
            return 1;
        }

        $sql = "
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `channel` varchar(20) DEFAULT NULL,
                `package` varchar(20) DEFAULT NULL,
                `server` varchar(20) DEFAULT '0' COMMENT '服务器',
                `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
                `ip` varchar(30) DEFAULT NULL COMMENT 'ip',
                `account` varchar(50) DEFAULT '0' COMMENT '账号',
                `role_id` double(50,0) DEFAULT '0',
                `status` tinyint(2) DEFAULT NULL COMMENT '节点',
                `first` tinyint(2) NOT NULL DEFAULT '0' COMMENT '为1则为新增用户',
                `create_time` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `id` (`id`),
                KEY `channel` (`channel`),
                KEY `package` (`package`),
                KEY `server` (`server`),
                KEY `create_time` (`create_time`),
                KEY `first` (`first`) USING BTREE
            ) ENGINE=MRG_MyISAM DEFAULT CHARSET=utf8 INSERT_METHOD=LAST UNION=($UNIONSTR);";

        return $this->query($sql);
    }

    private function create_son_table_of_node($newTableName, $incNumber)
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `$newTableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `channel` varchar(20) DEFAULT NULL,
                `package` varchar(20) DEFAULT NULL,
                `server` varchar(20) DEFAULT '0' COMMENT '服务器',
                `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
                `ip` varchar(30) DEFAULT NULL COMMENT 'ip',
                `account` varchar(50) DEFAULT '0' COMMENT '账号',
                `role_id` double(50,0) DEFAULT '0',
                `status` tinyint(2) DEFAULT NULL COMMENT '节点',
                `first` tinyint(2) NOT NULL DEFAULT '0' COMMENT '为1则为新增用户',
                `create_time` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `id` (`id`),
                KEY `channel` (`channel`),
                KEY `package` (`package`),
                KEY `server` (`server`),
                KEY `create_time` (`create_time`),
                KEY `first` (`first`) USING BTREE
            ) ENGINE=MyISAM AUTO_INCREMENT=$incNumber DEFAULT CHARSET=utf8;";

        return $this->query($sql);
    }

    private function create_table_of_talk($tableName, $UNIONSTR)
    {
        if ($this->query("SHOW TABLES LIKE '" . $tableName . "'")) {
            $this->addMergerTableUNION($tableName, $UNIONSTR);
            return 1;
        }

        $sql = "
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
                `package` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
                `psId` double(50,0) NOT NULL DEFAULT '0' COMMENT '服务器psId',
                `account` varchar(250) NOT NULL DEFAULT '' COMMENT '账号',
                `role_id` double(50,0) NOT NULL COMMENT '角色id',
                `role_name` varchar(100) NOT NULL DEFAULT '0' COMMENT '角色名',
                `role_level` int(11) NOT NULL COMMENT '角色等级',
                `money` int(11) NOT NULL DEFAULT '0' COMMENT '玩家充值数',
                `content` text NOT NULL COMMENT '聊天内容',
                `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1九洲 2本服',
                `ip` varchar(32) NOT NULL COMMENT '用户ip',
                `create_time` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
                PRIMARY KEY (`id`),
                KEY `create_time` (`create_time`) USING BTREE,
                KEY `package` (`package`) USING BTREE,
                KEY `account` (`account`) USING BTREE,
                KEY `role_id` (`role_id`) USING BTREE,
                KEY `psId` (`psId`) USING BTREE
            ) ENGINE=MRG_MyISAM DEFAULT CHARSET=utf8 INSERT_METHOD=LAST UNION=($UNIONSTR);";

        return $this->query($sql);
    }

    private function create_son_table_of_talk($newTableName, $incNumber)
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `$newTableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
                `package` int(11)  unsigned NOT NULL DEFAULT '0' COMMENT '包号',
                `psId` double(50,0) NOT NULL DEFAULT '0' COMMENT '服务器psId',
                `account` varchar(250) NOT NULL DEFAULT '' COMMENT '账号',
                `role_id` double(50,0) NOT NULL COMMENT '角色id',
                `role_name` varchar(100) NOT NULL DEFAULT '0' COMMENT '角色名',
                `role_level` int(11) NOT NULL COMMENT '角色等级',
                `money` int(11) NOT NULL DEFAULT '0' COMMENT '玩家充值数',
                `content` text NOT NULL COMMENT '聊天内容',
                `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1九洲 2本服',
                `ip` varchar(32) NOT NULL COMMENT '用户ip',
                `create_time` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
                PRIMARY KEY (`id`),
                KEY `create_time` (`create_time`) USING BTREE,
                KEY `package` (`package`) USING BTREE,
                KEY `account` (`account`) USING BTREE,
                KEY `role_id` (`role_id`) USING BTREE,
                KEY `psId` (`psId`) USING BTREE
            ) ENGINE=MyISAM AUTO_INCREMENT=$incNumber DEFAULT CHARSET=utf8;";

        return $this->query($sql);
    }

    private function create_table_of_active_account($tableName, $UNIONSTR)
    {
        if ($this->query("SHOW TABLES LIKE '" . $tableName . "'")) {
            $this->addMergerTableUNION($tableName, $UNIONSTR);
            return 1;
        }

        $sql = "
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `channel` varchar(20) DEFAULT NULL,
                `package` varchar(20) DEFAULT NULL,
                `server` varchar(20) DEFAULT '0' COMMENT '服务器',
                `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
                `ip` varchar(30) DEFAULT '0' COMMENT 'ip地址',
                `account` varchar(50) DEFAULT '0' COMMENT '账号',
                `create_time` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `id` (`id`),
                KEY `channel` (`channel`),
                KEY `package` (`package`),
                KEY `server` (`server`),
                KEY `create_time` (`create_time`)
            ) ENGINE=MRG_MyISAM DEFAULT CHARSET=utf8 INSERT_METHOD=LAST UNION=($UNIONSTR);";

        return $this->query($sql);
    }

    private function create_son_table_of_active_account($newTableName, $incNumber)
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `$newTableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `channel` varchar(20) DEFAULT NULL,
                `package` varchar(20) DEFAULT NULL,
                `server` varchar(20) DEFAULT '0' COMMENT '服务器',
                `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
                `ip` varchar(30) DEFAULT '0' COMMENT 'ip地址',
                `account` varchar(50) DEFAULT '0' COMMENT '账号',
                `create_time` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `id` (`id`),
                KEY `channel` (`channel`),
                KEY `package` (`package`),
                KEY `server` (`server`),
                KEY `create_time` (`create_time`)
            ) ENGINE=MyISAM AUTO_INCREMENT=$incNumber DEFAULT CHARSET=utf8;";

        return $this->query($sql);
    }
}