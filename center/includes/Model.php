<?php

class Model
{
    protected static $db;   //数据库连接句柄
    protected $tableName;    //表名
    protected $alias = 'tmptable';        //表别名
    protected $joinTable;    //预定义联表信息，过滤不在定义字段内的数据
    protected $limit;        //查询位置
    protected $prefix;        //表前缀

    use tChannelGroupServerAccess;//多继承server方法

    public function __construct($tableName = null)
    {
        $tableName && $this->tableName = $tableName;

        $this->prefix = !empty($prefix) ? $prefix : MYSQL_PREFIX;
        if (!$this->Db()) {
            $this->setDb();
            $this->Db()->setPrefix($this->prefix);
        }
        $this->limit = null;

        return $this->Db;
    }

    /**
     * @param string 重新定义的表前缀
     */
    public function setNewPrefix($prefix)
    {
        $this->prefix = !empty($prefix) ? $prefix : MYSQL_PREFIX;
        $this->Db()->setPrefix($this->prefix);
    }

    /**
     * [创建别名]
     * @param [string] $alias [description]
     */
    public function setAlias($alias)
    {
        return $this->alias = $alias;
    }

    protected function setDb()
    {
        self::$db = new MysqliDb(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWD, MYSQL_DB, MYSQL_PORT, 'utf8');
    }

    //获取句柄
    protected function Db()
    {
        return self::$db;
    }

    //插入语句，返回插入id
    public function add(array $data)
    {
        // if($this->has($data)){
        //     return false;
        // }
        // else{
        return $this->Db()->insert($this->tableName, $data);
        // }
    }

    //插入语句，返回插入id
    public function lastInsertId()
    {
        return $this->Db()->getInsertId();;
    }

    /**
     * @param  array 批量增加的数据
     * @return [type]
     */
    public function multiAdd(array $data)
    {
        $first = current($data);
        $total = count($first);
        $placeArr = array_fill(0, $total, '?');
        $valuesData = array();
        $placeData = array();
        $fields = array_keys($first);
        foreach ($data as $v) {
            $placeData[] = '(' . join(',', $placeArr) . ')';
            foreach ($v as $value) {
                $valuesData[] = $value;
            }
        }
        $sql = ' INSERT INTO ' . $this->prefix . $this->tableName . '( `' . implode('`,`', $fields) . '` ) VALUES ' . implode(',', $placeData);
        return $this->query($sql, $valuesData);
    }

    //替换语句，返回插入id
    public function replace(array $data)
    {
        return $this->Db()->replace($this->tableName, $data);
    }

    //更新语句，返回行数
    public function update(array $data, $where)
    {
        $this->buildWhere($where);
        $this->Db()->update($this->tableName, $data);
        return $this->Db()->count;
    }

    //删除语句
    public function delete($where)
    {
        $this->buildWhere($where);
        return $this->Db()->delete($this->tableName);
    }

    //执行一条指定的SQL语言
    public function query($sql = '', $value = null)
    {
        if ($sql) {
            return $this->Db()->rawQuery($sql, $value);
        } else {
            return false;
        }
    }

    /**
     * [返回一条数据]
     * @param  [string] $sql   [sql语句]
     * @param  [] $value [description]
     * @return array  一维
     */
    public function fetchOne($sql, $value = null)
    {
        $res = $this->query($sql, $value);
        if (empty($res)) {
            return false;
        }

        if (is_array($res)) {
            return $res[0];
        }

        return false;
    }

    //查询指定表中是否存有对应条件的记录
    public function has($where)
    {
        $this->buildWhere($where);
        return $this->Db()->has($this->tableName . ' as ' . $this->alias);
    }

    //获取指定条件的多条记录
    public function getRows($fields = '*', $where = array(), array $extends = array(), $count = true)
    {
        $this->buildJoin($fields);
        $this->buildWhere($where);
        $this->buildExtends($extends);
        $count && $this->Db()->withTotalCount();
        return $this->Db()->get($this->tableName . ' as ' . $this->alias, $this->limit, $fields);
    }

    //获取指定条件的一条记录
    public function getRow($fields = '*', $where = array(), array $extends = array())
    {
        $this->buildJoin($fields);
        $this->buildWhere($where);
        $this->buildExtends($extends);
        return $this->Db()->getOne($this->tableName . ' as ' . $this->alias, $fields);
    }

    public function getCount()
    {
        return $this->Db()->totalCount;
    }

    //查询两个字段并把第一个字段值作为key,第二个字段值作为value的关联数组
    public function getFieldAssoc(array $fields = array(), $where = array(), $extends = array(), $count = true)
    {
        $this->buildJoin($fields);
        $this->buildWhere($where);
        $this->buildExtends($extends);
        $count && $this->Db()->withTotalCount();
        $rs = $this->Db()->get($this->tableName . ' as ' . $this->alias, $this->limit, $fields);
        $result = array();
        foreach ($rs as $key => $value) {
            $result[array_shift($value)] = array_pop($value);
        }
        return $result;
    }

    //过滤条件为空数组
    protected function filterWhere(array $where)
    {
        return $where;

        return array_filter($where, function ($var) {
            return $var === '' ? false : true;
        });
    }

    //处理查询条件
    protected function buildWhere($where)
    {
        if (!empty($where) && is_array($where)) {
            $where = $this->filterWhere($where);
            $glue = ' AND ';
            if (array_key_exists('OR', $where)) {
                $where = end($where);
                $glue = 'OR';
            } elseif (array_key_exists('AND', $where)) {
                $where = end($where);
            }
            $val = array();
            $con = array();
            $types = array(
                1 => '/::%LIKE%/', 2 => '/::LIKE%/', 3 => '/::%LIKE/', 4 => '/::IN/',
                5 => '/::NOT_IN/', 6 => '/::BETWEEN/', 7 => '/::NULL/', 8 => '/::NONE/', 9 => '/::/'
            );
            foreach ($where as $key => $value) {
                $num = 0;
                foreach ($types as $k => $v) {
                    if (preg_match($v, $key)) {
                        $num = $k;
                        break;
                    }
                }
                if ($num) {
                    list($field, $type) = explode('::', $key);
                    switch ($num) {
                        case 1:
                            $val[] = $field . ' LIKE ? ';
                            $con[] = '%' . $value . '%';
                            break;
                        case 2:
                            $val[] = $field . ' LIKE ? ';
                            $con[] = $value . '%';
                            break;
                        case 3:
                            $val[] = $field . ' LIKE ? ';
                            $con[] = '%' . $value;
                            break;
                        case 4:
                            if (is_array($value) && !empty($value)) {
                                foreach ($value as $inValue) {
                                    $con[] = $inValue;
                                }
                                $val[] = $field . ' IN ' . '( ' . implode(',', array_fill(0, count($value), ' ? ')) . ')';
                            }
                            break;
                        case 5:
                            if (is_array($value) && !empty($value)) {
                                foreach ($value as $inValue) {
                                    $con[] = $inValue;
                                }
                                $val[] = $field . ' NOT IN ' . '( ' . implode(',', array_fill(0, count($value), ' ? ')) . ')';
                            }
                            break;
                        case 6:
                            $val[] = $field . ' BETWEEN ? AND ? ';
                            $con[] = $value[0];
                            $con[] = $value[1];
                            break;
                        case 7:
                            $val[] = $field . ' IS NULL ';
                            break;
                        case 8:
                            $val[] = $field . ' =  ? ';
                            $con[] = '';
                            break;
                        case 9:
                            $val[] = $field . ' ' . $type . ' ? ';
                            $con[] = $value;
                            break;
                        case 10 :

                            $val[] = "find_in_set(? , $field)";

                            $con[] = $value;
                    }
                } else {
                    $val[] = $key . ' = ?  ';
                    $con[] = $value;
                }
                if (!empty($con)) {
                    $this->Db()->where(implode($glue, $val), $con);
                } elseif (!empty($val)) {
                    $this->Db()->where(implode($glue, $val));
                }
                unset($con);
                unset($val);
            }
        } else {
            is_string($where) && $this->Db()->where($where);
        }
        return true;
    }

    //根据查询字段构建需要的联表信息
    public function buildJoin($fields = '*')
    {
        $joinTable = array();
        if (is_array($fields)) {
            foreach ($fields as $field) {
                if (strripos($field, '.') !== false) {
                    if (preg_match('/distinct[\s]+[\w]+\(([\w\.]+)\)/', $field, $matches)) {
                        $field = $matches[1];
                    } else if (preg_match('/distinct[\s]+([\w\.]+)/', $field, $matches)) {
                        $field = $matches[1];
                    } else if (preg_match('/[\w]+\(([\w\.]+)\)/', $field, $matches)) { //匹配mysql方法里面的参数，例如：sum(o.money),匹配o.money
                        $field = $matches[1];
                    } else if (preg_match('/([\w\.]+)[\s]+as[\s]+[\w]+/', $field, $matches)) {
                        $field = $matches[1];
                    }
                    list($alias, $field) = explode('.', $field);
                    if (array_key_exists($alias, $this->joinTable) && !in_array($alias, $joinTable)) {
                        array_push($joinTable, $alias);
                        $this->Db()->join("{$this->joinTable[$alias]['name']} as {$alias}", "{$this->joinTable[$alias]['on']}", "{$this->joinTable[$alias]['type']}");
                    }
                }
            }
        }
        return true;
    }

    //处理GROUP BY ORDER BY LIMIT 操作
    public function buildExtends($extends = array())
    {
        if (is_array($extends)) {
            if (array_key_exists('GROUP', $extends)) {
                if (is_array($extends['GROUP'])) {
                    foreach ($extends['GROUP'] as $val) {
                        $this->Db()->groupBy($val);
                    }
                } else {
                    $this->Db()->groupBy($extends['GROUP']);
                }
            }
            if (array_key_exists('ORDER', $extends)) {
                foreach ($extends['ORDER'] as $key => $value) {
                    list($field, $type) = explode('#', $value);
                    $this->Db()->orderBy($field, $type);
                }
            }
            if (array_key_exists('LIMIT', $extends)) {
                $this->limit = $extends['LIMIT'];
            }
        }
        return true;
    }

    //获取最后执行SQL语句错误
    public function lastError()
    {
        return $this->Db()->getLastError();
    }

    //获取最后的查询语句
    public function lastQuery()
    {
        return $this->Db()->getLastQuery();
    }

    //调用rpc
    public function rpcCall($host, $port, $method, $param = array())
    {
        require_once RPC . '/rpclib/RpcConst.php';
        require_once RPC . '/call/' . $method . '.php';

        try {
            $return = $method($host, $port, $param);
        } catch (\Exception $e) {
            $return = $e->getMessage();
        }

        return $return;
    }

    //调用websocket
    public function socketCall($host, $port, $method, $param = array())
    {
        require_once SOCKET . '/call/' . $method . '.php';

        try {
            $return = $method($host, $port, $param);
        } catch (\Exception $e) {
            $return = $e->getMessage();
        }

        return $return;
    }

    public function time_format($start_time, $end_time = null)
    {
        empty($end_time) && $end_time = time();
        $time_set = array('天' => 86400, '时' => 3600, '分' => 60, '秒' => 1);
        $duration = $end_time - $start_time;//秒
        foreach ($time_set as $key => $val) {
            $time_set[$key] = floor($duration / $val);
            $duration = $duration % $val;
            if ($time_set[$key] == 0) {
                unset($time_set[$key]);
            } else {
                $time_set[$key] .= $key;
            }
        }
        $duration = implode('', $time_set);
        return $duration;
    }

    public function second_format($second)
    {
        if ($second) {
            $time_set = array('天' => 86400, '时' => 3600, '分' => 60, '秒' => 1);
            foreach ($time_set as $key => $val) {
                $time_set[$key] = floor($second / $val);
                $second = $second % $val;
                if ($time_set[$key] == 0) {
                    unset($time_set[$key]);
                } else {
                    $time_set[$key] .= $key;
                }
            }
            $second = implode('', $time_set);
        }
        return $second;
    }

    public function formatFields($row)
    {
        foreach ($row as $key => $field) {
            $row[$key] = $field . ' ';
        }
        return $row;
    }

    public function setPageData($data, $count, $limit = array())
    {
        $limit && $data = array_slice($data, $limit[0], $limit[1]);
        $data = array(
            'sEcho' => intval($_GET['sEcho']),
            'iTotalRecords' => $count,
            'iTotalDisplayRecords' => $count,
            'aaData' => $data
        );
        echo json_encode($data);
    }

    /**
     * 如果数据为字符串，以","分隔开
     * @version 2017-09-06
     * @author xty
     */
    public function getGm($fields = array(), $server_id = array())
    {
        empty($fields) && $fields = array('s.name', 'sc.ip', 'sc.gm_port', 's.server_id', 's.num');
        $conditions = array();
        if (is_array($server_id)) {
            /*$temp = array();
            if(!empty($server_id)){
                foreach($server_id as $val){
                    $arr=explode(',',$val);
                    $temp = array_merge($temp,$arr);
                }
                $server_id = array_unique($temp);
            }*/
            $conditions['WHERE'] = array();
            $server_id && $conditions['WHERE']['s.server_id::IN'] = $server_id;

            $rs = (new ServerModel())->getRows($fields, $conditions['WHERE']);

            return $rs;
        } else {
            $conditions['WHERE'] = array();
            $server_id && $conditions['WHERE']['s.server_id'] = $server_id;
            return (new ServerModel())->getRow($fields, $conditions['WHERE']);
        }
    }

    //获取单服地址
    public function apiUrl($server_id)
    {
        $server_id = is_array($server_id) ? $server_id : array($server_id);
        $server = (new ServerconfigModel())->getApiUrl($server_id);
        if ($server) {
            $api_url = array();
            foreach ($server as $row) {
                $api_url[$row['server_id']] = $row['api_url'];
            }
            return $api_url;
        } else {
            return null;
        }
    }

    //调用单服方法
    public function remoteCall($class, $method, array $params = array(), $api_url = '', $server_id)
    {
        return Util::remoteCall($api_url, $class, $method, $params, null, $server_id);
    }

    //调用单服Model方法
    public function call($class, $method, array $params = array(), $server_id = '')
    {
        $class = $class . 'Model';

        if ($server_id) {
            Helper::log($server_id, 'txt', $server_id, 'singleCall');

            $api_url = $this->apiUrl($server_id);
            Helper::log($api_url, 'txt', $class, 'singleCall');

            $rs = $api_url ? $this->remoteCall($class, $method, $params, $api_url, $server_id) : false;


            Helper::log($rs, 'txt', $class, 'singleCall');

            if ($rs) {
                foreach ($rs as $k => $row) { //$k是服务器id ，$row是该服务器id的返回内容

                    $rs[$k] = json_decode($row, true);
                }

            }
            return $rs;
        } else {
            return (new $class())->$method($params);
        }
    }

    //合并使用call方法返回多个服务器的数据
    //对$call_result有格式要求：array('server_id1' => array(array() , 0) , 'server_id2' => ...) ， 数组server_id1的第一个值是返回的数据 , 第二个值是数据的条数
    public function callDataMerge($call_result, $server_add = false)
    {
        $data = array();
        $num = 0;
        $server = (new ServerModel())->getServerNameByMmc();

        foreach ($call_result as $k => $v) {

            if (!isset($v[0])) {
                return array();
            }

            foreach ($v[0] as $row) {
                $server_add && $row['call_server_id'] = $server[$k]; //如果$server_add为true则把server_id添加到每一条数据里面
                $data[] = $row;
            }
            $num += $v[1];
        }
        return array($data, $num);
    }


    /************************ 事务 ******************************/
    // public function setAttribute() {
    //     return $this->Db()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // }

    /**
     * 开启事务
     * @return [type] [description]
     */
    public function beginTransaction()
    {
        return $this->Db()->startTransaction();
    }

    /**
     * 事务提交
     * @return [type] [description]
     */
    public function commit()
    {
        return $this->Db()->commit();
    }

    /**
     * 事务回滚
     * @return [type] [description]
     */
    public function rollBack()
    {
        return $this->Db()->rollBack();
    }


    public function formatLTV($str)
    {
        return rtrim(substr($str, strpos($str, '(') + 1),  ')');
    }
}
