<?php
/**
 * 协议
 * Created by PhpStorm.
 * User: w
 * Date: 2018/11/30
 * Time: 18:17
 */

class Pact
{

    //热更新协议类型
    public static $HotUpdateType = array(
        'Nexus' => 0x100000,
        'Feature' => 0x200000,
        'Chat' => 0x300000,
        'Cross' => 0x400000,
        'Map' => 0x500000,
//        'Bg' => 0x600000, //php
        'Center' => 0x700000,
        'CrossT' => 0x800000
    );

    //热更新操作类型
    public static $HotUpdateOperate = array(
        'Bg_Hotupdate' => [0x2100000, '热更新'],            /*热更新*/
        'Bg_ReloadCfg' => [0x2100001, '重载配置']           /*重新加载配置*/
    );

    //GM指令协议
    public static $GMcommandPact = 0x2200000;

    //向服务端发送邮件协议
    public static $MailSendOperate = 0x2700000;

    //支付转账
    public static $SendGoodsOperate = 0x2700001;

    //封禁
    public static $RoleBlockOprate = 0x2100002;

    /*封禁
    const enum Bg_BlockFields {
		type = 0,			//1:封账号 2:封IP 3:禁言 4:封角色ID
		unblock = 1,			//解封
		str = 2,
		id = 3,
		time = 4,			//封禁时间 -1:永久 秒
		content = 5,			//封禁内容
	}
    type Bg_Block = [number, boolean, Array<string>, Array<number>, number, string];
    */
    //强制下线
    public static $RoleForceLogout = 0x2100003;

    //psId
    public static $Constant = array(
        'IdMask' => 0x7FFFFFFF,
        'GroupMask' => 0xFFF,
        'PlatformMask' => 0x3FF,

        'IdLimit' => 0x80000000,
        'GroupLimit' => 0x1000,
        'HighMask' => 0x3FFFFF,
    );

    /***
     * 根据psId获取渠号
     * @param $psId
     * @return int
     */
    public static function channelNum($psId)
    {
        $high = ($psId / self::$Constant['IdLimit']) & self::$Constant['HighMask'];
        return ($high / self::$Constant['GroupLimit']) & self::$Constant['PlatformMask'];
    }

    /**
     * 根据psId获取服务号
     * @param $psId
     * @return int
     */
    public static function serverNum($psId)
    {
        return ($psId / self::$Constant['IdLimit']) & self::$Constant['GroupMask'];
    }

    /**
     * 生成psId | role_id
     * @param $channelNum
     * @param $serverNum
     * @param int $id 为0则是psId
     * @return float|int
     */
    public static function psId($channelNum, $serverNum, $id = 0)
    {
        $high = ($channelNum * self::$Constant['GroupLimit']) + $serverNum;
        return ($high * self::$Constant['IdLimit']) + $id;
    }


    //发送广播
    public static $Broadcast = 0x2100004;
    //删除广播
    public static $DelBroadcast = 0x2100005;

    //意见反馈回复
    public static $FeebackReply = 0x2700002;            /*后台回复意见反馈*/

    //激活码
    public static $CDKEY = array(
        'Produce' => 0x2700003, //产生激活码
        'State' => 0x2700004, //改变激活码状态
        'Del' => 0x2700005, //删除激活码
    );

    //更新公告
    public static $UpdateNotice = 0x2700006; //更新公告

}

//type Bg_Hotupdate = [number, Array<string>];
//type Bg_ReloadCfg = [number];
/*
const enum Constant {
    IdMask = 0x7FFFFFFF,
    GroupMask = 0xFFF,
    PlatformMask = 0x3FF,

    IdLimit = 0x80000000,
    GroupLimit = 0x1000,
    HighMask = 0x3FFFFF,
}

public platformId(id: uint53): uint10 {
    let high = (id / Constant.IdLimit) &Constant.HighMask;
    return (high / Constant.GroupLimit) &Constant.PlatformMask;
}

public groupId(id: uint53): uint12 {
    return (id / Constant.IdLimit) & Constant.GroupMask;
}

public actorId(platform: uint10, group: uint12, id: uint31): uint53 {
    let high = (platform * Constant.GroupLimit) + group;
    return (high * Constant.IdLimit) + id;
}
 * */
