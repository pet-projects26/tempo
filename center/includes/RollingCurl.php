<?php

class RollingCurlRequest{
    public $server = false;
    public $url = false;
    public $method = 'GET';
    public $post_data = null;
    public $headers = null;
    public $options = null;

    function __construct($server , $url, $method = "GET", $post_data = null, $headers = null, $options = null){
        $this->server = $server;
        $this->url = $url;
        $this->method = $method;
        $this->post_data = $post_data;
        $this->headers = $headers;
        $this->options = $options;
    }

    public function __destruct(){
        unset($this->url , $this->method , $this->post_data , $this->headers , $this->options);
    }
}

class RollingCurl{
    private $timeout = 5;
    private $callback;
    private $headers = array();
    private $requests = array();
    protected $options = array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_CONNECTTIMEOUT => 30,
        CURLOPT_TIMEOUT => 60
    );

    function __construct($callback = null){
        $this->callback = $callback;
    }

    public function __destruct(){
        unset($this->callback , $this->options , $this->headers , $this->requests);
    }

    public function __get($name){
        return (isset($this->{$name})) ? $this->{$name} : null;
    }

    public function __set($name , $value){
        if($name == "options" || $name == "headers"){
            $this->{$name} = $value + $this->{$name};
        }
        else{
            $this->{$name} = $value;
        }
        return true;
    }

    public function add($request){
        $this->requests[] = $request;
        return true;
    }

    public function request($server , $url , $method = 'GET' , $post_data = null , $headers = null , $options = null){
        $this->requests[] = new RollingCurlRequest($server , $url , $method , $post_data , $headers , $options);
        return true;
    }

    public function get($url , $headers = null , $options = null){
        return $this->request($url , 'GET' , null , $headers , $options);
    }

    public function post($server , $url , $post_data = null , $headers = null , $options = null){

//        if ($options == null) $options = $this->options;

        return $this->request($server , $url , 'POST' , $post_data , $headers , $options);
    }

    public function execute(){
        return count($this->requests) == 1 ? $this->single_curl() : $this->multi_curl();
    }

    private function get_options($request){
        $options = $this->__get('options');
        if(ini_get('safe_mode') == 'Off' || !ini_get('safe_mode')){
            $options[CURLOPT_FOLLOWLOCATION] = 1;
            $options[CURLOPT_MAXREDIRS] = 5;
        }
        $headers = $this->__get('headers');
        if($request->options){
            $options = $request->options + $options;
        }

        $options[CURLOPT_URL] = $request->url;
        if($request->post_data){
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $request->post_data;
        }
        if($headers){
            $options[CURLOPT_HEADER] = 0;
            $options[CURLOPT_HTTPHEADER] = $headers;
        }
        return $options;
    }

    private function single_curl(){
        $curl = curl_init();
        $request = array_shift($this->requests);
        $options = $this->get_options($request);
        curl_setopt_array($curl , $options);
        $rs = curl_exec($curl);
        $info = curl_getinfo($curl);

        if($this->callback){
            $callback = $this->callback;
            if(is_callable($this->callback)){
                call_user_func($callback , $rs , $info , $request);
            }
        }
        return array($request->server => $rs);
    }

    private function multi_curl(){
        $master = curl_multi_init();
        $request_num = count($this->requests);
        $curls = array();
        for($i = 0;$i < $request_num;$i++){
            $curls[$i] = curl_init();
            $options = $this->get_options($this->requests[$i]);
            curl_setopt_array($curls[$i] , $options);
            curl_multi_add_handle($master , $curls[$i]);
        }
        $running = NULL;
        do{
            usleep(10000);
            $mrs = curl_multi_exec($master , $running);
        }
        while($mrs == CURLM_CALL_MULTI_PERFORM);
        while($running && $mrs == CURLM_OK){
            if(curl_multi_select($master, $this->timeout) != false){
                do{
                    $mrs = curl_multi_exec($master , $running);
                }
                while($mrs == CURLM_CALL_MULTI_PERFORM);
            }
        }
        $rs = array();
        foreach($this->requests as $k => $request){
            $rs[$request->server] = curl_multi_getcontent($curls[$k]);
            if(is_callable($this->callback)){
                $info = curl_getinfo($curls[$k]);
                call_user_func($this->callback , $rs[$request->url] , $info , $request);
            }
        }
        foreach($this->requests as $k => $request){
            curl_multi_remove_handle($master , $curls[$k]);
        }
        curl_multi_close($master);
        return $rs;
    }
}