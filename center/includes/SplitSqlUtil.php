<?php

/***
 * Innodb分表 /根据时间分表
 * Created by PhpStorm.
 * User: w
 * Date: 2019/7/30
 * Time: 16:49
 * Class SplitSqlUtil
 */
class SplitSqlUtil extends Model
{

    public $fields = [
        'node' => ['id', 'channel', 'package', 'server', 'mac', 'ip', 'account', 'role_id', 'status', 'first', 'create_time'],
        'talk' => ['id', 'package', 'psId', 'account', 'role_id', 'role_name', 'role_level', 'money', 'content', 'type', 'ip', 'create_time'],
        'active_account' => ['id', 'channel', 'package', 'server', 'mac', 'ip', 'account', 'create_time']
    ];


    public function __construct($table)
    {
        parent::__construct($table);
        $this->alias = 't';
    }

    public function checkRenameTable()
    {
        //检查是否需要分表
        //判断今天是否是1号
        $today = date('d');

        if ((int)$today !== 1) return false;

        $lastMonth = date('ym', strtotime(date('Y-m-01') . " - 1 month"));

        //判断上个月的表是否创建
        $tableName = $this->prefix . $this->tableName;

        $lastMonthTableName = $tableName.'_'.$lastMonth;

        //查询上月的分表是否存在 如果不存在则把当前表命名为上月表 并创建一个新表作为当月表
        $sql = "SHOW TABLES LIKE '" . $lastMonthTableName . "'";
        $res = $this->query($sql);

        if (empty($res)) {

            if (!method_exists($this, $this->tableName)) {

                Helper::log($this->tableName . '的建表方法不存在', 'checkRenameTable', 'run', 'SplitTableSql');

                return false;
            }

            $this->renameTable($lastMonth);

            Helper::log($this->tableName. '_' . $lastMonth . '表分表成功', 'checkRenameTable', 'run', 'SplitTableSql');
        }
    }

    //无分表时创建分表
    public function checkCreateNewTable($is_millisecond = 0)
    {
        //查询当前表内是否有其他月份的数据 如果有 分出去创建新表并且add进该表
        //获取当前月第一天的时间戳
        $thisMonthTime = strtotime(date('Y-m-01'));

        $data = $this->getLessThisMonthData($thisMonthTime, $is_millisecond);

        if (!empty($data)) {

            $tableData = $data['returnData'];
            $idSet = $data['idSet'];

            $thisTableName = $this->tableName;

            foreach ($tableData as $key => $rows) {

                //判断当前key的分表是否存在
                $tableName = $this->tableName. '_' . $key;

                $thatMonthTableName = $this->prefix . $tableName;

                $this->checkSplitTableExists($thatMonthTableName);

                parent::__construct($tableName);

                //拆分再做插入 防止过大
                $addRows = array_chunk($rows, 1000);
                $delIds = array_chunk($idSet[$key], 1000);

                foreach ($addRows as $row) {
                    $this->multiAdd($row);
                }

                Helper::log($idSet[$key], 'checkCreateNewTable', 'addId'.$this->tableName, 'SplitTableSql');

                parent::__construct($thisTableName);

                foreach ($delIds as $r) {
                    $this->delete(['id::IN' => $r]);
                }
            }
        }
    }

    public function getLessThisMonthData($thisMonthTime, $is_millisecond = 0)
    {
        $is_millisecond && $thisMonthTime *= 1000;

        $where = ['create_time::<' => $thisMonthTime];

        $rs = $this->getRow('id', $where);

        if (!empty($rs)) {

            $table = $this->tableName;

            $fields = $this->fields[$table];

            $data = $this->getRows($fields, $where);

            $returnData = [];

            foreach ($data as $row) {
                if ($is_millisecond) {
                    $thatMonth = date('ym', $row['create_time'] / 1000);
                } else {
                    $thatMonth = date('ym', $row['create_time']);
                }

                $returnData[$thatMonth][] = $row;

                $idSet[$thatMonth][] = $row['id'];
            }

            $data = [
                'returnData' => $returnData,
                'idSet' => $idSet
            ];

            return $data;
        }

        return [];
    }

    //查询表是否创建 如果没有则创建一个
    public function checkSplitTableExists($tableName)
    {
        //查询上月的分表是否存在 如果不存在则把当前表命名为上月表 并创建一个新表作为当月表
        $sql = "SHOW TABLES LIKE '" . $tableName . "'";
        $res = $this->query($sql);

        if (empty($res)) {

            if (!method_exists($this, $this->tableName)) {

                Helper::log($this->tableName . '的建表方法不存在', 'checkRenameTable', 'run', 'SplitTableSql');

                return false;
            }

            $table = $this->tableName;

            $this->$table($tableName);

            Helper::log($tableName . '表建表成功', 'checkRenameTable', 'run', 'SplitTableSql');
        }
    }


    public function returnDateTableName($date)
    {
        if (!$date) {
            return false;
        }

        $toMonth = date('ym');

        if(is_array($date) && $date[0] < $date[1])  {
            $start_time = $date[0];
            $end_time = $date[1];
            //判断是否需要跨月查询

            $toStartMonth = date('ym', $start_time);
            $toEndMonth = date('ym', $end_time);

            if ($toStartMonth == $toEndMonth) {
                if ($toStartMonth == $toMonth) {
                    return $this->tableName;
                } else {
                    $selectMonth = $toStartMonth;
                }
            } else {
                //跨月查询 先不做

            }

        } else {
            //判断是否为当前月
            $selectMonth = date('ym' , strtotime($date));
            if ($selectMonth > $toMonth) {
                return false;
            }
            if ($selectMonth == $toMonth) {
                return $this->tableName;
            }
        }

        $selectTableName = $this->selectTableNameExists($selectMonth);

        return $selectTableName;
    }

    public function selectTableNameExists($selectTime)
    {
        $tableName = $this->prefix . $this->tableName.'_'.$selectTime;

        //查询上月的分表是否存在 如果不存在则创建并且把
        $sql = "SHOW TABLES LIKE '" . $tableName . "'";
        if($this->query($sql)) {
            return $this->tableName. '_' . $selectTime;
        }
        return false;
    }

    public function createTable($tableName)
    {
        $table = $this->tableName;

        $this->$table($tableName);
    }

    public function renameTable($lastMonth)
    {
        $tableName = $this->prefix . $this->tableName;

        $tmpTableName = $tableName.'_tmp';
        //创建临时表
        $table = $this->tableName;

        $this->$table($tmpTableName);

        $lastMonthTableName = $tableName.'_'.$lastMonth;

        $renameSql = "RENAME TABLE ".$tableName." TO ".$lastMonthTableName.", ".$tmpTableName." TO ".$tableName;

        $this->query($renameSql);
    }

    public function node($tableName)
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `channel` varchar(20) DEFAULT NULL,
                `package` varchar(20) DEFAULT NULL,
                `server` varchar(20) DEFAULT '0' COMMENT '服务器',
                `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
                `ip` varchar(30) DEFAULT NULL COMMENT 'ip',
                `account` varchar(50) DEFAULT '0' COMMENT '账号',
                `role_id` double(50,0) DEFAULT '0',
                `status` tinyint(2) DEFAULT NULL COMMENT '节点',
                `first` tinyint(2) NOT NULL DEFAULT '0' COMMENT '为1则为新增用户',
                `create_time` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `id` (`id`),
                KEY `channel` (`channel`),
                KEY `package` (`package`),
                KEY `server` (`server`),
                KEY `create_time` (`create_time`),
                KEY `first` (`first`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        return $this->query($sql);
    }

    public function talk($tableName)
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
                `package` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '包号',
                `psId` double(50,0) NOT NULL DEFAULT '0' COMMENT '服务器psId',
                `account` varchar(250) NOT NULL DEFAULT '' COMMENT '账号',
                `role_id` double(50,0) NOT NULL COMMENT '角色id',
                `role_name` varchar(100) NOT NULL DEFAULT '0' COMMENT '角色名',
                `role_level` int(11) NOT NULL COMMENT '角色等级',
                `money` int(11) NOT NULL DEFAULT '0' COMMENT '玩家充值数',
                `content` text NOT NULL COMMENT '聊天内容',
                `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1九洲 2本服',
                `ip` varchar(32) NOT NULL COMMENT '用户ip',
                `create_time` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
                PRIMARY KEY (`id`),
                KEY `create_time` (`create_time`) USING BTREE,
                KEY `package` (`package`) USING BTREE,
                KEY `account` (`account`) USING BTREE,
                KEY `role_id` (`role_id`) USING BTREE,
                KEY `psId` (`psId`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        return $this->query($sql);
    }

    public function active_account($tableName)
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `channel` varchar(20) DEFAULT NULL,
                `package` varchar(20) DEFAULT NULL,
                `server` varchar(20) DEFAULT '0' COMMENT '服务器',
                `mac` varchar(30) DEFAULT '0' COMMENT 'mac地址',
                `ip` varchar(30) DEFAULT '0' COMMENT 'ip地址',
                `account` varchar(50) DEFAULT '0' COMMENT '账号',
                `create_time` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `id` (`id`),
                KEY `channel` (`channel`),
                KEY `package` (`package`),
                KEY `server` (`server`),
                KEY `create_time` (`create_time`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        return $this->query($sql);
    }
}