<?php

class Util
{


    /**
     * 导出CSV文件
     * @param array $data 数据
     * @param array $header_data 首行数据
     * @param string $file_name 文件名称
     * @return string
     */
    public static function export_csv_2($data = [], $header_data = [], $file_name = '')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $file_name);
        header('Cache-Control: max-age=0');
        $fp = fopen('php://output', 'a');
        if (!empty($header_data)) {
            foreach ($header_data as $key => $value) {
                $header_data[$key] = iconv('utf-8', 'gbk', $value);
            }
            fputcsv($fp, $header_data);
        }
        $num = 0;
        //每隔$limit行，刷新一下输出buffer，不要太大，也不要太小
        $limit = 100000;
        //逐行取出数据，不浪费内存
        $count = count($data);
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                $num++;
                //刷新一下输出buffer，防止由于数据过多造成问题
                if ($limit == $num) {
                    ob_flush();
                    flush();
                    $num = 0;
                }
                $row = $data[$i];
                foreach ($row as $key => $value) {
                    $row[$key] = @iconv('utf-8', 'gbk', $value);
                }
                fputcsv($fp, $row);
            }
        }
        fclose($fp);
    }

    public static function readExcel($filename, $encode = 'utf-8')
    {

        require_once('library/PHPExcel/PHPExcel/IOFactory.php');

        $objReader = PHPExcel_IOFactory::createReaderForFile($filename);


        $objPHPExcel = $objReader->load($filename);


        $sheet = $objPHPExcel->getSheet(0);

        $highestRow = $sheet->getHighestRow(); //取得总行数 

        $highestColumn = $sheet->getHighestColumn(); //取得总列数

        $highestColumn = PHPExcel_Cell::columnIndexFromString($highestColumn);

        for ($row = 1; $row <= $highestRow; $row++) {

            for ($col = 0; $col < $highestColumn; $col++) {

                $excelData[$row][] = (string)$sheet->getCellByColumnAndRow($col, $row)->getValue();
            }
        }
        return $excelData;
    }


    public static function exportExcel(array $data, $filename, $attach = true)
    {
        if (!$data) {
            return;
        }
        $objPHPExcel = new PHPExcel();
        //二维数组， 单个worksheet
        if (!is_array(current(current($data)))) {
            $worksheet = $objPHPExcel->getSheet();

            $row_pos = 1;
            foreach ($data as $row) {
                $col_pos = 0;
                foreach ($row as $cell) {
                    $worksheet->setCellValueByColumnAndRow($col_pos, $row_pos, $cell);
                    $col_pos++;
                }
                $row_pos++;
            }
        } //三维数组，多个worksheet
        else {
            $objPHPExcel->remoteCallveSheetByIndex(0);
            foreach ($data as $title => $content) {
                $title === '' and $title = 'empty';
                $worksheet = new PHPExcel_Worksheet($objPHPExcel, (string)$title);
                $objPHPExcel->addSheet($worksheet);
                $row_pos = 1;
                foreach ($content as $row) {
                    $col_pos = 0;
                    foreach ($row as $cell) {
                        $worksheet->setCellValueByColumnAndRow($col_pos, $row_pos, $cell);
                        $col_pos++;
                    }
                    $row_pos++;
                }
            }
        }

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        if (!$attach) {
            $objWriter->save($filename . '.xls');
        } else {
            ob_end_clean();
            $filename = iconv('UTF-8', 'GBK', $filename);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename={$filename}.xls");
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }
        $objPHPExcel->disconnectWorksheets();
        unset($objPHPExcel);
    }

    public static function exportCsv(array $data, $filename, $attach = true)
    {
        if (!$data) {
            return;
        }
        $file = ROOT . '/../files/' . $filename . '.csv';
        // if(!file_exists($file)){
        $out = fopen($file, 'w');
        foreach ($data as $key => $value) {
            fputcsv($out, $value);
        }
        fclose($out);
        // }
        if ($attach) {
            file_put_contents($file, iconv('UTF-8', 'GBK', file_get_contents($file)));
            header('Content-type:application/vnd.ms-excel');
            header("Content-Disposition: attachment; filename={$filename}.csv");
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
        unlink($file);
        exit;
    }

    public static function getCurl($url, array $params = null, $method = 'get', $option = array())
    {
        $options = array(
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_POST => false
        );
        if ($method == 'post') {
            $options[CURLOPT_POST] = true;
        }
        $options = array_merge($options, $option);
        if ($params) {
            $params = http_build_query($params);
            $separator = strpos($url, '?') === false ? '?' : '&';
            $url .= ($separator . $params);
        }
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $result['content'] = curl_exec($ch);
        $result['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result['errcode'] = curl_errno($ch);
        $result['errror'] = curl_error($ch);
        return $result;
    }

    //通信key生成
    public static function sign(array &$data)
    {
        $data['unixtime'] = time();
        $data['sign'] = md5(CDict::$apiKey . $data['unixtime']);
    }

    public static function fetch($url, array $params)
    {
        if (!$url) {
            return null;
        }
        self::sign($params);
        return self::getCurl($url, $params);
    }

    //获取系统日志目录
    public static function logDir()
    {
        $logDir = realpath(ROOT . '/../log/center/');
        if (!is_writable($logDir)) {
            $logDir = ROOT . '/runtime/';
        }
        return $logDir;
    }

    /**
     * [log 手动写日志方法]
     * @param  [type]  $msg      [日志内容]
     * @param  string $category [日志类型]
     * @param  integer $depth [堆栈帧数量]
     * @return [boolean]            [返回写入状态]
     */
    public static function log($msg, $category = 'info', $depth = 1)
    {
        $debugInfo = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $depth);
        $file = $debugInfo[$depth - 1]['file'];
        $line = $debugInfo[$depth - 1]['line'];

        if (is_array($msg) || is_object($msg)) {
            $msg = print_r($msg, true);
        } else if ($msg === null) {
            $msg = 'NULL';
        } else if ($msg === true) {
            $msg = 'true';
        } else if ($msg === false) {
            $msg = 'false';
        }
        $msg = "[" . date('Y-m-d H:i:s') . "] file $file line $line:\n$msg\n";
        $logfile = self::logDir() . '/' . "$category.txt";
        return @file_put_contents($logfile, $msg, FILE_APPEND);
    }

    /**
     * [remoteCall 远程调用单服方法]
     * @param  [type] $url      [地址]
     * @param  [type] $class    [类方法]
     * @param  [type] $method   [方法]
     * @param  array $params [方法需要的参数]
     * @return [type]           [返回执行的结果]
     */
    public static function remoteCall($url, $class, $method, array $params = array(), $callback = null, $server_id = '')
    {
        if (!$url) {
            throw new Exception('url must not be empty');
        }
        $postData = array(
            'r' => 'call_method',
            'class' => $class,
            'method' => $method,
            'params' => json_encode($params),
            's' => $server_id
        );
        self::sign($postData);
        $rc = new RollingCurl($callback);
        foreach ($url as $k => $v) {
            $rc->post($k, $v, $postData);
        }
        return $rc->execute();
    }

    /**
     * [remoteExecute 远程调用代码]
     * @param  [type] $server [服务器标识]
     * @param  [type] $block  [执行的代码块]
     * @param  array $params [代码参数]
     * @return [type]         [执行结果]
     */
    public static function remoteExecute($server_id, $block, $params = array())
    {
        $url = (new ServerconfigModel())->getApiUrl($server_id);
        if (!$url) {
            throw new Exception('$url must not be empty');
        }
        $postData = array(
            'r' => 'shell',
            'block' => $block,
            'params' => json_encode($params)
        );
        self::sign($postData);
        $rc = new RollingCurl();
        $rc->post($url, $postData);
        return $rc->execute();
    }

    //连接mongo

    /**
     * [mongoConn description]
     * @param  [string] $MONGO_HOST   [地址]
     * @param  [string] $MONGO_PORT   [端口]
     * @param  [string] $MONGO_USER   [用户名]
     * @param  [string] $MONGO_PASSWD [密码]
     * @param  [string] $MONGO_DB     [数据库]
     * @return [string]               [返回数据库]
     */
    public static function mongoConn($MONGO_HOST, $MONGO_PORT, $MONGO_USER, $MONGO_PASSWD, $MONGO_DB)
    {

        $server = 'mongodb://' . $MONGO_HOST . ':' . $MONGO_PORT;
        $options = array('connect' => true, 'db' => $MONGO_DB, 'password' => $MONGO_PASSWD, 'username' => $MONGO_USER);
        $mongo = new MongoClient($server, $options);

        return $mongo->$options['db'];
    }

    private static $mem = null;

    //连接memcache
    public static function memcacheConn($host = MMC_HOST, $port = MMC_PORT)
    {
        if (is_null(self::$mem) || isset(self::$mem)) {
            self::$mem = new Memcache();
        }
        self::$mem->connect($host, $port);
        return self::$mem;
    }

    private static $redis = null;

    //连接redis
    public static function redisConn($host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PWD, $db = REDIS_DB)
    {
        if (is_null(self::$redis) || isset(self::$redis)) {
            self::$redis = new Redis();
        }
        self::$redis->connect($host, $port);

        //密码验证
        if ($pwd != '') {
            self::$redis->auth($pwd);
        }

        //数据库
        if ($db != '') {
            self::$redis->select($db);
        }

        return self::$redis;
    }
}

