<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2018/10/30
 * Time: 17:28
 */
class redisServer{

    protected $redis;

    public function __construct($host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PWD, $db = REDIS_DB)
    {
        $this->redis = Util::redisConn($host, $port, $pwd, $db);
    }

}