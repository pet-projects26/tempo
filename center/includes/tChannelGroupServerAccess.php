<?php
trait tChannelGroupServerAccess{

    public function __construct(){

    }

    /**
     * 获取权限下的服务器列表
     */
    public function getServerByTrait($where = array(),$type=false){
        $channelGroupModel = new ChannelgroupModel();//实例化渠道组模型
        $channelId = $_SESSION['group_channel'];
        $groupRes = $this->getChannelGroupByTrait($channelId);
        $groupid = array_column($groupRes,'id');
        $depRw = '';
        if($type === false){
            $depRw = " and `server_id` not like '%9998' and `server_id` not like '%9999'";
        }
        $serverSql = "select server_id,`name`,group_id,num,`mom_server`,`type` from ny_server where group_id in ('".join('\',\'',$groupid)."') ".$depRw." order by num asc";

        $serverRes = $channelGroupModel->query($serverSql);
        $serverRes = empty($serverRes)?array():$serverRes;
        $serverRow = array();
        $serverInfo = [];
        foreach($serverRes as $key=>$val){
            if(empty($val['mom_server']))continue;//没有参与合服的即剔除
            $serverInfo[$val['group_id']][$val['server_id']]['mom_server'] = $val['mom_server'];
            $serverInfo[$val['group_id']][$val['server_id']]['num'] = $val['num'];
        }
        //$serverInfoJson = '{"8":{"xingwan0002":{"mom_server":"xingwan0001","num":2},"xingwan0001":{"mom_server":"xingwan0001","num":1},"xingwan0003":{"mom_server":"xingwan0003","num":3},"xingwan0004":{"mom_server":"xingwan0003","num":4}}}';
        //$serverInfo = json_decode($serverInfoJson,true);
        $mergeInfo = self::merge_name($serverInfo);
        if(empty($serverRes))return $serverRow;//当没有选择渠道的时候返回空服务器列表
        foreach ($serverRes as $row) {
            $merge_name = '';
            if($row['type'] == 2) continue;
            $group_id = $row['group_id'];

            $sign = $row['server_id'];
            $name = 'S'.$row['num'] . '-' . $row['name'];
            isset($mergeInfo[$group_id][$sign]) && $merge_name = $mergeInfo[$group_id][$sign];
            $serverRow[$group_id][$sign] = $name.$merge_name;
        }
        /*foreach($serverRow as $key=>&$msg){
            asort($msg);
            $serverRow[$key] = $msg;
        }*/
        return $serverRow;
    }
    
    //没有剔除合服的
    public function getServerByTrait2($where = array(),$type=false){
    	$channelGroupModel = new ChannelgroupModel();//实例化渠道组模型
    	$channelId = $_SESSION['group_channel'];
    	$groupRes = $this->getChannelGroupByTrait($channelId);
    	$groupid = array_column($groupRes,'id');
    	$depRw = '';
    	if($type === false){
    		$depRw = " and `server_id` not like '%9998' and `server_id` not like '%9999'";
    	}
    	$serverSql = "select server_id,`name`,group_id,num,`mom_server`,`type` from ny_server where group_id in ('".join('\',\'',$groupid)."') ".$depRw." order by num asc";
    
    	$serverRes = $channelGroupModel->query($serverSql);
    	$serverRes = empty($serverRes)?array():$serverRes;
    	$serverRow = array();
    	$serverInfo = [];
    	foreach($serverRes as $key=>$val){
    		if(empty($val['mom_server']))continue;//没有参与合服的即剔除
    		$serverInfo[$val['group_id']][$val['server_id']]['mom_server'] = $val['mom_server'];
    		$serverInfo[$val['group_id']][$val['server_id']]['num'] = $val['num'];
    	}
    	//$serverInfoJson = '{"8":{"xingwan0002":{"mom_server":"xingwan0001","num":2},"xingwan0001":{"mom_server":"xingwan0001","num":1},"xingwan0003":{"mom_server":"xingwan0003","num":3},"xingwan0004":{"mom_server":"xingwan0003","num":4}}}';
    	//$serverInfo = json_decode($serverInfoJson,true);
    	$mergeInfo = self::merge_name($serverInfo);
    	if(empty($serverRes))return $serverRow;//当没有选择渠道的时候返回空服务器列表
    	foreach ($serverRes as $row) {
    		$merge_name = '';
    		$group_id = $row['group_id'];
    
    		$sign = $row['server_id'];
    		$name = 'S'.$row['num'] . '-' . $row['name'];
    		isset($mergeInfo[$group_id][$sign]) && $merge_name = $mergeInfo[$group_id][$sign];
    		$serverRow[$group_id][$sign] = $name.$merge_name;
    	}
    	/*foreach($serverRow as $key=>&$msg){
    	 asort($msg);
    	$serverRow[$key] = $msg;
    	}*/
    	return $serverRow;
    }

    /**
     * 获取权限下的所属渠道组列表
     * @param array $cId
     * @param array $fields
     * @return array $groupRes
    */
    public function getChannelGroupByTrait($cId=array(),$fields = array('id','name') , $start = '' , $end = ''){

        $channelGroupModel = new ChannelgroupModel();//实例化渠道组模型
        $channelId = $cId?$cId:$_SESSION['group_channel'];
        $groupRes = array();
        //根据渠道权限来得到所属的服务器信息
        if($channelId == 'all'){
            $groupRes = $channelGroupModel->getChannelGroup(array(),array('id','name'));
        }else if(!empty($channelId)){
            $channel_keys = array_keys($channelId);
            $sql = "select distinct(group_id) from ny_package where channel_id in('".join('\',\'',$channel_keys)."')";
            $row = $channelGroupModel->query($sql);

            $g = array_column($row, 'group_id');

            $groupRes = (new ChannelgroupModel())->getChannelGroup($g , $fields,$start,$end);
        }else{
            return $groupRes;
        }
        return $groupRes;
    }

    /**
     * @version 版本2.0 兼容多并合服
     * @author xty 2018-01-29
     */
    private static function merge_name($mom_server){
        $output = [];
        $temp = [];
        foreach($mom_server as $key=>$val){
            //把父母相等的记录整合起来
            foreach($val as $sId=>$sMsg){
                $temp[$key][$sMsg['mom_server']][] = array('server_id'=>$sId,'num'=>$sMsg['num']);
            }
        }
        $monGHead = [];
        foreach($temp as $groupId=>&$info){
            //获取他妈的头部集合
            $monGHead[$groupId] = [];
            foreach($info as $momKey=>$mm){
                array_push($monGHead[$groupId],$momKey);
            }
            //遍历数据找出大奶妈
            foreach($info as $motherKey=>&$serInfo){
                foreach($serInfo as $sr_info){
                    if(!isset($monGHead[$groupId]))continue;
                    if(in_array($sr_info['server_id'],$monGHead[$groupId]) && $sr_info['server_id'] != $motherKey){
                        $serInfo = array_merge($serInfo,$temp[$groupId][$sr_info['server_id']]);//合并后代
                        unset($temp[$groupId][$sr_info['server_id']]);//删除原有后代
                    }
                }
            }
            //获取同个妈下面的子服务器
            foreach($info as $mom => $vs){
                $numArr = [];
                foreach($vs as $ins){
                    if(empty($ins['num']))continue;
                    $numArr[] = $ins['num'];
                }
                //获取最小的
                sort($numArr);
                $first = $numArr[0];
                //获取最大的
                rsort($numArr);
                $last = $numArr[0];
                if($first == $last)break;
                !empty($first) && $first = 'S'.$first;
                !empty($last) && $last = 'S'.$last;
                $merge_name = '('.$first.'-'.$last.')';
                $output[$groupId][$mom] = $merge_name;
            }
        }
        return $output;
    }

    /**
     * 剔除服务器中的提审服，但是保持单选情况
     * @param array &$data
    */
    public static function eliminateRew(array &$data){
        if(count($data) >1){
            foreach($data as $key=>&$server_id){
                $mat_str = '/^(\w+9999|\w+9998)$/i';
                $status = preg_match($mat_str,$server_id);
                if($status>0){
                    unset($data[$key]);
                }else{
                    continue;
                }
            }
        }else{
            return;
        }
    }
}
?>