<?php
trait tOperate{

    //来源于此集合内的控制器做操作日志记录
    private static $controllers = array(
        'sql'       =>  '技术模块',
        'ce'        =>  '数据导出',
        'dealgroup' =>  '交易组管理',
        'action'    =>  '功能开关',
        'whiteip'   =>  '白名单',
        'gif'       =>  '兑换码',
        'ban'       =>  '封禁管理',
        'manual_logout'=>'玩家下线',
        'mail'      =>  '邮件处理',
        'push'      =>  '消息推送',
        'merge'     =>  '合服计划',
        'plan'       => '开服计划',
        'review'    =>  '渠道版本号',
        'server'    =>  '服务器管理',
        'zone'      =>  '分区管理',
        'channel'   =>  '渠道管理',
        'package'   =>  '包管理',
        'menu'      =>  '央服菜单',
        'usergroup' =>  '用户组',
        'user'      =>  '管理员',
        'permit'    =>  '权限管理',
        'hupdate'   =>  '热更新',
        'gmcmd'     =>  'GM指令',
        'cross'     =>  '跨服计划',
        'activity'  =>  '活动',
        'maintenance'=> '维护计划',
    );

    /**
     * 记录操作日志
     * @mark 按周写入文件
     * @param string $_sessionMsg
     * @param string $filePath
     * @param int $debug
    */
    public static function tHandel($_sessionMsg,$filePath,$debug){

        $ctr = $_REQUEST['ctrl'];
        $act = $_REQUEST['act'];
        //允许集合中的控制器参与日志
        if(in_array($ctr,array_keys(self::$controllers))) {
            $time = date('Y-m-d H:i:s', time());
            $week_start = mktime(0, 0, 0, date("m"), date("d") - date("w") + 1, date("Y"));//获取每周开始时间
            $week_end = mktime(23, 59, 59, date("m"), date("d") - date("w") + 7, date("Y"));//获取每周结束时间
            $file_name = date('Y-m-d', $week_start) . date('Y-m-d', $week_end) . '.log';
            $file_name = $filePath . $file_name;
            $msg = "用户ID：" . $_sessionMsg['username'] . " 控制器:" . $ctr . "  操作方法:" . $act . " 响应码：" . http_response_code() . "  操作时间:" . $time . "\n";

            if ($debug === true && !empty($ctr) && !empty($act)) {

                if(!is_dir($filePath)) {
                    mkdir($filePath); //如果$filePath不存在，则创建
                }

                file_put_contents($file_name, $msg, 8);
            } else {
                #code for false
            }
        }
    }
}
?>