<?php
/***
 * 腾讯云cdn刷新基类 支持 url刷新与cdn刷新
 * Class tencentCdn
 */

class TencentCdn
{
    /*需要填写您的密钥，可从  https://console.cloud.tencent.com/capi 获取 SecretId 及 $secretKey*/
    private $secretId = 'AKIDmJcchhSAYCcniVrk7QffQJHB1cu1EqUt';
    private $secretKey = 'sXxVou0vCKirjNfAQTHq9GtLYaS7pJXb';

    private $HttpUrl = "cdn.api.qcloud.com";

    /*除非有特殊说明，如MultipartUploadVodFile，其它接口都支持GET及POST*/
    private $HttpMethod = "POST";

    /*是否https协议，大部分接口都必须为https，只有少部分接口除外（如MultipartUploadVodFile）*/
    private $isHttps = true;

    /*下面这五个参数为所有接口的 公共参数；对于某些接口没有地域概念，则不用传递Region（如DescribeDeals）*/
    private function COMMON_PARAMS($Action)
    {
        return array(
            'Action' => $Action,
            'Nonce' => rand(),
            'Timestamp' => time(),
            'SecretId' => $this->secretId
        );
    }

    public function RefreshCdnUrl(array $urls = [])
    {
        if (!empty($urls)) {

            $params = [];

            $i = 0;

            foreach ($urls as $url) {
                $params['urls.' . $i] = $url;
            }
            unset($i);

            return $this->CreateRequest('RefreshCdnUrl', $params);
        }
    }

    public function RefreshCdnDir(array $dirs = [], $type = 1)
    {
        if (!empty($dirs)) {
            $params = [
                'type' => $type
            ];

            $i = 0;

            foreach ($dirs as $dir) {
                $params['dirs.' . $i] = $dir;
                $i++;
            }
            unset($i);

            return $this->CreateRequest('RefreshCdnDir', $params);
        }
    }

    private function CreateRequest($Action, $PRIVATE_PARAMS)
    {
        $FullHttpUrl = $this->HttpUrl . "/v2/index.php";

        /***************对请求参数 按参数名 做字典序升序排列，注意此排序区分大小写*************/
        $COMMON_PARAMS = $this->COMMON_PARAMS($Action);
        $ReqParaArray = array_merge($COMMON_PARAMS, $PRIVATE_PARAMS);
        ksort($ReqParaArray);

        /**********************************生成签名原文**********************************
         * 将 请求方法, URI地址,及排序好的请求参数  按照下面格式  拼接在一起, 生成签名原文，此请求中的原文为
         * GETcvm.api.qcloud.com/v2/index.php?Action=DescribeInstances&Nonce=345122&Region=gz
         * &SecretId=AKIDz8krbsJ5yKBZQ    ·1pn74WFkmLPx3gnPhESA&Timestamp=1408704141
         * &instanceIds.0=qcvm12345&instanceIds.1=qcvm56789
         * ****************************************************************************/
        $SigTxt = $this->HttpMethod . $FullHttpUrl . "?";

        $isFirst = true;
        foreach ($ReqParaArray as $key => $value) {
            if (!$isFirst) {
                $SigTxt = $SigTxt . "&";
            }
            $isFirst = false;

            /*拼接签名原文时，如果参数名称中携带_，需要替换成.*/
            if (strpos($key, '_')) {
                $key = str_replace('_', '.', $key);
            }

            $SigTxt = $SigTxt . $key . "=" . $value;
        }

        /*********************根据签名原文字符串 $SigTxt，生成签名 Signature******************/
        $Signature = base64_encode(hash_hmac('sha1', $SigTxt, $this->secretKey, true));


        /***************拼接请求串,对于请求参数及签名，需要进行urlencode编码********************/
        $Req = "Signature=" . urlencode($Signature);
        foreach ($ReqParaArray as $key => $value) {
            $Req = $Req . "&" . $key . "=" . urlencode($value);
        }

        /*********************************发送请求********************************/
        if ($this->HttpMethod === 'GET') {
            if ($this->isHttps === true) {
                $Req = "https://" . $FullHttpUrl . "?" . $Req;
            } else {
                $Req = "http://" . $FullHttpUrl . "?" . $Req;
            }

            $Rsp = file_get_contents($Req);

        } else {
            if ($this->isHttps === true) {
                $Rsp = $this->SendPost("https://" . $FullHttpUrl, $Req, $this->isHttps);
            } else {
                $Rsp = $this->SendPost("http://" . $FullHttpUrl, $Req, $this->isHttps);
            }
        }

        return (json_decode($Rsp, true));
    }

    private function SendPost($FullHttpUrl, $Req, $isHttps)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Req);

        curl_setopt($ch, CURLOPT_URL, $FullHttpUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($isHttps === true) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $result = curl_exec($ch);

        return $result;
    }
}