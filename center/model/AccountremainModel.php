<?php
class AccountremainModel extends Model{
	
    public function __construct(){
        parent::__construct('server_remain');
//         $this->alias = 'cd';
    }

    public function remain_data($conditions){
    	
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['created::>=']) && $conditions['WHERE']['created::>='] = date("Y-m-d",$conditions['WHERE']['created::>=']);
    		isset($conditions['WHERE']['created::<=']) && $conditions['WHERE']['created::<='] = date("Y-m-d",$conditions['WHERE']['created::<=']);
    		 
    		$fields = array('role','data' ,"created");
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		$aaData = [];
//     		var_dump($arr);
//     		die;
//     		    		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		$aaData = [];
    		foreach($arr as $k => $row){
    			$data             = json_decode($row['data'], true);
    			$time             = strtotime($row['created']);
    			$aaData[$time][0] = $row['created'];
    			$aaData[$time][1] += $row['role'];
    			$aaData[$time][2] += intval($data[$time + 86400]);
//     			$aaData[$time][3] += intval($data[$time + 86400 * 2]);
//     			$aaData[$time][4] += intval($data[$time + 86400 * 3]);
//     			$aaData[$time][5] += intval($data[$time + 86400 * 4]);
//     			$aaData[$time][6] += intval($data[$time + 86400 * 5]);
//     			$aaData[$time][7] += intval($data[$time + 86400 * 6]);
    			$aaData[$time][3] += intval($data[$time + 86400 * 6]);
//     			$aaData[$time][9] += intval($data[$time + 86400 * 14]);
    			$aaData[$time][4] += intval($data[$time + 86400 * 29]);
//     			$rs[$row['time']]['time'] = $row['time'];
//     			$rs[$row['time']]['udid']      += $row['udid'];
//     			$rs[$row['time']]['account']   += $row['account'];
    		}
    		
    		foreach ($aaData as $key => &$value) {
    				$role = $value[1];
    				foreach ($value as $k => &$v) {
    					if ($k > 1) {
    						$v = $role && $v ? round($v / $role, 4) * 100 . '%' : '-';
    					}
    				}
    		}
    		
    		$rs = array_values($aaData);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , 0);
    	
    }
    public function remain_export($conditions){
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['created::>=']) && $conditions['WHERE']['created::>='] = date("Y-m-d",$conditions['WHERE']['created::>=']);
    		isset($conditions['WHERE']['created::<=']) && $conditions['WHERE']['created::<='] = date("Y-m-d",$conditions['WHERE']['created::<=']);
    		 
    		$fields = array('role','data' ,"created");
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		//     		    		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		
    		
    		foreach($arr as $k => $row){
    			$data             = json_decode($row['data'], true);
    			$time             = strtotime($row['created']);
    			$aaData[$time][0] = $row['created'];
    			$aaData[$time][1] += $row['role'];
    			$aaData[$time][2] += intval($data[$time + 86400]);
    			$aaData[$time][3] += intval($data[$time + 86400 * 6]);
    			$aaData[$time][4] += intval($data[$time + 86400 * 29]);
    		}
    		foreach ($aaData as $key => &$value) {
    			$role = $value[1];
    			foreach ($value as $k => &$v) {
    				if ($k > 1) {
    					$v = $role && $v ? round($v / $role, 4) * 100 . '%' : '-';
    				}
    			}
    		}
    		
    		$rs = array_values($aaData);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    		
    		$fields = array();
    		$fields[] = array('日期' , '新增角色数' ,'次留','周留','月留');
    		foreach($rs as $row){
    			$fields[] = $this->formatFields($row);
    		}
    		Util::exportExcel($fields , '玩家留存数据（' . date('Y年m月d日H时i分s秒') . '）');
    	}
    }
    
}