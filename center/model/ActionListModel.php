<?php
/** 功能开关Model **/
class ActionListModel extends Model{

    public function __construct(){
        parent::__construct('action');
    }
    /**
     * 获取列表 关闭或者开启日志
     * @param array $conditions
    */
    public function get_list($conditions){
        $this->alias = 'act';
        $this->joinTable = array(
            'cng'=>array('name'=>'channel_group','type'=>'left','on'=>'act.group_id=cng.id')
        );
        $limit = $conditions['Extends']['LIMIT'];
        $servers = $conditions['WHERE']['server::IN']?$conditions['WHERE']['server::IN']:array();
        $data = $this->getRows(array('act.id','act.name','act.mark','act.status','cng.name as c_name','act.last_modified_time','cng.id as cid'),$conditions['WHERE'],$conditions['Extends']);
        $num = $this->getCount();
        $temp =[];
        $action_conf = $this->action_config();
        $actions = json_decode($action_conf,true);
        foreach($data as &$val){
            if($val['status'] == 2){
                $val['status'] = '开启';
            }elseif($val['status'] == 3){
                $val['status'] = '关闭';
            }else{
                $val['status'] = '未知状态';
            }
            $val['last_modified_time'] = date('Y-m-d H:i:s',$val['last_modified_time']);
            $val['c_name'] = $val['cid'].'-'.$val['c_name'];
            array_unshift($val,'<input type="checkbox" data-channel = "'.$val['cid'].'" value="'.$val['name'].'" class="action_list"/>');
            array_push($val,'<input type="button" class="gbutton" data-key="'.$val['name'].'" data-channel ="'.$val['cid'].'"  value="详情" onclick="get_details(\''.$val['name'].'\','.$val['cid'].')" />');
            array_push($val,'<input type="button" class="gbutton" value="编辑" onclick="edit(\''.$val['name'].'\','.$val['cid'].')" /><input type="button" class="gbutton" value="复制" onclick="copy(\''.$val['name'].'\')">');
            $val['name'] = $actions[$val['name']];
            unset($val['cid']);
            unset($val['name']);
            $temp[] = array_values($val);
        }
        $this->setPageData($temp,$num,$limit);
    }

    /**
     * 记录操作
     * @param array $key;
     * @param int $type;
     * @param array $res
     * @return unknow
    */
    public function setHandelRecord($key,$type,$res = array()){

        $actionConf = json_decode($this->action_config(),true);
        if(empty($key) || empty($res)){
            return false;
        }
        if(!in_array($type,array(2,3))){
            return false;
        }
        $sql = "insert into ny_action set `name`='%s',`group_id`= %u,`server` = '%s',`status`=".intval($type).",`mark`='%s',`create_time`=UNIX_TIMESTAMP(),`last_modified_time` = UNIX_TIMESTAMP()
                  ON DUPLICATE KEY UPDATE `last_modified_time`=UNIX_TIMESTAMP(),`status`=".intval($type).",`server`='%s',`mark`='%s' ";
        $ret = [];
        foreach($res as $cid=>$msg){
            foreach($key as $val){
                $serMsg = $res[$cid];
                $serMsg = json_encode($serMsg);//格式化数据
                $ins = sprintf($sql,$val,$cid,$serMsg,$actionConf[$val],$serMsg,$actionConf[$val]);
                $ret[$cid][$val] = $this->query($ins);
            }
        }
        return $ret;
    }

    /**
     * 获取详情
     * @param int $group_id
     * @param string $key
     * @param unkonw $fields
    */
    public function getActionMsg($group_id,$key,$fields = '*'){
        $conditions['WHERE']['group_id'] = $group_id;
        $conditions['WHERE']['name'] = $key;
        $res = $this->getRow($fields,$conditions['WHERE']);
        return $res;
    }

    /**
     * 获取所有功能列表集合，读取服务端文件
     * @return array $str
     */
    public static function action_config(){
        $dir = dirname(__FILE__).'/../includes/action.json';//配置文件路径
        $str = file_get_contents($dir);
        $str = preg_replace("/(\/\*.*?\*\/)|(\/\/.*?\n)/s",'',str_replace(array("\r\n" , "\r") , "\n" , $str));
        return $str;
    }
}
?>