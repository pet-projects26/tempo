<?php


class ActiveAccountModel extends Model
{
    public function __construct()
    {
        parent::__construct('active_account');
        $this->alias = 't';
    }

    //添加活跃用户
    public function addActive($channel, $server, $package, $account, $ip, $mac)
    {

        $data = [];
        $data['channel'] = $channel;
        $data['package'] = $package;
        $data['server'] = $server;
        $data['account'] = $account;
        $data['mac'] = $mac;
        $data['ip'] = $ip;
        $data['create_time'] = time();

        $conditions = array();
        $conditions['WHERE']['channel'] = $channel;
        $conditions['WHERE']['package'] = $package;
        $conditions['WHERE']['server'] = $server;
        $conditions['WHERE']['account'] = $account;
        $conditions['WHERE']['create_time'] = $data['create_time'];

       // if (!$this->getRow(['id'], $conditions['WHERE'])) {
        $this->add($data);
       // }
    }

    //获取服务器活跃用户数
    public function getActiveAccountNum($start_time, $end_time, $server, $package = '', array $accounts = [])
    {
        $tableName = (new SplitSqlUtil($this->tableName))->returnDateTableName([$start_time, $end_time]);

        if (!$tableName) {
            return 0;
        }

        $old_tableName = $this->tableName;

        if ($tableName != $this->tableName) {
            parent::__construct($tableName);
        }

        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $package && $conditions['WHERE']['package'] = $package;
        $accounts && $conditions['WHERE']['account::IN'] = $accounts;

        $res = $this->getRow('count(distinct account) as count_account', $conditions['WHERE']);

        $count = $res['count_account'] ? $res['count_account'] : 0;

        parent::__construct($old_tableName);

        return $count;
    }

    //获取服务器活跃用户
    public function getActiveAccount($start_time, $end_time, $server, $package = '', array $accounts = [])
    {
        $tableName = (new SplitSqlUtil($this->tableName))->returnDateTableName([$start_time, $end_time]);

        if (!$tableName) {
            return [];
        }

        $old_tableName = $this->tableName;

        if ($tableName != $this->tableName) {
            parent::__construct($tableName);
        }

        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $package && $conditions['WHERE']['package'] = $package;
        $accounts && $conditions['WHERE']['account::IN'] = $accounts;

        $res = $this->getRows('distinct account as account', $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                array_push($data, $value['account']);
            }
        }

        parent::__construct($old_tableName);

        return $data;
    }

    public function getDataOnServer($start_time, $end_time, $server, $package = '', array $accounts = [])
    {
        $tableName = (new SplitSqlUtil($this->tableName))->returnDateTableName([$start_time, $end_time]);

        if (!$tableName) {
            return [];
        }

        $old_tableName = $this->tableName;

        if ($tableName != $this->tableName) {
            parent::__construct($tableName);
        }

        $fields = ['channel', 'package', 'mac', 'server', 'ip', 'account'];

        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $package && $conditions['WHERE']['package'] = $package;
        $accounts && $conditions['WHERE']['account::IN'] = $accounts;
        $conditions['Extends']['GROUP'] = 'account';

        $res = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                $data[$value['account']] = $value;
            }
        }

        parent::__construct($old_tableName);

        return $data;
    }
}