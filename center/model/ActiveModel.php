<?php

class ActiveModel extends Model
{

    public function __construct()
    {
        parent::__construct('active');
        $this->alias = 'a';
    }

    public function active_data($conditions)
    {
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'login_num', 'avg_login_num', 'avg_login_longtime', 'new_acc_num', 'new_acc_avg_login_num', 'new_acc_avg_login_longtime', 'old_acc_num', 'old_acc_avg_login_num', 'old_acc_avg_login_longtime');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['login_num'];
                $data[$k][4] = $row['avg_login_num'];
                $data[$k][5] = round($row['avg_login_longtime'] / 3600, 2) . '小时';
                $data[$k][6] = $row['new_acc_num'];
                $data[$k][7] = $row['new_acc_avg_login_num'];
                $data[$k][8] = round($row['new_acc_avg_login_longtime'] / 3600, 2) . '小时';
                $data[$k][9] = $row['old_acc_num'];
                $data[$k][10] = $row['old_acc_avg_login_num'];
                $data[$k][11] = round($row['old_acc_avg_login_longtime'] / 3600, 2) . '小时';
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function active_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'login_num', 'avg_login_num', 'avg_login_longtime', 'new_acc_num', 'new_acc_avg_login_num', 'new_acc_avg_login_longtime', 'old_acc_num', 'old_acc_avg_login_num', 'old_acc_avg_login_longtime');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['login_num'];
                $data[$k][4] = $row['avg_login_num'];
                $data[$k][5] = round($row['avg_login_longtime'] / 3600, 2) . '小时';
                $data[$k][6] = $row['new_acc_num'];
                $data[$k][7] = $row['new_acc_avg_login_num'];
                $data[$k][8] = round($row['new_acc_avg_login_longtime'] / 3600, 2) . '小时';
                $data[$k][9] = $row['old_acc_num'];
                $data[$k][10] = $row['old_acc_avg_login_num'];
                $data[$k][11] = round($row['old_acc_avg_login_longtime'] / 3600, 2) . '小时';
            }
        }

        $result = array();

        $result[] = array('日期', '服务器', '活跃人数', '登录次数', '人均登录次数', '人均在线时长', '新增用户数', '新增用户人均登录次数', '新增用户人均在线时长', '老玩家数', '老玩家人均登录次数', '老玩家人均在线时长');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '活跃统计（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('*');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}