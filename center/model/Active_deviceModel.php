<?php
class Active_deviceModel extends Model{
    public function __construct(){
        parent::__construct('active_device');
        $this->alias = 'rg';
    }
    
    public function activedevice($data){
    	$fields = array('id');
    	$conditions = array();
    	$conditions['WHERE']['deviceId'] = $data['deviceId'];
    	$conditions['WHERE']['package'] = $data['package'];
    	if(!$this->getRow($fields , $conditions['WHERE'])){
    		return $this->add($data);
    	}
    }

}