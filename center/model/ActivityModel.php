<?php

class ActivityModel extends Model
{

    use tChannelGroupServerAccess;

    public function __construct()
    {
        parent::__construct('activity');
        $this->alias = 'a';
    }

    /**
     * 活动更新
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function update_activity_action($data)
    {

        $url = Helper::v2();

        $urls = $postData = array();

        //渠道组，服务器
        $platforms = array_filter($data['channel_group']);
        if (empty($platforms)) {
            $platforms = self::getChannelGroupByTrait();
            $platforms = array_column($platforms, 'id');
        }
        $servers = array_filter($data['server']);
        $server = (new ServerModel)->getServerByServerAndChannelGroup($servers, $platforms, 1);

        $res = Helper::rpc($url, $server, 'uactivity');


        $server = array_keys($server);
        $result = $this->getGm(array(), $server);

        $serverRes = array();
        foreach ($result as $row) {
            $serverRes[$row['server_id']] = $row;
        }

        $str = '';
        foreach ($res as $sign => $v) {
            $row = $serverRes[$sign];

            $name = 'S' . $row['num'] . '-' . $row['name'];

            if ($v == 1) {
                $msg = '成功';
            } else {
                $msg = $v;
            }

            $str .= $name . ':' . $msg;
            $str .= "\r\n";
        }

        return $str;
    }

    /**
     * 活动列给
     * @param  [type] $conditions [description]
     * @return [type]             [description]
     */
    public function activitylist_data($conditions)
    {
        //如果是超级管理员，可以看到发布了的编辑
        $username = $_SESSION['username'];

        $sql = "select groupid from ny_user where username = '$username'";
        $groupid = $this->query($sql);
        $groupid = $groupid[0]['groupid'];

        $rs = array();
        $num = 0;
        $fields = array('id', 'title', 'start_time', 'end_time', 'type', 'act_id', 'act_id as act_name', 'platforms', 'add_user', 'check_user', 'check_time', 'status', 'model');
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        if ($rs) {
            $num = $this->getCount();
            foreach ($rs as $k => $v) {
                $rs[$k]['id'] = '<input class="sorting_1 checkbox"  type="checkbox" value=' . $v['id'] . '>' . $v['id'];
                $rs[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
                $rs[$k]['end_time'] = date('Y-m-d H:i:s', $v['end_time']);

                if ($v['type'] == 0) {
                    $rs[$k]['type'] = '<button class="btn btn-default">待提审</button>';
                } elseif ($v['type'] == 1) {
                    $rs[$k]['type'] = '<button class="btn btn-warning">已提审</button>';
                } elseif ($v['type'] == 2) {
                    $rs[$k]['type'] = '<button class="btn btn-success">已发布</button>';
                } elseif ($v['type'] == 3) {
                    $rs[$k]['type'] = '<button class="btn btn-danger">被驳回</button>';
                } elseif ($v['type'] == 4) {
                    $rs[$k]['type'] = '<button class="btn btn-info">下架</button>';
                }
                $rs[$k]['act_id'] = $v['act_id'];
                $rs[$k]['act_name'] = CDict::$activityType[$v['act_id']];


                if (!empty($v['platforms'])) {
                    $sql = "select name from  ny_channel_group where id in (" . $v['platforms'] . ")";
                    $group = $this->query($sql);
                    $group = array_column($group, 'name');
                    $group = implode(',', $group);
                    $rs[$k]['platforms'] = $group;
                } else {
                    $rs[$k]['platforms'] = '全部渠道组';
                }

                $rs[$k]['check_time'] = $v['check_time'] != 0 ? date('Y-m-d H:i:s', $v['check_time']) : '';

                //执行状态
                if ($v['status'] == 0) {
                    $rs[$k]['status'] = '<button class="btn btn-default">未执行</button>';
                } elseif ($v['status'] == 1) {
                    $rs[$k]['status'] = '<button class="btn btn-success">成功</button>';
                } elseif ($v['status'] == 2) {
                    $rs[$k]['status'] = '<button class="btn btn-danger">失败</button>';
                }


                $rs[$k]['remark'] = '<button class="gbutton" onclick="_remark(\'' . $v['id'] . '\')">查看</button>';
                if ($v['status'] == 0) {
                    $rs[$k]['remark'] = '';
                }

                $str = '<button style="margin-bottom:5px;" class="gbutton" onclick="_copy(\'' . $v['id'] . '\')">复制</button> ';

                if ($v['type'] != 2 || $groupid == 1) {
                    $str .= '<button style="margin-bottom:5px;" class="gbutton" onclick="_edit(\'' . $v['id'] . '\')">编辑</button> ';
                }

                //已提审的不可以审核,发布了的也不能再审核了
                if ($v['type'] != 1 && $v['type'] != 2 || $groupid == 1) {

                    $str .= '<button style="margin-bottom:5px;" class="gbutton" onclick="_check(\'' . $v['id'] . '\')">审核</button> ';
                }

                //已发布才可以下架
                if ($v['type'] == 2) {
                    $str .= '<button class="gbutton" onclick="_sync(\'' . $v['id'] . '\')">下架</button> ';
                } else {
                    $str .= '<button class="gbutton" onclick="_del(\'' . $v['id'] . '\')">删除</button> ';
                }

                if ($v['model'] == 0) {
                    $str .= '<button  class="gbutton" onclick="_model(\'' . $v['id'] . '\')">模版</button> ';
                }

                unset($rs[$k]['model']);
                $rs[$k]['caozuo'] = $str;
            }
            foreach ($rs as $k => $v) {
                $rs[$k] = array_values($v);
            }
        }
        $this->setPageData($rs, $num);
    }

    /**
     * [保存活动]
     * @param [array] $[data] [<传参过来的数组>]
     * @return [type] [description]
     */
    public function save($data)
    {
        $act_id = $data['act_id'];
        $start_time = isset($data['start_time']) ? strtotime($data['start_time']) : 0;
        $end_time = isset($data['end_time']) ? strtotime($data['end_time']) : 0;

        $servers = array_filter($data['server']);
        $platforms = array_filter($data['channel_group']);
        $group_ids = self::getChannelGroupByTrait();
        empty($platforms) && $platforms = array_column($group_ids, 'id');

        $end_time = strtotime($data['end_time']);
        $start_time = strtotime($data['start_time']);
        $activity_sign = intval($data['activity_sign']);
        $openSerDay = intval($data['openSerDay']);
        if ($start_time > $end_time) {
            return array('code' => 404, 'msg' => '结束时间不能小于开始时间');
            die();
        }
        switch ($act_id) {
            case 1065: //每日充值
                //档次

                $amount = $data['amount'];
                if (!is_array($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入钻石档次');
                    return $arr;
                }

                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }


                //判断是不否填入相对应物品条件
                if (empty($amount)) {
                    return array('code' => 404, 'msg' => '请填入钻石档次');
                }

                //遍历条件
                foreach ($amount as $k => $if) {
                    if (empty($if)) {
                        return array('code' => 404, 'msg' => '请填入钻石档次');
                    }

                    //物品条件
                    if (!isset($itemId[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行的奖励');
                    }

                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                            return array('code' => 404, 'msg' => '请按正确格式填写第' . ($k + 1) . '行钻石档次, 第' . ($k2 + 1) . '行的物品');
                        }
                    }

                    //物品数量
                    if (!isset($itemCount[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行钻石档次, 第' . ($k2 + 1) . '行的奖励');
                    }

                    foreach ($itemCount[$k] as $k => $goods_num) {

                        if (empty($goods_num) || !is_numeric($goods_num)) {
                            return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行钻石档次, 第' . ($k2 + 1) . '行的奖励的行物品数量');
                        }
                    }

                }

                //累计天数
                if (empty($data['amount2'])) {
                    return array('code' => 404, 'msg' => '请填入累计天数配置的钻石');
                }

                if ($data['day2'][1] != array()) {
                    foreach ($data['day2'][1] as $k => $v) {
                        if ($v < 0 || empty($v)) {
                            return array('code' => 404, 'msg' => '天数必须大于0');
                        }
                        if (empty($data['item_id2'][1][$k])) {
                            return array('code' => 404, 'msg' => '累计天数配置的物品id不能为空');
                        }
                        if ($data['item_count2'][1][$k] < 0 || empty($data['item_count2'][1][$k])) {
                            return array('code' => 404, 'msg' => '累计天数配置的物品数量必须大于0');
                        }
                    }
                } else {
                    return array('code' => 404, 'msg' => '请填入累计天数配置的天数');
                }

                $grade['amount2'] = $data['amount2'];
                $grade['day2'] = $data['day2'];
                $grade['item_id2'] = $data['item_id2'];
                $grade['item_count2'] = $data['item_count2'];
                //end foreach
                break;
            case 1002: //
                $time = array();
                //档次
                $grade = $data['grade'];

                if (!is_array($grade)) {
                    $arr = array('code' => 404, 'msg' => '请填入充值人数');
                    return $arr;
                }

                $amount = $data['amount'];

                if (!is_array($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入条件');
                    return $arr;
                }


                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }

                $career = $data['career'];
                if (!is_array($career)) {
                    $arr = array('code' => 404, 'msg' => '请填入角色');
                    return $arr;
                }


                foreach ($grade as $k => $level) {
                    $start = $level['start'];
                    $end = $level['end'];

                    if (empty($start)) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行的开始时间');
                    }

                    if (empty($end)) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行的结束时间');
                    }

                    array_push($time, strtotime($start));
                    array_push($time, strtotime($end));

                    //判断是不否填入相对应物品条件
                    if (!isset($amount[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行充值的条件');
                    }

                    //遍历条件
                    foreach ($amount[$k] as $k2 => $if) {
                        //物品条件
                        if (!isset($itemId[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励');
                        }

                        foreach ($itemId[$k][$k2] as $k3 => $goods_id) {
                            if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                                return array('code' => 404, 'msg' => '请按正确格式填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的' . ($k3 + 1) . '物品');
                            }
                        }

                        //物品数量
                        if (!isset($itemCount[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励');
                        }

                        foreach ($itemCount[$k][$k2] as $k3 => $goods_num) {

                            if (empty($goods_num) || !is_numeric($goods_num)) {
                                return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励的第' . ($k3 + 1) . '行物品数量');
                            }
                        }

                    }
                    //end foreach
                }
                //end foreach $grade

                $start_time = min($time);
                $end_time = max($time);
                break;
            case 1004:
                //档次
                $grade = $data['grade'];

                if (!is_array($grade)) {
                    $arr = array('code' => 404, 'msg' => '请填入充值人数');
                    return $arr;
                }

                $amount = $data['amount'];

                if (!is_array($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入条件');
                    return $arr;
                }

                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }

                $career = $data['career'];
                if (!is_array($career)) {
                    $arr = array('code' => 404, 'msg' => '请填入角色');
                    return $arr;
                }


                foreach ($grade as $k => $level) {
                    //判断是不否填入相对应物品条件
                    if (!isset($amount[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行充值的条件');
                    }

                    //遍历条件
                    foreach ($amount[$k] as $k2 => $if) {
                        //物品条件
                        if (!isset($itemId[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励');
                        }

                        foreach ($itemId[$k][$k2] as $k3 => $goods_id) {

                            if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                                return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的' . ($k3 + 1) . '物品');
                            }
                        }

                        //物品数量
                        if (!isset($itemCount[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励');
                        }

                        foreach ($itemCount[$k][$k2] as $k3 => $goods_num) {

                            if (empty($goods_num) || !is_numeric($goods_num)) {
                                return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励的第' . ($k3 + 1) . '行物品数量');
                            }
                        }

                    }
                    //end foreach
                }
                //end foreach $grade
                break;
            case 1008:
                $amount = $data['amount'];
                if (empty($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入购买价格');
                    return $arr;
                }

                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }

                $career = $data['career'];
                if (!is_array($career)) {
                    $arr = array('code' => 404, 'msg' => '请填入角色');
                    return $arr;
                }

                foreach ($itemId as $k => $goods_id) {
                    if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                        return array('code' => 404, 'msg' => '请正确格式输入' . ($k + 1) . '行的物品ID');
                    }
                }

                break;
            case 1030://砸蛋
            case 1033:
                $config = $data['config'];
                if (empty($config)) {
                    $arr = array('code' => 404, 'msg' => '请填入配置奖励');
                    return $arr;
                }

                //档次
                $grade = $data['grade'];

                if (!is_array($grade)) {
                    $arr = array('code' => 404, 'msg' => '请填入世界等级');
                    return $arr;
                }

                $amount = $data['amount'];

                $itemId = $data['item_id'];

                $itemCount = $data['item_count'];

                $career = $data['career'];
                isset($data['vip_level']) && $vip_level = $data['vip_level'];//扭蛋活动1033

                //配置奖励
                $cItemId = $config['item_id'];

                $cItemCount = $config['item_count'];
                $cCareer = $config['career'];
                $cWeight = $config['weight'];
                $crare = $config['rare'];

                foreach ($grade as $k => $level) {
                    //config  判断是否填完所有
                    if (empty($level)) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行的世界等级');
                    }

                    if (!isset($cItemId[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行的配置奖励');
                    }

                    foreach ($cItemId[$k] as $k2 => $goods_id) {
                        if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                            return array('code' => 404, 'msg' => '请按正确格式填写第' . $k . '行, 第' . ($k2 + 1) . '行的物品ID');
                        }


                        if (empty($cItemCount[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行, 第' . ($k2 + 1) . '行的物品数量');
                        }


                        if (empty($cWeight[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行, 第' . ($k2 + 1) . '行的权重');
                        }

                        // if (empty($crare[$k][$k2]) && empty($crare[$k][$k2]) !== 0) {
                        // 	return array('code' => 404, 'msg' => '请填写第'. $k .'行, 第'. ($k2 + 1) .'行的稀有');
                        // }

                    }

                    //判断是存该世界等级的额外条件
                    if (!isset($amount[$k])) {
                        continue;
                    }

                    //遍历条件
                    foreach ($amount[$k] as $k2 => $if) {
                        //物品条件
                        if (!isset($itemId[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行, 第' . ($k2 + 1) . '行的奖励');
                        }

                        foreach ($itemId[$k][$k2] as $k3 => $goods_id) {
                            if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                                return array('code' => 404, 'msg' => '请按正确格式填写第' . $k . '行, 第' . ($k2 + 1) . '行的' . ($k3 + 1) . '物品');
                            }
                        }

                        //物品数量
                        if (!isset($itemCount[$k][$k2])) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '行, 第' . ($k2 + 1) . '行的奖励');
                        }

                        foreach ($itemCount[$k][$k2] as $k3 => $goods_num) {
                            if (empty($goods_num) || !is_numeric($goods_num)) {
                                return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励的第' . ($k3 + 1) . '行物品数量');
                            }
                        }

                    }
                    //end foreach
                    //校验vip_level
                    if (isset($vip_level) && !empty($vip_level)) {
                        foreach ($vip_level[$k] as $kl => $vl) {
                            if (!isset($itemId[$k][$kl])) {
                                return array('code' => 404, 'msg' => '请填写第' . $k . '行, 第' . ($k2 + 1) . '行的奖励');
                            }

                            foreach ($itemId[$k][$kl] as $k3 => $goods_id) {
                                if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                                    return array('code' => 404, 'msg' => '请按正确格式填写第' . $k . '行, 第' . ($k2 + 1) . '行的' . ($k3 + 1) . '物品');
                                }
                            }

                            //物品数量
                            if (!isset($itemCount[$k][$kl])) {
                                return array('code' => 404, 'msg' => '请填写第' . $k . '行, 第' . ($k2 + 1) . '行的奖励');
                            }

                            foreach ($itemCount[$k][$kl] as $k3 => $goods_num) {
                                if (empty($goods_num) || !is_numeric($goods_num)) {
                                    return array('code' => 404, 'msg' => '请填写第' . $k . '行充值, 第' . ($k2 + 1) . '行的奖励的第' . ($k3 + 1) . '行物品数量');
                                }
                            }
                        }
                    }
                    //end foreach
                }
                //end foreach $grade

                break;
            case 1009: //消费排行榜
                $grade = $data['grade'];

                $itemId = $data['item_id'];

                $itemCount = $data['item_count'];

                foreach ($grade as $k => $level) {
                    //config  判断是否填完所有
                    if (empty($level)) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行的世界等级');
                    }

                    if (!isset($itemId[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . $k . '行的世界奖励');
                    }

                    foreach ($itemId[$k] as $k2 => $row) {
                        $car1 = $row['career1'];
                        $car2 = $row['career2'];
                        $car3 = $row['career3'];

                        if (empty($car1)) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '的世界奖励的' . $k2 . '名刀的奖励物品');
                        }

                        if (empty($car2)) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '的世界奖励的' . $k2 . '名剑的奖励物品');
                        }

                        if (empty($car3)) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '的世界奖励的' . $k2 . '名法的奖励物品');
                        }


                        $cCar1 = $itemCount[$k][$k2]['career1'];
                        $cCar2 = $itemCount[$k][$k2]['career2'];
                        $cCar3 = $itemCount[$k][$k2]['career3'];

                        if (empty($cCar1)) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '的世界奖励的' . $k2 . '名刀的奖励物品数量');
                        }

                        if (empty($cCar2)) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '的世界奖励的' . $k2 . '名剑的奖励物品数量');
                        }

                        if (empty($cCar3)) {
                            return array('code' => 404, 'msg' => '请填写第' . $k . '的世界奖励的' . $k2 . '名法的奖励物品数量');
                        }

                    }
                    //end foreach
                }
                break;
            case 1003: //累计充值
                //档次
                $amount = $data['amount'];

                if (!is_array($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入条件');
                    return $arr;
                }

                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }

                $career = $data['career'];
                if (!is_array($career)) {
                    $arr = array('code' => 404, 'msg' => '请填入角色');
                    return $arr;
                }


                //判断是不否填入相对应物品条件
                if (empty($amount)) {
                    return array('code' => 404, 'msg' => '请填写充值的条件');
                }

                //遍历条件
                foreach ($amount as $k => $if) {
                    //物品条件
                    if (!isset($itemId[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行的奖励');
                    }

                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                            return array('code' => 404, 'msg' => '请按正确格式填写第' . ($k + 1) . '行充值, 第' . ($k2 + 1) . '行的物品');
                        }
                    }

                    //物品数量
                    if (!isset($itemCount[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行充值, 第' . ($k2 + 1) . '行的奖励');
                    }

                    foreach ($itemCount[$k] as $k => $goods_num) {

                        if (empty($goods_num) || !is_numeric($goods_num)) {
                            return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行充值, 第' . ($k2 + 1) . '行的奖励的行物品数量');
                        }
                    }

                }
                //end foreach
                break;
            case 1006: // 幸运鉴宝
                $type = $data['type'];
                if (empty($type)) {
                    $arr = array('code' => 404, 'msg' => '请填入鉴宝方式');
                    return $arr;
                }

                $gold = $data['gold'];

                if (empty($gold)) {
                    $arr = array('code' => 404, 'msg' => '请填入鉴宝1次');
                    return $arr;
                }

                $freeGold = $data['freeGold'];

                if (empty($freeGold)) {
                    $arr = array('code' => 404, 'msg' => '请填入消耗');
                    return $arr;
                }


                //档次
                $amount = $data['amount'];

                if (!is_array($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入幸运大奖');
                    return $arr;
                }

                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入保底奖励大奖');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入保底奖励物品数量');
                    return $arr;
                }

                $itemWeight = $data['weight'];
                if (!is_array($itemWeight)) {
                    $arr = array('code' => 404, 'msg' => '请填入权重');
                    return $arr;
                }


                //判断是不否填入相对应物品条件
                if (empty($amount)) {
                    return array('code' => 404, 'msg' => '请填写充值的条件');
                }

                //遍历条件
                foreach ($amount as $k => $if) {
                    if (empty($if)) {
                        switch ($if) {
                            case 1:
                                $name = "刀";
                                break;
                            case 2:
                                $name = "剑";
                                break;
                            case 3:
                                $name = '弓';
                                break;
                        }
                    }

                    $goods_id = $if['goods_id'];
                    $goods_num = $if['goods_num'];
                    $weight = $if['weight'];

                    if (empty($goods_id) || empty($goods_num) || empty($weight)) {
                        return array('code' => 404, 'msg' => '请输入' . $name . '大奖奖励');
                    }

                }
                //end foreach
                break;
            case 1005: //云购
                $top = $data['top'];

                if (empty($top)) {
                    $arr = array('code' => 404, 'msg' => '请填入新服奖励');
                    return $arr;
                }
                //top name="top[2][item_id][2]"
                foreach ($top as $k => $r) {


                    $goods_num = $r['item_count'];
                    $weight = $r['item_weight'];

                    if ($k == 1) {
                        $name = "新服";
                    } else {
                        $name = "老服";
                    }


                    foreach ($r['item_id'] as $k2 => $goods_id) {
                        if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                            return array('code' => 404, 'msg' => $name . '大奖第' . ($k2 + 1) . '行按正确格式填写');
                        }
                    }

                    if (empty($goods_num)) {
                        return array('code' => 404, 'msg' => $name . '大奖第' . $k . '行物品数量不能为空');
                    }

                    if (empty($weight)) {
                        return array('code' => 404, 'msg' => $name . '大奖第' . $k . '行物品权重不能为空');
                    }
                }

                $itemId = $data['item_id'];

                foreach ($itemId as $k => $row) {
                    if ($k == 1) {
                        $name = "新服";
                    } else {
                        $name = "老服";
                    }

                    foreach ($row as $k2 => $goods_id) {

                        if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                            return array('code' => 404, 'msg' => $name . '保底奖励第' . $k . '行按正确格式填写');
                        }
                    }
                }

                $career = $data['career'];


                $itemCount = $data['item_count'];
                foreach ($itemCount as $k => $row) {
                    if ($k == 1) {
                        $name = "新服";
                    } else {
                        $name = "老服";
                    }

                    foreach ($row as $k2 => $goods_num) {

                        if (empty($goods_num)) {
                            return array('code' => 404, 'msg' => $name . '保底奖励第' . $k . '行按正确格式填写');
                        }
                    }
                }

                break;
            case 1007: //全民嗨
                //档次
                $amount = $data['amount'];

                if (!is_array($amount)) {
                    $arr = array('code' => 404, 'msg' => '请填入条件');
                    return $arr;
                }

                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }

                $career = $data['career'];
                if (!is_array($career)) {
                    $arr = array('code' => 404, 'msg' => '请填入角色');
                    return $arr;
                }


                //判断是不否填入相对应物品条件
                if (empty($amount)) {
                    return array('code' => 404, 'msg' => '请填写充值的条件');
                }

                //遍历条件
                foreach ($amount as $k => $if) {
                    //物品条件
                    if (!isset($itemId[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行的奖励');
                    }

                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                            return array('code' => 404, 'msg' => '请按正确格式填写第' . ($k + 1) . '行充值, 第' . ($k2 + 1) . '行的物品');
                        }
                    }

                    //物品数量
                    if (!isset($itemCount[$k])) {
                        return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行充值, 第' . ($k2 + 1) . '行的奖励');
                    }

                    foreach ($itemCount[$k] as $k => $goods_num) {

                        if (empty($goods_num) || !is_numeric($goods_num)) {
                            return array('code' => 404, 'msg' => '请填写第' . ($k + 1) . '行充值, 第' . ($k2 + 1) . '行的奖励的行物品数量');
                        }
                    }

                }

                break;
            case 1010: //节日掉落

                $shortDesc = $data['shortDesc'];

                if (empty($shortDesc)) {
                    $arr = array('code' => 404, 'msg' => '请输入短述');
                    return $arr;
                }

                $description = $data['description'];

                if (empty($description)) {
                    $arr = array('code' => 404, 'msg' => '请输入描述');
                    return $arr;
                }

                $itemNeedCount = $data['item_need_count'];

                if (!is_array($itemNeedCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入兑换物品数量');
                    return $arr;
                }

                foreach ($itemNeedCount as $k => $num) {
                    if (empty($num)) {
                        return array('code' => 404, 'msg' => '第' . ($k + 1) . '兑换物品数量不能为空');
                    }
                }


                $itemId = $data['item_id'];

                if (!is_array($itemId)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品');
                    return $arr;
                }

                foreach ($itemId as $k => $goods_id) {
                    if (!(is_numeric($goods_id) || preg_match('/\//', $goods_id))) {
                        return array('code' => 404, 'msg' => '请正确格式输入' . ($k + 1) . '行的物品ID');
                    }
                }


                $itemCount = $data['item_count'];
                if (!is_array($itemCount)) {
                    $arr = array('code' => 404, 'msg' => '请填入奖励物品数量');
                    return $arr;
                }

                foreach ($itemCount as $k => $num) {
                    if (empty($num)) {
                        return array('code' => 404, 'msg' => '第' . ($k + 1) . '得到的物品个数不能为空');
                    }
                }

                $itemExchange = $data['item_exchange'];

                if (!is_array($itemExchange)) {
                    $arr = array('code' => 404, 'msg' => '请填入可兑换次数');
                    return $arr;
                }

                foreach ($itemExchange as $k => $num) {
                    if (empty($num)) {
                        return array('code' => 404, 'msg' => '第' . ($k + 1) . '可兑换次数不能为空');
                    }
                }

                break;
            case 1020: //全民活动

                $form = $data['form'];

                if (empty($form)) {
                    $arr = array('code' => 404, 'msg' => '请填入类型');
                    return $arr;
                }

                $shortDesc = $data['shortDesc'];

                if (empty($shortDesc)) {
                    $arr = array('code' => 404, 'msg' => '请输入短述');
                    return $arr;
                }

                $description = $data['description'];

                if (empty($description)) {
                    $arr = array('code' => 404, 'msg' => '请输入描述');
                    return $arr;
                }

                $open_hour = $data['open_hour'];
                if (!is_numeric($open_hour)) {
                    $arr = array('code' => 404, 'msg' => '请输入开启时间小时');
                    return $arr;
                }

                $open_min = $data['open_min'];

                if (!is_numeric($open_min)) {
                    $arr = array('code' => 404, 'msg' => '请输入开启时间分钟');
                    return $arr;
                }

                $close_hour = $data['close_hour'];
                if (!is_numeric($close_hour)) {
                    $arr = array('code' => 404, 'msg' => '请输入关闭时间小时');
                    return $arr;
                }

                $close_min = $data['close_min'];

                if (!is_numeric($close_min)) {
                    $arr = array('code' => 404, 'msg' => '请输入半闭时间分钟');
                    return $arr;
                }
                break;
            //集福活动
            case 1032:
                //解析礼包奖励（参考json结构）
                //error_log('Activity json msg=='.json_encode($data,JSON_UNESCAPED_UNICODE)."\n\r",3,'error.log');
                $list = array();
                !empty($data['list']) && $list = $data['list'];
                //重排数据
                foreach ($list as &$val) {
                    ksort($val['topPrize']);
                    ksort($val['normal']);
                }
                $data['list'] = $list;
                break;
            case 1034:
                if ($start_time > $end_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['openLvl'])) {
                    $arr = array('code' => 404, 'msg' => '请填写开启等级');
                    return $arr;
                }
                #code for 聚划算
                //校验大礼包
                if (empty($data['big_price_name'])) {
                    $arr = array('code' => 404, 'msg' => '请填写大礼包名称');
                    return $arr;
                }
                if (empty($data['item_id']) || in_array('', $data['item_id'])) {
                    $arr = array('code' => 404, 'msg' => '请填写大礼包道具ID');
                    return $arr;
                }
                if (empty($data['price_old']) || empty($data['price_now'])) {
                    $arr = array('code' => 404, 'msg' => '请填写大礼包原价或者现价');
                    return $arr;
                }
                //小礼包奖励
                $config = $data['config'];
                if (empty($config['item_id'])) {
                    $arr = array('code' => 404, 'msg' => '请填写小礼包道具');
                    return $arr;
                }
                foreach ($config['item_id'] as $key => $value) {
                    if (empty($data['config']['price_name'][$key])) {
                        $arr = array('code' => 404, 'msg' => '请填写小礼包名称');
                        return $arr;
                    }
                    if (in_array('', $value)) {
                        $arr = array('code' => 404, 'msg' => '请填写小礼包道具ID');
                        return $arr;
                    }
                }
                break;
            case 1035:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['once_gold'])) {
                    $arr = array('code' => 404, 'msg' => '请填写抽一次需要的元宝数');
                    return $arr;
                }
                if (empty($data['tentimes_gold'])) {
                    $arr = array('code' => 404, 'msg' => '请填写抽十次需要元宝数');
                    return $arr;
                }
                if (empty($data['once_score'])) {
                    $arr = array('code' => 404, 'msg' => '请填写抽一次可获得积分数');
                    return $arr;
                }
                if (empty($data['input_gold'])) {
                    $arr = array('code' => 404, 'msg' => '请填写单词抽检进入奖池的元宝数');
                    return $arr;
                }
                $config = $data['config'];
                if (empty($config['item_id'])) return array('code' => 404, 'msg' => '请配置普通奖励');
                foreach ($config['item_id'] as $key => $val) {
                    if (empty($val)) return array('code' => 404, 'msg' => '请填写道具ID');
                    if (empty($config['item_count'][$key])) return array('code' => 404, '请填写道具' . $config['item_id'][$key] . '数量');
                }
                //积分奖励可以不填写吗
                //if(empty($data['score_num'])) return array('code'=>404,'msg'=>'请填写积分奖励积分数');
                break;
            case 1036:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['freeGold'])) {
                    $data['freeGold'] = 1;
                }
                $gold_num = count($data['gold']);
                if (($data['freeGold'] + $gold_num) != 9) {
                    return array('code' => 404, 'msg' => '免费次数与抽奖次数合计应等于9次');
                }
                if (empty($data['gold'])) return array('code' => 404, 'msg' => '请填写抽奖次数');
                if (empty($data['grade']) || in_array('', $data['grade'])) return array('code' => 404, 'msg' => '请填写世界等级');
                if (empty($data['itemId'])) return array('code' => 404, 'msg' => '请填写道具');
                $itemId = $data['itemId'];
                foreach ($itemId as $key => $val) {
                    if (empty($data['grade'][$key])) return array('code' => 404, 'msg' => '世界等级为空');
                    if (in_array('', $val) || in_array(null, $val)) return array('code' => 404, 'msg' => '道具ID不能为空');
                    if (in_array('', $data['count'][$key]) || in_array(0, $data['count'][$key])) return array('code' => 404, 'msg' => '道具数量不能为0或空');
                    $weight = $data['weight'][$key];
                    foreach ($weight as $kt => $mm) {
                        if (empty($mm)) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$key] . ',道具ID为' . $val[$kt] . ',权重不能为空');
                    }
                }
                $grade = $data['grade'];
                break;
            case 1037:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['roll_one_id']) || empty($data['gold_one_num'])) return array('code' => 404, 'msg' => '请填写摇一次道具ID或元宝数量');
                if (empty($data['roll_ten_id']) || empty($data['gold_ten_num'])) return array('code' => 404, 'msg' => '请填写摇十次道具ID或者元宝数量');
                $day = $data['day'];
                $top = $data['top'];
                $config = $data['config'];
                if (empty($day) || empty($top)) return array('code' => 404, 'msg' => '请填写道具奖励');
                foreach ($day['item_id'] as $level => $itemInfo) {
                    if (empty($data['grade'][$level])) return array('code' => 404, 'msg' => '请填写世界等级');
                    foreach ($itemInfo as $key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . '，请填写道具ID');
                        if (empty($data['day']['count'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . '，普通奖励，请填写道具数量');
                    }
                }
                foreach ($top['item_id'] as $level => $itemInfo) {
                    if (empty($data['grade'][$level])) return array('code' => 404, 'msg' => '请填写世界等级');
                    if (count($itemInfo) < 3) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . ',请填写完整3列');
                    foreach ($itemInfo as $key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . '，请填写道具ID');
                        if (empty($data['top']['count'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . '，大奖，,请填写道具数量');
                    }
                }
                if (empty($data['monsterId'])) return array('code' => 404, 'msg' => '请填写怪物ID');
                if (empty($data['dropItemId'])) return array('code' => 404, 'msg' => '请填写掉落ID');
                $data['open_hour'] = $data['roll_one_id'];
                $data['item_id'] = $data['roll_one_num'];
                $data['close_hour'] = $data['gold_one_num'];
                $data['open_min'] = $data['roll_ten_id'];
                $data['item_count'] = $data['roll_ten_num'];
                $data['close_min'] = $data['gold_ten_num'];
                $grade = $data['grade'];
                break;
            case 1038:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['grade']) || in_array('', $data['grade'])) return array('code' => 404, 'msg' => '请填写世界等级');
                if (empty($data['level'])) return array('code' => 404, 'msg' => '请填写等级');
                if (empty($data['item_num'])) return array('code' => 404, 'msg' => '请填写可双倍次数');
                $data['config'] = array('level' => $data['level'], 'item_num' => $data['item_num']);
                $grade = $data['grade'];
                unset($data['level']);
                unset($data['item_num']);
                break;
            case 1039:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                $itemId = $data['itemId'];
                $item_id = $data['item_id'];
                foreach ($itemId as $levelId => $itemInfo) {
                    if (empty($itemInfo)) return array('code' => 404, 'msg' => '请填写道具奖励');
                    if (empty($data['grade'][$levelId])) return array('code' => 404, 'msg' => '请填写世界等级');
                    if (empty($data['comsume'][$levelId][1])) return array('code' => 404, 'msg' => '世界等级为:' . $data['grade'][$levelId] . '累计充值目标，充值金额不能为空');
                    if (empty($data['comsume'][$levelId][2])) return array('code' => 404, 'msg' => '世界等级为:' . $data['grade'][$levelId] . '累计消费目标，消费金额不能为空');
                    if (empty($data['item_id'][$levelId][1])) return array('code' => 404, 'msg' => '世界等级为:' . $data['grade'][$levelId] . '累计充值目标，道具ID不能为空');
                    if (empty($data['item_id'][$levelId][2])) return array('code' => 404, 'msg' => '世界等级为:' . $data['grade'][$levelId] . '累计消费目标，道具ID不能为空');
                    if (empty($data['item_count'][$levelId][1])) return array('code' => 404, 'msg' => '世界等级为:' . $data['grade'][$levelId] . '累计充值目标，道具数量不能为空');
                    if (empty($data['item_count'][$levelId][2])) return array('code' => 404, 'msg' => '世界等级为:' . $data['grade'][$levelId] . '累计消费目标，道具数量不能为空');
                    if (empty($data['mapRange'][$levelId])) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$levelId] . '，请填写元宝配置');
                }
                $data['config'] = $data['comsume'];
                $grade = $data['grade'];
                break;
            case 1041:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['grade']) || in_array('', $data['grade'])) return array('code' => 404, 'msg' => '请填写世界等级');
                $itemId = $data['item_id'];
                foreach ($itemId as $level => $itemInfo) {
                    //得到世界等级
                    if (empty($itemInfo)) {
                        return array('code' => 404, 'msg' => '世界等级:' . $data['grade'][$level] . '请填写奖励配置');
                    }
                    foreach ($itemInfo as $com => $itemMsg) {
                        if (empty($data['comsume'][$level][$com])) {
                            return array('code' => 404, 'msg' => '世界等级：' . $data['grade'][$level] . '请填写完整消费元宝数');
                        }
                        foreach ($itemMsg as $key => $item) {
                            if (empty($item)) return array('code' => 404, 'msg' => '世界等级：' . $data['grade'][$level] . ',消费元宝数为：' . $data['comsume'][$level][$com] . '请填写完整道具');
                            if (empty($data[$level][$com]['count'][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . ',消费元宝数为：' . $data['comsume'][$level][$com] . ',请填写完整道具数量');
                        }
                    }
                }
                $grade = $data['grade'];
                $data['config'] = $data['comsume'];
                break;
            case 1042:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['grade']) || in_array('', $data['grade'])) return array('code' => 404, 'msg' => '请填写世界等级');
                if (empty($data['config'])) return array('code' => 404, 'msg' => '请填写材料提交奖励（游戏内显示）');
                foreach ($data['config']['item_id'] as $key => $val) {
                    if (empty($val)) return (['code' => 404, 'msg' => '请填写提交材料奖励（游戏内显示），道具ID']);
                }
                if (empty($data['day']) || empty($data['top'] || empty($data['level']))) return array('code' => 404, 'msg' => '请填写完整个人累计奖励或全服累计奖励');
                $goldItem = $data['gold']['item_id'];
                foreach ($goldItem as $key => $val) {
                    if (empty($val)) return array('code' => 404, 'msg' => '请填写花费元宝世界等级下' . $data['grade'][$key] . '道具ID');
                    foreach ($val as $kk => $vv) {
                        if (empty($vv)) return array('code' => 404, 'msg' => '请填写花费元宝世界等级：' . $data['grade'][$key] . '下道具ID列为：' . $kk . '');
                        if (empty($data['gold']['count'][$key][$kk])) return array('code' => 404, 'msg' => '请填写花费元宝世界等级为：' . $data['grade'][$key] . ',下道具数量');
                    }
                }
                $level = $data['level'];
                foreach ($level as $ls => $kk) {
                    switch ($ls) {
                        case 0;
                            if (empty($kk)) continue;
                            foreach ($kk['item_id'] as $wl => $dd) {
                                foreach ($dd as $mk => $mi) {
                                    if (empty($kk['num'][$wl][$mk])) continue;
                                    foreach ($mi as $item_key => $item) {
                                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$wl] . ',累计次数档次为:' . $kk['num'][$wl][$mk] . '，请填写道具ID');
                                        if (empty($kk['count'][$wl][$mk][$item_key])) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$wl] . '，累计次数档次为' . $kk['num'][$wl][$mk] . '，请填写道具数量');
                                    }
                                }
                            }
                            break;
                        case 1:
                            $num = $kk['num'];
                            foreach ($num as $key => $value) {
                                if (empty($value)) continue;
                                foreach ($value as $keys => $info) {
                                    if (empty($kk['time'][$key][$keys])) return array('code' => 404, 'msg' => '经验：累计次数档次为：' . $num[$key][$keys] . '，请填写累计时长');
                                    if (empty($kk['per'][$key][$keys])) return array('code' => 404, 'msg' => '经验：累计次数档次为：' . $num[$key][$keys] . '，请填写经验倍数');
                                }
                            }
                            break;
                        case 2:
                            $num = $kk['num'];
                            foreach ($num as $key => $msg) {
                                if (empty($msg)) continue;
                                if (empty($kk['time'][$key])) return array('code' => 404, 'msg' => '累计档次为' . $msg . '，请填写累计时长');
                                if (empty($kk['per'][$key])) return array('code' => 404, 'msg' => '累计档次为' . $msg . '，请填写经验倍数');
                            }
                            break;
                        case 3:
                            #code for type 3
                            break;
                        default:
                            return array('code' => 404, 'msg' => '非法类型');
                            break;
                    }
                }
                $grade = $data['grade'];
                break;
            case 1040:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['grade']) || in_array('', $data['grade'])) return array('code' => 404, 'msg' => '请填写世界等级');
                $itemId = $data['itemId'];
                foreach ($itemId as $key => $val) {
                    if (empty($val)) return array('code' => 404, 'msg' => '请填写档次');
                    foreach ($val as $k => $itemInfo) {
                        if (empty($itemInfo)) return array('code' => 404, 'msg' => '充值次数不能为空或者为0');
                        $pri = json_decode($data['count'][$key][$k], true);
                        $num = count($pri);
                        if ($itemInfo == '-1') {
                            if ($num <= 0) return array('code' => 404, 'msg' => '无限制次数-1 返利百分比中比例，请填写一个及一个以上的');
                        } else {
                            if ($itemInfo != $num) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$key] . ' ,充值次数为：' . $itemInfo . ' 请填写对应充值次数的比例个数');
                        }
                    }
                }
                $grade = $data['grade'];
                break;
            case 1045:
                if ($end_time < $start_time) {
                    $arr = array('code' => 404, 'msg' => '结束时间不能小于开始时间');
                    return $arr;
                }
                if (empty($data['grade']) || in_array('', $data['grade'])) return array('code' => 404, 'msg' => '请填写世界等级');
                $itemId = $data['item_id'];
                foreach ($itemId as $level => $itemInfo) {
                    if (empty($itemInfo)) return array('code' => 404, 'msg' => '请填写奖励');
                    foreach ($itemInfo as $com => $itemMsg) {
                        foreach ($itemMsg as $key => $item) {
                            if (empty($item)) return array('code' => 404, 'msg' => '世界等级：' . $data['grade'][$level] . ',消费元宝数为：' . $data['comsume'][$level][$com] . '请填写完整道具');
                            if (empty($data[$level][$com]['count'][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $data['grade'][$level] . ',消费元宝数为：' . $data['comsume'][$level][$com] . ',请填写完整道具数量');
                        }
                    }
                }
                $grade = $data['grade'];
                $data['config'] = $data['comsume'];
                break;
            case 1047:
                $grade = $data['grade'];
                if (empty($data['item_exchange'])) return array('code' => 404, 'msg' => '请填写祈愿十次可获得积分');
                if (empty($data['tentimes_gold'])) return array('code' => 404, 'msg' => '请填写祈愿十次花费元宝数');
                if (empty($data['item_need_count'])) return array('code' => 404, 'msg' => '请填写祈愿一次可获得积分');
                if (empty($data['once_gold'])) return array('code' => 404, 'msg' => '请填写祈愿一次花费的元宝');
                if (empty($data['freeGold'])) return array('code' => 404, 'msg' => '请填写免费次数');
                //保底奖励
                $day = $data['day'];
                foreach ($day['item_id2'] as $key => $msg) {
                    if (empty($msg)) return array('code' => 404, 'msg' => '积分商店配置，请填写道具ID');
                    if (empty($day['count2'][$key])) return array('code' => 404, 'msg' => '积分商店配置，请填写道具数量');
                    if (empty($day['score2'][$key])) return array('code' => 404, 'msg' => '积分商店配置，请填写积分数量');
                    if (!isset($day['limit2'][$key])) return array('code' => 404, 'msg' => '积分商店配置，请填写限制次数');
                }
                foreach ($day['item_id'] as $key => $msg) {
                    if (empty($msg)) return array('code' => 404, 'msg' => '积分商店配置，请填写道具ID');
                    if (empty($day['count'][$key])) return array('code' => 404, 'msg' => '积分商店配置，请填写道具数量');
                    if (empty($day['score'][$key])) return array('code' => 404, 'msg' => '积分商店配置，请填写积分数量');
                    if (!isset($day['limit'][$key])) return array('code' => 404, 'msg' => '积分商店配置，请填写限制次数');
                }
                //等级奖励
                $top = $data['top'];
                foreach ($top['item_id2'] as $level => $itemInfo) {
                    if (empty($grade[$level])) return array('code' => 404, 'msg' => '请填写世界等级');
                    foreach ($itemInfo as $item_key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '请填写道具ID');
                        if (empty($top['count2'][$level][$item_key])) return array('code' => 404, 'msg' => '世界等级为:' . $grade[$level] . '，道具ID为：' . $item . '请填写道具数量');
                        if (empty($top['weight2'][$level][$item_key])) return array('code' => 404, 'msg' => '世界等级为:' . $grade[$level] . '，道具ID为：' . $item . '，请填写权重信息');
                    }
                }
                foreach ($top['item_id'] as $level => $itemInfo) {
                    if (empty($grade[$level])) return array('code' => 404, 'msg' => '请填写世界等级');
                    foreach ($itemInfo as $item_key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '请填写道具ID');
                        if (empty($top['count'][$level][$item_key])) return array('code' => 404, 'msg' => '世界等级为:' . $grade[$level] . '，道具ID为：' . $item . '请填写道具数量');
                        if (empty($top['weight'][$level][$item_key])) return array('code' => 404, 'msg' => '世界等级为:' . $grade[$level] . '，道具ID为：' . $item . '，请填写权重信息');
                    }
                }
                break;
            case 1043:
                if (!isset($data['freeGold'])) return array('code' => 404, 'msg' => '请填写每天免费次数');
                if (!isset($data['once_gold'])) return array('code' => 404, 'msg' => '请填写求签一次花费元宝');
                if (empty($data['tentimes_gold'])) return array('code' => 404, 'msg' => '请填写求签十次花费元宝');
                if (empty($data['buyCount'])) return array('code' => 404, 'msg' => '请填写可抽签次数');
                $grade = $data['grade'];
                $day = $data['day'];
                $top = $data['top'];
                foreach ($top['item_id2'] as $level => $itemInfo) {
                    if (empty($itemInfo)) return array('code' => 404, 'msg' => '请填写大奖奖励');
                    if (empty($grade[$level])) return array('code' => 404, 'msg' => '请填写世界等级');
                    foreach ($itemInfo as $key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，请填写道具ID');
                        if (empty($top['count2'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，道具ID为：' . $item . '，请填写道具数量');
                    }
                }
                foreach ($top['item_id'] as $level => $itemInfo) {
                    if (empty($itemInfo)) return array('code' => 404, 'msg' => '请填写大奖奖励');
                    foreach ($itemInfo as $key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，请填写道具ID');
                        if (empty($top['count'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，道具ID为：' . $item . '，请填写道具数量');
                    }
                }
                foreach ($day['item_id'] as $level => $itemInfo) {
                    if (empty($itemInfo)) return array('code' => 404, 'msg' => '请填写普通奖励');
                    foreach ($itemInfo as $key => $value) {
                        if (empty($value)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，请填写道具ID');
                        if (empty($day['count'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，道具ID为：' . $value . '，请填写道具数量');
                        if (empty($day['weight'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，道具ID为：' . $value . '，请填写权重');
                    }
                }

                foreach ($data['gold']['round'] as $level => $info) {
                    if (empty($info)) return array('code' => 404, 'msg' => '世界等级为' . $grade[$level] . '，请填写剩余次数区间和权值信息');
                    foreach ($info as $k => $v) {
                        if (!isset($v)) return array('code' => 404, 'msg' => '世界等级为' . $grade[$level] . '，请填写剩余次数区间');
                        if (!isset($data['gold']['weight'][$level][$k])) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，剩余次数为：' . $v . '，请填写权值');
                    }
                }
                break;
            case 1051:
                if (empty($data['buyCount'])) return array('code' => 404, 'msg' => '请填写最低充值');
                $grade = $data['grade'];
                if (empty($grade) || in_array('', $grade) || in_array(null, $grade)) return array('code' => 404, 'msg' => '请填写世界等级');
                $item_id = $data['item_id'];
                foreach ($item_id as $level => $itemInfo) {
                    if (empty($itemInfo)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，请填写道具奖励');
                    foreach ($itemInfo as $key => $item) {
                        if (empty($item)) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，请填写道具ID');
                        if (empty($data['count'][$level][$key])) return array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，道具ID为' . $item . '，请填写道具数量');
                    }
                }
                break;
            default :
                exit('error activity type');
                break;//end foreach
        }
        //end switch
        //判断是否存在合服
        if (!empty($end_time) && !empty($start_time)) {
            $where = "merge_status = 1 and merge_time < {$end_time} and merge_time > {$start_time} ";
        } else {
            $where = "merge_status = 1 and merge_time < 0 and merge_time > 0 ";
        }
        //@todo去检查是否有合服情况
        $server = (new ServerModel)->getServerByServerAndChannelGroup($servers, $platforms, $where, false);
        if (!empty($server)) {
            $arr = array('code' => 404, 'msg' => implode(',', $server) . '在该活时间内正在进行合服');
            return $arr;
        }
        //做一下容错，避免某些活动没有
        if (empty($grade)) {
            $grade = array();
        }

        if (empty($amount)) {
            $amount = array();
        }

        if (empty($itemWeight)) {
            $itemWeight = array();
        }

        if (empty($career)) {
            $career = array();
        }

        if (empty($vip_level)) {
            $vip_level = array();
        }

        //活动ID
        $award = array(
            'grade' => $grade,
            'amount' => $amount,
            'item_id' => $itemId,
            'item_count' => $itemCount,
            'career' => $career,
        );


        isset($data['config']) ? $award['config'] = $data['config'] : '';
        //添加权重
        isset($data['weight']) ? $award['weight'] = $data['weight'] : '';
        //保底
        isset($data['top']) ? $award['top'] = $data['top'] : '';
        isset($data['day']) ? $award['day'] = $data['day'] : '';
        isset($data['buyCount']) ? $award['buyCount'] = $data['buyCount'] : array();
        isset($data['gold']) ? $award['gold'] = $data['gold'] : array();
        isset($data['count']) ? $award['count'] = $data['count'] : array();
        isset($data['type']) ? $award['type'] = $data['type'] : 0;
        isset($data['freeGold']) ? $award['freeGold'] = $data['freeGold'] : 0;

        //掉落活动
        isset($data['form']) ? $award['form'] = $data['form'] : '';
        isset($data['shortDesc']) ? $award['shortDesc'] = $data['shortDesc'] : '';
        isset($data['description']) ? $award['description'] = $data['description'] : '';
        isset($data['level']) ? $award['level'] = $data['level'] : '';
        isset($data['monsterLevel']) ? $award['monsterLevel'] = $data['monsterLevel'] : '';
        isset($data['random']) ? $award['random'] = $data['random'] : '';
        isset($data['iconId']) ? $award['iconId'] = $data['iconId'] : '';
        isset($data['dropItemId']) ? $award['dropItemId'] = $data['dropItemId'] : '';
        isset($data['monsterId']) ? $award['monsterId'] = $data['monsterId'] : '[]';
        isset($data['item_need_count']) ? $award['item_need_count'] = $data['item_need_count'] : array();
        isset($data['item_exchange']) ? $award['item_exchange'] = $data['item_exchange'] : array();
        isset($data['mapRange']) ? $award['mapRange'] = $data['mapRange'] : '[]';

        //全民
        isset($data['itemId']) ? $award['itemId'] = $data['itemId'] : '';
        isset($data['open_hour']) ? $award['open_hour'] = $data['open_hour'] : '';
        isset($data['open_min']) ? $award['open_min'] = $data['open_min'] : '';
        isset($data['close_hour']) ? $award['close_hour'] = $data['close_hour'] : '';
        isset($data['close_min']) ? $award['close_min'] = $data['close_min'] : '';
        isset($data['value']) ? $award['value'] = $data['value'] : '';

        isset($data['minGold']) ? $award['minGold'] = $data['minGold'] : '';
        //集福
        isset($data['list']) ? $award['list'] = $data['list'] : array();//奖励数组
        //扭蛋
        isset($data['vip_level']) ? $award['vip_level'] = $data['vip_level'] : array();//vip等级
        isset($data['charge_gold']) ? $award['charge_gold'] = $data['charge_gold'] : 0;//充值元宝
        //聚划算
        isset($data['big_price_name']) ? $award['big_price_name'] = $data['big_price_name'] : '';
        isset($data['price_old']) ? $award['price_old'] = $data['price_old'] : 0;
        isset($data['price_now']) ? $award['price_now'] = $data['price_now'] : 0;
        isset($data['item_id']) ? $award['item_id'] = $data['item_id'] : array();
        isset($data['item_count']) ? $award['item_count'] = $data['item_count'] : array();
        isset($data['career']) ? $award['career'] = $data['career'] : array();

        //幸运大转盘
        isset($data['once_gold']) && $award['once_gold'] = $data['once_gold'];
        isset($data['tentimes_gold']) && $award['tentimes_gold'] = $data['tentimes_gold'];
        isset($data['once_score']) && $award['once_score'] = $data['once_score'];
        isset($data['input_gold']) && $award['input_gold'] = $data['input_gold'];
        isset($data['new_server']) && $award['new_server'] = $data['new_server'];
        isset($data['score_num']) && $award['score_num'] = $data['score_num'];
        $award = json_encode($award);
        //执行award校验，后期扩展
        //error_log($award."\n\r",3,'server.log');
        switch ($act_id) {
            case 1033:
                //扭蛋
                self::check_reward($award);
                break;
            case 1038:
                //成长翻倍
                self::growLevelCheck($data['config']);
                break;
            case 1037://摇摇乐
                self::check_YYreward($award);
                break;
            case 1036://许愿树
                //普通物品的数量应该大于或者等于抽中贵重物品所需要的次数
                $infos = json_decode($award, true);
                $grade = $infos['grade'];
                $score_num = $infos['score_num'];

                foreach ($infos['career'] as $level => $car) {
                    $carInfo = [];
                    foreach ($car as $key => $car_num) {
                        if ($infos['value'][$level][$key] == 1) continue;//剔除贵重物品
                        $carInfo[$car_num][] = $infos['item_id'][$level][$key];
                    }

                    if (empty($carInfo)) continue;
                    foreach ($carInfo as $cNum => $vals) {
                        $num = count($vals);
                        $carName = $cNum == 1 ? '刀' : ($cNum == 2 ? '弓' : ($cNum == 3 ? '法' : '通用'));
                        if ($num < $score_num) die(json_encode(['code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，角色为：' . $carName . '，普通物品的数量应该大于或者等于抽中贵重物品所需要的次数，当前数量为:' . $num . '']));
                    }
                }
                break;
            case 1047:
                $infos = json_decode($award, true);
                $info = $infos['top'];
                $grade = $infos['grade'];
                $itemId = self::arrayMergeItem(array($info['item_id2'], $info['item_id']));
                foreach ($itemId as $level => $item) {
                    if (count($item) > 21) die(json_encode(array('code' => 404, 'msg' => '世界等级为：' . $grade[$level] . '，时装+装备和通用道具总数量请少于21项')));
                }
                break;
            default:
                #code for default
                break;
        }
        $platforms = implode(',', $_POST['channel_group']);
        $servers = implode(',', $_POST['server']);
        if (empty($platforms)) {
            $platforms = self::getChannelGroupByTrait();
            $platforms = array_column($platforms, 'id');
            $platforms = implode(',', $platforms);
        }
        $insert = array(
            'title' => $data['title'],
            'big_act_id' => isset($data['big_act_id']) ? $data['big_act_id'] : 0,
            'act_id' => $act_id,
            'platforms' => implode(',', $data['channel_group']),
            'servers' => implode(',', $data['server']),
            'start_time' => $start_time,
            'end_time' => $end_time,
            'content' => $data['content'],
            'state' => $data['state'],
            'award' => $award,
            'add_user' => $_SESSION['username'],
            'platforms' => $platforms,
            'servers' => $servers,
            'openLvl' => isset($data['openLvl']) ? intval($data['openLvl']) : 0,
            'create_time' => time(),
            'type' => 0,
            'status' => 0,
            'activity_sign' => $activity_sign,
            'open_ser_day' => $openSerDay,
        );

        $id = isset($_POST['id']) ? $_POST['id'] : '';
        //更新
        if (!empty($id)) {
            $insertId = $this->update($insert, array('id' => $id));
        } else {
            $insert['uuid'] = Helper::uuid();
            $insertId = $this->add($insert);
        }
        if (empty($insertId)) {
            return array('code' => 404, 'msg' => '数据库插入失败');
        }
        return array('code' => 200, 'msg' => '操作成功');

    }

    /**
     * [audit_data 审核]
     * @param  [array] $conditions [description]
     * @return [type]             [description]
     */
    public function audit_data($conditions)
    {
        $rs = array();
        $num = 0;
        $fields = array('id', 'title', 'start_time', 'end_time', 'type', 'act_id', 'platforms', 'add_user', 'check_user', 'check_time');
        $conditions['WHERE']['type'] = 1;
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($rs) {
            $num = $this->getCount();
            foreach ($rs as $k => $v) {
                $rs[$k]['id'] = '<input class="sorting_1 checkbox"  type="checkbox" value=' . $v['id'] . '>' . $v['id'];
                $rs[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
                $rs[$k]['end_time'] = date('Y-m-d H:i:s', $v['end_time']);

                if ($v['type'] == 0) {
                    $rs[$k]['type'] = '<button class="btn btn-default">待提审</button>';
                } elseif ($v['type'] == 1) {
                    $rs[$k]['type'] = '<button class="btn btn-warning">已提审</button>';
                } elseif ($v['type'] == 2) {
                    $rs[$k]['type'] = '<button class="btn btn-success">已发布</button>';
                } elseif ($v['type'] == 3) {
                    $rs[$k]['type'] = '<button class="btn btn-danger">被驳回</button>';
                }

                if (!empty($v['platforms'])) {
                    $sql = "select name from  ny_channel_group where id in (" . $v['platforms'] . ")";
                    $group = $this->query($sql);
                    $group = array_column($group, 'name');
                    $group = implode(',', $group);
                    $rs[$k]['platforms'] = $group;
                } else {
                    $rs[$k]['platforms'] = '全部渠道组';
                }

                $rs[$k]['act_id'] = CDict::$activityType[$v['act_id']];


                $rs[$k]['check_time'] = $v['check_time'] != 0 ? date('Y-m-d H:i:s', $v['check_time']) : '';

                $str = '<button style="margin-bottom:5px;" class="gbutton" onclick="_edit(\'' . $v['id'] . '\')">查看</button> | ' .
                    '<button style="margin-bottom:5px;" class="gbutton" onclick="_agree(\'' . $v['id'] . '\')">发布</button>' .
                    '<button style="margin-bottom:5px;" class="gbutton" onclick="_disagree(\'' . $v['id'] . '\')">驳回</button> | ';;

                $rs[$k]['caozuo'] = $str;
            }
            foreach ($rs as $k => $v) {
                $rs[$k] = array_values($v);
            }
        }

        $this->setPageData($rs, $num);
    }

    /**
     * 格式代数组
     * @param  [int] $id [活动ID]
     * @return [type]     [description]
     */
    public function formatData($data)
    {
        $act_id = $data['act_id'];
        $award = json_decode($data['award'], true);
        $grade = $award['grade'];
        $amount = $award['amount'];
        $itemCount = $award['item_count'];
        $itemId = $award['item_id'];
        $career = $award['career'];
        $return = [];

        switch ($act_id) {
            case 1065: //每日充值
                //档次
                //遍历条件
                $index = 1;
                $list = $dayList = array();
                foreach ($amount as $k => $if) {
                    //物品条件
                    if (!isset($itemId[$k])) {
                        continue;
                    }

                    $award2 = $career1 = $career2 = $career3 = array();
                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (empty($goods_id)) {
                            continue;
                        }
                        $goods_id = $this->explodeGoodsId($goods_id);
                        $goods_num = intval($itemCount[$k][$k2]);
                        $award2[] = array($goods_id, $goods_num);
                    }
                    $list[] = array(
                        'diamond' => (int)$if,
                        'itemList' => $award2,
                    );
                    $index++;
                }

                foreach ($grade['day2'][1] as $k => $v) {
                    $dayList[$k]['day'] = (int)$v;
                    $item_id2 = $this->explodeGoodsId($grade['item_id2'][1][$k]);
                    $dayList[$k]['itemList'] = [$item_id2, (int)$grade['item_count2'][1][$k]];
                }
                return array('diamondList' => $list, 'dayDiamond' => (int)$grade['amount2'], 'dayList' => $dayList);
                //end foreach
                break;
            case 1002: //单笔充值 Start
                $row = array();
                $index = 1;
                foreach ($grade as $k => $level) {
                    //判断是不否填入相对应物品条件
                    if (!isset($amount[$k])) {
                        continue;
                    }

                    //遍历条件
                    $if_index = 1;
                    $ifRow = array();
                    foreach ($amount[$k] as $k2 => $if) {
                        //物品条件
                        if (!isset($itemId[$k][$k2])) {
                            continue;
                        }

                        $career1 = $career2 = $career3 = array();

                        $goods = array();
                        foreach ($itemId[$k][$k2] as $k3 => $goods_id) {
                            if (empty($goods_id)) {
                                continue;
                            }

                            $goods_id = explode('/', $goods_id);
                            $goods_id = $goods_id[0];

                            $goods_num = intval($itemCount[$k][$k2][$k3]);
                            $car = $career[$k][$k2][$k3];

                            switch ($car) {
                                case 0: //全部
                                    $career1[] = array($goods_id, $goods_num);
                                    $career2[] = array($goods_id, $goods_num);
                                    $career3[] = array($goods_id, $goods_num);
                                    break;
                                case 1: //刀
                                    $career1[] = array($goods_id, $goods_num);
                                    break;
                                case 2:
                                    $career2[] = array($goods_id, $goods_num);
                                    break;
                                default:
                                    # code...
                                    break;
                            }

                        }
                        // itemId end

                        $goods = array($career1, $career2, $career3);

                        $ifRow[] = array('index' => $if_index, 'value' => intval($if), 'award' => $goods);

                        $if_index++;
                    }
                    //amount end

                    $uuid = substr($data['uuid'], 0, strlen($data['uuid']) - 1);

                    $uuid = $uuid . count($row);

                    $row[] = array(
                        '_id' => $uuid,
                        'aId' => $act_id,
                        'name' => '单笔充值',
                        'tmType' => 1,
                        'tm' => array('start' => strtotime($level['start']), 'close' => strtotime($level['end'])),

                        'state' => intval($data['state']),
                        'openLvl' => intval($data['openLvl']),
                        "srcId" => "srcId",
                        'content' => array('list' => $ifRow)
                    );
                    $index++;
                }

                return $row;
                break;
            case 1004: //首充团购
                $row = array();
                $index = 1;
                foreach ($grade as $k => $level) {
                    //判断是不否填入相对应物品条件
                    if (!isset($amount[$k])) {
                        continue;
                    }

                    //遍历条件
                    $if_index = 1;
                    $ifRow = array();
                    foreach ($amount[$k] as $k2 => $if) {
                        //物品条件
                        if (!isset($itemId[$k][$k2])) {
                            continue;
                        }

                        $career1 = $career2 = $career3 = array();

                        $goods = array();
                        foreach ($itemId[$k][$k2] as $k3 => $goods_id) {
                            if (empty($goods_id)) {
                                continue;
                            }

                            $goods_id = explode('/', $goods_id);
                            $goods_id = $goods_id[0];

                            $goods_num = intval($itemCount[$k][$k2][$k3]);
                            $car = $career[$k][$k2][$k3];

                            switch ($car) {
                                case 0: //全部
                                    $career1[] = array($goods_id, $goods_num);
                                    $career2[] = array($goods_id, $goods_num);
                                    $career3[] = array($goods_id, $goods_num);
                                    break;
                                case 1: //刀
                                    $career1[] = array($goods_id, $goods_num);
                                    break;
                                case 2:
                                    $career2[] = array($goods_id, $goods_num);
                                    break;
                                default:
                                    # code...
                                    break;
                            }

                        }
                        // itemId end

                        $goods = array($career1, $career2, $career3);

                        $ifRow[] = array('index' => $if_index, 'value' => intval($if), 'award' => $goods);

                        $if_index++;
                    }
                    //amount end

                    $lv = intval($level);
                    $row[] = array('index' => $index, 'count' => $lv, 'title' => $lv . '人首充', 'list' => $ifRow);
                    $index++;
                }

                return array('list' => $row);
                break;
            case 1008: //烟花
                $weight = $award['weight'];
                $row = array();

                foreach ($itemId as $k => $goods_id) {
                    if (empty($goods_id)) {
                        continue;
                    }

                    $goods_id = explode('/', $goods_id);
                    $goods_id = $goods_id[0];

                    $goods_num = intval($itemCount[$k]);
                    $car = $career[$k];

                    $w = $weight[$k];

                    switch ($car) {
                        case 0: //全部
                            $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $w);
                            $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $w);
                            $career3[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $w);
                            break;
                        case 1: //刀
                            $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $w);
                            break;
                        case 2:
                            $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $w);
                            break;
                        default:
                            # code...
                            break;
                    }
                    // itemId end

                    $goods = array($career1, $career2, $career3);

                    $reward = array('worldLevel' => 1, 'itemList' => $goods);

                }

                $row = array('yanhua' => 3980050000002, gold => intval($amount), 'reward' => array($reward));

                return $row;
            case 1030: //砸蛋
                $config = $award['config'];

                //配置奖励
                $cItemId = $config['item_id'];

                $cItemCount = $config['item_count'];
                $cCareer = $config['career'];
                $cWeight = $config['weight'];
                $crare = $config['rare'];

                $reward = $extraReward = array();

                foreach ($grade as $k => $level) {
                    //config  判断是否填完所有
                    if (empty($level)) {
                        continue;
                    }

                    if (!isset($cItemId[$k])) {
                        continue;
                    }

                    $itemList = array();
                    //配置奖励

                    $career1 = $career2 = $career3 = array();
                    foreach ($cItemId[$k] as $k2 => $goods_id) {

                        if (empty($goods_id)) {
                            continue;
                        }

                        $goods_id = explode('/', $goods_id);
                        $goods_id = $goods_id[0];


                        $goods_num = intval($cItemCount[$k][$k2]);
                        $car = $cCareer[$k][$k2][$k3];

                        $weight = intval($cWeight[$k][$k2]);
                        $rare = intval($crare[$k][$k2]);


                        switch ($car) {
                            case 0: //全部
                            case 0: //全部
                                $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight, 'rare' => $rare);
                                $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight, 'rare' => $rare);
                                $career3[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight, 'rare' => $rare);
                                break;
                            case 1: //刀
                                $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight, 'rare' => $rare);
                                break;
                            case 2:
                                $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight, 'rare' => $rare);
                                break;
                            default:
                                # code...
                                break;
                        }

                    }

                    $itemList = array($career1, $career2, $career3);

                    $reward[] = array(
                        'worldLevel' => intval($level),
                        'itemList' => $itemList,
                    );


                    //判断是存该世界等级的额外条件
                    if (!isset($amount[$k])) {
                        continue;
                    }

                    $list = array();

                    //遍历条件
                    foreach ($amount[$k] as $k2 => $if) {
                        //物品条件
                        if (!isset($itemId[$k][$k2])) {
                            continue;
                        }

                        $career1 = $career2 = $career3 = array();
                        foreach ($itemId[$k][$k2] as $k3 => $goods_id) {
                            if (empty($goods_id)) {
                                continue;
                            }

                            $goods_id = explode('/', $goods_id);
                            $goods_id = $goods_id[0];

                            $goods_num = intval($itemCount[$k][$k2][$k3]);
                            $car = $career[$k][$k2][$k3];

                            switch ($car) {
                                case 0: //全部
                                    $career1[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                    $career2[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                    $career3[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                    break;
                                case 1: //刀
                                    $career1[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                    break;
                                case 2:
                                    $career2[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                    break;
                                default:
                                    # code...
                                    break;
                            }

                            $itemList = array($career1, $career2, $career3);
                        }

                        $list[] = array(
                            'count' => intval($if),
                            'itemList' => $itemList,
                        );

                    }

                    $extraReward[] = array(
                        'worldLevel' => intval($level),
                        'list' => $list,
                    );
                    //end foreach
                }
                //end foreach $grade

                $content = array(
                    'eggGold' => 20,
                    'eggTotal' => 8,
                    'rEggGold' => 10,
                    'rTime' => 28800,
                    'rFreeCount' => 1,
                    'rarePer' => 0.03,
                    'rareTimes' => 2,
                    'rareWeight' => 1,
                    'reward' => $reward,
                    'extraReward' => $extraReward,
                );

                return $content;
                break;
            case 1009: //消费排行榜
                $reward = array();
                foreach ($grade as $k => $level) {
                    //config  判断是否填完所有
                    if (empty($level)) {
                        continue;
                    }

                    if (!isset($itemId[$k])) {
                        continue;
                    }

                    $itemList = array();
                    foreach ($itemId[$k] as $k2 => $row) {
                        $car1 = $this->explodeGoodsId($row['career1']);
                        $car2 = $this->explodeGoodsId($row['career2']);
                        $car3 = $this->explodeGoodsId($row['career3']);

                        $cCar1 = $itemCount[$k][$k2]['career1'];
                        $cCar2 = $itemCount[$k][$k2]['career2'];
                        $cCar3 = $itemCount[$k][$k2]['career3'];

                        $itemList[] = array(
                            array('itemId' => $car1, 'count' => intval($cCar1)),
                            array('itemId' => $car2, 'count' => intval($cCar2)),
                            array('itemId' => $car3, 'count' => intval($cCar3)),
                        );
                    }

                    //end foreach

                    $reward[] = array(
                        'worldLevel' => intval($level),
                        'itemList' => $itemList,
                    );
                }


                //end foreach

                return $content = array(
                    'minGold' => intval($award['minGold']),
                    'openPrizeTm' => array(21, 0),
                    'reward' => $reward,
                );
                break;
            case 1003: //累计充值
                //档次
                $amount = $award['amount'];
                $itemId = $award['item_id'];
                $itemCount = $award['item_count'];
                $career = $award['career'];

                //遍历条件
                $index = 1;
                $list = array();
                foreach ($amount as $k => $if) {
                    //物品条件
                    if (!isset($itemId[$k])) {
                        continue;
                    }

                    $career1 = $career2 = $career3 = array();
                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (empty($goods_id)) {
                            continue;
                        }

                        $goods_id = $this->explodeGoodsId($goods_id);

                        $goods_num = intval($itemCount[$k][$k2]);
                        $car = $career[$k][$k2];

                        switch ($car) {
                            case 0: //全部
                                $career1[] = array($goods_id, $goods_num);
                                $career2[] = array($goods_id, $goods_num);
                                $career3[] = array($goods_id, $goods_num);
                                break;
                            case 1: //刀
                                $career1[] = array($goods_id, $goods_num);
                                break;
                            case 2:
                                $career2[] = array($goods_id, $goods_num);
                                break;
                            default:
                                # code...
                                break;
                        }
                    }


                    $award = array($career1, $career2, $career3);

                    $list[] = array(
                        'index' => $index,
                        'value' => intval($if),
                        'award' => $award,
                    );

                    $index++;
                }

                return array('list' => $list);
                //end foreach
                break;
            case 1006:  //幸运鉴宝
                //档次
                $amount = $award['amount'];
                $itemId = $award['item_id'];
                $itemCount = $award['item_count'];
                $itemWeight = $award['weight'];

                //遍历条件
                $topPrize = array();
                foreach ($amount as $k => $if) {
                    $goods_id = $if['goods_id'];
                    $goods_num = intval($if['goods_num']);
                    $weight = intval($if['weight']);

                    $goods_id = $this->explodeGoodsId($goods_id);

                    $topPrize[] = array(
                        'itemId' => $goods_id,
                        'count' => $goods_num,
                        'weight' => $weight,
                    );
                }

                $prizeList = array();

                foreach ($itemId as $k => $goods_id) {
                    $goods_num = intval($itemCount[$k]);
                    $weight = intval($itemWeight[$k]);

                    $goods_id = $this->explodeGoodsId($goods_id);

                    $prizeList[] = array(
                        'itemId' => $goods_id,
                        'count' => $goods_num,
                        'weight' => $weight,
                    );
                }

                return array(
                    'type' => intval($award['type']),
                    'gold' => intval($award['gold']),
                    'freeGold' => intval($award['freeGold']),
                    'luckyValue' => 10,
                    'maxLuckyValue' => 600,
                    'topPrize' => $topPrize,
                    'prizeList' => $prizeList,
                );

                break;
            case 1005:
                $top = $award['top'];

                $itemId = $award['item_id'];
                $career = $award['career'];
                $itemWeight = $award['weight'];
                $itemCount = $award['item_count'];

                $list = array();
                foreach ($top as $k => $r) {

                    $buyCount = intval($award['buyCount'][$k]);
                    $gold = $award['gold'][$k];
                    $count = $award['count'][$k];

                    $count = json_decode($count, true);

                    $topPrize = array();

                    $goods_num = $r['item_count'];

                    $weight = $r['item_weight'];

                    foreach ($r['item_id'] as $k2 => $goods_id) {
                        $goods_id = $this->explodeGoodsId($goods_id);
                        $num = intval($goods_num[$k2]);

                        $topPrize[] = array('itemId' => $goods_id, 'count' => $num);
                    }


                    $prizeList = array();

                    $career1 = $career2 = $career3 = array();
                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (empty($goods_id)) {
                            continue;
                        }

                        $goods_id = $this->explodeGoodsId($goods_id);

                        $goods_num = intval($itemCount[$k][$k2]);

                        $weight = intval($itemWeight[$k][$k2]);

                        $car = $career[$k][$k2];

                        switch ($car) {
                            case 0: //全部
                                $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight);
                                $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight);
                                $career3[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight);
                                break;
                            case 1: //刀
                                $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight);
                                break;
                            case 2:
                                $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight);
                                break;
                            default:
                                # code...
                                break;
                        }

                        $prizeList = array($career1, $career2, $career3);
                    }

                    $list[] = array(
                        'buyCount' => $buyCount,
                        'gold' => intval($gold),
                        'count' => $count,
                        'openPrizeTm' => array(21, 0),
                        'topPrize' => $topPrize,
                        'prizeList' => $prizeList,
                    );
                }

                if (count($list) == 1) {
                    $list[1] = $list[0];
                }


                return array(
                    'day' => intval($award['day']),
                    'list' => $list,
                );
                break;
            case 1007: //全民嗨
                //档次
                $amount = $award['amount'];
                $itemId = $award['item_id'];
                $itemCount = $award['item_count'];
                $career = $award['career'];
                //遍历条件
                $list = array();
                foreach ($amount as $k => $if) {
                    //物品条件
                    if (!isset($itemId[$k])) {
                        continue;
                    }

                    $career1 = $career2 = $career3 = array();
                    foreach ($itemId[$k] as $k2 => $goods_id) {
                        if (empty($goods_id)) {
                            continue;
                        }

                        $goods_id = $this->explodeGoodsId($goods_id);

                        $goods_num = intval($itemCount[$k][$k2]);
                        $car = $career[$k][$k2];

                        switch ($car) {
                            case 0: //全部
                                $career1[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                $career2[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                $career3[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                break;
                            case 1: //刀
                                $career1[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                break;
                            case 2:
                                $career2[] = array('itemId' => $goods_id, 'count' => $goods_num);
                                break;
                            default:
                                # code...
                                break;
                        }
                    }


                    $itemList = array($career1, $career2, $career3);

                    $list[] = array(
                        'high' => intval($if),
                        'itemList' => $itemList,
                    );
                }

                $dailyTask = json_decode('[[1,1,20],[2,10,2],[3,10,3],[6,10,2],[7,10,2],[8,10,2],[11,10,3]]', true);
                $limitTask = json_decode('[[4,10,1],[5,2,10],[6,10,1],[7,10,1],[8,10,1]]', true);
                $operation = json_decode('[[1,3,50],[2,5,5]]', true);

                return array(
                    'dailyTask' => $dailyTask,
                    'limitTask' => $limitTask,
                    'operation' => $operation,
                    'reward' => $list
                );
                break;
            //end foreach
            case 1010:
                $row = array();

                $itemId = $award['item_id'];
                $itemCount = $award['item_count'];
                $itemNeedCount = $award['item_need_count'];
                $itemExchange = $award['item_exchange'];

                $exchangeList = array();
                foreach ($itemId as $k => $goods_id) {
                    if (empty($goods_id)) {
                        continue;
                    }

                    $goods_id = $this->explodeGoodsId($goods_id);

                    $needCount = intval($itemNeedCount[$k]);

                    $count = intval($itemCount[$k]);
                    $canExchange = intval($itemExchange[$k]);

                    $exchangeList[] = array(
                        'needCount' => $needCount,
                        'itemId' => $goods_id,
                        'count' => $count,
                        'canExchange' => $canExchange,
                    );
                }

                $return = array(
                    'iconId' => $this->explodeGoodsId($award['iconId']),
                    'dropItemId' => $this->explodeGoodsId($award['dropItemId']),
                    'form' => $award['form'],
                    'shortDesc' => $award['shortDesc'],
                    'description' => $award['description'],
                    'level' => intval($award['level']),
                    'monsterId' => json_decode($award['monsterId'], true),
                    'monsterLevel' => intval($award['monsterLevel']),
                    'mapRange' => json_decode($award['mapRange'], true),
                    'random' => doubleval($award['random']),
                    'exchangeList' => $exchangeList,
                );

                return $return;
                break;
            case 1020:
                $row = array();

                $form = $award['form'];

                $return = array(
                    'iconId' => $this->explodeGoodsId($award['iconId']),
                    'itemId' => $this->explodeGoodsId($award['itemId']),
                    'form' => $award['form'],
                    'shortDesc' => $award['shortDesc'],
                    'description' => $award['description'],
                    'level' => intval($award['level']),

                    'openTime' => array(intval($award['open_hour']), intval($award['open_min'])),
                    'closeTime' => array(intval($award['close_hour']), intval($award['close_min'])),
                    'value' => intval($award['value']),
                );

                return $return;
                break;
            case 1032:
                isset($award['list'][1]['topPrize']) && $newTop = $award['list'][1]['topPrize'];//大奖奖励
                isset($award['list'][1]['normal']) && $newLst = $award['list'][1]['normal'];//普通奖励
                $list = array();
                $price = array();
                if (isset($newTop)) {
                    //获取新服
                    $topPrize = self::getTopPack($newTop);//大奖
                    $price['topPrize'] = $topPrize;
                } else {
                    break;
                }
                if (isset($newLst)) {
                    $prizeList = self::getLstPack($newLst);
                    $price['prizeList'] = $prizeList;
                } else {
                    break;
                }
                array_push($list, $price);
                //获取老服
                $oldTop = isset($award['list'][2]['topPrize']) ? $award['list'][2]['topPrize'] : $award['list'][1]['topPrize'];
                $oldLst = isset($award['list'][2]['normal']) ? $award['list'][2]['normal'] : $award['list'][1]['normal'];
                $old['topPrize'] = self::getTopPack($oldTop);
                $old['prizeList'] = self::getLstPack($oldLst);
                array_push($list, $old);
                $return = array(
                    'day' => intval($award['day']),
                    'gold' => intval($award['gold']),
                    'totalCount' => 10,
                    'per' => 0.1,
                    'rule' => [[1, 1, 3], [2, 2, 9], [3, 7, 10]],
                    'text' => ["点击翻牌集福得超级大奖！", "还差一点点，就能获得一福大奖", "再来一个福即可获得二福大奖", "还差最后一个福即可获得三福超级大奖"],
                    'list' => $list
                );
                return $return;
                break;
            case 1033:
                $rewardPrice = self::getReward($award);
                $accRewardPrice = self::getAccReward($award);
                $return['gold'] = intval($award['charge_gold']);
                $return['record'] = 50;
                $return['reward'] = $rewardPrice;
                $return['AccAward'] = $accRewardPrice;
                return $return;
                break;
            case 1034:
                $prizeList = self::getPrizeList($award);
                $topPrize = self::getTopPrize($award);
                $return['prizeList'] = $prizeList;
                $return['topPrize'] = $topPrize;
                return $return;
                break;
            case 1035:
                $prizeList = self::luckyGetPrizeList($award['config']);
                $exchange = self::luckyGetExchange($award);
                $return['poolGoldNum'] = 10000;
                $return['onceGold'] = intval($award['once_gold']);
                $return['tenTimesGold'] = intval($award['tentimes_gold']);
                $return['luckyPrize'] = [[0.1, 1], [0.2, 1], [0.3, 1]];
                $return['recordNum'] = 50;
                $return['onceScore'] = intval($award['once_score']);
                $return['inputGold'] = intval($award['input_gold']);
                $return['prizeList'] = $prizeList;
                $return['exchange'] = $exchange;
                return $return;
                break;
            case 1036:
                #code for  Christmas wish
                $return['free'] = $award['freeGold'] ? intval($award['freeGold']) : 1;//免费次数
                $return['number'] = $award['score_num'] ? intval($award['score_num']) : 1;//抽中贵重物品需要次数

                $gold = $award['gold'];
                $goldArr = [];
                $i = 1;
                foreach ($gold as $mk => $msg) {
                    array_push($goldArr, array(intval($msg), $i));
                    $i++;
                }
                $return['gold'] = $goldArr;
                $itemList = self::getItemLst($award);
                $return['reward'] = $itemList;
                return $return;
                break;
            case 1037:
                $onceId = !empty($award['open_hour']) ? explode('/', $award['open_hour']) : null;
                $tenId = !empty($award['open_min']) ? explode('/', $award['open_min']) : null;
                $return['onceCost'] = array('itemId' => $onceId[0], 'count' => (!empty($onceId[0] && !empty($award['item_id']))) ? intval($award['item_id']) : 1, 'gold' => intval($award['close_hour']));
                $return['tenCost'] = array('itemId' => $tenId[0], 'count' => (!empty($award['item_count']) && !empty($award['item_count'])) ? intval($award['item_count']) : 1, 'gold' => intval($award['close_min']));
                /*$persenItem = [];
                foreach($award['item_exchange'] as $key=>$val){
                    $goodId = explode('/', $val);
                    $goodId = $goodId[0];
                    $temp['itemId'] = $goodId;
                    $temp['count'] = $award['item_need_count'][$key]?intval($award['item_need_count'][$key]):1;
                    $persenItem[] = $temp;
                }
                $return['presentItem'] = $persenItem;*/
                $topPrize = self::getYYPrize(array('info' => $award['top'], 'grade' => $award['grade']));//获取大奖
                $plist = self::getYYPrize(array('info' => $award['day'], 'grade' => $award['grade']));//获取普通奖励
                $accAward = self::getYYAcc(array('info' => $award['config'], 'grade' => $award['grade']));//累计奖励
                $return['topPrize'] = $topPrize;
                $return['prizeList'] = $plist;
                $return['AccAward'] = $accAward;
                return $return;
                break;
            case 1038:
                //成长翻倍
                $config = $award['config'];
                $level = array_shift($config['level']);
                $type = array_keys($level);
                foreach ($type as &$typeId) {
                    $typeId = intval($typeId);
                }
                sort($type);
                $return['openList'] = $type;
                $reward = self::growGetReward($award);
                $return['reward'] = $reward;
                return $return;
                break;
            case 1039:
                $content = self::getBoxContent($award);
                return $content;
                break;
            case 1041:
                #code for comsume
                $comsumeInfo = self::getComsumeInfo($award);
                $return['ComsumeInfo'] = $comsumeInfo;
                return $return;
                break;
            case 1042:
                $monsterLever = intval($award['monsterLevel']);
                $dropId = intval($award['dropItemId']);
                $return['drop'] = array('monsterLevel' => $monsterLever, 'dropId' => $dropId);
                $itemPrizeList = self::getItemPrizeList(array('info' => $award['day'], 'grade' => $award['grade']));
                $return['itemPrizeList'] = $itemPrizeList;
                $return['gold'] = !empty($award['freeGold']) ? intval($award['freeGold']) : 0;
                $goldPrizeList = self::getGoldrizeList(array('info' => $award['gold'], 'grade' => $award['grade']));
                $return['goldPrizeList'] = $goldPrizeList;
                $awardItemList = self::getAwardItemList($award['config']);
                $return['awardItemList'] = $awardItemList;
                $accAward = self::getSnowAcc(array('info' => $award['top'], 'grade' => $award['grade']));
                $return['AccAward'] = $accAward;
                $allAccAward = self::getSnowAllAcc(array('info' => $award['level'], 'grade' => $award['grade']));
                $return['AllAccAward'] = $allAccAward;
                return $return;
                break;
            case 1040:
                $payInfo = self::getPayInfo($award);
                $return['PayInfo'] = $payInfo;
                return $return;
                break;
            case 1045:
                $dayLogin = self::getDayLogin($award);
                $return['reward'] = $dayLogin;
                return $return;
                break;
            case 1047:
                //祈福活动
                $return['freeCount'] = intval($award['freeGold']);
                $return['recordCount'] = 20;
                $return['WishYuanBao'] = [intval($award['once_gold']), intval($award['tentimes_gold'])];
                $return['Integral'] = [intval($award['item_need_count']), intval($award['item_exchange'])];
                $prizeList = self::getWishPrizeLst(['grade' => $award['grade'], 'info' => $award['top']]);
                $exchange = self::getWishExchange($award['day']);
                $return['prizeList'] = $prizeList;
                $return['exchange'] = $exchange;
                return $return;
            case 1043:
                $return['reward'] = self::getShangShangQian($award);
                return $return;
                break;
            case 1051:
                $minGold = intval($award['buyCount']);
                $return['minGold'] = $minGold;
                $reward = self::getRankReward($award);
                $return['reward'] = $reward;
                return $return;
                break;
            default:
                # code...
                break;
        }
        return $row;
    }

    /**
     * 活动检查
     * @return [type] [description]
     */
    public function check($id)
    {
        $data = $this->getRow('*', array('id' => $id));
        //渠道组，服务器
        $platforms = array_filter(explode(',', $data['platforms']));
        $servers = array_filter(explode(',', $data['servers']));
        /*$time = time() - 86400 * 7;
        $where = "open_time < {$time}";*/
        $where = 1;
        $server = (new ServerModel)->getServerByServerAndChannelGroup($servers, $platforms, $where, false);
        $server = array_keys($server);
        if (empty($server)) {
            return 'empty';
        }
        $content = $this->formatData($data);
        $act_id = $data['act_id'];
        switch ($act_id) {
            case 1002: //单笔充值
                $rs = array();
                foreach ($content as $key => &$row) {
                    $row['start'] = $row['tm']['start'];
                    $row['close'] = $row['tm']['close'];
                }
                unset($row);
                return $this->postCheck($content, $server, true);
                break;
            case 1004:
            case 1008:
            case 1030: //砸蛋
            case 1009: //消费排行榜
            case 1003: //累计充值
            case 1006: // 幸运鉴宝
            case 1005: //云购
            case 1010: //掉落活动
            case 1020: //全民活动
            case 1007: //全民嗨
            case 1032: //集福活动
            case 1033:
            case 1034:
            case 1035:
            case 1036:
            case 1038:
            case 1039:
            case 1040:
            case 1041:
            case 1042:
            case 1045:
            case 1047:
            case 1043:
            case 1051:
            case 1065://每日充值
                return $this->postCheck($data, $server);
                break;
            default:
                # code...
                break;
        }
        //渠道组，服务器
    }


    /**
     * 发送到单服进行较验
     * @return [type] [description]
     */
    public function postCheck($data, $server, $mult = false)
    {
        if ($mult == false) {
            //并发调用请求协议，防止由于某个服连接不上，而阻塞。
            $name = isset(CDict::$activityType[$data['act_id']]) ? CDict::$activityType[$data['act_id']] : $data['act_id'];
            $vars = array(
                'document' => array(
                    'aId' => intval($data['act_id']),
                    'name' => $name,
                    '_id' => $data['uuid'],
                    'start' => $data['start_time'],
                    'close' => $data['end_time'])
            );
            $postData = array(
                'r' => 'call_method',
                'class' => 'ActivityModel',
                'method' => 'checkAct',
                'params' => json_encode($vars),
            );
        } else {
            //并发调用请求协议，防止由于某个服连接不上，而阻塞。
            $vars = array(
                'document' => $data,
            );
            $postData = array(
                'r' => 'call_method',
                'class' => 'ActivityModel',
                'method' => 'checkMultAct',
                'params' => json_encode($vars),
            );
        }
        $res = $this->getGm(array(), $server);
        $serverRes = array();
        foreach ($res as $row) {
            $serverRes[$row['server_id']] = $row;
        }
        //查找服务器的api Url
        $instance = new ServerconfigModel();
        $urls = $instance->getApiUrl($server, true);
        $rs = Helper::rolling_curl($urls, $postData);
        $vars = array();
        foreach ($rs as $sign => $json) {
            $server_name = isset($serverRes[$sign]) ? $serverRes[$sign]['name'] : $sign;
            $rows = json_decode($json, true);
            $str = '';
            if (isset($rows['errno'])) {
                $str .= $server_name . "错误信息：" . $rows['error'] . "\r\n";
            } else {

                if (is_array($rows)) {
                    foreach ($rows as $row) {
                        if (isset($row['name'])) {
                            $str .= $server_name . ':已存在相同类型 【' . $row['name'] . '】,其ID为 【' . $row['_id'] . '】' . "\r\n";
                        } else {
                            $str .= $server_name . $row;
                        }
                    }
                }
            }
            $vars[$sign] = $str;
        }
        if (!empty($vars)) {
            return array_filter($vars);
        }
        return array();

    }

    /**
     * 活动添加
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function push($id)
    {
        $data = $this->getRow('*', array('id' => $id));
        $content = $this->formatData($data);
        //渠道组，服务器
        $platforms = array_filter(explode(',', $data['platforms']));
        $servers = array_filter(explode(',', $data['servers']));
        /*$time = time() - 86400 * 7;
        $where = "open_time < {$time}";*/
        $where = 1;
        $server = (new ServerModel)->getServerByServerAndChannelGroup($servers, $platforms, $where, false);
        if (empty($server)) die(json_encode(['code' => 404, 'msg' => '未找到适合条件的服务器，请重新选择']));
        $server = array_keys($server);
        // $server = array(248);
        $res = $this->getGm(array(), $server);
        $serverRes = array();
        foreach ($res as $row) {
            $serverRes[$row['server_id']] = $row;
        }
        $act_id = $data['act_id'];
        switch ($act_id) {
            case 1002: //单笔充值
                $rs = $this->postPush($content, $server, true);
                $rs = $this->updateStatus($id, $rs, $serverRes);
                return $rs;
                break;
            case 1004:// 首充团购
            case 1008:
            case 1030://砸蛋
            case 1009://消费排行榜
            case 1003://累计充值
            case 1006://幸运鉴宝
            case 1005://云购
            case 1010://掉落
            case 1020://全民活动
            case 1007://全民嗨
            case 1032://翻牌
            case 1033://扭蛋
            case 1034://聚划算
            case 1035://大转盘
            case 1038://成长翻倍
            case 1039://幸运宝箱
            case 1036://圣诞许愿
            case 1037://摇摇乐
            case 1040://充值返利
            case 1041://消费有礼
            case 1042://堆雪人
            case 1045://每日登录
            case 1047://新年祈福
            case 1043://上上签
            case 1051://充值排行
            case 1065://每日充值
                $param['_id'] = $data['uuid'];
                $param['aId'] = intval($data['act_id']);
                $name = CDict::$activityType[$data['act_id']];
                $param['name'] = $name;
                $param['type'] = intval($data['activity_sign']);//存在默认值0
                $param['openSerDay'] = intval($data['open_ser_day']);//存在默认值7
                $param['tmType'] = 1;
                $param['tm']['start'] = $data['start_time'];
                $param['tm']['close'] = $data['end_time'];
                $param['state'] = $data['state'];
                $param['openLvl'] = intval($data['openLvl']);
                $param['srcId'] = 'srcId';
                $param['content'] = $content;
                $rs = $this->postPush($param, $server);
                $rs = $this->updateStatus($id, $rs, $serverRes);
                return $rs;
                break;
            default:
                break;
        }
    }

    /**
     * 要推送的信息
     * @param  [array] $param  [参数]
     * @param  [array] $server [服务器]
     * @return [type]         [插入的信息]
     */
    public function postPush($param, $server, $mult = false)
    {
        $msg = array();

        $param = Helper::mongoInt64($param);

        if ($mult == false) {
            //并发调用请求协议，防止由于某个服连接不上，而阻塞。
            $postData = array(
                'r' => 'call_method',
                'class' => 'ActivityModel',
                'method' => 'addAct',
                'params' => json_encode(array('document' => $param)),
            );
        } else {
            //并发调用请求协议，防止由于某个服连接不上，而阻塞。
            $postData = array(
                'r' => 'call_method',
                'class' => 'ActivityModel',
                'method' => 'addMultAct',
                'params' => json_encode(array('document' => $param)),
            );
        }

        //查找服务器的api Url
        $instance = new ServerconfigModel();

        $urls = $instance->getApiUrl($server, true);
        $rs = Helper::rolling_curl($urls, $postData);

        $msg = array();
        foreach ($rs as $sign => $code) {
            if ($code === "") {
                $status = 2;
            } elseif ($code == 0) {
                $status = 3;
            } elseif ($code == 1) {
                $status = 1;
            } else {
                $status = 4;
            }

            $msg[$sign] = $status;
        }

        return $msg;
    }

    /**
     * 更新状态
     * @return [type] [description]
     */
    public function updateStatus($id, $rs, $serverRes)
    {
        //       foreach ($rs as $sign => &$code) {
        //       	if ($code != 1) {
        //       		continue;
        //       	}

        //       	$serRow = isset($serverRes[$sign]) ? $serverRes[$sign] : '';

        //       	$server_name =  'S'. $serRow['num']. '-'. $serRow['name'];
        //       	$ip = $serRow['ip'];
        //       	$gm_port = $serRow['gm_port'];

        // 	$res = $this->rpcCall($ip , $gm_port , 'uactivity');

        // 	if ($res !== 0 ) {
        // 		$code = 5;
        // 	} else {
        // 		$code = 1;
        // 	}
        // }

        // unset($code);


        $url = Helper::v2();
        $urls = $postData = array();

        $res = array();

        foreach ($rs as $sign => $code) {
            if ($code != 1) {
                continue;
            }

            $res[$sign] = $code;
        }

        $res = Helper::rpc($url, $res, 'uactivity');
        foreach ($res as $sign => $v) {
            if ($v == 1) {
                $rs[$sign] = 1;
            } else {
                $rs[$sign] = 5;
            }
        }

        $arr = array();
        $time = time();
        foreach ($rs as $sign => $status) {
            $arr[] = "({$id}, '{$sign}', $status, $time)";
        }


        $sql = "INSERT INTO ny_activity_result(`activity_id`, `server`, `status`, `create_time`) VALUES " . implode(',', $arr);

        $this->query($sql);

        //判断是否所有都成功

        $msg = array();

        $code = array(
            1 => '更新成功',
            2 => '配置不存在',
            3 => '写入mongo库失败',
            4 => '未知错误',
            5 => '更新失败',
        );

        foreach ($rs as $sign => $v) {
            $vs = isset($code[$v]) ? $code[$v] : $v;
            $serRow = isset($serverRes[$sign]) ? $serverRes[$sign] : '';
            $server_name = 'S' . $serRow['num'] . '-' . $serRow['name'];

            $msg[$sign] = $v . '/' . $server_name . ':' . $vs;
        }

        $rs = array_values(array_unique($rs));

        if (count($rs) == 1 && $rs[0] == 1) {
            $status = 1;
        } else {
            $status = 2;
        }

        $remark = json_encode($msg);
        //print($remark);exit;

        $update = array(
            'type' => 2,
            'check_user' => $_SESSION['username'],
            'check_time' => time(),
            'status' => $status,
            'remark' => $remark,
        );

        $this->update($update, array('id' => $id));

        return $msg;
    }

    /**
     * 获取信息码
     * @param  [string] $rs [description]
     * @return [return]     [description]
     */
    public function getMessage($rs)
    {


        foreach ($rs as &$v) {
            $v = isset($code[$v]) ? $code[$v] : $v;
        }

        unset($v);

        return $rs;
    }

    /**
     * [删除活动]
     * @param  [string] $uuid [uuid]
     * @return [type]       [description]
     */
    public function offSale($id)
    {
        $data = $this->getRow('*', array('id' => $id));

        if (empty($data)) {
            return array(array('查询不到相关信息'));
        }

        //渠道组，服务器
        $platforms = array_filter(explode(',', $data['platforms']));

        $servers = array_filter(explode(',', $data['servers']));

        /*$time = time() - 86400 * 7;
        $where = "open_time < {$time}";*/
        $where = 1;

        $server = (new ServerModel)->getServerByServerAndChannelGroup($servers, $platforms, $where, false);

        $server = array_keys($server);

        $res = $this->getGm(array(), $server);

        $serverRes = array();
        foreach ($res as $row) {
            $serverRes[$row['server_id']] = $row;
        }

        $msg = array();

        $act_id = $data['act_id'];
        switch ($act_id) {
            case 1002: //单笔充值

                $award = json_decode($data['award'], true);

                $grade = $award;

                $uid = substr($data['uuid'], 0, strlen($data['uuid']) - 1);

                for ($i = 0; $i < count($grade); $i++) {
                    $uuid[] = $uid . $i;
                }
                break;
            default:
                $uuid = $data['uuid'];
                break;
        }

        $param = array(
            '_id' => $uuid,
        );

        //查找服务器的api Url
        $instance = new ServerconfigModel();

        $urls = $instance->getApiUrl($server, true);

        //并发调用请求协议，防止由于某个服连接不上，而阻塞。
        $postData = array(
            'r' => 'call_method',
            'class' => 'ActivityModel',
            'method' => 'resetAct',
            'params' => json_encode(array('document' => $param)),
        );


        $rs = Helper::rolling_curl($urls, $postData);


        $msg = array();

        //       foreach ($rs as $sign => &$code) {
        //       	if ($code != 1) {
        //       		$code = 3;
        //       	}

        //       	$serRow = isset($serverRes[$sign]) ? $serverRes[$sign] : '';

        //       	$server_name = 'S'. $serRow['num']. '-'. $serRow['name'];
        //       	$ip = $serRow['ip'];
        //       	$gm_port = $serRow['gm_port'];

        // 	$res = $this->rpcCall($ip , $gm_port , 'uactivity');

        // 	if ($res !== 0 ) {
        // 		$code = 5;
        // 	}

        // 	$code = 1;
        // }

        // unset($code);

        $url = Helper::v2();

        $urls = $postData = array();

        $res = array();
        foreach ($rs as $sign => $code) {
            if ($code != 1) {
                continue;
            }

            $res[$sign] = $code;
        }

        $res = Helper::rpc($url, $res, 'uactivity');

        foreach ($res as $sign => $v) {
            if ($v == 1) {
                $rs[$sign] = 1;
            } else {
                $rs[$sign] = 5;
            }
        }


        $arr = array();
        $time = time();

        foreach ($rs as $sign => $status) {
            $arr[] = "({$id}, '{$sign}', $status, $time)";
        }

        $sql = "INSERT INTO ny_activity_result(`activity_id`, `server`, `status`, `create_time`) VALUES " . implode(',', $arr);

        $this->query($sql);

        //判断是否所有都成功

        $msg = array();

        $code = array(
            1 => '下架成功',
            2 => '配置不存在',
            3 => '删除mongo库失败',
            4 => '未知错误',
            5 => '更新失败',
        );

        foreach ($rs as $sign => $v) {
            $vs = isset($code[$v]) ? $code[$v] : $v;
            $serRow = isset($serverRes[$sign]) ? $serverRes[$sign] : '';
            $server_name = 'S' . $serRow['num'] . '-' . $serRow['name'];


            $msg[$sign] = $v . '/' . $server_name . ':' . $vs;
        }

        $rs = array_values(array_unique($rs));

        if (count($rs) == 1 && $rs[0] = 1) {
            $status = 1;
        } else {
            $status = 2;
        }

        $remark = json_encode($msg);

        $update = array(
            'type' => 4,
            'status' => $status,
            'remark' => $remark,
        );

        $this->update($update, array('id' => $id));

        return $msg;
    }


    public function activity_result_data($conditions)
    {
        parent::__construct('activity_result');
        $rs = array();
        $num = 0;
        $this->joinTable = array(
            'ac' => array('name' => 'activity', 'type' => 'left', 'on' => 'ac.id = a.activity_id'),
            's' => array('name' => 'server', 'type' => 'left', 'on' => 'a.server = s.server_id')
        );

        $fields = array('a.activity_id', 'ac.title', 's.num', 's.name', 'a.status', 'a.create_time');

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($rs) {
            $num = $this->getCount();
            foreach ($rs as $k => $v) {
                switch ($v['status']) {
                    case 1:
                        $rs[$k]['status'] = '<button class="btn btn-success">更新成功</button>';
                        break;
                    case 2:
                        $rs[$k]['status'] = '<button class="btn btn-info">配置不存在</button>';
                        break;
                    case 3:
                        $rs[$k]['status'] = '<button class="btn btn-danger">写入mongo库失败</button>';
                        break;
                    case 4:
                        $rs[$k]['status'] = '<button class="btn btn-warning">未知错误</button>';
                        break;
                    case 5:
                        $rs[$k]['status'] = '<button class="btn btn-default">更新失败</button>';
                        break;
                }
                $rs[$k]['name'] = 'S' . $v['num'] . '-' . $v['name'];
                unset($rs[$k]['num']);
                $rs[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            foreach ($rs as $k => $v) {
                $rs[$k] = array_values($v);
            }
        }
        $this->setPageData($rs, $num);

    }

    /**
     * 切割物品ID
     * @param  [type] $goods_id [description]
     * @return [type]           [description]
     */
    public function explodeGoodsId($goods_id)
    {
        $goods_id = explode('/', $goods_id);
        return $goods_id = $goods_id[0];
    }


    /**
     * 活动模版
     * @param  [type] $conditions [description]
     * @return [type]             [description]
     */
    public function model_data($conditions)
    {
        $rs = array();
        $num = 0;
        $fields = array('id', 'title', 'start_time', 'end_time', 'act_id', 'platforms', 'add_user');

        $conditions['WHERE']['model'] = 1;//只读模版
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($rs) {
            $num = $this->getCount();
            foreach ($rs as $k => $v) {

                $rs[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
                $rs[$k]['end_time'] = date('Y-m-d H:i:s', $v['end_time']);

                $rs[$k]['act_id'] = CDict::$activityType[$v['act_id']];

                if (!empty($v['platforms'])) {
                    $sql = "select name from  ny_channel_group where id in (" . $v['platforms'] . ")";
                    $group = $this->query($sql);
                    $group = array_column($group, 'name');
                    $group = implode(',', $group);
                    $rs[$k]['platforms'] = $group;
                } else {
                    $rs[$k]['platforms'] = '全部渠道组';
                }

                $str = '<button style="margin-bottom:5px;" class="gbutton" onclick="_copy(\'' . $v['id'] . '\')">复制</button> |';

                $str .= '<button style="margin-bottom:5px;" class="gbutton" onclick="_edit(\'' . $v['id'] . '\')">编辑</button> | ';

                $str .= '<button style="margin-bottom:5px;" class="gbutton" onclick="_del(\'' . $v['id'] . '\')">删除</button> ';

                $rs[$k]['caozuo'] = $str;
            }
            foreach ($rs as $k => $v) {
                $rs[$k] = array_values($v);
            }
        }
        $this->setPageData($rs, $num);
    }

    /**
     * 集福奖励，解析数据包
     * @param array $data
     * @return array $output
     */

    private static function getTopPack($data)
    {
        //校验活动类型，后期优化
        if (!isset($data) || empty($data) || !is_array($data)) {
            return array();//返回空
        }
        //闭包实现递归
        $temp = array();

        $clud = function (&$datas, &$temp) use (&$clud) {
            static $indexs = 1;
            if (!is_array($datas)) {
                exit();
            }
            $rollValue = array_shift($datas);//获取第一轮

            $res = [];
            //获取每一福
            foreach ($rollValue as $key => $value) {
                $career1 = $career2 = $career3 = array();
                $itemMsg = [];
                //每一组道具奖励
                foreach ($value as $val) {
                    if (empty($val['itemId'])) {
                        //道具为空，客户端做处理
                        continue;
                    }
                    $good_id = explode('/', $val['itemId']);
                    $good_id = $good_id[0];
                    switch ($val['career_name']) {
                        case 0:
                            $career1[] = array('itemId' => $good_id, 'count' => intval($val['item_count']));
                            $career2[] = array('itemId' => $good_id, 'count' => intval($val['item_count']));
                            $career3[] = array('itemId' => $good_id, 'count' => intval($val['item_count']));
                            break;
                        case 1:
                            $career1[] = array('itemId' => $good_id, 'count' => intval($val['item_count']));
                            break;
                        case 2:
                            $career2[] = array('itemId' => $good_id, 'count' => intval($val['item_count']));
                            break;
                        default:
                            #code
                            break;
                    }

                }
                array_push($itemMsg, array('worldLevel' => 1, 'itemList' => array($career1, $career2, $career3)));
                array_push($res, array('count' => intval($key), 'reward' => $itemMsg));
            }
            if (is_array($rollValue)) {
                array_push($temp, array('index' => intval($indexs), 'reward' => $res));
                $indexs++;
                $clud($datas, $temp);//递归
            } else {
                return array();
            }
            unset($index);
            return $temp;
        };
        $output = $clud($data, $temp);
        return $output;
    }

    /**
     * 获取普通奖励包
     * @param array $data
     * @return array $output
     */
    private static function getLstPack($data)
    {

        //校验活动类型，后期优化
        $output = array();
        if (!isset($data) || empty($data) || !is_array($data)) {
            return $output;//返回空
        }
        //获取每一组普通奖励
        $career1 = $career2 = $career3 = [];
        foreach ($data as $key => $val) {
            //获取每一项 如果itemId为空，则丢弃该栏
            foreach ($val as $value) {
                if (empty($value['itemId'])) {
                    continue;
                }
                $good_id = explode('/', $value['itemId']);
                $good_id = $good_id[0];

                switch ($value['career_name']) {
                    case 0:
                        $career1[] = array('itemId' => $good_id, 'count' => intval($value['count']), 'weight' => intval($value['weight']));
                        $career2[] = array('itemId' => $good_id, 'count' => intval($value['count']), 'weight' => intval($value['weight']));
                        $career3[] = array('itemId' => $good_id, 'count' => intval($value['count']), 'weight' => intval($value['weight']));
                        break;
                    case 1:
                        $career1[] = array('itemId' => $good_id, 'count' => intval($value['count']), 'weight' => intval($value['weight']));
                        break;
                    case 2:
                        $career2[] = array('itemId' => $good_id, 'count' => intval($value['count']), 'weight' => intval($value['weight']));
                        break;
                    default:
                        #code
                        break;
                }
            }
        }
        $itemMsg = array($career1, $career2, $career3);
        array_push($output, array('worldLevel' => 1, 'itemList' => $itemMsg));
        return $output;
    }

    /**
     * [1033幸运扭蛋]
     * 获取reward 兼容后期版本
     * @param string $reward
     * @return array $outReward
     */
    private static function getReward($reward)
    {
        #code for get reward
        $rewardArr = $reward;
        $outReward = array();
        $grade = $rewardArr['grade'];
        $config = $rewardArr['config'];
        //配置奖励
        $cItemId = $config['item_id'];
        $cItemCount = $config['item_count'];
        $cCareer = $config['career'];
        $cWeight = $config['weight'];

        foreach ($grade as $k => $level) {
            //config  判断是否填完所有
            if (empty($level)) {
                continue;
            }
            if (!isset($cItemId[$k])) {
                continue;
            }
            //配置奖励
            $career1 = $career2 = $career3 = array();
            foreach ($cItemId[$k] as $k2 => $goods_id) {
                if (empty($goods_id)) {
                    continue;
                }
                $goods_id = explode('/', $goods_id);
                $goods_id = $goods_id[0];
                $goods_num = intval($cItemCount[$k][$k2]);
                $car = $cCareer[$k][$k2];
                $weight = intval($cWeight[$k][$k2]);
                switch ($car) {
                    case 0: //全部
                        $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight,);
                        $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight,);
                        $career3[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight,);
                        break;
                    case 1: //刀
                        $career1[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight,);
                        break;
                    case 2://弓
                        $career2[] = array('itemId' => $goods_id, 'count' => $goods_num, 'weight' => $weight,);
                        break;
                    default:
                        # code...
                        break;
                }
            }
            $itemList = array($career1, $career2, $career3);
            $outReward[] = array(
                'worldLevel' => intval($level),
                'itemList' => $itemList,
            );
        }
        return $outReward;
    }

    /**
     * [1033 幸运扭蛋]
     * 获取accReward 兼容后期版本
     * @param array $reward
     * @return array $outAccReward
     */
    private static function getAccReward($reward)
    {
        #code for get accReward
        $rewardArr = $reward;
        $accReward = array();
        $amount = $rewardArr['amount'];
        $grade = $rewardArr['grade'];
        $item_id = $rewardArr['item_id'];
        $item_count = $rewardArr['item_count'];
        $career = $rewardArr['career'];
        $vip_level = $rewardArr['vip_level'];
        #对amount 进行数据转换[amount/vip_level]={worldLevel}

        $awl = array();
        foreach ($amount as $worldLevelKey => $val) {
            foreach ($val as $key2 => $num) {
                $awl[$num . "/" . $vip_level[$worldLevelKey][$key2]][] = $worldLevelKey;
            }
        }
        foreach ($awl as $key => $val) {
            $keyArr = explode('/', $key);
            $totalCount = intval($keyArr[0]);
            $vip = intval($keyArr[1]);
            $data = [];
            $data['cond'] = array('totalCount' => intval($totalCount), 'personalCount' => 1, 'vip' => $vip);
            $reward = [];
            foreach ($val as $levelKey) {
                $mont = $amount[$levelKey];
                $key2 = array_search((string)$totalCount, $mont);
                if (!isset($key2)) {
                    continue;
                }
                $cItemId = $item_id[$levelKey][$key2];
                if (!isset($cItemId)) {
                    continue;
                }
                $career1 = $career2 = $career3 = [];
                foreach ($cItemId as $key3 => $goodIds) {
                    if (empty($goodIds)) continue;
                    $good_id = explode('/', $goodIds);
                    $good_id = $good_id[0];
                    $good_num = intval($item_count[$levelKey][$key2][$key3]);
                    $car = (int)$career[$levelKey][$key2][$key3];

                    switch ($car) {
                        case 0:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 1:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 2:
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 3:
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        default:
                            break;
                    }
                }
                $itemList = array($career1, $career2, $career3);
                $reward[] = array(
                    'worldLevel' => intval($grade[$levelKey]),
                    'itemList' => $itemList
                );
            }
            $data['reward'] = $reward;
            $accReward[] = $data;
        }
        return $accReward;
    }

    /**
     * 聚划算活动，普通礼包奖励276
     * @param array $award
     */
    private static function getPrizeList($award)
    {
        $config = $award['config'];
        $price_name = $config['price_name'];;
        $item_id = $config['item_id'];
        $item_count = $config['item_count'];
        $career = $config['career'];
        $price_old = $config['price_old'];
        $price_now = $config['price_now'];
        $pricePack = array();
        foreach ($item_id as $level => $itemMsg) {
            $temp = [];
            //校验原价
            $career1 = $career2 = $career3 = array();
            if (!isset($price_old[$level])) continue;
            //校验现价
            if (!isset($price_now[$level])) continue;
            foreach ($itemMsg as $key => $itemId) {
                if (empty($itemId)) continue;
                $count = $item_count[$level][$key];
                $car = $career[$level][$key];
                $good_id = explode('/', $itemId);
                $good_id = $good_id[0];

                if (!isset($count) || !isset($car)) continue;

                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $good_id, 'count' => intval($count));
                        $career2[] = array('itemId' => $good_id, 'count' => intval($count));
                        $career3[] = array('itemId' => $good_id, 'count' => intval($count));
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $good_id, 'count' => intval($count));
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $good_id, 'count' => intval($count));
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $good_id, 'count' => intval($count));
                        break;
                    default:
                        break;
                }
            }//end foreach
            $itemList = array($career1, $career2, $career3);
            $temp['priceOld'] = intval($price_old[$level]);
            $temp['priceNew'] = intval($price_now[$level]);
            $temp['limitCount'] = 0;
            $temp['giftPackId'] = '6000040010003';
            $temp['name'] = $price_name[$level];
            $temp['itemList'] = $itemList;
            $pricePack[] = $temp;
        }
        return $pricePack;
    }

    /**
     * 聚划算活动，大礼包奖励
     * @param array $award
     */
    private static function getTopPrize($award)
    {
        $item_id = $award['item_id'];
        $item_count = $award['item_count'];
        $career = $award['career'];
        $big_price_name = $award['big_price_name'];
        $price_old = $award['price_old'];
        $price_now = $award['price_now'];

        $pricePack = $career1 = $career2 = $career3 = array();
        foreach ($item_id as $level => $itemId) {
            if (!isset($item_count[$level])) continue;
            if (!isset($career[$level])) continue;
            $goodId = explode('/', $itemId);
            $goodId = $goodId[0];
            $car = $career[$level];
            $count = intval($item_count[$level]);

            switch ($car) {
                case 0:
                    $career1[] = array('itemId' => $goodId, 'count' => $count);
                    $career2[] = array('itemId' => $goodId, 'count' => $count);
                    $career3[] = array('itemId' => $goodId, 'count' => $count);
                    break;
                case 1:
                    $career1[] = array('itemId' => $goodId, 'count' => $count);
                    break;
                case 2:
                    $career2[] = array('itemId' => $goodId, 'count' => $count);
                    break;
                case 3:
                    $career3[] = array('itemId' => $goodId, 'count' => $count);
                    break;
                default:
                    break;
            }
        }
        $pricePack['priceOld'] = intval($price_old);
        $pricePack['priceNew'] = intval($price_now);
        $pricePack['limitCount'] = 1;
        $pricePack['giftPackId'] = '6000040010003';
        $pricePack['name'] = $big_price_name;
        $pricePack['itemList'] = array($career1, $career2, $career3);
        return $pricePack;
    }

    /**
     * 大转盘活动
     * @param array $config
     * @return array $prizeList
     */
    public static function luckyGetPrizeList($config)
    {
        if (empty($config)) return array();
        $item_id = $config['item_id'];
        $item_count = $config['item_count'];
        $career = $config['career'];
        $weight = $config['weight'];
        $career1 = $career2 = $career3 = array();
        foreach ($item_id as $level => $itemId) {
            if (!isset($item_count[$level])) continue;
            if (!isset($career[$level])) continue;
            $goodId = explode('/', $itemId);
            $goodId = $goodId[0];
            $car = $career[$level];
            $count = intval($item_count[$level]);
            $weight_num = intval($weight[$level]);

            switch ($car) {
                case 0:
                    $career1[] = array('itemId' => $goodId, 'count' => $count, 'weight' => $weight_num);
                    $career2[] = array('itemId' => $goodId, 'count' => $count, 'weight' => $weight_num);
                    $career3[] = array('itemId' => $goodId, 'count' => $count, 'weight' => $weight_num);
                    break;
                case 1:
                    $career1[] = array('itemId' => $goodId, 'count' => $count, 'weight' => $weight_num);
                    break;
                case 2:
                    $career2[] = array('itemId' => $goodId, 'count' => $count, 'weight' => $weight_num);
                    break;
                case 3:
                    $career3[] = array('itemId' => $goodId, 'count' => $count, 'weight' => $weight_num);
                    break;
                default:
                    break;
            }
        }
        $pricePack = array($career1, $career2, $career3);
        return $pricePack;
    }

    /**
     * 幸运大转盘 积分奖励
     * @param $data
     */
    public static function luckyGetExchange($data)
    {
        if (empty($data)) return array();
        $item_id = $data['item_id'];
        $item_count = $data['item_count'];
        $career = $data['career'];
        $score = $data['score_num'];
        $career1 = $career2 = $career3 = array();
        foreach ($item_id as $level => $itemId) {
            if (!isset($item_count[$level])) continue;
            if (!isset($career[$level])) continue;
            $goodId = explode('/', $itemId);
            $goodId = $goodId[0];
            $car = $career[$level];
            $count = intval($item_count[$level]);
            $score2 = intval($score[$level]);
            switch ($car) {
                case 0:
                    $career1[] = array('itemId' => $goodId, 'count' => $count, 'score' => $score2);
                    $career2[] = array('itemId' => $goodId, 'count' => $count, 'score' => $score2);
                    $career3[] = array('itemId' => $goodId, 'count' => $count, 'score' => $score2);
                    break;
                case 1:
                    $career1[] = array('itemId' => $goodId, 'count' => $count, 'score' => $score2);
                    break;
                case 2:
                    $career2[] = array('itemId' => $goodId, 'count' => $count, 'score' => $score2);
                    break;
                case 3:
                    $career3[] = array('itemId' => $goodId, 'count' => $count, 'score' => $score2);
                    break;
                default:
                    break;
            }
        }
        $pricePack = array($career1, $career2, $career3);
        return $pricePack;
    }

    //圣诞许愿
    //@param array $data
    private static function getItemLst($data)
    {
        if (empty($data)) return array();
        $grade = $data['grade'];
        $item_id = $data['item_id'];
        $item_count = $data['count'];
        $career = $data['career'];
        $weight = $data['weight'];
        $value = $data['value'];
        $reward = [];
        foreach ($item_id as $level => $itemMsg) {
            $temp = $career1 = $career2 = $career3 = array();
            if (!isset($item_count[$level])) continue;
            if (!isset($career[$level])) continue;
            foreach ($itemMsg as $keys => $itemId) {
                $goodId = explode('/', $itemId);
                $goodId = $goodId[0];
                $car = $career[$level][$keys];
                $count = intval($item_count[$level][$keys]);
                $weight_num = intval($weight[$level][$keys]);
                $value_num = intval($value[$level][$keys]);

                if (!isset($count) || !isset($car) || !isset($weight_num) || !isset($value_num)) continue;

                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => intval($count), 'value' => $value_num, 'weight' => $weight_num);
                        $career2[] = array('itemId' => $goodId, 'count' => intval($count), 'value' => $value_num, 'weight' => $weight_num);
                        $career3[] = array('itemId' => $goodId, 'count' => intval($count), 'value' => $value_num, 'weight' => $weight_num);
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => intval($count), 'value' => $value_num, 'weight' => $weight_num);
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => intval($count), 'value' => $value_num, 'weight' => $weight_num);
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => intval($count), 'value' => $value_num, 'weight' => $weight_num);
                        break;
                    default:
                        break;
                }
            }
            $pricePack = array($career1, $career2, $career3);
            $worldLevel = intval($grade[$level]);
            $temp['worldLevel'] = $worldLevel;
            $temp['itemList'] = $pricePack;
            $reward[] = $temp;
        }
        return $reward;
    }
    /**-----摇摇乐----*/
    //大奖
    //普通奖励
    private static function getYYPrize($data)
    {
        if (in_array(null, $data) || in_array('', $data)) return array();
        $grade = $data['grade'];
        $info = $data['info'];
        //解析内容
        $item_id = $info['item_id'];
        $count = $info['count'];
        $career = $info['career'];
        $weight = $info['weight'];
        $reward = [];
        foreach ($item_id as $level => $itemInfo) {
            if (empty($grade[$level])) continue;
            if (!isset($count[$level])) continue;
            if (!isset($career[$level])) continue;
            $temp = $career1 = $career2 = $career3 = array();
            foreach ($itemInfo as $key => $item) {
                $goodId = explode('/', $item);
                $goodId = $goodId[0];
                $car = $career[$level][$key];
                $countNum = intval($count[$level][$key]);
                $weight_num = intval($weight[$level][$key]);

                if (!isset($count) || !isset($car) || !isset($weight_num)) continue;
                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => intval($countNum), 'weight' => $weight_num);
                        $career2[] = array('itemId' => $goodId, 'count' => intval($countNum), 'weight' => $weight_num);
                        $career3[] = array('itemId' => $goodId, 'count' => intval($countNum), 'weight' => $weight_num);
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => intval($countNum), 'weight' => $weight_num);
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => intval($countNum), 'weight' => $weight_num);
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => intval($countNum), 'weight' => $weight_num);
                        break;
                    default:
                        break;
                }
            }
            $pricePack = array($career1, $career2, $career3);
            $worldLevel = intval($grade[$level]);
            $temp['worldLevel'] = $worldLevel;
            $temp['itemList'] = $pricePack;
            $reward[] = $temp;
        }
        return $reward;
    }

    //累计奖励
    private static function getYYAcc($data)
    {
        #code for get accReward
        $rewardArr = $data['info'];
        $grade = $data['grade'];
        $accReward = array();
        $item_id = $rewardArr['item_id'];
        $item_count = $rewardArr['count'];
        $career = $rewardArr['career'];
        $vip_level = $rewardArr['vip_level'];
        #对amount 进行数据转换['personalCount']={worldLevel}

        $awl = array();
        foreach ($vip_level as $worldLevelKey => $val) {
            foreach ($val as $keyl => $num) {
                $awl[$num][] = $worldLevelKey;
            }
        }
        foreach ($awl as $key => $val) {
            $vip = intval($key);
            $datas = [];
            $reward = [];
            //levelKey 世界等级
            foreach ($val as $levelKey) {
                $mont = $vip_level[$levelKey];
                $key2 = array_search((string)$vip, $mont);
                if (!isset($key2)) {
                    continue;
                }
                $cItemId = $item_id[$levelKey][$key2];

                if (!isset($cItemId)) {
                    continue;
                }
                $career1 = $career2 = $career3 = [];
                foreach ($cItemId as $key3 => $goodIds) {
                    if (empty($goodIds)) continue;
                    $good_id = explode('/', $goodIds);
                    $good_id = $good_id[0];
                    $good_num = intval($item_count[$levelKey][$key2][$key3]);
                    if (empty($good_num)) continue;
                    $car = (int)$career[$levelKey][$key2][$key3];

                    switch ($car) {
                        case 0:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 1:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 2:
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 3:
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        default:
                            break;
                    }
                }
                $itemList = array($career1, $career2, $career3);
                $reward[] = array(
                    'worldLevel' => intval($grade[$levelKey]),
                    'itemList' => $itemList
                );
            }
            $datas['cond'] = array('personalCount' => $vip);
            $datas['reward'] = $reward;
            $accReward[] = $datas;
        }
        return $accReward;
    }

    /**-----------摇摇乐 end-------------------*/

    /**
     * 成长翻倍，构造数据
     * @param array $data
     */
    private static function growGetReward($data)
    {
        $grade = $data['grade'];//等级
        $config = $data['config'];
        $level = $config['level'];//等级
        $item_num = $config['item_num'];//双倍次数，5类型特例
        //循环得到每一组世界等级
        $rewardRes = [];
        foreach ($item_num as $lv => $info) {
            if (!isset($grade[$lv])) die(json_encode(['code' => 404, 'msg' => '世界等级未填写完整']));
            $worldLevel = intval($grade[$lv]);
            $typeConfig = [];
            foreach ($info as $type => $con) {
                if ($type == 5) {
                    #code for type == 5
                    $temp = [];
                    $ps1 = $con[1];
                    $levels = null;
                    foreach ($ps1 as $key5 => $val) {
                        $levels = $level[$lv][5][$key5];
                        if (empty($levels)) die(json_encode(['code' => 404, 'msg' => $grade[$lv] . ':等级填写不完整'], JSON_UNESCAPED_UNICODE));
                        $temp['level'] = intval($level);
                        $val2 = $con[2][$key5];
                        if (empty($val2) || empty($val)) die(json_encode(['code' => 404, 'msg' => $grade[$lv] . ':等级双倍次数填写不完整'], JSON_UNESCAPED_UNICODE));
                        $temp['count'] = array(intval($val), intval($con[2][$key5]));
                    }
                    $temp['level'] = intval($levels);
                    $typeConfig[5][] = $temp;
                    continue;
                }
                #1,2,3,4
                foreach ($con as $key => $num) {
                    $temp = [];
                    if (!isset($level[$lv][$type][$key])) die(json_encode(['code' => 404, 'msg' => $grade[$lv] . ':等级填写不完整'], JSON_UNESCAPED_UNICODE));
                    $levels = $level[$lv][$type][$key];//等到等级
                    $temp['level'] = intval($levels);
                    $temp['count'] = array(intval($num), 0);
                    $typeConfig[$type][] = $temp;
                }
            }
            $reward['worldLevel'] = intval($worldLevel);
            $reward['list'] = $typeConfig;
            $rewardRes[] = $reward;
        }
        return $rewardRes;
    }

    /**
     * 消费有礼
     * @param array $data
     */
    private static function getComsumeInfo($data)
    {
        if (empty($data)) return array();
        $comsume = $data['config'];
        $grade = $data['grade'];
        $itemId = $data['itemId'];
        $count = $data['count'];
        $career = $data['career'];

        $comsumeInfo = [];
        foreach ($itemId as $level => $itemInfo) {
            $woldLevel = $grade[$level];
            if (empty($woldLevel)) continue;
            //得到世界等级
            $reward = [];
            foreach ($itemInfo as $com => $itemMsg) {
                $comsume_num = $comsume[$level][$com];
                if (empty($comsume_num)) continue;
                $pricePack = $career1 = $career2 = $career3 = array();
                foreach ($itemMsg as $keys => $item) {
                    $goodId = explode('/', $item);
                    $goodId = $goodId[0];
                    $car = $career[$level][$com][$keys];
                    $count_num = intval($count[$level][$com][$keys]);
                    if (!isset($count) || !isset($car) || !isset($goodId)) continue;
                    switch ($car) {
                        case 0:
                            #for all career
                            $career1[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            $career2[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            $career3[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        case 1:
                            #for 刀
                            $career1[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        case 2:
                            #for 弓
                            $career2[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        case 3:
                            #for 法
                            $career3[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        default:
                            break;
                    }
                }
                $pricePack = array($career1, $career2, $career3);
                $cmp['Comsume'] = intval($comsume_num);
                $cmp['itemList'] = $pricePack;
                $reward[] = $cmp;
            }
            $temp['worldLevel'] = intval($woldLevel);
            $temp['reward'] = $reward;
            $comsumeInfo[] = $temp;
        }
        return $comsumeInfo;
    }
    /**------------堆雪人-----------------*/
    //假掉落（提交材料奖励）day
    private static function getItemPrizeList($data)
    {
        $rewardArr = $data['info'];
        $grade = $data['grade'];
        if (empty($rewardArr)) return array();

        $accReward = array();
        $item_id = $rewardArr['item_id'];
        $item_count = $rewardArr['count'];
        $career = $rewardArr['career'];
        $vip_level = $rewardArr['drop_id'];
        $weight = $rewardArr['weight'];
        #对amount 进行数据转换['personalCount']={worldLevel}

        $awl = array();
        foreach ($vip_level as $worldLevelKey => $val) {
            foreach ($val as $keyl => $num) {
                $awl[$num][] = $worldLevelKey;
            }
        }
        foreach ($awl as $key => $val) {
            $vip = $key;
            $datas = [];
            $reward = [];
            //levelKey 世界等级
            foreach ($val as $levelKey) {
                $mont = $vip_level[$levelKey];
                $key2 = array_search((string)$vip, $mont);
                if (!isset($key2)) {
                    continue;
                }
                $cItemId = $item_id[$levelKey][$key2];
                if (!isset($cItemId)) {
                    continue;
                }
                $career1 = $career2 = $career3 = [];
                foreach ($cItemId as $key3 => $goodIds) {
                    if (empty($goodIds)) continue;
                    $good_id = explode('/', $goodIds);
                    $good_id = $good_id[0];
                    $good_num = intval($item_count[$levelKey][$key2][$key3]);
                    if (empty($good_num)) continue;
                    $weight_num = intval($weight[$levelKey][$key2][$key3]);
                    $car = (int)$career[$levelKey][$key2][$key3];

                    switch ($car) {
                        case 0:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num, 'weight' => $weight_num);
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num, 'weight' => $weight_num);
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num, 'weight' => $weight_num);
                            break;
                        case 1:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num, 'weight' => $weight_num);
                            break;
                        case 2:
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num, 'weight' => $weight_num);
                            break;
                        case 3:
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num, 'weight' => $weight_num);
                            break;
                        default:
                            break;
                    }
                }
                $itemList = array($career1, $career2, $career3);
                $reward[] = array(
                    'worldLevel' => intval($grade[$levelKey]),
                    'itemList' => $itemList
                );
            }
            $itemId = explode('/', $vip);
            $itemId = $itemId[0];
            $datas['itemId'] = $itemId;
            $datas['prizeList'] = $reward;
            $accReward[] = $datas;
        }
        return $accReward;
    }

    //花费元宝 gold
    private static function getGoldrizeList($data)
    {
        $info = $data['info'];
        $grade = $data['grade'];

        if (empty($info)) return array();
        $itemId = $info['item_id'];
        $career = $info['career'];
        $count = $info['count'];
        $weight = $info['weight'];
        $comsumeInfo = [];
        foreach ($itemId as $level => $itemInfo) {
            if (empty($itemInfo)) continue;
            if (empty($grade[$level])) continue;

            $pricePack = $career1 = $career2 = $career3 = array();
            foreach ($itemInfo as $keys => $item) {
                $goodId = explode('/', $item);
                $goodId = $goodId[0];
                $car = $career[$level][$keys];
                $count_num = intval($count[$level][$keys]);
                $weight_num = intval($weight[$level][$keys]);
                if (!isset($count) || !isset($car) || !isset($goodId)) continue;
                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => intval($count_num), 'weight' => $weight_num);
                        $career2[] = array('itemId' => $goodId, 'count' => intval($count_num), 'weight' => $weight_num);
                        $career3[] = array('itemId' => $goodId, 'count' => intval($count_num), 'weight' => $weight_num);
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => intval($count_num), 'weight' => $weight_num);
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => intval($count_num), 'weight' => $weight_num);
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => intval($count_num), 'weight' => $weight_num);
                        break;
                    default:
                        break;
                }
            }
            $pricePack['worldLevel'] = intval($grade[$level]);
            $pricePack['itemList'] = array($career1, $career2, $career3);
            $comsumeInfo[] = $pricePack;
        }
        return $comsumeInfo;
    }

    //奖励 config
    private static function getAwardItemList($data)
    {
        if (empty($data)) return array();
        $item_id = $data['item_id'];
        $career = $data['career'];
        $itemAward = [];
        $career1 = $career2 = $career3 = [];
        foreach ($item_id as $keys => $itemInfo) {
            if (empty($itemInfo)) continue;
            $car = $career[$keys];
            $goodId = explode('/', $itemInfo);
            $goodId = $goodId[0];
            if (!isset($car)) continue;
            switch ($car) {
                case 0:
                    #for all career
                    $career1[] = array('itemId' => $goodId);
                    $career2[] = array('itemId' => $goodId);
                    $career3[] = array('itemId' => $goodId);
                    break;
                case 1:
                    #for 刀
                    $career1[] = array('itemId' => $goodId);
                    break;
                case 2:
                    #for 弓
                    $career2[] = array('itemId' => $goodId);
                    break;
                case 3:
                    #for 法
                    $career3[] = array('itemId' => $goodId);
                    break;
                default:
                    break;
            }
        }
        $itemAward = array($career1, $career2, $career3);
        return $itemAward;
    }

    //个人累计奖励 top
    private static function getSnowAcc($data)
    {
        $rewardArr = $data['info'];
        $grade = $data['grade'];
        if (empty($rewardArr)) return array();

        $accReward = array();
        $item_id = $rewardArr['item_id'];
        $item_count = $rewardArr['count'];
        $career = $rewardArr['career'];
        $vip_level = $rewardArr['per'];
        #对amount 进行数据转换['personalCount']={worldLevel}

        $awl = array();
        foreach ($vip_level as $worldLevelKey => $val) {
            foreach ($val as $keyl => $num) {
                $awl[$num][] = $worldLevelKey;
            }
        }
        foreach ($awl as $key => $val) {
            $vip = intval($key);
            $datas = [];
            $reward = [];
            //levelKey 世界等级
            foreach ($val as $levelKey) {
                $mont = $vip_level[$levelKey];
                $key2 = array_search((string)$vip, $mont);
                if (!isset($key2)) {
                    continue;
                }
                $cItemId = $item_id[$levelKey][$key2];

                if (!isset($cItemId)) {
                    continue;
                }
                $career1 = $career2 = $career3 = [];
                foreach ($cItemId as $key3 => $goodIds) {
                    if (empty($goodIds)) continue;
                    $good_id = explode('/', $goodIds);
                    $good_id = $good_id[0];
                    $good_num = intval($item_count[$levelKey][$key2][$key3]);
                    if (empty($good_num)) continue;
                    $car = (int)$career[$levelKey][$key2][$key3];

                    switch ($car) {
                        case 0:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 1:
                            $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 2:
                            $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        case 3:
                            $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                            break;
                        default:
                            break;
                    }
                }
                $itemList = array($career1, $career2, $career3);
                $reward[] = array(
                    'worldLevel' => intval($grade[$levelKey]),
                    'itemList' => $itemList
                );
            }
            $datas['cond'] = array('personalCount' => $vip);
            $datas['reward'] = $reward;
            $accReward[] = $datas;
        }
        return $accReward;
    }

    //获取全服累计奖励 level
    private static function getSnowAllAcc($data)
    {
        $info = $data['info'];

        $grade = $data['grade'];
        if (empty($info)) return array();

        $allAward = [];
        foreach ($info as $type => $typeInfo) {
            switch ($type) {
                case '0':
                    //道具
                    $item_id = $typeInfo['item_id'];
                    $item_count = $typeInfo['count'];
                    $career = $typeInfo['career'];
                    $vip_level = $typeInfo['num'];
                    #对amount 进行数据转换['personalCount']={worldLevel}
                    $awl = array();
                    foreach ($vip_level as $worldLevelKey => $val) {
                        foreach ($val as $keyl => $num) {
                            $awl[$num][] = $worldLevelKey;
                        }
                    }
                    foreach ($awl as $key => $val) {
                        $vip = intval($key);
                        $datas = [];
                        $reward = [];
                        //levelKey 世界等级
                        foreach ($val as $levelKey) {
                            $mont = $vip_level[$levelKey];
                            $key2 = array_search((string)$vip, $mont);
                            if (!isset($key2)) {
                                continue;
                            }
                            $cItemId = $item_id[$levelKey][$key2];

                            if (!isset($cItemId)) {
                                continue;
                            }
                            $career1 = $career2 = $career3 = [];
                            foreach ($cItemId as $key3 => $goodIds) {
                                if (empty($goodIds)) continue;
                                $good_id = explode('/', $goodIds);
                                $good_id = $good_id[0];
                                $good_num = intval($item_count[$levelKey][$key2][$key3]);
                                if (empty($good_num)) continue;
                                $car = (int)$career[$levelKey][$key2][$key3];

                                switch ($car) {
                                    case 0:
                                        $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                                        $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                                        $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                                        break;
                                    case 1:
                                        $career1[] = array('itemId' => $good_id, 'count' => $good_num);
                                        break;
                                    case 2:
                                        $career2[] = array('itemId' => $good_id, 'count' => $good_num);
                                        break;
                                    case 3:
                                        $career3[] = array('itemId' => $good_id, 'count' => $good_num);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            $itemList = array($career1, $career2, $career3);
                            $reward[] = array(
                                'worldLevel' => intval($grade[$levelKey]),
                                'itemList' => $itemList
                            );
                        }
                        $datas['cond'] = array('totalCount' => $vip);
                        $datas['type'] = 0;
                        $datas['reward'] = $reward;
                        $allAward[] = $datas;
                    }
                    break;
                case '1':
                    $num = $typeInfo['num'];
                    $time = $typeInfo['time'];
                    $per = $typeInfo['per'];
                    $temp = [];
                    foreach ($num as $key => $per_num) {
                        if (empty($per_num)) continue;
                        if (empty($time[$key])) continue;
                        if (empty($per[$key])) continue;
                        $temp['cond'] = array('totalCount' => intval($per_num));
                        $temp['type'] = 1;
                        $temp['time'] = intval($time[$key]);
                        $temp['per'] = intval($per[$key]);
                        $allAward[] = $temp;
                    }
                    break;
                case '2':
                    $num = $typeInfo['num'];
                    $time = $typeInfo['time'];
                    $per = $typeInfo['per'];
                    $secne = $typeInfo['action'];
                    $temp = [];
                    foreach ($num as $key => $per_num) {
                        if (empty($per_num)) continue;
                        if (empty($time[$key])) continue;
                        if (empty($per[$key])) continue;
                        $temp['cond'] = array('totalCount' => intval($per_num));
                        $temp['type'] = 2;
                        $temp['time'] = intval($time[$key]);
                        $temp['per'] = intval($per[$key]);
                        foreach ($secne[$key] as &$msg) {
                            $msg = intval($msg);
                        }
                        $temp['secneType'] = array_filter($secne[$key]);
                        $allAward[] = $temp;
                    }
                    break;
                case '3':
                    $num = $typeInfo['num'];
                    $secne = $typeInfo['action'];
                    $temp = [];
                    foreach ($num as $key => $per_num) {
                        if (empty($per_num)) continue;
                        if (empty($secne[$key])) continue;
                        $temp['cond'] = array('totalCount' => intval($per_num));
                        $temp['type'] = 3;
                        foreach ($secne[$key] as &$msg) {
                            $msg = intval($msg);
                        }
                        $temp['bossType'] = array_filter($secne[$key]);
                        $allAward[] = $temp;
                    }
                    break;
                default:
                    break;
            }
        }
        return $allAward;
    }


    /**----------------------------------*/

    //幸运大宝箱
    private static function getBoxContent($data)
    {
        if (empty($data)) return array('Pay' => [], 'Comsume' => [], 'Yuanbao' => [], 'reward' => []);
        $grade = $data['grade'];
        $itemId = $data['itemId'];
        $item_id = $data['item_id'];
        $item_count = $data['item_count'];
        $career = $data['career'];
        $comsume = $data['config'];
        $weight = $data['weight'];
        $count = $data['count'];
        $mapRange = $data['mapRange'];
        $content['Pay'] = $content['Comsume'] = $content['YuanBao'] = $content['reward'] = [];
        foreach ($item_id as $level => $itemInfo) {
            //累计充值目标
            if (empty($grade[$level])) continue;
            if (empty($itemInfo)) continue;
            $temp = $cm = [];
            $worldLevel = intval($grade[$level]);
            $goodPay = !empty($itemInfo[1]) ? explode('/', $itemInfo[1]) : null;
            $temp['Add'] = intval($comsume[$level][1]);
            $temp['itemList'] = array('itemId' => $goodPay[0], 'count' => intval($item_count[$level][1]));
            $cm['worldLevel'] = $worldLevel;
            $cm['List'][] = $temp;
            $content['Pay'][] = $cm;
            //累计消费目标
            $temp = $cm = [];
            $worldLevel = intval($grade[$level]);
            $goodPay = !empty($itemInfo[2]) ? explode('/', $itemInfo[2]) : null;
            $temp['Add'] = intval($comsume[$level][2]);
            $temp['itemList'] = array('itemId' => $goodPay[0], 'count' => intval($item_count[$level][2]));
            $cm['worldLevel'] = $worldLevel;
            $cm['List'][] = $temp;
            $content['Comsume'][] = $cm;
            //元宝
            if (empty($mapRange[$level])) continue;
            $rangeList = [];
            foreach ($mapRange[$level][1] as $key => $range) {
                $range1 = intval($range);
                $range2 = isset($mapRange[$level][2][$key]) ? intval($mapRange[$level][2][$key]) : 0;
                $rm['Range'] = array($range1, $range2);
                $rm['weight'] = isset($weight[$level][$key]) ? intval($weight[$level][$key]) : 1;
                $rangeList[] = $rm;
            }
            $li['worldLevel'] = $worldLevel;
            $li['List'] = $rangeList;
            $content['YuanBao'][] = $li;
            //开启最多宝箱
            if (empty($itemId[$level])) continue;
            $career1 = $career2 = $career3 = array();
            foreach ($itemId[$level] as $key2 => $item) {
                $goodId = explode('/', $item);
                $goodId = $goodId[0];
                $car = $career[$level][$key2];
                $count_num = $count[$level][$key2];
                if (!isset($count) || !isset($car) || !isset($goodId)) continue;
                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => intval($count_num));
                        $career2[] = array('itemId' => $goodId, 'count' => intval($count_num));
                        $career3[] = array('itemId' => $goodId, 'count' => intval($count_num));
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => intval($count_num));
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => intval($count_num));
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => intval($count_num));
                        break;
                    default:
                        break;
                }
            }
            $pricePack = array($career1, $career2, $career3);
            $cmp['worldLevel'] = $worldLevel;
            $cmp['itemList'] = $pricePack;
            $content['reward'][] = $cmp;
        }
        return $content;
    }

    //充值返利获取奖励配置
    private static function getPayInfo($data)
    {

        if (empty($data)) return $data;
        $grade = $data['grade'];
        $count = $data['itemId'];
        $pay = $data['buyCount'];
        $rebate = $data['random'];
        $percent = $data['count'];
        $config = $data['config'];
        $payInfo = array();
        foreach ($count as $level => $info) {
            if (empty($info)) continue;
            $list = array();
            foreach ($info as $key => $countMsg) {
                $Return = isset($config[$level][$key]) ? $config[$level][$key] : 0;
                $payNum = isset($pay[$level][$key]) ? $pay[$level][$key] : 0;
                $rebate1 = isset($rebate[$level][1][$key]) ? $rebate[$level][1][$key] : 0;
                $rebate2 = isset($rebate[$level][2][$key]) ? $rebate[$level][2][$key] : 0;
                $percentNum = json_decode($percent[$level][$key]);
                if (empty($percent)) continue;
                $temp['Count'] = intval($countMsg);
                $temp['Pay'] = intval($payNum);
                $temp['Return'] = intval($Return);
                $temp['Rebate'] = array(intval($rebate1), intval($rebate2));
                $temp['Percent'] = $percentNum;
                $list[] = $temp;
            }
            $worldLevel = $grade[$level];
            if (empty($worldLevel)) continue;
            $tm['worldLevel'] = intval($worldLevel);
            $tm['List'] = $list;
            $payInfo[] = $tm;
        }

        return $payInfo;

    }

    //获取每日登录道具奖励
    private static function getDayLogin($data)
    {
        if (empty($data)) return array();
        $grade = $data['grade'];
        $itemId = $data['itemId'];
        $count = $data['count'];
        $career = $data['career'];

        $comsumeInfo = [];
        foreach ($itemId as $level => $itemInfo) {
            $woldLevel = $grade[$level];
            if (empty($woldLevel)) continue;
            //得到世界等级
            $reward = [];
            $id = 0;
            foreach ($itemInfo as $com => $itemMsg) {
                if (empty($itemMsg)) continue;
                $id++;
                $career1 = $career2 = $career3 = array();
                foreach ($itemMsg as $keys => $item) {
                    $goodId = explode('/', $item);
                    $goodId = $goodId[0];
                    $car = $career[$level][$com][$keys];
                    $count_num = intval($count[$level][$com][$keys]);
                    if (!isset($count) || !isset($car) || !isset($goodId)) continue;
                    switch ($car) {
                        case 0:
                            #for all career
                            $career1[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            $career2[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            $career3[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        case 1:
                            #for 刀
                            $career1[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        case 2:
                            #for 弓
                            $career2[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        case 3:
                            #for 法
                            $career3[] = array('itemId' => $goodId, 'count' => intval($count_num));
                            break;
                        default:
                            break;
                    }
                }
                $pricePack = array($career1, $career2, $career3);
                $cmp['id'] = $id;
                $cmp['itemList'] = $pricePack;
                $reward[] = $cmp;
            }
            $temp['worldLevel'] = intval($woldLevel);
            $temp['list'] = $reward;
            $comsumeInfo[] = $temp;
        }
        return $comsumeInfo;
    }

    /**------祈福活动-----*/
    private static function getWishPrizeLst($data)
    {
        $grade = $data['grade'];
        $info = $data['info'];//top
        if (empty($info)) return array();

        $itemId = self::arrayMergeItem(array($info['item_id2'], $info['item_id']));
        $count = self::arrayMergeItem(array($info['count2'], $info['count']));
        $career = self::arrayMergeItem(array($info['career2'], $info['career']));
        $weight = self::arrayMergeItem(array($info['weight2'], $info['weight']));
        $value = self::arrayMergeItem(array($info['value2'], $info['value']));
        //合并道具内容
        $comsumeInfo = [];
        foreach ($itemId as $level => $itemInfo) {
            $woldLevel = $grade[$level];
            if (empty($woldLevel)) continue;
            //得到世界等级
            $career1 = $career2 = $career3 = array();
            foreach ($itemInfo as $key => $item) {
                if (empty($item)) continue;
                $goodId = explode('/', $item);
                $goodId = $goodId[0];
                $car = $career[$level][$key];
                $wei = intval($weight[$level][$key]);
                $val = intval($value[$level][$key]);//特效
                $count_num = intval($count[$level][$key]);
                if (!isset($count_num) || !isset($car) || !isset($goodId) || !isset($wei)) continue;

                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei, 'value' => $val);
                        $career2[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei, 'value' => $val);
                        $career3[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei, 'value' => $val);
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei, 'value' => $val);
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei, 'value' => $val);
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei, 'value' => $val);
                        break;
                    default:
                        break;
                }
            }
            $pricePack = array($career1, $career2, $career3);
            $cmp['itemList'] = $pricePack;
            $cmp['worldLevel'] = intval($woldLevel);
            $comsumeInfo[] = $cmp;
        }
        return $comsumeInfo;
    }

    //获取保底奖励
    private static function getWishExchange($data)
    {
        if (empty($data)) return array();
        $itemId = self::arrayMergeItem(array($data['item_id2'], $data['item_id']), false);
        $count = self::arrayMergeItem(array($data['count2'], $data['count']), false);
        $career = self::arrayMergeItem(array($data['career2'], $data['career']), false);
        $score = self::arrayMergeItem(array($data['score2'], $data['score']), false);
        $limin = self::arrayMergeItem(array($data['limit2'], $data['limit']), false);

        $career1 = $career2 = $career3 = [];
        foreach ($itemId as $key => $item) {
            if (empty($item)) continue;
            $goodId = explode('/', $item);
            $goodId = $goodId[0];
            $car = $career[$key];
            $count_num = !empty($count[$key]) ? intval($count[$key]) : 1;
            $sco = intval($score[$key]);
            $limit_num = intval($limin[$key]);
            if (!isset($goodId) || !isset($limit_num) || !isset($sco) || !isset($limit_num)) continue;
            switch ($car) {
                case 0:
                    $career1[] = array('itemId' => $goodId, 'count' => $count_num, 'score' => $sco, 'limit' => $limit_num);
                    $career2[] = array('itemId' => $goodId, 'count' => $count_num, 'score' => $sco, 'limit' => $limit_num);
                    $career3[] = array('itemId' => $goodId, 'count' => $count_num, 'score' => $sco, 'limit' => $limit_num);
                    break;
                case 1:
                    $career1[] = array('itemId' => $goodId, 'count' => $count_num, 'score' => $sco, 'limit' => $limit_num);
                    break;
                case 2:
                    $career2[] = array('itemId' => $goodId, 'count' => $count_num, 'score' => $sco, 'limit' => $limit_num);
                    break;
                case 3:
                    $career3[] = array('itemId' => $goodId, 'count' => $count_num, 'score' => $sco, 'limit' => $limit_num);
                    break;
                default:
                    break;
            }
        }
        $pricePack = array($career1, $career2, $career3);
        return $pricePack;
    }

    //上上签
    private static function getShangShangQian($data)
    {
        if (empty($data)) return array();
        $grade = $data['grade'];
        //循环世界等级
        $reward = [];
        foreach ($grade as $level => $info) {
            //大奖
            $topItemId = self::arrayMergeItem(array($data['top']['item_id2'], $data['top']['item_id']));
            $topCount = self::arrayMergeItem(array($data['top']['count2'], $data['top']['count']));
            $topCareer = self::arrayMergeItem(array($data['top']['career2'], $data['top']['career']));
            $career1 = $career2 = $career3 = [];
            //大奖
            foreach ($topItemId[$level] as $top_key => $item) {
                $goodId = explode('/', $item);
                $goodId = $goodId[0];
                $car = $topCareer[$top_key];
                $count_num = !empty($topCount[$top_key]) ? intval($topCount[$top_key]) : 1;
                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => $count_num);
                        $career2[] = array('itemId' => $goodId, 'count' => $count_num);
                        $career3[] = array('itemId' => $goodId, 'count' => $count_num);
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => $count_num);
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => $count_num);
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => $count_num);
                        break;
                    default:
                        break;
                }
            }
            $cmp['worldLevel'] = intval($info);
            $cmp['totalCount'] = intval($data['buyCount']);
            $cmp['freeCount'] = intval($data['freeGold']);
            $cmp['oneCost'] = intval($data['once_gold']);
            $cmp['tenCost'] = intval($data['tentimes_gold']);
            $cmp['bigAward'] = array($career1, $career2, $career3);
            //普通
            $day = $data['day'];
            $career1 = $career2 = $career3 = [];
            foreach ($day['item_id'][$level] as $day_key => $day_item) {
                $goodId = explode('/', $day_item);
                $goodId = $goodId[0];
                $car = $day['career'][$level][$day_key];
                $count_num = !empty($day['count'][$level][$day_key]) ? intval($day['count'][$level][$day_key]) : 1;
                $wei = intval($day['weight'][$level][$day_key]);
                switch ($car) {
                    case 0:
                        #for all career
                        $career1[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei);
                        $career2[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei);
                        $career3[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei);
                        break;
                    case 1:
                        #for 刀
                        $career1[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei);
                        break;
                    case 2:
                        #for 弓
                        $career2[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei);
                        break;
                    case 3:
                        #for 法
                        $career3[] = array('itemId' => $goodId, 'count' => $count_num, 'weight' => $wei);
                        break;
                    default:
                        break;
                }
            }
            $cmp['normalAward'] = array($career1, $career2, $career3);
            $gold = $data['gold'];
            $big = [];
            foreach ($gold['round'][$level] as $gold_key => $gold_info) {
                array_push($big, array('count' => intval($gold_info), 'weight' => intval($gold['weight'][$level][$gold_key])));
            }
            $cmp['bigAwardWeight'] = $big;
            $reward[] = $cmp;
        }
        return $reward;
    }

    /**-----充值排行榜------*/
    private static function getRankReward($data)
    {
        if (empty($data)) return array();
        $grade = $data['grade'];
        $item_id = $data['item_id'];
        $count = $data['count'];
        $career = $data['career'];

        $reward = [];
        foreach ($item_id as $level => $itemInfo) {
            $career1 = $career2 = $career3 = [];
            foreach ($itemInfo as $key => $item) {
                $good = explode('/', $item);
                $good = $good[0];
                $num = intval($count[$level][$key]);
                $car = $career[$level][$key];
                if (!isset($good) || !isset($num) || empty($num)) continue;
                switch ($car) {
                    case 0:
                        $career1[] = array('itemId' => $good, 'count' => $num);
                        $career2[] = array('itemId' => $good, 'count' => $num);
                        $career3[] = array('itemId' => $good, 'count' => $num);
                        break;
                    case 1:
                        $career1[] = array('itemId' => $good, 'count' => $num);
                        break;
                    case 2:
                        $career2[] = array('itemId' => $good, 'count' => $num);
                        break;
                    case 3:
                        $career3[] = array('itemId' => $good, 'count' => $num);
                        break;
                    default:
                        break;
                }
            }
            $temp['worldLevel'] = intval($grade[$level]);
            $temp['itemList'] = array($career1, $career2, $career3);
            $reward[] = $temp;
        }
        return $reward;
    }

    /**
     * 扭蛋活动特殊要求，判断每对应的amount是否相似
     * @param string $data
     */
    private static function check_reward($data)
    {
        //格式化reward
        $reward = json_decode($data, true);
        $amount = $reward['amount'];
        $vip = $reward['vip_level'];
        $grade = $reward['grade'];

        $act = [];
        foreach ($amount as $world_level => $mmg) {
            $list = 0;
            foreach ($mmg as $montkey => $num) {
                if (!isset($grade[$world_level])) die(json_encode(['code' => 404, 'msg' => '世界等级' . $world_level . '不存在']));
                $act[$grade[$world_level]][$list]['amount'] = $num;
                $act[$grade[$world_level]][$list]['vip'] = $vip[$world_level][$montkey];
                $list++;
            }
        }
        //校验个数
        $mont_number_arr = [];
        foreach ($act as $key => $val) {
            array_push($mont_number_arr, count($val));
        }
        $mont_number = count(array_unique($mont_number_arr));

        if ($mont_number > 1) {
            die(json_encode(['code' => 404, 'msg' => '累计奖励个数不等']));
        }
        //对比每一栏目的amount
        $data = $act;
        $modelData = array_shift($act);//对比模板
        foreach ($data as $msg) {
            foreach ($msg as $key => $val) {
                if (!isset($modelData[$key]['amount'])) {
                    continue;
                }
                if (!isset($modelData[$key]['vip'])) {
                    continue;
                }
                if ($val['amount'] != $modelData[$key]['amount']) {
                    die(json_encode(['code' => 404, 'msg' => '存在不相等amount' . $key . ":" . $val['amount']]));
                }
                if ($val['vip'] != $modelData[$key]['vip']) {
                    die(json_encode(['code' => 404, 'msg' => '存在不相等vip' . $key . ":" . $val['vip']]));
                }
            }
        }
    }

    //摇摇乐世界等级下perAccount检测
    private static function check_YYreward($data)
    {
        $reward = json_decode($data, true);
        $vip = $reward['vip_level'];
        $grade = $reward['grade'];

        $act = [];
        foreach ($vip as $world_level => $mmg) {
            $list = 0;
            foreach ($mmg as $montkey => $num) {
                if (!isset($grade[$world_level])) die(json_encode(['code' => 404, 'msg' => '世界等级' . $world_level . '不存在']));
                $act[$grade[$world_level]][$list]['vip'] = $vip[$world_level][$montkey];
                $list++;
            }
        }
        //校验个数
        $mont_number_arr = [];
        foreach ($act as $key => $val) {
            array_push($mont_number_arr, count($val));
        }
        $mont_number = count(array_unique($mont_number_arr));

        if ($mont_number > 1) {
            die(json_encode(['code' => 404, 'msg' => '累计奖励个数不等']));
        }
        //对比每一栏目的amount
        $data = $act;
        $modelData = array_shift($act);//对比模板
        foreach ($data as $msg) {
            foreach ($msg as $key => $val) {
                if (!isset($modelData[$key]['vip'])) {
                    continue;
                }
                if ($val['vip'] != $modelData[$key]['vip']) {
                    die(json_encode(['code' => 404, 'msg' => '存在不相等个人次数' . $key . ":" . $val['vip']]));
                }
            }
        }
    }

    /**
     * 成长翻倍，等级检测
     * @param array $data
     */
    public static function growLevelCheck($data)
    {
        $level = $data['level'];
        static $levelListNum = [];
        foreach ($level as $lst => $typeId) {
            $num = array_keys($typeId);
            if ($levelListNum != null && !empty(array_diff($num, $levelListNum))) die(json_encode(['code' => 404, 'msg' => '类型不一致'], JSON_UNESCAPED_UNICODE));
            $levelListNum = $num;
        }
    }
    //合并单一道具和通用道具
    //@param $data1 $data2
    private static function arrayMergeItem($dataArr, $type = true)
    {
        $item1 = $dataArr[0];
        $item2 = $dataArr[1];
        if ($type === true) {
            foreach ($item1 as $world_level => &$itemInfo) {
                $itemInfo = array_merge($itemInfo, $item2[$world_level]);
            }
        } else if ($type === false) {
            $item1 = array_merge($item1, $item2);
        } else {
            return array();
        }
        return $item1;
    }
    //end class
}
