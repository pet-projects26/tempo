<?php
class ActivityTmpModel extends Model{
	public function __construct(){
        parent::__construct('activity_tmp');
        $this->alias = 'a';
    }

    /**
     * [根据UUID返回信息]
     * @param [string ]$uuid [uuid]
     * @return [array] 一条信息
     */
    public function getRowByuuid($uuid) {
    	$fields = "*";
    	$WHERE = array('uuid' => $uuid);

    	return $this->getRow($fields, $WHERE);
    }

   
	/**
	 * [临时活动数据]
	 * @param  [type] $conditions [description]
	 * @return [type]             [description]
	 */
	public function record_data($conditions){
		$rs = array();
		$fields = array('uuid', 'servers', 'name', 'type', 'start_time', 'end_time', 'state', 'status', 'create_time'); 
		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		$num  = 0;
		error_reporting(E_ALL);
		if($rs){
			$num = $this->getCount();
			foreach($rs as $k => &$v){
				$uuid = $v['uuid'];

				$servers = trim($v['servers'] , '[]');
				if (!empty($servers)) {
					$sql="select name from ny_server where server_id in  ($servers);";
				
					$server=$this->query($sql);
					$server= array_column($server,'name');
					$v['servers'] = implode(',',$server);
				} else {
					$v['servers'] = '';
				}
		  
				
				$v['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
				
				$v['end_time'] = date('Y-m-d H:i:s', $v['end_time']);
			
				$v['state'] = $v['state'] ==  0 ? '<button class="btn btn-success">开</button>' : '<button class="btn btn-default">关</button>';

				$v['status'] = $v['status'] ==  0 ? '<button class="btn btn-success">发布</button>' : '<button class="btn btn-default">删除</button>';

				$v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);

				$v['caozuo'] = " <input type='button'  class='gbutton' value='编辑'  onclick=edit('{$uuid}',0) > | <input type='button'  class='gbutton' value='删除'  onclick=del('{$uuid}') >  |  <input type='button'  class='gbutton' value='复制'  onclick=edit('{$uuid}',1) style ='margin-top:10px' >";
			
			}

			unset($v);
			//print_r($rs);exit;
			foreach($rs as $k=>$v){
				$rs[$k] = array_values($v);
			}
		}


		$this->setPageData($rs , $num);
	}

	/**
	 * [检测活动数据]
	 */
	public function check($data, $uuid = false) {
		$content = $data['content'];
		$type = $data['type'];		

		
		$param = array();

		$dbAction = 'update';

		if (!$uuid || $type == 1) {
			$dbAction = 'insert';

			$uuid = $this->uuid();
			$sql = "SELECT * FROM ny_activity_tmp WHERE uuid = '{$uuid}' limit 1";
			$res = $this->query($sql);

			if (!empty($res)) {
				return array(array('请重试'));
			}
		}


		$param['_id'] = $uuid;
		$param['aId'] = $data['aid'];
		$param['name'] = $data['name'];
		$param['tmType'] = 1;
		$param['tm']['start'] = $data['start_time'];
		$param['tm']['close'] = $data['end_time'];
		$param['state'] = $data['state'];
		$param['openLvl'] = intval($data['openlvl']);
		$param['srcId'] = 'srcId';
		$param['content'] = $content;


		$server = json_decode($data['server'], true);
		$res = $this->getGm(array(), $server);


		$serverRes = array();
		foreach ($res as $row) {
			$serverRes[$row['server_id']] = $row;
		}

		$msg = array();

		//并发调用请求协议，防止由于某个服连接不上，而阻塞。
		
		$vars = array(
			'document' => array(
				'aId' => $data['aid'], 
				'_id' => $uuid,  
				'start' => $data['start_time'],
				'close' => $data['end_time'])
		);
		
		$postData = array(
            'r' => 'call_method',
            'class' => 'ActivityModel',
            'method' => 'checkAct',
            'params' => json_encode($vars),
        );

        //查找服务器的api Url
        $instance  = new ServerconfigModel();

        $urls = $instance->getApiUrl($server, true);

        $rs = Helper::rolling_curl($urls, $postData);

        $vars = array();
        foreach ($rs as $sign => $json) {
        	$server_name = isset($serverRes[$sign]) ? $serverRes[$sign]['name'] : $sign;

        	$rows = json_decode($json, true);

        	$str = '';

        	if (isset($rows['errno'])) {
        		$str .= $server_name . "错误信息：". $rows['error'] ."\r\n";

        	} else {
        		if (is_array($rows)) {
	        		foreach ($rows as $row) {
	        			if (isset($row['name'])) {
		        			$str .= $server_name .':已存在相同类型 【'.$row['name'].'】,其ID为 【'. $row['_id'] .'】' . "\r\n";
	        			} else {
	        				$str .= $server_name . $row;
	        			}
		        	}
		        }
        	}
        	

        	$vars[$sign] = $str;
        }

        if (!empty($vars)) {
        	return array_filter($vars);
        }

        return array();
	}

	/**
	 * [检测活动数据]
	 */
	public function addData($data, $uuid = false) {
		$content = $data['content'];
		$type = $data['type'];		
		
		$param = array();

		$dbAction = 'update';

		if (!$uuid || $type == 1) {
			$dbAction = 'insert';

			$uuid = $this->uuid();
			$sql = "SELECT * FROM ny_activity_tmp WHERE uuid = '{$uuid}' limit 1";
			$res = $this->query($sql);

			if (!empty($res)) {
				return array(array('请重试'));
			}
		}

		$aidList = array(
			'1001' => '测试',
			'1002' => '单笔充值',
			'1003' => '累计充值',
			'1004' => '首冲团购',
			'1005' => '云购',
			'1006' =>  '鉴宝',
			'1007' =>  '全民嗨',
 			'1008' =>  '燃放烟花',
			'1009' =>  '消费',
			'1030' =>  '砸蛋',
			'1010' =>  '节日掉落',
			'1020' =>  '节日BUFF',
		);

		$aid = $data['aid'];

		$param['_id'] = $uuid;
		$param['aId'] = $data['aid'];
		
		$param['name'] = isset($aidList[$aid]) ? $aidList[$aid] : $data['name'];

		$param['tmType'] = 1;
		$param['tm']['start'] = $data['start_time'];
		$param['tm']['close'] = $data['end_time'];
		$param['state'] = $data['state'];
		$param['openLvl'] = intval($data['openLvl']);
		$param['srcId'] = 'srcId';
		$param['content'] = $content;
		
		
		$server = json_decode($data['server'], true);


		$res = $this->getGm(array(), $server);

		$serverRes = array();

		unset($row);

		foreach ($res as $row) {
			$serverRes[$row['server_id']] = $row;
		}

		$msg = array();

		//并发调用请求协议，防止由于某个服连接不上，而阻塞。
		$postData = array(
            'r' => 'call_method',
            'class' => 'ActivityModel',
            'method' => 'addAct',
            'params' => json_encode(array('document' => $param)),
        );

        //查找服务器的api Url
        $instance  = new ServerconfigModel();

        $urls = $instance->getApiUrl($server, true);

        $rs = Helper::rolling_curl($urls, $postData);

        $vars = $msg = array();

        foreach ($rs as $sign => $json) {
        	$serRow = isset($serverRes[$sign]) ? $serverRes[$sign] : '';
        	if (empty($serRow)) {
        		$msg[$sign] = $sign . "配置不存在";
        		continue;
        	}

        	$server_name =  $serRow['name'];
        	$ip = $serRow['ip'];
        	$gm_port = $serRow['gm_port'];

        	$rows = json_decode($json, true);

        	if (isset($rows['errno'])) {
        		$msg[$sign] = $server_name . "错误信息：". $rows['error'] ."\r\n";

        		continue;
        	} 

			if ($json != 1) {
				$msg[$sign] = $server_name . ': 写入mongo库失败';
				continue;
 			}

			$res = $this->rpcCall($ip , $gm_port , 'uactivity');

			if ($res !== 0 ) {
				$msg[$sign] = $server_name .': 通知更新活动失败';
				continue;
			}

			$msg[$sign] = $server_name. ' :更新成功'; 
		}



		$time = time();

		// // 写入数据库
		if ($dbAction == 'insert') {
			$insert = array(
				'uuid' => $uuid,
				'type' => $data['aid'],
				'name' => $data['title'],
				'openLvl' => intval($data['openLvl']),
				'content' => $data['old_content'],
				'servers' => json_encode($server),
				'start_time' => $data['start_time'],
				'end_time' => $data['end_time'],
				'state' => $data['state'],
				'create_time' => $time,
				'last_time' => $time,
				'msg' => json_encode($msg),
			);

			$insertId = $this->add($insert);
		} else {
			$update = array(
				'uuid' => $uuid,
				'type' => $data['aid'],
				'name' => $data['title'],
				'openLvl' => intval($data['openLvl']),
				'content' => $data['old_content'],
				'servers' => json_encode($server),
				'start_time' => $data['start_time'],
				'end_time' => $data['end_time'],
				'state' => $data['state'],
				'status' => 0 ,
				'last_time' => $time,
				'msg' => json_encode($msg),
			);

			$this->update($update, array('uuid' => $uuid));
		}
		return $msg;
	}

	/**
	 * [删除活动]
	 * @param  [string] $uuid [uuid]
	 * @return [type]       [description]
	 */
	public function del($uuid) {
		$res = $this->getRowByuuid($uuid);
		if (empty($res)) {
			return array(array('查询不到相关信息'));
		}

		$server = json_decode($res['servers'], true);

		$res = $this->getGm(array(), $server);

		$serverRes = array();
		foreach ($res as $row) {
			$serverRes[$row['server_id']] = $row;
		}


		$msg = array();

		$param = array(
			'_id' => $uuid,
		);

		foreach ($server as $key => $server_id) {

			$info = isset($serverRes[$server_id]) ? $serverRes[$server_id] : array();

			if (empty($info)) {
				$msg[$server_id] = '查找不到相对应的服务器配置';
				continue;
			}


			$ip = $info['ip'];
			$gm_port = $info['gm_port'];
			$server_name = $info['name'];

			// $gm_port = 6714;

			//写入mongo库
			$rs = $this->call('Activity' , 'resetAct' , array('document' => $param) , $server_id);


			$code  = $rs[$server_id];


			if ($code != 1) {
				$msg[$server_id] = $server_name . ': 写入mongo库失败';
				continue;
 			}


			$res = $this->rpcCall($ip , $gm_port , 'uactivity');


			if ($res != 0 ) {
				$msg[$server_id] = $server_name .': 通知更新活动失败';
				continue;
			}

			$msg[$server_id] = $server_name. ' :更新成功'; 
		}

		$time = time();

		$this->update(array('status' => 1, 'last_time' => $time), array('uuid' => $uuid));

		return array('msg' => $msg, 'data' => '');

	}

	/**
	 * [生成UUID]
	 * @return [string] [uuid]
	 */
	public function uuid() {
        $charid = md5(uniqid(mt_rand(), true));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
               .substr($charid, 8, 4).$hyphen
               .substr($charid,12, 4).$hyphen
               .substr($charid,16, 4).$hyphen
               .substr($charid,20,12);
        return $uuid;
   }
	
}