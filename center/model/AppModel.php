<?php

class AppModel extends Model{

    public function __construct(){
        parent::__construct('app');
        $this->alias = 'a';
    }

    public function record_data($conditions){
        $fields = array('app_id' , 'name' , 'secret' , 'platform' , "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        unset(CDict::$platform[0]);
        foreach($rs as $k => $row){
            $rs[$k] = array(
                $row['app_id'],
                $row['name'],
                $row['secret'],
                $row['platform'] ? CDict::$platform[$row['platform']] : implode(' , ' , CDict::$platform),
                $row['create_time'],
                '<input type="button" class="gbutton" onclick="edit(\'' . $row['app_id'] . '\')" value="编辑">
                <input type="button" class="gbutton" onclick="del(\'' . $row['app_id'] . '\')" value="删除">'
            );
        }
        $this->setPageData($rs , $this->getCount());
    }

    public function getApp($app_id = '' , $fields = array() , $start = '' , $end = ''){
        empty($fields) && $fields = array('app_id' , 'name' , 'secret' , 'platform' , 'create_time');
        $conditions = array();
        if(is_array($app_id)){
            $app_id && $conditions['WHERE']['app_id::IN'] = $app_id;
            $start && $conditions['WHERE']['create_time::>='] = $start;
            $end && $conditions['WHERE']['create_time::<='] = $end;
            $conditions['Extends']['ORDER'] = array('id#desc');
            return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        }
        else{
            $app_id && $conditions['WHERE']['app_id'] = $app_id;
            return $this->getRow($fields, $conditions['WHERE']);
        }
    }

    public function addApp($data){
        if($data){
            return $this->add($data);
        }
    }

    public function editApp($data , $app_id){
        if($data && $app_id){
            $conditions = array();
            $conditions['WHERE']['app_id'] = $app_id;
            return $this->update($data , $conditions['WHERE']);
        }
        else{
            return false;
        }
    }

    public function delApp($app_id){
        if($app_id){
            $conditions = array();
            $conditions['WHERE']['app_id'] = $app_id;
            return $this->delete($conditions['WHERE']);
        }
    }

    public function getSecret($app_id){
        $fields = array('app_id' , 'secret');
        return $this->getApp($app_id , $fields);
    }
}