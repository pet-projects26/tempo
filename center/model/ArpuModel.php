<?php
class ArpuModel extends Model{

    public function __construct(){
        parent::__construct('arpu');
        $this->alias = 'a';
    }

    public function arpu_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Arpu' , 'getArpuData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function arpu_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Arpu' , 'getArpuData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , 'ARPU' , 'ARPPU' , '首充ARPU' , '注册ARPU');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , 'ARPU（' . date('Y年m月d日H时i分s秒') . '）');
    }
}