<?php

class BanModel extends Model
{

    public function banTalk($server, $content, $type, $time, $reason)
    {

        $this->sm = new ServerModel();
        $this->ro = new RoleModel();
        if ($server && $content) {
            $content = explode(',', $content);
            $user = '脚本自动封禁';
            $create_time = time();
            $content_name = array();
            foreach ($content as $k => $v) {
                $content_name[] = trim($v);
            }

            $sql = "select server_id , ip,gm_port from ny_server_config where  server_id = '$server'";
            $serverarr = $this->sm->query($sql);
            unset($server);
            //兼容之前的写法
            $server[0] = $serverarr[0]['server_id'];
            $server[1] = $serverarr[0]['ip'];
            $server[2] = $serverarr[0]['gm_port'];

            $server_id = $server[0];

            $url2 = Helper::v2();

            if ($type == 2) {//封角色名
                $role_name = "'" . implode("','", $content_name) . "'";
                $sql = "select role_id from ny_role where name in ($role_name)";
                $arr = array('query' => array('data' => array($sql)));
                $res = (new ServerconfigModel())->makeHttpParams(array("$server_id"), $arr);

                $urls = $res['urls'];
                $param = $res['params'];
                $res = Helper::rolling_curl($urls, $param);
                $data = json_decode($res[$server_id], true);
                $data = $data['data']['query'][0];

                foreach ($data as $k => $v) {
                    $res = Helper::rpc($url2, array($server[0] => 1), 'logout', array('role_id' => $v['role_id']));
                }
            }

            //获取封禁列表中已存在的封禁对象，如果是已存在的对象则更新，如果不是则写入
            $rs = $this->call('Ban', 'getBanByTypeName', array('type' => $type, 'name' => $content_name, 'fields' => array('id', 'name', 'type')), $server[0]);
            $rs = $rs[$server[0]];

            if (empty($rs)) {

                $add_data = array();
                foreach ($content as $k => $v) {
                    $name = trim($v);
                    //if(!in_array($name . $type , $nametypes)){
                    $add_data[$k]['name'] = $name;
                    $add_data[$k]['type'] = $type;
                    $add_data[$k]['time'] = $time;
                    $add_data[$k]['reason'] = $reason;
                    $add_data[$k]['user'] = $user;
                    $add_data[$k]['create_time'] = $create_time;
                    //}
                }

                $ars = $this->call('Ban', 'addBan', array('data' => $add_data), $server[0]);//写入
                $ars = $ars[$server[0]];
            } else {


                $nametypes = array();
                foreach ($rs as $row) {
                    $names[$row['id']] = $row['name'];
                    $nametypes[$row['id']] = $row['name'] . $row['type'];
                }

                $update_data = array();
                $update_data['time'] = $time;
                $update_data['reason'] = $reason;
                $update_data['user'] = $user;
                $update_data['create_time'] = $create_time;
                $urs = $this->call('Ban', 'updateBan', array('data' => $update_data, 'id' => array_keys($names)), $server[0]);//更新
                $urs = $urs[$server[0]];
            }


            if (@$ars['state'] || @$urs['state']) {
                $ban = array( //发包内容
                    'delete' => 1,
                    'content' => !empty($names) ? $names : $ars['content'],
                    'type' => $type,
                    'time' => $time
                );

                $res = Helper::rpc($url2, array($server[0] => 1), 'ban', $ban);

                $json = $res[$server[0]] ? 'success' : 'fail';
            } else {
                $json = 'fail';
            }
            echo $json;
        }

    }

    public function record_data($conditions)
    {
        $rs = array();
        $num = 0;
        if (isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Ban', 'getRecordData', array('conditions' => $conditions), $server_id);
            $data = $rs[$server_id];

            !empty($data) && list($rs, $num) = $data;

            foreach ($rs as $k => $row) {
                $row['time'] = intval($row['time']);
                $date = date('Y-m-d H:i:s', $row['create_time'] + $row['time']);

                $row['time'] = CDict::$banTime[$row['time']] ? CDict::$banTime[$row['time']] : '未知';

                $status_str = '';

                if ($row['deleted']) {
                    $status_str = '<span style="color:#ff0000;">已解除</span>';
                } else {
                    $status_str = '<input type="button" class="gbutton checked" value="解封" onclick="_deleted(\'' . $row['id'] . '\',\'' . $server_id . '\')">';
                }

                $rs[$k] = array(
                    $row['id'],
                    CDict::$banType[$row['type']]['title'],
                    $row['time'],
                    $row['name'],
                    $date,
                    $row['reason'] ? CDict::$banReason[$row['reason']] : $row['other'],
                    $status_str,
                    $row['user'],
                    date('Y-m-d H:i:s', $row['create_time'])
                );
            }
        }
        $this->setPageData($rs, $num);
    }
}