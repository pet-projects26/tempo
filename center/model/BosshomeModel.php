<?php

class BosshomeModel extends Model{

    public function __construct(){
        parent::__construct('boss_home');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $conditions['WHERE']['a.layer'] = $conditions['WHERE']['layer'] ? $conditions['WHERE']['layer'] : 1;
        unset($conditions['WHERE']['layer']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'layer', 'layer_join_num', 'boss_die_num', 'free_num', 'free_count', 'pay_num', 'pay_count');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach($arr as $k => $row){
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = @sprintf("%.2f" , $row['join_num'] /$row['active_num'] * 100) . '%';
                //本层人均击杀BOSS次数
                $data[$k][5] = @round($row['boss_die_num'] /$row['layer_join_num'], 2);

                //本层免费进入占比
                $data[$k][6] = @sprintf("%.2f" , $row['free_num'] /$row['layer_join_num'] * 100) . '%';
                //本层免费进入人均次数
                $data[$k][7] = @round($row['free_count'] /$row['free_num'], 2);

                //本层付费进入人均次数
                $data[$k][8] = @round($row['pay_count'] /$row['pay_num'], 2);
            }
        }

        foreach($data as $k=>$row){
            $data[$k]=array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $conditions['WHERE']['a.layer'] = $conditions['WHERE']['layer'] ? $conditions['WHERE']['layer'] : 1;
        unset($conditions['WHERE']['layer']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'layer', 'layer_join_num', 'boss_die_num', 'free_num', 'free_count', 'pay_num', 'pay_count');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

        if ($arr) {
            foreach($arr as $k => $row){
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = @sprintf("%.2f" , $row['join_num'] /$row['active_num'] * 100) . '%';
                //本层人均击杀BOSS次数
                $data[$k][5] = @round($row['boss_die_num'] /$row['layer_join_num'], 2);

                //本层免费进入占比
                $data[$k][6] = @sprintf("%.2f" , $row['free_num'] /$row['layer_join_num'] * 100) . '%';
                //本层免费进入人均次数
                $data[$k][7] = @round($row['free_count'] /$row['free_num'], 2);

                //本层付费进入人均次数
                $data[$k][8] = @round($row['pay_count'] /$row['pay_num'], 2);
            }
        }

        $result = array();

        $result[] =  array('日期', '服务器' , '活跃人数' , '参与人数' , '参与度', '本层人均击杀BOSS次数', '本层免费进入占比', '本层免费进入人均次数', '本层付费进入人均次数');

        foreach($data as $row){
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result , '','多人BOSS（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('*');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}