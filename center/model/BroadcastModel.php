<?php
class BroadcastModel extends Model{
	private $s;
    public function __construct(){
        parent::__construct('broadcast');
        $this->alias = 'bc';
		$this->s=new ServerModel();
    }
	public function getBroadcast($conditions){
		$rs = array();$num = 0;

		$username = $_SESSION['username'];

		if($_SESSION['group_channel'] != 'all'){

            $server = $this->getServerByTrait();
        
            foreach ($server as $key => $value) {
                foreach($value as $k => $v){
                    $arr[] = $k;
                }
            }
            unset($server);
            
            foreach ($arr as $kk => $vv) {
              $array[]= "find_in_set('$vv',server)";
            }

            $array = implode(' or ' , $array);

            $where = " 1 and adder = '$username' or ";

            if(!empty($array)){
                $where .=  $array;
            }

            $sql = "select * from ny_broadcast where   $where   order by id desc ";

            $result = $this->query($sql);
            
        }else{

           
        	$result=$this->getRows($fields,$conditions['WHERE'],$conditions['Extends']);
        }

		
		
		$arr=array();
		if($result){
			foreach($result as $k=>$v){
				if($v['status'] != 2){
					$arr[$k]['id']='<input type="checkbox" class="checked" value="'.$v['id'].'">'.$v['id'];
				}else{
					$arr[$k]['id']=$v['id'];
				}
				
				$arr[$k]['starttime']=date('Y-m-d H:i',$v['starttime']);
				$arr[$k]['endtime']=date('Y-m-d H:i',$v['endtime']);

				$arr[$k]['type']= CDict::$broadCastType[$v['type']]['name'];

				$arr[$k]['intervaltime']=$v['intervaltime'];
                $arr[$k]['content'] = htmlspecialchars_decode(stripslashes($v['content']));
				switch($v['status']){
					case 0:
						$arr[$k]['status']='未同步';break;
					case 1:
						$arr[$k]['status']='已同步';break;
					case 2:
						$arr[$k]['status']='删除';break;
				}
				if($v['synctime']){
					$arr[$k]['synctime']=date('Y-m-d H:i',$v['synctime']);
				}else{
					$arr[$k]['synctime']='';	
				}
				$server_id=explode(',',$v['server']);
				
				$server=$this->s->getServer($server_id);
				$servername=array_column($server,'name');
				$servername=implode(',',$servername);
				$arr[$k]['log']=$servername;
				
				
				if($v['status']==0){
					$arr[$k]['caozuo']='<span><a  href="javascript:void(0)" onclick="edit('.$v['id'].')" style="margin-right:10px;">修改</a></span>';	
				}else{
					$arr[$k]['caozuo']='<span><a  href="javascript:void(0)" onclick="deleteid('.$v['id'].')" style="margin-right:10px;">取消同步</a></span>';
				}
				
			}
			
			foreach($arr as $key=>$rows){
				$rs[$key]=array_values($rows);	
			}
			$num=count($rs);
		}
		
		$this->setPageData($rs , $num);
	}	
	public function addbroadcast($post){
		
		if($post['id']==0){
			unset($post['id']);
			$this->add($post);	
		}else{
			$this->update($post,array('id'=>$post['id']));
		}
	}
}