<?php
class CenterlogModel extends Model{
	
    public function __construct(){
        parent::__construct('log');
    }

    public function centerlog_data($conditions){
    
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	isset($conditions['WHERE']['created::>=']) && $conditions['WHERE']['created::>='] = $conditions['WHERE']['created::>='];
    	isset($conditions['WHERE']['created::<=']) && $conditions['WHERE']['created::<='] = $conditions['WHERE']['created::<='];
    	 
    	$fields = array('log_id','username' ,"ip",'ctrl','act','desc2','created');
    	$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    	$aaData = [];
    	$num = $this->getCount();
    	$aaData = [];
    	foreach($arr as $k => $row){
    		$aaData[$k][0] = $row['log_id'];
    		$aaData[$k][1] = $row['ctrl'];
    		$aaData[$k][2] = $row['act'];
    		$aaData[$k][3] = $row['desc2'];
    		$aaData[$k][4] = $row['username'];
    		$aaData[$k][5] = $row['ip'];
    		$aaData[$k][6] = date("Y-m-d H:i:s",$row['created']);
    		
    	}
    	$this->setPageData($aaData , $num ,$limit);
    
    }
    
}