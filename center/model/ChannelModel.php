<?php

class ChannelModel extends Model{

    public function __construct(){
        parent::__construct('channel');
        $this->alias = 'c';
    }

    public function getChannel($channel_id = '' , $fields = array() , $start = '' , $end = ''){
        empty($fields) && $fields = array('id' , 'num' , 'channel_id' , 'name' , 'create_time','grouplist', 'game_id', 'game_name', 'game_abbreviation', 'game_secret_key', 'cdn_url');
        $conditions = array();
        if(is_array($channel_id)){
            $channel_id && $conditions['WHERE']['channel_id::IN'] = $channel_id;
            $start && $conditions['WHERE']['create_time::>='] = $start;
            $end && $conditions['WHERE']['create_time::<='] = $end;
            $row = $this->getRows($fields , $conditions['WHERE']);
            return $row;
        }
        else{
            $channel_id && $conditions['WHERE']['channel_id'] = $channel_id;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    public function record_data($conditions){
        $fields = array('id' , 'num' , 'channel_id' , 'name' , 'create_time');
        $rs = $this->getRows($fields , $conditions['WHERE'],$conditions['Extends']);
        $num = $this->getCount();
        foreach($rs as $k => $row){
            $row['create_time'] = date('Y-m-d H:i:s' , $row['create_time']);
            $row['options']  = '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['channel_id'] . '\')">';
            $row['options'] .= '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['channel_id'] . '\')">';
            unset($row['id']);
            unset($row['num']);
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $num);
    }

    public function addChannel($data){
        if($data){
            return $this->add($data);
        }
    }

    public function editChannel($data , $channel_id){
        if($data && $channel_id){
            $conditions = array();
            $conditions['WHERE']['channel_id'] = $channel_id;
            return $this->update($data , $conditions['WHERE']);
        }
        else{
            return false;
        }
    }

    public function delChannel($channel_id){
        if($channel_id){
            $cgm = new ChannelgroupModel();
            $channelGroup = $cgm->getChannelGroup(array() , array('id' , 'channel_package'));
            foreach($channelGroup as $row){
                $row['channel_package'] = json_decode($row['channel_package'] , true);
                foreach($row['channel_package'] as $channel => $package){
                    if($channel_id == $channel){
                        unset($row['channel_package'][$channel]);
                        $data = array('channel_package' => json_encode($row['channel_package']));
                        $cgm->editChannelGroup($data , $row['id']);
                        break;
                    }
                }
            }
            $conditions = array();
            $conditions['WHERE']['channel_id'] = $channel_id;
            return $this->delete($conditions['WHERE']);
        }
    }

    public function getChannelByNum($num = array() , $fields = array()){
        empty($fields) && $fields = array('id' , 'num' , 'channel_id' , 'name' , 'create_time');
        $conditions = array();
        if(is_array($num)){
            $num && $conditions['WHERE']['num::IN'] = $num;
            return $this->getRows($fields , $conditions['WHERE']);
        }
        else{
            $num && $conditions['WHERE']['num'] = $num;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    /**
     * 通过包号获取渠道号
     * @param array $package = array()
     * @return array $output
    */
    public function getChannelByPackage($package){
        $packageModel = new PackageModel();

        if(empty($package)){
            $out = $packageModel->getChannelPackage();
            return $out;//返回所有包号对应的渠道组
        }
        $conditions['WHERE']['package_id::IN'] = array_unique(array_filter($package));

        $fields = array('channel_id' , 'package_id' , 'create_time' , 'name');
        $out = $packageModel->getRows($fields,$conditions['WHERE']);
        return $out;
    }
}