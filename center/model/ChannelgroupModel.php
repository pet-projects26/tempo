<?php

class ChannelgroupModel extends Model{

    public function __construct(){
        parent::__construct('channel_group');
        $this->alias = 'cg';
    }

    public function getChannelGroup($id = '' , $fields = array() , $start = '' , $end = ''){
        empty($fields) && $fields = array('id' , 'name' , 'server' , 'channel_package' , 'create_time');
        $conditions = array();
        if(is_array($id)){

            $conditions['WHERE'] = array();

            $id && $conditions['WHERE']['id::IN'] = $id;
            $start && $conditions['WHERE']['create_time::>='] = $start;
            $end && $conditions['WHERE']['create_time::<='] = $end;
            return $this->getRows($fields , $conditions['WHERE']);
        }
        else{
            $id && $conditions['WHERE']['id'] = $id;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    public function record_data($conditions){
        $rs = $this->getChannelGroup(array() , array('id' , 'name' , 'create_time'));
        foreach($rs as $k => $row){
            $rs[$k] = array(
            	$row['id'],
                $row['name'],
                date('Y-m-d H:i:s' , $row['create_time']),
                '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['id'] . '\')">' .
                '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['id'] . '\')">'
            );
        }
        $this->setPageData($rs , count($rs) , $conditions['Extends']['LIMIT']);
    }

    public function addChannelGroup($data){
        if($data){
            $a =  $this->add($data);
            return $a;
        }
    }

    public function editChannelGroup($data , $id){
        if($data && $id){
            $conditions = array();
            $conditions['WHERE']['id'] = $id;
            return $this->update($data , $conditions['WHERE']);
        }
        else{
            return false;
        }
    }

    public function delChannelGroup($id){
        if($id){
            $conditions = array();
            $conditions['WHERE']['id'] = $id;
            return $this->delete($conditions['WHERE']);
        }
    }

    //根据服务器获取渠道
    public function getChannelByServer($server_id){
        $channelGroup = $this->getChannelGroup(array() , array('server' , 'channel_package'));
        foreach($channelGroup as $row){
            $server = json_decode($row['server'] , true);
            if(in_array($server_id , $server)){
                $channel_package = json_decode($row['channel_package'] , true);
                return array_keys($channel_package);
            }
        }
    }

    /**
     * 获取所有提审渠道组
     * @param array $conditions
     */
    public function get_list($conditions){
        //$this->alias = 'g';
        $fields = array('id','name');
        $row = $this->getRows($fields,$conditions['WHERE'],$conditions['Extends']);
        $num = $this->getCount();
        $temp = [];
        $array = array();
        if(!empty($row)){
            $id = array_column($row, 'id');
            $id = implode(',', $id);

            /*$sql ="select group_id, api_url,channel_rv from ny_server_config sc LEFT JOIN ny_server s
          on s.server_id = sc.server_id where s.group_id in ($id) and s.server_id
                like '%0001' GROUP BY s.group_id; ";*/
            $sql = "select group_id,api_url from ny_server_config sc left join ny_server as s
                  on s.server_id = sc.server_id where s.group_id in(".$id.") and s.server_id like '%0001' group by s.group_id";
            $rs = $this->query($sql);
            $sql9999 = "select group_id,channel_rv from ny_server_config as sc left join ny_server as s
                        on s.server_id = sc.server_id where s.group_id in(".$id.") and s.server_id like '%9999' or s.server_id like '%9998'
                         group by s.group_id";
            $rows = $this->query($sql9999);
            $arr = $cmp =  array();
            //获取提审
            foreach($rows as $kv=>$vv){
                $cmp[$vv['group_id']] = $vv;
            }
            foreach ($rs as $key => $value) {
                $url = $value['api_url'];
                $url = preg_replace("/index.php.*/", 'version.txt', $url);
                $ver = @file_get_contents($url);
                $arr[$value['group_id']] = !empty($ver) ? $ver : '0';
                unset($ver);
            }
            foreach ($row as $key => $val) {
                //$array[$val['id']] = $val;
                $array[$val['id']] = array(
                    'id'=>$val['id'],
                    'name'=>$val['name'],
                    'ver'=>$arr[$val['id']],
                    'channel_rv'=>!empty($cmp[$val['id']]['channel_rv'])?$cmp[$val['id']]['channel_rv']:'0',
                );
            }
        }

        foreach($array as $key=>$vals){
            $temp[] = array_values($vals);

        }

        $this->setPageData($temp,$num);
    }

    /**
     * 标定包进行提升操作
     * @mark 跨模型调用
     * @param array $data
     * @return int $res
     */
    public function set_rvtype($data){
        //print_r($data);
        $packageModel = new PackageModel();
        $review_num = $data['review_num'];
        $rv_param = $data['rv_param'];
        $res = null;
        $conditions = array();
        if(empty($data['package_id']) && empty($data['group_id'])){
            //$row = $packageModel->getPackage(array(),array('p.package_id'));
            $fields = array('package_id');
            $con['WHERE']['platform'] = 2;
            $row = $packageModel->getRows($fields , $con['WHERE']);
            $temp = [];
            foreach($row as $v) {
                array_push($temp, $v['package_id']);
            }
            $conditions['WHERE']['package_id::IN'] = $temp;

        }else if(empty($data['package_id']) && !empty($data['group_id'])){
            $sql = $sql = "select p.package_id from ny_package  as p left join ny_channel_group as g on p.group_id = g.id where p.group_id in('".join('\',\'',$data['group_id'])."') amd p.platform = 2";
            $row = $packageModel->query($sql);
            $temp = [];
            foreach($row as $v) {
                array_push($temp, $v['package_id']);
            }
            $conditions['WHERE']['package_id::IN'] = $temp;
        }else if(!empty($data['package_id'])){
            $conditions['WHERE']['package_id::IN'] = $data['package_id'];
        }
        if(!empty($conditions)) {
            $conditions['WHERE']['platform'] = 2;//IOS版本
            $res = $packageModel->update(array('review_num' => $review_num,'rv_param'=>json_encode($rv_param)), $conditions['WHERE']);
        }
        return $res;
    }

    public function set_serType($data){
        $group_ids = $data['group_id'];
        $rv_number = $data['review_num'];
        $conditions = array();
        if(empty($group_ids)){
            $channel_groupids = $this->getChannelGroupByTrait();
            foreach($channel_groupids as $msg){
                array_push($group_ids,$msg['id']);
            }
        }
        $group_ids = array_unique(array_filter($group_ids));
        $conditions['WHERE']['id::IN'] = $group_ids;
        $res = $this->update(array('version'=>$rv_number),$conditions['WHERE']);
        return$res;
    }

}