<?php

class ChargeDistributeModel extends Model{
    public function __construct(){
        parent::__construct('charge_distribute');
        $this->alias = 'o';
    }
    
    public function chargeDistribute_data($conditions){
    
    	//
//     	print_r($conditions);exit;
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['create_time::>=']) && $conditions['WHERE']['create_time::>='] = date("Y-m-d",$conditions['WHERE']['create_time::>=']);
    		isset($conditions['WHERE']['create_time::<=']) && $conditions['WHERE']['create_time::<='] = date("Y-m-d",$conditions['WHERE']['create_time::<=']);
    			
    		$fields = array('SUM(chargenum) AS chargenum','SUM(range1) AS range1',
    				'SUM(range2) AS range2', 'SUM(range3) AS range3' , 'SUM(range4) AS range4' , 'SUM(range5) AS range5' , 'SUM(range6) AS range6' 
    				, 'SUM(range7) AS range7' , 'SUM(range8) AS range8' ,
    				'SUM(range9) AS range9' ,"create_time" 
    		);
    		$conditions['Extends']['GROUP'] = "server,create_time";
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
//     		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$rs[$row['create_time']]['create_time'] = $row['create_time'];
    			$rs[$row['create_time']]['chargenum']   += $row['chargenum'];
    			$rs[$row['create_time']]['range1']      += $row['range1'];
    			$rs[$row['create_time']]['range2']      += $row['range2'];
    			$rs[$row['create_time']]['range3']      += $row['range3'];
    			$rs[$row['create_time']]['range4']      += $row['range4'];
    			$rs[$row['create_time']]['range5']      += $row['range5'];
    			$rs[$row['create_time']]['range6']      += $row['range6'];
    			$rs[$row['create_time']]['range7']      += $row['range7'];
    			$rs[$row['create_time']]['range8']      += $row['range8'];
    			$rs[$row['create_time']]['range9']      += $row['range9'];
    		}
    		$rs = array_values($rs);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , 0);
    }
    
    public function chargeDistribute_export($conditions){
    
    	$rs = array();
    	if(isset($conditions['WHERE']['server::IN'])){
    		unset($conditions['Extends']['LIMIT']);
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['create_time::>=']) && $conditions['WHERE']['create_time::>='] = date("Y-m-d",$conditions['WHERE']['create_time::>=']);
    		isset($conditions['WHERE']['create_time::<=']) && $conditions['WHERE']['create_time::<='] = date("Y-m-d",$conditions['WHERE']['create_time::<=']);
    			
    		$fields = array('SUM(chargenum) AS chargenum','SUM(range1) AS range1',
    				'SUM(range2) AS range2', 'SUM(range3) AS range3' , 'SUM(range4) AS range4' , 'SUM(range5) AS range5' , 'SUM(range6) AS range6' 
    				, 'SUM(range7) AS range7' , 'SUM(range8) AS range8' ,
    				'SUM(range9) AS range9' ,"create_time" 
    		);
    		$conditions['Extends']['GROUP'] = "server,create_time";
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    	}
    	$result = array();
    	$result[] = array('日期' , '总付费玩家' , '[0,100]' , '[100,200]',
    			'[200,500]' , '[500,1000]' , '[1000,2000]' , '[2000,5000]' , '[5000,10000]' , '[10000,20000]' , '[20000,&]'
    	);
    	
    	foreach($arr as $k => $row){
    		$rs[$row['create_time']]['create_time'] = $row['create_time'];
    		$rs[$row['create_time']]['chargenum']   += $row['chargenum'];
    		$rs[$row['create_time']]['range1']      += $row['range1'];
    		$rs[$row['create_time']]['range2']      += $row['range2'];
    		$rs[$row['create_time']]['range3']      += $row['range3'];
    		$rs[$row['create_time']]['range4']      += $row['range4'];
    		$rs[$row['create_time']]['range5']      += $row['range5'];
    		$rs[$row['create_time']]['range6']      += $row['range6'];
    		$rs[$row['create_time']]['range7']      += $row['range7'];
    		$rs[$row['create_time']]['range8']      += $row['range8'];
    		$rs[$row['create_time']]['range9']      += $row['range9'];
    	}
    	$rs = array_values($rs);
    	foreach($rs as $k=>$row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportExcel($result , '充值分布记录（' . date('Y年m月d日H时i分s秒') . '）');
    }

}