<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/4/30
 * Time: 17:42
 */

class ChargeIndexModel extends Model
{
    public function __construct()
    {
        parent::__construct('charge_index');
        $this->alias = 's';
    }


    /**
     * @param  [array] $conditions [搜索条件]
     * @return [array]             [返回结果]
     */
    public function get_charge_index_data($conditions)
    {
        unset($conditions['WHERE']['package::IN']);
        if ($conditions['WHERE']['server::IN']) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['s.server::IN'] = $server;
        }

        //默认最近一天
        $conditions['WHERE']['s.time::>='] = isset($conditions['WHERE']['s.time::>=']) ? $conditions['WHERE']['s.time::>='] : strtotime(date('Ymd'));
        $conditions['WHERE']['s.time::<='] = isset($conditions['WHERE']['s.time::<=']) ? $conditions['WHERE']['s.time::<='] : strtotime(date('Ymd')) + 86399;

        $this->joinTable = array(
            'r' => array('name' => 'server', 'type' => 'LEFT', 'on' => 'r.server_id = s.server'),
            'c' => array('name' => 'channel_group', 'type' => 'LEFT', 'on' => 'c.id = r.group_id')
        );

        $fields = array('r.name as server_name', 'c.name as group_name', 's.`index` as `index`', 's.name as name', 's.price as price', 's.charge_num as charge_num', 's.charge_count as charge_count', 's.charge_money as charge_money', 's.time', 's.server as server');

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $result = array();

        if ($rs) {

            //记录单服总次数数组
            $singleSum = [];

            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);//时间
                $result[$k]['group_name'] = $v['group_name'];
                $result[$k]['server_name'] = $v['server_name'];
                $result[$k]['index'] = $v['index'];
                $result[$k]['name'] = $v['name'];
                $result[$k]['price'] = $v['price'];
                $result[$k]['charge_num'] = $v['charge_num'];
                $result[$k]['charge_count'] = $v['charge_count'];
                $result[$k]['charge_money'] = $v['charge_money'];

                if (!array_key_exists($v['server'], $singleSum)) {
                    $where = [];
                    $where['server'] = $v['server'];
                    $where['time'] = $v['time'];
                    $num = $this->getRow('sum(charge_count) as sum', $where);
                    $singleSum[$v['server']] = isset($num['sum']) ? $num['sum'] : 0;
                }
                $result[$k]['charge_per'] = $v['charge_count'] && $singleSum[$v['server']] ? @sprintf("%.2f", $v['charge_count'] / $singleSum[$v['server']] * 100) . '%' : '0%';
            }
        }

        return $result;
    }

    //单服汇总
    public function charge_index_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_charge_index_data($conditions);

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }

    public function charge_index_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_charge_index_data($conditions);

        $result = array();
        $result[] = array(
            '日期','渠道', '服务器', '档位ID', '档位描述', '档位金额', '充值人数', '充值次数', '总金额');
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '充值档位统计（' . date('Y年m月d日H时i分s秒') . '）');

    }
}