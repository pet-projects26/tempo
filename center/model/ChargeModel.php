<?php

class ChargeModel extends Model
{
    public function __construct()
    {
        parent::__construct('order');
        $this->alias = 'o';
    }

    public function getFailOrder_data($conditions)
    {

        $rs = array();
        $num = 0;
        if (isset($conditions['WHERE']['server::IN'])) parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
        $order = $conditions['WHERE']['order'];
        isset($conditions['WHERE']['create_time::>=']) && $conditions['WHERE']['o.create_time::>='] = $conditions['WHERE']['create_time::>='];
        isset($conditions['WHERE']['create_time::<=']) && $conditions['WHERE']['o.create_time::<='] = $conditions['WHERE']['create_time::<='];
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);
        unset($conditions['WHERE']['order']);

        $conditions['WHERE']['o.status'] = 3;

        //权限处理
        if (is_array($_SESSION['group_channel'])) {
            $channellist = array_keys($_SESSION['group_channel']);
            $conditions['WHERE']['o.channel::IN'] = $channellist;
        }

        $this->joinTable = array(
            'c' => array('name' => 'channel', 'type' => 'LEFT', 'on' => 'c.channel_id = o.channel'),
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
        );
        $fields = array('c.name as channel', 's.name as server', 'package', 'corder_num',
            'o.id', 'role_name', 'account', 'role_level', 'money', 'gold', 'order_num',
            "from_unixtime(o.create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'o.status'
        );
        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        //echo $this->lastQuery();exit;
        $num = $this->getCount();

        foreach ($arr as $k => $row) {
            $btn = <<< END
           <a href="javascript:void(0)" onclick="logup('{$row['id']}')" style="text-decoration: underline;">补单</a>
END;
            $rs[$k]['channel'] = $row['channel'];
            $rs[$k]['server'] = $row['server'];
            $rs[$k]['package'] = $row['package'];
            $rs[$k]['corder_num'] = $row['corder_num'];
            $rs[$k]['order_num'] = $row['order_num'];
            $rs[$k]['account'] = $row['account'];
            $rs[$k]['role_name'] = $row['role_name'];
            $rs[$k]['money'] = $row['money'];
            $rs[$k]['gold'] = $row['money'] != 18 ? $row['gold'] : '宝箱';
            $rs[$k]['create_time'] = $row['create_time'];
            $rs[$k]['btn'] = $btn;
        }
        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs, $num);
    }

    public function record_data($conditions)
    {

        // print_r($conditions);exit;
        $rs = array();
        $num = 0;
//         if(isset($conditions['WHERE']['server::IN'])  ||  isset($conditions['WHERE']['package::IN'])){
        if (isset($conditions['WHERE']['server::IN'])) parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
        $order = $conditions['WHERE']['order'];
        isset($conditions['WHERE']['create_time::>=']) && $conditions['WHERE']['o.create_time::>='] = $conditions['WHERE']['create_time::>='];
        isset($conditions['WHERE']['create_time::<=']) && $conditions['WHERE']['o.create_time::<='] = $conditions['WHERE']['create_time::<='];
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);
        unset($conditions['WHERE']['order']);
//
        if (empty($conditions['WHERE']['o.status'])) {
            $conditions['WHERE']['o.status'] = 1;
        }

        //权限处理
        if (is_array($_SESSION['group_channel'])) {
            $channellist = array_keys($_SESSION['group_channel']);
            $conditions['WHERE']['o.channel::IN'] = $channellist;
        }

        $this->joinTable = array(
            'c' => array('name' => 'channel', 'type' => 'LEFT', 'on' => 'c.channel_id = o.channel'),
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
            'p' => array('name' => 'package', 'type' => 'LEFT', 'on' => 'p.package_id = o.package')
        );
        $fields = array('c.name as channel', 's.name as server', 'package', 'p.name as package_name', 'corder_num',
            'o.id', 'role_name', 'role_id', 'account', 'role_level', 'money', 'order_num',
            "from_unixtime(o.create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'o.status', 'item_id', 'reward_item_id', 'reward_item_count'
        );

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        //echo $this->lastQuery();exit;
        $num = $this->getCount();
        foreach ($arr as $k => $row) {

            $rs[$k]['channel'] = $row['channel'];
            $rs[$k]['server'] = $row['server'];
            $rs[$k]['package'] = $row['package'];
            $rs[$k]['package_name'] = $row['package_name'];
            $rs[$k]['corder_num'] = $row['corder_num'];

            $rs[$k]['order_num'] = $row['order_num'];
            $rs[$k]['account'] = $row['account'];
            $rs[$k]['role_name'] = $row['role_name'];
            $rs[$k]['role_id'] = $row['role_id'];
            $rs[$k]['role_level'] = $row['role_level'];
            $rs[$k]['money'] = $row['money'];
            $rs[$k]['item_id'] = $row['item_id'];
            $rs[$k]['create_time'] = $row['create_time'];

            if ($row['status'] == 1) {
                $span = '<span style="color:#228b22;">';
            } else {
                $span = '<span style="color:#ff0000;">';
            }

            $rs[$k]['status'] = $span . CDict::$chargeStatus[$row['status']] . "</span>";

        }
        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
// 		}
        $this->setPageData($rs, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();
//        if(isset($conditions['WHERE']['server::IN']) || isset($conditions['WHERE']['channel::IN']) ||  isset($conditions['WHERE']['package::IN'])){
        unset($conditions['Extends']['LIMIT']);

        //权限处理
        if (is_array($_SESSION['group_channel'])) {
            $channellist = array_keys($_SESSION['group_channel']);
            $conditions['WHERE']['o.channel::IN'] = $channellist;
        }

        $this->joinTable = array(
            'c' => array('name' => 'channel', 'type' => 'LEFT', 'on' => 'c.channel_id = o.channel'),
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
            'p' => array('name' => 'package', 'type' => 'LEFT', 'on' => 'p.package_id = o.package')
        );

        $fields = array('c.name as channel', 's.name as server', 'package', 'p.name as package_name', 'corder_num',
             'role_name', 'role_id', 'account', 'role_level', 'money', 'order_num',
            "from_unixtime(o.create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'o.status', 'item_id', 'gold'
        );

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);//
//         }
        $result = array();
        $result[] = array('渠道', '区服', '包号', '包名','第三方订单号',
             '角色名', '角色id', '账号', '等级', '金额', '订单号',  '充值时间', '订单状态', '充值档位', '元宝'
        );
        foreach ($rs as $row) {
            $corder_num = $row['corder_num'];
            $row['corder_num'] = $row['corder_num'] ? "'$corder_num'" : $row['corder_num'];
            $row['status'] = $row['status'] ? '成功' : '失败';
            $result[] = $this->formatFields($row);
        }
        Util::export_csv_2($result, '', '订单列表（' . date('Y年m月d日H时i分s秒') . '）.csv');
    }

    public function level_data($conditions)
    {
        $rs = array();
        $num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if (isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Charge', 'getLevelChargeData', array('conditions' => $conditions), $server_id);

            list($rs, $num) = $this->callDataMerge($rs);
            foreach ($rs as $k => $row) {
                if ($row[6] >= 0) {
                    $diff = $row[5] - $row[6];
                    if ($diff < 0) {
                        $row[6] = '<span style="color:#ff0000;">比昨天减少了 </span>' . abs($diff);
                    } else if ($diff == 0) {
                        $row[6] = '<span style="color:#ff8c00;">与昨天相同</span>';
                    } else {
                        $row[6] = '<span style="color:#008000;">比昨天增加了 </span>' . $diff;
                    }
                }
                $rs[$k] = $row;
            }
        }
        $this->setPageData($rs, $num, $limit);
    }

    public function level_export($conditions)
    {
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if (isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Charge', 'getLevelChargeData', array('conditions' => $conditions), $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
            foreach ($rs as $k => $row) {
                if ($row[6] >= 0) {
                    $diff = $row[5] - $row[6];
                    if ($diff < 0) {
                        $row[6] = '比昨天减少了' . abs($diff);
                    } else if ($diff == 0) {
                        $row[6] = '与昨天相同';
                    } else {
                        $row[6] = '比昨天增加了' . $diff;
                    }
                }
                $rs[$k] = $row;
            }
        }
        $result = array();
        $result[] = array('等级范围', '登录人数', '充值人数', '充值次数', '充值元宝', '比例', '对比昨天元宝充值');
        foreach ($rs as $k => $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '充值等级（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function rank_data($conditions)
    {
        $rs = array();
        $num = 0;

        //进行覆盖，限制条件
        $conditions['Extends']['ORDER'] = array('sum_money#desc', 'o.account#desc');

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        if (isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Charge', 'getRankData', array('conditions' => $conditions), $server_id);
            list($rs, $num) = $this->callDataMerge($rs);
        }

        if (empty($rs)) {
            $rs = array();
        }

        if (empty($num)) {
            $num = 0;
        }

        $this->setPageData($rs, $num, $limit);
    }

    public function rank_export($conditions)
    {
        $rs = array();
        if (isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Charge', 'getRankData', array('conditions' => $conditions), $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array(
            '排名', '账号', '角色名', '职业', '当前等级', '单次最高金额',
            '充值总额', '元宝总额', '累计充值次数', '最后一次充值时间', '最后登录时间', '平均充值周期'
        );
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '充值排行（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * 充值排行，多渠道，多服务器
     * @param array $conditions
     */
    public function rank_cs_data($conditions)
    {
        $limit = $conditions['Extends']['LIMIT'];
        $row = [];
        if (isset($conditions['WHERE']['server::IN']) || isset($conditions['WHERE']['channel::IN']) || isset($conditions['WHERE']['package::IN'])) {

            empty($conditions['WHERE']['o.create_time::>=']) && $conditions['WHERE']['o.create_time::>='] = mktime('0', '0', '0', date('m'), 1, date('Y'));
            empty($conditions['WHERE']['o.create_time::<=']) && $conditions['WHERE']['o.create_time::<='] = time();
            $condition = [];
            $condition['WHERE']['o.create_time::>='] = $conditions['WHERE']['o.create_time::>='];
            $condition['WHERE']['o.create_time::<='] = $conditions['WHERE']['o.create_time::<='];
            isset($conditions['WHERE']['o.money']) && $condition['WHERE']['o.money::BETWEEN'] = $conditions['WHERE']['o.money::BETWEEN'];
            if (isset($conditions['WHERE']['server::IN'])) {
                $condition['WHERE']['o.server::IN'] = $conditions['WHERE']['server::IN'];
                $condition['Extends']['GROUP'] = "o.server,o.role_id";
            } else {
                $condition['Extends']['GROUP'] = "o.role_id";
            }

            $condition['Extends']['ORDER'] = array('money_sum#desc', 'o.account#desc');
            isset($conditions['WHERE']['package::IN']) && $condition['WHERE']['o.package::IN'] = $conditions['WHERE']['package::IN'];
            //$condition['Extends']['LIMIT'] = $conditions['WHERE']['LIMIT'];
            $condition['WHERE']['o.status'] = 1;
            $this->joinTable = array(
                's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 'o.server = s.server_id'),
            );
            $condition['Extends']['LIMIT'] = $conditions['Extends']['LIMIT'];
            $fields = array('1 as level', 's.`name`', 'o.account', 'o.role_name', 'o.role_career', 'o.role_level', 'max(o.money) as charge_max',
                'sum(o.money) as money_sum', 'sum(o.gold) as gold_num', 'count(distinct(o.order_num)) as charge_num',
                'max(o.create_time) as last_charge_time');
            $row = $this->getRows($fields, $condition['WHERE'], $condition['Extends']);
        }
        $num = $this->getCount();
        $i = $limit[0];
        foreach ($row as &$msg) {
            $i++;
            $msg['level'] = $i;
            $msg['role_career'] = self::getRealCareer($msg['role_career']);
            $msg['last_charge_time'] = date('Y-m-d H:i:s', $msg['last_charge_time']);
            $msg = array_values($msg);
        }
        empty($row) && $num = 0;
        $this->setPageData($row, $num, $limit);
    }

    public function rank_cs_export($conditions)
    {
        empty($conditions['WHERE']['o.create_time::>=']) && $conditions['WHERE']['o.create_time::>='] = mktime('0', '0', '0', date('m'), 1, date('Y'));
        empty($conditions['WHERE']['o.create_time::<=']) && $conditions['WHERE']['o.create_time::<='] = time();
        $condition = [];
        $condition['WHERE']['o.create_time::>='] = $conditions['WHERE']['o.create_time::>='];
        $condition['WHERE']['o.create_time::<='] = $conditions['WHERE']['o.create_time::<='];
        isset($conditions['WHERE']['o.money']) && $condition['WHERE']['o.money::BETWEEN'] = $conditions['WHERE']['o.money::BETWEEN'];
        if (isset($conditions['WHERE']['server'])) {
            $condition['WHERE']['o.server::IN'] = $conditions['WHERE']['server::IN'];
            $condition['Extends']['GROUP'] = "o.server,o.role_id";
        } else {
            $condition['Extends']['GROUP'] = "o.role_id";
        }
        $condition['Extends']['ORDER'] = array('money_sum#desc', 'o.account#desc');
        isset($conditions['WHERE']['package::IN']) && $condition['WHERE']['o.package::IN'] = $conditions['WHERE']['package::IN'];
        //$condition['Extends']['LIMIT'] = $conditions['WHERE']['LIMIT'];
        $condition['WHERE']['o.status'] = 1;
        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 'o.server = s.server_id'),
        );
        $fields = array('1 as level', 's.`name`', 'o.account', 'o.role_name', 'o.role_career', 'o.role_level', 'max(o.money) as charge_max',
            'sum(o.money) as money_sum', 'sum(o.gold) as gold_num', 'count(distinct(o.order_num)) as charge_num',
            'max(o.create_time) as last_charge_time');
        $row = $this->getRows($fields, $condition['WHERE'], $condition['Extends']);
        $i = 0;
        foreach ($row as &$msg) {
            $i++;
            $msg['level'] = $i;
            $msg['role_career'] = self::getRealCareer($msg['role_career']);
            $msg['last_charge_time'] = date('Y-m-d H:i:s', $msg['last_charge_time']);
            $msg = array_values($msg);
        }
        $result = array();
        $result[] = array(
            '排名', '服务器', '账号', '角色名', '职业', '当前等级', '单次最高金额',
            '充值总额', '元宝总额', '累计充值次数', '最后一次充值时间'
        );
        foreach ($row as $rs) {
            $result[] = $this->formatFields($rs);
        }
        Util::exportExcel($result, '充值排行（区服）（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * 职业 私有方法
     */
    private static function getRealCareer($career_number)
    {
        $career = CDict::$career;
        $sign = mb_substr($career_number, 0, 1);//兼容中文！
        $careerArr = $career[$sign];
        return $careerArr[$career_number];
    }

    //内网测试用
    public function manual($data)
    {

        $order = array(
            'role_id' => $data['role_id'],
            'order_num' => $data['order_num'],
            'money' => $data['money'],
            'item_id' => $data['item_id'],
            'corder_num' => $data['order_num'],
            'is_test' => 1,
            'account' => $data['account'],
            'create_time' => time()
        );

        //获取服务器配置
        $server_config = (new ServerconfigModel())->getConfig($data['server'], ['server_id', 'websocket_host', 'websocket_port']);

        if ($server_config['websocket_host'] == '' || $server_config['websocket_port'] == '') {
            exit(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
        }

        $rs = $this->call('Charge', 'addOrder', array('data' => $order, 'item_id' => $data['item_id']), $data['server']);

        $rs = $rs[$data['server']];

        if ($rs['code'] == 1) {

            $sendOrder = [];

            array_push($sendOrder, [$data['order_num'], intval($data['role_id']), intval($data['item_id']), floatval($data['money']), intval($data['create_time']) * 1000, true]);

            $sendData = ['opcode' => Pact::$SendGoodsOperate, 'str' => array($sendOrder)];

            $result = $this->socketCall($server_config['websocket_host'], $server_config['websocket_port'], 'sendGoods', $sendData);

            $OrderModel = new ChargeModel();

            if ($result === 0) {
                //发送成功就增加订单
                $orderData = array(
                    'server' => $data['server'],
                    'channel' => 'local',
                    'package' => 8070,
                    'order_num' => $data['order_num'],
                    'corder_num' => $data['corder_num'],
                    'money' => $data['money'],
                    'account' => $data['account'],
                    'role_id' => $data['role_id'],
                    'role_name' => $data['role_name'],
                    'role_level' => 1,
                    'role_career' => 1,
                    'item_id' => $data['item_id'],
                    'create_time' => $data['create_time'],
                    'gold' => 0,
                    'reward_item_id' => 0,
                    'reward_item_count' => 0,
                    'idfa' => '',
                    'status' => 1
                );

                $OrderModel->add($orderData);

                $this->call('Charge', 'update', ['data' => ['status' => 1], 'where' => ['order_num' => $order['order_num']]], $data['server']);
                return 1;
            } else {
                $this->call('Charge', 'update', ['data' => ['status' => 2], 'where' => ['order_num' => $order['order_num']]], $data['server']);
                return 2;
            }

        } else {
            return 2;
        }

    }


    //订单发货
    public function addOrder($order, $params, $channel)
    {
        //写到中央服mysql
        $rs = $this->call('Charge', 'setOrder', array('order' => $order), $order['server']);
        $data = $rs[$order['server']];
        //$data['account'] = $channel . '_' . $data['account'];
        $account = (new RegisterModel())->getRegisterByAcc($data['account']);
        $data['package'] = $account['package'];
        $data['server'] = $order['server'];
        $data['channel'] = $channel;
        $data['params'] = $params;
        if ($this->add($data)) {
            //写入单服mysql和mongo
            unset($rs['status']);
            $rs = $this->call('Charge', 'addOrder', array('data' => $rs[$order['server']], 'item_id' => $order['item_id']), $order['server']);
            if ($rs[$order['server']]['state']) {
                $server = $this->getGm(array(), $order['server']);
                //发送指令告诉服务端有新订单
                $msg = $this->rpcCall($server['ip'], $server['gm_port'], 'order', array('order_num' => $order['order_num'], 'role_id' => $order['role_id']));
                if ($msg === 0) {
                    $status = array('status' => 1);
                    $order_num = array('order_num' => $data['order_num']);
                    $this->update($status, $order_num);
                }
            }
        }
    }

    public function supplementOrder($data, $item_id, $server)
    {
        $rs = $this->call('Charge', 'supplementOrder', array('data' => $data, 'item_id' => $item_id), $server);
        if ($rs[$server]['state']) {
            $server = $this->getGm(array(), $order['server']);
            //发送指令告诉服务端有新订单
            $msg = $this->rpcCall($server['ip'], $server['gm_port'], 'order', array('order_num' => $order['order_num'], 'role_id' => $order['role_id']));
            if ($msg === 0) {
                $status = array('status' => 1);
                $order_num = array('order_num' => $data['order_num']);
                $this->update($status, $order_num);
            }
        }
    }

}