<?php
class ChargedailyModel extends Model{

    public function __construct(){
        parent::__construct('charge_daily');
        $this->alias = 'cd';
    }

    public function daily_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Chargedaily' , 'getDailyData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs , true);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function daily_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Chargedaily' , 'getDailyData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs , true);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , '充值总额' , '充值人数' , '充值次数' , '首冲总额' , '首冲人数' , '首冲次数' , 'ARPU' , '创角数' , '登录数');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '每日充值（' . date('Y年m月d日H时i分s秒') . '）');
    }
}