<?php

class ChargenewModel extends Model{

    public function __construct(){
        parent::__construct('charge_new');
        $this->alias = 'cn';
    }

    public function cnew_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Chargenew' , 'getChargenewData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }

    public function cnew_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Chargenew' , 'getChargenewData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , '首充人数' , '新增人数' , '新增充值金额' , '新增付费率');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '新增玩家（' . date('Y年m月d日H时i分s秒') . '）');
    }
}