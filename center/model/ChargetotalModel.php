<?php
class ChargetotalModel extends Model{
    public function __construct(){
        parent::__construct('charge_total');
        $this->alias = 'ct';
    }
	public function record_data($conditions){
       	/*$fields = array(
            "FROM_UNIXTIME(date,'%Y-%m-%d')",'servername','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve',
            'thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen','twenty','twentyone','twentytwo',
            'twentythree','twentyfour','twentyfive','twentysix','twentyseven','twentyeight','twentynine','thirty'
        );*/
		
		
		$fields = array(
            "FROM_UNIXTIME(date,'%Y-%m-%d')",'servername','sum(one) as one ','sum(two) as two','sum(three) as three','sum(four) as four','sum(five) as five','sum(six) as six','sum(seven) as seven','sum(eight) as eight',' sum(nine) as nine ','sum(ten) as ten','sum(eleven) as eleven',' sum(twelve) as twelve ',
            '  sum(thirteen) as thirteen','  sum(fourteen) as fourteen','  sum(fifteen) as fifteen','  sum(sixteen) as sixteen','  sum(seventeen) as seventeen','  sum(eighteen) as eighteen','  sum(nineteen) as nineteen','  sum(twenty) as twenty','  sum(twentyone) as twentyone',' sum(twentytwo) as twentytwo',
            '  sum(twentythree) as twentythree','  sum(twentyfour) as twentyfour','  sum(twentyfive) as twentyfive','  sum(twentysix) as twentysix','  sum(twentyseven) as twentyseven','  sum(twentyeight) as twentyeight','  sum(twentynine) as twentynine','  sum(thirty) as thirty'
        );
		
		$conditions['Extends']['GROUP'] = 'server';
		
		//
		$channel = $_SESSION['group_channel'];
		
		if($channel != 'all'){
			foreach($channel as $k=>$v){
				$arr[] = $k;
			}
			$conditions['WHERE']['channel::IN'] = $arr;
		}
		
		$rs=$this->getRows($fields,$conditions['WHERE'],$conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $this->getCount());
	}
}