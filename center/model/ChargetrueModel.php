<?php

class ChargetrueModel extends Model{

    public function __construct(){
        parent::__construct('charge_true');
        $this->alias = 'vo';
    }
	public function  manuallist_data($conditions){
		$rs = array();
		$fields = array('id','server','order_num' ,'package'  , 'account' , 'role_id' , 'role_name' ,'money','admin',  'from_unixtime(create_time,"%Y-%m-%d %H:%i:%s") as create_time',' checked');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		$count=$this->getCount();
		
		foreach($rs as $k => $row){
			
			$server = $this->getGm(array('s.name') , $row['server'] );
			$row['server'] = $server['name'];
			
			if ($row['checked'] == 1) {
				$row['checked'] = '<button class="btn btn-success">已审核</button> ';
			}elseif ($row['checked'] == 2){
				$row['checked'] = '<button class="btn btn-wrong">已驳回</button> ';
			}else {
				$row['checked'] = '<input type="button" class="gbutton checked" value="审1核" onclick="_checked(\'' . $row['id'] . '\')"> <input type="button" class="gbutton checked" value="驳回" onclick="_outchecked(\'' . $row['id'] . '\')">';
			}

// 			$row['checked'] = $row['checked'] == 1 ? '<button class="btn btn-success">已审核</button> ' : '<input type="button" class="gbutton checked" value="审核" onclick="_checked(\'' . $row['id'] . '\')"> <input type="button" class="gbutton checked" value="驳回" onclick="_outchecked(\'' . $row['id'] . '\')">';

			unset($row['id']);
			$rs[$k] = array_values($row);	
		}
		
        $this->setPageData($rs , $count);	
	}
}