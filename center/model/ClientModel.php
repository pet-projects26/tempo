<?php

class ClientModel extends Model{

    public function __construct(){
        parent::__construct('client');
        $this->alias = 'c';
    }
    
    //(玩家数据)机型数据 20180310
    public function device_data($conditions){
    
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    
    		$start_time = $conditions['WHERE']['create_time::>=']?$conditions['WHERE']['create_time::>=']:strtotime(date("Y-m-d",time())) -6*86400;
    		$end_time = $conditions['WHERE']['create_time::<=']?strtotime(date("Y-m-d",$conditions['WHERE']['create_time::<='])) + 86400:strtotime(date("Y-m-d",time())) + 86400;
    		//玩家数
    		$sql = $this->getModel($conditions, $start_time, $end_time, 'account');
    		$arr = $this->query($sql);
    		//活跃玩家 (最近七天内有登录过的)
    		$sql = $this->getModel($conditions, $end_time - 60*60*24*7, $end_time, 'account_hy');
    		$arr_hy = $this->query($sql);
    		//流失玩家 (最近三天内没有登录过的)
    		$sql = $this->getModel($conditions, $end_time - 60*60*24*3, $end_time, 'account_ls');
    		$arr_ls = $this->query($sql);
    		foreach ($arr_ls as $value) {
    			$arr_lsArr[] = $value['model'];
    		}
    		//     		 echo $this->lastQuery();exit;
    		$total = $ls_total =  0;
    		$aaData = [];
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$aaData[$k][0] = $row['model'];
    			$aaData[$k][1] = $row['account'];
    			$total += $row['account'];
    			$hy_total = 0;
    			foreach ($arr_hy as $kk=>$vv) {
    				if ($row['model'] == $vv['model']) {
    					$aaData[$k][3] = $vv['account_hy'];
    				}
    				$hy_total += $vv['account_hy'];
    			}
    
    			if (in_array($row['model'], $arr_lsArr)) {
    				foreach ($arr_ls as $kk=>$vv) {
    					if ($row['model'] == $vv['model']) {
    						$aaData[$k][5] = $row['account'] - $vv['account_ls'];
    					}
    				}
    			}else {
    				$aaData[$k][5] = $row['account'];
    			}
    			$ls_total += $aaData[$k][5];
    		}
    		foreach ($aaData as $key => $value) {
    			if ($total) {
    				$aaData[$key][2] =  $total&&$value[1]? round($value[1] / $total, 4) * 100 . '%' : '-';
    			}
    			if ($hy_total) {
    				$aaData[$key][4] =  $hy_total&&$value[3]? round($value[3] / $hy_total, 4) * 100 . '%' : '-';
    			}
    			if ($ls_total) {
    				$aaData[$key][6] =  $ls_total&&$value[5]? round($value[5] / $ls_total, 4) * 100 . '%' : '-';
    			}
    		}
    
    		$rs = array_values($aaData);
    		foreach($rs as $k=>$row){
    			ksort($row);
    			$rs[$k] = $row;
    		}
    
    	}
    	 
    	$this->setPageData($rs ,0);
    }
    
    public function device_export($conditions){
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		$start_time = $conditions['WHERE']['create_time::>=']?$conditions['WHERE']['create_time::>=']:strtotime(date("Y-m-d",time())) -6*86400;
    		$end_time = $conditions['WHERE']['create_time::<=']?strtotime(date("Y-m-d",$conditions['WHERE']['create_time::<='])) + 86400:strtotime(date("Y-m-d",time())) + 86400;
    		//玩家数
    		$sql = $this->getModel($conditions, $start_time, $end_time, 'account');
    		$arr = $this->query($sql);
    		//活跃玩家 (最近七天内有登录过的)
    		$sql = $this->getModel($conditions, $end_time - 60*60*24*7, $end_time, 'account_hy');
    		$arr_hy = $this->query($sql);
    		//流失玩家 (最近三天内没有登录过的)
    		$sql = $this->getModel($conditions, $end_time - 60*60*24*3, $end_time, 'account_ls');
    		$arr_ls = $this->query($sql);
    		foreach ($arr_ls as $value) {
    			$arr_lsArr[] = $value['model'];
    		}
    		//     		 echo $this->lastQuery();exit;
    		$total = $ls_total =  0;
    		$aaData = [];
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$aaData[$k][0] = $row['model'];
    			$aaData[$k][1] = $row['account'];
    			$total += $row['account'];
    			$hy_total = 0;
    			foreach ($arr_hy as $kk=>$vv) {
    				if ($row['model'] == $vv['model']) {
    					$aaData[$k][3] = $vv['account_hy'];
    				}
    				$hy_total += $vv['account_hy'];
    			}
    
    			if (in_array($row['model'], $arr_lsArr)) {
    				foreach ($arr_ls as $kk=>$vv) {
    					if ($row['model'] == $vv['model']) {
    						$aaData[$k][5] = $row['account'] - $vv['account_ls'];
    					}
    				}
    			}else {
    				$aaData[$k][5] = $row['account'];
    			}
    			$ls_total += $aaData[$k][5];
    		}
    		foreach ($aaData as $key => $value) {
    			if ($total) {
    				$aaData[$key][2] =  $total&&$value[1]? round($value[1] / $total, 4) * 100 . '%' : '-';
    			}
    			if ($hy_total) {
    				$aaData[$key][4] =  $hy_total&&$value[3]? round($value[3] / $hy_total, 4) * 100 . '%' : '-';
    			}
    			if ($ls_total) {
    				$aaData[$key][6] =  $ls_total&&$value[5]? round($value[5] / $ls_total, 4) * 100 . '%' : '-';
    			}
    		}
    
    		$rs = array_values($aaData);
    		foreach($rs as $k=>$row){
    			ksort($row);
    			$rs[$k] = $row;
    		}
    
    		$fields = array();
    		$fields[] = array('机型' , '玩家数(账户)' ,'百分比',
    				'活跃玩家(账户)' , '百分比' ,'流失玩家(账户)','百分比');
    		foreach($rs as $row){
    			$fields[] = $this->formatFields($row);
    		}
    		Util::exportExcel($fields , '设备统计数据（' . date('Y年m月d日H时i分s秒') . '）');
    	}
    }
    
    public  function  getModel($conditions,$start_time,$end_time,$account){
    	$where = [];
    	$server = isset($conditions['WHERE']['server::IN']) ? "'" . implode("','" , $conditions['WHERE']['server::IN']) . "'" : '';
    	$server && $where[] = ' server in(' . $server . ') ';
    	$start_time && $end_time && $where[] = " create_time >= $start_time AND create_time <= $end_time ";
    	$where = $where ? 'where' . implode('and' , $where) : '';
    	$sql = 'select model,COUNT(DISTINCT account) AS '.$account.' from ny_client  ' . $where. ' group by model order by model desc';
    	return $sql;
    }

    public function model_network_data($conditions){
        $type = $conditions['WHERE']['type'];
        unset($conditions['WHERE']['type']);
        $type = $type ? 'network' : 'model';
        $fields = array($type , 'count(' . $type . ') as num');
        $conditions['Extends']['GROUP'] = $type;
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $total = 0;
        foreach($rs as $row){
            $total += $row['num'];
        }
        if($total){
            foreach($rs as $k => $row){
                $row['percent'] = $total ? sprintf("%.2f" , $row['num'] / $total * 100) . '%' : '0%';
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , count($rs));
    }
    public function model_network_export($conditions){
        $type = $conditions['WHERE']['type'];
        unset($conditions['WHERE']['type']);
        $type = $type ? 'network' : 'model';
        $fields = array($type , 'count(' . $type . ') as num');
        $conditions['Extends']['GROUP'] = $type;
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $total = 0;
        foreach($rs as $row){
            $total += $row['num'];
        }
        if($total){
            foreach($rs as $k => $row){
                $row['percent'] = $total ? sprintf("%.2f" , $row['num'] / $total * 100) . '%' : '0%';
                $rs[$k] = array_values($row);
            }
        }
        $result = array();
        $result[] = array('类型' , '数量' , '百分比');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '机型网络（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function getModelLossData($conditions){
        $where = array();
        $package = isset($conditions['WHERE']['package::IN']) ? "'" . implode("','" , $conditions['WHERE']['package::IN']) . "'" : '';
        $package && $where[] = ' package in(' . $package . ') ';
        $channel = isset($conditions['WHERE']['channel::IN']) ? "'" . implode("','" , $conditions['WHERE']['channel::IN']) . "'" : '';
        $channel && $where[] = ' channel in(' . $channel . ') ';
        $where = $where ? 'where' . implode('and' , $where) : '';
        $create_time =  strtotime($conditions['WHERE']['date']) - 3 * 86400;
        //流失数量
        $sql = 'select model,count(model) as model_loss_num from ny_client ' . $where . ' group by model having max(create_time) < ' . $create_time . ' order by model_loss_num desc';
        $rs = $this->query($sql);
        $rss = array();
        foreach($rs as $row){
            $rss[$row['model']] = $row['model_loss_num'];
        }
        //总量
        $sql = 'select model,count(model) as model_num from ny_client ' . $where . ' group by model order by model_num desc';
        $rst = $this->query($sql);
        foreach($rst as $k => $row){
            $rst[$k] = array(
                $row['model'],       //机型
                $rss[$row['model']] ? $rss[$row['model']] : 0, //流失数量
                $row['model_num'],   //总量
                $row['model_num'] ? sprintf("%.2f" , $rss[$row['model']] / $row['model_num'] * 100) . '%' : '0%' //流失率
            );
        }
        return $rst;
    }
    public function model_loss_data($conditions){
        $rs = $this->getModelLossData($conditions);
        $this->setPageData($rs , count($rs));
    }
    public function model_loss_export($conditions){
        $rs = $this->getModelLossData($conditions);
        $result = array();
        $result[] = array('机型' , '流失数量' , '总数' , '机型流失率');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '机型流失（' . date('Y年m月d日H时i分s秒') . '）');
    }

    //获取指定日期新增的设备数
    public function getNewDeviceNumByDate($date){

        $sql = 'select count(distinct mac) as device_num from ny_client group by mac having min(create_time) >= ' . strtotime($date) . ' and min(create_time) < ' . (strtotime($date) + 86400);
        $rs = $this->query($sql);
        return $rs[0]['device_num'];
    }

    public function getNewDeviceNumByHour($date, $server){


        $sql = 'select count(distinct mac) as device_num from ny_client group by mac having min(create_time) >= ' . strtotime($date) . ' and min(create_time) < ' . (strtotime($date) + 86400);
        $rs = $this->query($sql);
        return $rs[0]['device_num'];
    }

	public function getClientByrole($id){
		$fields=array('channel','package');
		$conditions['WHERE']['role']=$id;
		return $this->getRow($fields,$conditions['WHERE']);	
	}


}