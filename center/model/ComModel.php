<?php
class ComModel extends Model{
    
    public function __construct() {
    	parent::__construct();
    }

    /**
     * [修改tableName]
     * @param [string] $tableName [描述]
     */
    public function setTable($tableName) {
    	$this->tableName = $tableName;
    	return $this->tableName;
    }

    /**
     * [query description]
     * @param  [type] $sqlArr [description]
     * @return [type]         [description]
     */
    public function query($sqlArr) {
    	
    	if (is_string($sqlArr)) {
    		return  $this->query($sqlArr);
    	}

    	$tmp = array();
    	foreach ($sqlArr as $key => $value) {
    		$tmp[$key] = $this->query($value);
    	}

    	return $tmp;
    }
}