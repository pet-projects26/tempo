<?php

class CrossModel extends Model{

    public function __construct(){
        parent::__construct('server_cross');
        $this->alias = 'c';
    }

    public function record_data($conditions){
        $rs = array();
        $num = 0;
        $fields = array('id','name','groups','type','version','config','create_time','1 as caozuo');
        $result = $this->getRows($fields , $conditions['WHERE'],$conditions['Extends']);
        $num = $this->getCount();

        foreach($result as $k => $v){
            $groups  = $v['groups'];
            $sql ="select name from ny_channel_group where id in ($groups)";
            $name = $this->query($sql);
            $name = array_column($name, 'name');
            $result[$k]['groups'] = implode(',', $name);

            switch ($v['type']) {
                case '1':
                    $result[$k]['type'] = 'IOS';
                    break;
                case '2':
                    $result[$k]['type'] = '安卓';
                    break;
                case '3':
                    $result[$k]['type'] = '混服';
                    break;
                default:
                    $result[$k]['type'] = '未知';
                    break;
            }
            
            $result[$k]['config'] = $v['config'] == 1 ? '<button class="btn btn-success" >已配置</button>' : '<button class="btn btn-default" > 未配置</button>';
            $result[$k]['create_time'] = date('Y-m-d H:i' ,$v['create_time']);
            $result[$k]['caozuo'] = '<input type="button" class="gbutton" value="配置" onclick="config(\'' . $v['id'] . '\')"> <input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $v['id'] . '\')">';
        }

        foreach ($result as $k => $v) {
           $rs[] = array_values($v);
        }
        $this->setPageData($rs , $num);
    }


}