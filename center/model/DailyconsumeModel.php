<?php

class DailyconsumeModel extends Model
{

    public function __construct()
    {
        parent::__construct('daily_consume');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'consume_gold', 'consume_num', 'produce_gold', 'charge_produce_gold', 'free_produce_gold', 'gold_total_stock');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['consume_gold'];
                $data[$k][3] = $row['produce_gold'];
                $data[$k][4] = $row['charge_produce_gold'];

                //充值元宝占比
                $data[$k][5] = @sprintf("%.2f", $row['charge_produce_gold'] / $row['produce_gold'] * 100) . '%';

                //免费元宝
                $data[$k][6] = $row['free_produce_gold'];

                //免费元宝占比
                $data[$k][7] = @sprintf("%.2f", $row['free_produce_gold'] / $row['produce_gold'] * 100) . '%';

                //元宝滞留数
                $data[$k][8] = bcsub($row['produce_gold'], $row['consume_gold'], 2);

                //元宝滞留率
                $data[$k][9] = @sprintf("%.2f", $data[$k][8] / $row['produce_gold'] * 100) . '%';

                //元宝总存量
                $data[$k][10] = $row['gold_total_stock'];

                //活跃人数
                $data[$k][11] = $row['active_num'];

                //消费人数
                $data[$k][12] = $row['consume_num'];

                //消费率
                $data[$k][13] = @sprintf("%.2f", $row['consume_num'] / $row['active_num'] * 100) . '%';

                //消费arppu
                $data[$k][14] = @round($row['consume_gold'] / $row['consume_num'], 2);
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'consume_gold', 'consume_num', 'produce_gold', 'charge_produce_gold', 'free_produce_gold', 'gold_total_stock');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['consume_gold'];
                $data[$k][3] = $row['produce_gold'];
                $data[$k][4] = $row['charge_produce_gold'];

                //充值元宝占比
                $data[$k][5] = @sprintf("%.2f", $row['charge_produce_gold'] / $row['produce_gold'] * 100) . '%';

                //免费元宝
                $data[$k][6] = $row['free_produce_gold'];

                //免费元宝占比
                $data[$k][7] = @sprintf("%.2f", $row['free_produce_gold'] / $row['produce_gold'] * 100) . '%';

                //元宝滞留数
                $data[$k][8] = bcsub($row['produce_gold'], $row['consume_gold'], 2);

                //元宝滞留率
                $data[$k][9] = @sprintf("%.2f", $data[$k][8] / $row['produce_gold'] * 100) . '%';

                //元宝总存量
                $data[$k][10] = $row['gold_total_stock'];

                //活跃人数
                $data[$k][11] = $row['active_num'];

                //消费人数
                $data[$k][12] = $row['consume_num'];

                //消费率
                $data[$k][13] = @sprintf("%.2f", $row['consume_num'] / $row['active_num'] * 100) . '%';

                //消费arppu
                $data[$k][14] = @round($row['consume_gold'] / $row['consume_num'], 2);
            }
        }
        $result = array();

        $result[] = array('日期', '服务器', '日消费', '总元宝产出', '充值元宝产出', '充值产出占比', '免费元宝产出', '免费元宝产出占比', '元宝滞留数', '元宝滞留率', '元宝总存量', '活跃人数', '消费人数', '消费率', '消费ARPPU');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '每日消费统计（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}