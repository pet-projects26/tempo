<?php

class  DailyrechargeModel extends Model{
    public function __construct(){
        parent::__construct('daily_recharge');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'first_recharge', "recharge_num", "recharge_money", 'new_active_num',"new_recharge_num", "new_recharge_money", 'old_user_login_num', 'old_user_recharge_num', 'old_user_recharge_money');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['recharge_money'];
                $data[$k][3] = $row['recharge_num'];
                $data[$k][4] = $row['first_recharge'];
                //ARPPU
                $data[$k][5] = floatval($row['recharge_money']) ? @round($row['recharge_money'] / $row['recharge_num'], 2) : 0.00;
                //ARPU
                $data[$k][6] = floatval($row['recharge_money']) ? @round($row['recharge_money'] / $row['active_num'], 2) : 0.00;
                //付费率
                $data[$k][7] = intval($row['recharge_num']) ? @sprintf("%.2f", $row['recharge_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][8] = $row['new_recharge_num'];
                $data[$k][9] = $row['new_recharge_money'];

                $data[$k][10] = floatval($row['new_recharge_money']) ? @round($row['new_recharge_money'] / $row['new_recharge_num'], 2) : 0.00;

                $data[$k][11] = floatval($row['new_recharge_money']) ? @round($row['new_recharge_money'] / $row['new_active_num'], 2) : 0.00;
                //付费率
                $data[$k][12] = intval($row['new_recharge_num']) ? @sprintf("%.2f", $row['new_recharge_num'] / $row['new_active_num'] * 100) . '%' : '0%';

                $data[$k][13] = $row['old_user_recharge_num'];
                $data[$k][14] = $row['old_user_recharge_money'];

                $data[$k][15] = floatval($row['old_user_recharge_money']) ? @round($row['old_user_recharge_money'] / $row['old_user_recharge_num'], 2) : 0.00;

                $data[$k][16] = floatval($row['old_user_recharge_money']) ? @round($row['old_user_recharge_money'] / $row['old_user_login_num'], 2) : 0.00;

            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {
        $rs = array();
        unset($conditions['Extends']['LIMIT']);


        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'first_recharge', "recharge_num", "recharge_money", 'new_active_num',"new_recharge_num", "new_recharge_money", 'old_user_login_num', 'old_user_recharge_num', 'old_user_recharge_money');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['recharge_money'];
                $data[$k][3] = $row['recharge_num'];
                $data[$k][4] = $row['first_recharge'];
                //ARPPU
                $data[$k][5] = floatval($row['recharge_money']) ? @round($row['recharge_money'] / $row['recharge_num'], 2) : 0.00;
                //ARPU
                $data[$k][6] = floatval($row['recharge_money']) ? @round($row['recharge_money'] / $row['active_num'], 2) : 0.00;
                //付费率
                $data[$k][7] = intval($row['recharge_num']) ? @sprintf("%.2f", $row['recharge_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][8] = $row['new_recharge_num'];
                $data[$k][9] = $row['new_recharge_money'];

                $data[$k][10] = floatval($row['new_recharge_money']) ? @round($row['new_recharge_money'] / $row['new_recharge_num'], 2) : 0.00;

                $data[$k][11] = floatval($row['new_recharge_money']) ? @round($row['new_recharge_money'] / $row['new_active_num'], 2) : 0.00;
                //付费率
                $data[$k][12] = intval($row['new_recharge_num']) ? @sprintf("%.2f", $row['new_recharge_num'] / $row['new_active_num'] * 100) . '%' : '0%';

                $data[$k][13] = $row['old_user_recharge_num'];
                $data[$k][14] = $row['old_user_recharge_money'];

                $data[$k][15] = floatval($row['old_user_recharge_money']) ? @round($row['old_user_recharge_money'] / $row['old_user_recharge_num'], 2) : 0.00;

                $data[$k][16] = floatval($row['old_user_recharge_money']) ? @round($row['old_user_recharge_money'] / $row['old_user_login_num'], 2) : 0.00;

            }
        }

        $result = array();
        $result[] = array('时间', '服务器' , '本日流水' , '付费用户' , '首充人数', 'ARPPU', 'ARPU', '付费率', '新增付费用户', '新增付费金额', '新增用户ARPPU', '新增用户ARPU', '新增用户付费率', '老玩家付费用户', '老玩家付费金额', '老玩家ARPPU', '老玩家ARPU');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, 'VIP分布（' . date('Y年m月d日H时i分s秒') . '）');
    }


    public function getRecordData($date, $server)
    {
        $conditions = [];

        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $rs = $this->getRow('*', $conditions['WHERE']);

        return $rs;
    }
}