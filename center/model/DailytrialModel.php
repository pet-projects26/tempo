<?php

class DailytrialModel extends Model{

    public function __construct(){
        parent::__construct('daily_trial');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $conditions['WHERE']['a.type'] = $conditions['WHERE']['type'] ? $conditions['WHERE']['type'] : 1;
        $type = $conditions['WHERE']['a.type'];
        unset($conditions['WHERE']['type']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'challenge_num', 'challenge_count','inspire_num', 'inspire_count', 'add_num_role', 'add_num_sum');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach($arr as $k => $row){
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = @sprintf("%.2f" , $row['join_num'] /$row['active_num'] * 100) . '%';
                //人均挑战次数
                $data[$k][5] = @round($row['challenge_count'] /$row['join_num'], 2);
                if ($type == 5 || $type == 6) {
                    //鼓舞次数占比
                    $data[$k][6] = @sprintf("%.2f" , $row['inspire_num'] /$row['join_num'] * 100) . '%';
                    //人均鼓舞次数
                    $data[$k][7] = @round($row['inspire_count'] /$row['inspire_num'], 2);

                    //购买次数玩家占比
                    $data[$k][8] = $row['add_num_role'] ? @sprintf("%.2f", $row['add_num_role'] / $row['challenge_num'] * 100) . '%' : '0%';

                    $data[$k][9] = $row['add_num_sum'] ? @round($row['add_num_sum'] / $row['add_num_role'], 2) : 0;
                } else {
                    //购买次数玩家占比
                    $data[$k][6] = $row['add_num_role'] ? @sprintf("%.2f", $row['add_num_role'] / $row['challenge_num'] * 100) . '%' : '0%';

                    $data[$k][7] = $row['add_num_sum'] ? @round($row['add_num_sum'] / $row['add_num_role'], 2) : 0;
                }
            }
        }

        foreach($data as $k=>$row){
            $data[$k]=array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $conditions['WHERE']['a.type'] = $conditions['WHERE']['type'] ? $conditions['WHERE']['type'] : 1;
        $type = $conditions['WHERE']['a.type'];
        unset($conditions['WHERE']['type']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'challenge_num', 'challenge_count','inspire_num', 'inspire_count', 'add_num_role', 'add_num_sum');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach($arr as $k => $row){
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = @sprintf("%.2f" , $row['join_num'] /$row['active_num'] * 100) . '%';
                //人均挑战次数
                $data[$k][5] = @round($row['challenge_count'] /$row['join_num'], 2);
                if ($type == 5 || $type == 6) {
                    //鼓舞次数占比
                    $data[$k][6] = @sprintf("%.2f" , $row['inspire_num'] /$row['join_num'] * 100) . '%';
                    //人均鼓舞次数
                    $data[$k][7] = @round($row['inspire_count'] /$row['inspire_num'], 2);

                    //购买次数玩家占比
                    $data[$k][8] = $row['add_num_role'] ? @sprintf("%.2f", $row['add_num_role'] / $row['challenge_num'] * 100) . '%' : '0%';

                    $data[$k][9] = $row['add_num_sum'] ? @round($row['add_num_sum'] / $row['add_num_role'], 2) : 0;
                } else {
                    //购买次数玩家占比
                    $data[$k][6] = $row['add_num_role'] ? @sprintf("%.2f", $row['add_num_role'] / $row['challenge_num'] * 100) . '%' : '0%';

                    $data[$k][7] = $row['add_num_sum'] ? @round($row['add_num_sum'] / $row['add_num_role'], 2) : 0;
                }
            }
        }

        $result = array();

        if ($type == 5 || $type == 6) {
            $result[] = array('日期', '服务器', '活跃人数', '参与人数', '参与度', '人均挑战次数', '鼓舞人数占比', '人均鼓舞次数', '购买次数玩家占比', '人均购买次数');
        } else {
            $result[] = array('日期', '服务器', '活跃人数', '参与人数', '参与度', '人均挑战次数', '购买次数玩家占比', '人均购买次数');
        }

        foreach($data as $row){
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result , '','多人BOSS（' . date('Y年m月d日') . '）.csv');
    }


    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('*');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}