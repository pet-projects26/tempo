 <?php
class DataModel extends Model{

    public function __construct(){
        parent::__construct('role');
        $this->alias = 'r';
    }
	public function real_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Data' , 'getRealData' , array('conditions' => $conditions) , $server_id);
            $data = $this->callDataMerge($rs);
            !empty($data)&&list($rs , $num) = $data;

            //$rs = empty($rs)?array():$rs;//如果内容为空，则置空
			foreach($rs as $k => $row){
				$rs[$k] = array_values($row);
			}
        }
        $this->setPageData($rs , $num , $limit);
	}
	public function real_export($conditions){
		$rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Data' , 'getRealData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
		$result = array();
        $result[] = array( '日期','新增注册' , '新增角色数' , '活跃人数' , '当前时段在线人数' ,'新增付费人数' , '新增付费金额' , '新增付费AP' ,'充值付费人数' , '充值付费金额' , '充值付费AP' ,);
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '实时数据对比（' . date('Y年m月d日H时i分s秒') . '）');
			
	}
}