<?php

class DbcenterModel extends DbMysqli{
    protected $host = MYSQL_HOST;
    protected $port = MYSQL_PORT;
    protected $user = MYSQL_USER;
    protected $passwd = MYSQL_PASSWD;
    protected $db = MYSQL_DB;
    protected $link_name = MYSQL_DB;

    function __construct($name = null){
        parent::__construct();
        $name && $this->setTableName(MYSQL_PREFIX . $name);
    }
}