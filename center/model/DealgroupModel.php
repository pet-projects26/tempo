<?php

class DealgroupModel extends Model{
	
    public function __construct(){
        parent::__construct('deal_group');
        $this->alias = 'dg';
		
		
		//$this->db = Util::mongoConn("127.0.0.1", 27017,'','','d');
	
		
		$this->db = Util::mongoConn(TRADE_MONGO_HOST,TRADE_MONGO_PORT , TRADE_MONGO_USER , TRADE_MONGO_PASSWD , TRADE_MONGO_DB );

		//$this->db = Util::mongoConn("127.0.0.1", 8900,'','','dcxj_trade');
		//$this->db = Util::mongoConn("127.0.0.1", 20808,'fydpyh','jgnnYBFglGjmGwuOFcSo','dcxj_trade');
		
    }
	/**
     * [findAll 查询交易组全部数据]
     * @return [array] [description]
     */
	public function findAll(){
		return $this->db->trade_group->find();
	}
	
	/**
     * [findOne 查询交易组的一条数据]
     * @param  [string] $id [查询条件]
     * @return [array]     [description]
     */
	public function findOne($id){
		$id = intval($id);
		return $this->db->trade_group->findone(array('_id' => $id));	
	}
	/**
     * [search 根据server模糊查询数据]
     * @param  [string] $server [需要查询的服务器]
     * @return [array]         [description]
     */
	public function search($server){
		
		$q   = new MongoRegex("/$server/");  
		return $this->db->trade_group->find( array('server' => $q ));	
	}
	
    public function record_data($conditions){
        
		//从mongo查询出数据
		$result = array();
        $rs = $this->db->trade_group->find();
		
        foreach($rs as $k => $row){
			
            $result[$k] = array(
                $row['name'].'('.$row['_id'].')',
                date('Y-m-d H:i:s' , $row['create_time']),
                '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['_id'] . '\')">' .
                '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['_id'] . '\')">'
            );
        }
        $this->setPageData($result , count($result) , $conditions['Extends']['LIMIT']);
    }
    //添加交易组
    public function addDealGroup($data){
		
        if($data){
			 return $this->db->trade_group->insert($data);
            
        }
    }
    //编辑交易组
    public function editDealGroup($data , $id){
		
        if($data && $id){
			$id =  intval($id);
			
			return $this->db->trade_group->update(array('_id' => $id) , array('$set'=>$data));
        }
        else{
            return false;
        }
    }
	
	//删除交易组，并通知客户端
    public function delDealGroup($id){
		
        if($id){
				
			$id = intval($id);
			$this->db->trade_group->remove(array('_id' => $id));
			
			
			return $this->message();
			
        }
    }
	
	public function message(){
		
		
		$ip = TRADE_IP;
		
		$port =TRADE_PORT;
		
		
		$rs = $this->rpcCall($ip , $port , 'trade');
		
		return $rs;
		
	}
	
	
	/**
     * [createChannelServer 拼接服务器id]
     * @param  [int] $channelnum [渠道编号]
     * @param  [int] $servernum  [服务器编号]
     * @return [int] $num        [返回给客户端的服务器id]
     */
	public function createChannelServer($channelnum , $servernum){
		$num = ($channelnum + 1) *100000 +$servernum ;
		return $num;
	}
	
/*--------------------下面的是交易模版---------------------------*/	

    
	public function tpl_record_data($conditions){
		parent::__construct('deal_tpl');
        $rs = $this->getRows( array('id' , 'title' , 'type' , 'create_time'));
		
        foreach($rs as $k => $row){
			switch($row['type']){
				case 0 :
					$type = '老服';break;
				case 1 :
					$type = '新服';break;
				case 2 :
					$type = '合服';break;
				case 3 :
					$type = '跨服';break;
			}
			
            $rs[$k] = array(
                $row['id'],
				$row['title'],
				$type,
                date('Y-m-d H:i:s' , $row['create_time']),
                '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['id'] . '\',\'update\')">' .
				'<input type="button" class="gbutton" value="复制" onclick="edit(\'' . $row['id'] . '\',\'add\')">' .
                '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['id'] . '\')">'
            );
        }
        $this->setPageData($rs , count($rs) , $conditions['Extends']['LIMIT']);
    }
	
	public function tpl_delete($id){
        parent::__construct('deal_tpl');
		if($id){
			$id = $id;
            $id && $this->delete(array('id' =>$id));
			
        }
	}
    public function tpl_add($data){
        parent::__construct('deal_tpl');
        
        if($data['status'] == 'add'){
            unset($data['status']);
            unset($data['id']);
            return $this->add($data);
        }else{
            unset($data['status']);
            return $this->update($data , array('id'=> $data['id']));
        }        

    }

    public function tpl_findOne($id){
        parent::__construct('deal_tpl');
        return  $this->getRow('*' , array('id' => $id));
    }

/*--------------------下面的是交易管理---------------------------*/	

	/**
	 * [checkItem 查询交易组的物品]
	 * @param  [int] $groupId [交易组id]
	 * @return [array]          [返回物品的详细信息]
	 */
	public function checkItem($groupId){
		$groupId = intval($groupId);
		return  $this->db->bg_item->find(array('psId' =>$groupId))->sort(array('startTm'=>-1));
	
	}
}