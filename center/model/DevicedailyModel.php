<?php

class DevicedailyModel extends Model{

    public function __construct(){
        parent::__construct('device_daily');
        $this->alias = 'dd';
    }

    public function device_data($conditions){
        $fields = array('date' , 'num' , 'active_num' , 'login_loss');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $this->getCount());
    }
    public function device_export($conditions){
        unset($conditions['Extends']['LIMIT']);
        $fields = array("from_unixtime(date , '%Y-%m-%d') as date" , 'num' , 'active_num' , 'login_loss');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $result = array();
        $result[] = array('日期' , '设备数' , '激活数' , '登录页面流失率');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '机型网络（' . date('Y年m月d日H时i分s秒') . '）');
    }
}