<?php

class DevicelossModel extends Model{

    public function __construct(){
        parent::__construct('device_loss');
        $this->alias = 'dl';
    }

    public function getDevicelossByDate($date,$channel='',$package=''){
        $fields = array('date' , 'new_num' , 'two' , 'three' , 'seven' , 'fifteen' , 'thirty');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
		$conditions['WHERE']["channel"] = $channel;
		$conditions['WHERE']["package"] = $package;
        $result=$this->getRow($fields , $conditions['WHERE']);
		return $result;
    }

    public function setDeviceloss($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
			$conditions['WHERE']["channel"] = $data['channel'];
			$conditions['WHERE']["package"] = $data['package'];
            $this->update($data , $conditions['WHERE']);
        }
    }

    public function device_loss_data($conditions){
    	$array=array();
    	unset($conditions['Extends']['LIMIT']);
    	if($conditions['WHERE']['channel::IN']!='' || $conditions['WHERE']['package::IN']!=''){
    		$fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'new_num' , 'two' , 'three' , 'seven' , 'fifteen' , 'thirty');
    		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		//     		var_dump($this->lastQuery());die;
    		$arr=array();
    		if($rs){
    			foreach($rs as $k=>$v){
    				$key=$v['date'];
    				$arr[$key]['date']=$v['date'];
    				$arr[$key]['new_num']+=$v['new_num'];
    				if($v['two'] !='-'){
    					$arr[$key]['two']+=$v['two'];
    				}
    				if($v['three'] !='-'){
    					$arr[$key]['three']+=$v['three'];
    				}
    				if($v['seven'] !='-'){
    					$arr[$key]['seven']+=$v['seven'];
    				}
    				if($v['fifteen'] !='-'){
    					$arr[$key]['fifteen']+=$v['fifteen'];
    				}
    				if($v['thirty'] !='-'){
    					$arr[$key]['thirty']+=$v['thirty'];
    				}
    			}
    			foreach($arr as $k=>$v){
    				if (!array_key_exists("two",$arr[$k])) {$arr[$k]['two'] ='-';}
    				if (!array_key_exists("three",$arr[$k])) {$arr[$k]['three'] = '-';}
    				if (!array_key_exists("seven",$arr[$k])) {$arr[$k]['seven'] = '-';}
    				if (!array_key_exists("fifteen",$arr[$k])) {$arr[$k]['fifteen'] = '-';}
    				if (!array_key_exists("thirty",$arr[$k])) {$arr[$k]['thirty'] = '-';}
    			}
    			foreach($arr as $k=>$v){
    				if($v['new_num'] !=0){
    					if($v['two'] !='-'){
    						$arr[$k]['two']=$v['two'].'('.sprintf('%.2f',$v['two']/$v['new_num']*100).'%)';
    					}
    					if($v['three'] !='-'){
    						$arr[$k]['three']=$v['three'].'('.sprintf('%.2f',$v['three']/$v['new_num']*100).'%)';
    					}
    					if($v['seven'] !='-'){
    						$arr[$k]['seven']=$v['seven'].'('.sprintf('%.2f',$v['seven']/$v['new_num']*100).'%)';
    					}
    					if($v['fifteen'] !='-'){
    						$arr[$k]['fifteen']=$v['fifteen'].'('.sprintf('%.2f',$v['fifteen']/$v['new_num']*100).'%)';
    					}
    					if($v['thirty'] !='-'){
    						$arr[$k]['thirty']=$v['thirty'].'('.sprintf('%.2f',$v['thirty']/$v['new_num']*100).'%)';
    					}
    				}
    			}
    			foreach($arr as $kk => $row){
    				$array[] = array_values($row);
    			}
    		}
    	}
    	$this->setPageData($array , count($array));
    }
    public function device_loss_export($conditions){
    	/*$fields = array('date' , 'new_num' , 'two' , 'three' , 'seven' , 'fifteen' , 'thirty');
    	 $rs = $this->getRow($fields , $conditions['WHERE'] , $conditions['Extends']);*/
    	$array=array();
    	unset($conditions['Extends']['LIMIT']);
    	if($conditions['WHERE']['channel::IN']!='' || $conditions['WHERE']['package::IN']!=''){
    		$fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'new_num' , 'two' , 'three' , 'seven' , 'fifteen' , 'thirty');
    		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		$arr=array();
    		if($rs){
    			foreach($rs as $k=>$v){
    				$key=$v['date'];
    				$arr[$key]['date']=$v['date'];
    				$arr[$key]['new_num']+=$v['new_num'];
    				if($v['two'] !='-'){
    					$arr[$key]['two']+=$v['two'];
    				}
    				if($v['three'] !='-'){
    					$arr[$key]['three']+=$v['three'];
    				}
    				if($v['seven'] !='-'){
    					$arr[$key]['seven']+=$v['seven'];
    				}
    				if($v['fifteen'] !='-'){
    					$arr[$key]['fifteen']+=$v['fifteen'];
    				}
    				if($v['thirty'] !='-'){
    					$arr[$key]['thirty']+=$v['thirty'];
    				}
    			}
    			foreach($arr as $k=>$v){
    				if (!array_key_exists("two",$arr[$k])) {$arr[$k]['two'] ='-';}
    				if (!array_key_exists("three",$arr[$k])) {$arr[$k]['three'] = '-';}
    				if (!array_key_exists("seven",$arr[$k])) {$arr[$k]['seven'] = '-';}
    				if (!array_key_exists("fifteen",$arr[$k])) {$arr[$k]['fifteen'] = '-';}
    				if (!array_key_exists("thirty",$arr[$k])) {$arr[$k]['thirty'] = '-';}
    			}
    			foreach($arr as $k=>$v){
    				if($v['new_num'] !=0){
    					if($v['two'] !='-'){
    						$arr[$k]['two']=$v['two'].'('.sprintf('%.2f',$v['two']/$v['new_num']*100).'%)';
    					}
    					if($v['three'] !='-'){
    						$arr[$k]['three']=$v['three'].'('.sprintf('%.2f',$v['three']/$v['new_num']*100).'%)';
    					}
    					if($v['seven'] !='-'){
    						$arr[$k]['seven']=$v['seven'].'('.sprintf('%.2f',$v['seven']/$v['new_num']*100).'%)';
    					}
    					if($v['fifteen'] !='-'){
    						$arr[$k]['fifteen']=$v['fifteen'].'('.sprintf('%.2f',$v['fifteen']/$v['new_num']*100).'%)';
    					}
    					if($v['thirty'] !='-'){
    						$arr[$k]['thirty']=$v['thirty'].'('.sprintf('%.2f',$v['thirty']/$v['new_num']*100).'%)';
    					}
    				}
    			}
    			foreach($arr as $kk => $row){
    				$array[] = array_values($row);
    			}
    		}
    	}
    	$result = array();
    	$result[] = array('日期' , '新增设备数' , '第2天流失' , '第3天流失','第7天流失','第15天流失','第30天流失');
    	foreach($array as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportExcel($result , '设备流失（' . date('Y年m月d日H时i分s秒') . '）');
    }
}