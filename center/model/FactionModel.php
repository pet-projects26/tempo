<?php
class FactionModel extends Model{
    public function __construct(){
        parent::__construct('faction');
        $this->alias = 'f';
    }
	public function factionlist_data($conditions){
		$rs=array();$num=0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Faction' , 'getFaction' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
		$this->setPageData($rs , $num , $limit);
	}
}