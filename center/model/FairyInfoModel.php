<?php

class FairyInfoModel extends Model
{

    public function __construct()
    {
        parent::__construct('fairy_info');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'escort_num', 'escort_count', 'rob_num', 'rob_count', 'refresh_num', 'refresh_count', 'select_num', 'select_count', 'join_vip6');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['escort_num'];
                //参与度
                $data[$k][4] = $row['escort_num'] ? @sprintf("%.2f", $row['escort_num'] / $row['active_num'] * 100) . '%' : '0%';
                //人均护送次数
                $data[$k][5] = $row['escort_count'] ? @round($row['escort_count'] / $row['escort_num'], 2) : 0.00;

                $data[$k][6] = $row['rob_num'];
                //拦截度
                $data[$k][7] = $row['rob_num'] ? @sprintf("%.2f", $row['rob_num'] / $row['active_num'] * 100) . '%' : '0%';
                //人均拦截次数
                $data[$k][8] = $row['rob_count'] ? @round($row['rob_count'] / $row['rob_num'], 2) : 0.00;

                //$data[$k][9] = $row['refresh_num'];
                //刷新参与度
                $data[$k][9] = $row['refresh_num'] ? @sprintf("%.2f", $row['refresh_num'] / $row['escort_num'] * 100) . '%' : '0%';
                //人均刷新次数
                $data[$k][10] = $row['refresh_count'] ? @round($row['refresh_count'] / $row['refresh_num'], 2) : 0.00;

                // $data[$k][12] = $row['select_num'];
                //选择女帝参与度
                $data[$k][11] = $row['select_num'] ? @sprintf("%.2f", $row['select_num'] / $row['join_vip6'] * 100) . '%' : '0%';
                //人均选择女帝次数
                $data[$k][12] = $row['select_count'] ? @round($row['select_count'] / $row['select_num'], 2) : 0.00;

            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'escort_num', 'escort_count', 'rob_num', 'rob_count', 'refresh_num', 'refresh_count', 'select_num', 'select_count', 'join_vip6');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['escort_num'];
                //参与度
                $data[$k][4] = $row['escort_num'] ? @sprintf("%.2f", $row['escort_num'] / $row['active_num'] * 100) . '%' : '0%';
                //人均护送次数
                $data[$k][5] = $row['escort_count'] ? @round($row['escort_count'] / $row['escort_num'], 2) : 0.00;

                $data[$k][6] = $row['rob_num'];
                //拦截度
                $data[$k][7] = $row['rob_num'] ? @sprintf("%.2f", $row['rob_num'] / $row['active_num'] * 100) . '%' : '0%';
                //人均拦截次数
                $data[$k][8] = $row['rob_count'] ? @round($row['rob_count'] / $row['rob_num'], 2) : 0.00;

                //$data[$k][9] = $row['refresh_num'];
                //刷新参与度
                $data[$k][9] = $row['refresh_num'] ? @sprintf("%.2f", $row['refresh_num'] / $row['escort_num'] * 100) . '%' : '0%';
                //人均刷新次数
                $data[$k][10] = $row['refresh_count'] ? @round($row['refresh_count'] / $row['refresh_num'], 2) : 0.00;

                // $data[$k][12] = $row['select_num'];
                //选择女帝参与度
                $data[$k][11] = $row['select_num'] ? @sprintf("%.2f", $row['select_num'] / $row['join_vip6'] * 100) . '%' : '0%';
                //人均选择女帝次数
                $data[$k][12] = $row['select_count'] ? @round($row['select_count'] / $row['select_num'], 2) : 0.00;

            }
        }
        $result = array();

        $result[] = array('日期', '服务器', '活跃人数', '参与人数', '参与度', '人均护送次数', '拦截人数', '拦截参与度', '刷新参与度', '人均刷新次数', '选择女帝参与度', '人均选择女帝次数');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '护送仙女（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}