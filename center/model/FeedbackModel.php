<?php

class FeedbackModel extends Model{
    public function __construct(){
        parent::__construct('order');
        $this->alias = 'o';
    }


    public function record_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $server_ids = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);


        if ($conditions['WHERE']['f.type'] < 1) {
            unset($conditions['WHERE']['f.type']);
        }

        if ($conditions['WHERE']['f.status'] < 0) {
            unset($conditions['WHERE']['f.status']);
        }

        $tmp = [];

        foreach ($server_ids as $key => $value) {
            $rs = $this->call('FeedBack', 'record_data', array('conditions' => $conditions), $value);
            if (!empty($rs[$value])) {
                foreach ($rs[$value] as &$v) {
                    $v['server_id'] = $value;
                    $tmp[] = $v;
                }
            }
        }

        $AllPackage = (new PackageModel())->getAllPackageName();
        $AllServer = (new ServerModel())->getAllServerName();

        $rs = array();
        $num = count($tmp);
        foreach ($tmp as $k => $row) {
            $rs[$k][0] = $AllPackage[$row['package']];
            $rs[$k][1] = $AllServer[$row['server_id']];
            $rs[$k][2] = CDict::$questionType[$row['type']];
            $historyStr = $row['role_id'] . ',"' . $row['server_id'] . '"';
            $rs[$k][3] = $row['name'] . "<br><button class='gbutton' onclick='history({$historyStr})'>提问历史</button>";
            $rs[$k][4] = $row['money'] ? $row['money'] : 0.00;
            $rs[$k][5] = $row['content'];
            $rs[$k][6] = date('Y-m-d H:i:s', $row['create_time'] / 1000);
            $str = $row['id'] . ',' . '"' . $row['server_id'] . '"' . ',' . $row['type'] . ',' . $row['role_id'] . ',"' . $row['uuid'] . '"';
            $rs[$k][7] = $row['status'] ? date('Y-m-d H:i:s', $row['reply_time'] / 1000) : "<button class='gbutton' onclick='replyHtml({$str})'> 回复</button> ";
            $rs[$k][8] = $row['status'] ? $row['reply_user'] : '未回复';
        }

        $this->setPageData($rs , $num , $limit);
    }


    public function record_export($conditions)
    {
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $server_ids = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);


        if ($conditions['WHERE']['f.type'] < 1) {
            unset($conditions['WHERE']['f.type']);
        }

        if ($conditions['WHERE']['f.status'] < 0) {
            unset($conditions['WHERE']['f.status']);
        }

        $tmp = [];

        foreach ($server_ids as $key => $value) {
            $rs = $this->call('FeedBack', 'record_data', array('conditions' => $conditions), $value);
            if (!empty($rs[$value])) {
                foreach ($rs[$value] as &$v) {
                    $v['server_id'] = $value;
                    $tmp[] = $v;
                }
            }
        }

        $AllPackage = (new PackageModel())->getAllPackageName();
        $AllServer = (new ServerModel())->getAllServerName();

        $rs = array();
        foreach ($tmp as $k => $row) {
            $rs[$k][0] = $AllPackage[$row['package']];
            $rs[$k][1] = $AllServer[$row['server_id']];
            $rs[$k][2] = CDict::$questionType[$row['type']];
            $rs[$k][3] = $row['name'];
            $rs[$k][4] = $row['money'] ? $row['money'] : 0.00;
            $rs[$k][5] = $row['content'];
            $rs[$k][6] = date('Y-m-d H:i:s', $row['create_time'] / 1000);;
        }

        $result = [];

        $result[] = array('渠道', '服务器', '类型', '角色名', '充值金额', '内容', '提交时间');

        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result , '','意见反馈（' . date('Y年m月d日') . '）.csv');
    }


    public function find_reply($fid, $server_id) {
        $sql = "select id, fid, reply, reply_user, create_time from ny_feedback_reply where fid = {$fid} ";

        $arr  = array('query' => array('data' => array($sql)));

        $server_id = array($server_id);

       
        $res  = (new ServerconfigModel())->makeHttpParams($server_id, $arr);

        $urls = $res['urls'];
        $params = $res['params'];

        $res = Helper::rolling_curl($urls, $params);

        $tmp = array();
        foreach ($res as $serverId => $data) {
            $arr = json_decode($data, true);

            $code = $arr['code'];

            if ($code == 1) {
                $query = $arr['data']['query'];

                foreach ($query[0] as $key => $row) {
                    $row['server_id'] = $serverId;
                    $tmp[] = $row;
                }
            }

        }

        return $tmp;
    }

    public function post_reply($server_id, $fid, $text, $role_id, $type, $uuid)
    {
        // $server = $this->getGm(array(), array($server_id));

        $server = (new ServerconfigModel())->getConfig($server_id, ['server_id', 'websocket_host', 'websocket_port']);

        if (empty($server) || $server['websocket_host'] == '' || $server['websocket_port'] == '') {
            exit(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
        }

        $time = time();

        try {

            $time *= 1000;

            $reply = [$uuid, $text, $time];

            $msg = $this->socketCall($server['websocket_host'], $server['websocket_port'], 'sendFeedBackReply', array('opcode' => Pact::$FeebackReply, 'str' => $reply));

            //@todo 成功
            if ($msg === 0) {
                $insert = array(
                    'fid' => $fid,
                    'reply' => $text,
                    'reply_user' => $_SESSION['username'],
                    'create_time' => $time,
                );

                $arr  = array(
                    'insert' => array('data' => $insert, 'table' => 'feedback_reply'),
                    'update' => array('data' => array('status' => 1), 'table' => 'feedback', 'where' => array('id' => $fid)),
                );

                $server_id = array($server_id);
       
                $res  = (new ServerconfigModel())->makeHttpParams($server_id, $arr);

                $urls = $res['urls'];
                $params = $res['params'];

                $res = Helper::rolling_curl($urls, $params);
            }
            return $msg === 0 ? 0 : -1;
        } catch(Exception $e) {
           return $e->getMessage();
        }

    }

    public function roleHistory($role_id, $server_id)
    {

        $sql = "select f.id as id, f.account as account, f.uuid as uuid, f.name as name, f.role_id as role_id, f.type as type, f.content as content, f.create_time as create_time, f.status as status, fr.create_time as reply_time, fr.reply_user as reply_user, fr.reply as reply from ny_feedback as f left join ny_feedback_reply as fr on f.id = fr.fid where f.role_id = {$role_id} order by create_time desc ";

        $arr = array('query' => array('data' => array($sql)));

        $res = (new ServerconfigModel())->makeHttpParams([$server_id], $arr);

        $urls = $res['urls'];
        $params = $res['params'];

        $res = Helper::rolling_curl($urls, $params);

        $tmp = array();
        foreach ($res as $serverId => $data) {
            $arr = json_decode($data, true);

            $code = $arr['code'];

            if ($code == 1) {
                $query = $arr['data']['query'];

                foreach ($query[0] as $key => $row) {
                    $row['server_id'] = $serverId;
                    $tmp[] = $row;
                }
            }
        }

        $rs = array();
        $num = count($tmp);
        foreach ($tmp as $k => &$value) {
            $type = $value['type'];
            $value['type'] = CDict::$questionType[$value['type']];
            $value['create_time'] = date('Y-m-d H:i:s', $value['create_time'] / 1000);
            $value['reply'] = $value['status'] && $value['reply'] ? $value['reply'] : '无';
            $value['reply_time'] = $value['status'] && $value['reply_time'] ? date('Y-m-d H:i:s', $value['reply_time'] / 1000) : '无';
            $value['reply_user'] = $value['status'] && $value['reply_user'] ? $value['reply_user'] : '无';
            $value['status'] = $value['status'] ? '是' : '否';
        }

        return $tmp;

    }
}