<?php

class FollownewModel extends Model{
    public function __construct(){
        parent::__construct('follow_new');
        $this->alias = 'fln';
    }

    public function follow_new_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Follownew' , 'getFollownewData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function follow_new_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Follownew' , 'getFollownewData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , '新增充值人数' , '新增充值总额' , '1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '新增后续充值（' . date('Y年m月d日H时i分s秒') . '）');
    }
}