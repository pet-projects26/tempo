<?php

class GiftModel extends Model
{
    public function __construct()
    {
        parent::__construct('gift');
        $this->alias = 'g';
    }

    public function check_gift_acton($conditions)
    {
        $rs = array();
        $num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if (isset($conditions['WHERE']['server::IN'])) {

            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['fields'] = array('id', 'code', 'gift_id', 'role_name', "from_unixtime(create_time/1000,'%Y-%m-%d %H:%i:%s') as create_time");
            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Giftlog', 'gift_data', array('conditions' => $conditions), $value);
                foreach ($rs[$value][0] as $value) {
                    $rsArr[] = $value;
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }

        }
        $this->setPageData($rs, $num, $limit);
    }

    public function check_gift_export($conditions)
    {
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if (isset($conditions['WHERE']['server::IN'])) {

            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['fields'] = array('id', 'code', 'gift_id', 'role_name', "from_unixtime(create_time/1000,'%Y-%m-%d %H:%i:%s') as create_time");
            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Giftlog', 'gift_data', array('conditions' => $conditions), $value);
                foreach ($rs[$value][0] as $value) {
                    $rsArr[] = $value;
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }

        }
        $result = array();
        $result[] = array('编号', '激活码', '礼包id', '角色名', '领取时间');
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportCsv($result, '激活码领取记录（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function record_data($conditions)
    {

        $data = $this->get_record_data($conditions);

        $this->setPageData($data[0], $data[1]);
    }

    public function record_export($conditions)
    {
        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $data = $this->get_record_data($conditions, 1);

        $rs = $data[0];

        $result = array();
        $result[] = array(
            'ID', '说明', '使用次数', '类型', '生效时间', '过期时间', '服务器',
            '生成个数', '追加个数', '添加者', '添加时间', '发布'
        );
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '礼包列表（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function get_record_data($conditions, $isExport = 0)
    {
        $fields = array(
            'id', 'agent', 'name', 'info', 'tips', 'times', 'type', 'open_time', 'end_time',
            'server', 'item', 'sort', 'status', 'num', 'pnum', 'user', 'create_time', 'status', 'ischeck'
        );
        $conditions['WHERE']['ischeck::<'] = 2;
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $count = $this->getCount();
        $now = time();
        foreach ($rs as $k => $row) {
            if ($isExport) {
                $release = $row['ischeck'] ? '已发布' : '未发布';
                $status = !empty($row['status']) ? '否' : '是';
            } else {
                $release = $row['ischeck'] ? '<button class="btn btn-success">已发布</button>' : '<input type="button" class="gbutton" value="同意" onclick="_check(1,' . $row['id'] . ');"> | <input type="button" class="gbutton" value="拒绝" onclick="_check(2,' . $row['id'] . ');">';
                $status = !empty($row['status']) ? '<button class="btn btn-default">否</button>' : '<button class="btn btn-success">是</button>';
            }

            $server = json_decode($row['server'], true);
            if ($row['agent'] == '') {
                $channelGroup = [];
            } else {
                $channelGroup = explode(',', $row['agent']);
            }

            $serverList = $this->getServer($server, $channelGroup);

            $server = implode(',', $serverList);

            if ($row['ischeck']) {
                $more = '<input type="button" class="gbutton" value="追加" onclick="_more(' . $row['id'] . ');"> ';

                $forbid = $row['status'] ? '<input type="button" class="gbutton" value="禁用" onclick="_forbid(1,' . $row['id'] . ')"> ' : '<input type="button" class="gbutton" value="恢复" onclick="_forbid(0,' . $row['id'] . ');">';

                $operate = '<input type="button" class="gbutton" value="编辑" onclick="_edit(' . $row['id'] . ')"> ' . '<input type="button" class="gbutton" value="导出" onclick="_export_select(' . $row['id'] . ');"> ';

                $operate .= $more . $forbid;
            } else {
                $operate = '<input type="button" class="gbutton" value="编辑" onclick="_edit(' . $row['id'] . ')"> ';
            }

            if (!$isExport) {
                $rs[$k] = array(
                    $row['id'],
                    //$row['name'],
                    $row['info'],
                    //$row['tips'],
                    $row['times'],
                    CDict::$giftCDKEYType[$row['type']],
                    date('Y-m-d', $row['open_time']),
                    date('Y-m-d', $row['end_time']),
                    $server,
                    '<input type="button" class="gbutton" value="查看" onclick="_item(' . $row['id'] . ');"> ',
                    //$row['sort'],
                    $row['num'],
                    $row['pnum'],
                    $row['user'],
                    date('Y-m-d H:i:s', $row['create_time']),
                    $release,
                    $status,
                    $operate

                );
            } else {
                $rs[$k] = array(
                    $row['id'],
                    $row['info'],
                    $row['times'],
                    CDict::$giftCDKEYType[$row['type']],
                    date('Y-m-d', $row['open_time']),
                    date('Y-m-d', $row['end_time']),
                    $server,
                    $row['num'],
                    $row['pnum'],
                    $row['user'],
                    date('Y-m-d H:i:s', $row['create_time']),
                    $release,
                    $status
                );
            }

        }

        return [$rs, $count];
    }


    public function _export($id, $num)
    {
        $gift = $this->getGift($id, array('name'));
        $name = $gift['name'];
        //$gid = $this->formatGid($id);
        $gift = file_get_contents(ROOT . '/export/gift/' . $id . '.txt');
        $gift = json_decode($gift, true);
        $result = array();
        $result[] = array('兑换码');
        if ($num == -1) {
            $name .= '（全部）';
            foreach ($gift as $k => $row) {
                foreach ($row as $code) {
                    $result[] = array($code . ' ');
                }
            }
        } else {
            $name .= $num == 0 ? '（初次导入）' : '（第' . $num . '次追加）';
            foreach ($gift[$num] as $k => $code) {
                $result[] = array($code . ' ');
            }
        }
        Util::exportExcel($result, $name . '（' . date('Y年m月d日H时i分s秒') . '）');
    }

    //生成礼包码 , $num是生成个数
    public function generateCode($gift)
    {
        $exchange = array();

        $gid = $gift['id'];

        $filename = ROOT . '/export/gift/' . $gid . '.txt';

        $first = array();

        $key = $gid * 1000000;

        $CDKey = new CDKey();
        //新增
        for ($i = 1; $i <= $gift['num']; $i++) {
            $code = $CDKey->encry($key);
            $key ++;
            $first[] = $code;
        }

        $gift['code'] = $first;

        file_put_contents($filename, json_encode(array(0 => $first)));

        return $first;
    }

    public function addCode($gift, $pnum)
    {
        $more = array();
        //$gid = $this->formatGid($gift['id']);
        $gid = $gift['id'];
        $CDKey = new CDKey();

        $key = $gid * 1000000 + $gift['num'] + $gift['pnum'];

        for ($i = 1; $i <= $pnum; $i++) {
            //$code = $this->formatCode($gid);
            $more[] = $CDKey->encry($key);
            $key++;
        }
        $gifts = file_get_contents(ROOT . '/export/gift/' . $gid . '.txt');
        $gifts = json_decode($gifts, true);
        $gifts[] = $more;
        file_put_contents(ROOT . '/export/gift/' . $gid . '.txt', json_encode($gifts));
        return $more;
    }

    public function addGift($data)
    {
        if ($data) {
            return $this->add($data);
        }
    }

    public function getGift($id, $fields = array())
    {
        empty($fields) && $fields = array(
            'id', 'agent', 'name', 'info', 'tips', 'times', 'type', 'server', 'open_time',
            'end_time', 'item', 'sort', 'status', 'num', 'pnum', 'user', 'create_time', 'ischeck'
        );
        $conditions = array();
        $conditions['WHERE'] = array();
        $id && $conditions['WHERE']['id'] = $id;
        return $this->getRow($fields, $conditions['WHERE']);
    }

    /**
     * 返回服务器列表
     * @param  [type] $server       [description]
     * @param  [type] $channelGroup [description]
     * @return [type]               [description]
     */
    public function getServer($server, $channelGroup)
    {
        $Model = new Model();
        $serverList = array();

        $where = "1 and type !=2 ";// and display = 1 and status not in (0 , -1)

        if (empty($server) && empty($channelGroup)) {
            $sql = "SELECT server_id, name FROM ny_server WHERE {$where} ";

            $res = $Model->query($sql);

            foreach ($res as $row) {
                $sign = $row['server_id'];

                $serverList[$sign] = $row['name'];
            }

        } elseif (empty($server) && !empty($channelGroup)) {
            $str = implode(',', $channelGroup);

            $sql = "SELECT server_id, name FROM ny_server WHERE {$where} AND group_id in ({$str})";

            $res = $Model->query($sql);

            foreach ($res as $row) {
                $sign = $row['server_id'];

                $serverList[$sign] = $row['name'];
            }

        } elseif (!empty($server)) {

            $tmp = array();

            foreach ($server as $v) {
                $tmp[] = "'$v'";
            }

            $str = implode(',', $tmp);

            $sql = "SELECT server_id, name FROM ny_server WHERE {$where} AND server_id in ({$str})";

            $res = $Model->query($sql);

            foreach ($res as $row) {
                $sign = $row['server_id'];

                $serverList[$sign] = $row['name'];
            }

        }

        return $serverList;
    }

    /**
     * [同步兑换码] 说明，全渠道
     * @param array $serverMsg
     * @version 2017-09-29
     */
    public static function syncCode($serverMsg)
    {
        if (empty($serverMsg)) {
            return true;
        }
        //得到新开服数据
        $GiftModel = new GiftModel();
        $fields = ['id', 'agent', '`type`', 'open_time', 'end_time', 'item', 'times'];
        $res = $GiftModel->getRows($fields, ['ischeck' => 1, 'status' => 1, 'issync' => 1]);
        empty($res) && $res = array();
        if (empty($res)) return false;
        $data = [];

        $servers = [];

        //得到渠道下所有全服的兑换包
        foreach ($res as $row) {
            foreach ($serverMsg as $k => $value) {
                $giftAgent = explode(',', $row['agent']);

                if (in_array($k, $giftAgent) || $row['agent'] == '') {
                    foreach ($serverMsg[$k] as $server_id) {
                        $servers[$server_id][] = $row;
                    }
                }
            }
        }
        unset($row);

        if (empty($servers)) return false;

        $serverList = array_keys($servers);

        $instance = new ServerModel();
        $server_tmp_config = $instance->getServer($serverList, ['s.server_id as server_id', 's.name as name', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port']);

        if (empty($server_tmp_config)) return false;

        $server_config = [];

        foreach ($server_tmp_config as $s) {
            $server_config[$s['server_id']] = $s;
        }

        $itemSet = [];
        $backMsg = '';
        foreach ($servers as $key => $item) {

            foreach ($item as $row) {

                if (!array_key_exists($row['id'], $itemSet)) {
                    $itemSet[$row['id']] = json_decode($row['item'], true);
                }

                $sendItem = @$itemSet[$row['id']];

                if (empty($sendItem)) continue;

                $websocket_host = $server_config[$key]['websocket_host'];
                $websocket_port = $server_config[$key]['websocket_port'];

                $tmpMsg = 'gift_id:' . $row['id'] . ' | server_id:' . $key;

                $i = 1;

                //发送给后端
                $sendInfo = [];
                $sendInfo['opcode'] = Pact::$CDKEY['Produce'];
                $CDKEY = [(int)$row['id'], null, (int)($row['open_time'] * 1000), (int)($row['end_time'] * 1000), $sendItem, (int)$row['times'], (int)$row['type']];
                $sendInfo['str'] = [$CDKEY];

                $msg = $GiftModel->socketCall($websocket_host, $websocket_port, 'giftCDKEY', $sendInfo);

                $i++;

                if ($msg === 0) {
                    $tmpMsg .= ' 发送成功';
                } else {
                    $tmpMsg .= ' 发送失败';
                }

                $backMsg .= $tmpMsg . PHP_EOL;
            }
        }
        if ($backMsg != '') {
            Helper::log($backMsg, 'syncGiftCode', 'info', 'server');//记录每次同步日志
        }
    }
}