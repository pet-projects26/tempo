<?php

class GuideModel extends Model
{

    public function __construct()
    {
        parent::__construct('guide');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }


        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) + 86399 : strtotime(date('Y-m-d', time())) + 86399;
        unset($conditions['WHERE']['create_time']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", 'active_num', 'one_guide_get_num', "guide_id", 'get_num', 'done_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['guide_id'];
                $data[$k][4] = $row['get_num'];
                $data[$k][5] = $row['done_num'];

                //完成率： 完成人数 / 领取人数 （小数点后两位，四舍五入）
                $data[$k][6] = $row['done_num'] ? @round($row['done_num'] / $row['get_num'], 2) : 0;

                //整体完成率： 完成人数 / 新增用户 （小数点后两位，四舍五入）
                $data[$k][7] = $row['done_num'] ? @round($row['done_num'] / $row['one_guide_get_num'], 2) : 0;
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {
        $rs = array();
        unset($conditions['Extends']['LIMIT']);


        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) + 86399 : strtotime(date('Y-m-d', time())) + 86399;
        unset($conditions['WHERE']['create_time']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", 'active_num', 'one_guide_get_num', "guide_id", 'get_num', 'done_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['guide_id'];
                $data[$k][4] = $row['get_num'];
                $data[$k][5] = $row['done_num'];

                //完成率： 完成人数 / 领取人数 （小数点后两位，四舍五入）
                $data[$k][6] = $row['done_num'] ? @round($row['done_num'] / $row['get_num'], 2) : 0;

                //整体完成率： 完成人数 / 新增用户 （小数点后两位，四舍五入）
                $data[$k][7] = $row['done_num'] ? @round($row['done_num'] / $row['one_guide_get_num'], 2) : 0;
            }
        }


        $result = array();
        $result[] = array('日期', '服务器', '新增用户数', '引导ID', '达到人数', '通过人数', '该引导通过率', '整体通过率');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '新手引导留存（' . date('Y年m月d日H时i分s秒') . '）');
    }


    public function getRowData($date, $server)
    {
        $conditions = [];

        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $rs = $this->getRow('id', $conditions['WHERE']);

        return $rs;
    }
}