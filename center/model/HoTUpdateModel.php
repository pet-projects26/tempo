<?php


class HoTUpdateModel extends Model
{
    public function __construct()
    {
        parent::__construct('hotupdate_log');
        $this->alias = 'a';
    }

    public function hupdate($data)
    {
        //$server = (new ServerModel())->getGm(array(),array($data['server']));
        $server_id = array_unique(array_filter($data['server']));
        $channel_group = array_unique(array_filter($data['channel_group']));
        $serverids = (new ServerModel)->getServerByServerAndChannelGroup($server_id, $channel_group, '1', 1);
        $serverids = array_keys($serverids);
        if (empty($serverids)) {
            exit(
            json_encode([
                'code' => 400,
                'msg' => '服务器id为空'
            ])
            );
        }
        $server_config = (new ServerconfigModel())->getConfig($serverids, ['server_id', 'websocket_host', 'websocket_port', 'bin_path']);

        $returnData = [];

        $time = time();

        foreach ($server_config as $key => $value) {
            if (!$value['websocket_host'] || !$value['websocket_port'] /*|| !$value['bin_path'] */) {
                $errorServer[] = $value['server_id'];
            }
        }

        $susServer = [];
        $errServer = [];

        if (!empty($errorServer)) {
            $errorServer = implode(',', $errorServer);

            $msg = '请检查下列服务器的单服配置'.PHP_EOL;
            $msg .= $errorServer;

            exit(
                json_encode([
                    'code' => 400,
                    'msg' => $msg
                ])
            );
        }

        foreach ($server_config as $key => $value) {

            $data[$value['server_id']]['websocket_host'] = $value['websocket_host'];
            $data[$value['server_id']]['websocket_port'] = $value['websocket_port'];

            $info = [];

            $info = array('opcode' => $data['type'], 'str' => [intval($data['netinfo'])]);

            $strArr = [];

            if ($data['type'] == '34603008') {

                foreach ($data['str'] as $k => $v) {
                   // $strArr[$k] = $this->formatBinPath($value['bin_path'], $v);
                    $strArr[$k] = $v;
                }

                array_push($info['str'], $strArr);
            }
            $res = $this->socketCall($value['websocket_host'], $value['websocket_port'], 'hupdate', $info);

            if ($res === 0) {
                $susServer[] = $value['server_id'];
                //添加进热更新流水
                $addData = [
                    'group' => $data['channel_group'][0],
                    'server' => $value['server_id'],
                    'type' => $data['type'],
                    'pact' => $data['netinfo'],
                    'admin' => $_SESSION['username'],
                    'ip' => getClientIp(0, true),
                    'send_data' => json_encode($info),
                    'create_time' => $time
                ];
                $this->add($addData);
            } else {
                $errServer[] = $value['server_id'];
            }
        }

        if (!empty($errServer)) {
            $msg = '发送结果:'.PHP_EOL;
            $errServer = implode(',', $errServer);
            $msg .= '失败服务器: '.$errServer.PHP_EOL;
            $susServer = !empty($susServer)  ? implode(',', $susServer) : '';
            $msg .= '成功服务器: '.$susServer.PHP_EOL;
            $json = array('state' => true, 'code' => 400, 'msg' => $msg);
            echo json_encode($json);
        } else {
            $msg = '发送结果:'.PHP_EOL;
            $susServer = !empty($susServer)  ? implode(',', $susServer) : '';
            $msg .= '成功服务器: '.$susServer.PHP_EOL;
            $json = array('state' => true, 'code' => 200, 'msg' => $msg);
            echo json_encode($json);
        }
    }

    public function record_data($conditions)
    {
        $num = 0;

        $data = [];

        if (!empty($conditions['WHERE']['package::IN'])) {
            unset($conditions['WHERE']['package::IN']);
        }

        if (!empty($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
        }

        $this->joinTable = array(
            'cg' => array('name' => 'channel_group', 'type' => 'LEFT', 'on' => 'cg.id = a.group'),
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server')
        );


        $arr = $this->getRows(['cg.name as group_name', 's.name as server_name', 'a.`type`', 'a.pact', 'a.admin', 'a.ip', 'a.send_data', 'a.create_time as create_time'] , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {

            $netInfo = array_flip(Pact::$HotUpdateType);

            foreach($arr as $k => $row){
                //转化渠道
                $arr[$k]['type'] = $row['type'] == 0x2100000 ? '热更新' : '重载配置';

                $arr[$k]['pact'] = $netInfo[$row['pact']];

                $arr[$k]['create_time'] = date('Y-m-d H:i:s', $row['create_time']);

            }
        }

        foreach($arr as $k=>$row){
            $data[$k]=array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function formatBinPath($binPath, $str)
    {

        $binPath = rtrim($binPath, '/') . '/';

        $str = trim($str, '/');

        return $binPath.$str;
    }
}