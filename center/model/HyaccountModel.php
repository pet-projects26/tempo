<?php
//活跃玩家
class HyaccountModel extends Model{
	
    public function __construct(){
        parent::__construct('hy_account');
//         $this->alias = 'cd';
    }

    public function hyRoles_data($conditions){
    	
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['time::>=']) && $conditions['WHERE']['time::>='] = date("Y-m-d",$conditions['WHERE']['time::>=']);
    		isset($conditions['WHERE']['time::<=']) && $conditions['WHERE']['time::<='] = date("Y-m-d",$conditions['WHERE']['time::<=']);
    		 
    		$fields = array('server','time' ,'new','old','total' ,'wau','mau');
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
//     		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		$aaData = [];
    		foreach($arr as $k => $row){
    			$aaData[$row['time']][0] = $row['time'];
    			$aaData[$row['time']][1] += $row['new'];
    			$aaData[$row['time']][2] += $row['old'];
    			$aaData[$row['time']][3] += $row['total'];
    			$aaData[$row['time']][4] += $row['wau'];
    			$aaData[$row['time']][5] += $row['mau'];
    		}
    		
    		$rs = array_values($aaData);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , 0);
    	
    }
    public function hyRoles_export($conditions){
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['time::>=']) && $conditions['WHERE']['time::>='] = date("Y-m-d",$conditions['WHERE']['time::>=']);
    		isset($conditions['WHERE']['time::<=']) && $conditions['WHERE']['time::<='] = date("Y-m-d",$conditions['WHERE']['time::<=']);
    		 
    		$fields = array('server','time' ,'new','old','total' ,'wau','mau');
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		//     		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		$aaData = [];
    		foreach($arr as $k => $row){
    			$aaData[$row['time']][0] = $row['time'];
    			$aaData[$row['time']][1] += $row['new'];
    			$aaData[$row['time']][2] += $row['old'];
    			$aaData[$row['time']][3] += $row['total'];
    			$aaData[$row['time']][4] += $row['wau'];
    			$aaData[$row['time']][5] += $row['mau'];
    		}
    		
    		$rs = array_values($aaData);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    		
    		$fields = array();
    		$fields[] = array('日期' , '新玩家(账户)' ,'老玩家(账户)','总计','WAU','MAU');
    		foreach($rs as $row){
    			$fields[] = $this->formatFields($row);
    		}
    		Util::exportExcel($fields , '活跃玩家数据（' . date('Y年m月d日H时i分s秒') . '）');
    	}
    }
    
}