 <?php
class InstantdataModel extends Model{

    public function __construct(){
        parent::__construct('role');
        $this->alias = 'r';
    }

	public function real_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $server_id = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $date = $conditions['WHERE']['time'] ? $conditions['WHERE']['time'] : date('Y-m-d');
        $time = strtotime($date);
        unset($conditions['WHERE']['time']);

        $rs = $rsArr = [];
        foreach ($server_id as $key => $value) {
            $server = $value;
            $rs = $this->call('Instantdata', 'getRealData', array('time' => $time), $value);
            if ($rs[$value]) {
                $rs[$value]['server'] = $server;
                $rsArr[] = $rs[$value];
            }
        }
        $rsArr1[0] = $rsArr;
        $rsArr1[1] = count($rsArr);

        $ActiveAccount = new ActiveAccountModel();

        $start_time = strtotime($date);
        $end_time = $start_time + 86399;

        !(empty($rsArr1)) && list($rs, $num) = $rsArr1;

        $sumRow = ['总计', 0, 0, 0, 0, 0, 0, 0];

        $count = count($rs);


        //$rs = empty($rs)?array():$rs;//如果内容为空，则置空
        foreach($rs as $k => $row){

            //根据服务器id查找今天的活跃人数
            $todayActive = $ActiveAccount->getActiveAccountNum($start_time, $end_time, $row['server']);

            $serConf = (new ServerModel())->getServer($row['server'], ['num', 'name']);
            $serverName = $serConf ? 'S' . $serConf['num'] . '-' . $serConf['name'] : '未知';

            $rs[$k][0] = $serverName;
            $rs[$k][1] = $row['role_num'];
            $rs[$k][2] = $row['online_num'];
            $rs[$k][3] = $todayActive ? $todayActive : 0;
            $rs[$k][4] = $row['money'];
            $rs[$k][5] = $row['pay_num'];
            $rs[$k][6] =  $row['pay_num'] && $rs[$k][3] ? @sprintf("%.2f" , $row['pay_num'] /$rs[$k][3] * 100) . '%' : '0%';//付费率
            $rs[$k][7] = $row['money'] && $row['pay_num'] ?  @round($row['money'] / $row['pay_num'], 2) : 0.00;

            if ($count > 1) {
                $sumRow[1] += $row['role_num'];
                $sumRow[2] += $row['online_num'];
                $sumRow[3] += $todayActive;
                $sumRow[4] += $row['money'];
                $sumRow[5] += $row['pay_num'];
            }
        }

        $data = [];
        if ($count > 1) {
            $sumRow[6] = $sumRow[5] && $sumRow[3] ? @sprintf("%.2f" ,  $sumRow[5] / $sumRow[3] * 100) . '%' : '0%';//付费率
            $sumRow[7] = $sumRow[4] && $sumRow[6] ? @round($sumRow[4] / $sumRow[5], 2) : 0.00;
            $data[] = $sumRow;
        }

        $data = array_merge($data, $rs);


        $this->setPageData($data , $num , $limit);
	}
}