<?php

class ItemModel extends Model{

    public function __construct(){
        parent::__construct('item');
        $this->alias = 'i';
    }

    public function record_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Item' , 'getItemData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }

    public function record_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Item' , 'getItemData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $name = CDict::$coinType[$conditions['WHERE']['type']]['name'];
        $result = array();
        $result[] = array(
            '日期' , '产生' . $name , '参与产生数' ,  '消耗' . $name , '参与消耗数' ,'库存'. $name ,  '总产出', '总消耗' , '滞留率'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , $name . '产出消耗（' . date('Y年m月d日H时i分s秒') . '）');
    }
	
	public function pie_data($params){
		$scp=$params['scp'];
		$result=$this->call('Item','pie_data',array('params'=>$params),$scp);
		$result=$result[$scp];
		return $result;
	}
	
	public function pie_detail($params){
		$scp=$params['scp'];
		$result=$this->call('Item','pie_detail',array('params'=>$params),$scp);
		$result=$result[$scp];
		return $result;
	}
}