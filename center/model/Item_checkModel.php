<?php

class Item_checkModel extends Model{

    public function __construct(){
        parent::__construct('item_check');
        $this->alias = 'c';
    }

    public function getItemCheck($id = '' , $fields = array()){
        empty($fields) && $fields = array('*');
        $conditions = array();
        if(is_array($id)){
            $id && $conditions['WHERE']['id::IN'] = $id;
            $row = $this->getRows($fields , $conditions['WHERE']);
            return $row;
        }
        else{
            $id && $conditions['WHERE']['id'] = $id;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

   
}