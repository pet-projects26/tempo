<?php

class ItemcheckresultModel extends Model{
    public function __construct(){
        parent::__construct('itemcheck_result');
        $this->alias = 'o';
    }
    
 

    public function record_data($conditions){
       // print_r($conditions);exit;
        $rs = array();$num = 0;
//         if(isset($conditions['WHERE']['server::IN'])  ||  isset($conditions['WHERE']['package::IN'])){
           if(isset($conditions['WHERE']['server::IN'])) parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            isset($conditions['WHERE']['create_time::>=']) && $conditions['WHERE']['o.create_time::>='] = $conditions['WHERE']['create_time::>='];
            isset($conditions['WHERE']['create_time::<=']) && $conditions['WHERE']['o.create_time::<='] = $conditions['WHERE']['create_time::<='];
            unset($conditions['WHERE']['create_time::>=']);
            unset($conditions['WHERE']['create_time::<=']);
			
            $this->joinTable = array(
                's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
            );
			$fields = array( 'coin_type' , 'check_count','count','coin','s.name as server' ,'o.type AS type','item_id',
				'role_id','role_name',"from_unixtime(o.create_time,'%Y-%m-%d %H:%i:%s') as create_time" , 'dayu_count','get_source',
			);
			
			if (!$conditions['WHERE']['coin_type']) {
				$conditions['WHERE']['coin_type'] = 1;
			}
			
			$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
//             echo $this->lastQuery();exit;
			$num = $this->getCount();
			$coin_type = ['1'=>'元宝','2'=>'绑元','4'=>'内部元宝'];
			
			$m = new Model();
			$sql="select server_id from ny_server where status  not in ('-1' ,'0') and display = 1 limit 1";
			$ser = $m->query($sql);
			$AllItem = $m->call('Gift' , 'getItem' , array() , $ser[0]['server_id']);//全部物品
			$goods = $AllItem[$ser[0]['server_id']];
			
			
			foreach($arr as $k => $row){
				$get_source =  <<< END
        		<a href="javascript:void(0)" onclick="logup('{$row['get_source']}')" style="text-decoration: underline;">点击进入查看</a>
END;
				if ($row['coin_type'] != 3) {
					$rs[$k]['coin_type'] = $coin_type[$row['coin_type']];
					$count = '<span style="color:red;">'.$row['coin'].'</span>';
				}else {
					$goods_id = $row['item_id'];
					$name = isset($goods[$goods_id]) ? $goods[$goods_id] : '';
					$rs[$k]['coin_type']  = $row['item_id'] . '/'.$name;
					$count = '<span style="color:red;">'.$row['count'].'</span>';
				}
				
                $rs[$k]['check_count'] = $row['check_count'];
                $rs[$k]['count'] = $count;
                $rs[$k]['get_source'] = $get_source;
                $rs[$k]['server'] = $row['server'];
                $rs[$k]['role_id'] = $row['role_id'];
                $rs[$k]['role_name'] = $row['role_name'];
                $rs[$k]['dayu_count'] = '<span style="color:red;">'.$row['dayu_count'].'</span>';
				$rs[$k]['create_time'] = $row['create_time'];
			}
			foreach($rs as $k=>$row){
				$rs[$k]=array_values($row);
			}
// 		}
        $this->setPageData($rs , $num); 
    }
  

}