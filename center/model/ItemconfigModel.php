<?php

class ItemconfigModel extends Model{

    public function __construct(){
        parent::__construct('item_config');
        $this->alias = 'ic';
    }

    public function record_data($conditions){
        $fields = array('id','bind_item_id' , 'item_id' , 'item_name' , 'item_value' , 'item_info' , 'item_produce' , 'item_advice');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
       
        foreach($rs as $k => $row){
            $rs[$k]['caozuo'] = 
                
                '<input type="button" class="gbutton" onclick="edit(\'' . $row['id'] . '\')" value="编辑">
                <input type="button" class="gbutton" onclick="del(\'' . $row['id'] . '\')" value="删除">';
            
        }

        foreach ($rs as $key => $value) {
            $rs[$key] = array_values($value);
        }

        $this->setPageData($rs , $this->getCount());
    }

    public function record_export($conditions){

         $fields = array('bind_item_id' , 'item_id' , 'item_name' , 'item_value' , 'item_info' , 'item_produce' , 'item_advice');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        
        $fields = array();
        $fields[] = array('绑定ID', '不绑定ID', '道具名称', '道具价值', '描述', '非活动的产出途径', '投放方式建议' );
        foreach($rs as $row){
            $fields[] = $this->formatFields($row);
        }
        
        Util::exportExcel($fields , '道具表');
    }

}