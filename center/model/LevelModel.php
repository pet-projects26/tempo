<?php

class LevelModel extends Model{

    public function __construct(){
        parent::__construct('level');
        $this->alias = 'lv';
    }

    public function level_loss_data($conditions){

        $rss = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            //$psId = Helper::psIdByServerid($server_id);
            /*$rs = $this->call('Level' , 'getLevelLossData' , array('conditions'=>array(
                'date' => $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"],
                'package'=>$conditions['WHERE']['package::IN'],
                'psId'=>array_values($psId)
            )) , $server_id);*/
            $rs = $this->call('Level','getLevelLossData',array('date'=>$conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"]),$server_id);
            $data = $this->callDataMerge($rs);
            !empty($data) && list($rs,$num) = $data;
            foreach($rs as $row){
                $rss[] = array_values($row);
            }
            /*foreach($rs as $level => $row){
                $temp = [];
                $temp[] = $level;
                $temp[] = Helper::setTips($row,'num');
                $temp[] = Helper::setTips($row,'num_percent');
                $temp[] = Helper::setTips($row,'loss_num');
                $temp[] = Helper::setTips($row,'loss_percent');
                array_push($rss,$temp);
            }*/
        }
        $this->setPageData($rss , $num , $limit);
    }
    public function level_loss_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Level' , 'getLevelLossData' , array('date' => $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"]) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('等级' , '等级人数' , '比例' , '等级流失人数' , '等级流失率');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '等级流失（' . date('Y年m月d日H时i分s秒') . '）');
    }
}