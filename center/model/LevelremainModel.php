<?php

class LevelremainModel extends Model{

    public function __construct(){
        parent::__construct('level_remain');
        $this->alias = 'o';
    }

    public function level_data($conditions)
    {

        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = isset($conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"]) ? $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] : date('Y-m-d');
        $conditions['WHERE']['o.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);
        unset($conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"]);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
        );
        $fields = array("o.create_time as create_time", "s.name as server", "new_num", "level", 'level_count', 'reach_num', 'pass_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {

                $data[$k][0] = date('Y-m-d', $row['create_time']);

                $data[$k][1] = $row['server'];

                $data[$k][2] = $row['new_num'];

                $data[$k][3] = $row['level'];

                $data[$k][4] = $row['reach_num'];

                $data[$k][5] = $row['pass_num'];

                $data[$k][6] = $row['level_count'];

                //本级通过率
                $data[$k][7] = $row['pass_num'] && $row['reach_num'] ? @sprintf("%.2f", $row['pass_num'] / $row['reach_num'] * 100) . '%' : '0%';

                //整体达成率
                $data[$k][8] = $row['reach_num'] && $row['new_num'] ? @sprintf("%.2f", $row['reach_num'] / $row['new_num'] * 100) . '%' : '0%';

            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function level_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = isset($conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"]) ? $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] : date('Y-m-d', strtotime(date('Y-m-d') - 86400));
        $conditions['WHERE']['o.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);
        unset($conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"]);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
        );
        $fields = array("o.create_time as create_time", "s.name as server", "new_num", "level", 'level_count', 'reach_num', 'pass_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {

                $data[$k][0] = date('Y-m-d', $row['create_time']);

                $data[$k][1] = $row['server'];

                $data[$k][2] = $row['new_num'];

                $data[$k][3] = $row['level'];

                $data[$k][4] = $row['reach_num'];

                $data[$k][5] = $row['pass_num'];

                $data[$k][6] = $row['level_count'];

                //本级通过率
                $data[$k][7] = $row['pass_num'] && $row['reach_num'] ? @sprintf("%.2f", $row['pass_num'] / $row['reach_num'] * 100) . '%' : '0%';

                //整体达成率
                $data[$k][8] = $row['reach_num'] && $row['new_num'] ? @sprintf("%.2f", $row['reach_num'] / $row['new_num'] * 100) . '%' : '0%';

            }
        }
        $result = array();
        $result[] = array('日期', '服务器', '新增用户数', '等级', '达到人数', '通过人数', '停留人数', '本级通过率', '整体达成率');

        foreach($data as $row){
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result , '','等级留存（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}