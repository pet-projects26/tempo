<?php

class LogModel extends Model{
    public function __construct(){
		$this->c= new ClientModel();
    }
	
	public function getItem(){
		$server=$_SESSION['server'];
		$memache=Util::memcacheConn();
		$item=$memache->get($server.'item');
		if(!$item){
			$item=$this->call('Log','getItem',array(),$server);
			$memache->set($server.'item',$item[$server],0,7*24*3600);
		}
		$memache->close();
	}
	//等级流水
	public function level_data($conditions){
		$rs=array();$num=0;

		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}

		$limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
            $rs=$this->call('Log' , 'level_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
			foreach($rs as $k => $row){
            	$rs[$k] = array_values($row);
        	}
        }
       
		$this->setPageData($rs , $num,$limit);	
	}
	//等级流水导出
	public function level_export($conditions){
		$rs=array();$num=0;

		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}

		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$rs=$this->call('Log' , 'level_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
			$result = [];
			$result[] = array('等级','时间');
			foreach($rs as $k=>$row){
				$result[] = $this->formatFields($row);
			}

			Util::exportExcel($result,'等级流水（'.date('Y年m月d日H时i分s秒').'）');
		}
	}
	//道具流水
	public function prop_data($conditions){
		$server=$_SESSION['server'];
		$memache=Util::memcacheConn();
		$item=$memache->get($server.'item');
		$itemType=CDict::$itemType;
		if($conditions['WHERE']['item_id']){
			$item_id=$conditions['WHERE']['item_id'];
			unset($conditions['WHERE']['item_id']);
			foreach($item as $key=>$val){
				$arr["$val"][]=$key;
			}
			$i=implode("','",$arr["$item_id"]);
			$conditions['WHERE']['item_id::IN']=array($i);
		}

		$rs=array();$num=0;$arr=array();

		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}

		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'prop_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
			foreach($rs as $k=>$v){
				$source=$v['source'];
				$item_id=$v['item_id'];
				$arr[$k]['source']=$itemType[$source];
				$arr[$k]['item_id']=$item_id;
				$arr[$k]['item']=$item["$item_id"];

				if($v['type']==0){
					$arr[$k]['item_num']='-'.$v['item_num'];
				}else{
					$arr[$k]['item_num']='+'.$v['item_num'];
				}
				$arr[$k]['create_time']=$v['create_time'];
			}
			foreach($arr as $k => $row){
				$rs[$k] = array_values($row);
			}
        }
		$this->setPageData($rs , $num , $limit);
	}
	//道具流水
	public function prop_export($conditions){
		$server=$_SESSION['server'];
		$memache=Util::memcacheConn();
		$item=$memache->get($server.'item');
		$itemType=CDict::$itemType;
		if($conditions['WHERE']['item_id']){
			$item_id=$conditions['WHERE']['item_id'];
			unset($conditions['WHERE']['item_id']);
			foreach($item as $key=>$val){
				$arr["$val"][]=$key;
			}
			$i=implode("','",$arr["$item_id"]);
			$conditions['WHERE']['item_id::IN']=array($i);
		}

		$rs=array();$num=0;$arr=array();
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'prop_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
			foreach($rs as $k=>$v){
				$source=$v['source'];
				$item_id=$v['item_id'];
				$arr[$k]['source']=$itemType[$source];
				$arr[$k]['item_id']=$item_id;
				$arr[$k]['item']=$item["$item_id"];

				if($v['type']==0){
					$arr[$k]['item_num']='-'.$v['item_num'];
				}else{
					$arr[$k]['item_num']='+'.$v['item_num'];
				}
				$arr[$k]['create_time']=$v['create_time'];
			}
			$result = [];
			$result[] = array('类型','道具ID','道具','数量','时间');
			foreach($arr as $k=>$row){
				$result[] = $this->formatFields($row);
			}
			Util::exportExcel($result,'道具流水（'.date('Y年m月d日H时i分s秒').'）');
		}
	}
	//金钱流水
	public function money_data($conditions){
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'money_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
        }
		$itemType=CDict::$itemType;
		$money=CDict::$money;
		$arr=array();
		foreach($rs as $k=>$v){
			$source=$v['coin_source'];
			$money_type=$v['money_type'];
			$arr[$k]['source']=$itemType[$source];
			$arr[$k]['money_type']=$money[$money_type];
			
			$arr[$k]['copper']=$v['copper'];
			$arr[$k]['gold']=$v['gold'];
			$arr[$k]['bind_gold']=$v['bind_gold'];
			$arr[$k]['sorce']=$v['sorce'];
			$arr[$k]['bag_copper']=$v['bag_copper'];
			$arr[$k]['bag_gold']=$v['bag_gold'];
			$arr[$k]['bag_bind_gold']=$v['bag_bind_gold'];
			$arr[$k]['bag_sorce']=$v['bag_sorce'];
			if($v['type']==0){
				$flag='-';
			}else{
				$flag='+';
			}
			if($money_type==1){
				$arr[$k]['copper']=$flag.$arr[$k]['copper'];	
			}elseif($money_type==2){
				$arr[$k]['gold']=$flag.$arr[$k]['gold'];		
			}elseif($money_type==3){
				$arr[$k]['bind_gold']=$flag.$arr[$k]['bind_gold'];		
			}elseif($money_type==4){
				$arr[$k]['sorce']=$flag.$arr[$k]['sorce'];		
			}
			$arr[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
		}
	
		foreach($arr as $k => $row){
            $rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num , $limit);
	}
	//金钱流水导出
	public function money_export($conditions){
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'money_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
		}
		$itemType=CDict::$itemType;
		$money=CDict::$money;
		$arr=array();
		foreach($rs as $k=>$v){
			$source=$v['coin_source'];
			$money_type=$v['money_type'];
			$arr[$k]['source']=$itemType[$source];
			$arr[$k]['money_type']=$money[$money_type];

			$arr[$k]['copper']=$v['copper'];
			$arr[$k]['gold']=$v['gold'];
			$arr[$k]['bind_gold']=$v['bind_gold'];
			$arr[$k]['sorce']=$v['sorce'];
			$arr[$k]['bag_copper']=$v['bag_copper'];
			$arr[$k]['bag_gold']=$v['bag_gold'];
			$arr[$k]['bag_bind_gold']=$v['bag_bind_gold'];
			$arr[$k]['bag_sorce']=$v['bag_sorce'];
			if($v['type']==0){
				$flag='-';
			}else{
				$flag='+';
			}
			if($money_type==1){
				$arr[$k]['copper']=$flag.$arr[$k]['copper'];
			}elseif($money_type==2){
				$arr[$k]['gold']=$flag.$arr[$k]['gold'];
			}elseif($money_type==3){
				$arr[$k]['bind_gold']=$flag.$arr[$k]['bind_gold'];
			}elseif($money_type==4){
				$arr[$k]['sorce']=$flag.$arr[$k]['sorce'];
			}
			$arr[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
		}
		$result = [];
		$result[] = array('类型','金钱类型','变动的铜钱','变动的元宝','变动的绑元','变动的积分','剩余的铜钱','剩余的元宝','剩余的绑元','剩余的积分','时间');
		foreach($arr as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'金钱流水（'.date('Y年m月d日H时i分s秒').'）');
	}
	//坐骑流水
	public function ride_data($conditions){
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
            $rs = $this->call('Log' , 'ride_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
        }
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num ,$limit);
	}
	//坐骑流水导出
	public function ride_export($conditions){
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'ride_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
		}
		$result = [];
		$result[] = array('阶级','等级','时间');
		foreach($rs as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'坐骑流水（'.date('').'）');
	}
	//任务流水
	public function task_data($conditions){
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
            $result = $this->call('Log' , 'task_data' , array('conditions' => $conditions) , $server_id);
			
			list($rs , $num) = $this->callDataMerge($result);
        }
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num ,$limit);
	}
	//任务流水导出
	public function task_export($conditions){
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$result = $this->call('Log' , 'task_data' , array('conditions' => $conditions) , $server_id);

			list($rs , $num) = $this->callDataMerge($result);
		}
		$result = [];
		$result = array('任务ID','任务类型','任务名称','状态','时间');
		foreach($rs as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'任务流水（'.date('Y年m月d日H时i分s秒').'）');
	}
	//登录流水
	public function login_data($conditions){

		$rs=array();$num=0;
		if(empty($conditions['WHERE']['start_time::>='])){$conditions['WHERE']['start_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['start_time::<='])){$conditions['WHERE']['start_time::<=']=time();}

		
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
            $rs = $this->call('Log' , 'login_data' , array('conditions' => $conditions) , $server_id);
           
			list($rs , $num) = $this->callDataMerge($rs);
			
        }
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num ,$limit);
	}
	//登录流水导出
	public function login_export($conditions){

		$rs=array();$num=0;
		if(empty($conditions['WHERE']['start_time::>='])){$conditions['WHERE']['start_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['start_time::<='])){$conditions['WHERE']['start_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'login_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
		}
		$result = [];
		$result[] = array('等级','IP','登出时间','在线时长');
		foreach($rs as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'登录流水（'.date('Y年m月d日H时i分s秒').'）');
	}
	
	public function client_data($conditions){
		$rs=array();$num=0;

		//if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=time()-86400;}
		//if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$fields=array('channel','package','model','network','mac','udid','memory',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$rs=$this->c->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num );
	}
	public function client_export($conditions){
		$rs=array();$num=0;
		$fields=array('channel','package','model','network','mac','memory',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$rs=$this->c->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		$result = [];
		$result[] = array('渠道','包','机型','网络类型','mac','memory','时间');
		foreach($rs as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'客户端流水（'.date('Y年m月d日H时i分s秒').'）');
	}
	//装备强化流水
	public function equip_data($conditions){

		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}

		$rs=array();$num=0;
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
			
            $rs = $this->call('Log' , 'equip_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
        }
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num ,$limit);
	}
	//装备强化流水导出
	public function equip_export($conditions){

		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}

		$rs=array();$num=0;
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);

			$rs = $this->call('Log' , 'equip_data' , array('conditions' => $conditions) , $server_id);
			list($rs , $num) = $this->callDataMerge($rs);
		}
		$result = [];
		$result[] = array('部位','等级','时间');
		foreach($rs as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'装备强化流水('.date('Y年m月d日H时i分s秒').')');
	}
	//装备替换流水
	public function equip_replace_data($conditions){
		$server=$_SESSION['server'];
		$memache=Util::memcacheConn();
		$item=$memache->get($server.'item');
		
		
		if($conditions['WHERE']['item_id']){
			$item_id=$conditions['WHERE']['item_id'];
			unset($conditions['WHERE']['item_id']);
			foreach($item as $key=>$val){
				$arr["$val"][]=$key;
			}
			$i=implode("','",$arr["$item_id"]);
			$conditions['WHERE']['item_id::IN']=array($i);
		}
		
		
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
            $rs = $this->call('Log' , 'equip_replace_data' , array('conditions' => $conditions) , $server_id);
			$data = $this->callDataMerge($rs);
			!(empty($data))&&list($rs,$num) = $data;
			//list($rs , $num) = $this->callDataMerge($rs);
        }
		$arr = [];
		foreach($rs as $k=>$v){
			if($v['type']==0){
				$arr[$k]['type']='穿上装备';	
			}else{
				$arr[$k]['type']='脱下装备';
			}
			$item_id=$v['item_id'];
			$arr[$k]['item_id']=$item_id;
			$arr[$k]['item']=$item["$item_id"];
			$arr[$k]['create_time']=$v['create_time'];	
		}
        foreach($arr as $k => $row){
            $arr[$k] = array_values($row);
        }
		$this->setPageData($arr , $num,$limit );
		
	}
	//装备替换流水
	public function equip_replace_export($conditions){
		$server=$_SESSION['server'];
		$memache=Util::memcacheConn();
		$item=$memache->get($server.'item');


		if($conditions['WHERE']['item_id']){
			$item_id=$conditions['WHERE']['item_id'];
			unset($conditions['WHERE']['item_id']);
			foreach($item as $key=>$val){
				$arr["$val"][]=$key;
			}
			$i=implode("','",$arr["$item_id"]);
			$conditions['WHERE']['item_id::IN']=array($i);
		}
		$rs=array();$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);
			$rs = $this->call('Log' , 'equip_replace_data' , array('conditions' => $conditions) , $server_id);
			$data = $this->callDataMerge($rs);
			!(empty($data))&&list($rs,$num) = $data;
			//list($rs , $num) = $this->callDataMerge($rs);
		}
		$arr = [];
		foreach($rs as $k=>$v){
			if($v['type']==0){
				$arr[$k]['type']='穿上装备';
			}else{
				$arr[$k]['type']='脱下装备';
			}
			$item_id=$v['item_id'];
			$arr[$k]['item_id']=$item_id;
			$arr[$k]['item']=$item["$item_id"];
			$arr[$k]['create_time']=$v['create_time'];
		}
		$result = [];
		$result[] = array('类型','装备ID','装备','时间');
		foreach($arr as $k=>$row){
			$result[] = $this->formatFields($row);
		}
		Util::exportExcel($result,'装备替换流水（'.date('Y年m月d日H时i分s秒').'）');
	}
	//战斗力流水
	public function ce_data($conditions){
		$rs=array();
		$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
			
            $result = $this->call('Log' , 'ce_data' , array('conditions' => $conditions) , $server_id);
            $result = $result[$server_id];
            $num = count($num);
        }
        if(!empty($result)){
        	foreach($result as $k => $row){
            	$rs[$k] = array_values($row);
        	}
        }
        
		$this->setPageData($rs , $num ,$limit);
	}
	//战斗力流水导出
	public function ce_export($conditions){
		$rs=array();
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		unset($conditions['Extends']['LIMIT']);
		$result = [];
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);

			$result = $this->call('Log' , 'ce_data' , array('conditions' => $conditions) , $server_id);
			$result = $result[$server_id];
		}
		$res = [];
		$res[] = array('战斗力','时间');
		foreach($result as $k=>$row){
			$res[] = $this->formatFields($row);
		}
		Util::exportExcel($res,'战斗力流水（'.date('Y年m月d日H时i分s秒').'）');
	}
	//宝石流水
	public function gem_data($conditions){
		$rs=array();
		$num=0;
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		$limit = $conditions['Extends']['LIMIT'];
		unset($conditions['Extends']['LIMIT']);
		if(isset($conditions['WHERE']['server'])){
            $server_id = $conditions['WHERE']['server'];
            unset($conditions['WHERE']['server']);
			
            $result = $this->call('Log' , 'gem_data' , array('conditions' => $conditions) , $server_id);
            $result = $result[$server_id];

            $num = count($num);
        }
        if(!empty($result)){
        	$equie = CDict::$equie;
   
        	foreach($result as $k => $v){
        		$AllItem= $this->call('Gift' , 'getItem' , array() , $server_id);//全部物品
        		$item = $AllItem[$server_id];
        		$rs[$k]['equie'] = $equie[$v['part']] ;
        		$type = $v['type'];
        		switch ($type) {
        			case 0:
        				$type = '镶嵌' ; 
        				break;
        			case 1:
        				$type = '卸下' ; 
        				break;
        			case 2:
        				$type = '升级产出' ; 
        				break; 
        			case 3:
        				$type = '升级消耗' ; 
        				break;          			
        			default:
        				$type = '未知途径' ; 
        				break;
        		}
        		$item_id = trim($v['item_id']);
        		$rs[$k]['gem'] =  $type . $item[$item_id];

        		$rs[$k]['create_time'] = date('Y-m-d H:i:s' , $v['create_time']);
        	}
        }
        foreach ($rs as $k => $row) {
        	$rs[$k] = array_values($row);
        }
		$this->setPageData($rs , $num ,$limit);
	}
	//宝石流水导出
	public function gem_export($conditions){
		$rs=array();
		if(empty($conditions['WHERE']['create_time::>='])){$conditions['WHERE']['create_time::>=']=strtotime(date('Y-m-d'));}
		if(empty($conditions['WHERE']['create_time::<='])){$conditions['WHERE']['create_time::<=']=time();}
		unset($conditions['Extends']['LIMIT']);
		$result = [];
		if(isset($conditions['WHERE']['server'])){
			$server_id = $conditions['WHERE']['server'];
			unset($conditions['WHERE']['server']);

			$result = $this->call('Log' , 'gem_data' , array('conditions' => $conditions) , $server_id);
			$result = $result[$server_id];
		}
		if(!empty($result)){
			$equie = CDict::$equie;
			foreach($result as $k => $v){
				$AllItem= $this->call('Gift' , 'getItem' , array() , $server_id);//全部物品
				$item = $AllItem[$server_id];
				$rs[$k]['equie'] = $equie[$v['part']] ;
				$type = $v['type'];
				switch ($type) {
					case 0:
						$type = '镶嵌' ;
						break;
					case 1:
						$type = '卸下' ;
						break;
					case 2:
						$type = '升级产出' ;
						break;
					case 3:
						$type = '升级消耗' ;
						break;
					default:
						$type = '未知途径' ;
						break;
				}
				$item_id = trim($v['item_id']);
				$rs[$k]['gem'] =  $type . $item[$item_id];
				$rs[$k]['create_time'] = date('Y-m-d H:i:s' , $v['create_time']);
			}
		}
		$ret = [];
		$ret[] = array('部位','宝石','时间');
		foreach($result as $k=>$row){
			$ret = $this->formatFields($row);
		}
		Util::exportExcel($ret,'宝石流水（'.date('Y年m月d日H时i分s秒').'）');
	}
}