<?php

class LoginModel extends Model{

    public function online_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){

        	//只拿起最近三天的在线玩家,防止掉线的用户,服务端没有更新到,一直在库里,重复统计
        	$conditions['WHERE']['start_time::>='] = strtotime(date('Y-m-d',time())) - 2*86400;//20180322
        	$conditions['WHERE']['start_time::<='] = strtotime(date('Y-m-d',time()))+86400;//20180325
        	
        	$server_id = $conditions['WHERE']['server::IN'];
        	unset($conditions['WHERE']['server::IN']);
        	$serverCount = 	count($server_id);
        	$rs = array();
        	if ($serverCount > 1) {
        		$rsArr = [];
        		foreach ($server_id as $key=>$value) {
        			$rs = $this->call('Login' , 'getOnlineData' , array('conditions' => $conditions) , $value);
        			foreach ($rs[$value][0] as $value) {
        				$rsArr[] = $value;
        			}
        		}
        		
        		$rsArr1[0] = $rsArr;
        		$rsArr1[1] = count($rsArr);
        	
        		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
        		foreach($rs as $k => $row){
        			$rs[$k] = array_values($row);
        		}
        	}else {
        		$rs = $this->call('Login' , 'getOnlineData' , array('conditions' => $conditions) , $server_id);
        		$data = $this->callDataMerge($rs);
        		!(empty($data))&&list($rs,$num) = $data;
        		foreach($rs as $k => $row){
        			$rs[$k] = array_values($row);
        		}
        	}
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function online_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){


        	//只拿起最近三天的在线玩家,防止掉线的用户,服务端没有更新到,一直在库里,重复统计
        	$conditions['WHERE']['start_time::>='] = strtotime(date('Y-m-d',time())) - 2*86400;//20180322
        	$conditions['WHERE']['start_time::<='] = strtotime(date('Y-m-d',time()))+86400;//20180325
        	 
        	$server_id = $conditions['WHERE']['server::IN'];
        	unset($conditions['WHERE']['server::IN']);
        	$serverCount = 	count($server_id);
        	$rs = array();
        	if ($serverCount > 1) {
        		$rsArr = [];
        		foreach ($server_id as $key=>$value) {
        			$rs = $this->call('Login' , 'getOnlineData' , array('conditions' => $conditions) , $value);
        			foreach ($rs[$value][0] as $value) {
        				$rsArr[] = $value;
        			}
        		}
        	
        		$rsArr1[0] = $rsArr;
        		$rsArr1[1] = count($rsArr);
        		 
        		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
        		foreach($rs as $k => $row){
        			$rs[$k] = array_values($row);
        		}
        	}else {
        		$rs = $this->call('Login' , 'getOnlineData' , array('conditions' => $conditions) , $server_id);
        		$data = $this->callDataMerge($rs);
        		!(empty($data))&&list($rs,$num) = $data;
        		foreach($rs as $k => $row){
        			$rs[$k] = array_values($row);
        		}
        	}
        }
        $result = array();
        $result[] = array('角色ID' , '角色名' , '最后登录时间' , '在线时长' , 'IP');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '当前在线（' . date('Y年m月d日H时i分s秒') . '）');
    }
}