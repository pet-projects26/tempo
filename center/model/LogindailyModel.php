<?php
class LoginDailyModel extends Model{
    public function __construct(){
        parent::__construct('login_daily');
        $this->alias = 'ld';
    }

    public function record_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Logindaily' , 'getLogindailyData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $row['avg_old_oltime'] = $this->second_format($row['avg_old_oltime']);
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function record_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Logindaily' , 'getLogindailyData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , '登录人数' , '登录次数' , '平均登录次数' , '老玩家数' , '老玩家平均在线时长' , '活跃玩家数' , '忠实玩家数');
        foreach($rs as $row){
            $row['avg_old_oltime'] = $this->second_format($row['avg_old_oltime']);
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '当前在线（' . date('Y年m月d日H时i分s秒') . '）');
    }
}