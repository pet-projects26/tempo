<?php
class LtvModel extends Model{

    public function __construct(){
        parent::__construct('ltv');
        $this->alias = 'ltv';
    }

    public function rolecreate_data($conditions){
        $res = array();
        $num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if (isset($conditions['WHERE']['server::IN'])) {
            
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Ltv' , 'getRolecreateData' , array('conditions' => $conditions) , $server_id);

            list($rs , $num) = $this->callDataMerge($rs);

            if (!empty($rs)) {
                foreach($rs as $k => $row){
                    $num = $row['num'];

                    foreach ($row as $k2 => &$v2) {
                       if ($k2 != 'num') {
                            if (is_numeric($v2)) {
                                $v2 = empty($num) ? 0.00 : round($v2 / $num, 2);
                            } 
                       }
                    }

                    unset($v2);

                    $res[$k] = array_values($row);
                }
            }
        }

        $this->setPageData($res , $num , $limit);
    }


    public function rolecreate_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Ltv' , 'getRolecreateData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }

        $result = array();
        $result[] = array('日期' , '创角数' , '1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天');
        foreach($rs as $k =>  $row){
            $num = $row['num'];
            foreach ($row as $k2 => &$v2) {
               if ($k2 != 'num') {
                    if (is_numeric($v2)) {
                        $v2 = empty($num) ? 0.00 : round($v2 / $num, 2);
                    } 
               }
            }

            unset($v2);

            $result[] = $this->formatFields($row);

        }
        Util::exportExcel($result , '创角LTV（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function register_data($conditions){
        $res = array();
        $num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Ltv' , 'getRegisterData' , array('conditions' => $conditions) , $server_id);

            list($rs , $num) = $this->callDataMerge($rs);


            if (!empty($rs)) {
                foreach($rs as $k => $row){
                    $num  = $row['num'];
                    foreach ($row as $k2 => &$v2) {
                       if ($k2 != 'num') {
                            if (is_numeric($v2)) {
                                $v2 = empty($num) ? 0.00 : round($v2 / $num, 2);
                            } 
                       }
                    }

                    unset($v2);

                    $res[$k] = array_values($row);
                }
            }
        }

        $this->setPageData($res , $num , $limit);
    }

    public function register_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Ltv' , 'getRegisterData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , '注册数' , '1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天');
        foreach($rs as $row){

            $num  = $row['num'];

            foreach ($row as $k2 => &$v2) {
               if ($k2 != 'num') {
                    if (is_numeric($v2)) {
                        $v2 = empty($num) ? 0.00 : round($v2 / $num, 2);
                    } 
               }
            }

            unset($v2);

            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '注册LTV（' . date('Y年m月d日H时i分s秒') . '）');
    }
}