<?php

class LtvrecordModel extends Model{

    public function __construct(){
        parent::__construct('ltv_record');
        $this->alias = 'o';
    }

    public function record_data($conditions)
    {
        $rs = array();
        $num = 0;
//        $limit = $conditions['Extends']['LIMIT'];
//        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['o.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['o.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['o.server::IN'] = $conditions['WHERE']['server::IN'];

        unset($conditions['WHERE']['server::IN']);

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server')
        );
        $fields = array("from_unixtime(o.create_time,'%Y-%m-%d') as create_time", "s.name as server", "new_account", 'one_day', 'two_day', 'three_day', 'four_day', 'five_day', 'six_day', 'seven_day', 'ten_day', 'fourteen_day', 'twenty_day', 'thirty_day', 'forty_five_day', 'sixty_day', 'ninety_day', 'one_hundred_twenty_day');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                //$rs[$k] = array_values($row);
                $rs[$k][0] = $row['create_time'];
                $rs[$k][1] = $row['server'];
                $rs[$k][2] = $row['new_account'];
                $rs[$k][3] = $this->formatLTV($row['one_day']);
                $rs[$k][4] = $this->formatLTV($row['two_day']);
                $rs[$k][5] = $this->formatLTV($row['three_day']);
                $rs[$k][6] = $this->formatLTV($row['four_day']);
                $rs[$k][7] = $this->formatLTV($row['five_day']);
                $rs[$k][8] = $this->formatLTV($row['six_day']);
                $rs[$k][9] = $this->formatLTV($row['seven_day']);
                $rs[$k][10] = $this->formatLTV($row['ten_day']);
                $rs[$k][11] = $this->formatLTV($row['fourteen_day']);
                $rs[$k][12] = $this->formatLTV($row['twenty_day']);
                $rs[$k][13] = $this->formatLTV($row['thirty_day']);
                $rs[$k][14] = $this->formatLTV($row['forty_five_day']);
                $rs[$k][15] = $this->formatLTV($row['sixty_day']);
                $rs[$k][16] = $this->formatLTV($row['ninety_day']);
                $rs[$k][17] = $this->formatLTV($row['one_hundred_twenty_day']);
            }
        }

        $this->setPageData($rs, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['o.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['o.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);
        $conditions['WHERE']['o.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);
//        $server_id = $conditions['WHERE']['server::IN'];
//        unset($conditions['WHERE']['server::IN']);
//        $rs = $rsArr = [];
//        foreach ($server_id as $key => $value) {
//            $server = $value;
//            $rs = $this->call('Multiday', 'record_data', array('conditions' => $conditions), $value);
//            foreach ($rs[$value] as $value) {
//                $value['server'] = $server;
//                $rsArr[] = $value;
//            }
//        }
//        $rsArr1[0] = $rsArr;
//        $rsArr1[1] = count($rsArr);

//        !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
//
//        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
        );
        $fields = array("from_unixtime(o.create_time,'%Y-%m-%d') as create_time", "s.name as server", "new_account", 'one_day', 'two_day', 'three_day', 'four_day', 'five_day', 'six_day', 'seven_day', 'ten_day', 'fourteen_day', 'twenty_day', 'thirty_day', 'forty_five_day', 'sixty_day', 'ninety_day', 'one_hundred_twenty_day');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
               // $rs[$k] = array_values($row);
                $rs[$k][0] = $row['create_time'];
                $rs[$k][1] = $row['server'];
                $rs[$k][2] = $row['new_account'];
                $rs[$k][3] = $this->formatLTV($row['one_day']);
                $rs[$k][4] = $this->formatLTV($row['two_day']);
                $rs[$k][5] = $this->formatLTV($row['three_day']);
                $rs[$k][6] = $this->formatLTV($row['four_day']);
                $rs[$k][7] = $this->formatLTV($row['five_day']);
                $rs[$k][8] = $this->formatLTV($row['six_day']);
                $rs[$k][9] = $this->formatLTV($row['seven_day']);
                $rs[$k][10] = $this->formatLTV($row['ten_day']);
                $rs[$k][11] = $this->formatLTV($row['fourteen_day']);
                $rs[$k][12] = $this->formatLTV($row['twenty_day']);
                $rs[$k][13] = $this->formatLTV($row['thirty_day']);
                $rs[$k][14] = $this->formatLTV($row['forty_five_day']);
                $rs[$k][15] = $this->formatLTV($row['sixty_day']);
                $rs[$k][16] = $this->formatLTV($row['ninety_day']);
                $rs[$k][17] = $this->formatLTV($row['one_hundred_twenty_day']);
            }
        }

        $result = array('日期', '服务器', '新增用户数','1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '10天',  '14天' , '20天', '30天', '45天', '60天', '90天', '120天');

        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result , '','充值留存（' . date('Y年m月d日') . '）.csv');
    }


    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", "server","new_account", 'one_day', 'two_day', 'three_day', 'four_day', 'five_day', 'six_day', 'seven_day', 'eight_day', 'nine_day', 'ten_day' , 'fourteen_day', 'twenty_day', 'thirty_day', 'forty_five_day', 'sixty_day', 'ninety_day', 'one_hundred_twenty_day');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }

    public function setRemain($data , $date = '', $server_id = ''){
        if(empty($date) && $server_id == ''){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
            $conditions['WHERE']['server'] = $server_id;
            $this->update($data , $conditions['WHERE']);
        }
    }
}