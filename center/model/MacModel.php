<?php

class MacModel extends Model{

    public function __construct(){
        parent::__construct('mac');
        $this->alias = 'm';
    }
	 //获取指定日期新增的设备数
    public function getNewDeviceByDate($date,$channel='',$package=''){
        $time = strtotime($date);
		$sql="select mac from ny_mac where channel='$channel' and package='$package' group by mac having min(create_time) >=$time and min(create_time) <".($time + 86400);
        $rs = $this->query($sql);
        $rss = array();
        foreach($rs as $row){
            $rss[] = $row['mac'];
        }
        return $rss;
    }
	 public function getDevicelossNumByDate($date , $device,$channel='',$package=''){
		$where ='where 1';
        $where .= ($device && is_array($device)) ? " and  mac in ('" . implode("','" , $device) . "') " : '';
		$where.=$channel? " and channel='$channel'":'';
		$where.=$package? " and package='$package'":'';
        $sql = 'select channel,package,count(distinct mac) as device_loss_num from ny_mac ' . $where . ' group by mac,channel,package having max(create_time) < ' . strtotime($date);
        $rs = $this->query($sql);
        $device_loss_num = 0;
        foreach($rs as $row){
            $device_loss_num += $row['device_loss_num'];
        }
        return $device_loss_num;
    }
}