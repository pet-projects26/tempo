<?php

class MailModel extends Model{

    public function __construct(){
        parent::__construct('mail');
        $this->alias = 'm';
        $this->joinTable = array();
    }
    
    public function mail_list_export($conditions) {
    
    	$limit = $conditions['Extends']['LIMIT'];
        $conditions['WHERE']['status::!='] = 4;

        unset($conditions['Extends']['LIMIT']);
        if ($_SESSION['group_channel'] != 'all') {

            $server = $this->getServerByTrait();

            foreach ($server as $key => $value) {
                foreach ($value as $k => $v) {
                    $arr[] = $k;
                }
            }
            unset($server);

            foreach ($arr as $kk => $vv) {
                $array[] = "find_in_set('$vv',servers)";
            }

            $array = implode(' or ', $array);

            $where = ' 1 and ';
            if (!empty($array)) {
                $where .= $array;
            }

            if (!empty($conditions['WHERE'])) {
                $starttime = $conditions['WHERE']['create_time::>='];
                $endtime = $conditions['WHERE']['create_time::<='];
                $where .= "create_time between $starttime and $endtime";
            }

            $start = $conditions['Extends']['LIMIT'][0];
            $end = $conditions['Extends']['LIMIT'][1];

            $sql = "select id, title, groups, servers, role_name, attach, 1 as item, 1 as info, status, is_send, msg,sender,agreer, create_time,send_type,  book_time from ny_mail where   $where   order by create_time desc limit  $start, $end";

            $rs = $this->query($sql);

            $num = count($rs);

        } else {

            $fields = array('id', 'title', 'groups', 'servers', 'role_name', 'attach', '1 as item', '1 as info', 'status', 'is_send', 'msg', 'sender', 'agreer', 'create_time', 'send_type', 'book_time');

            $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

            $num = $this->getCount();
        }


        $isSendArr = array(0 => '未发送', 1 => '已发送', 2 => '超时未发送');
        $servers = array();
        foreach ($rs as $key => &$row) {
            $status = $row['status'];
            $is_send = $row['is_send'];

            if ($status == 1) {
                $row['status'] = '<button class="btn btn-success">已审核</button>';
            } elseif ($status == 2) {
                $row['status'] = '<button class="btn btn-danger">拒绝</button>';
            } else {
                $row['status'] = "<button class='gbutton' onclick='editMail(1, {$row['id']}, this)'>同意</button>  |  ";
                $row['status'] .= "<button class='gbutton' onclick='editMail(3, {$row['id']}, this)'>拒绝</button>";
            }

            $is_send_str = '';

            if ($is_send == 1) {
                $is_send_str = '已发送';
            } elseif ($is_send == 0 && $status == 2) {
                $is_send_str = '不允许发送';
            } elseif ($is_send == 3) {
                $is_send_str = '超时未审核未发送';
            } else {
                $is_send_str = "<button class='gbutton' id='aaa' onclick='editMail(2, {$row['id']}, this)'>发送</a>";
            }

            //$is_del_str = $is_send == 1 ? '' : "<button class='gbutton' onclick='editMail(4, {$row['id']}, this)'>删除</a>";

            $row['is_send'] = $is_send_str;

            if ($row['send_type'] == 1) {
                $row['send_type'] = '是';
                $row['book_time'] = date('Y-m-d H:i:s', $row['book_time']);

                if ($is_send == 0) $row['is_send'] = '定时邮件不允许手动发送';

            } else {
                $row['send_type'] = '否';
                $row['book_time'] = '无';
            }

            $role_name = $row['role_name'];
            if (empty($role_name)) {
                $row['role_name'] = '全服玩家';
            }

            $attach = json_decode($row['attach'], true);
            $arr = array();
//            $arr[] = '铜币：'.$attach['copper'];
//            $arr[] = '元宝：'.$attach['gold'];
//            $arr[] = '绑定元宝：'.$attach['bind_gold'];

            $source = $attach['source'];

            if (!empty($source)) {
                foreach($source as $source_key => $source_val) {
                    $arr[] = CDict::$mailSource[$source_key]['name'].':'.$source_val[1];
                }
            } else {
                $arr[] = '无';
            }


            $row['attach'] = implode(' ', $arr);

            $row['item'] = $attach['item'];

            //$row['info'] = "<button class='gbutton' onclick='info( {$row['id']}, this)'>邮件详情</a>";

            $servers[$key] = $row['servers'];
            $row['create_time'] = date('y-m-d H:i:s', $row['create_time']);

            $groups = $row['groups'];
            if (!empty($groups)) {

                $sql = "select name from ny_channel_group where id in ($groups)";

                $group = $this->query($sql);

                $group_name = implode(',', array_column($group, 'name'));
            } else {
                $group_name = '';
            }

            $row['groups'] = $group_name;

        }

        unset($row);
        $temp = [];
        if (!empty($server_id)) {
            foreach ($server_id as $val) {
                $arr = explode(',', $val);
                $temp = array_merge($temp, $arr);
            }
        }
        $servers = array_filter($temp);
        $servers = array_unique($servers);
        $res = $this->getGm(array(), $servers);
        $serverList = array();
        foreach ($res as $key => &$r) {
            $serverList[$r['server_id']] = $r['name'];
        }
        foreach ($rs as $key => &$row) {
            //此处就是server
            $row['servers'] = self::getServerLst($serverList, $row['servers']);
        }
    
    	unset($row);
    
    
    	$result = array();
    	$result[] = array('ID' , '标题' , '渠道组' , '服务器',
            '角色名称', '货币', '物品', '发送结果', '申请者', '审批人', '添加时间', '是否定时', '定时发送时间'
    	);
    	foreach($rs as $k=>$row){
    		$result[$k+1]['id'] = $row['id'];
    		$result[$k+1]['title'] = $row['title'];
    		$result[$k+1]['groups'] = $row['groups'];
    		$result[$k+1]['servers'] = $row['servers'];
    		$result[$k+1]['role_name'] = $row['role_name'];
    		$result[$k+1]['attach'] = $row['attach'];
    		$result[$k+1]['item'] = json_encode($row['item']);
    		$result[$k+1]['msg'] = $row['msg'];
    		$result[$k+1]['sender'] = $row['sender'];
    		$result[$k+1]['agreer'] = $row['agreer'];
    		$result[$k+1]['create_time'] = $row['create_time'];
            $result[$k + 1]['send_type'] = $row['send_type'];
            $result[$k + 1]['book_time'] = $row['book_time'];
    	}
    	Util::exportExcel($result ,'邮件列表（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * 发送邮件
     */
    public function manual_mail($data){

        if (mb_strlen($data['sender'] , 'UTF-8') > 13) {
            $json = array('state' => false , 'code' => 200 , 'msg' => '发件人名称太长，请参照说明描述的长度要求填写');
            exit(json_encode($json));
        }

        if (mb_strlen($data['title'] , 'UTF-8') > 13) {
            $json = array('state' => false , 'code' => 200 , 'msg' => '标题名称太长，请参照说明描述的长度要求填写');
            exit(json_encode($json));
        }

        if($data['content'] == '') {
            $json = array('state' => false , 'code' => 200 , 'msg' => '邮件内容不能为空');
            exit(json_encode($json));
        }

        $now = time();
//        $attach = array(
//            'copper' => $data['copper'],
//            'gold' => $data['gold'],
//            'bind_gold' => $data['bind_gold']
//        );
        $attach = [];

        foreach($data['source'] as $key => $val) {
            $val = intval($val);
            $key = intval($key);
            if ($val > 0){
                $attach['source'][$key] = [];
                $attach['source'][$key][0] = CDict::$mailSource[$key]['itemId'];
                $attach['source'][$key][1] = $val;
            }
        }
		
        $attach['item'] = array();
		
        if ($data['item_id'] && count($data['item_id'])) {
            foreach($data['item_id'] as $k => $row){			
                $item_id = explode('/' , $row);
				$row = $item_id[0];
                if (empty($row)) {
                    continue;
                }

                $count = $data['item_count'][$k];

                if (empty($count)) {
                    $json = array('state' => false , 'code' => 204 , 'msg' => '请填入第'.$k.'行物品数量');
                    exit(json_encode($json));
                }

                $attach['item'][$k][0] = intval($row);
                $attach['item'][$k][1] = intval($count);
            }
        }

        $status = $is_send = 1;
        $stop = false;

        if (!empty($attach['source']) || !empty($attach['item'])) {
            $stop = true;
            $status = 0;
            $is_send = 0;
        }

        $server = implode(',',$data['server']);

        $mail = array(
            'admin' => $_SESSION['username'],
            'groups' => $data['groups'],
            'servers' => $server,
            'role_id' => $data['role_id'],
            'role_name' => $data['role_name'],
            'sender' => $data['sender'],
            'title' => $data['title'],
            'status' => $status,
            'is_send' => $is_send,
            'send_type' => $data['send_type'],
            'book_time' => $data['book_time'],
            'content' => $data['content'],
            'attach' => json_encode($attach),
            'create_time' => $now
        );

        try {
            $id = (new MailModel())->add($mail);
        } catch(Exception $e) {
            $json = array('state' => false , 'code' => 500 , 'msg' => $e->getMessage());
            exit($json);
        }

        if ($stop) {
            if ($mail['send_type'] == 1) {
                $json = array('state' => true , 'code' => 200 , 'msg' => '邮件有资源且是定时发送邮件，请先审核才会自动发送');
            } else {
                $json = array('state' => true , 'code' => 200 , 'msg' => '邮件有资源，请审核再发送');
            }
            exit(json_encode($json));
        }

        if ($mail['send_type'] == 1) {
            $json = array('state' => true , 'code' => 200 , 'msg' => '邮件是定时发送邮件, 到时间会自动发送');
            exit(json_encode($json));
        }

        $server = $data['server'];

        $mail['role_id'] = $data['role_id'] == 0 ? array(intval(0)) : explode(',' , $data['role_id']);//把角色id转成数组

        foreach($mail['role_id'] as &$val) {
            $val = intval($val);
        }

        $msg = $this->send_mail($server, $mail, $attach);

        $this->update(array('msg' => $msg, 'send_time' => $now) , array('id'=>$id));

        /*if ($msg == 'ok') {
            $msg = '发送成功';
        }*/

        $json = array('state' => true , 'code' => 200 , 'msg' => $msg);

        exit(json_encode($json));
    }

    /**
     * 发送邮件
     * @param array $mail 邮件相关信息
     * @param array  $item 物品
     */
    public function send_mail($server, $mail, $attach = array()) {
        
        if (empty($server)) {
            return "服务器不能为空";
        }

        $items = [];
        //先把物品数组合并
        if (!empty($attach['source']) && !empty($attach['item'])) {
            $items = array_merge($attach['source'], $attach['item']);
        } elseif (!empty($attach['source']) && empty($attach['item'])) {
            $items = array_values($attach['source']);
        } elseif (empty($attach['source']) && !empty($attach['item'])) {
            $items = array_values($attach['item']);
        }

        $data = array($items, $mail['title'], $mail['content'], $mail['role_id']);

        $info = ['opcode' => Pact::$MailSendOperate, 'str' => $data];

//        $url = Helper::v2();
//
//        $res = Helper::rpc($url , $servers ,'smail' , $data);

        $res = [];

        $server_config = (new ServerconfigModel())->getConfig($server, ['server_id', 'websocket_host', 'websocket_port']);

        foreach($server_config as $key => $value) {

            $res[$value['server_id']] = $this->socketCall($value['websocket_host'], $value['websocket_port'], 'sendMail', $info);
        }

        $msg = '';
        
        foreach($res as $k=>$v){

            $sql = "select name from ny_server where server_id = '$k'";
            $server_name = $this->query($sql);
            $server_name = $server_name[0]['name'];
            
            if($v ==1){
                $msg .= $server_name .': 发送成功' ; 
            }else{
                $msg .= $server_name .': 发送失败';
            }
        }

        return $msg;
    }

    /**
     * 邮件列表
     *
     */
    public function mail_list_data($conditions) {
        
        $limit = $conditions['Extends']['LIMIT'];

        $conditions['WHERE']['status::!='] = 4;
/*
        if($_SESSION['group_channel'] != 'all'){
           
            $server = $this->getServerByTrait();
        
            foreach ($server as $key => $value) {
                foreach($value as $k => $v){
                    $arr[] = $k;
                }
            }
            unset($server);
            
            foreach ($arr as $kk => $vv) {
              $array[]= "find_in_set('$vv',servers)";
            }

            $array = implode(' or ' , $array);

            $where = ' 1 and ';
            if(!empty($array)){
                $where .=  $array;
            }

            if(!empty($conditions['WHERE'])){
                $starttime = $conditions['WHERE']['create_time::>='];
                $endtime   = $conditions['WHERE']['create_time::<='];
                $where .= "create_time between $starttime and $endtime";
            }

            $start = $conditions['Extends']['LIMIT'][0];
            $end = $conditions['Extends']['LIMIT'][1];

            $sql = "select id, title, groups, servers, role_name, attach, 1 as item, 1 as info, status, is_send, msg,sender,agreer, create_time,send_type,  book_time from ny_mail where   $where   order by create_time desc limit  $start, $end";
           
           $rs = $this->query($sql);

           $num = count($rs);
         
        }else{ */
           
        	unset($conditions['Extends']['LIMIT']);
             $fields = array('id', 'title', 'groups', 'servers', 'role_name', 'attach', '1 as item', '1 as info', 'status', 'is_send', 'msg','sender', 'agreer', 'create_time', 'send_type', 'book_time');

            $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

            $num = $this->getCount();
      /*  }
 */

        $isSendArr = array(0 => '未发送', 1 => '已发送', 2 => '超时未发送');
        $servers = array();
        foreach ($rs as $key => &$row) {
            $status = $row['status'];
            $is_send = $row['is_send'];

            if ($status == 1) {
                $row['status'] = '<button class="btn btn-success">已审核</button>';
            } elseif($status == 2){
                $row['status'] = '<button class="btn btn-danger">拒绝</button>';
            }else {
                $row['status'] = "<button class='gbutton' onclick='editMail(1, {$row['id']}, this)'>同意</button>  |  ";
                $row['status'] .= "<button class='gbutton' onclick='editMail(3, {$row['id']}, this)'>拒绝</button>";                
            }

            $is_send_str = '';

            if ($is_send == 1) {
                $is_send_str = '已发送';
            } elseif ($is_send == 0 && $status == 2) {
                $is_send_str = '不允许发送';
            } elseif ($is_send == 3) {
                $is_send_str = '超时未审核未发送';
            } else {
                $is_send_str = "<button class='gbutton' id='bbb' onclick='editMail(2, {$row['id']}, this)'>发送</button>";
            }

            $is_del_str = $is_send == 1 ? '' : "  <button class='gbutton' onclick='editMail(4, {$row['id']}, this)'>删除</button>";

            $row['is_send'] = $is_send_str . $is_del_str;

            if ($row['send_type'] == 1) {
                $row['send_type'] = '是';
                $row['book_time'] = date('Y-m-d H:i:s', $row['book_time']);

                if ($is_send == 0) $row['is_send'] = '定时邮件不允许手动发送';

            } else {
                $row['send_type'] = '否';
                $row['book_time'] = '无';
            }

            $role_name = $row['role_name'];
            if (empty($role_name)) {
                $row['role_name'] = '全服玩家';
            }

            $attach = json_decode($row['attach'], true);
            $arr = array();
//            $arr[] = '铜币：'.$attach['copper'];
//            $arr[] = '元宝：'.$attach['gold'];
//            $arr[] = '绑定元宝：'.$attach['bind_gold'];

            $source = $attach['source'];

            if (!empty($source)) {
                foreach($source as $source_key => $source_val) {
                    $arr[] = CDict::$mailSource[$source_key]['name'].':'.$source_val[1];
                }
            } else {
                $arr[] = '无';
            }


            $row['attach'] = implode(' ', $arr);
			
			$row['item'] = "<button class='gbutton' onclick='check( {$row['id']}, this)'>查看</a>";

            $row['info'] = "<button class='gbutton' onclick='info( {$row['id']}, this)'>邮件详情</a>";
          
            $servers[$key] = $row['servers'];
            $row['create_time'] = date('y-m-d H:i:s', $row['create_time']);

            $groups = $row['groups'];
            if(!empty($groups)){

                $sql ="select name from ny_channel_group where id in ($groups)";
            
                $group = $this->query($sql);
             
                $group_name = implode(',', array_column($group, 'name')) ;
            }else{
                $group_name = '';
            }
           
            $row['groups'] = $group_name;

        }
		
        unset($row);
        $temp = [];
        if(!empty($server_id)){
            foreach($server_id as $val){
                $arr=explode(',',$val);
                $temp = array_merge($temp,$arr);
            }
        }
        $servers = array_filter($temp);
        $servers = array_unique($servers);
        $res = $this->getGm(array(), $servers);
        $serverList = array();
        foreach ($res as $key => &$r) {
            $serverList[$r['server_id']] = $r['name'];
        }
        foreach ($rs  as $key => &$row) {
            //此处就是server
            $row['servers'] = self::getServerLst($serverList,$row['servers']);
        }

		unset($row); 
		
        foreach($rs as $k => $row){
			
            $rs[$k] = array_values($row);
        }
		
        $this->setPageData($rs , $num, $limit);
    }

    /**
     * 邮件审核
     */
    public function mail_check($id) {
       
       // $rs = $this->getRows($fields , $conditions['WHERE'] , array());
        try {
            $this->update(array('status' => 1,'agreer'=>$_SESSION['username']), array('id' => $id));
            return 'ok';
        } catch(Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * 邮件审核
     */
    public function mail_refuse($id) {
        //$rs = $this->getRows($fields , $conditions['WHERE'] , array());
        try {
            $this->update(array('status' => 2,'agreer'=>$_SESSION['username']), array('id' => $id));
            return 'ok';
        } catch(Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * 邮件删除
     */
    public function mail_delete($id) {
        //$rs = $this->getRows($fields , $conditions['WHERE'] , array());
        try {
            $this->update(array('status' => 4,'agreer'=>$_SESSION['username']), array('id' => $id));
            return 'ok';
        } catch(Exception $e) {
            return $e->getMessage();
        }

    }


    /**
     * 获取服务器列表
     * @param array $serverList
     * @param string $serverStr
     * @return string $server
    */
    private static function getServerLst($serverList,$serverStr){
        $server = '';
        if(!$serverStr || empty($serverList)){
            return $server;
        }
        $serverArr = explode(',',$serverStr);
        $temp = array();
        foreach($serverArr as $val){
            $temp[] = isset($serverList[$val]) ? $serverList[$val] : $val;
        }
        $server = join(',',$temp);
        return $server;
    }


    //获取时间段内待发送的邮件
    public function getBookEmail($start_time, $end_time)
    {
        $conditions = array();

        $conditions['WHERE']['is_send'] = 0;
        $conditions['WHERE']['send_type'] = 1;
        $conditions['WHERE']['book_time::>='] = $start_time;
        $conditions['WHERE']['book_time::<='] = $end_time;

        $res = $this->getRows('*', $conditions['WHERE']);

        return $res;
    }


}
