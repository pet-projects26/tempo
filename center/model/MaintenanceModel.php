<?php

class MaintenanceModel extends Model{

    public function __construct(){
        parent::__construct('server_maintenance');
        $this->alias = 'm';
        $this->cgm = new ChannelGroupModel();
    }

    public function record_data($conditions){

        $result = array();
        $num = 0;

        $fields = array('*');
        //$limit = $conditions['Extends']['LIMIT'];
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num  = count($rs);
        /*$groups = $this->cgm->getRows(array('id' , 'name'));

        $group = array();
        foreach($groups as $k=>$v){
            $group[$v['id']] = $v['name']; 
        }*/
        $rss = array();
        foreach($rs as $k=>$v){
            $rss[$k]['id'] = $v['id'];

            if($v['group_num'] == ''){
                $rss[$k]['group_num'] = "全渠道组";
            }else{
                $group_num = $v['group_num'];
                $groupsql = "select name from ny_channel_group where id in ($group_num)";
                $grouparr = $this->query($groupsql);
                $group_name = array_column($grouparr, 'name');
                $group_name = implode(',',$group_name);
                $rss[$k]['group_num'] = $v['group_num'].'('.$group_name.')';
            }

            if($v['server_id'] == ''){
                $rss[$k]['server'] = '全部服务器';
            }else{
                $server = explode(',',$v['server_id']);
                $server = implode("','" , $server);
                $sql = "select name from ny_server where server_id in ('$server')";
                $serverarr = $this->query($sql);
                $name = array_column($serverarr, 'name');
                $rss[$k]['server'] = implode(',',$name);
            }

            $rss[$k]['version'] = $v['version'];

            switch ($v['type']) {
                case 0:
                   $rss[$k]['type'] = '<button class="btn btn-default" >未配置</button>';
                    break;
                case 1:
                   $rss[$k]['type'] = '<button class="btn btn-primary" >已配置</button>';
                    break;
                case 2:
                   $rss[$k]['type'] = '<button class="btn btn-info" >维护中</button>';
                    break;
                case 3:
                   $rss[$k]['type'] = '<button class="btn btn-success" >维护完成</button>';
                    break;
                
                default:
                    
                    break;
            }

            $rss[$k]['starttime'] = date('Y-m-d H:i' , $v['starttime']);

            $rss[$k]['endtime'] = date('Y-m-d H:i' , $v['endtime']);

            $rss[$k]['createtime'] = date('Y-m-d H:i' , $v['createtime']);

            $config = $v['type'] == 0 ? '<input type="button" class="gbutton" value="配置" onclick="config(\'' . $v['id'] . '\')">' : '';

            $edit = $v['type'] < 3 ?  '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $v['id'] . '\')">' : '';

            $del = '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $v['id'] . '\')">';


            $rss[$k]['caozuo'] = $config . $edit . $del;


        }

        foreach ($rss as $key => $value) {
            $result[$key] = array_values($value);
        }

        $this->setPageData($result , $num);
       
    }

   
}