<?php

class MallModel extends Model
{

    public function __construct()
    {
        parent::__construct('mall_record');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) + 86399 : strtotime(date('Y-m-d', time())) + 86399;

        unset($conditions['WHERE']['create_time']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $conditions['WHERE']['a.mall_type'] = isset($conditions['WHERE']['mall_type']) ? $conditions['WHERE']['mall_type'] : 0;
        unset($conditions['WHERE']['mall_type']);

        $conditions['WHERE']['a.child_mall_type'] = isset($conditions['WHERE']['child_mall_type']) ? $conditions['WHERE']['child_mall_type'] : 1;
        unset($conditions['WHERE']['child_mall_type']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "product_id", 'item_name', 'currency_name', 'mall_name', 'child_mall_name', 'consumer', 'monetary', 'mall_consumer', 'mall_monetary');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['item_name'];
                $data[$k][3] = $row['monetary'];

                //消费占比
                $data[$k][4] = @sprintf("%.2f", $row['monetary'] / $row['mall_monetary'] * 100) . '%';

                //消费人数
                $data[$k][5] = $row['consumer'];

                //人数占比
                $data[$k][6] = @sprintf("%.2f", $row['consumer'] / $row['mall_consumer'] * 100) . '%';
                //消费货币
                $data[$k][7] = $row['currency_name'];

            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) + 86399 : strtotime(date('Y-m-d', time())) + 86399;

        unset($conditions['WHERE']['create_time']);

        $conditions['WHERE']['a.mall_type'] = isset($conditions['WHERE']['mall_type']) ? $conditions['WHERE']['mall_type'] : 0;
        unset($conditions['WHERE']['mall_type']);

        $conditions['WHERE']['a.child_mall_type'] = isset($conditions['WHERE']['child_mall_type']) ? $conditions['WHERE']['child_mall_type'] : 1;
        unset($conditions['WHERE']['child_mall_type']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "product_id", 'item_name', 'currency_name', 'mall_name', 'child_mall_name', 'consumer', 'monetary', 'mall_consumer', 'mall_monetary');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        $mallName = '';
        $childMallName = '';

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['item_name'];
                $data[$k][3] = $row['monetary'];

                //消费占比
                $data[$k][4] = @sprintf("%.2f", $row['monetary'] / $row['mall_monetary'] * 100) . '%';

                //消费人数
                $data[$k][5] = $row['consumer'];

                //人数占比
                $data[$k][6] = @sprintf("%.2f", $row['consumer'] / $row['mall_consumer'] * 100) . '%';
                //消费货币
                $data[$k][7] = $row['currency_name'];

                !$mallName && $mallName = $row['mall_name'];
                !$childMallName && $childMallName = $row['child_mall_name'];

            }
        }

        $result = array();

        $result[] = array('日期', '服务器', '商城货物名称', '消费金额', '消费占比', '消费人数', '人数占比', '货币');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', $mallName . '-' . $childMallName . '消费分布统计（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}