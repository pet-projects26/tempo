<?php

class MergeModel extends Model{

    public function __construct(){
        parent::__construct('merge');
        $this->alias = 'm';
    }

    public function merge_list_data($conditions){
    	
        $rs = array();

        $num = 0;
        
		$fields = array('id' ,'1 as groups','parent' ,'children' ,'merge_time' , 'start_time' , 'status', 'notify', 'create_time');

		$result = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		if($result){
			$num = $this->getCount();
			foreach ($result as $k => $v) {

                $rs[$k]['id'] = $v['id'];

                $parent = $v['parent'];
                $children = '';
                $children = $v['children'];
                $children = explode(',', $children);
                $children = implode("','" , $children);

                //渠道组
                
                $sql = "select distinct(g.name) as name  from ny_channel_group g left join ny_server s on s.group_id = g.id where s.server_id in ('$parent','$children')";

                $group = $this->query($sql);
                        
                if(!empty($group)){
                    $rs[$k]['group'] = implode(',', array_column($group, 'name'));

                }else{
                    $rs[$k]['group'] = '';
                }
			
				//母服
				$sql="select name,num from ny_server where server_id = '$parent'";
				$parent = $this->query($sql);
				$rs[$k]['parent'] = 'S'.$parent[0]['num']. '-'.$parent[0]['name'];

				//子服
				$sql = "select name, num from ny_server where server_id in ('$children') ";
				
				$children = $this->query($sql);
				
				foreach($children as $kk=> $vv){
					$children[$kk]['name'] = 'S'.$vv['num']. '-'.$vv['name'];
				}

				$children = array_column($children, 'name');

				$children = implode(',' , $children);
				
				$rs[$k]['children'] = $children;

                
				$rs[$k]['merge_time'] = date('Y-m-d H:i:s' , $v['merge_time']);
				$rs[$k]['start_time'] = date('Y-m-d H:i:s' , $v['start_time']);

                $rs[$k]['status'] = $v['status'] == 0 ? '<button class="btn btn-deafult">未开始</button>' : '<button class="btn btn-success">合服成功</button>';

                $rs[$k]['notify'] = $v['notify'];
                /*switch ($v['notify']) {
                    case '1':
                       $rs[$k]['notify'] = '<button class="btn btn-primary">合服前邮件</button>';
                        break;
                    case '2':
                        $rs[$k]['notify'] = '<button class="btn btn-warning">合服时物品下架</button>';
                        break;
                    case '3':
                        $rs[$k]['notify'] = '<button class="btn btn-danger">合服公告</button>';
                        break;
                    case '4':
                        $rs[$k]['notify'] = '<button class="btn btn-info">自动开服</button>';
                        break;
                    case '5':
                        $rs[$k]['notify'] = '<button class="btn btn-success">开启拍卖行</button>';
                        break;
                    
                    default:
                        $rs[$k]['notify'] = '<button class="btn btn-deafult">还未开始</button>';
                        break;
                }*/


				$rs[$k]['create_time'] = date('Y-m-d H:i:s' , $v['create_time']);

				$rs[$k]['caozuo'] = '<input class="gbutton" value="编辑" onclick="edit('.$v['id'].')" type="button">  <input class="gbutton" value="删除" onclick="del('.$v['id'].')" type="button">
                    <input class="gbutton" value="提前开服" onclick="_open('.$v['id'].')" type="button">
                    ' ;
				
			}

           

			foreach($rs as $k => $v){
				$rs[$k] = array_values($v);
			}

		}
		

		$this->setPageData($rs , $num);

    }

    /**
     * 检查活动
     * @param [array] $[servers] [<服务器>]
     * @param [string] $[time] [<时间>]
     * @return [type] [description]
     */
    public function checkAct($servers, $time) {
        //@todo 查询活动
        $vars = array (
            'document' => array(
                'table' => 'bg_activity',
                'data' => array(
                    'state' => 0,
                    'tm.start' => array('$lt' => strtotime($time)),
                    'tm.close' => array('$gt' => strtotime($time)),
                ),
            )
        );

        $postData = array(
            'r' => 'call_method',
            'class' => 'SqlModel',
            'method' => 'getMongo',
            'params' => json_encode($vars),
        );

        $instance  = new ServerconfigModel();
        
        $urls = $instance->getApiUrl($servers, true);

        $rs = Helper::rolling_curl($urls, $postData);

        $msg  = array();
        foreach ($rs as $sign => &$row) {
            $row = json_decode($row, true);

            if (empty($row)) {
                continue;
            }

            $act = array();
            foreach ($row as $k2 => $r2) {
                $act[$k2] = $r2['name'];    
            }

            $res = (new ServerModel())->getServer($sign);

            $server_name = (new ServerModel())->formatServerName($res);

            $msg[$sign] = $server_name .':'. implode(',', $act);

        }

        unset($row);

        if (!empty($msg)) {
          	return array('code' => 200, 'msg' => implode("<br><br>", $msg) .'<br><br> 在该活动时间存在活动,  不允许添加');
        }

        return  array();
    }

  
}