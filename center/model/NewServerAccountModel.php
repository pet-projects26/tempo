<?php


class NewServerAccountModel extends Model
{
    public function __construct($tableName = null)
    {
        parent::__construct('new_server_account');
    }

    public function isRollServerAccount($server, $account, $is_create = 0)
    {
        $conditions = array();
        $conditions['WHERE']['account'] = $account;
        $conditions['WHERE']['server::!='] = $server;
        $is_create && $conditions['WHERE']['is_create'] = 1;

        $res = $this->getRow('id', $conditions['WHERE']);

        if ($res) {
            return true;
        }

        return false;
    }

    public function getNodeByAccFirst($account, $channel, $server)
    {
        $conditions = array();
        $conditions['WHERE']["account"] = $account;
        $conditions['WHERE']["channel"] = $channel;
        $conditions['WHERE']['server'] = $server;

        $res = $this->getRow('id', $conditions['WHERE']);

        if ($res) {
            return false;
        }

        return true;
    }

    //获取服务器的新增用户
    public function getNewAccount($start_time, $end_time, $server, $package = '', array $accounts = [])
    {
        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $package && $conditions['WHERE']['package'] = $package;
        $accounts && $conditions['WHERE']['accounts'] = $accounts;

        $res = $this->getRows('distinct account as account', $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                array_push($data, $value['account']);
            }
        }

        return $data;
    }
}