<?php
class NewaccountModel extends Model{
	
    public function __construct(){
        parent::__construct('new_account');
//         $this->alias = 'cd';
    }

    public function newaccount_data($conditions){
    	
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['time::>=']) && $conditions['WHERE']['time::>='] = date("Y-m-d",$conditions['WHERE']['time::>=']);
    		isset($conditions['WHERE']['time::<=']) && $conditions['WHERE']['time::<='] = date("Y-m-d",$conditions['WHERE']['time::<=']);
    		 
    		$fields = array('SUM(account) AS account','SUM(udid) AS udid' ,"time");
    		$conditions['Extends']['GROUP'] = "server,time";
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
//     		    		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$rs[$row['time']]['time'] = $row['time'];
    			$rs[$row['time']]['udid']      += $row['udid'];
    			$rs[$row['time']]['account']   += $row['account'];
    		}
    		$rs = array_values($rs);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , 0);
    	
    }
    public function newaccount_export($conditions){
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['time::>=']) && $conditions['WHERE']['time::>='] = date("Y-m-d",$conditions['WHERE']['time::>=']);
    		isset($conditions['WHERE']['time::<=']) && $conditions['WHERE']['time::<='] = date("Y-m-d",$conditions['WHERE']['time::<=']);
    		 
    		$fields = array('SUM(account) AS account','SUM(udid) AS udid' ,"time");
    		$conditions['Extends']['GROUP'] = "server,time";
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		//     		    		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$rs[$row['time']]['time'] = $row['time'];
    			$rs[$row['time']]['udid']      += $row['udid'];
    			$rs[$row['time']]['account']   += $row['account'];
    		}
    		$rs = array_values($rs);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    		
    		$fields = array();
    		$fields[] = array('日期' , '激活设备(台)' ,'新增玩家(账号)');
    		foreach($rs as $row){
    			$fields[] = $this->formatFields($row);
    		}
    		Util::exportExcel($fields , '新增玩家和新增设备数据（' . date('Y年m月d日H时i分s秒') . '）');
    	}
    }
	
}