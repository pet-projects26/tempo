<?php

class NineCopyModel extends Model
{

    public function __construct()
    {
        parent::__construct('nine_copy');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'one_layer_num', 'two_layer_num', 'three_layer_num', 'four_layer_num', 'five_layer_num', 'six_layer_num', 'seven_layer_num', 'eight_layer_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = $row['join_num'] && $row['active_num'] ? @sprintf("%.2f", $row['join_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][5] = $row['eight_layer_num'];
                //通关比例
                $data[$k][6] = $row['one_layer_num'] ? @round($row['one_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][7] = $row['two_layer_num'] ? @round($row['two_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][8] = $row['three_layer_num'] ? @round($row['three_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][9] = $row['four_layer_num'] ? @round($row['four_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][10] = $row['five_layer_num'] ? @round($row['five_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][11] = $row['six_layer_num'] ? @round($row['six_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][12] = $row['seven_layer_num'] ? @round($row['seven_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][13] = $row['eight_layer_num'] ? @round($row['eight_layer_num'] / $row['join_num'], 2) : 0.00;
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'one_layer_num', 'two_layer_num', 'three_layer_num', 'four_layer_num', 'five_layer_num', 'six_layer_num', 'seven_layer_num', 'eight_layer_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = $row['join_num'] && $row['active_num'] ? @sprintf("%.2f", $row['join_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][5] = $row['eight_layer_num'];
                //通关比例
                $data[$k][6] = $row['one_layer_num'] ? @round($row['one_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][7] = $row['two_layer_num'] ? @round($row['two_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][8] = $row['three_layer_num'] ? @round($row['three_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][9] = $row['four_layer_num'] ? @round($row['four_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][10] = $row['five_layer_num'] ? @round($row['five_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][11] = $row['six_layer_num'] ? @round($row['six_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][12] = $row['seven_layer_num'] ? @round($row['seven_layer_num'] / $row['join_num'], 2) : 0.00;
                $data[$k][13] = $row['eight_layer_num'] ? @round($row['eight_layer_num'] / $row['join_num'], 2) : 0.00;
            }
        }
        $result = array();

        $result[] = array('日期', '服务器', '活跃人数', '参与人数', '参与度', '登顶人数', '通关第1层人数比例', '通关第2层人数比例', '通关第3层人数比例', '通关第4层人数比例', '通关第5层人数比例', '通关第6层人数比例', '通关第7层人数比例', '通关第8层人数比例');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '九天之巅（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}