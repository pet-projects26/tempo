<?php

class NodeModel extends Model
{
    public function __construct()
    {
        //(new MergeSqlUtil('node'))->createNewMergeTable();
        parent::__construct('node');
        $this->alias = 'n';
    }

    public function getNewDeviceByDate($date)
    {
        $starttime = strtotime($date . ' 00:00:00');
        $endtime = strtotime($date . ' 23:59:59');
        $sql = "select distinct(mac) as mac from ny_node where create_time between $starttime and $endtime and mac !='0'";
        $rs = $this->query($sql);
        return $rs;
    }

    public function getNodeBydate($date, $device)
    {
        $starttime = strtotime($date . ' 00:00:00');
        $endtime = strtotime($date . ' 23:59:59');
        if ($device) {
            foreach ($device as $k => $v) {
                $sql = "select account from ny_node where mac='" . $v['mac'] . "' and create_time between $starttime and $endtime  order by create_time asc limit 1 ";
                $rs = $this->query($sql);
                if ($rs[0]['account'] != 'p03') {//'p03是空的，忽略掉'
                    $account[] = $rs[0]['account'];
                }
            }
            $status = array('one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twleve');
            $fields = array();
            foreach ($status as $k => $v) {
                $fields[] = 'sum(status=' . ($k + 1) . ') as ' . $v;
            }
            $conditions = array();
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
            $conditions['WHERE']['account::IN'] = $account;
            $result = $this->getRow($fields, $conditions['WHERE']);
            return $result;
        } else {
            return array(
                'one' => 0, 'two' => 0, 'three' => 0, 'four' => 0, 'five' => 0, 'six' => 0,
                'seven' => 0, 'eight' => 0, 'nine' => 0, 'ten' => 0, 'eleven' => 0, 'twleve' => 0
            );
        }
    }

    public function getNodeByhour($date, $device, $server)
    {
        $status = array('one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twleve', 'thirteen', 'fourteen', 'fifteen');
        $fields = array();
        foreach ($status as $k => $v) {
            $fields[] = 'sum(status=' . ($k + 1) . ') as ' . $v;
        }
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d %H')"] = $date;
        $conditions['WHERE']['server'] = $server;
        $result = $this->getRow($fields, $conditions['WHERE']);
    }

    public function getNodeByAccFirst($account, $channel, $package, $server)
    {
        $conditions = array();
        $conditions['WHERE']["account"] = $account;
        $conditions['WHERE']["channel"] = $channel;
        $conditions['WHERE']['package'] = $package;
        $conditions['WHERE']['server'] = $server;
        $conditions['WHERE']['first'] = 1;
        // $conditions['WHERE']['status'] = CDict::$recordStep['StartLoadLib'];

        $res = $this->getRow('id', $conditions['WHERE']);

        if ($res) {
            return false;
        }

        return true;
    }

    public function getAccountConnectionServerData($start_time, $end_time, $server, $first = 2)
    {
        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $conditions['WHERE']['status'] = CDict::$recordStep['StartConnectServer'];
        if ($first != 2) {
            $conditions['WHERE']['first'] = $first;
            $conditions['WHERE']['status'] = CDict::$recordStep['StartLoadLib'];
        }

        $res = $this->getRows('distinct account as account', $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                array_push($data, $value['account']);
            }
        }

        return $data;
    }

    public function getAccountStatusServerLoadTime($start_time, $end_time, $server, $status, $accounts = [])
    {
        $tableName = (new SplitSqlUtil($this->tableName))->returnDateTableName([$start_time, $end_time]);

        if (!$tableName) {
            return [];
        }

        $old_tableName = $this->tableName;

        if ($tableName != $this->tableName) {
            parent::__construct($tableName);
        }

        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $conditions['WHERE']['status'] = $status;
        $accounts && $conditions['WHERE']['account::IN'] = $accounts;
        $fields = array('distinct account as account', 'create_time');

        $conditions['Extends']['GROUP'] = 'account';

        $res = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $statusAccount = [];
        $keyVal = [];
        if ($res) {
            foreach ($res as $value) {
                array_push($statusAccount, $value['account']);
                $keyVal[$value['account']] = $value['create_time'];
            }
        }

        $load_time = 0;

        if (!empty($statusAccount) && $status != 1 && $status != 9) {
            //获取该状态的上一步
            $step = array_values(CDict::$newNumStep);
            $key = array_search($status, $step);
            if ($key !== false) {
                $lastStatus = $step[$key - 1];
                $conditions['WHERE']['status'] = $lastStatus;
                $conditions['WHERE']['account::IN'] = $statusAccount;
                $lastRes = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
                if (!empty($lastRes)) {
                    foreach ($lastRes as $val) {
                        if ($keyVal[$val['account']] > $val['create_time']) {
                            $load_time += $keyVal[$val['account']] - $val['create_time'];
                        }
                    }
                }
            }
        }

        $data['account'] = $statusAccount;
        $data['load_time'] = $load_time;

        parent::__construct($old_tableName);

        return $data;
    }

    //判断账号是否为滚服玩家
    public function isRollServerAccount($account, $server, $status = 0)
    {
        $conditions = array();
        $conditions['WHERE']['account'] = $account;
        $conditions['WHERE']['server::!='] = $server;
        $status && $conditions['WHERE']['status'] = $status;

        $res = $this->getRow('id', $conditions['WHERE']);

        if ($res) {
            return true;
        }

        return false;
    }

    public function addNewAccountForServer(array $data)
    {
        $insert = [];

        $statusArr = [
            0 => CDict::$recordStep['StartLoadLib'],
            1 => CDict::$recordStep['LoadedAllLib'],
        ];

        foreach ($statusArr as $status) {
            $dat = $data;
            $dat['status'] = $status;
            $dat['first'] = $status == 1 ? 1 : 0;

            $insert[] = $dat;
        }

        return $this->multiAdd($insert);
    }

    //获取服务器的新增用户
    public function getNewAccount($start_time, $end_time, $server, $package = '', array $accounts = [])
    {
        $tableName = (new SplitSqlUtil($this->tableName))->returnDateTableName([$start_time, $end_time]);

        if (!$tableName) {
            return [];
        }

        $old_tableName = $this->tableName;

        if ($tableName != $this->tableName) {
            parent::__construct($tableName);
        }

        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $package && $conditions['WHERE']['package'] = $package;
        $accounts && $conditions['WHERE']['accounts'] = $accounts;
        $conditions['WHERE']['first'] = 1;
        $conditions['WHERE']['status'] = CDict::$recordStep['StartLoadLib'];

        $res = $this->getRows('distinct account as account', $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                array_push($data, $value['account']);
            }
        }

        parent::__construct($old_tableName);

        return $data;
    }

    //获取服务器的新增用户数
    public function getNewAccountNum($start_time, $end_time, $server, $package = '', array $accounts = [])
    {
        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']['server'] = $server;
        $package && $conditions['WHERE']['package'] = $package;
        $accounts && $conditions['WHERE']['accounts'] = $accounts;
        $conditions['WHERE']['first'] = 1;
        $conditions['WHERE']['status'] = CDict::$recordStep['StartLoadLib'];

        $res = $this->getRow('count(distinct account) as count_account', $conditions['WHERE']);

        $count = $res['count_account'] ? $res['count_account'] : 0;

        return $count;
    }

    public function getDataOnServerId($start_time, $end_time, $server)
    {
        $fields = ['channel', 'package', 'mac', 'server', 'account', 'create_time'];

        $conditions = array();
        $conditions['WHERE']["create_time::>="] = $start_time;
        $conditions['WHERE']["create_time::<="] = $end_time;
        $conditions['WHERE']["status::>="] = CDict::$recordStep['StartConnectServer'];
        $conditions['WHERE']['server::IN'] = $server;

        $res = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                $value['ip'] = 0;
                $data[$value['account']] = $value;
            }
        }

        return $data;
    }
}