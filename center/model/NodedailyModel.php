<?php
class NodedailyModel extends Model{
    public function __construct(){
        parent::__construct('node_daily');
        $this->alias = 'nd';
    }

    public function getNodedailyByDate($date){
        $fields = array(
            'date' , 'device_num' , 'one' , 'two' , 'three' , 'four' , 'five' ,
            'six' , 'seven' , 'eight' , 'nine' , 'ten' , 'eleven' , 'twleve'
        );
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        return $this->getRow($fields , $conditions['WHERE']);
    }

    public function setNodedaily($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

    public function node_loss_data($conditions){
        $fields = array(
            "from_unixtime(date,'%Y-%m-%d') as date" , 'device_num' , 'one' , 'two' , 'three' ,
            'four' , 'five' , 'six' , 'seven' , 'eight' , 'nine' , 'ten' , 'eleven' , 'twleve'
        );
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $this->getCount());
    }
    public function node_loss_export($conditions){
        unset($conditions['Extends']['Limit']);
        $fields = array(
            "from_unixtime(date,'%Y-%m-%d') as date" , 'device_num' , 'one' , 'two' , 'three' , 'four' , 'five' ,
            'six' , 'seven' , 'eight' , 'nine' , 'ten' , 'eleven' , 'twleve'
        );
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $result = array();
        $result[] = array(
            '日期' , '新增设备数' ,
            '成功进入注册页面（人数）' , '注册完毕进入游戏创角页面（人数）' , '完成创建角色（人数）' ,
            '进入加载页面（人数）' , '加载完成进入装逼副本（人数）' , '清完第一波小怪（人数）' ,
            '打完敌对NPC（人数）' , '打完装逼副本第1波BOSS（人数）' , '打完装逼副本第2波小怪（人数）' ,
            '打完装逼副本BOSS进入故事包装页（人数）' , '故事页播放完进入加载界面（人数）' , '进入新手村（人数）'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '节点流失（' . date('Y年m月d日H时i分s秒') . '）');
    }
}