<?php

class NodedailyhourModel extends Model
{

    public $statusArr = array('one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twleve', 'thirteen', 'fourteen', 'fifteen');

    public $sumStatusArr = array('sum(one) as one', 'sum(two) as two', 'sum(three) as three', 'sum(four) as four', 'sum(five) as five', 'sum(six) as six', 'sum(seven) as seven', 'sum(eight) as eight', 'sum(nine) as nine', 'sum(ten) as ten', 'sum(eleven) as eleven', 'sum(twleve) as twleve', 'sum(thirteen) as thirteen', 'sum(fourteen) as fourteen', 'sum(fifteen) as fifteen');

    public $loadTimeArr = array('two_load_time', 'three_load_time', 'four_load_time', 'five_load_time', 'six_load_time', 'seven_load_time', 'eight_load_time', 'nine_load_time', 'ten_load_time', 'eleven_load_time', 'twleve_load_time', 'thirteen_load_time', 'fourteen_load_time', 'fifteen_load_time');

    public $sumLoadTimeArr = array('sum(two_load_time) as two_load_time', 'sum(three_load_time) as three_load_time', 'sum(four_load_time) as four_load_time', 'sum(five_load_time) as five_load_time', 'sum(six_load_time) as six_load_time', 'sum(seven_load_time) as seven_load_time', 'sum(eight_load_time) as eight_load_time', 'sum(nine_load_time) as nine_load_time', 'sum(ten_load_time) as ten_load_time', 'sum(eleven_load_time) as eleven_load_time', 'sum(twleve_load_time) as twleve_load_time', 'sum(thirteen_load_time) as thirteen_load_time', 'sum(fourteen_load_time) as fourteen_load_time', 'sum(fifteen_load_time) as fifteen_load_time');

    public function __construct()
    {
        parent::__construct('node_daily_hour');
        $this->alias = 'nd';
    }

    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d %H')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }

    public function setNodedaily($data, $date = '')
    {
        if (empty($date)) {
            $this->add($data);
        } else {
            $conditions = array();
            $conditions['WHERE']["date"] = $date;
            $this->update($data, $conditions['WHERE']);
        }
    }

    public function perHour_data($conditions)
    {
        $rs = array();
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            //如果没有选择服务器则找出最新的服务器
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999

        if (!isset($conditions['WHERE']['server::IN'])) return false;

        $conditions['WHERE']['nd.create_time::>='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['nd.create_time::<='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) + 86399 : strtotime(date('Y-m-d', time())) + 86399;
        unset($conditions['WHERE']['create_time']);
        //unset($conditions['WHERE']['order']);

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = nd.server'),
        );
        $fields = array('s.name as server', "from_unixtime(nd.create_time,'%Y-%m-%d %H') as create_time");

        $fields1 = array_merge($fields, ['new_active'], $this->statusArr, $this->loadTimeArr);

        $arr = $this->getRows($fields1, $conditions['WHERE'], $conditions['Extends']);

        $fields2 = array_merge($fields, ['sum(new_active) as new_active'], $this->sumStatusArr, $this->sumLoadTimeArr);

        $all = $this->getRows($fields2, $conditions['WHERE'], $conditions['Extends']);

        $arr = array_merge($all, $arr);

        $num = count($arr);

        if ($arr) {
            foreach ($arr as $k => $row) {
//            $rs[$k]['channel']=$row['channel'];
                $rs[$k]['create_time'] = $row['create_time'] . '时';
                $rs[$k]['server'] = $row['server'];
                //平台新增跳转数
                $rs[$k]['new_active'] = $row['new_active'];

                $rs[$k]['one'] = $row['one'];
                $rs[$k]['one2'] = $row['one'] ? @sprintf("%.2f", $row['one'] / $row['new_active'] * 100) . '%' : '0%';

                $two_avg_time = $row['two_load_time'] && $row['two'] ? round($row['two_load_time'] / $row['two'], 2) : 0;

                $rs[$k]['two'] = $row['two'] . '(' . $two_avg_time . 's)';
                $rs[$k]['two1'] = $row['two'] ? @sprintf("%.2f", $row['two'] / $row['one'] * 100) . '%' : '0%';
                $rs[$k]['two2'] = $row['two'] ? @sprintf("%.2f", $row['two'] / $row['new_active'] * 100) . '%' : '0%';
                /*
                $rs[$k]['three'] = $row['three'];
                $rs[$k]['three1'] = $row['three'] ? @sprintf("%.2f", $row['three'] / $row['two'] * 100) . '%' : '0%';
                $rs[$k]['three2'] = $row['three'] ? @sprintf("%.2f", $row['three'] / $row['new_active'] * 100) . '%' : '0%';*/

                $four_avg_time = $row['four_load_time'] && $row['four'] ? round($row['four_load_time'] / $row['four'], 2) : 0;

                $rs[$k]['four'] = $row['four'] . '(' . $four_avg_time . 's)';
                $rs[$k]['four1'] = $row['four'] ? @sprintf("%.2f", $row['four'] / $row['two'] * 100) . '%' : '0%';
                $rs[$k]['four2'] = $row['four'] ? @sprintf("%.2f", $row['four'] / $row['new_active'] * 100) . '%' : '0%';

                $five_avg_time = $row['five_load_time'] && $row['five'] ? round($row['five_load_time'] / $row['five'], 2) : 0;
                $rs[$k]['five'] = $row['five'] . '(' . $five_avg_time . 's)';
                $rs[$k]['five1'] = $row['five'] ? @sprintf("%.2f", $row['five'] / $row['four'] * 100) . '%' : '0%';
                $rs[$k]['five2'] = $row['five'] ? @sprintf("%.2f", $row['five'] / $row['new_active'] * 100) . '%' : '0%';

                $six_avg_time = $row['six_load_time'] && $row['six'] ? round($row['six_load_time'] / $row['six'], 2) : 0;
                $rs[$k]['six'] = $row['six'] . '(' . $six_avg_time . 's)';
                $rs[$k]['six1'] = $row['six'] ? @sprintf("%.2f", $row['six'] / $row['five'] * 100) . '%' : '0%';
                $rs[$k]['six2'] = $row['six'] ? @sprintf("%.2f", $row['six'] / $row['new_active'] * 100) . '%' : '0%';

                $eight_avg_time = $row['eight_load_time'] && $row['eight'] ? round($row['eight_load_time'] / $row['eight'], 2) : 0;
                $rs[$k]['eight'] = $row['eight'] . '(' . $eight_avg_time . 's)';
                $rs[$k]['eight1'] = $row['eight'] ? @sprintf("%.2f", $row['eight'] / $row['six'] * 100) . '%' : '0%';
                $rs[$k]['eight2'] = $row['eight'] ? @sprintf("%.2f", $row['eight'] / $row['new_active'] * 100) . '%' : '0%';

                $ten_avg_time = $row['ten_load_time'] && $row['ten'] ? round($row['ten_load_time'] / $row['ten'], 2) : 0;
                $rs[$k]['ten'] = $row['ten'] . '(' . $ten_avg_time . 's)';
                $rs[$k]['ten1'] = $row['ten'] ? @sprintf("%.2f", $row['ten'] / $row['eight'] * 100) . '%' : '0%';
                $rs[$k]['ten2'] = $row['ten'] ? @sprintf("%.2f", $row['ten'] / $row['new_active'] * 100) . '%' : '0%';

                $eleven_avg_time = $row['eleven_load_time'] && $row['eleven'] ? round($row['eleven_load_time'] / $row['eleven'], 2) : 0;
                $rs[$k]['eleven'] = $row['eleven'] . '(' . $eleven_avg_time . 's)';
                $rs[$k]['eleven1'] = $row['eleven'] ? @sprintf("%.2f", $row['eleven'] / $row['ten'] * 100) . '%' : '0%';
                $rs[$k]['eleven2'] = $row['eleven'] ? @sprintf("%.2f", $row['eleven'] / $row['new_active'] * 100) . '%' : '0%';

                $seven_avg_time = $row['seven_load_time'] && $row['seven'] ? round($row['seven_load_time'] / $row['seven'], 2) : 0;
                $rs[$k]['seven'] = $row['seven'] . '(' . $seven_avg_time . 's)';
                $rs[$k]['seven1'] = $row['seven'] ? @sprintf("%.2f", $row['seven'] / $row['eleven'] * 100) . '%' : '0%';
                $rs[$k]['seven2'] = $row['seven'] ? @sprintf("%.2f", $row['seven'] / $row['new_active'] * 100) . '%' : '0%';

                $rs[$k]['nine'] = $row['nine'];

                $twleve_avg_time = $row['twleve_load_time'] && $row['twleve'] ? round($row['twleve_load_time'] / $row['twleve'], 2) : 0;
                $rs[$k]['twleve'] = $row['twleve'] . '(' . $twleve_avg_time . 's)';
                $rs[$k]['twleve1'] = $row['twleve'] ? @sprintf("%.2f", $row['twleve'] / $row['eleven'] * 100) . '%' : '0%';
                $rs[$k]['twleve2'] = $row['twleve'] ? @sprintf("%.2f", $row['twleve'] / $row['new_active'] * 100) . '%' : '0%';

                $thirteen_avg_time = $row['thirteen_load_time'] && $row['thirteen'] ? round($row['thirteen_load_time'] / $row['thirteen'], 2) : 0;
                $rs[$k]['thirteen'] = $row['thirteen'] . '(' . $thirteen_avg_time . 's)';
                $rs[$k]['thirteen1'] = $row['thirteen'] ? @sprintf("%.2f", $row['thirteen'] / $row['twleve'] * 100) . '%' : '0%';
                $rs[$k]['thirteen2'] = $row['thirteen'] ? @sprintf("%.2f", $row['thirteen'] / $row['new_active'] * 100) . '%' : '0%';

                $fourteen_avg_time = $row['fourteen_load_time'] && $row['fourteen'] ? round($row['fourteen_load_time'] / $row['fourteen'], 2) : 0;
                $rs[$k]['fourteen'] = $row['fourteen'] . '(' . $fourteen_avg_time . 's)';
                $rs[$k]['fourteen1'] = $row['fourteen'] ? @sprintf("%.2f", $row['fourteen'] / $row['thirteen'] * 100) . '%' : '0%';
                $rs[$k]['fourteen2'] = $row['fourteen'] ? @sprintf("%.2f", $row['fourteen'] / $row['new_active'] * 100) . '%' : '0%';

                $fifteen_avg_time = $row['fifteen_load_time'] && $row['fifteen'] ? round($row['fifteen_load_time'] / $row['fifteen'], 2) : 0;
                $rs[$k]['fifteen'] = $row['fifteen'] . '(' . $fifteen_avg_time . 's)';
                $rs[$k]['fifteen1'] = $row['fifteen'] ? @sprintf("%.2f", $row['fifteen'] / $row['fourteen'] * 100) . '%' : '0%';
                $rs[$k]['fifteen2'] = $row['fifteen'] ? @sprintf("%.2f", $row['fifteen'] / $row['new_active'] * 100) . '%' : '0%';

            }

            $rs[0]['create_time'] = '总计';
        }

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }

        $this->setPageData($rs, $num);
    }

    public function perHour_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        if (!isset($conditions['WHERE']['server::IN'])) {
            //如果没有选择服务器则找出最新的服务器
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999

        if (!isset($conditions['WHERE']['server::IN'])) return false;

        $conditions['WHERE']['nd.create_time::>='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['nd.create_time::<='] = isset($conditions['WHERE']['create_time']) ? strtotime($conditions['WHERE']['create_time']) + 86399 : strtotime(date('Y-m-d', time())) + 86399;
        unset($conditions['WHERE']['create_time']);

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = nd.server'),
        );
        $fields = array('s.name as server', "from_unixtime(nd.create_time,'%Y-%m-%d %H') as create_time");

        $fields1 = array_merge($fields, ['new_active'], $this->statusArr, $this->loadTimeArr);

        $arr = $this->getRows($fields1, $conditions['WHERE'], $conditions['Extends']);

        $fields2 = array_merge($fields, ['sum(new_active) as new_active'], $this->sumStatusArr, $this->sumLoadTimeArr);

        $all = $this->getRows($fields2, $conditions['WHERE'], $conditions['Extends']);

        $arr = array_merge($all, $arr);

        if ($arr) {
            foreach ($arr as $k => $row) {
//            $rs[$k]['channel']=$row['channel'];
                $rs[$k]['create_time'] = $row['create_time'] . '时';
                $rs[$k]['server'] = $row['server'];
                //平台新增跳转数
                $rs[$k]['new_active'] = $row['new_active'];

                $rs[$k]['one'] = $row['one'];
                $rs[$k]['one2'] = $row['one'] ? @sprintf("%.2f", $row['one'] / $row['new_active'] * 100) . '%' : '0%';

                $two_avg_time = $row['two_load_time'] && $row['two'] ? round($row['two_load_time'] / $row['two'], 2) : 0;

                $rs[$k]['two'] = $row['two'] . '(' . $two_avg_time . 's)';
                $rs[$k]['two1'] = $row['two'] ? @sprintf("%.2f", $row['two'] / $row['one'] * 100) . '%' : '0%';
                $rs[$k]['two2'] = $row['two'] ? @sprintf("%.2f", $row['two'] / $row['new_active'] * 100) . '%' : '0%';
                /*
                $rs[$k]['three'] = $row['three'];
                $rs[$k]['three1'] = $row['three'] ? @sprintf("%.2f", $row['three'] / $row['two'] * 100) . '%' : '0%';
                $rs[$k]['three2'] = $row['three'] ? @sprintf("%.2f", $row['three'] / $row['new_active'] * 100) . '%' : '0%';*/

                $four_avg_time = $row['four_load_time'] && $row['four'] ? round($row['four_load_time'] / $row['four'], 2) : 0;

                $rs[$k]['four'] = $row['four'] . '(' . $four_avg_time . 's)';
                $rs[$k]['four1'] = $row['four'] ? @sprintf("%.2f", $row['four'] / $row['two'] * 100) . '%' : '0%';
                $rs[$k]['four2'] = $row['four'] ? @sprintf("%.2f", $row['four'] / $row['new_active'] * 100) . '%' : '0%';

                $five_avg_time = $row['five_load_time'] && $row['five'] ? round($row['five_load_time'] / $row['five'], 2) : 0;
                $rs[$k]['five'] = $row['five'] . '(' . $five_avg_time . 's)';
                $rs[$k]['five1'] = $row['five'] ? @sprintf("%.2f", $row['five'] / $row['four'] * 100) . '%' : '0%';
                $rs[$k]['five2'] = $row['five'] ? @sprintf("%.2f", $row['five'] / $row['new_active'] * 100) . '%' : '0%';

                $six_avg_time = $row['six_load_time'] && $row['six'] ? round($row['six_load_time'] / $row['six'], 2) : 0;
                $rs[$k]['six'] = $row['six'] . '(' . $six_avg_time . 's)';
                $rs[$k]['six1'] = $row['six'] ? @sprintf("%.2f", $row['six'] / $row['five'] * 100) . '%' : '0%';
                $rs[$k]['six2'] = $row['six'] ? @sprintf("%.2f", $row['six'] / $row['new_active'] * 100) . '%' : '0%';

                $eight_avg_time = $row['eight_load_time'] && $row['eight'] ? round($row['eight_load_time'] / $row['eight'], 2) : 0;
                $rs[$k]['eight'] = $row['eight'] . '(' . $eight_avg_time . 's)';
                $rs[$k]['eight1'] = $row['eight'] ? @sprintf("%.2f", $row['eight'] / $row['six'] * 100) . '%' : '0%';
                $rs[$k]['eight2'] = $row['eight'] ? @sprintf("%.2f", $row['eight'] / $row['new_active'] * 100) . '%' : '0%';

                $ten_avg_time = $row['ten_load_time'] && $row['ten'] ? round($row['ten_load_time'] / $row['ten'], 2) : 0;
                $rs[$k]['ten'] = $row['ten'] . '(' . $ten_avg_time . 's)';
                $rs[$k]['ten1'] = $row['ten'] ? @sprintf("%.2f", $row['ten'] / $row['eight'] * 100) . '%' : '0%';
                $rs[$k]['ten2'] = $row['ten'] ? @sprintf("%.2f", $row['ten'] / $row['new_active'] * 100) . '%' : '0%';

                $eleven_avg_time = $row['eleven_load_time'] && $row['eleven'] ? round($row['eleven_load_time'] / $row['eleven'], 2) : 0;
                $rs[$k]['eleven'] = $row['eleven'] . '(' . $eleven_avg_time . 's)';
                $rs[$k]['eleven1'] = $row['eleven'] ? @sprintf("%.2f", $row['eleven'] / $row['ten'] * 100) . '%' : '0%';
                $rs[$k]['eleven2'] = $row['eleven'] ? @sprintf("%.2f", $row['eleven'] / $row['new_active'] * 100) . '%' : '0%';

                $seven_avg_time = $row['seven_load_time'] && $row['seven'] ? round($row['seven_load_time'] / $row['seven'], 2) : 0;
                $rs[$k]['seven'] = $row['seven'] . '(' . $seven_avg_time . 's)';
                $rs[$k]['seven1'] = $row['seven'] ? @sprintf("%.2f", $row['seven'] / $row['eleven'] * 100) . '%' : '0%';
                $rs[$k]['seven2'] = $row['seven'] ? @sprintf("%.2f", $row['seven'] / $row['new_active'] * 100) . '%' : '0%';

                $rs[$k]['nine'] = $row['nine'];

                $twleve_avg_time = $row['twleve_load_time'] && $row['twleve'] ? round($row['twleve_load_time'] / $row['twleve'], 2) : 0;
                $rs[$k]['twleve'] = $row['twleve'] . '(' . $twleve_avg_time . 's)';
                $rs[$k]['twleve1'] = $row['twleve'] ? @sprintf("%.2f", $row['twleve'] / $row['eleven'] * 100) . '%' : '0%';
                $rs[$k]['twleve2'] = $row['twleve'] ? @sprintf("%.2f", $row['twleve'] / $row['new_active'] * 100) . '%' : '0%';

                $thirteen_avg_time = $row['thirteen_load_time'] && $row['thirteen'] ? round($row['thirteen_load_time'] / $row['thirteen'], 2) : 0;
                $rs[$k]['thirteen'] = $row['thirteen'] . '(' . $thirteen_avg_time . 's)';
                $rs[$k]['thirteen1'] = $row['thirteen'] ? @sprintf("%.2f", $row['thirteen'] / $row['twleve'] * 100) . '%' : '0%';
                $rs[$k]['thirteen2'] = $row['thirteen'] ? @sprintf("%.2f", $row['thirteen'] / $row['new_active'] * 100) . '%' : '0%';

                $fourteen_avg_time = $row['fourteen_load_time'] && $row['fourteen'] ? round($row['fourteen_load_time'] / $row['fourteen'], 2) : 0;
                $rs[$k]['fourteen'] = $row['fourteen'] . '(' . $fourteen_avg_time . 's)';
                $rs[$k]['fourteen1'] = $row['fourteen'] ? @sprintf("%.2f", $row['fourteen'] / $row['thirteen'] * 100) . '%' : '0%';
                $rs[$k]['fourteen2'] = $row['fourteen'] ? @sprintf("%.2f", $row['fourteen'] / $row['new_active'] * 100) . '%' : '0%';

                $fifteen_avg_time = $row['fifteen_load_time'] && $row['fifteen'] ? round($row['fifteen_load_time'] / $row['fifteen'], 2) : 0;
                $rs[$k]['fifteen'] = $row['fifteen'] . '(' . $fifteen_avg_time . 's)';
                $rs[$k]['fifteen1'] = $row['fifteen'] ? @sprintf("%.2f", $row['fifteen'] / $row['fourteen'] * 100) . '%' : '0%';
                $rs[$k]['fifteen2'] = $row['fifteen'] ? @sprintf("%.2f", $row['fifteen'] / $row['new_active'] * 100) . '%' : '0%';
            }

            $rs[0]['create_time'] = '总计';
        }
        $result = array();

        $result[] = array('日期', '服务器', '平台新增跳转数', '加载类库', '加载类库成功率(初始)', '所有类库加载完成', '所有类库加载完成(上步)', '所有类库加载完成(初始)', /*'Web可用', 'Web可用率(上步)', 'Web可用率(初始)',*/
            '开始加载配置文件', '开始加载配置文件成功率(上步)', '开始加载配置文件成功率(初始)', '开始连接服务器', '开始连接服务器成功率(上步)', '开始连接服务器成功率(初始)', '连接服务器成功', '连接服务器成功成功率(上步)', '连接服务器成功成功率(初始)', '创角跳转', '创角跳转成功率(上步)', '创角跳转成功率(初始)', '开始创角', '开始创角成功率(上步)', '开始创角成功率(初始)', '创角成功', '创角成功成功率(上步)', '创角成功成功率(初始)', '登录成功', '登录成功成功率(上步)', '登录成功成功率(初始)', '登录失败', '开始进入场景', '开始进入场景成功率(上步)', '开始进入场景成功率(初始)', '开始加载通用资源', '开始加载通用资源成功率(上步)', '开始加载通用资源成功率(初始)', '通用资源加载完成', '通用资源加载完成成功率(上步)', '通用资源加载完成成功率(初始)', '进入场景', '进入场景成功率(上步)', '进入场景成功率(初始)');

        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::export_csv_2($result, '', '每小时统计数据（' . date('Y年m月d日') . '）.csv');
    }

    public function node_time_data($conditions)
    {
        $rs = array();
        $num = 0;

        $rs = $this->get_node_time_data($conditions);
        if ($rs) {
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }
        }

        $num = count($rs);
        $this->setPageData($rs, $num);
    }

    public function node_time_export($conditions)
    {
        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_node_time_data($conditions);

        $result = array();
        $result[] = array(
            '时间', '服务器', '平台新增跳转数', '加载类库', '平均时间->', '所有类库加载完成', '平均时间->',
            '开始加载配置文件', '平均时间->', '开始连接服务器', '平均时间->', '连接服务器成功', '平均时间->', '创角跳转', '平均时间->', '开始创角', '平均时间->', '创角成功', '平均时间->', '登录成功', '平均时间->', '登录失败', '开始进入场景', '平均时间->', '开始加载通用资源', '平均时间->', '通用资源加载完成', '平均时间->', '进入场景'
        );
        if ($rs) {
            foreach ($rs as $row) {
                $result[] = $this->formatFields($row);
            }
        }

        Util::exportExcel($result, '节点时间统计（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function get_node_time_data($conditions)
    {
        $rs = array();
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            //如果没有选择服务器则找出最新的服务器
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999

        if (!isset($conditions['WHERE']['server::IN'])) return false;

        $conditions['WHERE']['nd.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date("Y-m-d", time())) - 86400 * 7;
        $conditions['WHERE']['nd.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Y-m-d", time())) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = nd.server'),
        );
        $fields = array("from_unixtime(nd.create_time,'%Y-%m-%d') as create_time", 's.name as server');

        $fields2 = array_merge($fields, ['sum(new_active) as new_active'], $this->sumStatusArr, $this->sumLoadTimeArr);

        $conditions['Extends']['GROUP'] = ['server', 'create_time'];

        $res = $this->getRows($fields2, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['Extends']['GROUP']);

        $all = $this->getRows($fields2, $conditions['WHERE'], $conditions['Extends']);

        $arr = array_merge($all, $res);

        if ($arr) {
            foreach ($arr as $k => $row) {
                $rs[$k]['create_time'] = $row['create_time'];
                $rs[$k]['server'] = $row['server'];
                //平台新增跳转数
                $rs[$k]['new_active'] = $row['new_active'];

                $rs[$k]['one'] = $row['one'];
                $two_avg_time = $row['two_load_time'] && $row['two'] ? round($row['two_load_time'] / $row['two'], 2) : 0;
                $rs[$k]['two1'] = $two_avg_time;
                $rs[$k]['two'] = $row['two'];

                $four_avg_time = $row['four_load_time'] && $row['four'] ? round($row['four_load_time'] / $row['four'], 2) : 0;
                $rs[$k]['four1'] = $four_avg_time;
                $rs[$k]['four'] = $row['four'];


                $five_avg_time = $row['five_load_time'] && $row['five'] ? round($row['five_load_time'] / $row['five'], 2) : 0;
                $rs[$k]['five1'] = $five_avg_time;
                $rs[$k]['five'] = $row['five'];


                $six_avg_time = $row['six_load_time'] && $row['six'] ? round($row['six_load_time'] / $row['six'], 2) : 0;
                $rs[$k]['six1'] = $six_avg_time;
                $rs[$k]['six'] = $row['six'];


                $eight_avg_time = $row['eight_load_time'] && $row['eight'] ? round($row['eight_load_time'] / $row['eight'], 2) : 0;
                $rs[$k]['eight1'] = $eight_avg_time;
                $rs[$k]['eight'] = $row['eight'];


                $ten_avg_time = $row['ten_load_time'] && $row['ten'] ? round($row['ten_load_time'] / $row['ten'], 2) : 0;
                $rs[$k]['ten1'] = $ten_avg_time;
                $rs[$k]['ten'] = $row['ten'];

                $eleven_avg_time = $row['eleven_load_time'] && $row['eleven'] ? round($row['eleven_load_time'] / $row['eleven'], 2) : 0;
                $rs[$k]['eleven1'] = $eleven_avg_time;
                $rs[$k]['eleven'] = $row['eleven'];

                $seven_avg_time = $row['seven_load_time'] && $row['seven'] ? round($row['seven_load_time'] / $row['seven'], 2) : 0;
                $rs[$k]['seven1'] = $seven_avg_time;
                $rs[$k]['seven'] = $row['seven'];

                $rs[$k]['nine'] = $row['nine'];

                $twleve_avg_time = $row['twleve_load_time'] && $row['twleve'] ? round($row['twleve_load_time'] / $row['twleve'], 2) : 0;
                $rs[$k]['twleve1'] = $twleve_avg_time;
                $rs[$k]['twleve'] = $row['twleve'];

                $thirteen_avg_time = $row['thirteen_load_time'] && $row['thirteen'] ? round($row['thirteen_load_time'] / $row['thirteen'], 2) : 0;
                $rs[$k]['thirteen1'] = $thirteen_avg_time;
                $rs[$k]['thirteen'] = $row['thirteen'];


                $fourteen_avg_time = $row['fourteen_load_time'] && $row['fourteen'] ? round($row['fourteen_load_time'] / $row['fourteen'], 2) : 0;
                $rs[$k]['fourteen1'] = $fourteen_avg_time;
                $rs[$k]['fourteen'] = $row['fourteen'];

                $fifteen_avg_time = $row['fifteen_load_time'] && $row['fifteen'] ? round($row['fifteen_load_time'] / $row['fifteen'], 2) : 0;
                $rs[$k]['fifteen1'] = $fifteen_avg_time;
                $rs[$k]['fifteen'] = $row['fifteen'];

            }

            $rs[0]['create_time'] = '总计';

            if (count($conditions['WHERE']['server::IN']) > 1) {
                $rs[0]['server'] = '-';
            }
        }

        return $rs;
    }
}