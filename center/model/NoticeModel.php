<?php

class NoticeModel extends Model{

    public function __construct(){
        parent::__construct('notice');
        $this->alias = 'n';
    }


    public function record_data(){
		
		$rs  = array();
		$num = 0;
/*
		if($_SESSION['group_channel'] != 'all'){

			$group = $this->getChannelGroupByTrait();

			$group = array_column($group, 'id');
			$group = implode(',' , $group);

			$server = $this->getServerByTrait(); 
			
			if(!empty($server) && !empty($group)){

				foreach ($server as $key => $value) {
					foreach($value as $k => $v){
						$arr[] = $k;
					}
				}
				unset($server);
				
				foreach ($arr as $kk => $vv) {
	              $array[]= "find_in_set('$vv',servers)";
	            }


	            $array = implode(' or ' , $array);
	            $where = "  or groups in ($group)  or  $array  ";
			}else{
				$where = ' ';
			}
		
			$sql = "select id,groups , servers , type,title,admin  from ny_notice where 
				  (groups = '' and servers = '')  $where ";
			
			$result = $this->query($sql);
		}else{


		} */

        $result = $this->getRows(array('id', 'groups', 'servers', 'contents', 'admin', 'status'), ['status::<' => 2]);

		if($result){
			foreach($result as $k => $row){
				
				//转化渠道
				$group='';
				if($row['groups'] != ''){
					$groupid = $row['groups'];
					$sql= "select name from ny_channel_group where id in ($groupid)";
					$result = $this->query($sql);
					$group = array_column($result , 'name');
					$group = implode(',', $group);
					
				}else{
					$group = '全部渠道组';
				}
				$row['groups']  = $group;


				//转化渠道
				$server='';
				if($row['servers'] != ''){
					$serverid = $row['servers'];
					$serverid = explode(',', $serverid);

					$serverid = "'" . implode("','", $serverid)."'";

					$sql= "select name from ny_server where server_id in ($serverid)";
					$result = $this->query($sql);
					$server = array_column($result , 'name');
					$server = implode(',', $server);
					
				}else{

					$server = '全部服务器';

				}

				$row['servers']  = $server;
                /*
                                 switch($row['type']){
                                    case 0 :
                                        $type = '<button class="btn btn-primary">新服公告</button>';break;
                                    case 1 :
                                        $type = '<button class="btn btn-default">通用公告</button>';break;
                                    case 2 :
                                        $type = '<button class="btn btn-warning">活动公告</button>';break;
                                    case 3 :
                                        $type = '<button class="btn btn-error">合服公告</button>';break;
                                }

                                $row['type'] = $type; */

                $row['contents'] = '<input type="button" class="gbutton" value="查看" onclick="content(' . $row['id'] . ')">';

                $row['status'] = $row['status'] == 1 ? '<button class="btn btn-success">已发布</button>' : '<input type="button" class="gbutton" value="同意" onclick="notice(1,' . $row['id'] . ');"> | <input type="button" class="gbutton" value="拒绝" onclick="notice(2,' . $row['id'] . ');">';

                $row['options'] .= '<input type="button" class="gbutton" value="编辑" onclick="edit(' . $row['id'] . ')">';
				$row['options'] .= '<input type="button" class="gbutton" value="删除 " style="margin-top: 5px;" onclick="del(' . $row['id'] . ')">';
				$rs[$k] = array_values($row);
			}	
			$num = count($rs);
		}

        $this->setPageData($rs, $num);
    }

    public function add_notice($data){

        if ($data['OperatingState'] == 'add') {
            unset($data['OperatingState']);
            unset($data['id']);
            return $this->add($data);
			
        }else{
            unset($data['OperatingState']);
            return $this->update($data , array('id'=> $data['id']));
        }        
    }
    /**
     * [notice 更新公告发布状态]
     * @param  [int] $id   [发布的公告id]
     * @param  [int] $type [修改的公告状态]
     * @return [int]       [返回修改状态]
     */
	public function notice($id,$type){
		
		return $this->update(array('status' => $type ,'admin' => $_SESSION['username'], 'notice_time' => time()) , array('id' => $id));
	}
	/**
	 * [getGroupName 根据传入的渠道组id查询渠道组名称]
	 * @param  [string] $id [渠道组id的字符串，用,隔开]
	 * @return [string]     [渠道组名称]
	 */
	public function getGroupName($id){
		$sql= "select name from ny_channel_group where id in ($id)";
		$result = $this->query($sql);
		$group = array_column($result , 'name');
		$group = implode(',', $group);
		return $group;
	}

    /**
     * [同步公告] 说明，全渠道
     * @param array $serverMsg
     * @version 2017-09-29
     */
    public static function syncNotice($serverMsg)
    {
        if (empty($serverMsg)) {
            return true;
        }
        //得到新开服数据
        $NoticeModel = new NoticeModel();
        $fields = ['id', 'groups', 'servers', 'contents'];
        $res = $NoticeModel->getRows($fields, ['status' => 1]);
        empty($res) && $res = array();
        if (empty($res)) return false;
        $data = [];

        $servers = [];

        //得到渠道下所有全服的兑换包
        foreach ($res as $row) {
            foreach ($serverMsg as $k => $value) {
                $giftAgent = explode(',', $row['agent']);

                if (in_array($k, $giftAgent) || $row['agent'] == '') {
                    foreach ($serverMsg[$k] as $server_id) {
                        $servers[$server_id][] = $row;
                    }
                }
            }
        }
        unset($row);

        if (empty($servers)) return false;

        $serverList = array_keys($servers);

        $instance = new ServerModel();
        $server_tmp_config = $instance->getServer($serverList, ['s.server_id as server_id', 's.name as name', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port']);

        if (empty($server_tmp_config)) return false;

        $server_config = [];

        foreach ($server_tmp_config as $s) {
            $server_config[$s['server_id']] = $s;
        }

        $backMsg = '';
        foreach ($servers as $key => $item) {

            foreach ($item as $row) {

                $websocket_host = $server_config[$key]['websocket_host'];
                $websocket_port = $server_config[$key]['websocket_port'];

                $tmpMsg = 'notice_id:' . $row['id'] . ' | server_id:' . $key;

                //发送给后端
                $sendInfo = [];
                $sendInfo['opcode'] = Pact::$UpdateNotice;

                $contents = htmlspecialchars_decode(stripslashes($row['contents']));

                if ($contents == '') {
                    continue;
                }

                $sendInfo['str'] = [$contents];

                $msg = $NoticeModel->socketCall($websocket_host, $websocket_port, 'giftCDKEY', $sendInfo);

                if ($msg === 0) {

                    $servers = $row['servers'];

                    if ($servers != '') {
                        $servers .= ','.$key;
                    }
                    $NoticeModel->update(['servers' => $servers], ['id' => $row['id']]);

                    $tmpMsg .= ' 发送成功';
                } else {
                    $tmpMsg .= ' 发送失败';
                }

                $backMsg .= $tmpMsg . PHP_EOL;

            }
        }
        if ($backMsg != '') {
            Helper::log($backMsg, 'syncNotice', 'info', 'server');//记录每次同步日志
        }
    }

}