<?php

class OnlineModel extends Model{

    public function __construct(){
        parent::__construct('online_hook');
        $this->alias = 'oh';
    }

    public function real_data($conditions){
        $limit = $conditions['Extends']['LIMIT'];
        $rs = array();
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Online' , 'getRealData' , array('conditions' => $conditions) , $server_id);
            $rs = $rs[$server_id];
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , count($rs) , $limit);
    }
    public function real_export($conditions){
        $rs = array();
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Online' , 'getRealData' , array('conditions' => $conditions) , $server_id);
            $rs = $rs[$server_id];
        }
        $result = array();
        $result[] = array('时间' , '登录人数');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '实时在线（' . date('Y年m月d日H时i分s秒') . '）');
    }
}