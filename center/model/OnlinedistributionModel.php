<?php

class OnlinedistributionModel extends Model
{

    public function __construct()
    {
        parent::__construct('online_distribution');
        $this->alias = 'a';
    }

    public function distribution_data($conditions)
    {
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", '`(0,1]`', '`(1,5]`', '`(5,10]`', '`(10,20]`', '`(20,30]`', '`(30,60]`', '`(60,90]`', '`(90,120]`', '`(120,150]`', '`(150,180]`', '`(180,240]`', '`(240,300]`', '`(300,1440]`', '`(1440,-]`');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
//            $rs[$k] = array_values($row);
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $active_num = $row['active_num'];
                $data[$k][2] = $active_num;
                $data[$k][3] = $row['(0,1]'] ? @sprintf("%.2f", $row['(0,1]'] / $active_num * 100) . '%' : '0%';
                $data[$k][4] = $row['(1,5]'] ? @sprintf("%.2f", $row['(1,5]'] / $active_num * 100) . '%' : '0%';
                $data[$k][5] = $row['(5,10]'] ? @sprintf("%.2f", $row['(5,10]'] / $active_num * 100) . '%' : '0%';
                $data[$k][6] = $row['(10,20]'] ? @sprintf("%.2f", $row['(10,20]'] / $active_num * 100) . '%' : '0%';
                $data[$k][7] = $row['(20,30]'] ? @sprintf("%.2f", $row['(20,30]'] / $active_num * 100) . '%' : '0%';
                $data[$k][8] = $row['(30,60]'] ? @sprintf("%.2f", $row['(30,60]'] / $active_num * 100) . '%' : '0%';
                $data[$k][9] = $row['(60,90]'] ? @sprintf("%.2f", $row['(60,90]'] / $active_num * 100) . '%' : '0%';
                $data[$k][10] = $row['(90,120]'] ? @sprintf("%.2f", $row['(90,120]'] / $active_num * 100) . '%' : '0%';
                $data[$k][11] = $row['(120,150]'] ? @sprintf("%.2f", $row['(120,150]'] / $active_num * 100) . '%' : '0%';
                $data[$k][12] = $row['(150,180]'] ? @sprintf("%.2f", $row['(150,180]'] / $active_num * 100) . '%' : '0%';
                $data[$k][13] = $row['(180,240]'] ? @sprintf("%.2f", $row['(180,240]'] / $active_num * 100) . '%' : '0%';
                $data[$k][14] = $row['(240,300]'] ? @sprintf("%.2f", $row['(240,300]'] / $active_num * 100) . '%' : '0%';
                $data[$k][15] = $row['(300,1440]'] ? @sprintf("%.2f", $row['(300,1440]'] / $active_num * 100) . '%' : '0%';
                $data[$k][16] = $row['(1440,-]'] ? @sprintf("%.2f", $row['(1440,-]'] / $active_num * 100) . '%' : '0%';
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function distribution_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", '`(0,1]`', '`(1,5]`', '`(5,10]`', '`(10,20]`', '`(20,30]`', '`(30,60]`', '`(60,90]`', '`(90,120]`', '`(120,150]`', '`(150,180]`', '`(180,240]`', '`(240,300]`', '`(300,1440]`', '`(1440,-]`');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
//            $rs[$k] = array_values($row);
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $active_num = $row['active_num'];
                $data[$k][2] = $active_num;
                $data[$k][3] = $row['(0,1]'] ? @sprintf("%.2f", $row['(0,1]'] / $active_num * 100) . '%' : '0%';
                $data[$k][4] = $row['(1,5]'] ? @sprintf("%.2f", $row['(1,5]'] / $active_num * 100) . '%' : '0%';
                $data[$k][5] = $row['(5,10]'] ? @sprintf("%.2f", $row['(5,10]'] / $active_num * 100) . '%' : '0%';
                $data[$k][6] = $row['(10,20]'] ? @sprintf("%.2f", $row['(10,20]'] / $active_num * 100) . '%' : '0%';
                $data[$k][7] = $row['(20,30]'] ? @sprintf("%.2f", $row['(20,30]'] / $active_num * 100) . '%' : '0%';
                $data[$k][8] = $row['(30,60]'] ? @sprintf("%.2f", $row['(30,60]'] / $active_num * 100) . '%' : '0%';
                $data[$k][9] = $row['(60,90]'] ? @sprintf("%.2f", $row['(60,90]'] / $active_num * 100) . '%' : '0%';
                $data[$k][10] = $row['(90,120]'] ? @sprintf("%.2f", $row['(90,120]'] / $active_num * 100) . '%' : '0%';
                $data[$k][11] = $row['(120,150]'] ? @sprintf("%.2f", $row['(120,150]'] / $active_num * 100) . '%' : '0%';
                $data[$k][12] = $row['(150,180]'] ? @sprintf("%.2f", $row['(150,180]'] / $active_num * 100) . '%' : '0%';
                $data[$k][13] = $row['(180,240]'] ? @sprintf("%.2f", $row['(180,240]'] / $active_num * 100) . '%' : '0%';
                $data[$k][14] = $row['(240,300]'] ? @sprintf("%.2f", $row['(240,300]'] / $active_num * 100) . '%' : '0%';
                $data[$k][15] = $row['(300,1440]'] ? @sprintf("%.2f", $row['(300,1440]'] / $active_num * 100) . '%' : '0%';
                $data[$k][16] = $row['(1440,-]'] ? @sprintf("%.2f", $row['(1440,-]'] / $active_num * 100) . '%' : '0%';
            }
        }

        $result = [];

        $result[] = array('日期', '服务器', '活跃人数', '(0, 1]', '(1, 5]', '(5, 10]', '(10, 20]', '(20, 30]', '(30, 60]', '(60, 90]', '(90, 120]', '(120, 150]', '(150, 180]', '(180, 240]', '(240, 300]', '(300, 1440]', '(1440, -]');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '在线分布（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('*');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}