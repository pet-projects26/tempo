<?php

class PackageModel extends Model{

    public function __construct(){
        parent::__construct('package');
        $this->alias = 'p';
        $this->joinTable = array(
            'c' => array('name' => 'channel' , 'type' => 'LEFT' , 'on' => 'p.channel_id = c.channel_id')
        );
    }

    public function getPackage($package_id = '' , $fields = array() , $group = '' , $start = '' , $end = ''){
        empty($fields) && $fields = array('p.id' , 'p.channel_id' ,  'p.group_id' , 'p.package_id' , 'p.name' , 'p.platform',
            'p.create_time', 'p.game_name', 'p.review_num', 'p.test_server_num', 'p.url', 'p.cdn_url', 'p.rv_param', ' p.type');
        $conditions = array();
        if(is_array($package_id)){
            $package_id && $conditions['WHERE']['p.package_id::IN'] = $package_id;
            $start && $conditions['WHERE']['p.create_time::>='] = $start;
            $end && $conditions['WHERE']['p.create_time::<='] = $end;
            $group && $conditions['Extends']['GROUP'] = $group;
            return $this->getRows($fields , $conditions['WHERE']);
        }
        else{
            $package_id && $conditions['WHERE']['p.package_id'] = $package_id;
            return $this->getRow($fields , $conditions['WHERE']); 
        }
    }

    public function getPackageByChannel($channel , $fields = array()){
        empty($fields) && $fields = array('id' , 'channel_id'  , 'package_id' , 'name' , 'create_time');
        $conditions = array();
        $key = is_array($channel) ? 'channel_id::IN' : 'channel_id';
        $conditions['WHERE'][$key] = $channel;
        return $this->getRows($fields , $conditions['WHERE']);
    }

    public function record_data($conditions){
        //$rs = $this->getPackage(array() , array('id' , 'channel_id' , 'package_id' , 'group_id' , 'create_time'));
		$rs = $this->getRows(
            array(
                'p.package_id', 'p.name', 'c.game_name', 'c.game_id', 'p.channel_id', 'p.group_id', 'p.create_time', 'p.url', 'c.cdn_url', 'p.platform'),
                $conditions['WHERE'] ,
                $conditions['Extends']
            );
        $num = $this->getCount();
        $rsc = (new ChannelModel)->getChannel(array() , array('channel_id' , 'name'));
		$res = (new ChannelgroupModel)->getChannelGroup(array() , array('id' , 'name'));
	
        $channel = array();
        foreach($rsc as $k => $row){
            $channel[$row['channel_id']] = $row['name'];
        }
		$channelgroup = array();
		foreach($res as $k=> $row){
			$channelgroup[$row['id']] =$row['name'];
		}
        foreach($rs as $k => $row){
            $row['create_time'] = date('Y-m-d H:i:s' , $row['create_time']);
            $row['options']  = '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['package_id'] . '\')">';
            $row['options'] .= '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['package_id'] . '\')">';
            $row['channel_id'] = $channel[$row['channel_id']] ? $channel[$row['channel_id']] : '<span style="color:#ff0000;">选择的渠道已被删除</span>';
			$row['group_id'] = $channelgroup[$row['group_id']] ? $channelgroup[$row['group_id']] : '<span style="color:#ff0000;">选择的渠道组已被删除</span>';
            $row['platform'] = $row['platform']==1?'安卓':($row['platform'] == 2?'IOS':'暂无');
            //$row['test_server_num'] = !empty($row['test_server_num'])?trim($row['test_server_num']):'';
            unset($row['id']);
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $num);
    }

    public function addPackage($data){
        if($data){
            return $this->add($data);
        }
    }

    public function editPackage($data , $package_id){
        if($data && $package_id){
            $conditions = array();
            $conditions['WHERE']['package_id'] = $package_id;
            return $this->update($data , $conditions['WHERE']);
        }
    }

    public function editPackages($data , $where){
        if($data && $where){
            return $this->update($data , $where);
        }
        else{
            return false;
        }
    }

    public function delPackage($package_id){
        if($package_id){
            $cgm = new ChannelgroupModel();
            $channelGroup = $cgm->getChannelGroup(array() , array('id' , 'channel_package'));
            foreach($channelGroup as $row){
                $row['channel_package'] = json_decode($row['channel_package'] , true);
                if (!empty($row['channel_package'])) {
                    foreach($row['channel_package'] as $channel => $package){
                        if(in_array($package_id , $package)){
                            $key = array_search($package_id , $package);
                            unset($row['channel_package'][$channel][$key]);
                            $data = array('channel_package' => json_encode($row['channel_package']));
                            $cgm->editChannelGroup($data , $row['id']);
                            break;
                        }
                    }
                }
            }
            $conditions = array();
            $conditions['WHERE']['package_id'] = $package_id;
            return $this->delete($conditions['WHERE']);
        }
    }

    public function getChannelPackage($name = false){
        $fields = array('p.channel_id' , 'p.package_id' , 'p.create_time' , 'c.name as channel_name');
        $rs = $this->getRows($fields);
        $rss = array();
        foreach($rs as $row){
            if(!$name){
                $rss[$row['channel_id']][] = $row['package_id'];
            }
            else{
                $rss[$row['channel_id']]['name'] = $row['channel_name'];
                $rss[$row['channel_id']]['package'][] = $row['package_id'];
            }
        }
        return $rss;
    }
	//获取全部包的详情
	public function getPackages(){
		$fileds=array('channel_id','package_id');
		return $this->getRows($fileds);
	}

    /**
     * 根据渠道组获取包信息
     * @param array $channel_group
     * @param array $fields
     * @param string $where
    */
    public function getPackagesByChannelGroup($channel_group = array(),$fields,$where = '1=1'){
        empty($fields) && $fields = array('p.id','p.group_id','p.channel_id','p.package_id','p.name','p.game_name');

        if(!empty($channel_group)){
            $where .= " and p.group_id in('".join('\',\'',$channel_group)."')";
        }
        $where .= " and p.platform = 2";//为IOS包
        $fields = join(',',$fields);
        $sql = "select ".$fields." from ny_package  as p left join ny_channel_group as g on p.group_id = g.id where ".$where."";
        $row = $this->query($sql);
        $output = array();
        foreach($row as $value){
            $output[$value['group_id']][$value['package_id']] = $value;
        }
        return $output;
    }

    //判断包唯一性
    public function getPackageUnique($field, $value, $old_value_package_id = '')
    {
        $conditions = [];

        $conditions['WHERE']["p." . $field] = $value;
        $old_value_package_id && $conditions['WHERE']['p.package_id::!='] = $old_value_package_id;

        $rs = $this->getRow('id', $conditions['WHERE']);

        return $rs;
    }

    public function getAllPackageName($type = 'channel')
    {

        $rs = $this->getRows(['p.package_id as package', 'c.name as channel_name', 'p.name as package_name']);

        $data = [];

        if (!empty($rs)) {

            foreach ($rs as $v) {

                $data[$v['package']] = $type == 'channel' ? $v['channel_name'] : $v['channel_name'] . '-' . $v['package_name'];
            }
        }

        return $data;
    }


}