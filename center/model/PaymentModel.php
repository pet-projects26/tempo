<?php

class PaymentModel extends Model{
    public function __construct(){
        parent::__construct('payment');
        $this->alias = 'p';
        $this->joinTable = array(
            'r' => array('name' => 'role' , 'type' => 'LEFT' , 'on' => 'r.role_id = p.role_id')
        );
    }
}