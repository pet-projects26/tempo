<?php
class PayrolesModel extends Model{
	
    public function __construct(){
        parent::__construct('payroles');
//         $this->alias = 'cd';
    }

    public function payRate_data($conditions){
    	
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){
    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['time::>=']) && $conditions['WHERE']['time::>='] = date("Y-m-d",$conditions['WHERE']['time::>=']);
    		isset($conditions['WHERE']['time::<=']) && $conditions['WHERE']['time::<='] = date("Y-m-d",$conditions['WHERE']['time::<=']);
    		 
    		$fields = array('server','time' ,'payRoles_1','loginRoles_1','payRoles_7' ,'loginRoles_7','payRoles_30','loginRoles_30');
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
//     		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$aaData[$row['time']][0] = $row['time'];
    			$aaData[$row['time']][1] += $row['payRoles_1'];
    			$aaData[$row['time']][2] += $row['loginRoles_1'];
    			$aaData[$row['time']][3] += $row['payRoles_7'];
    			$aaData[$row['time']][4] += $row['loginRoles_7'];
    			$aaData[$row['time']][5] += $row['payRoles_30'];
    			$aaData[$row['time']][6] += $row['loginRoles_30'];
    		}
    		
    		foreach ($aaData as $key => $value) {
    			$rs[$key][0] = $value[0];
    			$rs[$key][1] = $value[1] && $value[2] ? round($value[1] / $value[2], 4) * 100 . '%' : '-';
    			$rs[$key][2] = $value[3] && $value[4] ? round($value[3] / $value[4], 4) * 100 . '%' : '-';
    			$rs[$key][3] = $value[5] && $value[6] ? round($value[5] / $value[6], 4) * 100 . '%' : '-';
    		}
    		
    		$rs = array_values($rs);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , 0);
    	
    }
    public function payRate_export($conditions){
    	$rs = array();$num = 0;
    	if(isset($conditions['WHERE']['server::IN'])){

    		parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
    		isset($conditions['WHERE']['time::>=']) && $conditions['WHERE']['time::>='] = date("Y-m-d",$conditions['WHERE']['time::>=']);
    		isset($conditions['WHERE']['time::<=']) && $conditions['WHERE']['time::<='] = date("Y-m-d",$conditions['WHERE']['time::<=']);
    		 
    		$fields = array('server','time' ,'payRoles_1','loginRoles_1','payRoles_7' ,'loginRoles_7','payRoles_30','loginRoles_30');
    		$arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    		//     		echo $this->lastQuery();exit;
    		$num = $this->getCount();
    		foreach($arr as $k => $row){
    			$aaData[$row['time']][0] = $row['time'];
    			$aaData[$row['time']][1] += $row['payRoles_1'];
    			$aaData[$row['time']][2] += $row['loginRoles_1'];
    			$aaData[$row['time']][3] += $row['payRoles_7'];
    			$aaData[$row['time']][4] += $row['loginRoles_7'];
    			$aaData[$row['time']][5] += $row['payRoles_30'];
    			$aaData[$row['time']][6] += $row['loginRoles_30'];
    		}
    		
    		foreach ($aaData as $key => $value) {
    			$rs[$key][0] = $value[0];
    			$rs[$key][1] = $value[1] && $value[2] ? round($value[1] / $value[2], 4) * 100 . '%' : '-';
    			$rs[$key][2] = $value[3] && $value[4] ? round($value[3] / $value[4], 4) * 100 . '%' : '-';
    			$rs[$key][3] = $value[5] && $value[6] ? round($value[5] / $value[6], 4) * 100 . '%' : '-';
    		}
    		
    		$rs = array_values($rs);
    		foreach($rs as $k=>$row){
    			$rs[$k] = array_values($row);
    		}
    		
    		$fields = array();
    		$fields[] = array('日期' , '日付费率' ,'周付费率','月付费率');
    		foreach($rs as $row){
    			$fields[] = $this->formatFields($row);
    		}
    		Util::exportExcel($fields , '玩家付费率数据（' . date('Y年m月d日H时i分s秒') . '）');
    	}
    }
    
}