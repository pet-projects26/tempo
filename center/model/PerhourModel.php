<?php

class PerhourModel extends Model{

    public function __construct(){
        parent::__construct('perhour');
        $this->alias = 'o';
    }

    public function perHour_data($conditions)
    {

        $rs = array();$num = 0;
        if(isset($conditions['WHERE']['server::IN'])) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
        } else {
            //如果没有选择服务器则找出最新的服务器
            $conditions['WHERE']['server::IN'] = (new ServerModel())->getLatestServer();
        }

        //如果没有选择时间则默认为当天
        if (!isset($conditions['WHERE']['time::='])) {
            $conditions['WHERE']['time::='] = strtotime(date('Ymd'));
        }

        $order=$conditions['WHERE']['order'];
        isset($conditions['WHERE']['time::=']) && $conditions['WHERE']['o.time::='] = $conditions['WHERE']['time::='];
        unset($conditions['WHERE']['time::=']);
        unset($conditions['WHERE']['order']);

        //权限处理
        if (is_array($_SESSION['group_channel'])) {
            $channellist = array_keys($_SESSION['group_channel']);
            $conditions['WHERE']['o.channel::IN'] = $channellist;
        }

        $this->joinTable = array(
            'c' => array('name' => 'channel', 'type' => 'LEFT', 'on' => 'c.channel_id = o.channel'),
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
        );
        $fields = array( 'c.name as channel' , 's.name as server' ,'package','new_jumps',
            'login_sus' , 'create_role' , 'year' , 'time', 'month' , 'day', 'hour');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach($arr as $k => $row){
//            $rs[$k]['channel']=$row['channel'];
                $rs[$k]['time']=date('Y-m-d', $row['time']).' '.$row['hour'].'时';
                $rs[$k]['server']=$row['server'];
//            $rs[$k]['package']=$row['package'];
                //平台新增跳转数
                $rs[$k]['new_jumps']=$row['new_jumps'];
                //连接登录成功数
                $rs[$k]['login_sus']=$row['login_sus'];
                //连接成功率
                $rs[$k]['connection_success_rate'] = bcdiv(round(($row['login_sus'] / $row['new_jumps']), 2),100, 2);
                //创角数
                $rs[$k]['create_role']=$row['create_role'];
                //创角率
                $rs[$k]['create_role_rate'] = bcdiv(round(($row['create_role'] / $row['login_sus']), 2),100, 2);
            }
        }

        foreach($rs as $k=>$row){
            $rs[$k]=array_values($row);
        }

        $this->setPageData($rs , $num);
    }
    public function perHour_export($conditions)
    {

        $rs = array();

        if(isset($conditions['WHERE']['server::IN'])) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
        } else {
            //如果没有选择服务器则找出最新的服务器
            $conditions['WHERE']['server::IN'] = (new ServerModel())->getLatestServer();
        }

        //如果没有选择时间则默认为当天
        if (isset($conditions['WHERE']['time::='])) {
            $conditions['WHERE']['time::='] = strtotime(date('Ymd'));
        }

        unset($conditions['Extends']['LIMIT']);

        $order=$conditions['WHERE']['order'];
        isset($conditions['WHERE']['time::=']) && $conditions['WHERE']['o.time::='] = $conditions['WHERE']['time::='];
        unset($conditions['WHERE']['time::=']);
        unset($conditions['WHERE']['order']);

        //权限处理
        if (is_array($_SESSION['group_channel'])) {
            $channellist = array_keys($_SESSION['group_channel']);
            $conditions['WHERE']['o.channel::IN'] = $channellist;
        }

        $this->joinTable = array(
            'c' => array('name' => 'channel', 'type' => 'LEFT', 'on' => 'c.channel_id = o.channel'),
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = o.server'),
        );

        $fields = array( 'c.name as channel' , 's.name as server' ,'package','new_jumps',
            'login_sus' , 'create_role' , 'year' , 'time', 'month' , 'day', 'hour'
        );

        $arr = $this->getRows($fields , $conditions['WHERE'] ,$conditions['Extends'] );//
//         }
        $result = array();
        $result[] = array('服务器' , '时间', '平台新增跳转数' , '连接登录成功数' , '连接成功率' , '创角数', '创角率', '进入场景数', '进入率');

        foreach($arr as $k => $row){
//            $rs[$k]['channel']=$row['channel'];
            $rs[$k]['server']=$row['server'];
            $rs[$k]['time']=date('Y-m-d', $row['time']).' '.$row['hour'].'时';
//            $rs[$k]['package']=$row['package'];
            $rs[$k]['new_jumps']=$row['new_jumps'];
            $rs[$k]['login_sus']=$row['login_sus'];
            //连接成功率
            $rs[$k]['connection_success_rate'] = bcdiv(round(($row['login_sus'] / $row['new_jumps']), 2),100, 2);
            $rs[$k]['create_role']=$row['create_role'];
            //创角率
            $rs[$k]['create_role_rate'] = bcdiv(round(($row['create_role'] / $row['login_sus']), 2),100, 2);

            //进入场景数
            $rs[$k]['into_the_scene_num'] = '待统计';

            //进入率
            $rs[$k]['into_the_scene_rate'] = '待统计';
        }

        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::export_csv_2($result , '','每小时统计数据（' . date('Y年m月d日') . '）.csv');
    }

    //插入数据库
    public function insterData($data)
    {
        if (empty($data) || !is_array($data)) return false;

        //判断数据的时间决定inster与update
        $data['year'] = date('Y', $data['time']);
        $data['month'] = date('m', $data['time']);
        $data['day'] = date('d', $data['time']);
        $data['hour'] = date('H', $data['time']);

        //当前该小时的开始结束时间戳
        $start_time = strtotime(date('YmdH', $data['time']));
        $end_time = $start_time - 3599;

        //获取该服务器指定时间的登录人数
        $data['login_sus'] = $this->call('AccLogin', 'getAccountLoginNumByBetweenDate', ['date' => $start_time, 'end_date' => $end_time], $data['server']);
        //获取指定时间的创角数
        $data['create_role'] = $this->call('Role', 'getRoleByDate', ['type' => 'bcount', 'date' => $start_time, 'end_date' => $end_time], $data['server']);

        $conditions = [];
        $conditions['WHERE'] = [];

        $conditions['WHERE']['year::='] = $data['year'];
        $conditions['WHERE']['month::='] = $data['month'];
        $conditions['WHERE']['day::='] = $data['day'];
        $conditions['WHERE']['hour::='] = $data['hour'];
        $conditions['WHERE']['channel::='] = $data['channel'];
        $conditions['WHERE']['server::='] = $data['server'];
        $conditions['WHERE']['package::='] = $data['package'];

        $res = $this->getRow('id', $conditions['WHERE']);

        if ($res) {
            //update
            $sql = 'update '.$this->prefix.$this->tableName.' set new_jumps = new_jumps + '.$data['new_jumps'].', 
                login_sus = '.$data['login_sus'].', create_role = '.$data['create_role'].',
                update_time = '.$data['time'].' where id = '.$res['id'];

            return $this->query($sql);
        } else {
            //inster
            return $this->add($data);
        }
    }
}