<?php

class PlanModel extends Model{

    public function __construct(){
        parent::__construct('server_plan');
        $this->alias = 'sp';
        $this->cg=new ChannelgroupModel();
    }

    public function record_data($conditions){
    	$rs  = array();
		$num = 0;
		
        $result  = $this->getRows( array('id' ,'num' ,  'group_id' , 'name' , 'server_id' , 'is_config','open_time' ,'create_time' , 'is_config') , $conditions['WHERE'] , $conditions['Extends']);

        if($result){
        	$num = $this->getCount();

            //查选全部渠道组
            $groups = $this->cg->getRows(array('id' , 'name'));

            $group = array();
            foreach($groups as $k=>$v){
                $group[$v['id']] = $v['name']; 
            }

        	foreach ($result as $key => $value) {
        		$rs[$key]['num'] = $value['num'];
        		$rs[$key]['group_id'] = $value['group_id']. '('.$group[$value['group_id']].')';
        		$rs[$key]['name'] = $value['name'];
        		$rs[$key]['server_id'] = $value['server_id'];

                $rs[$key]['is_config'] = $value['is_config'] == 0 ? '<input type="button" class="btn btn-default" value="未配置" )">' :  '<input type="button" class="btn btn-success" value="已配置" )">';

        		$rs[$key]['open_time'] = $value['open_time'] != 0 ? date('Y-m-d H:i:s' , $value['open_time']) : '未填写';
        		$rs[$key]['create_time'] = $value['create_time'] != 0 ? date('Y-m-d H:i:s' , $value['create_time']) : '';

                $rs[$key]['caozuo'] .= '<input type="button" class="gbutton" value="编辑" onclick="edit(' . $value['id'] . ')">' ;
                if($value['is_config'] == 0){
                    
                    $rs[$key]['caozuo'] .= '<input type="button" class="gbutton" value="配置" onclick="config(' . $value['id'] . ')">' ;
                }
                

                $rs[$key]['caozuo'] .= '<input type="button" class="gbutton" value="删除 " style="margin-top: 5px;" onclick="del(' . $value['id'] .')">';
        	}

        	foreach($rs as $k => $v){
        		$rs[$k]  = array_values($v);
        	}
        }

        $this->setPageData($rs , $num );
    }
}