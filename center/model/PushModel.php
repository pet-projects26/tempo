<?php

class PushModel extends Model{

    public function __construct(){
        parent::__construct('push');
        $this->alias = 'p';
    }

    public function record_data($conditions){
        $fields = array('object' , 'android_received' , 'ios_received' , 'errno' , 'create_time');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $row['object'] = json_decode($row['object'] , true);
            $rs[$k] = array(
                $row['create_time'],
                $row['object']['notification']['alert'],
                $row['ios_received'],
                $row['android_received'],
                $row['errno']
            );
        }
        $this->setPageData($rs , $this->getCount());
    }

    public function getPush($id = '' , $fields = array() , $start = '' , $end = ''){
        empty($fields) && $fields = array(
            'app' , 'msg_id' , 'type' , 'object' , 'platform' , 'android_received' , 'ios_received' ,
            'cron' , 'cron_time' , 'errno' , 'errstr' , 'create_time' , 'last_modify_time'
        );
        $conditions = array();
        if(is_array($id)){
            $id && $conditions['WHERE']['id::IN'] = $id;
            $start && $conditions['WHERE']['create_time::>='] = $start;
            $end && $conditions['WHERE']['create_time::<='] = $end;
            $conditions['Extends']['ORDER'] = array('id#desc');
            return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        }
        else{
            $id && $conditions['WHERE']['id'] = $id;
            return $this->getRow($fields, $conditions['WHERE']);
        }
    }

    public function addPush($data){
        return $this->add($data);
    }

    public function editPush($data , $id , $msg_id = '' ,$app = ''){
        $conditions = array();
        $id && $conditions['WHERE']['id'] = $id;
        $msg_id && $conditions['WHERE']['msg_id'] = $msg_id;
        $app && $conditions['WHERE']['app'] = $app;
        return $this->update($data , $conditions);
    }

    //获取要推送的消息的ID
    public function getCron(){
        $fields = array('id');
        return $this->getPush(array() , $fields , time());
    }

    //获取
    public function getReceived(){
        $fields = array('msg_id' , 'app');
        $conditions = array();
        $conditions['WHERE']['cron'] = 0;
        $conditions['WHERE']['errno'] = 0;
        return $this->getRows($fields , $conditions);
    }
}
