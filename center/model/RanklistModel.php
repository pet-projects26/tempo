<?php

class RanklistModel extends Model{

    public function __construct(){
        parent::__construct('ranklist');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        if (!isset($conditions['WHERE']['type'])) $conditions['WHERE']['type'] = 0;

        $conditions['WHERE']['a.type'] = $conditions['WHERE']['type'];

        $type = $conditions['WHERE']['type'];

        $conditions['WHERE']["from_unixtime(a.create_time,'%Y-%m-%d')"] = isset($conditions['WHERE']['create_time']) ? $conditions['WHERE']['create_time'] : date('Y-m-d');

        unset($conditions['WHERE']['create_time']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];

        unset($conditions['WHERE']['server::IN']);
        unset($conditions['WHERE']['type']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("a.create_time as create_time", "s.name as server", "rank", 'a.type as type', 'role_name', 'role_id', 'faction_id', 'level', 'power', 'recharge_money', 'over_ingots', 'offline_time');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
//            $rs[$k] = array_values($row);
                $data[$k][0] = $row['rank'];
                if ($type == 0) {
                    $data[$k][1] = $row['level'];
                } elseif ($type == 1) {
                    $data[$k][1] = $row['power'];
                } else {
                    $data[$k][1] = $row['recharge_money'];
                }

                $data[$k][2] = $row['role_name'];

                $data[$k][3] = $row['faction_id'];

                if ($type == 1) {
                    $data[$k][4] = $row['level'];
                } else {
                    $data[$k][4] = $row['power'];
                }

                if ($type == 2) {
                    $data[$k][5] = $row['level'];
                } else {
                    $data[$k][5] = $row['recharge_money'];
                }

                $data[$k][6] = $row['over_ingots'];

                $data[$k][7] = round($row['offline_time'] / 86400, 2) . '天';

                $data[$k][8] = date('Y-m-d', $row['create_time']);

                $data[$k][9] = $row['server'];
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        if (!isset($conditions['WHERE']['type'])) $conditions['WHERE']['type'] = 0;

        $conditions['WHERE']['a.type'] = $conditions['WHERE']['type'];

        $type = $conditions['WHERE']['type'];

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Ymd')) - 1;

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];

        unset($conditions['WHERE']['server::IN']);
        unset($conditions['WHERE']['type']);
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("a.create_time as create_time", "s.name as server", "rank", 'a.type as type', 'role_name', 'role_id', 'faction_id', 'level', 'power', 'recharge_money', 'over_ingots', 'offline_time');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
//            $rs[$k] = array_values($row);

                $data[$k][0] = $row['rank'];
                if ($type == 0) {
                    $data[$k][1] = $row['level'];
                } elseif ($type == 1) {
                    $data[$k][1] = $row['power'];
                } else {
                    $data[$k][1] = $row['recharge_money'];
                }

                $data[$k][2] = $row['role_name'];

                $data[$k][3] = $row['faction_id'];

                if ($type == 1) {
                    $data[$k][4] = $row['level'];
                } else {
                    $data[$k][4] = $row['power'];
                }

                if ($type == 2) {
                    $data[$k][5] = $row['level'];
                } else {
                    $data[$k][5] = $row['recharge_money'];
                }

                $data[$k][6] = $row['over_ingots'];

                $data[$k][7] = round($row['offline_time'] / 86400, 2) . '天';

                $data[$k][8] = date('Y-m-d', $row['create_time']);

                $data[$k][9] = $row['server'];
            }
        }


        $result = array();

        switch ($type) {
            case 0:
                $result[] =  array('日期', '名次', '等级' , '玩家名' , '仙盟' , '战力', '充值金额', '剩余元宝', '离线时长', '统计时间', '服务器');
                break;
            case 1:
                $result[] =  array('日期', '名次', '战力' , '玩家名' , '仙盟' , '等级', '充值金额', '剩余元宝', '离线时长', '统计时间', '服务器');
                break;
            case 2:
                $result[] =  array('日期', '名次', '充值金额' , '玩家名' , '仙盟' , '战力', '等级', '剩余元宝', '离线时长', '统计时间', '服务器');
                break;
            default:
                $type = 0;
                $result[] =  array('日期', '名次', '等级' , '玩家名' , '仙盟' , '战力', '充值金额', '剩余元宝', '离线时长', '统计时间', '服务器');
                break;
        }

        foreach($data as $row){
            $result[] = $this->formatFields($row);
        }

        $name = '';

        if ($type == 0) $name = '等级'; elseif ($type == 1) $name = '战力'; else $name = '付费排行';


        Util::export_csv_2($result , '',$name.'排行榜（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}