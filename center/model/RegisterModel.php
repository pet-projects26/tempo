<?php
class RegisterModel extends Model{
    public function __construct(){
        parent::__construct('register');
        $this->alias = 'rg';
    }

    public function setRegister($account , $channel , $package){
        $data = array(
            'account' => $account,
            'channel' => $channel,
            'package' => $package,
            'create_time' => time()
        );
        return $this->add($data);
    }

    public function newRegister($account , $channel , $package){
        $fields = array('id');
        $conditions = array();
        $conditions['WHERE']['account'] = $account;
        if(!$this->getRow($fields , $conditions['WHERE'])){
            return $this->setRegister($account , $channel , $package);
        }
    }

    public function getRegisterByAcc($account){
        $fields = array('package' , 'create_time');
        $conditions = array();
        $conditions['WHERE']['account'] = $account;
        return $this->getRow($fields , $conditions['WHERE']);
    }

    public function getRegisterByDate($type , $date , $end_date = '' , $order = array('id#desc')){
        $types = array('eq' , 'between' , 'bcount' , 'ecount');
        if(in_array($type , $types)){
            $conditions = array();
            $conditions['Extends']['ORDER'] = $order;
            switch($type){
                case 'eq':
                    $fields = array('account' , 'package' , 'create_time');
                    $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] =  $date;
                    return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
                case 'between':
                    $fields = array('account' , 'package' , 'create_time');
                    $conditions['WHERE']['create_time::>='] =  $date;
                    $end_date && $conditions['WHERE']['create_time::<='] =  $end_date;
                    return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
                case 'bcount':
                    $fields = array('count(id) as count_id');
                    $date && $conditions['WHERE']['create_time::>='] =  $date;
                    $end_date && $conditions['WHERE']['create_time::<='] =  $end_date;
                    $rs = $this->getRows($fields , $conditions['WHERE']);
                    return $rs['count_id'];
                case 'ecount':
                    $fields = array('count(id) as count_id');
                    $date && $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
                    $rs = $this->getRow($fields , $conditions['WHERE']);
                    return $rs['count_id'];
            }
        }
        else{
            return false;
        }
    }
}