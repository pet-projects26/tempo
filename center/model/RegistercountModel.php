<?php

class RegistercountModel extends Model{
    public function __construct(){
        parent::__construct('register_count');
        $this->alias = 'rc';
    }

    public function record_data($conditions){
		$fields = array("date" , 'sum(count)');
		$conditions['Extends']['GROUP']='date';
		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		$num = $this->getCount();
		$channel = isset($conditions['WHERE']['channel::IN']) ? $conditions['WHERE']['channel::IN'] : array_keys($_SESSION['group_channel']);
		$channel = implode(',' , $channel);
        foreach($rs as $k => $row){
			$row['date']="<span class='data' data-date='".$row['date']."'  data-channel='$channel'  style='cursor:pointer; border-bottom: 1px dashed blue'>" . $row['date'] . "</span>";
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $num);
    }
	public function getDetail($date, $channel){
		$where='';
		$date&&$where="where date= '$date'";
		$channel&&$where.=" and channel in ($channel)";
		$sql="select countlist from ny_register_count $where ";
		$result=$this->query($sql);
		$arr=array();
		for($i=0;$i<count($result);$i++){
			foreach(json_decode($result[$i]['countlist']) as $k=>$v){
				$arr[$k]+=$v;
			}
		}
		foreach($arr as $k=>$v){ 
			if($k>9){
				$k="$k:00";
			}else{
				$k="0$k:00";	
			}
			$data[$k]=$v;
		}
        return $data;
    }
    public function record_export($conditions){
        $rs = array();
		$fields = array("date" , 'sum(count)');
		$conditions['Extends']['GROUP']='date';
		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
     	
        $result = array();
        $result[] = array('日期' , '注册数');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '注册数（' . date('Y-m-d') . '）');
    }
}