<?php

class RoleModel extends Model
{
    public function __construct()
    {
        parent::__construct('role');
        $this->alias = 'r';
    }

    public function manual_logout($data)
    {
//        $data['server'] = explode(':' , $data['server']);
//        $msg = $this->rpcCall($data['server'][0] , $data['server'][1] , 'logout' , array('role_id' => $data['role_id']));
//        $json = array('state' => true , 'code' => 200 , 'msg' => $msg === 0 ? '发送成功' : $msg);
//        echo json_encode($json);


        //获取角色id信息是否存在
        $role = $this->call('Role', 'getRoleIdToType', array('type' => 'role_id', 'value' => $data['role_id']), $data['server']);
        $role_id = $role[$data['server']];

        if (empty($role_id)) {
            exit(json_encode(['state' => false, 'msg' => '该角色不存在']));
        }

        //获取服务器配置
        $server_config = (new ServerconfigModel())->getConfig($data['server'], ['server_id', 'websocket_host', 'websocket_port']);

        if ($server_config['websocket_host'] == '' || $server_config['websocket_port'] == '') {
            exit(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
        }

        $info = ['opcode' => Pact::$RoleForceLogout, 'str' => [[intval($role_id)]]];

        $msg = $this->socketCall($server_config['websocket_host'], $server_config['websocket_port'], 'roleBlock', $info);

        $json = array('state' => true, 'code' => 200, 'msg' => $msg === 0 ? '发送成功' : '发送失败');
        echo json_encode($json);
    }
}