<?php

class RolecreateModel extends Model{
    public function __construct(){
        parent::__construct('rolecreat');
        $this->alias = 'rc';
    }
	public function create_data($conditions){
		$fields = array("date" , 'sum(rolecount)','sum(accountcount)');
		$conditions['Extends']['GROUP']='date';
		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		$num = $this->getCount();
		$server = $conditions['WHERE']['server::IN'];
		$server = implode(',' , $server);
        foreach($rs as $k => $row){
			$row['date']="<span class='data' data-date='".$row['date']."' data-server='".$server."'   style='cursor:pointer; border-bottom: 1px dashed blue'>" . $row['date'] . "</span>";
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $num);
    }
	public function create_export(){
		$rs = array();
		$fields = array("date" , 'sum(rolecount)','sum(accountcount)');
		$conditions['Extends']['GROUP']='date';
		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
     	
        $result = array();
        $result[] = array('日期' , '创角数','创角设备数');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '创角数（' . date('Y-m-d') . '）');
    
	}
	public function getDetail($date,$server){

		
		$server = explode(',',$server);

		$server = "'".implode("','" , $server)."'";

		$where='';
		$date&&$where="where date= '$date'";
		$server&&$where.=" and server in ($server)";
		$sql="select rolelist,accountlist from ny_rolecreat $where ";
		
		$result=$this->query($sql);


		$arr=array();
		for($i=0;$i<count($result);$i++){
			foreach(json_decode($result[$i]['rolelist']) as $k=>$v){
				$arr[$k]['rolelist']+=$v;
			}
			foreach(json_decode($result[$i]['accountlist']) as $k=>$v){
				$arr[$k]['accountlist']+=$v;
			}
		}
		foreach($arr as $k=>$v){ 
			if($k>9){
				$k="$k:00";
			}else{
				$k="0$k:00";	
			}
			$data[$k]=$v;
		}
		//file_put_contents('../test.txt',json_encode($data));
        return $data;	
	}
}