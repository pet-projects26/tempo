<?php
class RoledailyModel extends Model{
    public function __construct(){
        parent::__construct('role_daily');
        $this->alias = 'rd';
    }
	
    public function record_data($conditions){
		
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
           // $rs = $this->call('Roledaily' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
			$rs = $this->call('Role' , 'getRoleListData' , array('conditions' => $conditions) , $server_id);
            $data = $this->callDataMerge($rs);
            !(empty($data))&&list($rs,$num) = $data;
            foreach($rs as $k => $row){
                $row['career'] = CDict::$career[floor($row['career'] / 100)][$row['career']];
                $attr = array(
                    1 => '基础信息' , 2 => '流水查询' , 3 => '背包'
                );
//                 foreach($attr as $type => $title){
//                    $row['option'][] = '<input type="button" class="gbutton" value="' . $title . '" onclick="attr(' . $type . ',' . '\''.$row['role_id'] .'\',this );" style="cursor:pointer;margin:3px 0;">';
//                 }

                $row['option'] = "<input type='button' class='gbutton' value='查看' onclick='attr(\"{1}\", \"{$row['role_id']}\",this);' style='cursor:pointer;margin:3px 0;'>";
//                 $row['option'] = implode(' ' , $row['option']);
                $rs[$k] = array_values($row);
            }
			
        }
        $this->setPageData($rs , $num,$limit );
    }
    
    public function record_export2($conditions){
    
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		// $rs = $this->call('Roledaily' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
    		$rs = $this->call('Role' , 'getRoleListData' , array('conditions' => $conditions) , $server_id);
    		$data = $this->callDataMerge($rs);
    		$result = array();
    		$result[] = array('角色ID' , '角色名' , '账号' ,'包号' , '职业' , '等级' , '元宝' , '创号时间' , '最后登录时间', '最后退出时间','ip');
    		!(empty($data))&&list($rs,$num) = $data;
    		foreach($rs as $k => $row){
    			$row['career'] = CDict::$career[floor($row['career'] / 100)][$row['career']];
    			$result[] = $this->formatFields($row);
    			//$rs[$k] = array_values($row);
    		}
    		Util::export_csv_2($result , '','角色列表（' . date('Y年m月d日H时i分s秒') . '）.csv');
    		//Util::exportCsv($result , '角色列表（' . date('Y年m月d日H时i分s秒') . '）');
    			
    	}
    }
    
    public function record_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Roledaily' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('角色ID' , '角色名' , '账号' , '职业' , '等级' , '元宝' , '创号时间' , '最后登录时间');
        foreach($rs as $row){
            $row['career'] = CDict::$career[floor($row['career'] / 100)][$row['career']];
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '角色列表（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function career_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Roledaily' , 'getCareerData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $row['djs'] .= '(' . sprintf("%.2f" , $row['djs'] / $row['count_role_id'] * 100) . '%)';
                $row['yls'] .= '(' . sprintf("%.2f" , $row['yls'] / $row['count_role_id'] * 100) . '%)';
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function career_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Roledaily' , 'getCareerData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
            foreach($rs as $k => $row){
                $row['djs'] .= '(' . sprintf("%.2f" , $row['djs'] / $row['count_role_id'] * 100) . '%)';
                $row['yls'] .= '(' . sprintf("%.2f" , $row['yls'] / $row['count_role_id'] * 100) . '%)';
            }
        }
        $result = array();
        $result[] = array('日期' , '刀剑师' , '羽翎师' ,'总人数');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '职业统计（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function level_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Roledaily' , 'getLevelData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function level_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Roledaily' , 'getLevelData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('等级' , '人数' , '总人数' ,'比例');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '等级统计（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function create_analysis_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Roledaily' , 'getCreateAnalysisData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs , true);
            foreach($rs as $k => $row){
                $rs[$k] = array(
                    $row['call_server_id'],
                    $row['count_role_id'] ? $row['count_role_id'] : 0,
                    $row['count_account'] ? $row['count_account'] : 0,
                    $row['count_role_id'] ? sprintf("%.2f" , $row['count_account'] / $row['count_role_id'] * 100) . '%' : '0%',
                    $row['count_role_id'] - $row['count_account'],
                    $row['count_role_id'] ? sprintf("%.2f" , ($row['count_role_id'] - $row['count_account']) / $row['count_role_id'] * 100) . '%' : '0%'
                );
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
}