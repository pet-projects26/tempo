<?php
class SensitModel extends Model{
    public function __construct(){
        parent::__construct('sensit');
        $this->alias = 'al';
    }
    
    public function sensit_data($conditions){
    	$fields = array('id' , 'name', 'content' , 'create_time');
    	$rs = $this->getRows($fields , $conditions['WHERE'],$conditions['Extends']);
    	$num = $this->getCount();
    	foreach($rs as $k => $row){
    		$row['create_time'] = date('Y-m-d H:i:s' , $row['create_time']);
    		$row['options']  = '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['id'] . '\')">';
    		$row['options'] .= '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['id'] . '\')">';
    		unset($row['id']);
    		unset($row['num']);
    		$rs[$k] = array_values($row);
    	}
    	$this->setPageData($rs , $num);
    }
    
    public function sensit_export($conditions){
    	$rs = array();
    	$fields = array('name');
    	$rs = $this->getRows($fields , $conditions['WHERE'],$conditions['Extends']);
    	$result = array();
    	$result2 = array('名称');
    	
    	foreach($rs as $k=>$row){
    		$result[$k]['name'] = $row['name'];
    	}
    	
    	Util::export_csv_2($result , $result2,'敏感字（' . date('Y年m月d日H时i分s秒') . '）.xls');
    }
}