<?php

class ServerAutoPlanModel extends Model{

    public function __construct(){
        parent::__construct('server_auto_plan');
        $this->alias = 'm';
        //$this->cgm = new ChannelGroupModel();
    }

    public function record_data($conditions){

        $result = array();
        $num = 0;

        $fields = array('id', 'group_id', 'name', 'status', 'type', 'create_role_num', 'pay_num', 'create_time');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num  = count($rs);
        $rss = array();
        foreach($rs as $k=>$v){
            $rss[$k]['id'] = $v['id'];
            $group_num = $v['group_id'];
            $groupsql = "select name from ny_channel_group where id = '".$group_num."'";
            $grouparr = $this->query($groupsql);
            $rss[$k]['group_id'] = $v['group_id'].'('.$grouparr[0]['name'].')';
            $rss[$k]['name'] = $v['name'];

            $configStr = '';
            $configStatus = 0;

            switch ($v['status']) {
                case 0:
                    $rss[$k]['status'] = '<button class="btn btn-default" >未配置</button>';
                    $configStr = '配置';
                    $configStatus= 1;
                    break;
                case 1:
                    $rss[$k]['status'] = '<button class="btn btn-primary" >已配置</button>';
                    $configStr = '废弃';
                    $configStatus = 2;
                    break;
                case 2:
                    $rss[$k]['status'] = '<button class="btn btn-danger" >已废弃</button>';
                    $configStr = '删除';
                    $configStatus = 3;
                    break;
                    break;
                default:
                    break;
            }

            $rss[$k]['type'] = '<button class="btn btn-primary" >'.CDict::$AutoPlanType[$v['type']].'</button>';


            $rss[$k]['create_role_num'] = $v['create_role_num'];

            $rss[$k]['pay_num'] = $v['pay_num'];

            $rss[$k]['createtime'] = date('Y-m-d H:i' , $v['create_time']);

            $config = '<input type="button" class="gbutton" value="'.$configStr.'" onclick="config('.$configStatus.' ,' . $v['id'] . ')">';

            $edit =  '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $v['id'] . '\')">';

          //  $del = '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $v['id'] . '\')">';


            $rss[$k]['caozuo'] = $config . $edit;


        }

        foreach ($rss as $key => $value) {
            $result[$key] = array_values($value);
        }

        $this->setPageData($result , $num);

    }


}