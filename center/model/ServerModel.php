<?php

class ServerModel extends Model{
    public function __construct(){
        parent::__construct('server');
        $this->alias = 's';
        $this->setNewPrefix('ny_');
        $this->joinTable = array(
            'sc' => array('name' => 'server_config' , 'type' => 'LEFT' , 'on' => 's.server_id = sc.server_id')
        );
        $this->cgm = new ChannelgroupModel();
    }

    public function getServer($server_id = '' , $fields = array() , $display = '' , $start = '' , $end = ''){
        empty($fields) && $fields = array(
            'num' , 'server_id' , 'name' , 'zone' , 'status' , 'type' , 'sort' , 'open_time' ,
            'close_time' , 'create_time' , 'display' , 'max_online' , 'tips','review','display_time'
        );
        $conditions = array();

        $conditions['Extends']['ORDER'] = array('s.num#DESC', 's.open_time#DESC');

        if(is_array($server_id)){
            $server_id && $conditions['WHERE']['s.server_id::IN'] = $server_id;
            ($display != '') && $conditions['WHERE']['display'] = $display;
            $start && $conditions['WHERE']['s.create_time::>='] = $start;
            $end && $conditions['WHERE']['s.create_time::<='] = $end;
            return $this->getRows($fields , $conditions['WHERE'], $conditions['Extends']);
        } else {
            $server_id && $conditions['WHERE']['s.server_id'] = $server_id;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    /**
     * 根据服ID来查找服
     * @param  [int] $id [服ID]
     * @return array
     */
    public function getSeverById($id) {
        $conditions['WHERE']['id'] = $id;
        $res = $this->getRow(array('*'), $conditions['WHERE']);
        if (empty($res)) {
            return array();
        }
        return $res[0];
    }


    /**
     * [添加服务器]
     * @param [array] $data [description]
     */
    public function addServer($data){

        $num = $data['num'];
        $sql = "SELECT max(channel_num) AS channel_num FROM ny_server WHERE num = {$num} limit 1";

        $res  = $this->query($sql);

        $channel_num = $res[0]['channel_num'] + 1;

        if ($channel_num < 1) {
            $channel_num = 1;
        }

        if ($channel_num > 255) {
            return false;
        }

        $data['channel_num'] = $channel_num;

        if($data){
            return $this->add($data);
        }
    }

    public function editServer($data , $server_id){
        if($data && $server_id){
            $conditions = array();
            if(is_array($server_id)){
                $conditions['WHERE']['server_id::IN'] = $server_id;
            }
            else{
                $conditions['WHERE']['server_id'] = $server_id;
            }
            return $this->update($data , $conditions['WHERE']);
        }
        else{
            return false;
        }
    }

    public function record_data($conditions){
        $channel = $conditions['WHERE']['channel_id'];
        if($conditions['WHERE']['s.status'] == '-2'){
            unset($conditions['WHERE']['s.status']);
        }
        unset($conditions['WHERE']['channel_id']);
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        //$conditions['Extends']['ORDER'] = array('s.id#DESC');
        $fields = array('s.num' , 's.channel_num', 's.name' , 's.server_id' , 's.zone' , 'sc.mdkey' , 's.status' , 's.open_time' , 's.display', 's.group_id','s.mom_server');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);


        $rsz = (new ZoneModel())->getZone(array());
        $zone = array();
        foreach($rsz as $row){
            $zone[$row['id']] = $row['name'];
        }


        //查选全部渠道组
        $groups = $this->cgm->getRows(array('id' , 'name'));

        $group = array();
        foreach($groups as $k=>$v){
            $group[$v['id']] = $v['name']; 
        }

        $rss = array();
        $serverStatus = CDict::$serverStatus;
        foreach($rs as $row){
            $open_time = $row['open_time'] ? ceil((time() - $row['open_time']) / 86400) : 0;

            switch ($row['status']) {
                case '-1':
                    $status = '<button class="btn btn-default" >维护中</button>';
                    break;
                case '0':
                    $status = '<button class="btn btn-default" >未开服</button>';
                    break;
                case '1':
                    $status = '<button class="btn btn-warning" >繁忙</button>';
                    break;
                case '2':
                    $status = '<button class="btn btn-success" >新服</button>';
                    break;
            }

            $rss[] = array(
                $row['num'],
                $row['channel_num'].'('.$group[$row['group_id']].')',
                $row['name'],
                $row['server_id'],
                $row['zone'] ? array_key_exists($row['zone'] , $zone) ? $zone[$row['zone']] : '分区已删除' : '未选择分区',
                //$row['mdkey'] ? $row['mdkey'] : '无',
                $status,
                $row['open_time'] ? date('Y-m-d H:i:s' , $row['open_time']) : '无',
                $open_time > 0 ? '第' . $open_time . '天' : '未开服',
                $row['display'] ? '<button class="btn btn-success" >显示</button>' : '<button class="btn btn-danger">不显示</button>',
            	$row['mom_server'],
                '<input type="button" class="gbutton" value="编辑" onclick="edit(\'' . $row['server_id'] . '\')">'.
                '<input type="button" class="gbutton" value="删除" onclick="del(\'' . $row['server_id'] . '\')">'

                //.'<input type="button" class="gbutton" value="开通渠道" onclick="channel_open(\'' . $row['server_id'] . '\', \'' . $row['group_id'] . '\')">'
            );
        }
        $this->setPageData($rss , count($rss) , $limit);
    }

    public function delServer($server_id){
        if($server_id){
            $cgm = new ChannelgroupModel();
            $channelGroup = $cgm->getChannelGroup(array() , array('id' , 'server'));
            foreach($channelGroup as $row){
                $row['server'] = json_decode($row['server'] , true);
                if(in_array($server_id , $row['server'])){
                    $key = array_search($server_id , $row['server']);
                    unset($row['server'][$key]);
                    $data = array('server' => json_encode($row['server']));
                    $cgm->editChannelGroup($data , $row['id']);
                    break;
                }
            }
            $conditions = array();
            $conditions['WHERE']['server_id'] = $server_id;
            return $this->delete($conditions['WHERE']);
        }
    }

    //根据渠道获取服务器列表
    public function getServerByChannelPackage($channel , $package = '' , $account = '' , $version = '' , $fields = array() , $group = false){
        $rs = (new ChannelgroupModel())->getChannelGroup(array() , array('id' , 'name' , 'server' , 'channel_package'));
        $server = array();

        foreach($rs as $row){
            $row['channel_package'] = json_decode($row['channel_package'] , true);
            if (!is_array($row['channel_package'])) {
                $row['channel_package'] = array();
            }

            if(array_key_exists($channel , $row['channel_package'])){
                $row['server'] = json_decode($row['server'] , true);
                if (is_array($row['server'])) {
                    foreach($row['server'] as $v){
                        if($group){
                            $server[$row['id'] . '_' . $row['name']][] = $v;
                        }
                        else{
                            $server[] = $v;
                        }
                    }
                }
            }
        }

        empty($fields) && $fields = array(
            's.server_id' , 's.name' , 's.open_time' , 's.zone' , 's.num' , 's.channel_num','s.status' , 'sc.domain' , 's.display' ,
            'sc.mdkey' , 's.sort' , 's.tips' , 's.max_online' , 'sc.login_host' , 'sc.login_port'
        );
        $server = $server ? $server : array('no server');

        if($group){
            $rs = array();
            foreach($server as $k => $row){
                $rs[$k] = $this->getServer($row , $fields);
            }
            return $rs;
        }
        else{
            return $this->getServer($server , $fields);
        }
    }

    public function getServerByNum($num = array() , $fields = array()){
        empty($fields) && $fields = array(
            'num' , 'server_id' , 'name' , 'zone' , 'status' , 'type' , 'sort' , 'open_time' ,
            'close_time' , 'create_time' , 'display' , 'max_online' , 'tips'
        );
        $conditions = array();
        if(is_array($num)){
            $num && $conditions['WHERE']['s.num::IN'] = $num;
            return $this->getRows($fields , $conditions['WHERE']);
        }
        else{
            $num && $conditions['WHERE']['s.num'] = $num;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    //从memcache中获取服务器基础信息
    public function getServerByMmc(){
        $mem = Util::memcacheConn();
        $server_list = $mem->get('server_list');
        if(!$server_list){
            $server_list = array();
            $server = $this->getServer();
            foreach($server as $row){
                $server_list[$row['server_id']] = $row;
            }
            $mem->set('server_list' , $server_list);
        }
        return $server_list;
    }

    public function getServerNameByMmc(){
        $mem = Util::memcacheConn();
        $server_name_list = $mem->get('server_name_list');
        if(!$server_name_list){
            $server_name_list = array();
            $server = $this->getServer(array() , array('server_id' , 'name'));
            foreach($server as $row){
                $server_name_list[$row['server_id']] = $row['name'];
            }
            $mem->set('server_name_list' , $server_name_list);
        }
        return $server_name_list;
    }

    //获取服务器的标识和名称
	public function getServerName($start = '' , $end = '' , $status = ''){
        $conditions = array();
        $fields = array('server_id' , 'name');
        $start && $conditions['WHERE']['open_time>=::'] = $start;
        $end && $conditions['WHERE']['open_time<=::'] = $end;
        ($status !== '') && $conditions['WHERE']['status!=::'] = $status;
        return $this->getRows($fields , $conditions['WHERE']);
	}

    /**
     * [根据组对服务器进行分组]
     * @param  array  $where [description]
     * @return [type]        [description]
     */
    public function getServerGroupbyGroupId($where = array()) {
        $serverRes = $this->getServer(array(), array('group_id', 'server_id', 'num', 'name'));

        $serverRow = array();

        foreach ($serverRes as $row) {
            $group_id = $row['group_id'];

            $sign = $row['server_id'];
            
            $name = 'S'.$row['num'] . '-' . $row['name'];

            $serverRow[$group_id][$sign] = $name; 
        }

        return $serverRow;
    }

    /**
     * 格式化用户名
     * @param  [array] $row [description]
     * @return [type]      [description]
     */
    public function formatServerName($row) {
        return $name = 'S'.$row['num'] . '-' . $row['name'];
    }

    /**
     * 返回服务器列表
     * @param  [array] $server       [description]
     * @param  [array] $channelGroup [description]
     * @return [type]               [description]
     */
    public function getServerByServerAndChannelGroup($server, $channelGroup, $where = "1", $isPass = true) {
//        $this->db = new Model();
        $serverList = array();

        $where = "{$where} and display = 1 and type !=2 and status not in (0 , -1)" ;

        if ($isPass) {
            $where = 1;
        }

        if (empty($server) && empty($channelGroup)) {
            $sql = "SELECT server_id, num, name FROM ny_server WHERE {$where} ";

            $res = $this->query($sql);

            foreach ($res as $row) {
                $sign = $row['server_id'];

                $serverList[$sign] = 'S'.$row['num'] . '-' . $row['name'];
            }

        } elseif (empty($server) && !empty($channelGroup)) {
            $str = implode(',', $channelGroup);

            $sql = "SELECT server_id, num,  name FROM ny_server WHERE {$where} AND group_id in ({$str})";

            $res = $this->query($sql);

            foreach ($res as $row) {
                $sign = $row['server_id'];

                $serverList[$sign] = 'S'.$row['num']. '-'. $row['name'];
            }

        } elseif (!empty($server)) {

            $tmp  = array();

            foreach ($server as $v) {
                $tmp[]  = "'$v'";
            }

            $str = implode(',', $tmp);

            $sql = "SELECT server_id, num, name FROM ny_server WHERE {$where} AND server_id in ({$str})";

            $res = $this->query($sql);

            foreach ($res as $row) {
                $sign = $row['server_id'];

                $serverList[$sign] = 'S'. $row['num']. '-'. $row['name'];
            }

        }

        return $serverList;
    }
    
    //没有剔除合服
    public function getServerByServerAndChannelGroup2($server, $channelGroup, $where = "1", $isPass = true) {
    	$this->db = new Model();
    	$serverList = array();
    
    	$where = "{$where} and display = 1 " ;
    
    	if (empty($server) && empty($channelGroup)) {
    		$sql = "SELECT server_id, num, name FROM ny_server WHERE {$where} ";
    
    		$res = $this->db->query($sql);
    
    		foreach ($res as $row) {
    			$sign = $row['server_id'];
    
    			$serverList[$sign] = 'S'.$row['num'] . '-' . $row['name'];
    		}
    
    	} elseif (empty($server) && !empty($channelGroup)) {
    		$str = implode(',', $channelGroup);
    
    		$sql = "SELECT server_id, num,  name FROM ny_server WHERE {$where} AND group_id in ({$str})";
    
    		$res = $this->db->query($sql);
    
    		foreach ($res as $row) {
    			$sign = $row['server_id'];
    
    			$serverList[$sign] = 'S'.$row['num']. '-'. $row['name'];
    		}
    
    	} elseif (!empty($server)) {
    		if ($isPass) {
    			$where = 1;
    		}
    
    		$tmp  = array();
    
    		foreach ($server as $v) {
    			$tmp[]  = "'$v'";
    		}
    
    		$str = implode(',', $tmp);
    
    		$sql = "SELECT server_id, num, name FROM ny_server WHERE {$where} AND server_id in ({$str})";
    
    		$res = $this->db->query($sql);
    
    		foreach ($res as $row) {
    			$sign = $row['server_id'];
    
    			$serverList[$sign] = 'S'. $row['num']. '-'. $row['name'];
    		}
    
    	}
    
    	return $serverList;
    }

    /**
     * getFields
     * @param array $group_id
     * @return array $temp
    */
    public function getServerBygroupId($group_id = array())
    {
        $group_ids = (new ChannelgroupModel())->getChannelGroup(array(),array('id'));
        $group_id = empty($group_id)?$group_ids:$group_id;
        $conditions['WHERE'] = 1;
        $conditions['WHERE']['group_id::IN'] = $group_id;

        $res = $this->getRows(array('group_id,server_id'),$conditions['WHERE']);
        $tmp = [];
        foreach($res as $val){
            $tmp[$val['group_id']][] = $val['server_id'];
        }

        return $tmp;
    }

    //获取最新的服务器 by wang
    public function getLatestServer()
    {
        $Extends = array(
            'ORDER' => ['open_time#desc']
        );

        $conditions = [];
        $conditions['WHERE']['status'] = 2;
        $conditions['WHERE']['display'] = 1;

        $server = $this->getRow('server_id', $conditions['WHERE'], $Extends);

        $server = $server['server_id'] ? $server['server_id'] : '';

        return $server;
    }

    //获取服务器列表最新服务器
    public function getNewServer($group_id, $rvtype = 0)
    {
        $Extends = array(
            'ORDER' => ['create_time#desc']
        );

        $conditions = [];
        $conditions['WHERE']['status'] = 2;
        $conditions['WHERE']['display'] = 1;
        $rvtype && $conditions['WHERE']['review'] = 1;
        $conditions['WHERE']['group_id'] = $group_id;

        $server = $this->getRow('server_id', $conditions['WHERE'], $Extends);

        $server = $server['server_id'] ? $server['server_id'] : '';

        return $server;
    }

    public function getServerIdListBygroupId($group_id = array(), $rvtype = 0, $isWhite = false)
    {
        $group_ids = (new ChannelgroupModel())->getChannelGroup(array(), array('id'));
        $group_id = empty($group_id) ? $group_ids : $group_id;
        $conditions = [];
        $conditions['WHERE'] = [];
        $conditions['WHERE']['group_id::IN'] = $group_id;
        if (!$isWhite) {
            $rvtype && $conditions['WHERE']['review'] = 1;
            $conditions['WHERE']['status::!='] = 0;
            $conditions['WHERE']['display'] = 1;
        }

        $res = $this->getRows(array('group_id,server_id'), $conditions['WHERE']);
        $tmp = [];
        foreach ($res as $val) {
            $tmp[$val['group_id']][] = $val['server_id'];
        }

        return $tmp;
    }

    public function getAllServerName()
    {
        $rs = $this->getRows(['server_id', 'name']);

        $data = [];

        if (!empty($rs)) {

            foreach ($rs as $v) {
                $data[$v['server_id']] = $v['name'];
            }
        }

        return $data;
    }

}
