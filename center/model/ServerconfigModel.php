<?php

class ServerconfigModel extends Model{
    public function __construct(){
        parent::__construct('server_config');
        $this->alias = 'sc';
    }

    public function getConfig($server_id = '' , $fields = array()){

        empty($fields) && $fields = array(
            'server_id' , 'ip' , 'api_url' , 'domain' , 'mysql_host' , 'mysql_port' , 'mysql_user' ,
            'mysql_passwd' , 'mysql_db' , 'mysql_prefix' , 'mongo_host' , 'mongo_port' , 'mongo_user' ,
            'mongo_passwd' , 'mongo_db' , 'login_host' , 'login_port' , 'gm_port' , 'mdkey','channel_rv',
            'redis_host', 'redis_port', 'redis_passwd', 'redis_prefix', 'redis_db', 'websocket_host', 'websocket_port', 'configs_path', 'bin_path'
        );
        $conditions = array();
        if(is_array($server_id)){
            $server_id && $conditions['WHERE']['server_id::IN'] = $server_id;
            $conditions['Extends']['GROUP'] = 'id';
            return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        }
        else{
            $server_id && $conditions['WHERE']['server_id'] = $server_id;
            return $this->getRow($fields ,  $conditions['WHERE']);
        }
    }

    public function addConfig($data){
        if($data){
            return $this->add($data);
        }
    }

    public function editConfig($data , $server_id){
        if($data && $server_id){
            $conditions = array();
            $conditions['WHERE']['server_id'] = $server_id;
            return $this->update($data , $conditions['WHERE']);
        }
    }

    public function delConfig($server_id){
        if($server_id){
            $conditions = array();
            $conditions['WHERE']['server_id'] = $server_id;
            return $this->delete($conditions['WHERE']);
        }
    }
    /**
     * [获取服务器列表]
     * @param  [string | array] $server_id 服务器标识符
     * @param  [bool] $isString [是否为字符串]
     * @return [type]            [description]
     */
    public function getApiUrl($server_id, $isString = false){

        if (!is_array($server_id)) {

            $result = $this->getConfig($server_id , $fields = array('server_id' , 'api_url'));

        } else {

            $tmp = array();
            foreach ($server_id as $v) {
                $tmp[] = "'{$v}'";
            }

            $str = implode(',', $tmp);

            $sql = "SELECT server_id, api_url FROM ny_server_config WHERE `server_id` IN ({$str}) ";
          
            $result = $this->query($sql);
        }

        if (empty($result)) {
            return array();
        }

        $vars = array();

        if (!is_array($server_id)) {
            
            $vars[$result['server_id']] = $result['api_url'];

        }else{
            foreach ($result as $k => $row) {

                if ($isString) {
                    $vars[$row['server_id']] = $row['api_url'];
                } else {
                    $vars[$row['server_id']] = $row;
                }
            }
        }
        

        return $vars;
    }

    //从memcache中获取服务器配置信息
    public function getServerConfigByMmc(){
        $mem = Util::memcacheConn();
        $server_config_list = $mem->get('server_config_list');
        if(!$server_config_list){
            $server_config_list = array();
            $server = $this->getConfig();
            foreach($server as $row){
                $server_config_list[$row['server_id']] = $row;
            }
            $mem->set('server_config_list' , $server_config_list);
        }
        return $server_config_list;
    }


     /**
     * [生成http请求参数]
     * @param  [array | string ] $server_id [description]
     * @return [type]            [description]
     */
    public function makeHttpParams($server_id, $data = array(), $encryption = true) {
        $res = $this->getConfig($server_id, array('*'));

        if (empty($res)) {
            return array();
        }

        $time = time();

        $params = array();

        $urls = array();
       
        foreach ($res as $key => $row) {
            $server_id = $row['server_id'];
           
            $url = $row['api_url'];
            $mdkey = $row['mdkey'];
            $url = str_replace('index.php', 'api.php', $url);

            $urls[$server_id] = $url;

            $json  = json_encode($data);

            if ($encryption == true) {
                $md5 = Helper::authcode($json, 'ENCODE');
            } else {
                $md5 = $json;
            }
            
            $post = array('unixtime' => $time, 'data' => $md5);

            $post['s'] = $row['server_id'];

            $post['sign'] = Helper::checkSign($post, $mdkey);


            $params[$server_id] = $post;

        }

        return  array('urls' => $urls, 'params' => $params);
    }

}
