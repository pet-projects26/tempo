<?php
/**
 * Created by PhpStorm.
 * User: jiangzhe
 * Date: 2016-12-27
 * Time: 11:50
 */

class ServerversionModel extends Model{

    public function __construct(){
        parent::__construct('server_version');
        $this->insertFileds = array('server_name','server_flag','version','start_time','end_time');
        $this->updateFileds = $this->insertFileds;
    }

    public function get_server_version($fileds = '*' , $server_flag){
        if(empty($server_flag)){
            return false;
        }
        $conditions = array();
        $conditions['WHERE']['server_flag'] = $server_flag;
        return $this->getRow($fileds , $conditions['WHERE']);
    }

    public function get_server_version_list($fileds = "*" , $where = array() , $extends = array()){
        $conditions = array();
        $conditions['WHERE'] = $where;
        $conditions['Extends'] = $extends;
        if(!isset($conditions['Extends']['ORDER'])){
            $conditions['Extends']['ORDER'] = 'create_time#desc';
        }
        return $this->getRows($fileds , $where);
    }

    public function add_server_version(array $data){
        return $this->replace($data);
    }

    public function update_server_version(array $data , $where = array()){
        return $this->update($data , $where);
    }

    public function delete_server_version($server_flag){
        if(empty($server_flag)){
            return false;
        }
        $conditions = array();
        $data = array('delete' => 1);
        $conditions['WHERE']['server_flag'] = $server_flag;
        return $this->update($data , $conditions['WHERE']);
    }
} 