<?php

class SettingModel extends Model
{

    public function gmcmd($data)
    {
//        $server = (new ServerModel())->getGm(array(),array($data['server']));
        //获取服务器配置
        $server_config = (new ServerconfigModel())->getConfig($data['server'], ['server_id', 'websocket_host', 'websocket_port']);

        if ($server_config['websocket_host'] == '' || $server_config['websocket_port'] == '') {
            exit(json_encode(['state' => false, 'msg' => '该服务器没有配置好websocket']));
        }

        $info = ['opcode' => $data['pact'], 'str' => [intval($data['role_id']), $data['cmd']]];

        $this->socketCall($server_config['websocket_host'], $server_config['websocket_port'], 'gmcmd', $info);
        $json = array('state' => true, 'code' => 200, '发送成功');
        echo json_encode($json);
    }


    public function cleansql($server)
    {
        //获取服务器配置
        $server_config = (new ServerconfigModel())->getConfig($server, ['mysql_host', 'mysql_port', 'mysql_user', 'mysql_passwd', 'mysql_db', 'mysql_prefix']);

        if (empty($server_config) || !$server_config['mysql_host'] || !$server_config['mysql_port'] || !$server_config['mysql_user'] || !$server_config['mysql_passwd'] || !$server_config['mysql_db'] || !$server_config['mysql_prefix']) {
            exit(
            json_encode([
                'state' => false,
                'msg' => '请检查该服务器的单服Mysql配置'
            ])
            );
        }
        //先清空单服表再清央服
        $result = $this->call('Setting', 'cleanSql', ['type' => 0], $server); //传0 用 truncate 传1 用delete

        if (empty($result[$server]) || $result[$server]['state'] == '400') {
            exit(
            json_encode([
                'state' => false,
                'msg' => '单服表清空失败'
            ])
            );
        }

        //央服可以清除数据的表
        $centerRecordTable = [
            'active',
            'boss_home',
            'charge_true',
            'daily_consume',
            'daily_recharge',
            'daily_trial',
            'ltv_record',
            'mall_record',
            'many_boss',
            'multiday_retention',
            'node',
            'node_daily_hour',
            'order',
            'order_return',
            'recharge_remain',
            'rune_copy',
            'single_boss',
            'team_copy',
            'three_realms_boss',
            'vip',
            'vipnotice',
            'virtual_order',
            'zones',
            'online_distribution',
            'riches_copy',
            'swimming_copy',
            'cloudland_copy',
            'fairy_info',
            'nine_copy',
            'tianti_copy',
            'adventure_info',
            'xianfu_info',
            'gift_order',
            'summary',
            'ranklist',
            'level_remain',
            'active_account',
            'novice_task'
        ];

        foreach ($centerRecordTable as $tableName) {

            $sql = 'delete from ny_' . $tableName . " where server = '" . $server . "'";

            //  $sql = 'truncate table ny_'.$tableName;

            $rs = $this->query($sql);

            if ($this->lastError()) {
                exit(
                json_encode([
                    'state' => false,
                    'msg' => $this->lastError()
                ])
                );
            }
        }

        exit(
        json_encode([
            'state' => true,
            'msg' => '成功'
        ])
        );
    }

}