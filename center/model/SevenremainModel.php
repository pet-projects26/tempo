<?php
class SevenremainModel extends Model{
    public function __construct(){
        parent::__construct('seven_remain');
        $this->alias = 'sr';
    }

    public function role_seven_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Sevenremain', 'getSevenremainData', array('conditions' => $conditions), $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function role_seven_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Sevenremain' , 'getSevenremainData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array('日期' , '创角数' , '第2天' , '第3天' , '第4天' , '第5天' , '第6天' , '第7天' , '第8天' , '第9天' , '第10天' , '第11天' , '第12天' , '第13天' , '第14天' , '第15天' , '第16天' , '第17天' , '第18天' , '第19天' , '第20天' , '第21天' , '第22天' , '第23天' , '第24天' , '第25天' , '第26天' , '第27天' , '第28天' , '第29天' , '第30天');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '创角留存（' . date('Y年m月d日H时i分s秒') . '）');
    }
	
	public function reg_seven_data($conditions){
		$rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Sevenremain', 'getAccountSevenremainData', array('conditions' => $conditions), $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
   
    public function reg_seven_export($conditions){
		$rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])) {
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Sevenremain', 'getAccountSevenremainData', array('conditions' => $conditions), $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result[] = array('日期' , '注册数' , '第2天' , '第3天' , '第4天' , '第5天' , '第6天' , '第7天' , '第8天' , '第9天' , '第10天' , '第11天' , '第12天' , '第13天' , '第14天' , '第15天' , '第16天' , '第17天' , '第18天' , '第19天' , '第20天' , '第21天' , '第22天' , '第23天' , '第24天' , '第25天' , '第26天' , '第27天' , '第28天' , '第29天' , '第30天');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '注册留存（' . date('Y年m月d日H时i分s秒') . '）');
    }
}