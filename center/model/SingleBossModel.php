<?php

class SingleBossModel extends Model{

    public function __construct(){
        parent::__construct('single_boss');
        $this->alias = 'a';
    }

    public function single_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server","active_num", 'join_num', 'join_for_level_50_num', 'join_for_level_50_count', 'join_for_rebirth_1_num', 'join_for_rebirth_1_count', 'join_for_rebirth_2_num', 'join_for_rebirth_2_count', 'join_for_rebirth_3_num', 'join_for_rebirth_3_count', 'join_for_rebirth_4_num', 'join_for_rebirth_4_count', 'join_for_rebirth_5_num', 'join_for_rebirth_5_count','join_for_rebirth_6_num', 'join_for_rebirth_6_count','join_for_rebirth_7_num', 'join_for_rebirth_7_count');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach($arr as $k => $row){
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = @sprintf("%.2f" , $row['join_num'] /$row['active_num'] * 100) . '%';
                //人均挑战次数
                $data[$k][5] = @round($row['join_for_level_50_count'] /$row['join_for_level_50_num'], 2);
                $data[$k][6] = @round($row['join_for_rebirth_1_count'] /$row['join_for_rebirth_1_num'], 2);
                $data[$k][7] = @round($row['join_for_rebirth_2_count'] /$row['join_for_rebirth_2_num'], 2);
                $data[$k][8] = @round($row['join_for_rebirth_3_count'] /$row['join_for_rebirth_3_num'], 2);
                $data[$k][9] = @round($row['join_for_rebirth_4_count'] /$row['join_for_rebirth_4_num'], 2);
                $data[$k][10] = @round($row['join_for_rebirth_5_count'] /$row['join_for_rebirth_5_num'], 2);
                $data[$k][11] = @round($row['join_for_rebirth_6_count'] /$row['join_for_rebirth_6_num'], 2);
                $data[$k][12] = @round($row['join_for_rebirth_7_count'] /$row['join_for_rebirth_7_num'], 2) ;
            }
        }

        foreach($data as $k=>$row){
            $data[$k]=array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function single_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server","active_num", 'join_num', 'join_for_level_50_num', 'join_for_level_50_count', 'join_for_rebirth_1_num', 'join_for_rebirth_1_count', 'join_for_rebirth_2_num', 'join_for_rebirth_2_count', 'join_for_rebirth_3_num', 'join_for_rebirth_3_count', 'join_for_rebirth_4_num', 'join_for_rebirth_4_count', 'join_for_rebirth_5_num', 'join_for_rebirth_5_count','join_for_rebirth_6_num', 'join_for_rebirth_6_count','join_for_rebirth_7_num', 'join_for_rebirth_7_count');

        $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

        if ($arr) {
            foreach($arr as $k => $row){
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = @sprintf("%.2f" , $row['join_num'] /$row['active_num'] * 100) . '%';
                //人均挑战次数
                $data[$k][5] = @round($row['join_for_level_50_count'] /$row['join_for_level_50_num'], 2);
                $data[$k][6] = @round($row['join_for_rebirth_1_count'] /$row['join_for_rebirth_1_num'], 2);
                $data[$k][7] = @round($row['join_for_rebirth_2_count'] /$row['join_for_rebirth_2_num'], 2);
                $data[$k][8] = @round($row['join_for_rebirth_3_count'] /$row['join_for_rebirth_3_num'], 2);
                $data[$k][9] = @round($row['join_for_rebirth_4_count'] /$row['join_for_rebirth_4_num'], 2);
                $data[$k][10] = @round($row['join_for_rebirth_5_count'] /$row['join_for_rebirth_5_num'], 2);
                $data[$k][11] = @round($row['join_for_rebirth_6_count'] /$row['join_for_rebirth_6_num'], 2);
                $data[$k][12] = @round($row['join_for_rebirth_7_count'] /$row['join_for_rebirth_7_num'], 2) ;
            }
        }

        $result = array();

        $result[] =  array('日期','服务器' , '活跃人数' , '参与人数' , '参与度', '50级人均参与次数', '1转人均参与次数', '2转人均参与次数', '3转人均参与次数', '4转人均参与次数', '5转人均参与次数', '6转人均参与次数', '7转人均参与次数');

        foreach($data as $row){
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result , '','单人BOSS（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('*');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}