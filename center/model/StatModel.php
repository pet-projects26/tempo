<?php
class StatModel extends Model{

    public function __construct(){
        parent::__construct('');
        $this->setNewPrefix('stat_');
        $this->alias = 'cd';
    }

    public function summary_package_data($conditions){
        $rs = array();
        $num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        $rs = $this->get_summary_package_data($conditions);
        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }
    public function get_summary_package_data($conditions){

        if ($conditions['WHERE']['server::IN']) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['s.server::IN'] = $server;
        }

        if ($conditions['WHERE']['package::IN']) {
            $server = $conditions['WHERE']['package::IN'];
            unset($conditions['WHERE']['package::IN']);
            $conditions['WHERE']['s.package::IN'] = $server;
        }
        //默认最近七天
        $conditions['WHERE']['s.time::>='] = isset($conditions['WHERE']['s.time::>=']) ? $conditions['WHERE']['s.time::>='] : strtotime(date("Y-m-d", time())) - 86400 * 6;
        $conditions['WHERE']['s.time::<='] = isset($conditions['WHERE']['s.time::<=']) ? $conditions['WHERE']['s.time::<='] : strtotime(date("Y-m-d", time())) + 86400 - 1;


        parent::__construct('summary');
        $this->alias = 's';
        $this->joinTable = array(
            'r' => array('name' => 'remain', 'type' => 'LEFT', 'on' => 'r.time = s.time and r.server = s.server and r.package = s.package')
        );

        $fields = array('s.time', 's.package', 'sum(s.acc_reg_num) as acc_reg_num', 'sum(s.login_num) as login_num', 'sum(s.active_num) as active_num', 'sum(s.charge_num) as charge_num', 'sum(s.charge_money) as charge_money', 'sum(r.login_2) as login_2', 'sum(r.login_3) as login_3', 'sum(r.login_7) as login_7', 'sum(r.login_15) as login_15', 'sum(r.login_30) as login_30', 'sum(r.reg_num) as register_num');

        $conditions['Extends']['GROUP'] = "s.time,s.package";

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        //     	unset($conditions['Extends']['GROUP']);

        //     	$all = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

        //     	$rs = array_merge($all, $rs);

        $result = array();

        if ($rs[0]['time']) {
            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);//时间
                $result[$k]['package'] = $v['package'];//包号
                $result[$k]['acc_reg_num'] = $v['acc_reg_num'];//注册数
//     			$result[$k]['active_num'] = $v['active_num'];//活跃数
                $result[$k]['login_num'] = $v['login_num'];//登录数
                $result[$k]['charge_num'] = $v['charge_num'];//充值人数
                $result[$k]['charge_money'] = $v['charge_money'];//充值金额

                $result[$k]['login_2'] = $v['login_2'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_2'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_3'] = $v['login_3'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_3'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_7'] = $v['login_7'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_7'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_15'] = $v['login_15'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_15'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_30'] = $v['login_30'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_30'] / $v['register_num'] * 100) . '%' : '0%';

            }

        }

        return $result;
    }

    public function summary_package_export($conditions){

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_package_data($conditions);

        $result = array();
        $result[] = array('日期', '包号', '注册数', '登录数', '充值人数', '充值金额', '次留', '3留', '7留', '15留', '30留');
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '包数据汇总（' . date('Y年m月d日H时i分s秒') . '）');
    }

    //帐号留存
    public function remain_account_data($conditions){
        $rs = array();

        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $result = $this->getaccountremain($conditions);

        foreach ($result as $k => $row) {

            $rs[$k] = array_values($row);
        }
        $num = count($rs);

        $this->setPageData($rs, $num, $limit);
    }
    public function remain_account_export($conditions){

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->getaccountremain($conditions);

        $result = array();
        $result[] = array('日期', '账号数', '第2天', '第3天', '第4天', '第5天', '第6天', '第7天', '第8天', '第9天', '第10天', '第11天', '第12天', '第13天', '第14天', '第15天', '第16天', '第17天', '第18天', '第19天', '第20天', '第21天', '第22天', '第23天', '第24天', '第25天', '第26天', '第27天', '第28天', '第29天', '第30天');
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '账号留存（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * [getremain 获取留存数据]
     * @param  [array] $conditions [搜索的条件]
     * @return [array]             [返回的数组]
     */
    public function getaccountremain($conditions){
        parent::__construct('account_remain');
        parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999

        $fields = array('time', 'sum(reg_num) as reg_num', 'sum(login_2) as login_2', 'sum(login_3) as login_3', 'sum(login_4) as login_4', 'sum(login_5) as login_5', 'sum(login_6) as login_6', 'sum(login_7) as login_7', 'sum(login_8) as login_8', 'sum(login_9) as login_9', 'sum(login_10) as login_10', 'sum(login_11) as login_11', 'sum(login_12) as login_12', 'sum(login_13) as login_13', 'sum(login_14) as login_14', 'sum(login_15) as login_15', 'sum(login_16) as login_16', 'sum(login_17) as login_17', 'sum(login_18) as login_18', 'sum(login_19) as login_19', 'sum(login_20) as login_20', 'sum(login_21) as login_21', 'sum(login_22) as login_22', 'sum(login_23) as login_23', 'sum(login_24) as login_24', 'sum(login_25) as login_25', 'sum(login_26) as login_26', 'sum(login_27) as login_27', 'sum(login_28) as login_28', 'sum(login_29) as login_29', 'sum(login_30) as login_30');

        $conditions['Extends']['GROUP'] = "time";
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['Extends']['GROUP']);

        $all = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = array_merge($all, $rs);

        $result = array();
        if ($rs) {
            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);
                $result[$k]['reg_num'] = $v['reg_num'];
                $result[$k]['login_2'] = $v['login_2'] != 0 && $v['reg_num'] != 0 ? $v['login_2'] . '(' . sprintf('%.2f', $v['login_2'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_3'] = $v['login_3'] != 0 && $v['reg_num'] != 0 ? $v['login_3'] . '(' . sprintf('%.2f', $v['login_3'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_4'] = $v['login_4'] != 0 && $v['reg_num'] != 0 ? $v['login_4'] . '(' . sprintf('%.2f', $v['login_4'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_5'] = $v['login_5'] != 0 && $v['reg_num'] != 0 ? $v['login_5'] . '(' . sprintf('%.2f', $v['login_5'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_6'] = $v['login_6'] != 0 && $v['reg_num'] != 0 ? $v['login_6'] . '(' . sprintf('%.2f', $v['login_6'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_7'] = $v['login_7'] != 0 && $v['reg_num'] != 0 ? $v['login_7'] . '(' . sprintf('%.2f', $v['login_7'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_8'] = $v['login_8'] != 0 && $v['reg_num'] != 0 ? $v['login_8'] . '(' . sprintf('%.2f', $v['login_8'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_9'] = $v['login_9'] != 0 && $v['reg_num'] != 0 ? $v['login_9'] . '(' . sprintf('%.2f', $v['login_9'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_10'] = $v['login_10'] != 0 && $v['reg_num'] != 0 ? $v['login_10'] . '(' . sprintf('%.2f', $v['login_10'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_11'] = $v['login_11'] != 0 && $v['reg_num'] != 0 ? $v['login_11'] . '(' . sprintf('%.2f', $v['login_11'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_12'] = $v['login_12'] != 0 && $v['reg_num'] != 0 ? $v['login_12'] . '(' . sprintf('%.2f', $v['login_12'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_13'] = $v['login_13'] != 0 && $v['reg_num'] != 0 ? $v['login_13'] . '(' . sprintf('%.2f', $v['login_13'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_14'] = $v['login_14'] != 0 && $v['reg_num'] != 0 ? $v['login_14'] . '(' . sprintf('%.2f', $v['login_14'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_15'] = $v['login_15'] != 0 && $v['reg_num'] != 0 ? $v['login_15'] . '(' . sprintf('%.2f', $v['login_15'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_16'] = $v['login_16'] != 0 && $v['reg_num'] != 0 ? $v['login_16'] . '(' . sprintf('%.2f', $v['login_16'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_17'] = $v['login_17'] != 0 && $v['reg_num'] != 0 ? $v['login_17'] . '(' . sprintf('%.2f', $v['login_17'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_18'] = $v['login_18'] != 0 && $v['reg_num'] != 0 ? $v['login_18'] . '(' . sprintf('%.2f', $v['login_18'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_19'] = $v['login_19'] != 0 && $v['reg_num'] != 0 ? $v['login_19'] . '(' . sprintf('%.2f', $v['login_19'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_20'] = $v['login_20'] != 0 && $v['reg_num'] != 0 ? $v['login_20'] . '(' . sprintf('%.2f', $v['login_20'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_21'] = $v['login_21'] != 0 && $v['reg_num'] != 0 ? $v['login_21'] . '(' . sprintf('%.2f', $v['login_21'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_22'] = $v['login_22'] != 0 && $v['reg_num'] != 0 ? $v['login_22'] . '(' . sprintf('%.2f', $v['login_22'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_23'] = $v['login_23'] != 0 && $v['reg_num'] != 0 ? $v['login_23'] . '(' . sprintf('%.2f', $v['login_23'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_24'] = $v['login_24'] != 0 && $v['reg_num'] != 0 ? $v['login_24'] . '(' . sprintf('%.2f', $v['login_24'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_25'] = $v['login_25'] != 0 && $v['reg_num'] != 0 ? $v['login_25'] . '(' . sprintf('%.2f', $v['login_25'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_26'] = $v['login_26'] != 0 && $v['reg_num'] != 0 ? $v['login_26'] . '(' . sprintf('%.2f', $v['login_26'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_27'] = $v['login_27'] != 0 && $v['reg_num'] != 0 ? $v['login_27'] . '(' . sprintf('%.2f', $v['login_27'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_28'] = $v['login_28'] != 0 && $v['reg_num'] != 0 ? $v['login_28'] . '(' . sprintf('%.2f', $v['login_28'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_29'] = $v['login_29'] != 0 && $v['reg_num'] != 0 ? $v['login_29'] . '(' . sprintf('%.2f', $v['login_29'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_30'] = $v['login_30'] != 0 && $v['reg_num'] != 0 ? $v['login_30'] . '(' . sprintf('%.2f', $v['login_30'] / $v['reg_num'] * 100) . '%)' : '0(0%)';

            }
            $result['0']['time'] = '总计';
        }
        return $result;
    }

    public function daily_data($conditions){
        $rs = array();

        $num = 0;

        /*$search = $_REQUEST['sSearch_0'];

        if($search == ''){

            $conditions['WHERE']['time::>='] = strtotime(date('Y-m-d',strtotime('-1 week ')));
            $conditions['WHERE']['time::<='] = time();
        }*/

        unset($conditions['WHERE']['channel::IN']);
        unset($conditions['WHERE']['group::IN']);

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $result = $this->get_daily_data($conditions);

        if (empty($result)) {
            $result = array();
        }

        $num = count($result);

        foreach ($result as $k => $row) {

            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $num , $limit );
    }
    public function daily_export($conditions){
        $rs = array();
        $num = 0;
        /*$search = $_REQUEST['sSearch_0'];
        if($search == ''){
            $conditions['WHERE']['time::>='] = strtotime(date('Y-m-d',strtotime('-1 week ')));
            $conditions['WHERE']['time::<='] = time() ;
        }*/
        unset($conditions['WHERE']['channel::IN']);
        unset($conditions['WHERE']['group::IN']);

        unset($conditions['Extends']['LIMIT']);

        $result = $this->get_daily_data($conditions);



        $fields = array();
        $fields[] = array('日期' , '新增充值人数' ,'新增充值总额' , '首充人数' , '首充金额' , '充值总额' , '充值人数' , '充值次数' ,'新增充值ARPU' , 'ARPU' ,'登录ARPU', ' 注册ARPU ' );
        foreach($result as $row){
            $fields[] = $this->formatFields($row);
        }

        Util::exportExcel($fields , '每日充值（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * [get_daily_data 获取每日充值数据]
     * @param  [array] $conditions [搜索条件]
     * @return [array] $rs         [返回查询到的数组]
     */
    public function get_daily_data($conditions)
    {
        //单天
        parent::__construct('charge_daily');
        parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
        $fields = array('time', 'sum(reg_num) as reg_num', 'sum(login_num) as login_num', 'sum(money) as money', 'sum(charge_num) as charge_num', 'sum(charge_times) as charge_times ', 'sum(new_charge_num) as new_charge_num', 'sum(new_charge_money) as new_charge_money', 'sum(first_charge_num) as first_charge_num', 'sum(first_charge_money) as first_charge_money');

        $conditions['Extends']['GROUP'] = "time";
        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        if ($result) {
            foreach ($result as $k => $v) {
                $rs[$k]['time'] = date('Y-m-d', $v['time']);//日期
                $rs[$k]['new_charge_num'] = $v['new_charge_num'];//新增充值人数
                $rs[$k]['new_charge_money'] = $v['new_charge_money'];//新增充值总额
                $rs[$k]['first_charge_num'] = $v['first_charge_num'];//首充人数
                $rs[$k]['first_charge_money'] = $v['first_charge_money'];//首充金额
                $rs[$k]['money'] = $v['money']; //充值总额
                $rs[$k]['charge_num'] = $v['charge_num'];//充值人数
                $rs[$k]['charge_times'] = $v['charge_times'];//充值次数
                $rs[$k]['new_arpu'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['new_charge_money'] / $v['charge_num']) : 0.00;//新增充值ARPU
                $rs[$k]['arpu'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['money'] / $v['charge_num']) : 0.00;//ARPU
                $rs[$k]['login_arpu'] = $v['login_num'] != 0 ? sprintf('%.2f', $v['money'] / $v['login_num']) : 0.00;//登录ARPU
                $rs[$k]['reg_arpu'] = $v['reg_num'] != 0 ? sprintf('%.2f', $v['money'] / $v['reg_num']) : 0.00;//注册ARPU
            }

            //总计
            $sumfields = array('sum(reg_num) as reg_num', 'sum(login_num) as login_num', 'sum(money) as money', 'sum(charge_num) as charge_num', 'sum(charge_times) as charge_times ', 'sum(new_charge_num) as new_charge_num', 'sum(new_charge_money) as new_charge_money', 'sum(first_charge_num) as first_charge_num', 'sum(first_charge_money) as first_charge_money');

            $sum = $this->getRow($sumfields, $conditions['WHERE']);

            if ($sum) {
                $sumarr['time'] = '总计';//日期
                $sumarr['new_charge_num'] = $sum['new_charge_num'];//新增充值人数
                $sumarr['new_charge_money'] = $sum['new_charge_money'];//新增充值总额
                $sumarr['first_charge_num'] = $sum['first_charge_num'];//首充人数
                $sumarr['first_charge_money'] = $sum['first_charge_money'];//首充金额
                $sumarr['money'] = $sum['money']; //充值总额
                $sumarr['charge_num'] = $sum['charge_num'];//充值人数
                $sumarr['charge_times'] = $sum['charge_times'];//充值次数
                $sumarr['new_arpu'] = $sum['charge_num'] != 0 ? sprintf('%.2f', $sum['new_charge_money'] / $sum['charge_num']) : 0.00;//新增充值ARPU
                $sumarr['arpu'] = $sum['charge_num'] != 0 ? sprintf('%.2f', $sum['money'] / $sum['charge_num']) : 0.00;//ARPU
                $sumarr['login_arpu'] = $sum['login_num'] != 0 ? sprintf('%.2f', $sum['money'] / $sum['login_num']) : 0.00;//登录ARPU
                $sumarr['reg_arpu'] = $sum['reg_num'] != 0 ? sprintf('%.2f', $sum['money'] / $sum['reg_num']) : 0.00;//注册ARPU
                array_unshift($rs, $sumarr);
            }
        }

        return $rs;
    }

    public function ltv_data($conditions)
    {

        $rs = array();

        $num = 0;

        /*$search = $_REQUEST['sSearch_0'];

        if($search == ''){

            $conditions['WHERE']['time::>='] = strtotime(date('Y-m-d',strtotime('-1 week ')));
            $conditions['WHERE']['time::<='] = time()  ;
        }*/

        unset($conditions['WHERE']['channel::IN']);
        unset($conditions['WHERE']['group::IN']);

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $result = $this->get_ltv_data($conditions);
        $num = count($result);

        foreach ($result as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $num , $limit);
    }

    public function ltv_export($conditions)
    {
        $rs = array();

        $num = 0;
        //$search = $_REQUEST['sSearch_0'];

        /*if($search == ''){

            $conditions['WHERE']['time::>='] = strtotime(date('Y-m-d',strtotime('-1 week ')));

            $conditions['WHERE']['time::<='] = time() ;
        }*/
        unset($conditions['WHERE']['channel::IN']);

        unset($conditions['WHERE']['group::IN']);

        unset($conditions['Extends']['LIMIT']);

        $result = $this->get_ltv_data($conditions);

        $fields = array();

        $fields[] = array('日期', '创角数' , '1天' , '2天' , '3天' , '4天' , '5天' , '6天' , '7天' , '14天' , '30天' , '60天' , '90天');

        foreach($result as $row){

            $fields[] = $this->formatFields($row);
        }
        if ($conditions['WHERE']['type'] == 1) {
            $name = '创角LTV';
        } else {
            $name = '注册LTV';
        }

        Util::exportExcel($fields, $name . '（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * [get_rolecreate_data 创角LTV数据]
     * @param  [array] $conditions [搜索条件]
     * @return [array] $result     [返回的数组]
     */
    public function get_ltv_data($conditions)
    {

        parent::__construct('ltv');

        //单天
        $fields = array('time', 'sum(num) as num', 'sum(day1) as day1', 'sum(day2) as day2', 'sum(day3) as day3', 'sum(day4) as day4', 'sum(day5) as day5', 'sum(day6) as day6', 'sum(day7) as day7', 'sum(day14) as day14', 'sum(day30) as day30', 'sum(day60) as day60', 'sum(day90) as day90');

        $conditions['Extends']['GROUP'] = "time";

        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($result) {

            foreach ($result as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);//日期
                $result[$k]['day1'] = $v['num'] != 0 ? sprintf('%.2f', $v['day1'] / $v['num']) : 0.00;
                $result[$k]['day2'] = $v['num'] != 0 ? sprintf('%.2f', $v['day2'] / $v['num']) : 0.00;
                $result[$k]['day3'] = $v['num'] != 0 ? sprintf('%.2f', $v['day3'] / $v['num']) : 0.00;
                $result[$k]['day4'] = $v['num'] != 0 ? sprintf('%.2f', $v['day4'] / $v['num']) : 0.00;
                $result[$k]['day5'] = $v['num'] != 0 ? sprintf('%.2f', $v['day5'] / $v['num']) : 0.00;
                $result[$k]['day6'] = $v['num'] != 0 ? sprintf('%.2f', $v['day6'] / $v['num']) : 0.00;
                $result[$k]['day7'] = $v['num'] != 0 ? sprintf('%.2f', $v['day7'] / $v['num']) : 0.00;
                $result[$k]['day14'] = $v['num'] != 0 ? sprintf('%.2f', $v['day14'] / $v['num']) : 0.00;
                $result[$k]['day30'] = $v['num'] != 0 ? sprintf('%.2f', $v['day30'] / $v['num']) : 0.00;
                $result[$k]['day60'] = $v['num'] != 0 ? sprintf('%.2f', $v['day60'] / $v['num']) : 0.00;
                $result[$k]['day90'] = $v['num'] != 0 ? sprintf('%.2f', $v['day90'] / $v['num']) : 0.00;
            }
            //总计
            $sum = $this->getRow($fields, $conditions['WHERE']);

            $sum['time'] = '总计';//日期
            $sum['day1'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day1'] / $sum['num']) : 0.00;
            $sum['day2'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day2'] / $sum['num']) : 0.00;
            $sum['day3'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day3'] / $sum['num']) : 0.00;
            $sum['day4'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day4'] / $sum['num']) : 0.00;
            $sum['day5'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day5'] / $sum['num']) : 0.00;
            $sum['day6'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day6'] / $sum['num']) : 0.00;
            $sum['day7'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day7'] / $sum['num']) : 0.00;
            $sum['day14'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day14'] / $sum['num']) : 0.00;
            $sum['day30'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day30'] / $sum['num']) : 0.00;
            $sum['day60'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day60'] / $sum['num']) : 0.00;
            $sum['day90'] = $sum['num'] != 0 ? sprintf('%.2f', $sum['day90'] / $sum['num']) : 0.00;

            array_unshift($result, $sum);

        }
        return $result;

    }

    //角色帐号留存
    public function remain_data($conditions)
    {
        $rs = array();

        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $result = $this->getremain($conditions);

        foreach ($result as $k => $row) {

            $rs[$k] = array_values($row);
        }
        $num = count($rs);

        $this->setPageData($rs , $num ,$limit );
    }
    public function remain_export($conditions){

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->getremain($conditions);

        $result = array();
        $result[] = array('日期' , '创角数' , '第2天' , '第3天' , '第4天' , '第5天' , '第6天' , '第7天' , '第8天' , '第9天' , '第10天' , '第11天' , '第12天' , '第13天' , '第14天' , '第15天' , '第16天' , '第17天' , '第18天' , '第19天' , '第20天' , '第21天' , '第22天' , '第23天' , '第24天' , '第25天' , '第26天' , '第27天' , '第28天' , '第29天' , '第30天');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '创角留存（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * [getremain 获取留存数据]
     * @param  [array] $conditions [搜索的条件]
     * @return [array]             [返回的数组]
     */
    public function getremain($conditions)
    {
        parent::__construct('remain');
        //parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999

        $fields = array('time', 'sum(reg_num) as reg_num', 'sum(login_2) as login_2', 'sum(login_3) as login_3', 'sum(login_4) as login_4', 'sum(login_5) as login_5', 'sum(login_6) as login_6', 'sum(login_7) as login_7', 'sum(login_8) as login_8', 'sum(login_9) as login_9', 'sum(login_10) as login_10', 'sum(login_11) as login_11', 'sum(login_12) as login_12', 'sum(login_13) as login_13', 'sum(login_14) as login_14', 'sum(login_15) as login_15', 'sum(login_16) as login_16', 'sum(login_17) as login_17', 'sum(login_18) as login_18', 'sum(login_19) as login_19', 'sum(login_20) as login_20', 'sum(login_21) as login_21', 'sum(login_22) as login_22', 'sum(login_23) as login_23', 'sum(login_24) as login_24', 'sum(login_25) as login_25', 'sum(login_26) as login_26', 'sum(login_27) as login_27', 'sum(login_28) as login_28', 'sum(login_29) as login_29', 'sum(login_30) as login_30');

        $conditions['Extends']['GROUP'] = "time";
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['Extends']['GROUP']);

        $all = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = array_merge($all, $rs);

        $result = array();
        if ($rs) {
            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);
                $result[$k]['reg_num'] = $v['reg_num'];
                $result[$k]['login_2'] = $v['login_2'] != 0 && $v['reg_num'] != 0 ? $v['login_2'] . '(' . sprintf('%.2f', $v['login_2'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_3'] = $v['login_3'] != 0 && $v['reg_num'] != 0 ? $v['login_3'] . '(' . sprintf('%.2f', $v['login_3'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_4'] = $v['login_4'] != 0 && $v['reg_num'] != 0 ? $v['login_4'] . '(' . sprintf('%.2f', $v['login_4'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_5'] = $v['login_5'] != 0 && $v['reg_num'] != 0 ? $v['login_5'] . '(' . sprintf('%.2f', $v['login_5'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_6'] = $v['login_6'] != 0 && $v['reg_num'] != 0 ? $v['login_6'] . '(' . sprintf('%.2f', $v['login_6'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_7'] = $v['login_7'] != 0 && $v['reg_num'] != 0 ? $v['login_7'] . '(' . sprintf('%.2f', $v['login_7'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_8'] = $v['login_8'] != 0 && $v['reg_num'] != 0 ? $v['login_8'] . '(' . sprintf('%.2f', $v['login_8'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_9'] = $v['login_9'] != 0 && $v['reg_num'] != 0 ? $v['login_9'] . '(' . sprintf('%.2f', $v['login_9'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_10'] = $v['login_10'] != 0 && $v['reg_num'] != 0 ? $v['login_10'] . '(' . sprintf('%.2f', $v['login_10'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_11'] = $v['login_11'] != 0 && $v['reg_num'] != 0 ? $v['login_11'] . '(' . sprintf('%.2f', $v['login_11'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_12'] = $v['login_12'] != 0 && $v['reg_num'] != 0 ? $v['login_12'] . '(' . sprintf('%.2f', $v['login_12'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_13'] = $v['login_13'] != 0 && $v['reg_num'] != 0 ? $v['login_13'] . '(' . sprintf('%.2f', $v['login_13'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_14'] = $v['login_14'] != 0 && $v['reg_num'] != 0 ? $v['login_14'] . '(' . sprintf('%.2f', $v['login_14'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_15'] = $v['login_15'] != 0 && $v['reg_num'] != 0 ? $v['login_15'] . '(' . sprintf('%.2f', $v['login_15'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_16'] = $v['login_16'] != 0 && $v['reg_num'] != 0 ? $v['login_16'] . '(' . sprintf('%.2f', $v['login_16'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_17'] = $v['login_17'] != 0 && $v['reg_num'] != 0 ? $v['login_17'] . '(' . sprintf('%.2f', $v['login_17'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_18'] = $v['login_18'] != 0 && $v['reg_num'] != 0 ? $v['login_18'] . '(' . sprintf('%.2f', $v['login_18'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_19'] = $v['login_19'] != 0 && $v['reg_num'] != 0 ? $v['login_19'] . '(' . sprintf('%.2f', $v['login_19'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_20'] = $v['login_20'] != 0 && $v['reg_num'] != 0 ? $v['login_20'] . '(' . sprintf('%.2f', $v['login_20'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_21'] = $v['login_21'] != 0 && $v['reg_num'] != 0 ? $v['login_21'] . '(' . sprintf('%.2f', $v['login_21'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_22'] = $v['login_22'] != 0 && $v['reg_num'] != 0 ? $v['login_22'] . '(' . sprintf('%.2f', $v['login_22'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_23'] = $v['login_23'] != 0 && $v['reg_num'] != 0 ? $v['login_23'] . '(' . sprintf('%.2f', $v['login_23'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_24'] = $v['login_24'] != 0 && $v['reg_num'] != 0 ? $v['login_24'] . '(' . sprintf('%.2f', $v['login_24'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_25'] = $v['login_25'] != 0 && $v['reg_num'] != 0 ? $v['login_25'] . '(' . sprintf('%.2f', $v['login_25'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_26'] = $v['login_26'] != 0 && $v['reg_num'] != 0 ? $v['login_26'] . '(' . sprintf('%.2f', $v['login_26'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_27'] = $v['login_27'] != 0 && $v['reg_num'] != 0 ? $v['login_27'] . '(' . sprintf('%.2f', $v['login_27'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_28'] = $v['login_28'] != 0 && $v['reg_num'] != 0 ? $v['login_28'] . '(' . sprintf('%.2f', $v['login_28'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_29'] = $v['login_29'] != 0 && $v['reg_num'] != 0 ? $v['login_29'] . '(' . sprintf('%.2f', $v['login_29'] / $v['reg_num'] * 100) . '%)' : '0(0%)';
                $result[$k]['login_30'] = $v['login_30'] != 0 && $v['reg_num'] != 0 ? $v['login_30'] . '(' . sprintf('%.2f', $v['login_30'] / $v['reg_num'] * 100) . '%)' : '0(0%)';

            }
            $result['0']['time'] = '总计';
        }
        return $result;
    }

    //角色数据汇总
    public function summary_role_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_role_data($conditions);

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }

    public function summary_role_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_role_data($conditions);

        $result = array();
        $result[] = array('日期','注册数' , '创角数' , '登录数' , '活跃数', '活跃AP' , '老角色数' , '充值人数' , '充值金额' , '登录付费率' , '付费AP' , '新充值人数' ,
            '新充值金额', '新增付费率', '新充值AP', '老充值人数', '老充值金额', '老付费率', '老充值AP', '登录AP', '注册AP',
            '最高在线数'  , '次留' , '3留' , '4留' , '5留' , '6留' , '7留' , '15留' , '30留');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '角色数据汇总（' . date('Y年m月d日H时i分s秒') . '）');

    }

    /**
     * [get_summary_role_data 数据汇总]
     * @param  [array] $conditions [搜索条件]
     * @return [array]             [返回结果]
     */
    public function get_summary_role_data($conditions)
    {

        if ($conditions['WHERE']['server::IN']) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['s.server::IN'] = $server;
        }

        if ($conditions['WHERE']['package::IN']) {
            $server = $conditions['WHERE']['package::IN'];
            unset($conditions['WHERE']['package::IN']);
            $conditions['WHERE']['s.package::IN'] = $server;
        }

        //默认最近七天
        $conditions['WHERE']['s.time::>='] = isset($conditions['WHERE']['s.time::>=']) ? $conditions['WHERE']['s.time::>='] : strtotime(date("Y-m-d", time())) - 86400 * 6;
        $conditions['WHERE']['s.time::<='] = isset($conditions['WHERE']['s.time::<=']) ? $conditions['WHERE']['s.time::<='] : strtotime(date("Y-m-d", time())) + 86400 - 1;

        parent::__construct('summary');
        $this->alias = 's';
        $this->joinTable = array(
            'r' => array('name' => 'remain' , 'type' => 'LEFT' , 'on' => 'r.time = s.time and r.server = s.server and r.package = s.package')
        );

        $fields = array('s.time', 'sum(s.active_charge_money) as active_charge_money', 'sum(s.active_charge_num) as active_charge_num', 'sum(s.acc_reg_num) as acc_reg_num', 'sum(r.reg_num) as reg_num', 'sum(s.login_num) as login_num', 'sum(s.active_num) as active_num', 'sum(s.old_num) as old_num', 'sum(s.charge_num) as charge_num', 'sum(s.charge_money) as charge_money', 'sum(s.new_charge_num) as new_charge_num', 'sum(s.new_charge_money) as new_charge_money', 'sum(s.old_charge_num) as old_charge_num', 'sum(s.old_charge_money) as old_charge_money', 'max(s.max_online_num) as max_online_num', 'avg(s.avg_online_num) as avg_online_num', 'sum(r.login_2) as login_2', 'sum(r.login_3) as login_3', 'sum(r.login_4) as login_4', 'sum(r.login_5) as login_5', 'sum(r.login_6) as login_6', 'sum(r.login_7) as login_7', 'sum(r.login_15) as login_15', 'sum(r.login_30) as login_30', 'sum(r.reg_num) as register_num');

        $conditions['Extends']['GROUP'] = "s.time";

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['Extends']['GROUP']);

        $all = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = array_merge($all, $rs);

        $result = array();

        if ($rs[0]['time']) {
            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);//时间
                $result[$k]['acc_reg_num'] = $v['acc_reg_num'];//注册数
                $result[$k]['reg_num'] = $v['reg_num'];//创角数
                $result[$k]['login_num'] = $v['login_num'];//登陆数
                $result[$k]['active_num'] = $v['active_num'];//活跃数
                $result[$k]['active_AP'] = $v['active_charge_num'] != 0 ? sprintf('%.2f', $v['active_charge_money'] / $v['active_charge_num']) : 0;//老AP
                $result[$k]['old_num'] = $v['old_num'];//老角色数
                $result[$k]['charge_num'] = $v['charge_num'];//充值人数
                $result[$k]['charge_money'] = $v['charge_money'];//充值金额


                $result[$k]['login_rate'] = $v['login_num'] != 0 ? sprintf('%.2f', $v['charge_num'] / $v['login_num'] * 100) . '%' : '0%';//登录付费率
                $result[$k]['AP'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['charge_num']) : 0;//登录AP

                $result[$k]['new_charge_num'] = $v['new_charge_num'];//新充值人数
                $result[$k]['new_charge_money'] = $v['new_charge_money'];//新充值金额
                $result[$k]['new_rate'] = $v['reg_num'] != 0 ? sprintf('%.2f', $v['new_charge_num'] / $v['reg_num'] * 100) . '%' : '0%';//新增付费率
                $result[$k]['new_AP'] = $v['new_charge_num'] != 0 ? sprintf('%.2f', $v['new_charge_money'] / $v['new_charge_num']) : 0;//新AP

                $result[$k]['old_charge_num'] = $v['old_charge_num'];//老充值人数
                $result[$k]['old_charge_money'] = $v['old_charge_money'];//老充值金额
                $result[$k]['old_rate'] = $v['old_num'] != 0 ? sprintf('%.2f', $v['old_charge_num'] / $v['old_num'] * 100) . '%' : '0%';//新增付费率
                $result[$k]['old_AP'] = $v['old_charge_num'] != 0 ? sprintf('%.2f', $v['old_charge_money'] / $v['old_charge_num']) : 0;//老AP

                $result[$k]['login_AP'] = $v['login_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['login_num']) : 0;//登录AP

                $result[$k]['reg_AP'] = $v['acc_reg_num'] != 0 ? sprintf('%.2f', $v['new_charge_money'] / $v['acc_reg_num']) : 0;//注册AP

                $result[$k]['max_online_num'] = $v['max_online_num'];//最高在线
                //$result[$k]['avg_online_num'] = sprintf('%.2f' ,$v['avg_online_num']);//平均在线

                $result[$k]['login_2'] = $v['login_2'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_2'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_3'] = $v['login_3'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_3'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_4'] = $v['login_4'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_4'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_5'] = $v['login_5'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_5'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_6'] = $v['login_6'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_6'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_7'] = $v['login_7'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_7'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_15'] = $v['login_15'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_15'] / $v['register_num'] * 100) . '%' : '0%';
                $result[$k]['login_30'] = $v['login_30'] != 0 && $v['register_num'] != 0 ? sprintf('%.2f', $v['login_30'] / $v['register_num'] * 100) . '%' : '0%';

            }

            $result[0]['time'] = '总计';
        }

        return $result;
    }


    //帐号数据汇总
    public function summary_account_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_account_data($conditions);

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }

    public function summary_account_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_account_data($conditions);

        $result = array();
        $result[] = array( '日期','注册数' , '登录数' ,  '老角色数' , '充值人数' , '充值金额' , '登录付费率' , '付费AP' , '新充值人数' ,
            '新充值金额' , '新增付费率' , '新充值AP' , '老充值人数' , '老充值金额' , '老付费率' , '老充值AP' , '登录AP' , '注册AP' );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '帐号数据汇总（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * [get_summary_account_data 帐号数据汇总]
     * @param  [array] $conditions [搜索条件]
     * @return [array]             [返回结果]
     */
    public function get_summary_account_data($conditions)
    {

        parent::__construct('account_summary');


        $fields = array('time', 'sum(reg_num) as reg_num', 'sum(login_num) as login_num', 'sum(old_num) as old_num', 'sum(charge_num) as charge_num', 'sum(charge_money) as charge_money', 'sum(new_charge_num) as new_charge_num', 'sum(new_charge_money) as new_charge_money', 'sum(old_charge_num) as old_charge_num', 'sum(old_charge_money) as old_charge_money');

        $conditions['Extends']['GROUP'] = "time";

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['Extends']['GROUP']);

        $all = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = array_merge($all, $rs);

        $result = array();

        if ($rs[0]['time']) {
            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m-d', $v['time']);//时间

                $result[$k]['reg_num'] = $v['reg_num'];//注册数
                $result[$k]['login_num'] = $v['login_num'];//登陆数

                $result[$k]['old_num'] = $v['old_num'];//老角色数
                $result[$k]['charge_num'] = $v['charge_num'];//充值人数
                $result[$k]['charge_money'] = $v['charge_money'];//充值金额

                $result[$k]['login_rate'] = $v['login_num'] != 0 ? sprintf('%.2f', $v['charge_num'] / $v['login_num'] * 100) . '%' : '0%';//登录付费率
                $result[$k]['AP'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['charge_num']) : 0;//付费AP

                $result[$k]['new_charge_num'] = $v['new_charge_num'];//新充值人数
                $result[$k]['new_charge_money'] = $v['new_charge_money'];//新充值金额
                $result[$k]['new_rate'] = $v['reg_num'] != 0 ? sprintf('%.2f', $v['new_charge_num'] / $v['reg_num'] * 100) . '%' : '0%';//新增付费率
                $result[$k]['new_AP'] = $v['new_charge_num'] != 0 ? sprintf('%.2f', $v['new_charge_money'] / $v['new_charge_num']) : 0;//新AP

                $result[$k]['old_charge_num'] = $v['old_charge_num'];//老充值人数
                $result[$k]['old_charge_money'] = $v['old_charge_money'];//老充值金额
                $result[$k]['old_rate'] = $v['old_num'] != 0 ? sprintf('%.2f', $v['old_charge_num'] / $v['old_num'] * 100) . '%' : '0%';//新增付费率
                $result[$k]['old_AP'] = $v['old_charge_num'] != 0 ? sprintf('%.2f', $v['old_charge_money'] / $v['old_charge_num']) : 0;//老AP

                $result[$k]['login_AP'] = $v['login_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['login_num']) : 0;//登录AP
                $result[$k]['reg_AP'] = $v['reg_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['reg_num']) : 0;//注册AP

            }
            $result[0]['time'] = '总计';
        }
        return $result;
    }

    public function create_analysis_data($conditions)
    {

        parent::__construct('summary');
        $rs = array();
        $num = 0;
        $fields = array('time', 'sum(reg_num) as reg_num', 'sum(reg_no_distinct_num) as reg_no_distinct_num');
        $conditions['Extends']['GROUP'] = 'time';
        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($result) {
            $num = $this->getCount();
            $rs = array();

            unset($conditions['Extends']['GROUP']);
            $total = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

            $result = array_merge($total, $result);

            foreach ($result as $k => $v) {
                $rs[$k]['time'] = date('Y-m-d', $v['time']);
                $rs[$k]['reg_num'] = $v['reg_num'];
                $rs[$k]['reg_no_distinct_num'] = $v['reg_no_distinct_num'];
                $rs[$k]['reg_no_percentage'] = $v['reg_num'] != 0 ? sprintf('%.2f', $v['reg_no_distinct_num'] / $v['reg_num'] * 100) . '%' : '0%';
                $rs[$k]['reg_repeat_num'] = $v['reg_num'] - $v['reg_no_distinct_num'];

                $rs[$k]['reg_repeat_percentage'] = $v['reg_num'] != 0 ? sprintf('%.2f', ($v['reg_num'] - $v['reg_no_distinct_num']) / $v['reg_num'] * 100) . '%' : '0%';
            }
            $rs[0]['time'] = '总计';


            foreach ($rs as $k => $v) {
                $rs[$k] = array_values($v);
            }

        }

        $this->setPageData($rs, $num);

    }

    //登录统计
    public function login($conditions)
    {
        parent::__construct('login');
        $rs = array();
        $num = 0;
        $fields = array('time', 'sum(login_num) as login_num', 'sum(login_times) as login_times', 'sum(old_num) as old_num', 'sum(old_online_time) as old_online_time', 'sum(active_num) as active_num', 'sum(fans_num) as fans_num');
        $conditions['Extends']['GROUP'] = 'time';

        //默认最近七天
        $conditions['WHERE']['time::>='] = isset($conditions['WHERE']['time::>=']) ? $conditions['WHERE']['time::>='] : strtotime(date("Y-m-d", time())) - 86400 * 6;
        $conditions['WHERE']['time::<='] = isset($conditions['WHERE']['time::<=']) ? $conditions['WHERE']['time::<='] : strtotime(date("Y-m-d", time())) + 86400 - 1;

        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($result) {
            $num = $this->getCount();
            unset($conditions['Extends']['GROUP']);
            $total = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

            $result = array_merge($total, $result);

            foreach ($result as $k => $v) {
                $rs[$k]['time'] = date('Y-m-d', $v['time']);
                $rs[$k]['login_num'] = $v['login_num'];
                $rs[$k]['login_times'] = $v['login_times'];
                $rs[$k]['avg_login_times'] = $v['login_num'] != 0 ? sprintf('%.2f', $v['login_times'] / $v['login_num']) : 0;
                $rs[$k]['old_num'] = $v['old_num'];
                $rs[$k]['old_online_time'] = $this->time_format1(ceil($v['old_online_time'] / $v['old_num']));
                $rs[$k]['active_num'] = $v['active_num'];
                $rs[$k]['fans_num'] = $v['fans_num'];
            }
            $rs[0]['time'] = '总计';
            foreach ($rs as $k => $v) {
                $rs[$k] = array_values($v);
            }
        }
        $this->setPageData($rs, $num);

    }


    public function time_format1($duration)
    {
        $time_set = array('天' => 86400 , '时' => 3600 , '分' => 60 , '秒' => 1);
        $duration = $duration;//秒
        foreach($time_set as $key => $val){
            $time_set[$key] = floor($duration / $val);
            $duration = $duration % $val;
            if($time_set[$key] == 0){
                unset($time_set[$key]);
            }
            else{
                $time_set[$key] .= $key;
            }
        }
        $duration = implode('' , $time_set);
        return $duration;
    }

    //消耗产出
    public function item_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $result = $this->get_item_data($conditions);

        if ($result) {
            foreach($result as $k => $row){
                $rs[$k] = array_values($row);
            }
        }

        $num = count($rs);
        $this->setPageData($rs, $num, $limit);

    }

    //消耗产出的导出
    public function item_export($conditions)
    {

        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        $rs = $this->get_item_data($conditions);
        $name = CDict::$coinType[$conditions['WHERE']['type']]['name'];
        $result = array();
        $result[] = array(
            '日期' , '产生' . $name , '参与产生数' ,  '消耗' . $name , '参与消耗数' ,'库存'. $name ,  '总产出', '总消耗' , '滞留率'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , $name . '产出消耗（' . date('Y年m月d日H时i分s秒') . '）');

    }
    /**
     * [get_item_data 消耗产出数据源]
     * @param  [array] $conditions [description]
     * @return [array]             [description]
     */

    /**
     * [get_item_data 消耗产出数据源]
     * @param  [array] $conditions [description]
     * @return [array]             [description]
     */
    public function get_item_data($conditions)
    {
        parent::__construct('item');
        $fields = array('time', 'sum(use_role_num) as use_role_num', 'sum(get_num) as get_num', 'sum(get_role_num) as get_role_num', 'sum(inventory) as inventory', 'sum(use_num) as use_num', 'sum(total_get) as total_get', 'sum(total_use) as total_use',
            'GROUP_CONCAT(use_item_id) AS use_item_id',
            'GROUP_CONCAT(use_source) AS use_source',
            'GROUP_CONCAT(get_item_id) AS get_item_id',
            'GROUP_CONCAT(get_source) AS get_source', 'type', 'GROUP_CONCAT(use_coin) AS use_coin', 'GROUP_CONCAT(use_coin_source) AS use_coin_source'
        );
        $conditions['Extends']['GROUP'] = 'time';

        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($result) {
            unset($conditions['Extends']['GROUP']);
            $total = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

// 			$total[0]['inventory'] = $total[0]['get_num'] - $total[0]['use_num'] ;
// 			$total[0]['total_get'] = $total[0]['get_num'];
// 			$total[0]['total_use'] = $total[0]['use_num'];

            $result = array_merge($total, $result);
            foreach ($result as $key => $value) {
                $get_item = <<< END
        		<a href="javascript:void(0)" onclick="logup('{$value['get_item_id']}','{$value['get_source']}')" style="text-decoration: underline;">点击进入查看</a> | <a href="javascript:void(0)" onclick="export_logup('{$value['get_item_id']}','{$value['get_source']}')" style="text-decoration: underline;">导出</a>
END;
                $use_item = <<< END
        		<a href="javascript:void(0)" onclick="logup('{$value['use_item_id']}','{$value['use_source']}')" style="text-decoration: underline;">点击进入查看</a> | <a href="javascript:void(0)" onclick="export_logup('{$value['use_item_id']}','{$value['use_source']}')" style="text-decoration: underline;">导出</a>
END;
                $use_coin = <<< END
        		<a href="javascript:void(0)" onclick="logup_coin('{$value['use_coin']}','{$value['use_coin_source']}')" style="text-decoration: underline;">点击进入查看</a> | <a href="javascript:void(0)" onclick="export_logup_coin('{$value['use_coin']}','{$value['use_coin_source']}')" style="text-decoration: underline;">导出</a>
END;
                $rs[$key]['time'] = date('Y-m-d', $value['time']);
                $rs[$key]['get_num'] = $value['get_num'];//产出数
                if ($value['type'] == 4) {
                    $rs[$key]['get_item'] = $get_item;
                }
                $rs[$key]['get_role_num'] = $value['get_role_num'];//产出角色数
                $rs[$key]['use_num'] = $value['use_num'];//消耗数

                if ($value['type'] == 1 || $value['type'] == 3 || $value['type'] == 2) {
                    $rs[$key]['use_coin'] = $use_coin;
                }
                if ($value['type'] == 4) {
                    $rs[$key]['use_item'] = $use_item;
                }
                $rs[$key]['use_role_num'] = $value['use_role_num'];//消耗角色数
                $rs[$key]['inventory'] = $value['inventory'];//库存数量
                $rs[$key]['total_get'] = $value['total_get'];//总产出
                $rs[$key]['total_use'] = $value['total_use'];//总消耗
                $rs[$key]['stranded'] = (1 - sprintf('%.4f', $value['total_use'] / $value['total_get'])) * 100 . '%';
            }
            $rs[0]['time'] = '总计';
            return $rs;
        }

    }

    //活动参与率
    public function activity_data($conditions)
    {
        $rs = array();
        $num = 0;

        $result = $this->get_activity_data($conditions);

        list($rs, $num) = $result;

        foreach ($rs as $k => $v) {
            $rs[$k] = array_values($v);
        }

        $this->setPageData($rs, $num);
    }

    public function activity_export($conditions)
    {

        $rs = array();
        $result = array();
        unset($conditions['Extends']['LIMIT']);
        $rs = $this->get_activity_data($conditions);

        list($rs,$num) = $rs;

        $result[] = array(
            '日期', '总消耗元宝', '总消耗角色数', '充值元宝', '充值角色数', '库存元宝', '该活动消耗的元宝', '参与该活动的角色数', '消耗占比',  '消耗人数占比'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '活动参与率（' . date('Y年m月d日H时i分s秒') . '）');
    }

    /**
     * [get_activity_data description]
     * @param  [array] $conditions [description]
     * @return [array]             [description]
     */
    public function get_activity_data($conditions)
    {
        parent::__construct('activity');

        $rs = array();
        $num = 0;

        $fields = array('time', 'sum(total_consume_gold) as total_consume_gold', 'sum(total_consume_num) as total_consume_num', 'sum(charge_gold) as charge_gold', 'sum(charge_num) as charge_num', 'sum(single_consume_gold) as single_consume_gold', 'sum(single_consume_num) as single_consume_num', 'sum(inventory_gold) as inventory_gold');

        $conditions['Extends']['GROUP'] = 'time';

        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($result) {
            $num = $this->getCount();
            unset($conditions['Extends']['GROUP']);
            $total = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
            $result = array_merge($total, $result);

            foreach ($result as $k => $v) {

                $rs[$k]['time'] = date('Y-m-d', $v['time']);
                $rs[$k]['total_consume_gold'] = $v['total_consume_gold']; //总消耗元宝
                $rs[$k]['total_consume_num'] = $v['total_consume_num']; //总消耗人数
                $rs[$k]['charge_gold'] = $v['charge_gold']; //充值元宝
                $rs[$k]['charge_num'] = $v['charge_num']; //充值人数
                $rs[$k]['inventory_gold'] = $v['inventory_gold']; //库存元宝
                $rs[$k]['single_consume_gold'] = $v['single_consume_gold'];//单活动消耗的元宝数
                $rs[$k]['single_consume_num'] = $v['single_consume_num'];//单活动消耗的人数

                $rs[$k]['percentage'] = $v['total_consume_gold'] != 0 ? sprintf('%.2f', $v['single_consume_gold'] / $v['total_consume_gold'] * 100) . '%' : '0%'; //消耗占比

                //$rs[$k]['charge_consume_percentage'] = $v['inventory_gold'] != 0 ? sprintf('%.2f' , $v['single_consume_gold']/$v['inventory_gold'])* 100 .'%' : '0%'; //库存消耗占比

                $rs[$k]['consume_num_percentage'] = $v['total_consume_num'] != 0 ? sprintf('%.2f', $v['single_consume_num'] / $v['total_consume_num'] * 100) . '%' : '0%'; //消耗人数占比

            }
            $rs[0]['time'] = '总计';
        }

        return array($rs, $num);

    }

    /**
     * 创角数
     */

    public function create_data($conditions)
    {
        parent::__construct('role');
        $rs = array();
        $num = 0;

        $fields = array("FROM_UNIXTIME(time,'%Y-%m-%d') as time", 'sum(reg_num) as reg_num', 'sum(mac_num) as mac_num');

        $conditions['Extends']['GROUP'] = 'FROM_UNIXTIME(time, "%Y-%m-%d")';

        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $server = $conditions['WHERE']['server::IN'];
        $server = implode(',', $server);

        $package = $conditions['WHERE']['package::IN'];
        $package = implode(',', $package);

        if (!empty($result[0]['time'])) {
            $num = $this->getCount();
            foreach ($result as $k => $v) {
                $v['time'] = "<span class='data' data-date='" . $v['time'] . "' data-server='" . $server . "'   data-package='" . $package . "' style='cursor:pointer; border-bottom: 1px dashed blue'>" . $v['time'] . "</span>";
                $rs[$k] = array_values($v);
            }
        }

        $this->setPageData($rs, $num);

    }

    /**
     * [getDetail 获取创角数详情]
     * @param  [string] $date    [日期]
     * @param  [string] $server  [服务器]
     * @param  [string] $package [包号]
     * @return [array]          [description]
     */
    public function getDetail($date, $server, $package)
    {

        $server = explode(',', $server);

        $server = implode("','", $server);

        $package = explode(',', $package);

        $package = implode("','", $package);

        $where = '';
        $date && $where = " and FROM_UNIXTIME(time,'%Y-%m-%d') = '$date'";
        $server && $where .= " and server in ('$server')";
        $package && $where .= " and package in ('$package') ";

        $sql = "select time , sum(reg_num) as reg_num , sum(mac_num) as mac_num from stat_role where 1  $where group by time";

        $result = $this->query($sql);

        if ($result) {
            foreach ($result as $k => $v) {
                $k = date('H', $v['time']);
                $k = $k . ':00';
                $data[$k]['rolelist'] = $v['reg_num'];
                $data[$k]['accountlist'] = $v['mac_num'];
            }
        }
        return $data;
    }

    /**
     * 导出创角数
     */
    public function create_export($conditions)
    {
        parent::__construct('role');
        $rs = array();

        $fields = array("FROM_UNIXTIME(time,'%Y-%m-%d') as time", 'sum(reg_num) as reg_num', 'sum(mac_num) as mac_num');
        $conditons['Extends']['GROUP'] = "FROM_UNIXTIME(time,'%Y-%m-d')";

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $result = array();
        $result[] = array('日期' , '创角数','创角设备数');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '创角数（' . date('Y-m-d') . '）');

    }

    //等级留存率
    /* public function level_data($conditions){
         parent::__construct('level');
         $rs = array();$num = 0;
         $limit = $conditions['Extends']['LIMIT'];

         if(isset($conditions['WHERE']['server::IN'])) {
             parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
         } else {
             //如果没有选择服务器则找出最新的服务器
             $conditions['WHERE']['server::IN'] = (new ServerModel())->getLatestServer();
             $this->setNewPrefix('stat_');
         }

         //如果没有选择时间则默认为当天
         if (!isset($conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"])) {
             $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = date('Y-m-d');
         }

         $fields = array( 'server' , 'level' ,'level_count', 'role_count',
             'level_rate' , 'stay_num' , 'level_loss_num' , 'level_loss_rate', 'create_time');

         $conditions['Extends']['GROUP'] = 'level';
         $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
         $num = $this->getCount();

         if ($arr) {
             foreach ($arr as $k => $row) {
                 $rs[$k]['server'] = $row['server'];

                 $rs[$k]['level'] = $row['level'];

                 $rs[$k]['level_count'] = $row['level_count'];

                 $rs[$k]['role_count'] = $row['role_count'];

                 $rs[$k]['level_rate'] = $row['level_rate'];

                 $rs[$k]['stay_num'] = $row['stay_num'];

                 $rs[$k]['level_loss_num'] = $row['level_loss_num'];

                 $rs[$k]['level_loss_rate'] = $row['level_loss_rate'];

                 $rs[$k]['create_time'] = date('Y-m-d H:i:s', $row['create_time']);
             }
         }
         foreach($rs as $k=>$row){
             $rs[$k]=array_values($row);
         }

         $this->setPageData($rs , $num , $limit);
     }

     public function level_export($conditions){

         unset($conditions['Extends']['LIMIT']);
         parent::__construct('level');
         $rs = array();$num = 0;
         $limit = $conditions['Extends']['LIMIT'];

         if(isset($conditions['WHERE']['server::IN'])) {
             parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
         } else {
             //如果没有选择服务器则找出最新的服务器
             $conditions['WHERE']['server::IN'] = (new ServerModel())->getLatestServer();
             $this->setNewPrefix('stat_');
         }

         //如果没有选择时间则默认为当天
         if (!isset($conditions['WHERE']['create_time::='])) {
             $conditions['WHERE']['create_time::='] = strtotime(date('Ymd'));
         }

         $fields = array( 'server' , 'level' ,'level_count', 'role_count',
             'level_rate' , 'stay_num' , 'level_loss_num' , 'level_loss_rate', 'create_time');

         $conditions['Extends']['GROUP'] = 'level';
         $arr = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

         $result = array();
         $result[] = array('服务器ID','等级' , '分布人数' , '玩家总人数' ,'分布率', '滞留人数', '等级流失人数', '等级流失率', '统计时间');

         if ($arr) {
             foreach ($arr as $k => $row) {
                 $rs[$k]['server'] = $row['server'];

                 $rs[$k]['level'] = $row['level'];

                 $rs[$k]['level_count'] = $row['level_count'];

                 $rs[$k]['role_count'] = $row['role_count'];

                 $rs[$k]['level_rate'] = $row['level_rate'];

                 $rs[$k]['stay_num'] = $row['stay_num'];

                 $rs[$k]['level_loss_num'] = $row['level_loss_num'];

                 $rs[$k]['level_loss_rate'] = $row['level_loss_rate'];

                 $rs[$k]['create_time'] = date('Y-m-d H:i:s', $row['create_time']);
             }
         }

         foreach($rs as $row){
             $result[] = $this->formatFields($row);
         }
         Util::exportExcel($result , '等级留存（' . date('Y年m月d日H时i分s秒') . '）');
     }*/


}