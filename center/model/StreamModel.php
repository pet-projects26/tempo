<?php

class StreamModel extends Model{

    public function summary_data($conditions)
    {
        $rs = array();
        $num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
        $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
        $server_id = $conditions['WHERE']['server::IN'];

        //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
        if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '') {
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
        }

        $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
        $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

        unset($conditions['WHERE']['server::IN']);

        $rs = $rs1 = $rsArr = [];
        foreach ($server_id as $key => $value) {
            $rs = $this->call('Stream', 'getSummaryData', array('conditions' => $conditions), $value);
            if ($rs[$value][0]) {
                foreach ($rs[$value][0] as $value) {
                    $rsArr[] = $value;
                }
            }
        }
        $rsArr1[0] = $rsArr;
        $rsArr1[1] = count($rsArr);
        !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }

        $this->setPageData($rs, $num, $limit);
    }

    public function equip_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){

        	$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
        	$server_id = $conditions['WHERE']['server::IN'];
        	unset($conditions['WHERE']['server::IN']);
			$conditions['fields'] = array('part','level',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",'role_id');
        	$rs = $rsArr = [];
        	foreach ($server_id as $key=>$value) {
        		$rs = $this->call('Stream' , 'equip_data' , array('conditions' => $conditions) , $value);
        		if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
        	}
        	$rsArr1[0] = $rsArr;
        	$rsArr1[1] = count($rsArr);
        	 
        	!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
        	foreach($rs as $k => $row){
        		$rs[$k] = array_values($row);
        	}
        	 
        }
        $this->setPageData($rs , $num , $limit);
    }

    public function equip_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){

        	$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
        	$server_id = $conditions['WHERE']['server::IN'];
        	unset($conditions['WHERE']['server::IN']);
			$conditions['fields'] = array('part','level',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",'role_id');
        	$rs = $rsArr = [];
        	foreach ($server_id as $key=>$value) {
        		$rs = $this->call('Stream' , 'equip_data' , array('conditions' => $conditions) , $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
        	}
        	$rsArr1[0] = $rsArr;
        	$rsArr1[1] = count($rsArr);
        	 
        	!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
        	foreach($rs as $k => $row){
        		$rs[$k] = array_values($row);
        	}
        	 
        }
        $result = array();
        $result[] = array('部位' , '等级' , '时间' , '角色id');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportCsv($result , '装备强化流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    
    public function equip_replace_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		
    		$memache=Util::memcacheConn();
    		$item=$memache->get($server_id[0].'item');
    		
    		if($conditions['WHERE']['item_id']){
    			$item_id=$conditions['WHERE']['item_id'];
    			unset($conditions['WHERE']['item_id']);
    			foreach($item as $key=>$val){
    				$arr["$val"][]=$key;
    			}      
    			$i=implode("','",$arr["$item_id"]);
    			$conditions['WHERE']['item_id::IN']=array($i);
    		}
    		
    		$conditions['fields'] = array('type','item_id',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",'role_id');
    		$rs = $rs1 = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'equip_replace_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			  if($row['type']==0){
    					$rs1[$k]['type']='穿上装备';
    				}else{
    					$rs1[$k]['type']='脱下装备';
    				}
    				$item_id = $row['item_id'];
    				$rs1[$k]['item_id']=$item_id;
    				$rs1[$k]['item']= $item["$item_id"];
    				$rs1[$k]['create_time'] = $row['create_time'];
    				$rs1[$k]['role_id'] = $row['role_id'];
    		}
    		foreach($rs1 as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , $num , $limit);
    }
    
    public function equip_replace_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		
    		$memache=Util::memcacheConn();
    		$item=$memache->get($server_id[0].'item');
    		
    		if($conditions['WHERE']['item_id']){
    			$item_id=$conditions['WHERE']['item_id'];
    			unset($conditions['WHERE']['item_id']);
    			foreach($item as $key=>$val){
    				$arr["$val"][]=$key;
    			}      
    			$i=implode("','",$arr["$item_id"]);
    			$conditions['WHERE']['item_id::IN']=array($i);
    		}
    		
    		$conditions['fields'] = array('type','item_id',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",'role_id');
    		$rs = $rs1 = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'equip_replace_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			  if($row['type']==0){
    					$rs1[$k]['type']='穿上装备';
    				}else{
    					$rs1[$k]['type']='脱下装备';
    				}
    				$item_id = $row['item_id'];
    				$rs1[$k]['item_id']=$item_id;
    				$rs1[$k]['item']= $item["$item_id"];
    				$rs1[$k]['create_time'] = $row['create_time'];
    				$rs1[$k]['role_id'] = $row['role_id'];
    		}
    		foreach($rs1 as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$result = array();
    	$result[] = array('类型' , '类型id' , '装备' ,'时间' , '角色id');
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportCsv($result , '装备替换流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    
    public function prop_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);

    	if (!empty($conditions['WHERE'])) {
            if(!isset($conditions['WHERE']['server::IN'])){
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['item_id'] == '' && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            unset($conditions['WHERE']['server::IN']);

            $AllItem= $this->call('Gift' , 'getItem' , array() , $server_id[0]);//全部物品
            $AllItem = $AllItem[$server_id[0]];
            $itemType=CDict::$itemSource;
            if($conditions['WHERE']['item_id']){
                $item_id = $conditions['WHERE']['item_id'];
                foreach ($AllItem as $k=>$v) {
                    if ($item_id == $v) {
                        $conditions['WHERE']['item_id'] = (String)$k;
                        break;
                    }
                }
            }
            $rs = $rs1 = $rsArr = [];
            foreach ($server_id as $key=>$value) {
                $rs = $this->call('Stream' , 'getitemstream' , array('conditions' => $conditions) , $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);
            !(empty($rsArr1))&&list($rs,$num) = $rsArr1;
            foreach($rs as $k => $v){
                $source = $v['source'];
                $item_id = $v['item_id'];
                $rs1[$k]['source']=$itemType[$source];
                $rs1[$k]['item_id']=$item_id;
                if (!empty($AllItem["$item_id"])) {
                    $rs1[$k]['item'] = implode(',', $AllItem["$item_id"]);
                }  else {
                    $rs1[$k]['item'] = '';
                }

                if($v['type']==0){
                    $rs1[$k]['item_num']='-'.$v['item_num'];
                }else{
                    $rs1[$k]['item_num']='+'.$v['item_num'];
                }
                $rs1[$k]['create_time'] = $v['create_time'];
                $rs1[$k]['role_id'] = $v['role_id'];
            }
            foreach($rs1 as $k => $row){
                $rs[$k] = array_values($row);
            }
        }

    	$this->setPageData($rs , $num , $limit);
    }
    public function prop_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['item_id'] == '' && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            $AllItem = $this->call('Gift', 'getItem', array(), $server_id[0]);//全部物品
            $AllItem = $AllItem[$server_id[0]];
            $itemType = CDict::$itemSource;
            if ($conditions['WHERE']['item_id']) {
                $item_id = $conditions['WHERE']['item_id'];
                foreach ($AllItem as $k => $v) {
                    if ($item_id == $v) {
                        $conditions['WHERE']['item_id'] = (String)$k;
                        break;
                    }
                }
            }
            $rs = $rs1 = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'getitemstream', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);
            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $v) {
                $source = $v['source'];
                $item_id = $v['item_id'];
                $rs1[$k]['source'] = $itemType[$source];
                $rs1[$k]['item_id'] = $item_id;
                $rs1[$k]['item'] = $AllItem["$item_id"];
                if ($v['type'] == 0) {
                    $rs1[$k]['item_num'] = '-' . $v['item_num'];
                } else {
                    $rs1[$k]['item_num'] = '+' . $v['item_num'];
                }
                $rs1[$k]['create_time'] = $v['create_time'];
                $rs1[$k]['role_id'] = $v['role_id'];
            }
            foreach ($rs1 as $k => $row) {
                $rs[$k] = array_values($row);
            }

            $result = array();
            $result[] = array('类型', '道具id', '道具', '数量', '时间', '角色id');
            foreach ($rs as $row) {
                $result[] = $this->formatFields($row);
            }
            Util::exportCsv($result, '道具流水（' . date('Y年m月d日H时i分s秒') . '）');
        }
    }
    
    public function money_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['coin_source'] == '' && $conditions['WHERE']['money_type'] == '' && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            $rs = $rs1 = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'money_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);
            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            $itemType = CDict::$itemSource;
            $money = CDict::$money;
            foreach ($rs as $k => $v) {
                $rs1[$k]['source'] = $itemType[$v['coin_source']];
                $rs1[$k]['money_type'] = $money[$v['money_type']];
                if ($v['type'] == 0) {
                    $flag = '-';
                } else {
                    $flag = '+';
                }
                $rs1[$k]['count'] = $flag . $v['count'];
                $rs1[$k]['balance'] = $v['balance'];

                $rs1[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time'] / 1000);
                $rs1[$k]['role_id'] = $v['role_id'];
            }
            foreach ($rs1 as $k => $row) {
                $rs[$k] = array_values($row);
            }
        }
    	$this->setPageData($rs , $num , $limit);
    }
    public function money_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['coin_source'] == '' && $conditions['WHERE']['money_type'] == '' && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            $rs = $rs1 = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'money_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);
            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            $itemType = CDict::$itemSource;
            $money = CDict::$money;
            foreach ($rs as $k => $v) {
                $rs1[$k]['source'] = $itemType[$v['coin_source']];
                $rs1[$k]['money_type'] = $money[$v['money_type']];
                if ($v['type'] == 0) {
                    $flag = '-';
                } else {
                    $flag = '+';
                }
                $rs1[$k]['count'] = $flag . $v['count'];
                $rs1[$k]['balance'] = $v['balance'];
                $rs1[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time'] / 1000);
                $rs1[$k]['role_id'] = $v['role_id'];
            }
            foreach ($rs1 as $k => $row) {
                $rs[$k] = array_values($row);
            }

            $result = array();
            $result[] = array('类型', '金钱类型', '数量', '该货币类型余额', '时间', '角色id');
            foreach ($rs as $row) {
                $result[] = $this->formatFields($row);
            }
            Util::exportCsv($result, '货币流水（' . date('Y年m月d日H时i分s秒') . '）');
        }
    }
    
    public function level_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            unset($conditions['WHERE']['server::IN']);
            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {

                $rs = $this->call('Stream', 'level_data', array('conditions' => $conditions), $value);

                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }

        }
    	$this->setPageData($rs , $num , $limit);
    }
    public function level_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'level_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }

            $result = array();
            $result[] = array('等级', '时间', '角色id');
            foreach ($rs as $row) {
                $result[] = $this->formatFields($row);
            }
            Util::exportCsv($result, '等级流水（' . date('Y年m月d日H时i分s秒') . '）');
        }
    }
    public function login_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['start_time::>='] = isset($conditions['WHERE']['start_time::>=']) ? $conditions['WHERE']['start_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['start_time::<='] = isset($conditions['WHERE']['start_time::<=']) ? $conditions['WHERE']['start_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['start_time::>='] = $conditions['WHERE']['start_time::>='] * 1000;
            $conditions['WHERE']['start_time::<='] = $conditions['WHERE']['start_time::<='] * 1000;

            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'login_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }
        }

    	$this->setPageData($rs , $num , $limit);
    }
    
    public function login_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['start_time::>='] = isset($conditions['WHERE']['start_time::>=']) ? $conditions['WHERE']['start_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['start_time::<='] = isset($conditions['WHERE']['start_time::<=']) ? $conditions['WHERE']['start_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['start_time::>='] = $conditions['WHERE']['start_time::>='] * 1000;
            $conditions['WHERE']['start_time::<='] = $conditions['WHERE']['start_time::<='] * 1000;

            unset($conditions['WHERE']['server::IN']);
            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'login_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }

            $result = array();
            $result[] = array('等级', 'IP', '登录时间', '登出时间', '角色id', '在线时长');
            foreach ($rs as $row) {
                $result[] = $this->formatFields($row);
            }
            Util::exportCsv($result, '登录流水（' . date('Y年m月d日H时i分s秒') . '）');
        }
    }

    public function email_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '' && $conditions['WHERE']['type'] == '' && $conditions['WHERE']['email_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            unset($conditions['WHERE']['server::IN']);
            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {

                $rs = $this->call('Stream', 'email_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function email_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if (!empty($conditions['WHERE'])) {
            if (!isset($conditions['WHERE']['server::IN'])) {
                $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
            }

            $conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Y-m-d', time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);

            //如果时间范围大于一周, 将只显示一周都数据 防止数据量过大
            if ($conditions['WHERE']['create_time::<='] - $conditions['WHERE']['create_time::>='] < 604800 && count($server_id) > 1 && $conditions['WHERE']['role_id'] == '' && $conditions['WHERE']['type'] == '' && $conditions['WHERE']['email_id'] == '') {
                $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::>='] + 604800;
            }

            $conditions['WHERE']['create_time::>='] = $conditions['WHERE']['create_time::>='] * 1000;
            $conditions['WHERE']['create_time::<='] = $conditions['WHERE']['create_time::<='] * 1000;

            $rs = $rsArr = [];
            foreach ($server_id as $key => $value) {
                $rs = $this->call('Stream', 'email_data', array('conditions' => $conditions), $value);
                if ($rs[$value][0]) {
                    foreach ($rs[$value][0] as $value) {
                        $rsArr[] = $value;
                    }
                }
            }
            $rsArr1[0] = $rsArr;
            $rsArr1[1] = count($rsArr);

            !(empty($rsArr1)) && list($rs, $num) = $rsArr1;
            foreach ($rs as $k => $row) {
                $rs[$k] = array_values($row);
            }

            $result = array();
            $result[] = array('类型', '邮件id', '邮件名', '时间', '角色id');
            foreach ($rs as $row) {
                $result[] = $this->formatFields($row);
            }
            Util::exportCsv($result, '等级流水（' . date('Y年m月d日H时i分s秒') . '）');
        }
    }


    public function client_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    		$c = new ClientModel();
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$rs = $rsArr = [];
    		$fields = array('channel','package','model','network','mac','udid','memory',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",'role');
    		$rs = $c->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , count($rs) , $limit);
    }
    
    public function client_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    		$c = new ClientModel();
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$rs = $rsArr = [];
    		$fields = array('channel','package','model','network','mac','udid','memory',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",'role');
    		$rs = $c->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$result = array();
    	$result[] = array('渠道' , '包',  '机型','网络类型','mac','udid','memory','时间','角色id');
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportCsv($result , '客户端流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    public function task_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$rs = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'task_data' , array('conditions' => $conditions) , $value);
    			 
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    
    	}
    	$this->setPageData($rs , $num , $limit);
    }
    
    public function task_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){


    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$rs = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'task_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    		
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$result = array();
    	$result[] = array('任务ID' ,'任务类型' , '任务名称','状态' ,'时间','角色id');
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportCsv($result , '任务流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    
    public function ride_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$rs = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'ride_data' , array('conditions' => $conditions) , $value);
    			 
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    
    	}
    	$this->setPageData($rs , $num , $limit);
    }
    
    public function ride_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$rs = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'ride_data' , array('conditions' => $conditions) , $value);
    		
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    		
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$result = array();
    	$result[] = array('阶级' ,'等级' , '时间','角色id');
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportCsv($result , '坐骑流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    public function ce_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$rs = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'ce_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    
    	}
    	$this->setPageData($rs , $num , $limit);
    }
    
    public function ce_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$rs = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'ce_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		foreach($rs as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    
    	}
    	$result = array();
    	$result[] = array('战斗力','时间','角色id');
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportCsv($result , '战斗力流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    
    public function gem_data($conditions){

    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    	
    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    	
    		$rs = $rs1 = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'gem_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		if(!empty($rs)){
    			$equie = CDict::$equie;
    			$AllItem = $this->call('Gift' , 'getItem' , array() , $server_id[0]);//全部物品
    			$item = $AllItem[$server_id[0]];
    			foreach($rs as $k => $v){
    				
    				$rs1[$k]['equie'] = $equie[$v['part']] ;
    				$type = $v['type'];
    				switch ($type) {
    					case 0:
    						$type = '镶嵌' ;
    						break;
    					case 1:
    						$type = '卸下' ;
    						break;
    					case 2:
    						$type = '升级产出' ;
    						break;
    					case 3:
    						$type = '升级消耗' ;
    						break;
    					default:
    						$type = '未知途径' ;
    						break;
    				}
    				$item_id = trim($v['item_id']);
    				$rs1[$k]['gem'] =  $type . $item[$item_id];
    				$rs1[$k]['create_time'] =  $v['create_time'];
    				$rs1[$k]['role_id'] =  $v['role_id'];
    			}
    		}
    		foreach($rs1 as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$this->setPageData($rs , $num , $limit);
    }
    
    public function gem_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){


    		$conditions['WHERE']['create_time::>='] = isset($conditions['WHERE']['create_time::>='])? $conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d',time()));
            $conditions['WHERE']['create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date('Y-m-d', time())) + 86399;
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		 
    		$rs = $rs1 = $rsArr = [];
    		foreach ($server_id as $key=>$value) {
    			$rs = $this->call('Stream' , 'gem_data' , array('conditions' => $conditions) , $value);
    			foreach ($rs[$value][0] as $value) {
    				$rsArr[] = $value;
    			}
    		}
    		$rsArr1[0] = $rsArr;
    		$rsArr1[1] = count($rsArr);
    		!(empty($rsArr1))&&list($rs,$num) = $rsArr1;
    		if(!empty($rs)){
    			$equie = CDict::$equie;
    			$AllItem = $this->call('Gift' , 'getItem' , array() , $server_id[0]);//全部物品
    			$item = $AllItem[$server_id[0]];
    			foreach($rs as $k => $v){
    		
    				$rs1[$k]['equie'] = $equie[$v['part']] ;
    				$type = $v['type'];
    				switch ($type) {
    					case 0:
    						$type = '镶嵌' ;
    						break;
    					case 1:
    						$type = '卸下' ;
    						break;
    					case 2:
    						$type = '升级产出' ;
    						break;
    					case 3:
    						$type = '升级消耗' ;
    						break;
    					default:
    						$type = '未知途径' ;
    						break;
    				}
    				$item_id = trim($v['item_id']);
    				$rs1[$k]['gem'] =  $type . $item[$item_id];
    				$rs1[$k]['create_time'] =  $v['create_time'];
    				$rs1[$k]['role_id'] =  $v['role_id'];
    			}
    		}
    		foreach($rs1 as $k => $row){
    			$rs[$k] = array_values($row);
    		}
    	}
    	$result = array();
    	$result[] = array('部位','宝石','时间','角色id');
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    	Util::exportCsv($result , '宝石流水（' . date('Y年m月d日H时i分s秒') . '）');
    }
    
}