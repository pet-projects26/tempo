<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/4/29
 * Time: 16:34
 */

class SummaryModel extends Model
{

    public function __construct()
    {
        parent::__construct('summary');
        $this->alias = 's';
    }

    //角色数据汇总
    public function summary_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_data($conditions);

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }

    public function summary_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_summary_data($conditions);

        $result = array();
        $result[] = array(
            '日期', '新增用户', '老用户', '活跃用户', '设备激活数', '平均在线', '最高在线', '付费人数', '总充值金额', '付费ARPU', '活跃付费率', '新用户付费人数', '新用户付费金额', '新用户ARPU', '新用户付费率', '老用户付费人数', '老用户付费金额', '老用户ARPU', '老用户付费率'
        );
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '数据总览（' . date('Y年m月d日H时i分s秒') . '）');

    }

    /**
     * [get_summary_role_data 数据汇总]
     * @param  [array] $conditions [搜索条件]
     * @return [array]             [返回结果]
     */
    public function get_summary_data($conditions)
    {
        if ($conditions['WHERE']['server::IN']) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['s.server::IN'] = $server;
        }

        if ($conditions['WHERE']['package::IN']) {
            $server = $conditions['WHERE']['package::IN'];
            unset($conditions['WHERE']['package::IN']);
            $conditions['WHERE']['s.package::IN'] = $server;
        }

        //默认最近七天
        $conditions['WHERE']['s.time::>='] = isset($conditions['WHERE']['s.time::>=']) ? $conditions['WHERE']['s.time::>='] : strtotime(date("Y-m-d", time())) - 86400 * 7;
        $conditions['WHERE']['s.time::<='] = isset($conditions['WHERE']['s.time::<=']) ? $conditions['WHERE']['s.time::<='] : strtotime(date("Y-m-d", time())) + 86400 - 1;

        $this->joinTable = array(
            'r' => array('name' => 'server', 'type' => 'LEFT', 'on' => 'r.server_id = s.server')
        );

        $fields = array('s.time', 'sum(s.active_charge_money) as active_charge_money', 'sum(s.active_charge_num) as active_charge_num', 'sum(s.acc_reg_num) as acc_reg_num', 'sum(s.login_num) as login_num', 'sum(s.active_num) as active_num', 'sum(s.old_num) as old_num', 'sum(s.charge_num) as charge_num', 'sum(s.charge_money) as charge_money', 'sum(s.new_charge_num) as new_charge_num', 'sum(s.new_charge_money) as new_charge_money', 'sum(s.old_charge_num) as old_charge_num', 'sum(s.old_charge_money) as old_charge_money', 'max(s.max_online_num) as max_online_num', 'avg(s.avg_online_num) as avg_online_num', 'sum(s.new_num) as new_num');

        $conditions['Extends']['GROUP'] = "s.time";

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['Extends']['GROUP']);

        $all = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = array_merge($all, $rs);

        $result = array();

        if ($rs[0]['time']) {
            foreach ($rs as $k => $v) {

                $result[$k]['time'] = date('Y-m-d', $v['time']);//时间
                $result[$k]['new_num'] = $v['new_num'];//新增用户数
                $result[$k]['old_num'] = $v['old_num'];//老用户
                $result[$k]['active_num'] = $v['active_num']; //活跃用户数
                $result[$k]['device_num'] = 0; //设备激活数
                $result[$k]['avg_online_num'] = sprintf('%.2f', $v['avg_online_num']);//平均在线
                $result[$k]['max_online_num'] = $v['max_online_num'];//最高在线
                $result[$k]['charge_num'] = $v['charge_num'];//充值人数
                $result[$k]['charge_money'] = $v['charge_money'];//充值金额

                //付费arpu
                $result[$k]['AP'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['charge_num']) : 0;
                //活跃付费率
                $result[$k]['active_rate'] = $v['active_num'] != 0 ? sprintf('%.2f', $v['charge_num'] / $v['active_num'] * 100) . '%' : '0%';//老付费率

                //新用户付费
                $result[$k]['new_charge_num'] = $v['new_charge_num'];//新充值人数
                $result[$k]['new_charge_money'] = $v['new_charge_money'];//新充值金额
                $result[$k]['new_AP'] = $v['new_charge_num'] != 0 ? sprintf('%.2f', $v['new_charge_money'] / $v['new_charge_num']) : 0;//新AP
                $result[$k]['new_rate'] = $v['new_num'] != 0 ? sprintf('%.2f', $v['new_charge_num'] / $v['new_num'] * 100) . '%' : '0%';//新增付费率

                //老用户付费
                $result[$k]['old_charge_num'] = $v['old_charge_num'];//老充值人数
                $result[$k]['old_charge_money'] = $v['old_charge_money'];//老充值金额
                $result[$k]['old_AP'] = $v['old_charge_num'] != 0 ? sprintf('%.2f', $v['old_charge_money'] / $v['old_charge_num']) : 0;//老AP
                $result[$k]['old_rate'] = $v['old_num'] != 0 ? sprintf('%.2f', $v['old_charge_num'] / $v['old_num'] * 100) . '%' : '0%';//老付费率

            }

            $result[0]['time'] = '总计';
        }

        return $result;
    }


    //月收入统计
    public function monthly_income_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_monthly_income_data($conditions);

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }

    public function monthly_income_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_monthly_income_data($conditions);

        $result = array();
        $result[] = array(
            '时间', '渠道', '服', '充值金额');
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '月收入统计（' . date('Y年m月d日H时i分s秒') . '）');

    }


    /**
     * [get_monthly_income_data 月收入统计]
     * @param  [array] $conditions [搜索条件]
     * @return [array]             [返回结果]
     */
    public function get_monthly_income_data($conditions)
    {

        if ($conditions['WHERE']['server::IN']) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['s.server::IN'] = $server;
        }

        if ($conditions['WHERE']['package::IN']) {
            $server = $conditions['WHERE']['package::IN'];
            $key = array_search('0', $server);
            if ($key) {
                unset($server[$key]);
            }
            unset($conditions['WHERE']['package::IN']);
            $conditions['WHERE']['s.package::IN'] = $server;
        }

        $now_month_first_date = date('Y-m-01');
        $now_month_last_date = date('Y-m-d', strtotime(date('Y-m-1', strtotime('next month')) . '-1 day'));

        //默认最近一个月
        $conditions['WHERE']['s.time::>='] = isset($conditions['WHERE']['s.time::>=']) ? $conditions['WHERE']['s.time::>='] : strtotime($now_month_first_date);
        $conditions['WHERE']['s.time::<='] = isset($conditions['WHERE']['s.time::<=']) ? $conditions['WHERE']['s.time::<='] : strtotime($now_month_last_date) + 86400 - 1;

        $time = $conditions['WHERE']['s.time::>='];

        unset($conditions['WHERE']['time::<=']);
        unset($conditions['WHERE']['time::<=']);

        $this->joinTable = array(
            'r' => array('name' => 'server', 'type' => 'LEFT', 'on' => 'r.server_id = s.server'),
            'p' => array('name' => 'package', 'type' => 'LEFT', 'on' => 'p.package_id = s.package'),
            'c' => array('name' => 'channel', 'type' => 'LEFT', 'on' => 'c.channel_id = p.channel_id')
        );

        $fields = array('p.name as package_name', 'sum(s.charge_money) as charge_money', 'c.name as channel_name');

        if ($conditions['WHERE']['s.package::IN']) {
            $conditions['Extends']['GROUP'] = "s.package";
        }

        if ($conditions['WHERE']['s.server::IN']) {
            $conditions['Extends']['GROUP'] = "s.server";
            $fields = array('p.name as package_name', 'r.name as server_name', 'sum(s.charge_money) as charge_money', 'c.name as channel_name');
        }

        if ($conditions['WHERE']['s.package::IN'] && $conditions['WHERE']['s.server::IN']) {
            $conditions['Extends']['GROUP'] = ["s.package", "s.server"];
        }

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        //unset($conditions['Extends']['GROUP']);

        // $all = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

        //$rs = array_merge($all, $rs);

        $result = array();

        if ($rs) {
            foreach ($rs as $k => $v) {
                $result[$k]['time'] = date('Y-m', $time);//时间
                $result[$k]['channel_package'] = $v['channel_name'] . '-' . $v['package_name'];
                $result[$k]['server_name'] = $v['server_name'] ? $v['server_name'] : 'all';
                $result[$k]['charge_money'] = $v['charge_money'];
            }

            //$result[0]['time'] = '总计';
        }

        return $result;
    }

    //单服汇总
    public function single_summary_data($conditions)
    {

        $rs = array();
        $num = 0;

        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_single_summary_data($conditions);

        foreach ($rs as $k => $row) {
            $rs[$k] = array_values($row);
        }
        $num = count($rs);
        $this->setPageData($rs, $num, $limit);
    }

    public function single_summary_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);

        $rs = $this->get_single_summary_data($conditions);

        $result = array();
        $result[] = array(
            '服', '总创角', '总充值人数', '总付费金额', '总付费率', '总付费ARPU', '上月充值金额', '本月充值金额', '昨天充值金额', '今天充值金额');
        foreach ($rs as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, '单服汇总（' . date('Y年m月d日H时i分s秒') . '）');

    }

    /**
     * [get_monthly_income_data 单服汇总]
     * @param  [array] $conditions [搜索条件]
     * @return [array]             [返回结果]
     */
    public function get_single_summary_data($conditions)
    {

        if ($conditions['WHERE']['server::IN']) {
            parent::eliminateRew($conditions['WHERE']['server::IN']);//剔除提审服如*_9998或者*_9999
            $server = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $conditions['WHERE']['s.server::IN'] = $server;
        }

        if ($conditions['WHERE']['package::IN']) {
            $server = $conditions['WHERE']['package::IN'];
            unset($conditions['WHERE']['package::IN']);
            $conditions['WHERE']['s.package::IN'] = $server;
        }

        $this->joinTable = array(
            'r' => array('name' => 'server', 'type' => 'LEFT', 'on' => 'r.server_id = s.server')
        );

        $fields = array('r.name as server_name', 's.server as server', 'sum(s.reg_num) as reg_num', 'sum(s.charge_num) as charge_num', 'sum(s.charge_money) as charge_money');

        $conditions['Extends']['GROUP'] = "s.server";

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $result = array();

        if ($rs) {
            foreach ($rs as $k => $v) {
                $result[$k]['server_name'] = $v['server_name'];
                $result[$k]['reg_num'] = $v['reg_num'];

                //去单服获取充值人数 截止上次summary统计的人数
               //$last_hour_end_time = strtotime(date('Y-m-d H:00:00', time())) - 1;
                $last_hour_end_time = time();

                $res = $this->call('Charge', 'getPayingUserNum', ['date' => date('Y-m-d H:i:s', $last_hour_end_time)], $v['server']);

                $v['charge_num'] = $res[$v['server']] ? $res[$v['server']] : 0;

                $result[$k]['charge_num'] = $v['charge_num'];
                $result[$k]['charge_money'] = $v['charge_money'];

                //付费率
                $result[$k]['rate'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['charge_num'] / $v['reg_num'] * 100) . '%' : '0%';
                //付费arpu
                $result[$k]['AP'] = $v['charge_num'] != 0 ? sprintf('%.2f', $v['charge_money'] / $v['charge_num']) : 0;

                //上月充值
                $last_month_first_time = strtotime(date('Y-m-01', strtotime('-1 month')));
                $last_month_end_time = strtotime(date('Y-m-01')) - 1;
                $result[$k]['last_month_charge_money'] = $this->sumChargeMoney($v['server'], $last_month_first_time, $last_month_end_time);

                //本月充值
                $now_month_first_time = strtotime(date('Y-m-01'));
                $now_month_end_time = strtotime(date('Y-m-01', strtotime('next month')) . '-1 day');
                $result[$k]['this_month_charge_money'] = $this->sumChargeMoney($v['server'], $now_month_first_time, $now_month_end_time);

                //昨天充值
                $last_day_first_time = strtotime(date('Y-m-d', strtotime('-1 day')));
                $last_day_end_time = $last_day_first_time + 86399;
                $result[$k]['last_day_charge_money'] = $this->sumChargeMoney($v['server'], $last_day_first_time, $last_day_end_time);

                //今天充值 得去单服现查 - -
                $today_date = date('Y-m-d');
                $today_res = $this->call('Charge', 'getChargeDataByDate', ['date' => $today_date], $v['server']);

                $today_res = $today_res[$v['server']];
                $result[$k]['today_charge_money'] = $today_res['money'] ? $today_res['money'] : 0;
            }
        }

        return $result;
    }

    public function sumChargeMoney($server, $start_time, $end_time)
    {
        $conditions = [];
        $conditions['WHERE'] = [];
        $conditions['WHERE']['server'] = $server;
        $conditions['WHERE']['time::>='] = $start_time;
        $conditions['WHERE']['time::<='] = $end_time;

        $rs = $this->getRow('sum(charge_money) as charge_money', $conditions['WHERE']);

        return $rs['charge_money'] ? $rs['charge_money'] : 0;
    }

}