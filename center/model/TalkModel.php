<?php


class TalkModel extends Model
{

    public function __construct()
    {
        //(new MergeSqlUtil('talk'))->createNewMergeTable();
        parent::__construct('talk');
        $this->alias = 't';
    }

    public function getPackageListTalkData($packageIdList, $start_time, $end_time)
    {
        $tableName = (new SplitSqlUtil($this->tableName))->returnDateTableName([$start_time, $end_time]);

        if (!$tableName) {
            return [];
        }

        $old_tableName = $this->tableName;

        if ($tableName != $this->tableName) {
            parent::__construct($tableName);
        }

        $fields = array('t.account as platform_uid', 't.role_id as roleid', 't.role_name as rolename', 't.role_level as role_level', 't.ip as user_ip', 't.content as content', 't.type as chat_type', 't.package as package', 't.psId as psId');

        $conditions['WHERE']['t.package::IN'] = $packageIdList;
        $conditions['WHERE']['t.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['t.create_time::<='] = $end_time * 1000;
        $rs = $this->getRows($fields, $conditions['WHERE']);

        parent::__construct($old_tableName);

        return $rs;
    }
}