<?php
class TaskModel extends Model{

    public function __construct(){
        parent::__construct('task');
        $this->alias = 't';
    }
    /**
     * 支持多服汇总
     * @mark spId 兼容合服
     */
    public function task_loss_data($conditions){
    	$rs = array();$num = 0;
    	$limit = $conditions['Extends']['LIMIT'];
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$conditions['WHERE']['psId::IN'] = array_values(Helper::psIdByServerid($server_id));
    		$serverCount = 	count($server_id);
    		if ($serverCount > 1) {
    			$rsArr = [];
    			foreach ($server_id as $key=>$value) {
    				$rs = $this->call('Task' , 'getAllSerTaskLossData' , array('conditions' => $conditions) , $value);
    				foreach ($rs as $k=>$v) {
    					foreach ($v[0] as $kk=>$vv) {
    						$rsArr[] = $vv;
    					}
    				}
    			}
    
    			foreach($rsArr as $k => $v){
    				$rsArr1[$v['id']]['id'] = $v['id'];
    				$rsArr1[$v['id']]['type'] = $v['type'];
    				$rsArr1[$v['id']]['name'] = $v['name'];
    				$rsArr1[$v['id']]['role_num']  += $v['role_num'];
    				$rsArr1[$v['id']]['get']  += $v['get'];
    				$rsArr1[$v['id']]['done']  += $v['done'];
    			}
    
    			foreach ($rsArr1 as $k=>$v) {
    				if($v['role_num'] != 0){
    					$rsArr1[$k]['role_done'] = sprintf("%.2f" ,$v['done']/$v['role_num'] * 100) . '%';//角色完成率
    				}else{
    					$rsArr1[$k]['role_done'] = 0;
    				}
    				if($v['get'] !=0){
    					$rsArr1[$k]['task_done'] = sprintf("%.2f" ,$v['done']/$v['get'] * 100) . '%';//任务通过率
    				}else{
    					$rsArr1[$k]['task_done'] = 0;
    				}
    				if($v['role_num'] !=0){
    					$rsArr1[$k]['task_get'] = sprintf("%.4f" ,$v['get']/$v['role_num'] ) ;//任务接受率
    				}else{
    					$rsArr1[$k]['task_get'] = 0;
    				}
    
    				$rsArr1[$k]['loss'] = (sprintf("%.2f" ,(1-$rsArr1[$k]['task_get'])* 100)).'%';
    				$rsArr1[$k]['task_get'] = $rsArr1[$k]['task_get']*100 . '%';//任务接受率
    			}
    
    			 
    			$rs = array();
    			foreach($rsArr1 as $k=>$v){
    				$rs[] = array_values($v);
    			}
    			$num = count($rs);
    		}else {
    			$rs = $this->call('Task' , 'getTaskLossData' , array('conditions' => $conditions) , $server_id);
    
    			$row = $rs[array_shift($server_id)];
    			$rs = array();
    			foreach($row[0] as $k=>$v){
    				$rs[] = array_values($v);
    			}
    			$num = count($rs);
    		}
    	}
    	$this->setPageData($rs , $num , $limit);
    }
    
    public function task_loss_export($conditions){
    	$rs = array();
    	unset($conditions['Extends']['LIMIT']);
    	if(isset($conditions['WHERE']['server::IN'])){
    		$server_id = $conditions['WHERE']['server::IN'];
    		unset($conditions['WHERE']['server::IN']);
    		$serverCount = 	count($server_id);
    		if ($serverCount > 1) {
    			$rsArr = [];
    			foreach ($server_id as $key=>$value) {
    				$rs = $this->call('Task' , 'getAllSerTaskLossData' , array('conditions' => $conditions) , $value);
    				foreach ($rs as $k=>$v) {
    					foreach ($v[0] as $kk=>$vv) {
    						$rsArr[] = $vv;
    					}
    				}
    			}
    
    			foreach($rsArr as $k => $v){
    				$rsArr1[$v['id']]['id'] = $v['id'];
    				$rsArr1[$v['id']]['type'] = $v['type'];
    				$rsArr1[$v['id']]['name'] = $v['name'];
    				$rsArr1[$v['id']]['role_num']  += $v['role_num'];
    				$rsArr1[$v['id']]['get']  += $v['get'];
    				$rsArr1[$v['id']]['done']  += $v['done'];
    			}
    
    			foreach ($rsArr1 as $k=>$v) {
    				if($v['role_num'] != 0){
    					$rsArr1[$k]['role_done'] = sprintf("%.2f" ,$v['done']/$v['role_num'] * 100) . '%';//角色完成率
    				}else{
    					$rsArr1[$k]['role_done'] = 0;
    				}
    				if($v['get'] !=0){
    					$rsArr1[$k]['task_done'] = sprintf("%.2f" ,$v['done']/$v['get'] * 100) . '%';//任务通过率
    				}else{
    					$rsArr1[$k]['task_done'] = 0;
    				}
    				if($v['role_num'] !=0){
    					$rsArr1[$k]['task_get'] = sprintf("%.4f" ,$v['get']/$v['role_num'] ) ;//任务接受率
    				}else{
    					$rsArr1[$k]['task_get'] = 0;
    				}
    
    				$rsArr1[$k]['loss'] = (sprintf("%.2f" ,(1-$rsArr1[$k]['task_get'])* 100)).'%';
    				$rsArr1[$k]['task_get'] = $rsArr1[$k]['task_get']*100 . '%';//任务接受率
    			}
    			 
    
    			$rs = array();
    			$rs = $rsArr1;
    		}else {
    			$rs = $this->call('Task' , 'getTaskLossData' , array('conditions' => $conditions) , $server_id);
    			$rs = $this->callDataMerge($rs);
    			$rs = $rs[0];
    		}
    
    	}
    	$result = array();
    	$result[] = array(
    			'任务ID', '任务类型' , '任务名称' ,
    			'角色数','接受任务','完成任务',
    			'角色完成率','任务通过率','任务接受率','流失率'
    	);
    	foreach($rs as $row){
    		$result[] = $this->formatFields($row);
    	}
    
    	Util::exportExcel($result , '任务流失（' . date('Y年m月d日H时i分s秒') . '）');
    }
}