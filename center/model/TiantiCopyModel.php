<?php

class TiantiCopyModel extends Model
{

    public function __construct()
    {
        parent::__construct('tianti_copy');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'count', 'add_num_role', 'add_num_sum');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = $row['join_num'] ? @sprintf("%.2f", $row['join_num'] / $row['active_num'] * 100) . '%' : '0%';
                //人均参与场数
                $data[$k][5] = $row['join_num'] ? @round($row['count'] / $row['join_num'], 2) : 0.00;

                //购买次数人数
                $data[$k][6] = $row['add_num_role'];
                //购买参与度
                $data[$k][7] = $row['add_num_role'] ? @sprintf("%.2f", $row['add_num_role'] / $row['join_num'] * 100) . '%' : '0%';
                //人均购买次数
                $data[$k][8] = $row['add_num_sum'] ? @round($row['add_num_sum'] / $row['add_num_role'], 2) : 0.00;
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'join_num', 'count', 'add_num_role', 'add_num_sum');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];
                $data[$k][3] = $row['join_num'];
                //参与度
                $data[$k][4] = $row['join_num'] ? @sprintf("%.2f", $row['join_num'] / $row['active_num'] * 100) . '%' : '0%';
                //人均参与场数
                $data[$k][5] = $row['join_num'] ? @sprintf("%.2f", $row['count'] / $row['join_num'] * 100) . '%' : '0%';

                //购买次数人数
                $data[$k][6] = $row['add_num_role'];
                //购买参与度
                $data[$k][7] = $row['add_num_role'] ? @sprintf("%.2f", $row['add_num_role'] / $row['join_num'] * 100) . '%' : '0%';
                //人均购买次数
                $data[$k][8] = $row['add_num_sum'] ? @round($row['add_num_sum'] / $row['add_num_role'], 2) : 0.00;
            }
        }
        $result = array();

        $result[] = array('日期', '服务器', '活跃人数', '参与人数', '参与度', '人均参与场数', '购买次数人数', '购买参与度', '人均购买次数');
        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '天梯斗法（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}