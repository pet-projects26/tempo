<?php

class TotalModel extends Model{
    public function __construct(){
        parent::__construct('order');
        $this->alias = 'o';
    }
	public function role_fortune_data($conditions){
		$rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
			$conditions['Extends']['GROUP']='role_id';
            $rs = $this->call('Order' , 'getOrder' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
			foreach($rs as $k=>$v){
				$rs[$k]=array_values($v);
			}
        }
        $this->setPageData($rs , $num , $limit);
	}
	public function role_fortune_export($conditions){
		$rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'][0];
            unset($conditions['WHERE']['server::IN']);
			$conditions['Extends']['GROUP']='role_id';
            $rs = $this->call('Order' , 'getOrder' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
		$result = array();
        $result[] = array( '范围' , '人数' , '金额' , '比例' , '平均金额');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '角色财富统计（' . date('Y年m月d日H时i分s秒') . '）');	
	}
	public function account_fortune_data($conditions){
		$rss = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
			$conditions['Extends']['GROUP']='account';
            $rs = $this->call('Order' , 'getOrder' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
			foreach($rs as $k=>$v){
				$rss[]=array_values($v);
			}
        }
        $this->setPageData($rss , $num , $limit);
	}
	public function account_fortune_export($conditions){
		$rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
			$conditions['Extends']['GROUP']='account';
            $rs = $this->call('Order' , 'getOrder' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
		$result = array();
        $result[] = array( '范围' , '人数' , '金额' , '比例' , '平均金额');
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '帐号财富统计（' . date('Y年m月d日H时i分s秒') . '）');	
	}
}