<?php
class TotaldailyModel extends Model{
    public function __construct(){
        parent::__construct('total_daily');
        $this->alias = 'td';
    }

    public function record_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Totaldaily' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function record_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Totaldaily' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array(
            '日期' , '注册数' , '创角数' , '登录数' , '活跃数' , '老玩家数' , '充值人数' ,
            '充值金额' , '登录付费率' , '总AP' , '新充值人数' , '新充值金额' , '新增付费率' ,
            '新充值AP' , '老充值人数' , '老充值金额' , '老付费率' , '老充值AP' , '登录AP' ,
            '最高在线数' , '平均在线数' , '次留' , '3留' , '4留' , '5留' , '6留' , '7留' , '15留' , '30留'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '数据汇总（' . date('Y年m月d日H时i分s秒') . '）');
    }

    public function account_record_data($conditions){
        $rs = array();$num = 0;
        $limit = $conditions['Extends']['LIMIT'];
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Acctotaldaily' , 'getAccRecordData' , array('conditions' => $conditions) , $server_id);
            list($rs , $num) = $this->callDataMerge($rs);
            foreach($rs as $k => $row){
                $rs[$k] = array_values($row);
            }
        }
        $this->setPageData($rs , $num , $limit);
    }
    public function account_record_export($conditions){
        $rs = array();
        unset($conditions['Extends']['LIMIT']);
        if(isset($conditions['WHERE']['server::IN'])){
            $server_id = $conditions['WHERE']['server::IN'];
            unset($conditions['WHERE']['server::IN']);
            $rs = $this->call('Acctotaldaily' , 'getAccRecordData' , array('conditions' => $conditions) , $server_id);
            $rs = $this->callDataMerge($rs);
            $rs = $rs[0];
        }
        $result = array();
        $result[] = array(
            '日期' , '注册数' , '登录数' , '老玩家数' , '充值人数' , '充值金额' , '登录付费率' , '总AP' , '新充值人数' ,
            '新充值金额' , '新增付费率' , '新充值AP' , '老充值人数' , '老充值金额' , '老付费率' , '老充值AP' , '注册AP'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '账号付费（' . date('Y年m月d日H时i分s秒') . '）');
    }
}