<?php

class VipModel extends Model{

    public function __construct(){
        parent::__construct('vip');
        $this->alias = 'a';
    }

    /*
        public function record_data($conditions){
            $rs = array();$num = 0;
            $limit = $conditions['Extends']['LIMIT'];
            unset($conditions['Extends']['LIMIT']);
            if(isset($conditions['WHERE']['server::IN'])){
                $server_id = $conditions['WHERE']['server::IN'];
                unset($conditions['WHERE']['server::IN']);
                $rs = $this->call('Vip' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
                list($rs , $num) = $this->callDataMerge($rs);
            }
            $this->setPageData($rs , $num , $limit);
        }
        public function record_export($conditions){
            $rs = array();
            unset($conditions['Extends']['LIMIT']);
            if(isset($conditions['WHERE']['server::IN'])){
                $server_id = $conditions['WHERE']['server::IN'];
                unset($conditions['WHERE']['server::IN']);
                $rs = $this->call('Vip' , 'getRecordData' , array('conditions' => $conditions) , $server_id);
                $rs = $this->callDataMerge($rs);
                $rs = $rs[0];
            }
            $result = array();
            $result[] = array('VIP等级' , '角色名' ,  '领取时间' , '过期时间');
            foreach($rs as $row){
                $result[] = $this->formatFields($row);
            }
            Util::exportExcel($result , 'VIP信息（' . date('Y年m月d日H时i分s秒') . '）');
        }

        public function total_data($conditions){
            $rs = array();$num = 0;
            $limit = $conditions['Extends']['LIMIT'];
            unset($conditions['Extends']['LIMIT']);
            if(isset($conditions['WHERE']['server::IN'])){
                $server_id = $conditions['WHERE']['server::IN'];
                unset($conditions['WHERE']['server::IN']);
                $rs = $this->call('Vip', 'getTotaData', array('conditions' => $conditions), $server_id);
                list($rs , $num) = $this->callDataMerge($rs);
            }
            $this->setPageData($rs , $num , $limit);
        }
        public function total_export($conditions){
            $rs = array();
            unset($conditions['Extends']['LIMIT']);
            if(isset($conditions['WHERE']['server::IN'])){
                $server_id = $conditions['WHERE']['server::IN'];
                unset($conditions['WHERE']['server::IN']);
                $rs = $this->call('Vip' , 'getTotaData' , array('conditions' => $conditions) , $server_id);
                $rs = $this->callDataMerge($rs);
                $rs = $rs[0];
            }
            $result = array();
            $result[] = array('VIP等级' , 'VIP人数' , '占总VIP人数比例' , '占总人数比例');
            foreach($rs as $row){
                $result[] = $this->formatFields($row);
            }
            Util::exportExcel($result , 'VIP统计（' . date('Y年m月d日H时i分s秒') . '）');
        }
    */
    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "paying_user_num", 'vip_msg');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['paying_user_num'];

                $vip_msg = json_decode($row['vip_msg']);

                $n = 3;

                foreach ($vip_msg as $i => $j) {
                    $data[$k][$n] = $j;
                    $n++;
                }
            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {
        $rs = array();
        unset($conditions['Extends']['LIMIT']);


        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "paying_user_num", 'vip_msg');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['paying_user_num'];

                $vip_msg = json_decode($row['vip_msg']);

                $n = 3;

                foreach ($vip_msg as $i => $j) {
                    $data[$k][$n] = $j;
                    $n++;
                }
            }
        }


        $result = array();
        $result[] = array('日期', '服务器', '付费用户数');

        $result = array_merge($result[0], CDict::$vip);

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result, 'VIP分布（' . date('Y年m月d日H时i分s秒') . '）');
    }


    public function getRecordData($date, $server)
    {
        $conditions = [];

        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $rs = $this->getRow('*', $conditions['WHERE']);

        return $rs;
    }
}