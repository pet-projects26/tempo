<?php
class VipnoticeModel extends Model{

    public $type;

    public function __construct(){
        parent::__construct('vipnotice');
        $this->alias = 'vn';
        $this->type  = array('1'=>'邮件推送','2'=>'');
    }

    /**
     * 获取VIP公告列表
     * @param array $conditions
    */
    public function record_data($conditions){
        $fields = array('vn.id','cg.name','vn.server','title','content','status','back_massage','manage','vn.create_time');
        isset($conditions['WHERE']['group_id']) && $conditions['WHERE']['vn.group_id'] = $conditions['WHERE']['group_id'];
        isset($conditions['WHERE']['create_time']) && $conditions['WHERE']['vn.create_time'] = $conditions['WHERE']['create_time'];
        unset($conditions['WHERE']['create_time']);unset($conditions['WHERE']['group_id']);
        $this->joinTable = array(
            'cg'=>array('name'=>'channel_group','type'=>'left','on'=>'vn.group_id = cg.id')
        );
        $num = 0;
        $row = $this->getRows($fields,$conditions['WHERE'],$conditions['Extends']);
        $num = $this->getCount();
        //$serverInfo = (new ServerModel())->getServerByServerAndChannelGroup(array(),array());
        $pageData = [];
        foreach($row as $msg){
            $temp = [];
            $temp[] = $msg['id'];
            $temp[] = $msg['name'];
            //$temp[] = self::tips($msg['server'],$serverInfo);
            $temp[] = join(',',json_decode($msg['title'],true));
            $temp[] = '<input type="button" class="gbutton view-btn" value="查看" onclick="view('.$msg['id'].')">';
            /*$temp[] = '<input type="button" class="gbutton status-btn" value="同意" onclick="agreed('.$msg['id'].',1)">
             | <input type="button" class="gbutton status-btn" value="拒绝" onclick="agreed('.$msg['id'].',2)">';*/
            $temp[] = '<input type="button" class="gbutton send" value="发送" onclick="send('.$msg['id'].')">';
            //$temp[] = $msg['massage'];
            if($msg['status'] == 1){
                #code for not send
                $temp[] = '<span style="color: #33ca5a;font-weight: 600;font-size: 14px">保存成功</span>';
            }else if($msg['status'] == 2){
                $temp[] = '<span style="color: #33ca5a;font-weight: 600;font-size: 14px">保存成功</span>';
            }else if($msg['status'] == 3){
                $temp[] = '<span style="color: #5142ff;font-weight: 600;font-size: 14px">发布成功</span>';
            }else{
                $temp[] = '';
            }
            $temp[] = '<input type="button" class="gbutton more-msg" value="详情" onclick="more('.$msg['id'].')" />';
            $temp[] = $msg['manage'];
            $temp[] = date('Y-m-d H:i:s',$msg['create_time']);
            $btn = '<input type="button" class="gbutton view-btn" value="编辑" onclick="edit('.$msg['id'].')">';
            //返回成功状态才允许下架,不涉及服务器进度，可以支持热下架
            if($msg['status'] == 3){
                //$btn .= ' | <input type="button" class="gbutton down-btn" value="下架" onclick="down('.$msg['id'].')">';
            }
            $temp[] = $btn;
            array_push($pageData,$temp);
        }
        $this->setPageData($pageData,$num);
    }

    /**
     * 获取VIP公告信息
     * @param array $fields
     * @param $id
     * @return array
    */
    public function getVipnotice($fields = array(),$id = ''){
        empty($fields) && $fields = array('id','group_id','server','type','title','connect','content','level','manage','create_time');
        $row = $condition = [];
        if(is_array($id)){
            $condition['WHERE']['id::IN'] = array_filter($id);
            $row = $this->getRows($fields,$condition['WHERE']);
        }else{
            $condition['WHERE']['id'] = intval($id);
            $row = $this->getRow($fields,$condition['WHERE']);
        }
        return $row;
    }

    /**
     * 公告邮件更新至MongoDB
     * @param array $data
     * @return array $res
    */
    public function syncPush($data){
        if(empty($data)) return false;
        $id = intval($data['id']);
        if(empty($id)) return false;//失败
        $list = [];
        $goldLevel = $data['first_level'];//充值档次
        foreach($goldLevel as $key=>$val){
            $content = '会员公告';
            $contact = $data['connect'][$key];
            $emailTitle = $data['mail_title'][$key];
            $emailContent = $data['content'][$key];
            !isset($contact) && $contact = '';
            !isset($emailTitle) && $emailTitle = '';
            !isset($emailContent) && $emailContent = '';

            foreach($emailContent as &$msg){
                $msg = str_replace('X22#X22#','\n',$msg);
            }


            $itemList = [];   //道具 暂未设计
            $gold = intval($val);
            $temp = array(
                'content'=>$content,
                'contact'=>$contact,
                'gold'=>$gold,
                'emailTitle'=>$emailTitle,
                'emailSender'=>$data['emailSender'],
                'emailContent'=>$emailContent,
                'itemList'=>$itemList
            );
            $list[] = $temp;
        }
        $pushData['_id'] = $id;
        $pushData['updateTm'] = time();
        $pushData['list'] = $list;
        $type = $data['type'];
        $upType = array_intersect(array('1','2'),$type);
        empty(array_diff(array('1','2'),$type)) && $upType=[3];
        $pushData['type'] = array_shift($upType);
        //error_log(json_encode($pushData)."\n\r",3,'server.log');exit;
        if(empty($data['channel_group']) || is_array($data['channel_group'])) return false;
        $servers = empty($data['server'][0])?array():$data['server'];
        //过滤服务器ID
        $serverMsg = (new ServerModel())->getServerByServerAndChannelGroup($servers,array($data['channel_group']));
        if(empty($serverMsg)) return ['code'=>404,'msg'=>'未找到适合条件的服务器，请重新选择'];
        $server = array_keys($serverMsg);
        //$server = ['open1'];
        $res = $this->getGm(array(), $server);
        $param = Helper::mongoInt64($pushData);
        $serverRes = array();
        foreach ($res as $row) {
            $serverRes[$row['server_id']] = $row;
        }
        $postData = array(
            'r' => 'call_method',
            'method' => 'addNotice',//方法名(新增&&更新)
            'class' => 'VipnoticeModel',//单服类)
            'params' => json_encode(array('document' => $param)),
        );
        //查找服务器的api Url
        $instance  = new ServerconfigModel();
        $urls = $instance->getApiUrl($server, true);
        $rs = Helper::rolling_curl($urls, $postData);
        $code = array(
            1 => '更新mongo成功',
            2 => '配置不存在',
            3 => '写入mongo库失败',
            4 => '未知错误',
            5 => '更新失败',
        );
        $msg = [];
        foreach ($rs as $sign => $v) {
            $vs = isset($code[$v]) ? $code[$v] : $v;
            $serRow = isset($serverRes[$sign]) ? $serverRes[$sign] : '';
            $server_name =  'S'. $serRow['num']. '-'. $serRow['name'];
            $msg[$sign] = $v.'/'.$server_name . ':' .$vs;
        }
        $server = join(',',$server);
        $sql = "update ny_vipnotice set `status`=1,`server`='".$server."',`back_massage`='".json_encode($rs,JSON_UNESCAPED_UNICODE)."',`last_modified_time`=".time()." where `id`=".$id."";
        $res = $this->query($sql);
        //$res = $this->update(array('server'=>$server,'back_massage'=>$rs,'last_modified_time'=>time()),array('id'=>$id));
        return $msg;
    }

    /**
     * 公告更新，通知游戏服务器端
     * @param array $data
     * @return array
    */
    public function upVipNotice($data){
        if(empty($data)) return false;
        $group_id = $data['group_id'];
        $row_id = $data['id'];
        $handel_type = $data['handel_type'];//array('up','down')
        $upType = null;
        $type = $data['type'];
        $type = explode(',',$type);
        if(empty($group_id) || empty($type) || empty($row_id)) die(json_encode(['code'=>404,'msg'=>'开启失败，请检查配置']));
        $serverModel = new ServerModel();

        $upType = array_intersect(array('1','2'),$type);
        empty(array_diff(array('1','2'),$type)) && $upType=[3];
        $server = explode(',',$data['server']);
        empty(array_filter($server)) && $server = array();
        //过滤服务器ID
        $serverRes = $serverModel->getServerByServerAndChannelGroup($server,array($group_id));
        if(empty($serverRes)) die(json_encode(['code'=>0,'msg'=>'渠道组服务器不存在']));
        $server_id = array_keys($serverRes);
        $serId = [];
        $upType = array_shift($upType);
        foreach($server_id as $row){
            $serId[$row] = 1;
        }
        $url = Helper::v2();
        $res = Helper::rpc($url,$serId,'upRechMember',array('type'=>$upType,'handel_type'=>$handel_type));
        //error_log(json_encode($res)."\n\r",3,'server.log');
        #record the res
        $backMsg = array(
            '0'=>'发布失败',
            '2'=>'非法操作',
            '1'=>'发布成功'
        );
        //[server_id]=>0
        $out = $res;
        foreach($res as $key=>&$val){
            $val = isset($backMsg[$val])?$val.'/'.$backMsg[$val]:'未知错误';
        }
        $sql = "update ny_vipnotice set `status`= 3, `back_massage`='".json_encode($res,JSON_UNESCAPED_UNICODE)."',`last_modified_time`=".time()." where `id`=".$row_id."";
        $this->query($sql);
        //$this->update(array('back_massage'=>json_encode($res),'last_modified_time'=>time()),array('id'=>$row_id));
        return $out;
    }

    /**
     * 服务器提示插件
     * @mark tips
     * @param string $servers
     * @param array $serInfo
     * @return string $span
    */
    public static function tips($servers,$serInfo){
        if(empty($servers)) return '';
        $servers = explode(',',$servers);
        $servers = array_filter($servers);
        $title = '';
        $i = 1;
        foreach($servers as $sid){
            $title .= $serInfo[$sid];
            if($i%2 == 0) {
                $title .= "<br/>";
            }else{
                $title .=" , ";
            }
            $i++;
        }
        $span = '<span class="tb_tips_popover tag" tips="'.$title.'">'.mb_substr(str_replace('<br/>',',',$title),0,5,'utf-8').'....'.'</span>';
        return $span;
    }
}
?>