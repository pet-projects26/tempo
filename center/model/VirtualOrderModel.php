<?php

class VirtualOrderModel extends Model{

    public function __construct(){
        parent::__construct('virtual_order');
        $this->alias = 'vo';
    }
	public function  manuallist_data($conditions){
		unset($conditions['WHERE']['package::IN']);
		$rs = array();
		$fields = array('id','server' , 'corder_num', 'account' , 'role_id' , 'role_name' ,'money','admin',  'from_unixtime(create_time,"%Y-%m-%d %H:%i:%s") as create_time',' checked');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		$count=$this->getCount();
		
		foreach($rs as $k => $row){
			
			$server = $this->getGm(array('s.name') , $row['server'] );
			$row['server'] = $server['name'];

			$row['checked'] = $row['checked'] == 1 ? '<button class="btn btn-success">已审核</button> ' : '<input type="button" class="gbutton checked" value="审3核" onclick="_checked(\'' . $row['id'] . '\')">';

			unset($row['id']);
			$rs[$k] = array_values($row);	
		}
		
        $this->setPageData($rs , $count);	
	}
	
	public function  manuallist_export($conditions){
		unset($conditions['WHERE']['package::IN']);
		unset($conditions['Extends']['LIMIT']);
		$rs = array();
		$fields = array('id','server' , 'corder_num', 'account' , 'role_id' , 'role_name' ,'money','admin',  'from_unixtime(create_time,"%Y-%m-%d %H:%i:%s") as create_time',' checked');
		$rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		$count=$this->getCount();
	
		$result = array();
		$result[] = array('服务器' , '账号' , '角色id' , '角色名',
				'金额' , '申请者' ,'创建时间'
		);
		foreach($rs as $k=>$row){
			$server = $this->getGm(array('s.name') , $row['server'] );
			$result[$k+1]['server'] = $server['name'];
			$result[$k+1]['account'] = $row['account'];
			$result[$k+1]['role_id'] = $row['role_id'];
			$result[$k+1]['role_name'] = $row['role_name'];
			$result[$k+1]['money'] = $row['money'];
			$result[$k+1]['admin'] = $row['admin'];
			$result[$k+1]['create_time'] = $row['create_time'];
		}
		Util::exportExcel($result ,'补单列表（' . date('Y年m月d日H时i分s秒') . '）');
	
	}
}