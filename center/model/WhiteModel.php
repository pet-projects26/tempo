<?php

class WhiteModel extends Model{

    public function __construct(){
        parent::__construct('white_ip');
        $this->alias = 'w';
    }

    public function record_data($conditions){
        $fields = array('id' , 'server' , 'ip' , 'user' , 'from_unixtime(create_time,"%Y-%m-%d %H:%i:%s") as create_time');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $gm = $this->getGm(array('s.server_id' , 's.name'));
        $server = array();
        foreach($gm as $row){
            $server[$row['server_id']] = $row['name'];
        }
        foreach($rs as $k => $row){
            $rs[$k] = array(
                $row['ip'],
                $server[$row['server']],
                $row['user'],
                $row['create_time'],
                '<input type="button" class="gbutton" onclick="del(' . $row['id'] . ')" value="删除">'
            );
        }
        $this->setPageData($rs , $this->getCount());
    }

    //获取IP白名单
    public function getWhiteIps($type = 0){
        $mem = Util::memcacheConn();
        $white = $type ? array() : $mem->get('white_ip_list');
        if(!$white){
            $white = array();
            $fields = array('server' , 'ip');
            $rs = (new WhiteModel())->getRows($fields);
            foreach($rs as $row){
                $white[$row['server']][] = $row['ip'];
            }
            $mem->set('white_ip_list' , $white);
        }
        return $white;
    }
}