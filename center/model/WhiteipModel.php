<?php

class WhiteipModel extends Model{

    public function __construct(){
        parent::__construct('white_ip');
		$this->setNewPrefix('game_');
        $this->alias = 'w';
    }

    public function record_data($conditions){
		$result = array();
        $fields = array( 'id' , 'ip' , 'type', 'contents' ,  'create_time');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        
       
        foreach($rs as $k => $row){
			
			$result[$k]['ip'] = $row['ip'];
			switch($row['type']){
				case 0:
					$result[$k]['type'] = '启用';break; 
				case 1:
					$result[$k]['type'] = '关闭';break; 
			}
			$result[$k]['contents'] = $row['contents'];
			$result[$k]['create_time'] = date('Y-m-d H:i:s' , $row['create_time']);
           
          	$result[$k]['caozuo'] = '<input type="button" class="gbutton" onclick="change(' . $row['id'] . ',' . $row['type'] . ')" value="更改"><input type="button" class="gbutton" onclick="del(' . $row['id'] . ')" value="删除">' ;
            
        }
		
		foreach($result as $k => $row){
			$result[$k]	= array_values($row);
		}
		
        $this->setPageData($result , $this->getCount());
    }

    //获取IP白名单
   /* public function getWhiteIps($type = 0){
        $mem = Util::memcacheConn();
        $white = $type ? array() : $mem->get('white_ip_list');
        if(!$white){
            $white = array();
            $fields = array('server' , 'ip');
            $rs = (new WhiteModel())->getRows($fields);
            foreach($rs as $row){
                $white[$row['server']][] = $row['ip'];
            }
            $mem->set('white_ip_list' , $white);
        }
        return $white;
    }*/
}