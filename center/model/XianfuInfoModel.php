<?php

class XianfuInfoModel extends Model
{

    public function __construct()
    {
        parent::__construct('xianfu_info');
        $this->alias = 'a';
    }

    public function record_data($conditions)
    {
        $num = 0;
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'huiling_num', 'jubao_num', 'lianzhi_num', 'lianzhi_count', 'youli_num', 'youli_count', 'is_compass_num', 'is_compass_count', 'is_sihai_or_jiuzhou_count', 'is_end_num', 'is_end_count', 'active_val_20_num', 'active_val_60_num', 'active_val_100_num', 'active_val_150_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $num = $this->getCount();

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];

                $data[$k][3] = $row['huiling_num'] ? @sprintf("%.2f", $row['huiling_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][4] = $row['jubao_num'] ? @sprintf("%.2f", $row['jubao_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][5] = $row['lianzhi_num'] ? @sprintf("%.2f", $row['lianzhi_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][6] = $row['lianzhi_count'] ? @round($row['lianzhi_count'] / $row['lianzhi_num'], 2) : 0.00;

                $data[$k][7] = $row['youli_num'] ? @sprintf("%.2f", $row['youli_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][8] = $row['youli_count'] ? @round($row['youli_count'] / $row['youli_num'], 2) : 0.00;

                $data[$k][9] = $row['is_compass_count'] ? @sprintf("%.2f", $row['is_compass_count'] / $row['is_sihai_or_jiuzhou_count'] * 100) . '%' : '0%';

                $data[$k][10] = $row['is_compass_count'] ? @round($row['is_compass_count'] / $row['is_compass_num'], 2) : 0.00;

                $data[$k][11] = $row['is_end_count'] ? @sprintf("%.2f", $row['is_end_count'] / $row['youli_count'] * 100) . '%' : '0%';

                $data[$k][12] = $row['is_end_num'] ? @round($row['is_end_count'] / $row['is_end_num'], 2) : 0.00;

                $data[$k][13] = $row['active_val_20_num'] ? @sprintf("%.2f", $row['active_val_20_num'] / $row['active_num'] * 100) . '%' : '0%';
                $data[$k][14] = $row['active_val_60_num'] ? @sprintf("%.2f", $row['active_val_60_num'] / $row['active_num'] * 100) . '%' : '0%';
                $data[$k][15] = $row['active_val_100_num'] ? @sprintf("%.2f", $row['active_val_100_num'] / $row['active_num'] * 100) . '%' : '0%';
                $data[$k][16] = $row['active_val_150_num'] ? @sprintf("%.2f", $row['active_val_150_num'] / $row['active_num'] * 100) . '%' : '0%';

            }
        }

        foreach ($data as $k => $row) {
            $data[$k] = array_values($row);
        }

        $this->setPageData($data, $num);
    }

    public function record_export($conditions)
    {

        $rs = array();

        unset($conditions['Extends']['LIMIT']);
        if (!isset($conditions['WHERE']['server::IN'])) {
            $conditions['WHERE']['server::IN'][] = (new ServerModel())->getLatestServer();
        }

        $conditions['WHERE']['a.create_time::>='] = isset($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : strtotime(date('Ymd')) - 86400 * 9;
        $conditions['WHERE']['a.create_time::<='] = isset($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : strtotime(date("Ymd")) + 86400 - 1;
        unset($conditions['WHERE']['create_time::>=']);
        unset($conditions['WHERE']['create_time::<=']);

        $conditions['WHERE']['a.server::IN'] = $conditions['WHERE']['server::IN'];
        unset($conditions['WHERE']['server::IN']);

        $data = [];

        $this->joinTable = array(
            's' => array('name' => 'server', 'type' => 'LEFT', 'on' => 's.server_id = a.server'),
        );
        $fields = array("from_unixtime(a.create_time,'%Y-%m-%d') as create_time", "s.name as server", "active_num", 'huiling_num', 'jubao_num', 'lianzhi_num', 'lianzhi_count', 'youli_num', 'youli_count', 'is_compass_num', 'is_compass_count', 'is_sihai_or_jiuzhou_count', 'is_end_num', 'is_end_count', 'active_val_20_num', 'active_val_60_num', 'active_val_100_num', 'active_val_150_num');

        $arr = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if ($arr) {
            foreach ($arr as $k => $row) {
                $data[$k][0] = $row['create_time'];
                $data[$k][1] = $row['server'];
                $data[$k][2] = $row['active_num'];

                $data[$k][3] = $row['huiling_num'] ? @sprintf("%.2f", $row['huiling_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][4] = $row['jubao_num'] ? @sprintf("%.2f", $row['jubao_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][5] = $row['lianzhi_num'] ? @sprintf("%.2f", $row['lianzhi_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][6] = $row['lianzhi_count'] ? @round($row['lianzhi_count'] / $row['lianzhi_num'], 2) : 0.00;

                $data[$k][7] = $row['youli_num'] ? @sprintf("%.2f", $row['youli_num'] / $row['active_num'] * 100) . '%' : '0%';

                $data[$k][8] = $row['youli_count'] ? @round($row['youli_count'] / $row['youli_num'], 2) : 0.00;

                $data[$k][9] = $row['is_compass_count'] ? @sprintf("%.2f", $row['is_compass_count'] / $row['is_sihai_or_jiuzhou_count'] * 100) . '%' : '0%';

                $data[$k][10] = $row['is_compass_count'] ? @round($row['is_compass_count'] / $row['is_compass_num'], 2) : 0.00;

                $data[$k][11] = $row['is_end_count'] ? @sprintf("%.2f", $row['is_end_count'] / $row['youli_count'] * 100) . '%' : '0%';

                $data[$k][12] = $row['is_end_num'] ? @round($row['is_end_count'] / $row['is_end_num'], 2) : 0.00;

                $data[$k][13] = $row['active_val_20_num'] ? @sprintf("%.2f", $row['active_val_20_num'] / $row['active_num'] * 100) . '%' : '0%';
                $data[$k][14] = $row['active_val_60_num'] ? @sprintf("%.2f", $row['active_val_60_num'] / $row['active_num'] * 100) . '%' : '0%';
                $data[$k][15] = $row['active_val_100_num'] ? @sprintf("%.2f", $row['active_val_100_num'] / $row['active_num'] * 100) . '%' : '0%';
                $data[$k][16] = $row['active_val_150_num'] ? @sprintf("%.2f", $row['active_val_150_num'] / $row['active_num'] * 100) . '%' : '0%';
            }
        }
        $result = array();

        $result[] = array('日期', '服务器', '活跃人数', '汇灵参与度', '聚宝参与度', '炼制参与度', '人均炼制次数', '游历参与度', '人均游历次数', '游历罗盘参与度', '人均罗盘次数', '游历立即结束参与度', '人均立即结束次数', '仙府任务参与度1', '仙府任务参与度2', '仙府任务参与度3', '仙府任务参与度4');

        foreach ($data as $row) {
            $result[] = $this->formatFields($row);
        }

        Util::export_csv_2($result, '', '仙府洞天（' . date('Y年m月d日') . '）.csv');
    }

    public function getRowData($date, $server)
    {
        $conditions = [];
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['server'] = $server;

        $fields = array('id');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }
}