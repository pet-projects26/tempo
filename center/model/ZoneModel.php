<?php

class ZoneModel extends Model{

    public function __construct(){
        parent::__construct('zone');
        $this->alias = 'z';
    }

    public function getZone($id = '' , $fields = array() , $start = '' , $end = ''){
        empty($fields) && $fields = array('id' , 'group_id', 'name' , 'sort', 'remark', 'cdn_url', 'create_time');
        $conditions = array();
        if(is_array($id)){
            
            $conditions['WHERE'] = array();

            $id && $conditions['WHERE']['id::IN'] = $id;
            $start && $conditions['WHERE']['create_time::>='] = $start;
            $end && $conditions['WHERE']['create_time::<='] = $end;
            $conditions['Extends']['ORDER'] = array('id#desc');
            return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        }
        else{
            $id && $conditions['WHERE']['id'] = $id;
            return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    public function getCdnUrl($zone_id)
    {
        $rs = $this->getRow('cdn_url', ['id' => $zone_id]);

        $cdn_url = $rs['cdn_url'] ? $rs['cdn_url'] : '';

        return $cdn_url;
    }

    public function record_data($conditions){
        $rs = $this->getZone(array());
        foreach($rs as $k => $row){

            $group_num = $row['group_id'];
            $groupsql = "select name from ny_channel_group where id = '".$group_num."'";
            $grouparr = $this->query($groupsql);
            $row['group_id'] = $row['group_id'].'('.$grouparr[0]['name'].')';

            $row['create_time'] = date('Y-m-d H:i:s' , $row['create_time']);
            $row['options']  = '<input type="button" class="gbutton" value="编辑" onclick="edit(' . $row['id'] . ')">';
            $row['options'] .= '<input type="button" class="gbutton" value="删除" onclick="del(' . $row['id'] . ')">';
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , count($rs) , $conditions['Extends']['LIMIT']);
    }

    public function addZone($data){
        if($data){
            return $this->add($data);
        }
    }

    public function editZone($data , $id){
        if($data && $id){
            $conditions = array();
            $conditions['WHERE']['id'] = $id;
            return $this->update($data , $conditions['WHERE']);
        }
        else{
            return false;
        }
    }

    public function delZone($id){
        if($id){
            $conditions = array();
            $conditions['WHERE']['id'] = $id;
            return $this->delete($conditions['WHERE']);
        }
    }
}