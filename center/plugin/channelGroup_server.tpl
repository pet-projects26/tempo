<style type="text/css">
	.p-input {
		vertical-align:middle;
	}

	.p-text {
		vertical-align:middle;
	}

	.p-span {
		padding: 5px 10px;
		margin: 0px 3px;
	}

	.w-150 {
		width: 150px;
	}

	fieldset {
		padding: 5px;
		border: 2px solid #cc0;
		margin: 10px 0px;
	}

	.p-checked {
		background: #ff0;
		border-radius: 3px;
	}
</style>

<!-- 渠道组 Start -->
<tr class='p-tr'>
	<td style="width:150px">渠道组</td>
	<td>
		<span class="p-span c-all c-channel-group p-checked span-click">
			<input class="p-input"  type="checkbox" name="channel_group[]"  value="" checked id="channel-group-all" />
			<label for="channel-group-all" class="p-text">所有渠道组</label>
		</span>

		<{foreach from = $groups key = id item = row}>
			<span class="p-span c-channel-group  c-channel-group-list span-click">
				<input class="p-input"  type="checkbox" name="channel_group[]"  value="<{$id}>"  id="channel_group-<{$id}>" />
				<label for="channel_group-<{$id}>" class="p-input" ><{$row}></label>
			</span> 
		<{/foreach}>
	</td>
</tr>

<!-- 渠道组 End -->

<!-- 服务器 Start  -->
<tr class="p-tr server">
	<td class="w-150">服务器</td>
	<td>
		<fieldset>
		    <legend> 当前渠道组下所有服务器 </legend>
	    	<span class="p-span p-checked s-server-click">
				<input class="p-input"  type="checkbox" name="server[]"  value="" id="server-all" checked />
				<label for="server-all" class="p-input" >当前所选渠道组下所有服务器</label>
			</span> 
			<font style="color:#f00; font-weight: bold;">选择该选项后，代表选中当前所选的渠道组下所有服务器</font>
		</fieldset>

		<{foreach from = $servers key = group_id  item = rows }> 
		<fieldset id="<{$group_id}>" style="display: none">
		    <legend> <{$groups[$group_id]}> </legend>
		    <{foreach from = $rows  key = sign  item = name}>
		    	<span class="p-span s-server-click" >
					<input class="p-input"  type="checkbox" name="server[]"  value="<{$sign}>" id="server-<{$sign}>" />
					<label for="server-<{$sign}>" class="p-input" ><{$name}></label>
				</span> 
		    <{/foreach}>
		</fieldset>
		<{/foreach}>
	</td>
</tr>
<!-- 服务器 End -->

<script type="text/javascript">
	$().ready(function() {
		//所有渠道
		$('.c-all').unbind('click').bind('click', function() {
			var checked = $(this).find('input').attr('checked');

			var obj = $(this).find('input');

			var mark = obj.prop('checked');

			if (mark) {
				$('.c-channel-group-list input').prop('checked', 0);
				$('.c-channel-group-list input').parent('span').removeClass('p-checked');

				$(this).addClass('p-checked');

			} else {
				$(this).removeClass('p-checked');
			}

			$('.server fieldset').css('display' , 'block');
			$('.server fieldset').removeClass('show' );


			//自动选中全部服务器
			$('.server input[value !=""]').prop('checked', 0);
			$('.server  input[value !=""]').parent('span').removeClass('p-checked');
			$('.server input[value=""]').prop('checked', 1);
			$('.server input[value=""]').parent('span').addClass('p-checked');
			
		});

		//其他渠道
		$('.c-channel-group-list input').unbind('click').bind('click', function() {
			var mark = $(this).prop('checked');
			if (mark) {
				$(this).parent('span').addClass('p-checked');
			} else {
				$(this).parent('span').removeClass('p-checked');
			}

			var allLen = $('.c-channel-group input').length;

			var checkLen = $('.c-channel-group-list input:checked').length;

			//如果都没有选中，则自动选中全选 
			if (checkLen == 0) {
				$('.c-channel-group input[value=""]').prop('checked', 1);
				$('.c-channel-group input[value=""]').parent('span').addClass('p-checked');

				//自动选中全部服务器
				$('.server input[value !=""]').prop('checked', 0);
				$('.server input[value !=""]').parent('span').removeClass('p-checked');
				$('.server input[value=""]').prop('checked', 1);
				$('.server input[value=""]').parent('span').addClass('p-checked');

			} else {
				$('.c-channel-group input[value=""]').prop('checked', 0);
				$('.c-channel-group input[value=""]').parent('span').removeClass('p-checked');

			}

			//根据渠道组筛选服务器
			InputValue=$(this).val();

			if($('#'+InputValue).hasClass('show')){
       
	          $('#'+InputValue).removeClass('show');
	        } else{

	          $('#'+InputValue).addClass('show');
	        }

			$('.server fieldset').each(function () {  //遍历child,如果有show 就让他显示，没有就隐藏
				$('.server fieldset').eq(0).show();
				if ($(this).hasClass('show')) {
					$(this).show();
				} else {
					//清除选择
					$(this).children('span').removeClass('p-checked');
					$(this).children('span').find('.p-input').prop('checked',0);
					$(this).hide();
				}
			});
	        //如果没选中，则全部显示
	       	if(checkLen == 0){
	        	$('.server fieldset ').show();
	        }

		});	

		
		//当前渠道的所有服
		$('.s-server-click input[value=""]').unbind('click').bind('click', function() {
			var mark = $(this).prop('checked');

			if (mark) {
				$('.s-server-click  input[value != ""]').prop('checked', 0);
				$('.s-server-click  input[value != ""]').parent('span').removeClass('p-checked');
				
				$(this).parent('span').addClass('p-checked');
			} else {
				$(this).parent('span').removeClass('p-checked');
			}
		});
		
		// 非全服的其他服
		$('.s-server-click  input[value != ""]').unbind('click').bind('click', function() {
			var obj = '.s-server-click';
			var mark = $(this).prop('checked');
			if (mark) {

				$(this).parent('span').addClass('p-checked');
			} else {
				$(this).parent('span').removeClass('p-checked');
			}

			var allLen = $(obj + ' input').length;

			var checkLen = $(obj + ' input[value != ""]:checked').length;

			//如果都没有选中，则自动选中全选 
			if (checkLen == 0) {
				$(obj + ' input[value=""]').prop('checked', 1);
				$(obj + ' input[value=""]').parent('span').addClass('p-checked');
			} else {
				$(obj + ' input[value=""]').prop('checked', 0);
				$(obj + ' input[value=""]').parent('span').removeClass('p-checked');
			}

		});	


	});
</script>

