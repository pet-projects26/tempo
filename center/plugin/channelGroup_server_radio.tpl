<style>
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }
    .p-input {
        vertical-align:middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }
    .highlight {
        background: yellow;
        color: red;
    }
    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<script>
    var pretime = $('.pretime').html();
    $('#btnSearch').click(function () {
        $('#pretime').html(pretime);
        var searchText = $('#txtKey').val();
        if (searchText.length == 0) {
            alert('请输入搜索关键词!');
            $('#txtKey').focus();
            return false;
        }
        var regExp = new RegExp(searchText, 'g');
        $('#pretime span.p-span').each(function () {
            var html = $(this).html();
            var _time = $(this).parent().find("strong").text();
            var newHtml = html.replace(regExp, '<span class="highlight">' + searchText + '</span>');
            $(this).html(newHtml);
            flag = 1;

        });
        if (flag) {
            if ($(".highlight").size() > 1) {
                var _top = $(".highlight").eq(0).offset().top;
                $("html,body").animate({
                    "scrollTop": _top
                }, 500)
            }
        }
    });
    //点击后加入标识
    $('.s-server-click  input[value != ""]').unbind('click').bind('click', function() {
        var mark = $(this).prop('checked');
        if (mark) {
            var parent = $(this).parent('span').parent('fieldset').siblings().children('span');
            console.log(parent);
            parent.each(function(){
                $(this).removeClass('p-checked')
            });
            $(this).parent('span').addClass('p-checked');
        } else {
            $(this).parent('span').removeClass('p-checked');
        }
        $(this).parent('span').siblings().removeClass('p-checked');

    })

</script>
<tr>
    <!--<td colspan="2">
        <div>
            <label for="txtKey">服务器名：</label>
            <input id="txtKey" type="text" />
            <input id="btnSearch" type="button" class="gbutton" value="查找">
        </div>
    </td>-->
</tr>
<tr>
    <td style="width: 150px; margin-top: 3px; margin-bottom: 3px;">服务器列表</td>
    <td>
        <{foreach from = $servers key = group_id  item = rows }>
        <fieldset class="pretime">
            <legend> <{$groups[$group_id]}> </legend>
            <{foreach from = $rows  key = sign  item = name}>
                        <span class="p-span s-server-click" >
                            <input type="hidden" value="">
                            <input class="p-input" type="radio" name="server"  value="<{$sign}>" id="server-<{$sign}>" />
                            <label for="server-<{$sign}>" class="p-input" ><{$name}></label>
                        </span>
            <{/foreach}>
        </fieldset>
        <{/foreach}>
    </td>
</tr>