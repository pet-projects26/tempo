<style type="text/css">
	.p-input {
		vertical-align:middle;
	}

	.p-text {
		vertical-align:middle;
	}

	.p-span {
		padding: 5px 10px;
		margin: 0px 3px;
	}

	.w-150 {
		width: 150px;
	}

	fieldset {
		padding: 5px;
		border: 2px solid #cc0;
		margin: 10px 0px;
	}

	.p-checked {
		background: #ff0;
		border-radius: 3px;
	}
</style>

<!-- 渠道组 Start -->
<tr class='p-tr channel_group'>
	<td style="width:150px">渠道组</td>
	<td>
      	<{foreach from = $groups key = id item = row}>
			<span class="p-span c-channel-group  c-channel-group-list span-click">
				<input class="p-input"  type="checkbox" name="channel_group"  value="<{$id}>"  id="channel_group-<{$id}>" />
				<label for="channel_group-<{$id}>" class="p-input" ><{$row}></label>
			</span> 
		<{/foreach}>
	</td>
</tr>

<!-- 渠道组 End -->

<!-- 服务器 Start  -->
<tr class="p-tr server" style="display: none;">
	<td class="w-150">服务器</td>
	<td>
		
		<{foreach from = $servers key = group_id  item = rows }> 
		<fieldset id="<{$group_id}>" style="display: none;">
		    <legend> <{$groups[$group_id]}> </legend>
		    <{foreach from = $rows  key = sign  item = name}>
		    	<span class="p-span s-server-click" >
					<input class="p-input" type="radio" name="server" value="<{$sign}>" id="server-<{$sign}>"/>
					<label for="server-<{$sign}>" class="p-input" ><{$name}></label>
				</span> 
		    <{/foreach}>
		</fieldset>
		<{/foreach}>
	</td>
</tr>
<!-- 服务器 End -->

<script type="text/javascript">
	$().ready(function() {
		

		//其他渠道
		$('.c-channel-group-list input').unbind('click').bind('click', function() {

			$('.server').show();

			var mark = $(this).prop('checked');

			if (mark) {
				$(this).parent('span').addClass('p-checked');
			} else {
				$(this).parent('span').removeClass('p-checked');

			}

			var checkLen = $('.c-channel-group-list input:checked').length;
			
			if (checkLen == 0) {
				$('.server').hide();
				$('.c-channel-group input[value=""]').prop('checked', 1);
				$('.c-channel-group input[value=""]').parent('span').addClass('p-checked');

			} else {

				$('.c-channel-group input[value=""]').prop('checked', 0);
				$('.c-channel-group input[value=""]').parent('span').removeClass('p-checked');

			}

			//根据渠道组筛选服务器
			InputValue=$(this).val();

			if($('#'+InputValue).hasClass('show')){
       
	          $('#'+InputValue).removeClass('show');

	          //去除服务器的被勾选状态
	          $('#'+InputValue + ' .p-span').removeClass('p-checked');
	          $('#'+InputValue + ' .p-span').find('input').attr('checked' ,false);

	        } else{

	          $('#'+InputValue).addClass('show');
	          $('#'+InputValue).siblings().removeClass('show');
	        }

	        $('.server fieldset').each(function(){  //遍历child,如果有show 就让他显示，没有就隐藏
	        
	          if($(this).hasClass('show')){
	            $(this).show();
	          }else{
	            $(this).hide();
	          }
	        })
	       
		});	

		
		//点击后加入标识
	    $('.s-server-click  input[value != ""]').unbind('click').bind('click', function() {
	        var mark = $(this).prop('checked');
	        if (mark) {
	            var parent = $(this).parent('span').parent('fieldset').siblings().children('span');
	            //console.log(parent);
	            parent.each(function(){

	                $(this).removeClass('p-checked');
	                $('.s-server-click').find('input').attr('checked',false);
	            });
	            $(this).attr('checked',true);

	            $(this).parent('span').addClass('p-checked');
	        } else {
	            $(this).parent('span').removeClass('p-checked');
	        }
	        $(this).parent('span').siblings().removeClass('p-checked');

	    })

	    $('.server.p-input').click(function(){
             if(!$(this).hasClass('select')){
                $('.p-input').attr("checked", false); 
                $('.p-input').removeClass('select');
                $('.p-span').removeClass('p-checked');

                $(this).addClass('select');
                $(this).parent().addClass('p-checked');
                $(this).attr("checked", true); 
            }
            else{
                $(this).attr("checked", false); 
                $(this).removeClass('select');
                $(this).parent().removeClass('p-checked');
            }
            //getServerSelected();
        });

         $('.channel_group .p-input').click(function(){
             if(!$(this).hasClass('select')){
                $('.p-input').attr("checked", false); 
                $('.p-input').removeClass('select');
                $('.p-span').removeClass('p-checked');

                $(this).addClass('select');
                $(this).parent().addClass('p-checked');
                $(this).attr("checked", true); 
            }
            else{
                $(this).attr("checked", false); 
                $(this).removeClass('select');
                $(this).parent().removeClass('p-checked');
            }
           
        });



	});
</script>

