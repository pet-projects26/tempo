<?php


class NewServerAccountRedis extends redisServer
{

    private $NewServerAccountKey = 'NewServerAccountHash';

    public $Model;

    public function __construct($db = REDIS_DB, $host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PWD)
    {
        parent::__construct($host, $port, $pwd, $db);

        $this->Model = new NewServerAccountModel();
    }

    public function addNewServerAccountKey($channel, $package, $server, $account, $row)
    {
        $key = $channel.'_'.$package.'_'.$server.'_'.$this->NewServerAccountKey;

        if (!$this->redis->hExists($key, $account)) {
            $row_pack = msgpack_pack($row);
            $this->redis->hSet($key, $account, $row_pack);

            $this->Model->add($row);
        }
    }

    public function updateNewServerAccountField($channel, $package, $server, $account, $fieldsValues)
    {
        $key = $channel.'_'.$package.'_'.$server.'_'.$this->NewServerAccountKey;

        if ($this->redis->hExists($key, $account)) {
            $row = $this->redis->hGet($key, $account);
            $row = msgpack_unpack($row);
            foreach ($fieldsValues as $f => $v) {
                $row[$f] = $fieldsValues[$f];
            }
            $row = msgpack_pack($row);
            $this->redis->hSet($key, $account, $row);

            $this->Model->update($fieldsValues, ['channel' => $channel, 'package' => $package, 'server' => $server, 'account' => $account]);
        }
    }

    public function selectAccountStatus($channel, $package, $server, $account, $filed)
    {
        $key = $channel.'_'.$package.'_'.$server.'_'.$this->NewServerAccountKey;

        if (!$this->redis->hExists($key, $account)) {
            return false;
        }

        $row = $this->redis->hGet($key, $account);
        $row = msgpack_unpack($row);

        $val = array_key_exists($filed, $row) ? $row[$filed] : 0;

        return $val;
    }

    public function selectAccountIsFirst($channel, $package, $server, $account)
    {
        $key = $channel.'_'.$package.'_'.$server.'_'.$this->NewServerAccountKey;
        return $this->redis->hExists($key, $account);
    }

    public function multiAddNewServerAccountKey($channel, $package, $server, $account, $row)
    {
        $key = $channel.'_'.$package.'_'.$server.'_'.$this->NewServerAccountKey;

        if (!$this->redis->hExists($key, $account)) {
            $row_pack = msgpack_pack($row);
            $this->redis->hSet($key, $account, $row_pack);

            return true;
        }

        return false;
    }
}