<?php


class NodeRedis extends redisServer
{
    private $nodeWaitKey = 'NodeWaitList';

    private $nodeHashkey = 'NodeWaitHash'; //node节点统计的

    private $activeAccountWaitKey = 'ActiveAccountList'; //活跃玩家等待队列

    private $activeAccountHashKey = 'ActiveAccountHash'; // 活跃玩家的hash

    public function __construct($db = REDIS_DB, $host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PWD)
    {
        parent::__construct($host, $port, $pwd, $db);
    }

    //入队
    public function lPushAccountNodeList($channel, $server, $package, $status, $account, $row)
    {
        $hashKey = $channel.'_'.$server.'_'.$package.'_'.$account.'_'.$this->nodeHashkey;

        if (!$this->redis->hExists($hashKey, $status)) {
            $row = msgpack_pack($row);
            $this->redis->hSet($hashKey, $status, $row);

            //判断队列中是否已存在
            $ListSet = $this->redis->lRange($this->nodeWaitKey, 0, -1);

            if (empty($ListSet) || !in_array($hashKey, $ListSet)) {
                //入队
                $this->redis->lPush($this->nodeWaitKey, $hashKey);
            }
        }
    }

    //入队
    public function rPushAccountNodeList(array $hashKey)
    {
        krsort($hashKey);
        foreach ($hashKey as $row) {
            $this->redis->rPush($this->nodeWaitKey, $row);
        }
    }

    //出队
    public function rPopAccountNodeList($num = 10)
    {
        $List = [];
        $HashKey = [];

        if ($num === 0) {
            $listSet = $this->redis->lrange($this->activeAccountWaitKey, 0, -1);

            $num = count($listSet);
        }

        if ($num != 0) {
            for ($i = 0; $i < $num; $i++) {
                $key = $this->redis->rPop($this->nodeWaitKey);

                if (!$key) break;
                $HashKey[] = $key;
                $rows = $this->redis->hGetAll($key);
                foreach ($rows as $row) {
                    $row = msgpack_unpack($row);
                    $k = $row['server'] . '_' . $row['account'];
                    $List[$k][$row['status']] = $row;
                }
            }
        }

        $info = [
            'list' => $List,
            'hashKey' => $HashKey
        ];

        return $info;
    }

    //删除对应的hash键
    public function delHashKey(array $hashKey = [])
    {
        if (!empty($hashKey)) {
            $this->redis->delete($hashKey);
        }
    }

    //入队
    public function lPushActiveAccountList($channel, $server, $package, $account, $create_time, $row)
    {
        $hashKey = $channel.'_'.$server.'_'.$package.'_'.$account.'_'.$this->activeAccountHashKey;

        if (!$this->redis->hExists($hashKey, $create_time)) {
            $row = msgpack_pack($row);
            $this->redis->hSet($hashKey, $create_time, $row);

            //判断队列中是否已存在
            $ListSet = $this->redis->lRange($this->activeAccountWaitKey, 0, -1);
            if (empty($ListSet) || !in_array($hashKey, $ListSet)) {
                //入队
                $this->redis->lPush($this->activeAccountWaitKey, $hashKey);
            }
        }
    }

    //入队
    public function rPushActiveAccountList(array $hashKey)
    {
        krsort($hashKey);
        foreach ($hashKey as $row) {
            $this->redis->rPush($this->activeAccountWaitKey, $row);
        }
    }

    //出队
    public function rPopActiveAccountList($num = 10)
    {
        $List = [];
        $HashKey = [];

        if ($num === 0) {
            $listSet = $this->redis->lrange($this->activeAccountWaitKey, 0, -1);

            $num = count($listSet);
        }

        if ($num != 0) {
            for($i = 0; $i < $num; $i++) {
                $key= $this->redis->rPop($this->activeAccountWaitKey);
                if (!$key) break;

                $HashKey[] = $key;
                $rows = $this->redis->hGetAll($key);
                foreach ($rows as $row) {
                    $List[] = msgpack_unpack($row);
                }
            }
        }

        $info = [
            'list' => $List,
            'hashKey' => $HashKey
        ];

        return $info;
    }

}