<?php
/**
 * 每小时统计数据的redis操作类
 * Created by PhpStorm.
 * User: w
 * Date: 2018/10/30
 * Time: 17:16
 */

class PerhourRedis extends redisServer
{
    protected $queueKey = 'perHourQueue'; //队列的key
    protected $strKey = 'perHourStr_'; //str的key

    public function __construct($host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PWD, $db = REDIS_DB)
    {
        parent::__construct($host, $port, $pwd, $db);
    }

    //插入数据 生成队列
    public function insterQueue(array $data)
    {
        if (empty($data)) return false;

        //将数据 msgpack_pack 后写进strkey 插进队列中 shell脚本定时去执行插入数据库
        $dataStr = msgpack_pack($data);

        $strKey = $this->strKey.date('YmdHis');

        if ($this->add($strKey, $dataStr)) {
            return $this->addQueue($this->queueKey, $strKey);
        }

        return false;
    }

    //查出数据循环插入数据库 出队
    public function insterMysql()
    {
        while (true) {
            try {
                $key = $this->outQueue($this->queueKey);
                if (!$key) {
                    break;
                }

                //取出对应的值
                $dataStr = $this->get($key);

                $value = msgpack_unpack($dataStr);

                (new PerhourModel())->insterData($value);

                //存储完数据后删除key
                $this->delete($key);

                /*
                * 利用$value进行逻辑和数据处理
                */
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

}