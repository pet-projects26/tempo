<?php
/**
 * 服务器redis数据操作类
 * Created by PhpStorm.
 * User: w
 * Date: 2019/4/25
 * Time: 17:45
 */

class ServerRedis extends redisServer
{
    public $isWhite = false; //是否为ip白名单

    private $serverKey = 'Server'; //hash 里的 key serverId

    private $isWhiteServerKey = 'isWhiteServer'; // 白名单里的serverId

    private $groupKey = 'Group'; //hash key group_id field msg_pack(组内serverId);

    private $isWhiteGroupKey = 'isWhiteGroup';

    private $channelSetKey = 'ChannelSet'; //存储所有channel标识 set

    private $groupSetKey = 'GroupSet'; //存储所有渠道号标识 set

    private $newServerKey = 'NewServer'; //hash key group_id_提审状态 value server_id  最新服的serverId

    private $isWhiteNewServerKey = 'isWhiteNewServer';

    private $recentServerKey = 'RecentServerSet'; //Set 集合 每个元素名是$accountRecentServerKey  便于统一管理List

    private $isWhiteRecentServerKey = 'isWhiteRecentServerSet';

    private $accountRecentServerKey = 'AccountRecentServerList'; // List 玩家历史选择的服务器 队列

    private $isWhiteAccountRecentServerKey = 'isWhiteAccountRecentServerList'; // List 玩家历史选择的服务器 队列

    private $packageKey = 'Package'; //hash 里的key package_id

    private $zoneKey = 'Zone'; //hash 里的key zoneId

    private $whiteIpKey = 'WhiteIpSet'; //白名单ip集合

    private $serverSignKey = '5727a407-bfc5-4b9b-a8ce-1db64e0d9dd1'; //与服务端验签的KEY

    public function __construct($isAdmin = false, $host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PWD, $db = REDIS_DB)
    {
        parent::__construct($host, $port, $pwd, $db);

        if (!$isAdmin) {
            $isWhite = $this->checkIp();
            if ($isWhite) {
                $this->serverKey = $this->isWhiteServerKey;
                $this->groupKey = $this->isWhiteGroupKey;
                $this->newServerKey = $this->isWhiteNewServerKey;
                $this->recentServerKey = $this->isWhiteRecentServerKey;
                $this->accountRecentServerKey = $this->isWhiteAccountRecentServerKey;
                $this->isWhite = true;
            }
        }
    }

    public function getPackageById($packageId)
    {

        if (!$this->redis->hExists($this->packageKey, $packageId)) {
            $package = $this->packageExists($packageId, 'package_id');
        } else {
            $package = $this->redis->hGet($this->packageKey, $packageId);
            $package = msgpack_unpack($package);
        }

        return $package;
    }

    public function getPackage($packageName)
    {
        //获取所有package
        $packageList = $this->redis->hGetAll($this->packageKey);
        $is_redis = 1;

        if (empty($packageList)) {
            $is_redis = 0;
            $packageList = $this->packageExists($packageName, 'all');
        }

        if (empty($packageList)) return [];

        $package = [];
        foreach ($packageList as $row) {
            if ($is_redis == 1) {
                $row = msgpack_unpack($row);
            }
            if ($row['name'] == $packageName) {
                $package = $row;
                break;
            }
        }

        //如果不存在则从数据库读取并写入Package
        if (empty($package)) {
            $package = $this->packageExists($packageName, 'name');
        }

        return $package;
    }

    //获取渠道组服务器列表
    public function getGroupServerList($group_id, $package, $account, $rvtype = 0)
    {
        $serverIdList = [];

        $key = $group_id;

        if ($rvtype) {
            $key .= '_1'; //带1代表为渠道提审服
        }

        //获取对应渠道组id的服务器列表
        if (!$this->redis->hExists($this->groupKey, $key)) {
            //从Mysql读取该组需要的服务器数据
            $serverIdList = [];
        } else {
            $serverIdList = $this->redis->hGet($this->groupKey, $key);
            $serverIdList = msgpack_unpack($serverIdList);
            //如果为空, 也去Mysql读取
        }

        if (empty($serverIdList)) {
            //去Mysql读取改group_id的服务器数据, 并且 把Group里对应的field的值改写
            $serverIdList = $this->groupServerIdExists($group_id, $rvtype);
        }

        //如果服务器id还是空, 返回空数组
        if (empty($serverIdList)) return [];

        //获取Server里对应的服务器数据
        $serverList = $this->redis->hMGet($this->serverKey, $serverIdList);

        $servers = [];
        $serverNum = [];

        foreach ($serverList as $k => $row) {
            $row = msgpack_unpack($row);
            if (empty($row)) {
                //获取服务器数据并写入redis
                $row = $this->serverExists($k);
            }

            if (empty($row)) continue;

            $serverNum[] = (int)$row['num'];
            $servers[] = $this->serverFormat($row, $package, $account);
        }

        unset($row);

        //排序
        if (!empty($servers)) {
            array_multisort($serverNum, SORT_DESC, $servers);
        }

        return $servers;
    }

    //获取根据serverId获取服务器
    public function getServerById($server_id, $package, $account)
    {
        $row = [];
        if (!$this->redis->hExists($this->serverKey, $server_id)) {
            $row = $this->serverExists($server_id);
        } else {
            $row = $this->redis->hGet($this->serverKey, $server_id);
            $row = msgpack_unpack($row);
        }

        if (empty($row)) return [];

        $row = $this->serverFormat($row, $package, $account);

        return $row;
    }

    //获取服务器配置
    public function getServerConfig($server_id)
    {
        $row = [];
        if (!$this->redis->hExists($this->serverKey, $server_id)) {
            $row = $this->serverExists($server_id);
        } else {
            $row = $this->redis->hGet($this->serverKey, $server_id);
            $row = msgpack_unpack($row);
        }

        return $row;
    }

    //获取历史服务器
    public function getRecentServer($group_id, $package, $account, $rvtype = 0)
    {
        //ListKey
        $ListKey = $group_id . '_' . $account . '_' . $this->accountRecentServerKey;

        $servers = [];

        //获取List是否存在
        if (!$this->AccountRecentServerExists($group_id, $account)) {
            //不存在, 直接返回最新服务器
            $server = $this->getNewServer($group_id, $package, $account, $rvtype);
            $servers[] = $server;
        } else {
            $serverIdList = $this->redis->lRange($ListKey, 0, -1);

            //获取Server里对应的服务器数据
            $serverList = $this->redis->hMGet($this->serverKey, $serverIdList);

            foreach ($serverList as $k => $row) {
                $row = msgpack_unpack($row);
                if (empty($row)) {
                    //获取服务器数据并写入redis
                    $row = $this->serverExists($k);
                }

                if (empty($row)) continue;

                $servers[] = $this->serverFormat($row, $package, $account);
            }
            unset($row);
        }

        return $servers;
    }

    public function AccountRecentServerExists($group_id, $account)
    {
        //ListKey
        $ListKey = $group_id . '_' . $account . '_' . $this->accountRecentServerKey;

        if (!$this->redis->exists($ListKey)) {
            return false;
        }
        return true;
    }


    //玩家选中服务器时 改变队列
    public function AccountRecentServerListChange($package, $server_id, $account)
    {
        $packageData = $this->getPackageById($package);
        $group_id = $packageData['group_id'];

        //ListKey
        $ListKey = $group_id . '_' . $account . '_' . $this->accountRecentServerKey;

        //LPUSH 或者 LREM $List count value
        //获取List是否存在
        if (!$this->redis->exists($ListKey)) {
            //不存在的话, 要创建队列, 并且在setKey中增加 $ListKey 元素
            $this->redis->sAdd($this->recentServerKey, $ListKey);
        } else {
            //判断队列中是否已存在
            $serverList = $this->redis->lRange($ListKey, 0, -1);

            foreach ($serverList as $v) {
                if ($v == $server_id) {
                    //去掉队列中的对应元素
                    $this->redis->lRem($ListKey, $v, 0);
                    break; //结束循环
                }
            }
        }

        $this->redis->lPush($ListKey, $server_id);
    }

    //获取最新服务器
    public function getNewServer($group_id, $package, $account, $rvtype = 0)
    {
        $key = $group_id;

        if ($rvtype) {
            $key .= '_1'; //带1 代表为提审的最新服
        }

        $server_id = $this->getNewServerId($key, $rvtype);

        $server = $this->getServerById($server_id, $package, $account);

        return $server;
    }

    //获取最新服务器id
    public function getNewServerId($group_id, $rvtype)
    {
        //获取最新的ServerId
        if (!$this->redis->hExists($this->newServerKey, $group_id)) {
            $server_id = (new ServerModel)->getNewServer($group_id, $rvtype);
            if ($server_id) {
                $this->redis->hSet($this->newServerKey, $group_id, $server_id);
            }
        } else {
            $server_id = $this->redis->hGet($this->newServerKey, $group_id);
        }

        return $server_id;
    }

    //获取分区数据
    public function getZone()
    {
        $zone = [];
        if (!$this->redis->exists($this->zoneKey)) {
            $zone = (new ZoneModel())->getZone(array(), array('id' , 'group_id', 'name' , 'sort', 'cdn_url'));
            if (!empty($zone)) {
                foreach ($zone as $v) {
                    $this->redis->hSet($this->zoneKey, $v['id'], msgpack_pack($v));
                }
            }
        }

        $zone = $this->redis->hGetAll($this->zoneKey);
        foreach ($zone as &$v) {
            $v = msgpack_unpack($v);
        }

        return $zone;
    }

    public function channelIdExists($channel)
    {
        //查询键是否存在 不存在直接生成channelKey
        if (!$this->redis->exists($this->channelSetKey)) {
            //查询mysql
            $ChannelModel = new ChannelModel();

            $ChannelMysqlSet = $ChannelModel->getChannel([], 'channel_id');

            if (!empty($ChannelMysqlSet)) {

                $ChannelSet = [];
                foreach ($ChannelMysqlSet as $val) {
                    array_push($ChannelSet, $val['channel_id']);
                }

                $this->redis->sAddArray($this->channelSetKey, $ChannelSet);
            }
        }

        if (!$this->redis->sIsMember($this->channelSetKey, $channel)) {
            //查询mysql
            empty($ChannelModel) && $ChannelModel = new ChannelModel();
            $rs = $ChannelModel->getChannel($channel, ['channel_id']);
            if (!empty($rs) && $rs['channel_id']) {
                $this->addChannelIdSet($channel);
            }
        }

        return $this->redis->sIsMember($this->channelSetKey, $channel);

    }

    public function addChannelIdSet($channel)
    {
        if (!$this->redis->sIsMember($this->channelSetKey, $channel)) {
            $this->redis->sAdd($this->channelSetKey, $channel);
        }
    }

    public function GroupIdExists($group_id)
    {
        //查询键是否存在 不存在直接生成channelKey
        if (!$this->redis->exists($this->groupSetKey)) {
            //查询mysql
            $ChannelgroupModel = new ChannelgroupModel();

            $ChannelgroupMysqlSet = $ChannelgroupModel->getChannelGroup([], 'id');

            if (!empty($ChannelgroupMysqlSet)) {

                $ChannelgroupSet = [];
                foreach ($ChannelgroupMysqlSet as $val) {
                    array_push($ChannelgroupSet, $val['id']);
                }

                $this->redis->sAddArray($this->groupSetKey, $ChannelgroupSet);
            }
        }

        if (!$this->redis->sIsMember($this->groupSetKey, $group_id)) {
            //查询mysql
            empty($ChannelgroupModel) && $ChannelgroupModel = new ChannelgroupModel();
            $rs = $ChannelgroupModel->getChannelGroup($group_id, ['id']);
            if (!empty($rs) && $rs['id']) {
                $this->addGroupIdSet($group_id);
            }
        }

        return $this->redis->sIsMember($this->groupSetKey, $group_id);

    }

    public function addGroupIdSet($group_id)
    {
        if (!$this->redis->sIsMember($this->groupSetKey, $group_id)) {
            $this->redis->sAdd($this->groupSetKey, $group_id);
        }
    }

    //从Mysql读取包数据并写入Redis
    public function packageExists($val, $type = 'package_id')
    {
        $Model = new PackageModel();

        $package = [];
        if ($type == 'all') {
            $package = $Model->getPackage([], array('p.package_id as package_id', 'p.name as name', 'c.game_id as game_id', 'c.game_secret_key as secret_key', 'c.game_abbreviation as game_abbreviation', 'c.cdn_url as cdn_url', 'c.channel_id as channel', 'p.group_id as group_id', 'p.review_num as review_num', 'p.platform as os'));
            if (!empty($package)) {
                foreach ($package as $row) {
                    $this->redis->hSet($this->packageKey, (int)$row['package_id'], msgpack_pack($row));
                }
            }
        } else {
            if ($type == 'name') {
                $package = BaseCheckSession::getPackage($val);
            } else if ($type == 'package_id') {
                $package = BaseCheckSession::getPackageById($val);
            }
            if (!empty($package)) {
                $this->redis->hSet($this->packageKey, (int)$package['package_id'], msgpack_pack($package));
            }
        }

        return $package;
    }

    //Mysql改动package 修改redis Package
    public function setPackage($type, $row)
    {
        switch ($type) {
            case 1: //add
                $this->redis->hSet($this->packageKey, (int)$row['package_id'], msgpack_pack($row));
                break;
            case 2: //update
                //先删后add
                $package_id = (int)$row['package_id'];
                if ($this->redis->hExists($this->packageKey, $package_id)) {
                    $this->redis->hDel($this->packageKey, (int)$row['package_id']);
                }
                $this->redis->hSet($this->packageKey, $package_id, msgpack_pack($row));
                break;
            case 3: //delete
                $package_id = (int)$row['package_id'];
                if ($this->redis->hExists($this->packageKey, $package_id)) {
                    $this->redis->hDel($this->packageKey, (int)$row['package_id']);
                }
                break;
            default:
                break;
        }
    }

    //Mysql改到server 修改redis
    public function setServer($oldServerId, $newServerId)
    {
        //先删后add
        if ($this->redis->hExists($this->serverKey, $oldServerId)) {
            $this->redis->hDel($this->serverKey, $oldServerId);
        }

        $this->serverExists($newServerId);

        //刷新一次Group, 直接删掉
        $this->redis->del($this->groupKey);

        //最新服列表重新生成 直接删掉
        $this->redis->del($this->newServerKey);
    }

    //Mysql改到zone 修改redis
    public function setZone($type, $row = [])
    {
        switch ($type) {
            case 1: //add
                $this->redis->hSet($this->zoneKey, (int)$row['id'], msgpack_pack($row));
                break;
            case 2: //update
                //先删后add
                $id = (int)$row['id'];
                if ($this->redis->hExists($this->zoneKey, $id)) {
                    $this->redis->hDel($this->zoneKey, $id);
                }
                $this->redis->hSet($this->zoneKey, $id, msgpack_pack($row));
                break;
            case 3: //delete
                $this->redis->del($this->zoneKey);
                break;
            default:
                break;
        }
    }

    //从MySql读取渠道组服务器ID数据
    public function groupServerIdExists($group_id, $rvtype = 0)
    {
        $key = $group_id;

        if ($rvtype) {
            $key .= '_1';
        }

        $serverIdList = (new ServerModel())->getServerIdListBygroupId($group_id, $rvtype, $this->isWhite);

        if (!empty($serverIdList)) {
            $serverIdList = $serverIdList[$group_id];
            $this->redis->hSet($this->groupKey, $key, msgpack_pack($serverIdList));
        } else {
            $serverIdList = [];
        }

        return $serverIdList;
    }

    //从Mysql读取对应服务器数据 并写进redis
    public function serverExists($server_id)
    {
        $serverModel = new ServerModel();

        $where = [];
        $where['s.server_id'] = $server_id;

        if (!$this->isWhite) {
            $where['s.display'] = 1;
            $where['s.status::!='] = 0;
        }

        $server = $serverModel->getRow(array('s.server_id as server_id', 's.name as `name`', 's.num as num', 's.channel_num as channel_num', 'sc.login_host as login_host', 'sc.login_port as login_port', 's.status as status', 's.zone as zone', 's.mom_server as mom_server', 'sc.websocket_host as websocket_host', 'sc.websocket_port as websocket_port', 's.display as display'), $where);

        if (!empty($server)) {

            $server['cdn'] = (new ZoneModel())->getCdnUrl($server['zone']);

            if ($server['mom_server']) {
                $where['s.server_id'] = $server['mom_server'];
                $momServer = $serverModel->getRow(array('sc.login_host as login_host', 'sc.login_port as login_port'), $where);
                $server['login_host'] = $momServer['login_host'];
                $server['login_port'] = $momServer['login_port'];
            }

            $this->redis->hSet($this->serverKey, $server_id, msgpack_pack($server));

        }

        return $server;
    }

    /***
     * 服务器数据接口输出格式化
     * @param $row
     * @param $package
     * @param $account
     * @return array
     */
    private function serverFormat($row, $package, $account)
    {
        $time = time() + (5 * 60);

        return array(
            'name' => $row['name'],
            'server_id' => $row['server_id'],
            'server_num' => (int)$row['num'],
            'channel_num' => (int)$row['channel_num'],
            'server_addr' => $row['login_host'],
            'server_port' => (int)$row['login_port'],
            'status' => $this->isWhite ? 1 : (int)$row['status'],  //如果是白名单 都返回状态为1
            'zone' => $row['zone'],
            'tick' => (int)$time,
            'sign' => md5($account . $row['channel_num'] . $row['num'] . $time . $this->serverSignKey),
            'package' => (int)$package,
            'cdn' => trim($row['cdn']),
            'mom_server' => $row['mom_server']
        );
    }

    //清除服务器缓存
    public function delServerKey()
    {
        $this->redis->del(array($this->serverKey, $this->isWhiteServerKey, $this->groupKey, $this->isWhiteGroupKey, $this->packageKey, $this->zoneKey, $this->newServerKey, $this->isWhiteNewServerKey, $this->channelSetKey, $this->groupSetKey));
        //更新ip白名单
        $this->updateWhiteIp();
    }

    /***
     * 检查是否为白名单
     * @return bool
     */
    public function checkIp()
    {
        $ip = $this->getClientIp(0, true);
        if (!$this->redis->sIsMember($this->whiteIpKey, $ip)) {
            return false;
        }

        return true;
    }

    /**
     * 获取客户端IP地址
     * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
     * @return mixed
     */
    public function getClientIp($type = 0, $adv = false)
    {
        $type = $type ? 1 : 0;
        static $ip = null;
        if (null !== $ip) {
            return $ip[$type];
        }
        if ($adv) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $pos = array_search('unknown', $arr);
                if (false !== $pos) {
                    unset($arr[$pos]);
                }
                $ip = trim($arr[0]);
            } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip = $long ? [$ip, $long] : ['0.0.0.0', 0];
        return $ip[$type];
    }

    public function addWhiteIp($ip)
    {
        if (!$this->redis->sIsMember($this->whiteIpKey, $ip)) {
            $this->redis->sAdd($this->whiteIpKey, $ip);
        }
    }

    public function delWhiteIp($ip)
    {
        if ($this->redis->sIsMember($this->whiteIpKey, $ip)) {
            $this->redis->sRem($this->whiteIpKey, $ip);
        }
    }

    public function updateWhiteIp()
    {
        if ($this->redis->exists($this->whiteIpKey)) {
            $this->redis->del($this->whiteIpKey);
        }

        $WhiteIpModel = new WhiteipModel();

        $fields = array('ip');
        $rs = $WhiteIpModel->getRows($fields, ['type' => 0]);
        $whiteIps = [];

        if (!empty($rs)) {

            foreach ($rs as $val) {
                array_push($whiteIps, $val['ip']);
            }

            $this->redis->sAddArray($this->whiteIpKey, $whiteIps);
        }
    }
}