$(document).ready(function() {
    var tips = [''];
    $('.condType').change(function(event) {
        if ($(this).val() == 1) {
            $('#auto').prop('checked', true);
        }
    });
    $('.week-block').delegate('.week-input', 'click', function(event) {
        $(this).prop('checked', true);
        $(this).parent().addClass('week-block-checked').siblings('label').removeClass('week-block-checked');
    });
    $('#auto').click(function(event) {
        if ($(this).prop('checked')) {
            if ($('.condType').val() != 2) {
                $('.condArea').removeClass('hidden');
            }
            $('.auto .addAuto').val(1);
        } else {
            $('.auto .addAuto').val(2);
            $('.condArea').addClass('hidden');
        }
    });
    $('.timeWidget .hidden input').attr('disabled', 'disabled');
    $('.areaWidget .hidden input').attr('disabled', 'disabled');
    $('input[name="item_num[]"]').css('width', '75px');
    $('.condType').change(function(event) {
        if ($(this).val() == 0) { // 自定义时间
            if ($('#act_type').val() != 1 && $('#act_type').val() != 2) {
                $('.timeTips').removeClass('hidden').html($('.timeTips').data('otips'));
                $('.auto').show().find('input').attr('checked', false);
            } else {
                $('.auto').hide().find('input').attr('checked', false);
                $('.timeTips').addClass('hidden');
            }
            $('.auto .addAuto').remove();
            $('input[name="condType[type]"]').removeAttr('disabled');
            $('.timeWidget li:eq(0)').removeClass('hidden').find('input').removeAttr('disabled').end().siblings().addClass('hidden').find('input').attr('disabled', 'disabled');
        } else if ($(this).val() == 1) { // 开服时间
            $('.auto').show();
            $('.timeTips').removeClass('hidden');
            $('.auto .addAuto').remove();
            $('.timeWidget li:last').removeClass('hidden').find('input').removeAttr('disabled').end().siblings().addClass('hidden').find('input').attr('disabled', 'disabled');
        } else if ($(this).val() == 2) { // 定时周期
            // $('#auto').prop('value', '2');
            // $('.auto').find('input').attr('checked', true);
            $('.auto').append('<input type="hidden" value="2" name="auto" class="addAuto" />');
            $('.timeTips').data('otips', $('.timeTips').html()).html('(当前服务器条件与下方区服任选其一)');
            $('.condArea').addClass('hidden');
            $('input[name="condType[type]"]').attr('disabled', 'disabled');
            $('.timeWidget li:last').removeClass('hidden').find('input').removeAttr('disabled').end().siblings().addClass('hidden').find('input').attr('disabled', 'disabled');
        }
    });
    $('.areaType').change(function(event) {
        $('.areaWidget li:eq(' + $(this).val() + ')').removeClass('hidden').find('input').removeAttr('disabled').end().siblings().addClass('hidden').find('input').attr('disabled', 'disabled');
    });
    // first type  tab
    $('.actType').click(function(event) {
        if ($('form input[name="id"]').val() == '') {
            $(this).addClass('cur').siblings().removeClass('cur');
            $('#act_type').val($(this).attr('data-type'));
            $tabs.tabs("url", 1, "admin.php?ctrl=activity&act=wonderfulEdit&type=1002&act_type=" + $('#act_type').val());
            $tabs.tabs("load", 1);
        }
    });
    // activity tab
    $('.aType').click(function(event) {
        if ($(this).attr('data-type') != 0) {
            $tabs.tabs("url", 1, "admin.php?ctrl=activity&act=wonderfulEdit&type=" + $(this).attr('data-type') + "&act_type=" + $('#act_type').val());
            $tabs.tabs("load", 1);
        }
    });
    // channel
    $('.showChanel').click(function(event) {
        $(this).parent('td').next().children().toggle();
    });
    // search item
    $(".search_item").autocomplete({
        source: "admin.php?ctrl=mail&act=search_item",
        minLength: 2,
        select: function(event, ui) {
            var id = ui.item.id;
            var name_elem = $(this);
            $.get("admin.php?ctrl=mail&act=get_item_color&item_id=" + id, function(data) {
                if (data != "[]") {
                    var data = JSON.parse(data);
                    var num_elem = name_elem.parent().next().children();
                    name_elem.parent().next().next().children().val(data.color);
                    if (data.bind !== undefined)
                        name_elem.parent().next().next().next().children().val(data.bind);

                    if (data.overlap === 1) {
                        // num_elem.prop('readonly', true)
                        // num_elem.val("1")
                    } else {
                        num_elem.prop('readonly', false)
                        num_elem.bind('keypress keyup', {
                            max: data.overlap
                        }, filterNum)
                        num_elem.focus()
                    }
                }
            });
        }
    });
    var options = $(".search_item").autocomplete("option");
    var item = $('#activity-level .body-item tr:eq(0)').html();
    var itemWidget = $('.itemTable .itemWidget').html();
    var timepickerlang = {
        timeText: '时间',
        hourText: '小时',
        minuteText: '分钟',
        currentText: '现在',
        closeText: '确定'
    }
    $('.datetimepicker').datetimepicker(timepickerlang);
    $('table').delegate('.itemTable .addItem', 'click', function(event) {
        $(this).parent().parent().parent().next('tbody').append('<tr>' + itemWidget + '</tr>');
        $(".search_item").autocomplete(options)
        event.stopPropagation();
    });
    $('.add-rows').click(function(event) {
        $('#activity-level .body-item').append('<tr>' + item + '</tr>');
        $('#activity-level tbody:first-child:last .search_item').autocomplete(options);
    });
    $('td').delegate('.del-rows', 'click', function(event) {
        if ($('#activity-level tbody:first-child').length > 1) {
            $(this).parent().parent().remove();
        }
    });
    var tpl = '';

    // post from
    // check form
    $('#singleCharge-form').submit(function(event) {
        var cur_type = $('#cur_type').val();
        var flag = true;
        if ($('input[name="name"]', this).val() == '') {
            alert('活动名称配置有误');
            return false;
        }
        if ($('#auto:checked').val() != 1) {
            var timeFlag = !$('input[name="sTime"]:enabled', this).val() || !$('input[name="eTime"]:enabled', this).val();
            var condFlag = !$('input[name="condType[cond][]"]:enabled:eq(0)', this).val() || !$('input[name="condType[cond][]"]:enabled:eq(1)', this).val();
            if (timeFlag  && condFlag　　) {
                alert('活动时间配置有误');
                return false;
            }
        } else {
            if ($('#condType').val() == 2) {
                if (!$('input[name="condType[cond][]"]:enabled:checked:eq(0)', this).val() || !$('input[name="condType[cond][]"]:enabled:eq(7)', this).val()) {
                    alert('活动时间配置有误');
                    return false;
                }
            } else {
                var timeFlag = !$('input[name="sTime"]:enabled', this).val() || !$('input[name="eTime"]:enabled', this).val();
                var condFlag = !$('input[name="condType[cond][]"]:enabled:eq(1)', this).val();
                if ((timeFlag && condFlag) ) {
                    alert('活动时间配置有误');
                    return false;
                }
            }
        }
        if (cur_type == 1012 || cur_type == 1019 || cur_type == 1021) {
            $('input[name="amount[]"]', this).each(function(index, el) {
                if ($(this).val() == '' || $(this).parent().next().find('input').val() == '' ) {
                    alert('档次内容配置有误');
                    flag = false;
                    return false;
                }
            });
        } else {
            $('input[name="amount[]"]', this).each(function(index, el) {
                if ($(this).val() == '') {
                    alert('档次内容配置有误');
                    flag = false;
                    return false;
                }
            });
        }
        if (flag) {
            $('input[name="item_name[]"]', this).each(function(index, el) {

                if ( ($(this).val() != '') !== ($(this).parent().next().find('input').val() != '') ) {
                    alert('档次物品配置有误');
                    flag = false;
                    return false;
                }
            });
        }
        if( flag ){
            if ($('textarea[name="desc"]', this).val() == '') {
                alert('活动描述配置有误');
                return false;
            }
        }
        if( flag ){
            var channel = [];
            $('input[name="channel[]"]:checked', this).each(function(index, el) {
                channel.push($(this).val());
            });
            var server = [];
            if ($('#auto:checked').val() != 0) {
                $('.server1', this).each(function(index, el) {
                    if ($(this).prop('checked')) {
                        server.push($(this).val());
                    }
                });
                if ($('.condType').val() == 2) {
                    var areaCondFlag = $('input[name="condType[cond][]"]:enabled:eq(8)').val();
                } else {
                    var areaCondFlag = $('input[name="condType[cond][]"]:enabled:eq(0)').val();
                }
                if (server.length < 1 && !areaCondFlag) {
                    alert('请勾选需要发送的区服');
                    return false;
                }
            }
        }

        var url = 'admin.php?ctrl=activity&act=wonderfulSave&type=' + $('#cur_type').val();
        console.log(url);
        var data = $(this).serialize() + "&server=" + server + "&channel=" + JSON.stringify(channel);
        if ( $('#cur_type').val() != '1014' && $('#cur_type').val() != '1015') {
            url += '&cut=1';
            var itemSlice = [];
            $('.itemTable').each(function(index, el) {
                var i = 0;
                $('input[name="item_name[]"]', this).each(function(index, el) {
                    if ($(this).val() != '') {
                        i++;
                    }
                });
                itemSlice.push(i);
            });
            data += "&itemSlice=" + JSON.stringify(itemSlice);
        }
        if ($('#cur_type').val() == '1004') {
            var brocast = [];
            $('.brocast').each(function(index, el) {
                if ($(this).prop('checked')) {
                    brocast.push(1);
                } else {
                    brocast.push(0);
                }
            });
            data += '&brocast=' + JSON.stringify(brocast);
        }
        // check end
        // var data =
        if (flag) {
            // form post
            $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                })
                .done(function(response) {
                    if (response.state == true) {
                        $.dialog.alert(response.msg);
                        $tabs.tabs("select", 0);
                    } else {
                        $.dialog.alert(response.msg);
                    }
                })
        }
        return false;
    });
    $('.sync').click(function(event) {
        var id = $('#singleCharge-form input[name="id"]').val();
        $.ajax({
                url: 'admin.php?ctrl=activity&act=syncActivity',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    id: id
                },
            })
            .done(function(response) {
                if (response.state == true) {
                    $.dialog.alert(response.msg);
                    $tabs.tabs("select", 0);
                } else {
                    $.dialog.alert(response.msg);
                }
            })
        return false;
    });

});

function filterNum(e) {
    if (e.which < 48 || e.which > 57) {
        e.preventDefault()
        return
    }

    if (parseInt(this.value) > e.data.max) {
        e.preventDefault()
        this.value = this.value.slice(0, -1)
        alert("最大允许值: " + e.data.max)
    }
}

function checknum(elem) {
    if (elem.value && elem.value != parseInt(elem.value))
        elem.value = ''
}
