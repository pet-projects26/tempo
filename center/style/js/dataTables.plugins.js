$.fn.dataTableExt.oApi.fnMultiFilter = function(oSettings, oData) {
	if( $.isEmptyObject(oSettings.aanFeatures)){
		oSettings.aanFeatures['r'] = [];
	}
    for (var i = 0, iLen = oSettings.aoColumns.length; i < iLen; i++) {
        oSettings.aoPreSearchCols[i].sSearch = oData[i];
    }
    oSettings._iDisplayStart = 0;
    this.oApi._fnDraw(oSettings);
};
$.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
this.fnClearTable(this);
this.oApi._fnProcessingDisplay(oSettings, true);
var that = this;
$.getJSON(oSettings.sAjaxSource, null, function (json) {
    for (var i = 0; i < json.aaData.length; i++) {
        that.oApi._fnAddData(oSettings, json.aaData[i]);
    }
    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    that.fnDraw(that);
    that.oApi._fnProcessingDisplay(oSettings, false);
});
}

$.fn.dataTableExt.oApi.fnReloadAjax2 = function (oSettings,oData) {
this.fnClearTable(this);
this.oApi._fnProcessingDisplay(oSettings, true);
var that = this;
$.post(oSettings.sAjaxSource, oData, function (json) {
    for (var i = 0; i < json.aaData.length; i++) {
        that.oApi._fnAddData(oSettings, json.aaData[i]);
    }
    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    that.fnDraw(that);
    that.oApi._fnProcessingDisplay(oSettings, false);
},'json');
}
