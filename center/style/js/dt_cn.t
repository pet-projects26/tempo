{
	"sLengthMenu": "_MENU_",
	"sSearch": "",
    "sInfo": "当前数据为从第 _START_ 到第 _END_ 条数据；总共有 _TOTAL_ 条记录",
	"sInfoEmpty": "当前数据为从第 0 到第 0 总共有 0 条数据",
	"sInfoFiltered": "",
	"oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前一页",
                    "sNext": "后一页",
                    "sLast": "尾页"
                },

	"sZeroRecords": "没有检索到数据，请使用筛选按钮选择服务器，然后使用搜索按钮进行搜索",
	"sInfoEmtpy": "没有数据",
	"sProcessing": "<img src='style/images/loading.gif' />"
}
