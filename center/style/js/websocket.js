(function($) {


    $.config = {
        url: '', //链接地址
    };


    $.init=function(config) {
        this.config = config;
        return this;
    };


    /**
     * 连接webcocket
     */
    $.connect = function() {
        var protocol = (window.location.protocol == 'http:') ? 'ws:' : 'wss:';
        this.host = protocol + this.config.url;


        window.WebSocket = window.WebSocket || window.MozWebSocket;
        if(!window.WebSocket) { // 检测浏览器支持
            this.error('Error: WebSocket is not supported .');
            return;
        }
        this.socket = new WebSocket(this.host); // 创建连接并注册响应函数

        this.socket.binaryType = "arraybuffer";

        this.socket.onopen = function() {

        };

        this.socket.onmessage = function(message) {
            $.onMessage(message);
        };
        this.socket.onclose = function() {
            $.onclose();
            $.socket = null; // 清理
        };
        this.socket.onerror = function(errorMsg) {
            $.onerror(errorMsg);
        }
        return this;
    }


    /**
     * 自定义异常函数
     * @param {Object} errorMsg
     */
    $.error = function(errorMsg) {
        this.onerror(errorMsg);
    }


    /**
     * 消息发送
     */
    $.send = function(message) {
        if(this.socket) {
            this.socket.send(message);
            return true;
        }
        this.error('please connect to the server first !!!');
        return false;
    }


    $.close = function() {
        if(this.socket != undefined && this.socket != null) {
            this.socket.close();
        } else {
            this.error("this socket is not available");
        }
    }

    $.publish = function(opcode, tuple) {
        var content = null;
        var size = 0;

        if (tuple != null) {
            content = serializeMsgPack(tuple);
            size = content.byteLength;
        }

        var buffer =new Uint8Array(2 + 4 + size);
        var stream = new DataView(buffer.buffer);
        var offset = 0;
        stream.setUint16(offset, buffer.byteLength, true);
        offset += 2;
        stream.setUint32(offset, opcode, true);
        offset += 4;
        if (content != null) {
            buffer.set(content, offset);
        }

        this.send(buffer);

        console.log(buffer);

        return this;
    }

    /**
     * 消息回調
     * @param {Object} message
     */
    $.onMessage = function(evt) {

        var data = evt.data;
        var stream = new DataView(data);
        var offset = 0;
        var size = stream.getUint16(offset, true);
        offset += 2;
        var opcode = stream.getUint32(offset, true);
        offset += 4;
        var bytes = new Uint8Array(data, offset);
        var tuple = deserializeMsgPack(bytes);

        // console.log(tuple);
        if (tuple == 0) $.close();
    }

    $.decode = function(data) {
        var stream = new DataView(data);
        var offset = 0;
        var size = stream.getUint16(offset, true);
        offset += 2;
        var opcode = stream.getUint32(offset, true);
        offset += 4;
        var bytes = new Uint8Array(data, offset);
        var tuple = deserializeMsgPack(bytes);

        return tuple;
    }

    /**
     * 链接回调函数
     */
    $.onopen = function(a) {
        console.log(a);
    }


    /**
     * 关闭回调
     */
    $.onclose = function() {


    }


    /**
     * 异常回调
     */
    $.onerror = function() {



    }


})(ws = {});
