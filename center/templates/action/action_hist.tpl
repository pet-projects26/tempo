<style></style>
<div>
    <div style="margin-bottom: 10px">
        <form action="" method="post">
            <table class="itable itable-color">
                <tr>
                    <td style="width: 15%">选择服务器</td>
                    <td>
                        <select>
                            <option value="0">未选择</option>
                            <{foreach from=$servers key=sid item=msg name=foo}>
                            <option value="<{$msg.id}>"><{$msg.name}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>
                <tr></tr>
            </table>
            <button type="button" class="gbutton">查询</button>
        </form>
    </div>
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--显示所有功能开关情况-->
        <table class="itable itable-color">
            <thead>
            <tr></tr>
            <tr>
                <td colspan="2">
                    <label>名称</label>
                    <input type="text" value="" placeholder="请输入功能名称"/>
                    <button type="button" class="gbutton">查找</button>
                </td>
            </tr>
            <tr>
                <td width="10%">编号</td>
                <td width="20%">功能编号</td>
                <td width="20%">功能名称</td>
                <td width="10%">状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <!--展示信息-->
            </tbody>
        </table>
    </div>
</div>