<!--单选，显示所有渠道组-->
<!--显示查询框-->
<!--通过RPC请求服务端，得到被被关闭的功能项，table显示所有列表[开启][关闭]-->
<style>
    .all-checked {
        vertical-align: middle;
    }
    .select{
        background-color: #36ca95;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form class="fm" id="cgroup-edit-form">
        <div>
            <fieldset>
                <legend>功能开关列表</legend>
               <!--<span>
                   <input class="all-checked" type="checkbox" id="check-all"/>
                   <label for="check-all" class="check-all">全选</label>
               </span>-->
                <strong>当前功能开关数：<{$action_list|@count}></strong>
                <div style="line-height: 30px">
                    <!--功能开关列表-->
                    <{foreach from=$action_list item=msg name=foo key=k}>
                    <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                    <input checked="checked" class="all-checked" type="checkbox" name="action[]" value="<{$k}>"/>
                    <label class="all-checked"><{$msg}></label>
                    </span>,
                    <{/foreach}>
                </div>
            </fieldset>
        </div>
        <div>
            <table class="itable itable-color">
                <tr>
                    <td>功能开关key</td>
                    <td>
                        <input type="text" name="unview_key" value="<{$action_key}>" placeholder="请输入功能开关KEY">
                        <span style="color: #cc1e2f">*功能开列表未能找到时，请联系服务端获取KEY</span>
                    </td>
                </tr>
                <tr></tr>

                <tr>
                    <td width="10%">功能开关</td>
                    <td>
                        <select name="status">
                            <option <{if $status eq 2}>selected="selected"<{/if}> value="2">开启</option>
                            <option <{if $status eq 3}>selected="selected"<{/if}> value="3">关闭</option>
                        </select>
                    </td>
                </tr>
                <{include file='../plugin/channelGroup_server_edit.tpl' }>
            </table>
        </div>
        <button type="button" class="gbutton set-action">提交</button>
    </form>
</div>
<script>
    $(function(){
        //点选
        $(".action-are").bind('click',function(){
            var child = $(this).children('input.all-checked');
            if($(this).hasClass('select')){
                //选中状态下
                child.attr('checked',false);
                $(this).removeClass('select');
            }else{
                $(this).addClass('select');
                child.attr('checked',true);
            }
        });

        //全选
        $("input#check-all").change('click',function(){
            if($(this).prop('checked')){
                $("input.all-checked").attr('checked',true);
                $("span.action-are").addClass('select');
            }else{
                $("input.all-checked").attr('checked',false);
                $("span.action-are").removeClass('select');
            }
        });

        $(".set-action").click(function(event){
            var action = [];
            var unviewkey = $("input[name='unview_key']").val();
            $("input.all-checked").each(function(){
                if($(this).prop('checked')){
                    action.push($(this).val());
                }
            });
            if(action.length == 0 && !unviewkey){
                alert('请选择功能！');
                return false;
            }
            $.ajax({
                type:'post',
                url:'admin.php?&ctrl=action&act=handel',
                dataType: 'JSON',
                data:$("#cgroup-edit-form").serialize(),
                beforeSend:function(){
                    alert('确认修改')
                },
                success:function(data){
                    console.log(data);
                    $.dialog({
                        'title':data.title,
                        'max':false,
                        'min':false,
                        'content':data.html
                    });
                    $tabs.tabs('load' , 0);
                },
                error:function(){

                }
            })
        })
    })
</script>