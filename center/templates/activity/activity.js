// JavaScript Document

$(function(){
	
	var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
	 $('#start_time').datetimepicker(timepickerlang);
	 $('#end_time').datetimepicker(timepickerlang);

	//类型选择
	$('.aType').click(function(){
		$('.type-list ul li').removeClass('cur');
        $(this).addClass('cur');

        var aId  = $(this).attr('data-type');

       	$('#load').html();

       	if (aId == 1002) {
       		$('#add_time').hide();
       	} else {
       		$('#add_time').show();
       	}

       	$('#load').html();

       	var date = new Date();

       	var s = date.getSeconds();

       	var file = 'templates/activity/add_'+ aId +'.tpl' + '?time=' + s;
        $('#load').load(file);

       	$(this).attr("checked","checked");


	});

	
	//提交
	$('#manual-mail-form').submit(function(event){
		//档次
		var objGrade = $('input[name="grade[]"]');
		var res = ckeckCondition(objGrade, '充值档次');
		
		
		if (!res) {
			return false;
		}

		//条件
		
		var objAmount = $('input[name="amount[]"]');
		
		var res = ckeckCondition(objAmount, '领取条件')			
		if (!res) {
			return false;
		}
		
		// //物品ID
		// var objItemId = $('input[name="item_id[]"]');
		
		// var res = ckeckCondition(objItemId, '物品ID');
		
		
		if (!res) {
			return false;
		}
		
		
		// 物品数量
		var objItemCount = $('input[name="item_count[]"]');
		var res = ckeckCondition(objItemCount, '物品数量');
		
		if (!res) {
			return false;
		} 
		
		var title = $('input[name="title"]').val();
		if (title == '') {
			alert('请输入标题');
			return false;
		}
		
		// var start_time = $('input[name="start_time"]').val();
		// if (start_time == '') {
		// 	alert('请填入开始时间');
		// 	return false;
		// }
		
		// var end_time  = $('input[name="end_time"]').val();
		
		// if(end_time == '') {
		// 	alert('请输入结束时间');
		// 	return false;
		// } 

		var act_id = $('li.cur').attr('data-type');

		if (typeof act_id == 'undefined' ) {
			alert('请选择活动类型');
			return false;
		}

		$('input[name="act_id"]').val(act_id);


		
		$.ajax({
			url: 'admin.php?ctrl=activity&act=save',
			type: 'POST',
			dataType: 'JSON',
			data: $(this).serialize()
		}).done(function(data){
			$.dialog.tips(data.msg);
		});
		
		return false;
    });
  
});


//检查条件
function ckeckCondition(obj, tips) {
	var mark = true;
	
	var len = obj.length;
	if (obj.length == 0) {
		return true;
	}
	
	obj.each(function(index) {
		var val = $(this).val();
		
		var i = index + 1;
		
		if (val == '') {
			alert('请输入' + 1 +  '的' + tips );
			mark = false;
			return false;
		}
		
		if (isNaN(val)) {
			alert('第' + 1 +  '的'+ tips +'不是整数，请输入整数' );
			mark = false;
			return false;
		}
	});
	
	return mark;
}

