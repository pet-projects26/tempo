// JavaScript Document
$(function() {
	//获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
		$( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //绑定时间
	addTime();


	//添加档次
	$('#addgrade').die('click');
	$("#addgrade").live('click', function(){
		var num = $(this).attr('num');
		num = parseInt(num);
		num = num + 1;
		
		var grade_name = "grade["+ num + "]";
		
		var amount_name = "amount[" + num +"][0]";
		
			var addgrade = '<tr grade_num=' + num +'> ' +
					'<td style="width:150px;">' +
						'<label>时间</label> ' +
						'<input type="text" class="start_time" name="' + grade_name +'[start]" placeholder="开始时间">' + 
						'~' + 
						'<input type="text" class="end_time" name="' + grade_name +'[end]" placeholder="结束时间">' + 

						'<input type="button" value="添加条件" class="gbutton c-add-reward" add_grade_num='+ num +'  add_amount_num="0" > ' +
						'<input type="button" value="删除该档次" class="gbutton del-level" >' + 
					'</td> ' + 
					'<td> '  +
						'<table style="width:100%;" class= "table-activity-reward"> ' +
							'<thead>' +
								'<tr>' +
									'<th style="width:80px;">条件</th>' + 
									'<th>奖励</th>' + 
									'<th>删除</th>' + 
								'</tr> ' + 
							'</thead>'   +
							
							'<tbody class="body-item"> ' + 
								'<tr>' +
									
									'<td style="width:100px;">' +
										'<input type="text" class="amount" name="'+ amount_name +'" value="" placeholder="额度" /> ' +
									'</td>' +
									
									'<td>' + 
										'<table> ' + 
											'<tbody>' +
												'<tr>' +
													'<td>' +
														'<table class="itemTable">' + 
															'<thead>' +
																'<tr>' + 
																	'<td width="">物品ID</td>' +
																	'<td width="">数量</td>' +
																	'<td width="">角色</td>'  +
																	'<td width="">操作</td>' +
																'</tr>' +
															'</thead>' + 
															'<tbody> </tbody> ' + 
														'</table>' +
													'</td>' + 
													'<td></td>' + 
												'</tr>' +
											'</tbody>' +
										'</table>' +
										'<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num='+ num +' >' + 
									'</td>' + 
									
									'<td>' +
										'<input type="button" value="删除该条件" class="gbutton delitem" >' +
										'<input type="button" value="复制该条件" class="gbutton c-copy-reward" >' +
									'</td>' +
								'</tr> ' + 
							'</tbody> ' + 
						'</table>' + 
					'</td>' + 
				'</tr>';		
		$(this).parent('td').parent('tr').before(addgrade);
		$(this).attr('num', num);

		//添加时间
		addTime();
	});
	
	
	
	//添加充值条件
	$('.c-add-reward').die('click');
	$('.c-add-reward').live('click', function(){
		var grade_num  = $(this).attr('add_grade_num');
		grade_num = parseInt(grade_num);
		
		var amount_num = $(this).attr('add_amount_num');
		amount_num = parseInt(amount_num);
		amount_num = amount_num + 1;
		
		var name = "amount["+ grade_num +"]["+ amount_num +"]";
		var  add = '<tr>' +
					'<td style="width:100px;"> ' + 
						'<input type="text" class="amount" name="'+ name +'" value="" placeholder="额度"> ' +
					'</td>' +
					'<td>' + 
						'<table>' +
							'<tbody> ' +  
								'<tr>' + 
									'<td>' +
										'<table class="itemTable">' +
											'<thead>' +
												'<tr>' + 
													'<td width="">物品ID</td>' +
													'<td width="">数量</td>' +
													'<td width="">职业</td>' + 
													'<td width="">操作</td>' +
												'</tr>' + 
											'</thead>' +
											
											'<tbody></tbody>' +
											
										'</table>' + 
									'</td>' +  
								'</tr>' +
							'</tbody>' + 
						'</table>' + 
						'<input type="button" value="添加物品条件" class="gbutton c-additem" add_amount_num="'+ amount_num +'"  add_grade_num="'+ grade_num +'" > ' +
					'</td>' + 
					
					'<td>' + 
						'<input type="button" value="删除该条件" class="gbutton del-reward" />' + 
						'<input type="button" value="复制该条件" class="gbutton c-copy-reward" />' + 
					'</td>'
					
				'</tr>';
				
		$(this).parent('td').next('td').children('.table-activity-reward').eq(0).append(add);
        $(this).attr('add_amount_num', amount_num);
    });
	

   
	
	//更先die，然后再重监听 
	$('.c-additem').die("click");
	$('.c-additem').live('click', function(){
		var add_grade_num = $(this).attr('add_grade_num');
		add_grade_num = parseInt(add_grade_num);
		
		var add_amount_num = $(this).attr('add_amount_num');
		add_amount_num = parseInt(add_amount_num);
		
		
		var item_id_name =  "item_id["+ add_grade_num +"]["+ add_amount_num +"][]";
		var item_count_name = "item_count[" + add_grade_num +"]["+ add_amount_num +"][]";
		var career_name = "career["+ add_grade_num +"]["+ add_amount_num +"][]";
		
		var itemWidget = '<tr class="itemWidget">' +
									'<td>' +
										'<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  + 
									'</td>' +
									
									'<td>' +
										'<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
									'</td>' + 
									
									'<td>' + 
										'<select name="'+ career_name +'">' + 
											'<option value="0">通用</option>' + 
											'<option value="1">刀剑师</option>' + 
											'<option value="2">羽翎师</option>' +
										'<select>' + 
									'</td>' +
									
									'<td>' + 
											'<input type="button" value="删除" class="gbutton delitem" title="删除该行">' + 
									'</td>' +								
					'</tr>';
					
		//alert($(this).prev('table').length);
		$(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget); 
		
		//物品绑定
		$( ".item_id" ).autocomplete({
            source: _goods
        });

	});
	
	//删除某个物品
	$('.delitem').die('click');
	$('.delitem').live('click', function() {
		$(this).parent().parent().remove();
	});
	
	//删除某个条件

	$('.del-reward').die('click');
	$('.del-reward').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});

	//复制该条件
	$('.c-copy-reward').die('click');
	$('.c-copy-reward').live('click', function() {
		//查找出最大条件
		
		var obj = $(this).parent('td').parent('tr').parent('tbody').parent('table').parent('td').prev('td').find('.c-add-reward');

		var add_grade_num = obj.attr('add_grade_num');

		var add_amount_num = obj.attr('add_amount_num');
		
		var num = parseInt(add_amount_num) + 1;

		var replace = "["+ add_grade_num + "]["+ num +"]";

		obj.attr('add_amount_num', num);

		var oTr = $(this).parent('td').parent('tr');
		var clone = oTr.clone();
		//替换相对应名称
		clone.find('.amount').attr('name', 'amount' + replace);
		clone.find('.item_id').attr('name', 'item_id' + replace + '[]');
		clone.find('.item_count').attr('name', 'item_count' + replace + '[]');
		clone.find('select').attr('name', 'career' + replace + '[]');

		$(this).parent('td').parent('tr').parent('tbody').append(clone);

		$( ".item_id" ).autocomplete({
            source: _goods
        });
	});
	
	//删除该档充值
	$('.del-level').die('click');
	$('.del-level').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});
	
	
});


//绑定时间
function addTime() {
	var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
	$('.start_time').datetimepicker(timepickerlang);
	$('.end_time').datetimepicker(timepickerlang);
}

