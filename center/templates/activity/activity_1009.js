// JavaScript Document
$(function(){

	//获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
         //物品绑定
		$( ".config_item_id" ).autocomplete({
	        source: _goods
	    });

	    $( ".item_id" ).autocomplete({
	        source: _goods
	    });
    },'json');


   

	//添加档次
	$('#addgrade').die('click');
	$("#addgrade").live('click', function(){
		var num = $(this).attr('num');
		num = parseInt(num);
		num = num + 1;
		
		var grade_name = "grade["+ num + "]";
		
		var amount_name = "amount[" + num +"][0]";
		
			var addgrade = '<tr grade_num=' + num +'> ' +
					'<td style="width:150px;">' +
						'<label>充值档次 </label> ' +
						'<input type="text" id="grade" name="' + grade_name +'">' + 
						'<input type="button" value="添加条件" class="gbutton c-add-reward" add_grade_num='+ num +'  add_amount_num="0" > ' +
						'<input type="button" value="删除该档次" class="gbutton del-level" >' + 
					'</td> ' + 
					'<td> '  +
						'<table style="width:100%;" class= "table-activity-reward"> ' +
							'<thead>' +
								'<tr>' +
									'<th style="width:80px;">条件</th>' + 
									'<th>奖励</th>' + 
									'<th>删除</th>' + 
								'</tr> ' + 
							'</thead>'   +
							
							'<tbody class="body-item"> ' + 
								'<tr>' +
									
									'<td style="width:100px;">' +
										'<input type="text" class="amount" name="'+ amount_name +'" value="" /> ' +
									'</td>' +
									
									'<td>' + 
										'<table> ' + 
											'<tbody>' +
												'<tr>' +
													'<td>' +
														'<table class="itemTable">' + 
															'<thead>' +
																'<tr>' + 
																	'<td width="">物品ID</td>' +
																	'<td width="">数量</td>' +
																	'<td width="">角色</td>'  +
																	'<td width="">操作</td>' +
																'</tr>' +
															'</thead>' + 
															'<tbody> </tbody> ' + 
														'</table>' +
													'</td>' + 
													'<td></td>' + 
												'</tr>' +
											'</tbody>' +
										'</table>' +
										'<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num='+ num +' >' + 
									'</td>' + 
									
									'<td>' +
										'<input type="button" value="删除该条件" class="gbutton delitem" >' +
										'<input type="button" value="复制该条件" class="gbutton c-copy-reward" >' +
									'</td>' +
								'</tr> ' + 
							'</tbody> ' + 
						'</table>' + 
					'</td>' + 
				'</tr>';		
		$(this).parent('td').parent('tr').before(addgrade);
		$(this).attr('num', num);
	});
	
	
	
	//添加充值条件
	$('.c-add-reward').die('click');
	$('.c-add-reward').live('click', function(){
		var grade_num  = $(this).attr('add_grade_num');
		grade_num = parseInt(grade_num);
		
		var amount_num = $(this).attr('add_amount_num');
		amount_num = parseInt(amount_num);
		amount_num = amount_num + 1;
		
		var name = "amount["+ grade_num +"]["+ amount_num +"]";
		var  add = '<tr>' +
					'<td style="width:100px;"> ' + 
						'<input type="text" class="amount" name="'+ name +'" value=""> ' +
					'</td>' +
					'<td>' + 
						'<table>' +
							'<tbody> ' +  
								'<tr>' + 
									'<td>' +
										'<table class="itemTable">' +
											'<thead>' +
												'<tr>' + 
													'<td width="">物品ID</td>' +
													'<td width="">数量</td>' +
													'<td width="">职业</td>' + 
													'<td width="">操作</td>' +
												'</tr>' + 
											'</thead>' +
											
											'<tbody></tbody>' +
											
										'</table>' + 
									'</td>' +  
								'</tr>' +
							'</tbody>' + 
						'</table>' + 
						'<input type="button" value="添加物品条件" class="gbutton c-extra-additem" add_amount_num="'+ amount_num +'"  add_grade_num="'+ grade_num +'" > ' +
					'</td>' + 
					
					'<td>' + 
						'<input type="button" value="删除该条件" class="gbutton del-reward" />' + 
						'<input type="button" value="复制该条件" class="gbutton c-copy-reward" />' + 
					'</td>'
					
				'</tr>';


				
		var obj = $(this).parent('td').next('td').children('table').children('tbody').children('tr').eq(1).children('td').eq(1).children('table').children('tbody').append(add);
        $(this).attr('add_amount_num', amount_num);
    });
   
	
	//更先die，然后再重监听 
	$('.c-additem').die("click");
	$('.c-additem').live('click', function(){
		var add_grade_num = $(this).attr('add_grade_num');
		add_grade_num = parseInt(add_grade_num);
		
		
		var item_id_name =  "config[item_id]["+ add_grade_num +"][]";
		var item_count_name = "config[item_count][" + add_grade_num +"][]";
		var career_name = "config[career]["+ add_grade_num +"][]";

		var item_weight_name = "config[weight]["+ add_grade_num +"][]";
		var item_rare_name = "config[rare]["+ add_grade_num +"][]";
		
		var itemWidget = '<tr class="itemWidget">' +
									'<td>' +
										'<input style="width:200px" type="text" name="'+ item_id_name +'" class="config_item_id" autocomplete="on">'  + 
									'</td>' +
									
									'<td>' +
										'<input type="text" name="'+ item_count_name +'" value="1" class="config_item_count">' +
									'</td>' + 

									'<td>' +
										'<input type="text" name="'+ item_weight_name +'" value="1" class="config_item_weight">' +
									'</td>' + 

									'<td>' +
										'<input type="text" name="'+ item_rare_name +'" value="1" class="config_item_rare">' +
									'</td>' + 

									
									'<td>' + 
										'<select name="'+ career_name +'" class="config_item_career">' + 
											'<option value="0">通用</option>' + 
											'<option value="1">刀剑师</option>' + 
											'<option value="2">羽翎师</option>' +
										'<select>' + 
									'</td>' +
									
									'<td>' + 
											'<input type="button" value="删除" class="gbutton delitem" title="删除该行">' + 
									'</td>' +								
					'</tr>';
					
		//alert($(this).prev('table').length);
		$(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget); 
		
		//物品绑定
		$( ".config_item_id" ).autocomplete({
            source: _goods
        });

	});



	//复制该条件
	$('.c-copy-level').die('click');
	$('.c-copy-level').live('click', function() {
		//查找出最大条件
		var obj = $('#add_grade_num');

		var add_grade_num = obj.attr('add_grade_num');
		add_grade_num= parseInt(add_grade_num) + 1;
		obj.attr('add_grade_num', add_grade_num);
		//配置奖励
		var replace = "["+ add_grade_num + "]";

		//额外奖励
		var oTr = $(this).parent('td').parent('tr');
		var clone = oTr.clone();

		clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

		//配置奖励
		clone.find('.config_item_id').attr('name', 'config[item_id]' + replace + '[]');
		clone.find('.config_item_count').attr('name', 'config[item_count]' + replace + '[]');
		clone.find('.config_item_weight').attr('name', 'config[weight]' + replace + '[]');
		clone.find('.config_item_rare').attr('name', 'config[rare]' + replace + '[]');
		clone.find('.config_item_career').attr('name', 'config[career]' + replace + '[]');

		var str = "$1["+ add_grade_num +"]";

		var reg = /(\w+)\[\d\]/g; 

		//额外奖励
		var OitemId = clone.find('.item_id');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		//额外奖励
		var OitemId = clone.find('.item_count');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		//额外奖励
		var OitemId = clone.find('.amount');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		//额外奖励
		var OitemId = clone.find('select');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		$(this).parent('td').parent('tr').parent('tbody').append(clone);

		  //物品绑定
		$(".config_item_id").autocomplete({
	        source: _goods
	    });

	    $(".item_id").autocomplete({
	        source: _goods
	    });
	});



	//更先die，然后再重监听 
	$('.c-extra-additem').die("click");
	$('.c-extra-additem').live('click', function(){
		var add_grade_num = $(this).attr('add_grade_num');
		add_grade_num = parseInt(add_grade_num);
		
		var add_amount_num = $(this).attr('add_amount_num');
		add_amount_num = parseInt(add_amount_num);
		
		
		var item_id_name =  "item_id["+ add_grade_num +"]["+ add_amount_num +"][]";
		var item_count_name = "item_count[" + add_grade_num +"]["+ add_amount_num +"][]";
		var career_name = "career["+ add_grade_num +"]["+ add_amount_num +"][]";

		var itemWidget = '<tr class="itemWidget">' +
									'<td>' +
										'<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  + 
									'</td>' +
									
									'<td>' +
										'<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
									'</td>' + 
									
									'<td>' + 
										'<select name="'+ career_name +'">' + 
											'<option value="0">通用</option>' + 
											'<option value="1">刀剑师</option>' + 
											'<option value="2">羽翎师</option>' +
										'<select>' + 
									'</td>' +
									
									'<td>' + 
											'<input type="button" value="删除" class="gbutton delitem" title="删除该行">' + 
									'</td>' +								
					'</tr>';
					
		//alert($(this).prev('table').length);
		$(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget); 
		
		//物品绑定
		$( ".item_id" ).autocomplete({
            source: _goods
        });

	});
	
	//删除某个物品
	$('.delitem').die('click');
	$('.delitem').live('click', function() {
		$(this).parent().parent().remove();
	});
	
	//删除某个条件
	$('.del-reward').die('click');
	$('.del-reward').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});

	//删除该档充值
	$('.del-level').die('click');
	$('.del-level').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});
	
	
});

