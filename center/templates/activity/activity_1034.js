//javascript document
$(function(){
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');


    //小礼包添加物品条件
    $('.c-additem').die("click");
    $('.c-additem').live('click', function(){

        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);

        var item_id_name =  "config[item_id]["+ add_grade_num +"][]";
        var item_count_name = "config[item_count][" + add_grade_num +"][]";
        var career_name = "config[career]["+ add_grade_num +"][]";
        var itemWidget = '<tr class="itemWidget">' +
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="config_item_id" autocomplete="on">'  +
            '</td>' +
            '<td>' +
            '<input style="width:100px" type="number" name="'+ item_count_name +'" value="1" class="config_item_count">' +
            '</td>' +
            '<td>' +
            '<select name="'+ career_name +'" class="config_item_career">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀</option>' +
            '<option value="2">弓</option>' +
            '<option value="3">法</option>' +
            '<select>' +
            '</td>' +

            '<td>' +
            '<input type="button" value="删除道具" class="gbutton delitem" title="删除道具">' +
            '<input type="button" value="复制道具" class="gbutton copyitem" title="复制道具">'+
            '</td>' +
            '</tr>';

        //alert($(this).prev('table').length);
        $(this).prev('table').eq(0).children('tbody').append(itemWidget);
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

    });

    //大礼包添加物品条件
    $(".b-additem").die();
    $(".b-additem").live('click',function(){

        var item_id_name =  "item_id[]";
        var item_count_name = "item_count[]";
        var career_name = "career[]";
        var itemWidget = '<tr class="itemWidget">' +
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="config_item_id" autocomplete="on">'  +
            '</td>' +

            '<td>' +
            '<input style="width:100px" type="number" name="'+ item_count_name +'" value="1" class="config_item_count">' +
            '</td>' +
            '<td>' +
            '<select name="'+ career_name +'" class="item_career">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀</option>' +
            '<option value="2">弓</option>' +
            '<option value="3">法</option>' +
            '<select>' +
            '</td>' +
            '<td>' +
            '<input type="button" value="删除道具" class="gbutton delitem" title="删除道具">' +
            '<input type="button" value="复制道具" class="gbutton copyitem" title="复制道具">'+
            '</td>' +
            '</tr>';

        //alert($(this).prev('table').length);
        $(this).prev('table').eq(0).children('tbody').append(itemWidget);
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });
    });

    //复制礼包
    $(".c-copy-level").die();
    $(".c-copy-level").live('click',function(){
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);
        //配置奖励
        var replace = "["+ add_grade_num + "]";

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        //配置奖励
        clone.find('.config_item_id').attr('name', 'config[item_id]' + replace + '[]');
        clone.find('.config_item_count').attr('name', 'config[item_count]' + replace + '[]');
        clone.find('.config_item_career').attr('name', 'config[career]' + replace + '[]');
        clone.find('.c-additem').attr('add_grade_num',add_grade_num);
        var str = "$1["+ add_grade_num +"]";

        var reg = /(\w+)\[\d+\]/g;

        //额外奖励
        var OitemId = clone.find('.item_id');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.item_count');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.amount');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });
        //VIP等级
        var OitemId = clone.find('.vip_level');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('select');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });
        //礼包名称
        var price_name = clone.find('.price_name');
        price_name.attr('name','config[price_name]['+add_grade_num+']');
        clone.find('.config_item_price_old').attr('name', 'config[price_old]' + replace);
        clone.find('.config_item_price_now').attr('name', 'config[price_now]' + replace);


        $(this).parent('td').parent('tr').parent('tbody').children('tr.normal-price').before(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //添加礼包
    $(".c-add-level").die();
    $(".c-add-level").live('click',function(){
        var obj = $("#add_grade_num");
        var grade_num = obj.attr('add_grade_num');
        grade_num = parseInt(grade_num)+1;

        obj.attr('add_grade_num',grade_num);

        var price_name = "config[price_name]["+grade_num+"]";
        var config_item_price_old = "config[price_old]["+grade_num+"][]";
        var config_item_price_now = "config[price_now]["+grade_num+"][]";

        var html='<tr class="big-price">'+
                '<td>'+
                    '<input type="button" value="添加礼包" class="gbutton w-120 c-add-level" style="margin: 5px 14px">'+
                    '<input type="button" value="删除礼包" class="gbutton w-120 c-del-level" style="margin: 5px 14px">'+
                    '<input type="button" value="复制礼包" class="gbutton w-120 c-copy-level" style="margin: 5px 14px">'+
                '</td>'+
                '<td>'+
                    '<table width="100%" class="table-activity-reward">'+
                        '<tbody>'+
                        '<tr></tr>'+
                        '<tr>' +
                            '<td style="width:100px;border-bottom: 1px solid #000000">礼包名称</td>' +
                            '<td style="border-bottom: 1px solid #000000">礼包内容<tb>' +
                            '<td style="width:8%;border-bottom: 1px solid #000000">原价</td>'+
                            '<td style="width:8%;border-bottom: 1px solid #000000">现价</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<input class="price_name" type="text" name="'+price_name+'">'+
                            '</td>'+
                            '<td>'+
                                '<table>'+
                                    '<tbody>'+
                                        '<tr></tr>'+
                                        '<tr>'+
                                            '<td>物品ID</td>'+
                                            '<td>数量</td>'+
                                            '<td>角色</td>'+
                                            '<td>操作</td>'+
                                        '</tr>'+
                                    '</tbody>'+
                                '</table>'+
                                '<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="1" add_amount_num="1" add_grade_num="'+grade_num+'">'+
                            '</td>'+
                            '<td><input class="config_item_price_old" style="width: 80px" type="number" name="config[price_old]['+grade_num+']" /></td>'+
                            '<td><input class="config_item_price_now" style="width: 80px" type="number" name="config[price_now]['+grade_num+']" /></td>'+
                        '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</td>'+
            '</tr>';
        $(this).parent('td').parent('tr').parent('tbody').children('tr.normal-price').before(html);
    });
    //删除礼包
    $(".c-del-level").die();
    $(".c-del-level").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
    //复制道具
    $(".copyitem").die();
    $(".copyitem").live('click',function(){
        var parent = $(this).parent('td').parent('tr');
        var clone = parent.clone();
        parent.parent('tbody').append(clone);
    })
});