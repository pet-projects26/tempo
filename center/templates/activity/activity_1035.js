// JavaScript Document
$(function(){
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //添加充值条件
    $('.c-add-reward').die('click');
    $('.c-add-reward').live('click', function(){
        var grade_num  = $(this).attr('add_grade_num');
        grade_num = parseInt(grade_num);

        var amount_num = $(this).attr('add_amount_num');
        amount_num = parseInt(amount_num);

        var name = "amount["+ grade_num +"]["+ amount_num +"]";
        var vip_level = "vip_level["+grade_num+"]["+amount_num+"]";
        var  add = '<tr>' +
            '<td style="width:100px;"> ' +
            '<input type="number" style="width:80px" class="score" name="'+vip_level+'" value="" />  积分' +
            '</td>' +
            '<td>' +
            '<table>' +
            '<tbody> ' +
            '<tr>' +
            '<td>' +
            '<table class="itemTable">' +
            '<thead>' +
            '<tr>' +
            '<td width="">物品ID</td>' +
            '<td width="">数量</td>' +
            '<td width="">职业</td>' +
            '<td width="">操作</td>' +
            '</tr>' +
            '</thead>' +

            '<tbody></tbody>' +

            '</table>' +
            '</td>' +
            '</tr>' +
            '</tbody>' +
            '</table>' +
            '<input type="button" value="添加物品条件" class="gbutton c-extra-additem" add_amount_num="'+ amount_num +'"  add_grade_num="'+ grade_num +'" > ' +
            '</td>' +

            '<td>' +
            '<input type="button" value="删除该条件" class="gbutton del-reward" />' +
            '<input type="button" value="复制该条件" class="gbutton c-copy-reward" />' +
            '</td>'+

            '</tr>';

        var obj = $(this).parent('td').next('td').children('table').children('tbody').children('tr').eq(1).children('td').eq(1).children('table').children('tbody').append(add);
        amount_num = amount_num + 1;//更新amount
        $(this).attr('add_amount_num', amount_num);
    });


    //更先die，然后再重监听
    $('.c-additem').die("click");
    $('.c-additem').live('click', function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);


        var item_id_name =  "config[item_id][]";
        var item_count_name = "config[item_count][]";
        var career_name = "config[career][]";

        var item_weight_name = "config[weight][]";

        var itemWidget = '<tr class="itemWidget">' +
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="config_item_id" autocomplete="on">'  +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_count_name +'" value="1" class="config_item_count">' +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_weight_name +'" value="1" class="config_item_weight">' +
            '</td>' +
            '<td>' +
            '<select name="'+ career_name +'" class="config_item_career">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀</option>' +
            '<option value="2">弓</option>' +
            '<option value="3">法</option>' +
            '<select>' +
            '</td>' +
            '<td>' +
            '<input type="button" value="删除" class="gbutton delitem" title="删除该行">' +
            '<input type="button" value="复制" class="gbutton copyitem" title="复制该行">' +
            '</td>' +
            '</tr>';

        //alert($(this).prev('table').length);
        $(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget);

        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

    });

    //更先die，然后再重监听
    $('.c-extra-additem').die("click");
    $('.c-extra-additem').live('click', function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);

        var add_amount_num = $(this).attr('add_amount_num');
        add_amount_num = parseInt(add_amount_num);


        var score_num_name =  "score_num[]";
        var item_id_name =  "item_id[]";
        var item_count_name = "item_count[]";
        var career_name = "career[]";

        var itemWidget = '<tr class="itemWidget">' +
        
	        '<td>' +
	        '<input type="text" name="'+ score_num_name +'" class="score" style="width: 80px">' +
	        '</td>' +
	        
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
            '</td>' +

            '<td>' +
            '<select name="'+ career_name +'">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀</option>' +
            '<option value="2">弓</option>' +
            '<option value="3">法</option>' +
            '<select>' +
            '</td>' +

            '<td>' +
            '<input type="button" value="删除" class="gbutton delitem" title="删除该行">' +
            '<input type="button" value="复制" class="gbutton copyitem" title="复制该行">'+
            '</td>' +
            '</tr>';

        //alert($(this).prev('table').length);
        $(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget);

        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });

    });

    //删除某个物品
    $('.delitem').die('click');
    $('.delitem').live('click', function() {
        $(this).parent().parent().remove();
    });

    //删除某个条件
    $('.del-reward').die('click');
    $('.del-reward').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
    //复制该行
    $(".copyitem").die('click');
    $(".copyitem").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var cloneTr = obj.clone();
        obj.parent('tbody').append(cloneTr);
    })
});

