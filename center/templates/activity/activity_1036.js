// JavaScript Document
$(function(){
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');
    //添加物品条件
    $('.c-additem').die();
    $('.c-additem').live('click',function(){
        var nowLevel = $(this).attr('add_grade_num');
        var item_name = 'itemId['+nowLevel+'][]';
        var count_name = 'count['+nowLevel+'][]';
        var career_name = 'career['+nowLevel+'][]';
        var value_name = 'value['+nowLevel+'][]';
        var weight_name = 'weight['+weight_name+']';
        var str ='<tr>' +
                '<td width="25%"><input type="text" style="width: 300px;" class="item_id ui-autocomplete-input" name="'+item_name+'" /></td>'+
                '<td width="10%"><input type="number" style="width: 100%" class="item_count" name="'+count_name+'" /></td>'+
                '<td>' +
                    '<select class="career" name="'+career_name+'">' +
                        '<option value="0">通用</option>'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                        '<option value="3">法</option>'+
                    '</select>'+
                '</td>'+
                '<td width="10%">' +
                    '<select class="item_value" name="'+value_name+'">' +
                        '<option value="0">普通物品</option>'+
                        '<option value="1">贵重物品</option>'+
                    '</select>'+
                '</td>'+
                '<td width="15%">' +
                    '<input type="number" style="width: 100px" class="weight" name="'+weight_name+'" />'+
                '</td>'+
                '<td>' +
                    '<input type="button" class="gbutton add_item" name="add_item" value="复制" />'+
                    '<input type="button" class="gbutton del_item" name="del_item" value="删除">'+
                '</td>>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

        var str = "$1["+ add_grade_num +"]";

        var reg = /(\w+)\[\d+\]/g;

        //道具ID
        var itemId = clone.find('.item_id');
        $(itemId).each(function(index){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //数量
        var itemCount = clone.find('.item_count');
        $(itemCount).each(function(index){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //角色
        var career = clone.find('.career');
        $(career).each(function(index){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
        //类型
        var item_value = clone.find('.item_value');
        $(item_value).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //权重
        var weight = clone.find('.weight');
        $(weight).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //添加物品条件
        var addItemT = clone.find('.c-additem');
        addItemT.attr('add_grade_num',add_grade_num);
        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //添加免费次数
    $(".add_gold_level").die('click');
    $(".add_gold_level").live('click',function(){
        var level_num = $(this).attr('level_num');
        level_num = parseInt(level_num);
        var obj = $(this).prev('table').children('tbody');
        var len = obj.children('tr').length;
        if(len >9){
            $.dialog.tips('已达到9次上限');
            return false;
        }
        var str = '<tr class="gold_list">' +
                '<td style="text-align: center"><span class="num" data_num="'+level_num+'">免费次数后第'+level_num+'次元宝数</span></td>'+
                '<td style="text-align: center;"><input type="number" name="gold[]"></td>'+
                '<td><input type="button" value="删除" class="gbutton del_gold" title="删除"></td>'+
            '</tr>';
        obj.append(str);
        level_num = level_num+1;
        $(this).attr('level_num',level_num);
    });
    //删除免费次数
    $(".del_gold").die('click');
    $(".del_gold").live('click',function(){
        var obj = $(this).parent('td').parent('tr').parent('tbody');
        var len = obj.children('tr').find('.gold_list').length;
        if(typeof len == 'undefined'){
            $(".add_gold_level").attr('level_num',1);
        }
        //获取当前被减小的num值
        var nowNumObj = $(this).parent('td').parent('tr').children('td').first();
        var now_num = nowNumObj.children('span').attr('data_num');
        $('span.num').each(function(){
            var data_num = $(this).attr('data_num');
            if(data_num >= now_num){
                data_num--;
            }
            $(this).attr('data_num',data_num);
            $(this).html('免费次数后第'+data_num+'次元宝数');
        });
        var level_numObj = $(".add_gold_level");
        var level_num = level_numObj.attr('level_num');
        level_num = parseInt(level_num)-1;
        level_numObj.attr('level_num',level_num);

        $(this).parent('td').parent('tr').remove();
    });

    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
});

