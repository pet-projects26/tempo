// JavaScript Document
$(function(){
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);
        //配置奖励
        var replace = "["+ add_grade_num + "]";

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');
        var str = "$1["+ add_grade_num +"]";
        var reg = /(\w+\[\w+\])\[\d+\]/g;
        var item_id = clone.find('.item_id');
        $(item_id).each(function(index) {
            var name = $(this).attr('name');
            var s = name.replace(reg, str);
            $(this).attr('name', s)
        });
        var count = clone.find('.count');
        $(count).each(function(index) {
            var name = $(this).attr('name');
            var s = name.replace(reg, str);
            $(this).attr('name', s)
        });
        var career = clone.find('.career');
        $(career).each(function(index) {
            var name = $(this).attr('name');
            var s = name.replace(reg, str);
            $(this).attr('name', s)
        });
        var weight = clone.find('.weight');
        $(weight).each(function(index) {
            var name = $(this).attr('name');
            var s = name.replace(reg, str);
            $(this).attr('name', s)
        });

        //额外奖励
        var vip_level = clone.find('.vip_level');
        $(vip_level).each(function(index) {
            var name = $(this).attr('name');
            var s = name.replace(reg, str);
            $(this).attr('name', s)
        });
        //添加按钮
        var addGradeTop = clone.find('.add_top_item');
        $(addGradeTop).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        var addGradeDay = clone.find('.add_day_item');
        $(addGradeDay).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        var addGradeAmount = clone.find('.add_amount_item');
        $(addGradeAmount).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //添加赠送物品
    $(".add_per_item").die('click');
    $(".add_per_item").live('click',function(){
        var str = '<tr>' +
                '<td width="30%"><input type="text" name="item_exchange[]" class="config_item_id" style="width:250px"></td>'+
                '<td width="12%"><input type="number" name="item_need_count[]" style="width:80px" min="0"></td>'+
                '<td>' +
                 '<input type="button" class="gbutton add_item" value="复制" />'+
                 '<input type="button" class="gbutton del_item" value="删除" />'+
                '</td>'+
            '</tr>';
        $(this).parent('td').children('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });
    });
    //添加大奖奖励
    $(".add_top_item").die('click');
    $(".add_top_item").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);
        var item_name = 'top[item_id]['+add_grade_num+'][]';
        var count_name = 'top[count]['+add_grade_num+'][]';
        var career_name = 'top[career]['+add_grade_num+'][]';
        var weight_name = 'top[weight]['+add_grade_num+'][]';

        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_name+'" class="item_id"></td>'+
                '<td style="text-align: center"><input type="number" min="0" style="width: 100px" name="'+count_name+'" class="count"></td>'+
                '<td style="text-align: center">' +
                    '<select class="career" name="'+career_name+'">'+
                        '<option value="0">通用</option>'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                        '<option value="3">法</option>'+
                    '</select>'+
                '</td>'+
                '<td style="text-align: center"><input type="number" style="width: 100px" name="'+weight_name+'" class="weight" min="0" /></td>'+
                '<td>' +
                    '<input type="button" class="gbutton add_item" value="复制" title="复制">'+
                    '<input type="button" class="gbutton del_item" value="删除" title="删除">'+
                '</td>'+
            '</tr>';
        $(this).parent('td').children('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //添加大奖奖励
    $(".add_day_item").die('click');
    $(".add_day_item").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        var item_name = 'day[item_id]['+add_grade_num+'][]';
        var count_name = 'day[count]['+add_grade_num+'][]';
        var career_name = 'day[career]['+add_grade_num+'][]';
        var weight_name = 'day[weight]['+add_grade_num+'][]';

        var str = '<tr>' +
            '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_name+'" class="item_id"></td>'+
            '<td style="text-align: center"><input type="number" min="0" style="width: 100px" name="'+count_name+'" class="count"></td>'+
            '<td style="text-align: center">' +
            '<select class="career" name="'+career_name+'">'+
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="2">弓</option>'+
            '<option value="3">法</option>'+
            '</select>'+
            '</td>'+
            '<td style="text-align: center"><input type="number" style="width: 100px" name="'+weight_name+'" class="weight" min="0" /></td>'+
            '<td>' +
            '<input type="button" class="gbutton add_item" value="复制" title="复制">'+
            '<input type="button" class="gbutton del_item" value="删除" title="删除">'+
            '</td>'+
            '</tr>';
        $(this).parent('td').children('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //累计奖励添加
    $(".add_amount_item").die('click');
    $(".add_amount_item").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        var add_comsume_num = $(this).attr('add_comsume_num');
        var item_name = 'config[item_id]['+add_grade_num+']['+add_comsume_num+'][]';
        var count_name = 'config[count]['+add_grade_num+']['+add_comsume_num+'][]';
        var career_name = 'config[career]['+add_grade_num+']['+add_comsume_num+'][]';
        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_name+'" class="item_id"></td>'+
                '<td style="text-align: center"><input type="number" min="0" style="width: 100px" name="'+count_name+'" class="count"></td>'+
                '<td style="text-align: center">'+
                    '<select class="career" name="'+career_name+'">'+
                        '<option value="0">通用</option>'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                        '<option value="3">法</option>'+
                    '</select>'+
                '<td>'+
                    '<input type="button" style="margin: 3px" class="gbutton add_item" value="复制" title="复制">'+
                    '<input type="button" style="margin: 3px" class="gbutton del_item" value="删除" title="删除">'+
                '</td>'+
            '</tr>';
        $(this).parent('td').children('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //复制该等级奖励
    $(".copy_amount_level").die('click');
    $(".copy_amount_level").live('click',function(){
        //查找出新世界等级
        var obj = $('#comsume_data');
        var add_comsume_num = obj.attr('comsume_data');
        add_comsume_num= parseInt(add_comsume_num) + 1;
        obj.attr('comsume_data', add_comsume_num);

        var Obj = $(this).parent('td').parent('tr');
        var clone = Obj.clone();

        var preg = /(\w+\[\w+\]\[\d+\])\[\d+\]/g;//config[item_id][1][]
        var str = "$1["+add_comsume_num+"]";
        //天数
        var vip_level = clone.find('.vip_level');
        var names = vip_level.attr('name');
        var ss = names.replace(preg,str);
        vip_level.attr('name',ss);
        //道具ID
        var item_id = clone.find('.item_id');
        $(item_id).each(function(index){
            var name = $(this).attr('name');
            var s = name.replace(preg, str);
            $(this).attr('name', s)
        });
        //数量
        var count = clone.find('.count');
        $(count).each(function(index){
            var name = $(this).attr('name');
            var s = name.replace(preg, str);
            $(this).attr('name', s)
        });
        //数量
        var career = clone.find('.career');
        $(career).each(function(index){
            var name = $(this).attr('name');
            var s = name.replace(preg, str);
            $(this).attr('name', s)
        });
        var addGradeAmount = clone.find('.add_amount_item');
        $(addGradeAmount).each(function(){
            $(this).attr('add_comsume_num',add_comsume_num);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
    //删除等级
    $(".del_amount_level").die('click');
    $(".del_amount_level").live('click',function(){
        $(this).parent('td').parent('tr').remove();
    })
});

