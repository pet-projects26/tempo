// JavaScript Document
$(function(){
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);
        //配置奖励
        var replace = "["+ add_grade_num + "]";

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

        var str = "$1["+ add_grade_num +"]";

        var reg = /(\w+)\[\d+\]/g;

        //坐骑
        var OitemId = clone.find('.horse');

        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.life');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.attack');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });
        //VIP等级
        var OitemId = clone.find('.critical');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.spirit');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        $(this).parent('td').parent('tr').parent('tbody').append(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
});

