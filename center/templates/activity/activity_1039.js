// JavaScript Document
$(function(){
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
        $( ".itemId" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

        var str = "$1["+ add_grade_num +"]";
        var reg = /(\w+)\[\d+\]/g;
        //消费等级
        var comsume = clone.find('.comsume');
        $(comsume).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //物品ID
        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //数量
        var item_count = clone.find('.item_count');
        $(item_count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //元宝
        var mapRange = clone.find('.mapRange');
        $(mapRange).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //权重
        var weight = clone.find('.weight');
        $(weight).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //角色
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //添加按钮
        var addGrade = clone.find('.c-additem');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
        var itemId = clone.find('.itemId');
        $(itemId).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //添加物品条件
        var addItemG = clone.find('.add_gold_pic');
        addItemG.attr('add_grade_num',add_grade_num);
        var addItemB = clone.find('.add_box_pic');
        addItemB.attr('add_grade_num',add_grade_num);
        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //添加物品条件
    $('.add_gold_pic').die();
    $('.add_gold_pic').live('click',function(){
        var nowLevel = $(this).attr('add_grade_num');
        var mapRange1 = 'mapRange['+nowLevel+'][1][]';
        var mapRange2 = 'mapRange['+nowLevel+'][2][]';
        var weight = 'weight['+nowLevel+'][]';
        var str ='<tr>' +
            '<td style="text-align: center">' +
                '<input type="number" class="w-80 mapRange" name="'+mapRange1+'" value=""/>-'+
                '<input type="number" class="w-80 mapRange" name="'+mapRange2+'" value=""/>'+
            '</td>'+
            '<td style="text-align: center"><input type="number" class="weight" value="" name="'+weight+'"></td>'+
            '<td>' +
            '<input type="button" class="gbutton add_item" name="add_item" value="复制" />'+
            '<input type="button" class="gbutton del_item" name="del_item" value="删除">'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    $('.add_box_pic').die('click');
    $(".add_box_pic").live('click',function(){
        var nowLevel = $(this).attr('add_grade_num');
        var item_name = 'itemId['+nowLevel+'][]';
        var count_name = 'count['+nowLevel+'][]';
        var career_name = 'career['+nowLevel+'][]';
        var str ='<tr>' +
            '<td style="text-align: center"><input type="text" style="width: 200px;" class="itemId ui-autocomplete-input" autocomplete="on" name="'+item_name+'" /></td>'+
            '<td style="text-align: center"><input type="number" class="w-80 count" name="'+count_name+'" /></td>'+
            '<td style="text-align: center">' +
            '<select class="career" name="'+career_name+'">' +
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="2">弓</option>'+
            '<option value="3">法</option>'+
            '</select>'+
            '</td>'+
            '<td>' +
            '<input type="button" class="gbutton add_item" name="add_item" value="复制" />'+
            '<input type="button" class="gbutton del_item" name="del_item" value="删除">'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".itemId" ).autocomplete({
            source: _goods
        });
    });
    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
    //删除消费等级
    $(".del_comsume_level").die('click');
    $('.del_comsume_level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });

});

