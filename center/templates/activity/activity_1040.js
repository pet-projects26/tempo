// JavaScript Document
$(function(){

    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

        var str = "$1["+ add_grade_num +"]";
        var reg = /(\w+)\[\d+\]/g;

        //次数
        var itemId = clone.find('.item_id');
        $(itemId).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //单笔充值
        var buyCount = clone.find('.buyCount');
        $(buyCount).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //返利
        var config = clone.find('.config');
        $(config).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //区间1
        var random1 = clone.find('.random1');
        $(random1).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //区间1
        var random2 = clone.find('.random2');
        $(random2).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //添加按钮
        var addGrade = clone.find('.c-additem');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        var item_count = clone.find('.item_count');
        $(item_count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
    });
    //添加物品条件
    $('.c-additem').die();
    $('.c-additem').live('click',function(){
        var nowLevel = $(this).attr('add_grade_num');
        var item_name = 'itemId['+nowLevel+'][]';
        var count_name = 'count['+nowLevel+'][]';
        var config = 'config['+nowLevel+'][]';
        var buyCount_name = 'buyCount['+nowLevel+'][]';
        var random1_name = 'random['+nowLevel+'][1][]';
        var random2_name = 'random['+nowLevel+'][2][]';
        var str ='<tr>' +
            '<td style="text-align: center"><input type="number" class="item_id" name="'+item_name+'" placeholder="填写-1为无限制"></td>'+
            '<td style="text-align: center"><input type="number" class="w-80 buyCount" name="'+buyCount_name+'" placeholder="元"></td>'+
            '<td style="text-align: center">' +
                '<input type="number" class="w-80 random1" name="'+random1_name+'" placeholder="元">'+
                '<input type="number" class="w-80 random2" name="'+random2_name+'" placeholder="元">'+
            '</td>'+
            '<td style="text-align: center"><input type="number" class="w-80 config" name="'+config+'" placeholder="返利"></td>'+
            '<td style="text-align: center">' +
                '<textarea style="width: 300px;height: 50px" class="item_count" name="'+count_name+'" placeholder="[同上]"></textarea>'+
            '</td>'+
            '<td>' +
                '<input type="button" class="gbutton copy_comsume_level" title="复制" value="复制此栏目">'+
                '<input type="button" class="gbutton del_comsume_level" title="删除" value="删除此栏目">'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
    });
    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
    //删除消费等级
    $(".del_comsume_level").die('click');
    $('.del_comsume_level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });

    //复制消费等级
    $(".copy_comsume_level").die('click');
    $(".copy_comsume_level").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    })
});

