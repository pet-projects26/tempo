$(function(){
    $(".action-are").bind('click',function(){
        return false;
        /*var child = $(this).children('input.all-checked');
        if($(this).hasClass('select')){
            //选中状态下
            child.attr('checked',false);
            $(this).removeClass('select');
        }else{
            $(this).addClass('select');
            child.attr('checked',true);
        }*/
    });

    //全选
    $("input#check-all").change('click',function(){
        if($(this).prop('checked')){
            $("input.all-checked").attr('checked',true);
            $("span.action-are").addClass('select');
        }else{
            $("input.all-checked").attr('checked',false);
            $("span.action-are").removeClass('select');
        }
    });
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
        $( ".level_item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

        var str = "$1["+ add_grade_num +"]";
        var reg = /(\w+\[\w+\])\[\d+\]/g;
        var preg = /(\w+\[\d+\]\[\w+\])\[\d+\]/g;
        //提交材料
        var drop = clone.find('.drop');
        $(drop).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        var weight = clone.find('.weight');
        $(weight).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        var per = clone.find('.per');
        $(per).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //type 1,2,3,4 level
        var level_num = clone.find('.level_num');
        $(level_num).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_item_id = clone.find('.level_item_id');
        $(level_item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_count = clone.find('.level_count');
        $(level_count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_career = clone.find('.level_career');
        $(level_career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_time = clone.find('.level_time');
        $(level_time).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_per = clone.find('.level_per');
        $(level_per).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var all_type = clone.find('.all_type');
        $(all_type).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        //添加按钮
        var addGrade = clone.find('.add_drop_item');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        addGrade = clone.find('.add_gold_item');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        addGrade = clone.find('.add_per_item');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        addGrade = clone.find('.add_all_item');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        //$(this).parent('td').parent('tr').parent('tbody').append(clone);
        $(this).parent('td').parent('tr').siblings('.foot_award').before(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //掉落物品添加
    $('.add_drop_item').die('click');
    $('.add_drop_item').live('click',function(){
        var grade_num = $(this).attr('add_grade_num');
        grade_num = parseInt(grade_num);
        var drop_num = $(this).attr('add_drop_num');
        drop_num = parseInt(drop_num);
        var bk = '['+grade_num+']['+drop_num+'][]';
        var item_name = 'day[item_id]'+bk;
        var count_name = 'day[count]'+bk;
        var career_name = 'day[career]'+bk;
        var weight_name = 'day[weight]'+bk;
        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_name+'" class="item_id"></td>'+
                '<td style="text-align: center"><input type="number" style="width: 100px" name="'+count_name+'" class="count"></td>'+
                '<td style="text-align: center;">'+
                '<select class="career" name="'+career_name+'">'+
                    '<option value="0">通用</option>'+
                    '<option value="1">刀</option>'+
                    '<option value="0">弓</option>'+
                    '<option value="0">法</option>'+
                ' </select>'+
                '</td>'+
                '<td style="text-align: center"><input type="number" style="width: 100px" name="'+weight_name+'" class="weight"></td>'+
                '<td>' +
                    '<input type="button" class="gbutton add_item" value="复制" />'+
                    ' <input type="button" class="gbutton del_item" value="删除" />'+
                '</td>'+
            '</tr>';

        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //使用元宝
    $('.add_gold_item').die('click');
    $('.add_gold_item').live('click',function(){
        var grade_num = $(this).attr('add_grade_num');
        grade_num = parseInt(grade_num);
        var bk = '['+grade_num+'][]';
        var item_name = 'gold[item_id]'+bk;
        var count_name = 'gold[count]'+bk;
        var career_name = 'gold[career]'+bk;
        var weight_name = 'gold[weight]'+bk;

        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" value="" style="width: 250px" class="item_id" name="'+item_name+'"></td>'+
                '<td style="text-align: center"><input type="number" value="" style="width: 100px" class="count" name="'+count_name+'"></td>'+
            '<td style="text-align: center;">'+
                '<select class="career" name="'+career_name+'">'+
                    '<option value="0">通用</option>'+
                    '<option value="1">刀</option>'+
                    '<option value="0">弓</option>'+
                    '<option value="0">法</option>'+
                ' </select>'+
            '</td>'+
                '<td style="text-align: center"><input type="number" style="width: 100px" name="'+weight_name+'" class="weight"></td>'+
            '<td>' +
                '<input type="button" class="gbutton add_item" value="复制" />'+
                '<input type="button" class="gbutton del_item" value="删除" />'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //个人奖励
    $(".add_per_item").die('click');
    $('.add_per_item').live('click',function(){
        var grade_num = $(this).attr('add_grade_num');
        grade_num = parseInt(grade_num);
        var drop_num = $(this).attr('add_per_num');
        drop_num = parseInt(drop_num);
        var bk = '['+grade_num+']['+drop_num+'][]';
        var item_name = 'top[item_id]'+bk;
        var count_name = 'top[count]'+bk;
        var career_name = 'top[career]'+bk;
        var weight = 'top[weight]'+bk;
        var str = '<tr>' +
            '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_name+'" class="item_id"></td>'+
            '<td style="text-align: center"><input type="number" style="width: 100px" name="'+count_name+'" class="count"></td>'+
            '<td style="text-align: center;">'+
            '<select class="career" name="'+career_name+'">'+
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="0">弓</option>'+
            '<option value="0">法</option>'+
            ' </select>'+
            '</td>'+
            '<td>' +
            '<input type="button" class="gbutton add_item" value="复制" />'+
            ' <input type="button" class="gbutton del_item" value="删除" />'+
            '</td>'+
            '</tr>';

        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //全服累计
    $(".add_all_item").die('click');
    $('.add_all_item').live('click',function(){
        var grade_num = $(this).attr('add_grade_num');
        grade_num = parseInt(grade_num);
        var drop_num = $(this).attr('add_all_num');
        drop_num = parseInt(drop_num);
        var bk = '['+grade_num+']['+drop_num+'][]';
        var item_name = 'level[0][item_id]'+bk;
        var count_name = 'level[0][count]'+bk;
        var career_name = 'level[0][career]'+bk;
        var str = '<tr>' +
            '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_name+'" class="level_item_id"></td>'+
            '<td style="text-align: center"><input type="number" style="width: 100px" name="'+count_name+'" class="level_count"></td>'+
            '<td style="text-align: center;">'+
            '<select class="level_career" name="'+career_name+'">'+
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="0">弓</option>'+
            '<option value="0">法</option>'+
            ' </select>'+
            '</td>'+
            '<td>' +
            '<input type="button" class="gbutton add_item" value="复制" />'+
            ' <input type="button" class="gbutton del_item" value="删除" />'+
            '</td>'+
            '</tr>';

        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".level_item_id" ).autocomplete({
            source: _goods
        });
    });
    //添加显示奖励
    $(".add_show_item").die('click');
    $(".add_show_item").live('click',function(event){
        var obj = $(this).parent('td').children('table').children('tbody');
        var str = '<tr>' +
                '<td><input type="text" name="config[item_id][]" style="width: 230px" class="config_item_id" placeholder="请填写道具ID"></td>'+
                '<td>' +
                    '<select name="config[career][]">' +
                        '<option value="0">通用</option>'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                        '<option value="3">法</option>'+
                    '</select>'+
                '</td>'+
                '<td><input type="button" class="del_item gbutton" value="删除"></td>'+
            '</tr>';
        obj.append(str);
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });
    });

    //类型选择
    $(".chose-type").die('change');
    $(".chose-type").live('change',function(){
        var type_value = $(this).val();
        var nowObj=$(this).parent('td').parent('tr').children('td.options-type').children("#type_"+type_value+"");
        nowObj.addClass('chose').siblings('div.reward-type').removeClass('chose');
        var obj = $('#level_data2');
        var add_grade_num = obj.attr('level_data2');
        var preg = /(\w+\[\d+\]\[\w+\])\[\d+\]/g;
        var str = "$1["+add_grade_num+"]";//获取最新level

        changeAll(nowObj,preg,str);

        var msg = nowObj.siblings();

        $(msg).each(function(){
            var data_type = $(this).attr('data-type');
            var obj = $(this);
            obj.find('.level_num').val('');
            //bug置换为当前最大的level
            switch (data_type){
                case '0':
                    obj.find('.level_item_id').val('');
                    obj.find('.level_count').val('');
                    break;
                case '1':
                    obj.find('.level_time').val('');
                    obj.find('.level_per').val('');
                    break;
                case '2':
                    obj.find('.level_time').val('');
                    obj.find('.level_per').val('');
                    //obj.find('.all_type').attr('checked',false);
                    break;
                case '3':
                    //obj.find('.all_type').attr('checked',false);
                    break;
                default :
                    break;
            }
        });
    });

    //掉落
    $(".copy_drop_level").die('click');
    $(".copy_drop_level").live('click',function(){
        var obj = $('#drop_data');
        var add_grade_num = obj.attr('drop_data');
        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('drop_data', add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        var preg = /(\w+\[\w+\]\[\d+\])\[\d+\]/g;
        var str = "$1["+add_grade_num+"]";
        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var weight = clone.find('.weight');
        $(weight).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });

        //新增按钮
        var add_item_btn = clone.find('.add_drop_item');
        $(add_item_btn).each(function(){
            $(this).attr('add_drop_num',add_grade_num);
        });
        oTr.parent('tbody').append(clone);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    //个人累计
    $(".copy_per_level").die('click');
    $(".copy_per_level").live('click',function(){
        var obj = $('#top_data');
        var add_grade_num = obj.attr('top_data');
        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('top_data', add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        var preg = /(\w+\[\w+\]\[\d+\])\[\d+\]/g;
        var str = "$1["+add_grade_num+"]";

        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var per = clone.find('.per');
        $(per).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        //新增按钮
        var add_item_btn = clone.find('.add_per_item');
        $(add_item_btn).each(function(){
            $(this).attr('add_per_num',add_grade_num);
        });
        oTr.parent('tbody').append(clone);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });

    });

    //全服累计
    $(".copy_type_item").die('click');
    $(".copy_type_item").live('click',function(){
        var obj = $('#level_data');
        var add_grade_num = obj.attr('level_data');
        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('level_data', add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        var preg = /(\w+\[\d+\]\[\w+\]\[\d+\])\[\d+\]/g;
        var str = "$1["+add_grade_num+"]";
        //changeAll(clone,preg,str);

        var item_id = clone.find('.level_item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var count = clone.find('.level_count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_num = clone.find('.level_num');
        $(level_num).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_career = clone.find('.level_career');
        $(level_career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        oTr.parent('tbody').append(clone);
        //物品绑定
        $( ".level_item_id" ).autocomplete({
            source: _goods
        });
    });

    //全服累计
    $(".copy_type2_item").die('click');
    $(".copy_type2_item").live('click',function(){
        var obj = $('#level_data2');
        var add_grade_num = obj.attr('level_data2');
        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('level_data2', add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        var preg = /(\w+\[\d+\]\[\w+\])\[\d+\]/g;
        var str = "$1["+add_grade_num+"]";
        //changeAll(clone,preg,str);
        var level_per = clone.find('.level_per');
        $(level_per).each(function(){
            var name = $(this).attr('name');
            console.log(str);
            var s = name.replace(preg,str);
            console.log(s);
            $(this).attr('name',s);
        });
        var level_time = clone.find('.level_time');
        $(level_time).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var all_type = clone.find('.all_type');
        $(all_type).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var level_num = clone.find('.level_num');
        $(level_num).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });

        oTr.parent('tbody').append(clone);
        //物品绑定
        $( ".level_item_id" ).autocomplete({
            source: _goods
        });
    });

    $(".del_drop_level").die('click');
    $(".del_drop_level").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
    $(".del_per_level").die('click');
    $(".del_per_level").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });

    $(".del_type2_item").die('click');
    $(".del_type2_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
    $(".del_type_item").die('click');
    $(".del_type_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
        $( ".level_item_id" ).autocomplete({
            source: _goods
        });
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
});

function changeAll(clone,preg,str){

    var level_per = clone.find('.level_per');
    $(level_per).each(function(){
        var name = $(this).attr('name');
        console.log(name);
        var s = name.replace(preg,str);
        $(this).attr('name',s);
    });
    var level_time = clone.find('.level_time');
    $(level_time).each(function(){
        var name = $(this).attr('name');
        var s = name.replace(preg,str);
        $(this).attr('name',s);
    });
    var all_type = clone.find('.all_type');
    $(all_type).each(function(){
        var name = $(this).attr('name');
        var s = name.replace(preg,str);
        $(this).attr('name',s);
    });
    var level_num = clone.find('.level_num');
    $(level_per).each(function(){
        var name = $(this).attr('name');
        var s = name.replace(preg,str);
        $(this).attr('name',s);
    });
    //新增按钮
    var add_item_btn = clone.find('.add_all_item');
    $(add_item_btn).each(function(){
        $(this).attr('add_all_num',add_grade_num);
    });
}