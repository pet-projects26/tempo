//document
$(function(){
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制世界等级
    $(".c-copy-level").die('click');
    $(".c-copy-level").live('click',function(){
        //复制世界等级
        var obj = $("#add_grade_num");
        var add_grade_num = obj.attr("add_grade_num");

        add_grade_num = parseInt(add_grade_num)+1;
        obj.attr('add_grade_num',add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');
        var str = "$1["+ add_grade_num +"]";
        var preg = /(\w+\[\w+\])\[\d+\]/g;

        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        })
        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        })
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        })
        var weight = clone.find('.weight');
        $(weight).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        })
        var round = clone.find('.round');
        $(round).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        })

        var c_add_item = clone.find('.c-additem');
        $(c_add_item).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        })
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
        //自动加载ID
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
    //套装
    $(".top_dress_item").die('click');
    $(".top_dress_item").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        var item_name = 'top[item_id2]['+add_grade_num+'][]';
        var count_name = 'top[count2]['+add_grade_num+'][]';
        var career_name = 'top[career2]['+add_grade_num+'][]';
        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" name="'+item_name+'" class="item_id" style="width:250px;"></td>'+
                '<td style="text-align: center"><input type="number" name="'+count_name+'" class="count"></td>'+
                '<td style="text-align: center">'+
                    '<select class="career" name="'+career_name+'">'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                    '</select>'+
                '</td>'+
                '<td>'+
                '<input type="button" class="gbutton add_item" value="复制">'+
                '<input type="button" class="gbutton del_item" value="删除">'+
                '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    //通用
    $(".top_normal_item").die('click');
    $(".top_normal_item").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        var item_name = 'top[item_id]['+add_grade_num+'][]';
        var count_name = 'top[count]['+add_grade_num+'][]';
        var career_name = 'top[career]['+add_grade_num+'][]';
        var str = '<tr>' +
            '<td style="text-align: center"><input type="text" name="'+item_name+'" class="item_id" style="width:250px;"></td>'+
            '<td style="text-align: center"><input type="number" name="'+count_name+'" class="count"></td>'+
            '<td style="text-align: center">'+
            '<select class="career" name="'+career_name+'">'+
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="2">弓</option>'+
            '</select>'+
            '</td>'+
            '<td>'+
            '<input type="button" class="gbutton add_item" value="复制">'+
            '<input type="button" class="gbutton del_item" value="删除">'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //普通
    $(".day_item-btn").die('click');
    $(".day_item-btn").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        var item_id_name = 'day[item_id]['+add_grade_num+'][]';
        var count_name = 'day[count]['+add_grade_num+'][]';
        var career_name = 'day[career]['+add_grade_num+'][]';
        var weight_name = 'day[weight]['+add_grade_num+'][]';

        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" style="width: 250px" name="'+item_id_name+'" class="item_id"></td>'+
                '<td style="text-align: center"><input type="number" name="'+count_name+'" class="count"></td>'+
                '<td style="text-align: center">'+
                    '<select class="career" name="'+career_name+'">'+
                        '<option value="0">通用</option>'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                        '<option value="3">法</option>'+
                    '</select>'+
                '</td>'+
                '<td style="text-align: center"><input type="number" name="'+weight_name+'" class="weight" /></td>'+
                '<td>'+
                    '<input type="button" class="gbutton add_item" value="复制">'+
                    '<input type="button" class="gbutton del_item" value="删除">'+
                '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //剩余签数
    $(".big_add_btn").die('click');
    $(".big_add_btn").live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        var round_name = 'gold[round]['+add_grade_num+'][]';
        var weight_name = 'gold[weight]['+add_grade_num+'][]';
        var str = '<tr>' +
                '<td style="text-align: center"><input type="number" class="round" name="'+round_name+'"></td>'+
                '<td style="text-align: center"><input type="number" class="weight" name="'+weight_name+'"></td>'+
                '<td>'+
                    '<input type="button" class="gbutton add_item" value="复制">'+
                    '<input type="button" class="gbutton del_item" value="删除">'+
                '</td>'+
            '</tr>';

        $(this).prev('table').children('tbody').append(str);
    })
    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
});
