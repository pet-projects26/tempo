// JavaScript Document
$(function(){
    var day_num = 1;
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');

        var str = "$1["+ add_grade_num +"]";
        var reg = /(\w+)\[\d\]/g;
        //消费等级
        var comsume = clone.find('.comsume');
        $(comsume).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //物品ID
        var itemId = clone.find('.item_id');
        $(itemId).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //数量
        var item_count = clone.find('.item_count');
        $(item_count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //角色
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(reg,str);
            $(this).attr('name',s);
        });
        //添加按钮
        var addGrade = clone.find('.c-additem');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //添加物品条件
    $('.c-additem').die();
    $('.c-additem').live('click',function(){
        var nowLevel = $(this).attr('add_grade_num');
        var comsume__num = $(this).attr('add_comsume_num');
        var item_name = 'itemId['+nowLevel+']['+comsume__num+'][]';
        var count_name = 'count['+nowLevel+']['+comsume__num+'][]';
        var career_name = 'career['+nowLevel+']['+comsume__num+'][]';
        var str ='<tr>' +
            '<td width="25%"><input type="text" style="width: 200px;" class="item_id ui-autocomplete-input" autocomplete="on" name="'+item_name+'" /></td>'+
            '<td width="10%"><input type="number" style="width: 100px" class="item_count" name="'+count_name+'" /></td>'+
            '<td>' +
            '<select class="career" name="'+career_name+'">' +
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="2">弓</option>'+
            '<option value="3">法</option>'+
            '</select>'+
            '</td>'+
            '<td>' +
            '<input type="button" class="gbutton add_item" name="add_item" value="复制" />'+
            '<input type="button" class="gbutton del_item" name="del_item" value="删除">'+
            '</td>>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
    //删除消费等级
    $(".del_comsume_level").die('click');
    $('.del_comsume_level').live('click', function() {
        var delObj = $(this).parent('td').parent('tr').children('td').eq(0);
        var delNum = delObj.children('input').val();
        var comsume = $(this).parent('td').parent('tr').parent('tbody').find('.comsume');
        $(comsume).each(function(){
            var num = $(this).val();
            num = parseInt(num);
            if(num>=delNum){
                num--;
            }
            $(this).val(num);
        });
        day_num--;
        $(this).parent('td').parent('tr').remove();
    });

    //复制消费等级
    $(".copy_comsume_level").die('click');
    $(".copy_comsume_level").live('click',function(){
        day_num++;
        var comsumeObj = $('#comsume_data');
        var comsume_num = comsumeObj.attr('comsume_data');
        comsume_num = parseInt(comsume_num) + 1;
        comsumeObj.attr('comsume_data',comsume_num);
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        //等级转换
        var preg = /(\w+\[\d\])\[\d\]/g;
        var str = "$1["+comsume_num+"]";
        //元宝数
        var comsume = clone.find('.comsume');
        var comsume_name = comsume.attr('name');
        var s = comsume_name.replace(preg,str);
        comsume.attr('name',s);
        comsume.val(day_num);
        //物品ID
        var itemId = clone.find('.item_id');
        $(itemId).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        //数量
        var count = clone.find('.item_count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        //角色
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        //新增按钮
        var add_item_btn = clone.find('.c-additem');
        $(add_item_btn).each(function(){
            $(this).attr('add_comsume_num',comsume_num);
        });
        var item_count = clone.find('.item_count');
        obj.parent('tbody').append(clone);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    })
});

