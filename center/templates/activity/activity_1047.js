//document
$(function(){

    //初始化加载道具ID
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制世界等级
    $(".c-copy-level").die('click');
    $(".c-copy-level").live('click',function(){
        //复制世界等级
        var obj = $("#add_grade_num");
        var add_grade_num = obj.attr("add_grade_num");

        add_grade_num = parseInt(add_grade_num)+1;
        obj.attr('add_grade_num',add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');
        var str = "$1["+ add_grade_num +"]";
        var preg = /(\w+\[\w+\])\[\d+\]/g;

        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });

        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });

        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });

        var weight = clone.find('.weight');
        $(weight).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        //添加按钮
        var addGrade = clone.find('.c-additem');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        $(this).parent('td').parent('tr').siblings('.foot_reward').before(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //世界等级，时装
    $('.top_dress_btn').die('click');
    $('.top_dress_btn').live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);
        var item_id2_name = 'top[item_id2]['+add_grade_num+'][]';
        var count2_name = 'top[count2]['+add_grade_num+'][]';
        var career2_name = 'top[career2]['+add_grade_num+'][]';
        var weight2_name = 'top[weight2]['+add_grade_num+'][]';
        var value2_name = 'top[value2]['+add_grade_num+'][]';

        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" name="'+item_id2_name+'" style="width: 250px" class="item_id"></td>'+
                '<td style="text-align: center"><input type="text" name="'+count2_name+'" class="count"></td>'+
                '<td style="text-align: center">'+
                '<select class="career" name="'+career2_name+'">'+
                    '<option value="1">刀</option>'+
                    '<option value="2">弓</option>'+
                '</select>'+
                '</td>'+
                '<td><input type="number" name="'+weight2_name+'" class="weight"></td>'+
                '<td style="text-align: center">'+
                    '<select class="career" name="'+value2_name+'">'+
                        '<option value="0">无</option>'+
                        '<option value="1">有</option>'+
                    '</select>'+
                ' </td>'+
                '<td>'+
                    '<input type="button" class="gbutton add_item" value="复制">'+
                    '<input type="button" class="gbutton del_item" value="删除">'+
                '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //世界等级，通用
    $('.top_normal_btn').die('click');
    $('.top_normal_btn').live('click',function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);
        var item_id2_name = 'top[item_id]['+add_grade_num+'][]';
        var count2_name = 'top[count]['+add_grade_num+'][]';
        var career2_name = 'top[career]['+add_grade_num+'][]';
        var weight2_name = 'top[weight]['+add_grade_num+'][]';
        var value2_name = 'top[value]['+add_grade_num+'][]';

        var str = '<tr>' +
            '<td style="text-align: center"><input type="text" name="'+item_id2_name+'" style="width: 250px" class="item_id"></td>'+
            '<td style="text-align: center"><input type="text" name="'+count2_name+'" class="count"></td>'+
            '<td style="text-align: center">'+
            '<select class="career" name="'+career2_name+'">'+
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="2">弓</option>'+
            '</select>'+
            '</td>'+
            '<td><input type="number" name="'+weight2_name+'" class="weight"></td>'+
            '<td style="text-align: center">'+
            '<select class="career" name="'+value2_name+'">'+
            '<option value="0">无</option>'+
            '<option value="1">有</option>'+
            '</select>'+
            ' </td>'+
            '<td>'+
            '<input type="button" class="gbutton add_item" value="复制">'+
            '<input type="button" class="gbutton del_item" value="删除">'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //保底，时装
    $(".day_dress_btn").die('click');
    $(".day_dress_btn").live('click',function(){
        var item_id_name = 'day[item_id2][]';
        var count_name = 'day[count2][]';
        var career_name = 'day[career2][]';
        var score_name = 'day[score2][]';
        var limit_name = 'day[limit2][]';

        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" name="'+item_id_name+'" style="width: 250px" class="item_id"></td>'+
                '<td style="text-align: center"><input type="text" name="'+count_name+'" class="count"></td>'+
                ' <td style="text-align: center">'+
                    '<select class="career" name="'+career_name+'">'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                    ' </select>'+
                '</td>'+
                '<td style="text-align: center"><input type="number" name="'+score_name+'" class="score"></td>'+
                '<td style="text-align: center"><input type="number" name="'+limit_name+'" placeholder="0:没有限制次数" class="score"></td>'+
                '<td>'+
                    '<input type="button" class="gbutton add_item" value="复制">'+
                    '<input type="button" class="gbutton del_item" value="删除">'+
                '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });
    //保底，通用

    $(".day_normal_btn").die('click');
    $(".day_normal_btn").live('click',function(){
        var item_id_name = 'day[item_id][]';
        var count_name = 'day[count][]';
        var career_name = 'day[career][]';
        var score_name = 'day[score][]';
        var limit_name = 'day[limit][]';

        var str = '<tr>' +
            '<td style="text-align: center"><input type="text" name="'+item_id_name+'" style="width: 250px" class="item_id"></td>'+
            '<td style="text-align: center"><input type="text" name="'+count_name+'" class="count"></td>'+
            ' <td style="text-align: center">'+
            '<select class="career" name="'+career_name+'">'+
            '<option value="0">通用</option>'+
            '<option value="1">刀</option>'+
            '<option value="2">弓</option>'+
            ' </select>'+
            '</td>'+
            '<td style="text-align: center"><input type="number" name="'+score_name+'" class="score"></td>'+
            '<td style="text-align: center"><input type="number" name="'+limit_name+'" placeholder="0:没有限制次数" class="limit"></td>'+
            '<td>'+
            '<input type="button" class="gbutton add_item" value="复制">'+
            '<input type="button" class="gbutton del_item" value="删除">'+
            '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
});