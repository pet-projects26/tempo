$(function(){
    //初始化加载道具ID
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //复制世界等级
    $(".c-copy-level").die('click');
    $(".c-copy-level").live('click',function(){
        var obj = $("#add_grade_num");
        var add_grade_num = obj.attr("add_grade_num");

        add_grade_num = parseInt(add_grade_num)+1;
        obj.attr('add_grade_num',add_grade_num);
        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');
        var str = "$1["+ add_grade_num +"]";
        var preg = /(\w+)\[\d+\]/g;

        var item_id = clone.find('.item_id');
        $(item_id).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var count = clone.find('.count');
        $(count).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });
        var career = clone.find('.career');
        $(career).each(function(){
            var name = $(this).attr('name');
            var s = name.replace(preg,str);
            $(this).attr('name',s);
        });

        var addGrade = clone.find('.c-additem');
        $(addGrade).each(function(){
            $(this).attr('add_grade_num',add_grade_num);
        });
        $(this).parent('td').parent('tr').parent('tbody').append(clone);

        $(".item_id").autocomplete({
            source: _goods
        });
    });

    $('.c-additem').die('click');
    $('.c-additem').live('click',function(){
        var add_grade_level = $(this).attr('add_grade_num');
        add_grade_level = parseInt(add_grade_level);
        var item_id = 'item_id['+add_grade_level+'][]';
        var count = 'count['+add_grade_level+'][]';
        var career = 'career['+add_grade_level+'][]';

        var str = '<tr>' +
                '<td style="text-align: center"><input type="text" name="'+item_id+'" style="width: 250px" class="item_id"></td>'+
                '<td style="text-align: center"><input type="number" name="'+count+'" class="count"></td>'+
                '<td style="text-align: center">'+
                    '<select name="'+career+'" class="career">'+
                        '<option value="0">通用</option>'+
                        '<option value="1">刀</option>'+
                        '<option value="2">弓</option>'+
                    '</select>'+
                '</td>'+
                '<td>'+
                    '<input type="button" class="gbutton copy_item" value="复制">'+
                    '<input type="button" class="gbutton del_item" value="删除">'+
                '</td>'+
            '</tr>';
        $(this).prev('table').children('tbody').append(str);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //复制道具
    $(".add_item").die();
    $(".add_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        var clone = obj.clone();
        obj.parent('tbody').append(clone);
    });
    //删除道具
    $(".del_item").die();
    $(".del_item").live('click',function(){
        var obj = $(this).parent('td').parent('tr');
        obj.remove();
    });
});