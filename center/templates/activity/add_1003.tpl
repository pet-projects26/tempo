<tr grade_num="1">
    <td style="width:150px;">
        <input type="button" value="添加条件" class="gbutton c-add-reward" add_amount_num="1">
        <script type="text/javascript" src="templates/activity/activity_1003.js"></script>
    </td>
    <td>
        <table style="width:100%;" class="table-activity-reward">
            <thead>
                <tr>
                    <th style="width:80px;">条件</th>
                    <th>奖励</th>
                    <th>删除</th>
                </tr>
            </thead>
            <tbody class="body-item">
                <tr>
                    <td style="width:100px;">
                        <input type="text" class="amount" name="amount[1]" value="" placeholder="额度">
                    </td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <table class="itemTable">
                                            <thead>
                                                <tr>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>角色</td>
                                                    <td>操作</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0"
                        add_amount_num="1">
                    </td>
                    <td>
                        <input type="button" value="删除该条件" class="gbutton delitem">
                        <input type="button" value="复制该条件" class="gbutton c-copy-reward">
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>