
<tr>
    <td>充值元宝</td>
    <td>
        <input type="number" value="" name="charge_gold">
    </td>
</tr>
<tr>
    <td>扭蛋条目上限</td>
    <td>
        <input type="number" readonly="readonly" value="50" style="background-color: #dddddd">
    </td>
</tr>
<tr>
    <td style="width:150px">
        <label>世界等级</label>

        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1033.js"></script>

        <input type="button" value="添加累计奖励" class="gbutton c-add-reward w-120" add_grade_num="1" add_amount_num="1">
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tr>
                <td style="width: 15%">配置奖励</td>
                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">

                        <tbody class="body-item">
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table class="itemTable">
                                                <thead>
                                                <tr>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>权重</td>
                                                    <td>角色</td>
                                                    <td>操作</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>

            <tr class="tr-extra">
                <td>
                    累计奖励<br/>
                    个人扭蛋<input type="text" readonly="readonly" style="text-align: center;color: #ca1b36;font-weight: 600;width: 30px;margin: 0 2px" value="1">次可领取<br/>
                    <em style="font-size: 8px;color: #ca1b36">*所有世界等级累计奖励条件数目一致</em>
                </td>

                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">
                        <thead>
                        <tr>
                            <th>条件</th>
                            <th>奖励</th>
                            <th>删除</th>
                        </tr>
                        </thead>
                        <tbody class="body-item">
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->