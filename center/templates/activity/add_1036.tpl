{*圣诞许愿 活动内容配置*}
<tr>
    <td>免费次数</td>
    <td>
        <input type="number" name="freeGold" value="1" >&nbsp;&nbsp;<span style="color: #ca1b36">免费次数+使用元宝次数必须为9次</span>
    </td>
</tr>
<tr>
    <td>使用元宝次数</td>
    <td>
        <table>
            <tbody>
            <tr>
                <td width="10%" style="text-align: center">次数</td>
                <td width="12%" style="text-align: center;">元宝数</td>
                <td>操作</td>
            </tr>

            </tbody>
        </table>
        <input type="button" class="gbutton add_gold_level" level_num="1" value="添加抽奖次数" title="添加">
    </td>
</tr>
<tr>
    <td>抽中贵重物品所需要的次数</td>
    <td><input type="number" name="score_num">&nbsp;&nbsp;<span style="color:#ca1b36">单个角色普通物品的数量应大于或者等于抽中贵重物品所需要的次数</span></td>
</tr>
<tr>
    <td style="width:150px">
        <label>世界等级</label>

        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1036.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tbody>
            <tr>
                <td class="td_title">道具ID</td>
                <td class="td_title">数量</td>
                <td class="td_title">角色</td>
                <td class="td_title">类型</td>
                <td style="border-bottom: 1px solid #000">概率的权重</td>
            </tr>
            <!-- 第一档次 -->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第一档次 end-->
            <!--第二档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="1">刀</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第二档次end-->

            <!--第三档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第三档次end-->

            <!--第四档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第四档次end-->

            <!--第五档次-->

            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第五档次end-->

            <!--第六档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第六档次end-->

            <!--第七档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第七档次end-->

            <!--第八档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第八档次end-->

            <!--第九档次-->
            <tr>
                <td width="25%"><input type="text" style="width: 300px" class="item_id" name="itemId[1][]" /></td>
                <td width="10%"><input type="number" style="width: 100px" class="item_count" name="count[1][]"/></td>
                <td width="10%">
                    <select class="career" name="career[1][]">
                        <option value="2">弓</option>
                    </select>
                </td>
                <td width="10%">
                    <select class="item_value" name="value[1][]">
                        <option value="0">普通物品</option>
                        <option value="1">贵重物品</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="weight" style="width: 100px" name="weight[1][]" />
                </td>
            </tr>
            <!--第九档次end-->

            </tbody>
        </table>
        <!--<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">-->
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->