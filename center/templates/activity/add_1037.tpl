{*摇摇乐活动配置*}
<tr>
    <td>摇一次所需幸运宝符或元宝</td>
    <td>
        道具ID &nbsp;<input type="text" class="roll_one_id config_item_id" readonly="readonly" value="3200030000011/幸运宝符" name="roll_one_id" style="width: 250px;background-color: #c3c3c3" />
        道具数量 &nbsp;<input type="number" min="0" name="roll_one_num" style="width: 80px" >
        元宝数量 &nbsp;<input type="number" min="0" name="gold_one_num" style="width: 80px">
    </td>
</tr>
<tr>
    <td>摇十次所需幸运宝符或元宝</td>
    <td>
        道具ID &nbsp;<input type="text" class="roll_ten_id config_item_id" readonly="readonly" name="roll_ten_id" value="3200030000011/幸运宝符" style="width: 250px;background-color: #c3c3c3" />
        道具数量 &nbsp;<input type="number" min="0" name="roll_ten_num" style="width: 80px" >
        元宝数量 &nbsp;<input type="number" min="0" name="gold_ten_num" style="width: 80px">
    </td>
</tr>
<!--<tr>
    <td>赠送物品</td>
    <td>
        <table>
            <tbody>
            <tr>
                <td>道具ID</td>
                <td>数量</td>
                <td>操作</td>
            </tr>
            <tr>
                <td width="30%"><input type="text" name="item_exchange[]" class="config_item_id" style="width:250px"></td>
                <td width="12%"><input type="number" name="item_need_count[]" style="width:80px" min="0"></td>
                <td>
                    <input type="button" class="gbutton add_item" value="复制" />
                    <input type="button" class="gbutton del_item" value="删除" />
                </td>
            </tr>
            </tbody>
        </table>
        <input type="button" class="gbutton add_per_item" value="添加赠送物品">
    </td>
</tr>-->
<tr>
    <td>怪物掉落</td>
    <td>
        怪物的等级 &nbsp;<input type="number" min="0" value="" style="width:110px;" name="monsterId"> 掉落ID &nbsp;<input value="10006" style="background-color: #c3c3c3" readonly="readonly" type="text" name="dropItemId">
    </td>
</tr>
<tr>
    <td style="width:150px">
        <label>世界等级</label>
        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1037.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tbody>
            <tr>
                <td style="text-align: center;border: 1px solid #000" width="10%">大奖</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="15%" class="td_title">角色</td>
                            <td style="border-bottom: 1px solid #000">权重</td>
                        </tr>
                        <!--第一档次-->
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="1">刀</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" style="width: 100px" name="top[weight][1][]" class="weight" min="0" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="1">刀</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" style="width: 100px" name="top[weight][1][]" class="weight" min="0" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="1">刀</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" style="width: 100px" name="top[weight][1][]" class="weight" min="0" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" style="width: 100px" name="top[weight][1][]" class="weight" min="0" />
                            </td>
                        </tr>
                        <!--第一档次 end-->
                        <!--第二档次-->
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" style="width: 100px" name="top[weight][1][]" class="weight" min="0" />
                            </td>
                        </tr>
                        <!--第二档次 end-->
                        <!--第三档次-->
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" style="width: 100px" name="top[weight][1][]" class="weight" min="0" />
                            </td>
                        </tr>
                        <!--第三档次 end-->
                    </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;border: 1px solid #000" width="10%">普通奖励</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="15%" class="td_title">角色</td>
                            <td width="15%" class="td_title">权重</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="day[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="day[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="day[career][1][]">
                                    <option value="0">通用</option>
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                    <option value="3">法</option>
                                </select>
                            </td>
                            <td style="text-align: center">
                                <input type="number" style="width: 100px" name="day[weight][1][]" min="0" class="weight" />
                            </td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制" title="复制">
                                <input type="button" class="gbutton del_item" value="删除" title="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton add_day_item" value="添加普通奖励" add_item_num="1"
                           add_comsume_num="1" add_grade_num="1">
                </td>
            </tr>
            <tr>
                <td style="text-align: center;border: 1px solid #000" width="10%">累计奖励</td>
                <td>
                    <table>
                        <tbody>
                        <tr></tr>
                        <!--标记-->
                        <tr style="display: none;">
                            <td colspan="2">
                                <input type="button" value="天数标记"  id="comsume_data" comsume_data="1" >
                            </td>
                        </tr>
                        <!--标记-->
                        <tr>
                            <td width="12%" class="td_title">个人次数</td>
                            <td width="60%" class="td_title">奖励配置</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="number" class="vip_level" name="config[vip_level][1][1]" min="0" style="width: 100px"></td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="40%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="15%" class="td_title">角色</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton add_amount_item" title="添加" value="添加道具奖励" add_item_num="1"
                                       add_comsume_num="1" add_grade_num="1">
                            </td>
                            <td>
                                <input type="button" style="margin: 3px 0" class="gbutton copy_amount_level" title="复制" value="复制该等级奖励">
                                <input type="button" style="margin: 3px 0" class="gbutton del_amount_level" title="删除" value="删除该等级奖励">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->