
<tr>
    <td style="width:150px">
        <label>世界等级</label>

        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1038.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tr>
                <td style="width:8%">配置奖励</td>
                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">
                        <tbody class="">
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table class="itemTable">
                                                <thead>
                                                <tr>
                                                    <td style="border-bottom: 1px solid #000000">类型</td>
                                                    <td style="text-align: center;border-bottom: 1px solid #000000">配置</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>坐骑</td>
                                                    <td>
                                                        <table>
                                                            <thead>
                                                            <tr>
                                                                <td width="20%">等级</td>
                                                                <td width="20%">道具可双倍次数</td>
                                                                <td></td>
                                                                <td>操作</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><input type="number" name="level[1][1][]" class="horse" /></td>
                                                                <td><input type="number" name="item_num[1][1][]" class="horse" /></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="button" class="gbutton add_item" value="复制">
                                                                    <input type="button" class="gbutton del_item" value="删除">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>生命神兵</td>
                                                    <td>
                                                        <table>
                                                            <thead>
                                                            <tr>
                                                                <td width="20%">等级</td>
                                                                <td width="20%">道具可双倍次数</td>
                                                                <td></td>
                                                                <td>操作</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><input type="number" name="level[1][2][]" class="life" /></td>
                                                                <td><input type="number" name="item_num[1][2][]" class="life" /></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="button" class="gbutton add_item" value="复制">
                                                                    <input type="button" class="gbutton del_item" value="删除">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>攻击神兵</td>
                                                    <td>
                                                        <table>
                                                            <thead>
                                                            <tr>
                                                                <td width="20%">等级</td>
                                                                <td width="20%">道具可双倍次数</td>
                                                                <td></td>
                                                                <td>操作</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><input type="number" name="level[1][3][]" class="attack" /></td>
                                                                <td><input type="number" name="item_num[1][3][]" class="attack" /></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="button" class="gbutton add_item" value="复制">
                                                                    <input type="button" class="gbutton del_item" value="删除">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>暴击神兵</td>
                                                    <td>
                                                        <table>
                                                            <thead>
                                                            <tr>
                                                                <td width="20%">等级</td>
                                                                <td width="20%">道具可双倍次数</td>
                                                                <td></td>
                                                                <td>操作</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><input type="number" name="level[1][4][]" class="critical" /></td>
                                                                <td><input type="number" name="item_num[1][4][]" class="critical" /></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="button" class="gbutton add_item" value="复制">
                                                                    <input type="button" class="gbutton del_item" value="删除">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>元神</td>
                                                    <td>
                                                        <table>
                                                            <thead>
                                                            <tr>
                                                                <td width="20%">等级</td>
                                                                <td width="20%">道具可双倍次数</td>
                                                                <td width="20%">吞噬装备可双倍次数</td>
                                                                <td>操作</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><input type="number" name="level[1][5][]" class="spirit" /></td>
                                                                <td><input type="number" name="item_num[1][5][1][]" class="spirit" /></td>
                                                                <td><input type="number" name="item_num[1][5][2][]" class="spirit" /></td>
                                                                <td>
                                                                    <input type="button" class="gbutton add_item" value="复制">
                                                                    <input type="button" class="gbutton del_item" value="删除">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!--<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">-->
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr></tr>
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->