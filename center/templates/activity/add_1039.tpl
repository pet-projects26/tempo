<tr>
    <td style="width:150px">
        <label>世界等级</label>
        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1039.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table class="" style="border: 2px solid #FFFFFF" border="1" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td width="15%">累计充值目标</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">充值金额</td>
                            <td style="border-bottom: 1px solid #000000;" align="char">奖励配置</td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <input type="number" name="comsume[1][1]" class="comsume ui-autocomplete-input" autocomplete="on" style="width: 120px" value="">
                            </td>
                            <td>
                                道具ID&nbsp;&nbsp;<input type="text" style="width: 200px;background-color: #c3c3c3" class="item_id ui-autocomplete-input" readonly="readonly" autocomplete="on" name="item_id[1][1]" value="3200030000012/金钥匙">
                                数量&nbsp;&nbsp;<input type="number" class="w-80 item_count" name="item_count[1][1]">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>累计消费目标</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">消费金额</td>
                            <td style="border-bottom: 1px solid #000000;" align="char">奖励配置</td>
                        </tr>
                        <!--配置部分-->
                        <tr>
                            <td style="text-align: center"><input type="number" name="comsume[1][2]" class="comsume" style="width:120px"></td>
                            <td>
                                道具ID&nbsp;&nbsp;<input type="text" style="width: 200px;background-color: #c3c3c3" class="item_id" readonly="readonly" name="item_id[1][2]" value="3200030000013/银钥匙">
                                数量&nbsp;&nbsp;<input type="number" class="w-80 item_count" name="item_count[1][2]">
                            </td>
                        </tr>
                        <!--------->
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>宝箱开出元宝条件</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">元宝个数</td>
                            <td width="30%" class="td_title">权重</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <!--配置部分--->
                        <tr>
                            <td style="text-align: center">
                                <input type="number" class="w-80 mapRange" name="mapRange[1][1][]" value=""/>-
                                <input type="number" class="w-80 mapRange" name="mapRange[1][2][]" value=""/>
                            </td>
                            <td style="text-align: center">
                                <input type="number" class="weight" value="" name="weight[1][]">
                            </td>
                            <td>
                                <input type="button" class="add_item gbutton" title="复制" value="复制">
                                <input type="button" class="del_item gbutton" title="删除" value="删除">
                            </td>
                        </tr>
                        <!------------>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton add_gold_pic" add_item_num="0" add_amount_num="0" add_grade_num="1" value="添加配置条件">
                </td>
            </tr>
            <tr>
                <td>开启最多宝箱奖励</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="14%" class="td_title">数量</td>
                            <td width="14%" class="td_title">角色</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <!--配置部分-->
                        <tr>
                            <td style="text-align: center"><input type="text" class="itemId" style="width: 200px" name="itemId[1][]"></td>
                            <td style="text-align: center"><input type="number" name="count[1][]" class="w-80 count"></td>
                            <td style="text-align: center">
                                <select class="career" name="career[1][]">
                                    <option value="0">通用</option>
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                    <option value="3">法</option>
                                </select>
                            </td>
                            <td>
                                <input type="button" class="add_item gbutton" title="复制" value="复制">
                                <input type="button" class="del_item gbutton" title="删除" value="删除">
                            </td>
                        </tr>
                        <!-------->
                        </tbody>
                    </table>
                    <input type="button" class="gbutton add_box_pic" add_item_num="0" add_amount_num="0" add_grade_num="1" value="添加配置条件">
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr></tr>
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->