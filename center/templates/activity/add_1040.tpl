{*充值返利 活动配置*}
<tr></tr>
<tr>
    <td style="width:150px">
       <label>世界等级</label>
         <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1040.js"></script>
       <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="10%" class="td_title">返利次数</td>
                            <td width="10%" class="td_title">单笔充值</td>
                            <td width="22%" class="td_title">充值范围</td>
                            <td width="10%" class="td_title">返利%(只是显示,一般配置最低的)</td>
                            <td width="25%" class="td_title">返利百分比（游戏内按照下面顺序显示返利比例）</td>
                            <td class="td_title">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="number" class="item_id" name="itemId[1][]" placeholder="填写-1为无限制"></td>
                            <td style="text-align: center"><input type="number" class="w-80 buyCount" name="buyCount[1][]" placeholder="元"></td>
                            <td style="text-align: center">
                                <input type="number" class="w-80 random1" name="random[1][1][]" placeholder="元">~<!--[等级][开始]-->
                                <input type="number" class="w-80 random2" name="random[1][2][]" placeholder="元"><!--[等级][结束]-->
                            </td>
                            <td style="text-align: center"><input type="number" class="w-80 config" name="config[1][]" placeholder="返利"></td>
                            <td style="text-align: center">
                                <textarea style="width: 300px;height: 50px" class="item_count" name="count[1][]" placeholder="如：第1次返利10%，则填写[10]&#10如：第1次返利10%，第2次返利20%，则填写[10,20]"></textarea>
                            </td>
                            <td>
                                <input type="button" class="gbutton copy_comsume_level" title="复制" value="复制此栏目">
                                <input type="button" class="gbutton del_comsume_level" title="删除" value="删除此栏目">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加充值档次">
                </td>
            </tr>
            </tbody>
        </table>
     </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->