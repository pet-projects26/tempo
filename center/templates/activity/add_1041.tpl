{*消费有礼 活动内容配置*}
<tr>
    <td style="width:150px">
        <label>世界等级</label>
        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1041.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tbody>
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="元宝等级标记"  id="comsume_data" comsume_data="1" >
                </td>
            </tr>
            <tr>
                <td style="width:100px;border-bottom: 1px solid #000000">
                    消费元宝数
                </td>
                <td style="border-bottom: 1px solid #000000">奖励配置</td>
                <td style="width:22%;border-bottom: 1px solid #000000">
                    操作
                </td>
            </tr>
            <tr>
                <td>
                    <input class="comsume" type="number" style="width: 100px" name="comsume[1][1]">
                </td>
                <td>
                    <table>
                        <tbody>
                        <tr></tr>
                        <tr>
                            <td style="width: 28%">物品ID</td>
                            <td>数量</td>
                            <td>角色</td>
                            <td>操作</td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="1"
                           add_comsume_num="1" add_grade_num="1">
                </td>
                <td>
                    <input type="button" class="gbutton copy_comsume_level" value="复制消费等级" />
                    <input type="button" class="gbutton del_comsume_level" value="删除消费等级" />
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->