{*堆雪人 活动配置*}
<tr>
    <td>掉落</td>
    <td>
        怪物等级 &nbsp;<input type="number" name="monsterLevel" >
        掉落Id &nbsp;<input type="number" name="dropItemId" value="10007" placeholder="10007">
    </td>
</tr>
<tr>
    <td>元宝捐献所需金额</td>
    <td><input type="number" name="freeGold" aria-valuemin="0"></td>
</tr>
<tr>
    <td>材料提交奖励（游戏内展现）</td>
    <td>
        <table style="border-collapse: collapse;border-spacing: 0">
            <tbody>
            <tr>
                <td width="18%">道具ID</td>
                <td width="8%">角色</td>
                <td>操作</td>
            </tr>
            <tr>
                <td><input type="text" class="config_item_id" name="config[item_id][]" style="width: 230px" placeholder="请填写道具ID"></td>
                <td>
                    <select name="config[career][]">
                        <option value="0">通用</option>
                        <option value="1">刀</option>
                        <option value="2">弓</option>
                        <option value="3">法</option>
                    </select>
                </td>
                <td><input type="button" class="del_item gbutton" value="删除"></td>
            </tr>
            <tr>
                <td><input type="text" class="config_item_id" name="config[item_id][]" style="width: 230px" placeholder="请填写道具ID"></td>
                <td>
                    <select name="config[career][]">
                        <option value="0">通用</option>
                        <option value="1">刀</option>
                        <option value="2">弓</option>
                        <option value="3">法</option>
                    </select>
                </td>
                <td><input type="button" class="del_item gbutton" value="删除"></td>
            </tr>
            <tr>
                <td><input type="text" class="config_item_id" name="config[item_id][]" style="width: 230px" placeholder="请填写道具ID"></td>
                <td>
                    <select name="config[career][]">
                        <option value="0">通用</option>
                        <option value="1">刀</option>
                        <option value="2">弓</option>
                        <option value="3">法</option>
                    </select>
                </td>
                <td><input type="button" class="del_item gbutton" value="删除"></td>
            </tr>
            <tr>
                <td><input type="text" class="config_item_id" name="config[item_id][]" style="width: 230px" placeholder="请填写道具ID"></td>
                <td>
                    <select name="config[career][]">
                        <option value="0">通用</option>
                        <option value="1">刀</option>
                        <option value="2">弓</option>
                        <option value="3">法</option>
                    </select>
                </td>
                <td><input type="button" class="del_item gbutton" value="删除"></td>
            </tr>
            </tbody>
        </table>
        <input type="button" class="gbutton add_show_item" value="添加显示奖励" />
    </td>
</tr>
<tr>
    <td style="width:150px">
        <label>世界等级</label>
        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1042.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table style="border: 2px solid #FFFFFF" border="1">
            <tbody>
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center">提交材料奖励</td>
                <td>
                    <table border="0">
                        <tbody>
                        <tr></tr>
                        <!--标记-->
                        <tr style="display: none;">
                            <td colspan="2">
                                <input type="button" value="天数标记"  id="drop_data" drop_data="1" >
                            </td>
                        </tr>
                        <!--标记-->
                        <tr>
                            <td width=18%" class="td_title">掉落道具ID</td>
                            <td width="65%" class="td_title">奖励配置</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" class="item_id drop" name="day[drop_id][1][1]" min="0" style="width: 200px"></td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="40%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="15%" class="td_title">角色</td>
                                        <td width="15%" class="td_title">权重</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center"><input type="text" style="width: 250px" name="day[item_id][1][1][]" class="item_id"></td>
                                        <td style="text-align: center"><input type="number" style="width: 100px" name="day[count][1][1][]" class="count"></td>
                                        <td style="text-align: center;">
                                            <select class="career" name="day[career][1][1][]">
                                                <option value="0">通用</option>
                                                <option value="1">刀</option>
                                                <option value="2">弓</option>
                                                <option value="3">法</option>
                                            </select>
                                        </td>
                                        <td style="text-align: center"><input type="number" style="width: 100px" name="day[weight][1][1][]" class="weight"></td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制" />
                                            <input type="button" class="gbutton del_item" value="删除" />
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton add_drop_item" title="添加" value="添加道具奖励" add_item_num="1"
                                       add_drop_num="1" add_grade_num="1">
                            </td>
                            <td>
                                <input type="button" style="margin: 3px 3px" class="gbutton copy_drop_level" title="复制" value="复制该等级">
                                <input type="button" style="margin: 3px 3px" class="gbutton del_drop_level" title="删除" value="删除该等级">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center">花费元宝提交一次</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="20%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="10%" class="td_title">角色</td>
                            <td width="10%" class="td_title">权重</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" value="" style="width: 250px" class="item_id" name="gold[item_id][1][]"></td>
                            <td style="text-align: center"><input type="number" value="" style="width: 100px" class="count" name="gold[count][1][]"></td>
                            <td style="text-align: center">
                                <select class="career" name="gold[career][1][]">
                                    <option value="0">通用</option>
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                    <option value="3">法</option>
                                </select>
                            </td>
                            <td style="text-align: center"><input type="number" style="width: 100px" name="gold[weight][1][]" class="weight"></td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制" />
                                <input type="button" class="gbutton del_item" value="删除" />
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton add_gold_item" value="添加道具奖励" add_item_num="1"
                           add_drop_num="1" add_grade_num="1">
                </td>
            </tr>
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center">个人累计奖励</td>
                <td>
                    <table>
                        <tbody>
                        <!--标记-->
                        <tr style="display: none;">
                            <td colspan="2">
                                <input type="button" value="天数标记"  id="top_data" top_data="1" >
                            </td>
                        </tr>
                        <!--标记-->
                        <tr>
                            <td width=12%" class="td_title">个人累计次数</td>
                            <td width="68%" class="td_title">奖励配置</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input class="per" type="number" name="top[per][1][1]" min="0" style="width: 150px"></td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="40%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="15%" class="td_title">角色</td>
                                        <td width="15%" class="td_title">权重</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center"><input type="text" name="top[item_id][1][1][]" style="width: 250px" class="item_id"></td>
                                        <td style="text-align: center"><input type="number" name="top[count][1][1][]" style="width: 100px" class="count"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[career][1][1][]">
                                                <option value="0">通用</option>
                                                <option value="1">刀</option>
                                                <option value="2">弓</option>
                                                <option value="3">法</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制" />
                                            <input type="button" class="gbutton del_item" value="删除" />
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton add_per_item" title="添加" value="添加道具奖励" add_item_num="1"
                                       add_per_num="1" add_grade_num="1">
                            </td>
                            <td>
                                <input type="button" style="margin: 3px 3px" class="gbutton copy_per_level" title="复制" value="复制该等级">
                                <input type="button" style="margin: 3px 3px" class="gbutton del_per_level" title="删除" value="删除该等级">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <!--全服累计奖励-->
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center">全服累计奖励</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="10%" style="text-align: center;border-bottom: 1px solid #000">奖励类型</td>
                            <td width="70%" style="text-align: center;border-bottom: 1px solid #000">奖励配置</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <select style="padding: 4px 2px;" class="chose-type">
                                    <option value="0" selected>道具</option>
                                </select>
                            </td>
                            <td class="options-type">
                                <div class="reward-type chose" id="type_0" data-type="0">
                                    <!--类型0-->
                                    <table>
                                        <tbody>
                                        <!--标记-->
                                        <tr style="display: none;">
                                            <td colspan="2">
                                                <input type="button" value="天数标记"  id="level_data" level_data="1" >
                                            </td>
                                        </tr>
                                        <!--标记-->
                                        <tr>
                                            <td width="12%" class="td_title">累计次数</td>
                                            <td class="td_title">奖励配置</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center"><input class="level_num" type="number" name="level[0][num][1][1]" min="0" style="width: 100px"></td>
                                            <td>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td width="40%" class="td_title">道具ID</td>
                                                        <td width="15%" class="td_title">数量</td>
                                                        <td width="15%" class="td_title">角色</td>
                                                        <td style="border-bottom: 1px solid #000">操作</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center"><input class="level_item_id" type="text" name="level[0][item_id][1][1][]" value="" style="width: 250px"></td>
                                                        <td style="text-align: center"><input class="level_count" type="number" name="level[0][count][1][1][]" value="" style="width: 100px"></td>
                                                        <td style="text-align: center">
                                                            <select class="level_career" name="level[0][career][1][1][]">
                                                                <option value="0">通用</option>
                                                                <option value="1">刀</option>
                                                                <option value="2">弓</option>
                                                                <option value="3">法</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input style="margin: 3px" type="button" class="gbutton add_item" value="复制" />
                                                            <input style="margin: 3px" type="button" class="gbutton del_item" value="删除" />
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <input type="button" class="gbutton add_all_item" title="添加" value="添加道具奖励" add_item_num="1"
                                                       add_all_num="1" add_grade_num="1">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <input type="button" class="gbutton copy_type_item" title="复制" value="复制此类型奖励">
                                <input type="button" class="gbutton del_type_item" title="删除" value="删除此类型奖励">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr class="foot_award">
    <td width="10%" style="border: 1px solid #000;text-align: center">全服累计奖励</td>
    <td>
        <table>
            <tbody>
            <!--标记-->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="天数标记"  id="level_data2" level_data2="1" >
                </td>
            </tr>
            <!--标记-->
            <tr>
                <td width="10%" style="text-align: center;border-bottom: 1px solid #000">奖励类型</td>
                <td width="70%" style="text-align: center;border-bottom: 1px solid #000">奖励配置</td>
                <td style="border-bottom: 1px solid #000">操作</td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <select style="padding: 4px 2px;" class="chose-type">
                        <option value="1">经验</option>
                        <option value="2">副本掉落</option>
                        <option value="3">BOSS复活</option>
                    </select>
                </td>
                <td class="options-type">
                    <div class="reward-type chose" id="type_1" data-type="1">
                        <!--类型1-->
                        <table>
                            <tbody>
                            <tr>
                                <td width="12%" class="td_title">累计次数</td>
                                <td class="td_title">奖励配置</td>
                            </tr>
                            <tr>
                                <td style="text-align: center"><input class="level_num" type="number" name="level[1][num][1]" min="0" style="width: 100px"></td>
                                <td>
                                    <div style="line-height: 30px">
                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[1][time][1]" min="0" placeholder="请填写大于0的数字">&nbsp;秒<br/>
                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[1][per][1]" min="0" placeholder="请填写大于0的数字">&nbsp;倍
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="reward-type" id="type_2" data-type="2">
                        <!--类型2-->
                        <table>
                            <tbody>
                            <tr>
                                <td width="12%" class="td_title">累计次数</td>
                                <td width="25%" class="td_title">奖励配置</td>
                                <td class="td_title">场景配置</td>
                            </tr>
                            <tr>
                                <td style="text-align: center"><input class="level_num" type="number" name="level[2][num][1]" min="0" style="width: 100px"></td>
                                <td>
                                    <div style="line-height: 30px">
                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[2][time][1]" min="0" placeholder="请填写大于0的数字">&nbsp;秒<br/>
                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[2][per][1]" min="0" placeholder="请填写大于0的数字">&nbsp;倍
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <fieldset>
                                            <legend>场景列表</legend>
                                            <div id="pretime" style="line-height: 30px">
                                                <!--功能开关列表-->
                                                            <span class="action-are select" onclick="return false;" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked="checked" type="checkbox" name="level[2][action][1][]" value="3"/>
                                                            <label class="all-checked">单人副本：法器副本、内丹副本、洗练石副本</label></span>,
                                                            <span class="action-are select" onclick="return false;" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" readonly="readonly" checked="checked" type="checkbox" name="level[2][action][1][]" value="4"/>
                                                            <label class="all-checked">双人副本：组队打宝、技能经验</label></span>,
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="reward-type" id="type_3" data-type="3">
                        <!--类型3-->
                        <table>
                            <tbody>
                            <tr>
                                <td width="12%" class="td_title">累计次数</td>
                                <td class="td_title">BOSS类型</td>
                            </tr>
                            <tr>
                                <td style="text-align: center"><input class="level_num" type="number" value="" name="level[3][num][1]" min="0" style="width: 100px"></td>
                                <td>
                                    <div>
                                        <fieldset>
                                            <legend>场景列表</legend>
                                            <div style="line-height: 30px">
                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][1][]" value="1"/>
                                                            <label class="all-checked">世界BOSS</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" readonly="readonly" checked type="checkbox" name="level[3][action][1][]" value="2"/>
                                                            <label class="all-checked">BOSS之家</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" readonly="readonly" checked type="checkbox" name="level[3][action][1][]" value="3"/>
                                                            <label class="all-checked">虚无境地</label>
                                                            </span>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td>
                    <input type="button" class="gbutton copy_type2_item" title="复制" value="复制此类型奖励">
                    <input type="button" class="gbutton del_type2_item" title="删除" value="删除此类型奖励">
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->