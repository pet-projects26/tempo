<!--上上签-->
<tr>
    <td>每天免费次数</td>
    <td><input type="number" name="freeGold" value="1"></td>
</tr>
<tr>
    <td>求签一次花费元宝</td>
    <td><input type="number" name="once_gold"></td>
</tr>
<tr>
    <td>求签十次花费元宝</td>
    <td><input type="number" name="tentimes_gold"></td>
</tr>
<tr>
    <td>可抽签次数</td>
    <td><input type="number" name="buyCount" value="50"></td>
</tr>
<tr>
    <td style="width:150px;text-align: center">
        <label>世界等级</label>
        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1043.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <span style="margin-left: 180px;font-size: 15px;color: #ca1b36">PS:大奖需要配置多个，抽中大奖后，大奖更换，大奖更换可按填表配置的顺序进行更换当配置的大奖更换完后，则从新循环</span>
        <table>
            <tbody>
            <!--标记-->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="天数标记"  id="add_grade_num" add_grade_num="1" >
                </td>
            </tr>
            <!--标记-->
            <!--大奖-->
            <tr>
                <td width="12%" style="border: 1px solid #000;text-align: center">
                    大奖
                </td>
                <td>
                    <!--奖励配置-->
                    <table>
                        <tbody>
                        <tr>
                            <td style="border: 1px solid #000;text-align: center" width="10%">时装+装备</td>
                            <td>
                                <!--时装配置-->
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="25%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <tr>
                                        <!--配置-->
                                        <td style="text-align: center"><input type="text" name="top[item_id2][1][]" class="item_id" style="width:250px;"></td>
                                        <td style="text-align: center"><input type="number" name="top[count2][1][]" class="count"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[career2][1][]">
                                                <option value="1">刀</option>
                                                <option value="2">弓</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制">
                                            <input type="button" class="gbutton del_item" value="删除">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton c-additem top_dress_item" add_item_num="1" add_comsume_num="1" add_grade_num="1" value="添加奖励">
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #000;text-align: center" width="10%">通用道具</td>
                            <td>
                                <!--通用道具配置-->
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="25%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <tr>
                                        <!--配置-->
                                        <td style="text-align: center"><input type="text" name="top[item_id][1][]" class="item_id" style="width:250px;"></td>
                                        <td style="text-align: center"><input type="number" name="top[count][1][]" class="count"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[career][1][]">
                                                <option value="0">通用</option>
                                                <option value="1">刀</option>
                                                <option value="2">弓</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制">
                                            <input type="button" class="gbutton del_item" value="删除">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton c-additem top_normal_item" add_item_num="1" add_comsume_num="1" add_grade_num="1" value="添加奖励">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <!--普通奖励-->
            <tr>
                <td style="border: 1px solid #000;text-align: center">普通奖励</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td class="td_title">道具ID</td>
                            <td class="td_title">数量</td>
                            <td class="td_title">角色</td>
                            <td class="td_title">权重</td>
                            <td style="border-bottom: 1px solid #000;">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" style="width: 250px" name="day[item_id][1][]" class="item_id"></td>
                            <td style="text-align: center"><input type="number" name="day[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="day[career][1][]">
                                    <option value="0">通用</option>
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td style="text-align: center"><input type="number" name="day[weight][1][]" class="weight" /></td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem day_item-btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" value="添加奖励">
                </td>
            </tr>
            <!--剩余签数区间-->
            <tr>
                <td style="border: 1px solid #000;text-align: center">剩余签数区间<br/>(对应大奖权值)</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="20%" class="td_title">剩余次数区间</td>
                            <td width="20%" class="td_title">大奖权值</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="number" class="round" name="gold[round][1][]"></td>
                            <td style="text-align: center"><input type="number" class="weight" name="gold[weight][1][]"></td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem big_add_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" value="添加区间">
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>