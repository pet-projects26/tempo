{*每日登录 活动配置*}
<tr></tr>
<tr>
    <td style="width:150px">
        <label>世界等级</label>

        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1045.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tbody>
            <tr></tr>
            <!--标记-->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="天数标记"  id="comsume_data" comsume_data="1" >
                </td>
            </tr>
            <!--标记-->
            <tr>
                <td class="td_title" width="15%">登录天数</td>
                <td class="td_title">奖励配置</td>
                <td class="td_title">操作</td>
            </tr>
            <tr>
                <td>登录<input type="number" value="1" class="comsume" name="day" style="width: 60px; margin: 0 5px" >天</td>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td width="40%" class="td_title">道具ID</td>
                                <td width="20%" class="td_title">数量</td>
                                <td width="10%" class="td_title">角色</td>
                                <td class="td_title">操作</td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem" add_item_num="1" add_comsume_num="1" add_grade_num="1" value="添加道具奖励">
                </td>
                <td width="26%">
                    <input type="button" class="gbutton copy_comsume_level" value="复制该天数等级" />
                    <input type="button" class="gbutton del_comsume_level" value="删除该天数等级" />
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->