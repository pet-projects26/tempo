<!--新年祈愿-->
<tr>
    <td>每天免费次数</td>
    <td><input type="number" name="freeGold" value="1"></td>
</tr>
<tr>
    <td>祈愿一次花费的元宝</td>
    <td><input type="number" name="once_gold"></td>
</tr>
<tr>
    <td>祈愿一次可获得积分</td>
    <td><input type="number" name="item_need_count"></td>
</tr>
<tr>
    <td>祈愿十次花费元宝</td>
    <td><input type="number" name="tentimes_gold"></td>
</tr>
<tr>
    <td>祈愿十次可获得积分</td>
    <td><input type="number" name="item_exchange"></td>
</tr>
<tr>
    <td style="width:150px;text-align: center">
        <label>世界等级</label>
        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1047.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <span style="margin: 0 144px;color: #ca1b36;font-size: 15px">PS:游戏内抽奖获得物品总数量最多配置21项！（时装+装备和通用道具）</span>
        <table style="border: 1px solid #fff" border="1" >
            <tbody>
            <!--标记-->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="天数标记"  id="add_grade_num" add_grade_num="1" >
                </td>
            </tr>
            <!--标记-->
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center;">时装+装备</td>
                <td>
                    <table>
                        <tbody>
                        <!--top-->
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="10%" class="td_title">角色</td>
                            <td width="15%" class="td_title">权重</td>
                            <td width="10%" class="td_title">特效</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" name="top[item_id2][1][]" style="width: 250px" class="item_id"></td>
                            <td style="text-align: center"><input type="text" name="top[count2][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career2][1][]">
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td><input type="number" name="top[weight2][1][]" class="weight"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[value2][1][]">
                                    <option value="0">无</option>
                                    <option value="1">有</option>
                                </select>
                            </td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem top_dress_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加时装+装备奖励">
                </td>
            </tr>
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center;">通用道具</td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="10%" class="td_title">角色</td>
                            <td width="15%" class="td_title">权重</td>
                            <td width="10%" class="td_title">特效</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" name="top[item_id][1][]" style="width: 250px" class="item_id"></td>
                            <td style="text-align: center"><input type="text" name="top[count][1][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[career][1][]">
                                    <option value="0">通用</option>
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td><input type="number" name="top[weight][1][]" class="weight"></td>
                            <td style="text-align: center">
                                <select class="career" name="top[value][1][]">
                                    <option value="0">无</option>
                                    <option value="1">有</option>
                                </select>
                            </td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem top_normal_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加通用奖励">
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr class="foot_reward">
    <!--保底奖励-->
    <td style="text-align: center">
        积分商店<br/>
        奖励配置
    </td>
    <td>
        <table style="border: 1px solid #fff" border="1">
            <tbody>
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center;">时装+装备</td>
                <td>
                    <table>
                        <tbody>
                        <!--day-->
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="10%" class="td_title">角色</td>
                            <td width="15%" class="td_title">积分</td>
                            <td width="10%" class="td_title">限制次数</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" name="day[item_id2][]" style="width: 250px" class="item_id"></td>
                            <td style="text-align: center"><input type="text" name="day[count2][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="day[career2][]">
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td style="text-align: center"><input type="number" name="day[score2][]" class="score"></td>
                            <td style="text-align: center"><input type="number" name="day[limit2][]" placeholder="0:没有限制次数" class="limit"></td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton day_dress_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加时装+装备奖励">
                </td>
            </tr>
            <tr>
                <td width="10%" style="border: 1px solid #000;text-align: center;">通用道具</td>
                <td>
                    <table>
                        <tbody>
                        <!--day-->
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="10%" class="td_title">角色</td>
                            <td width="15%" class="td_title">积分</td>
                            <td width="10%" class="td_title">限制次数</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><input type="text" name="day[item_id][]" style="width: 250px" class="item_id"></td>
                            <td style="text-align: center"><input type="text" name="day[count][]" class="count"></td>
                            <td style="text-align: center">
                                <select class="career" name="day[career][]">
                                    <option value="0">通用</option>
                                    <option value="1">刀</option>
                                    <option value="2">弓</option>
                                </select>
                            </td>
                            <td style="text-align: center"><input type="number" name="day[score][]" class="score"></td>
                            <td style="text-align: center"><input type="number" name="day[limit][]" placeholder="0:没有限制次数" class="limit"></td>
                            <td>
                                <input type="button" class="gbutton add_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton day_normal_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加时装+装备奖励">
                </td>
            </tr>
            </tbody>
            </tbody>
        </table>
    </td>
</tr>