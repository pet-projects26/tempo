<!--充值排行榜-->
<tr>
    <td>最低充值</td>
    <td><input type="number" name="buyCount"></td>
</tr>

<tr>
    <td style="width:150px">
        <label>世界等级</label>

        <input type="text" name="grade[1]" value="" class="w-120 grade">
        <script type="text/javascript" src="templates/activity/activity_1051.js"></script>
        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
    <td>
        <table>
            <tbody>
            <tr></tr>
            <tr>
                <td width="30%" class="td_title">道具ID</td>
                <td width="15%" class="td_title">数量</td>
                <td width="10%" class="td_title">角色</td>
                <td style="border-bottom:1px solid #000">操作</td>
            </tr>
            <tr>
                <td style="text-align: center"><input type="text" name="item_id[1][]" style="width: 250px" class="item_id"></td>
                <td style="text-align: center"><input type="number" name="count[1][]" class="count"></td>
                <td style="text-align: center">
                    <select name="career[1][]" class="career">
                        <option value="0">通用</option>
                        <option value="1">刀</option>
                        <option value="2">弓</option>
                    </select>
                </td>
                <td>
                    <input type="button" class="gbutton copy_item" value="复制">
                    <input type="button" class="gbutton del_item" value="删除">
                </td>
            </tr>
            </tbody>
        </table>
        <input type="button" class="gbutton c-additem top_dress_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加道具">
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->