<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }
    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }
    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    #checkboxServerList .select {
        background-color: #dcd8d8;
    }
    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    .cur {
        background-color: beige;
    }


    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }
    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1034.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>" >聚划算</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>"></td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            </tbody>

            <tr>
                <td style="width:150px;">开启等级</td>
                <td><input type="text" name="openLvl" value="<{$row.openLvl}>" style="width:200px;"></td>
            </tr>
        </table>
        <table class="itable itable-color">
            <tbody id ="load">
            <{foreach from=$row.config.item_id key=k item=v}>
            <tr class="big-price">
                <td style="width:150px; text-align: center;">
                    <input type="button" value="添加礼包" class="gbutton w-120 c-add-level" style="margin: 5px 14px">
                    <input type="button" value="删除礼包" class="gbutton w-120 c-del-level" style="margin: 5px 14px">
                    <input type="button" value="复制礼包" class="gbutton w-120 c-copy-level" style="margin: 5px 14px">
                </td>
                <td>
                    <table style="width:100%;" class="table-activity-reward">
                        <tbody class="body-item">
                        <tr></tr>
                        <tr>
                            <td style="width:100px;border-bottom: 1px solid #000000">
                                礼包名称
                            </td>
                            <td style="border-bottom: 1px solid #000000">礼包内容</td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                原价
                            </td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                现价
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="price_name" type="text" value="<{$row.config.price_name[$k]}>" name="config[price_name][<{$k}>]">
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr></tr>
                                    <tr>
                                        <td>物品ID</td>
                                        <td>数量</td>
                                        <td>角色</td>
                                        <td>操作</td>
                                    </tr>
                                    <{foreach from=$v key=key2 item=value info=tem}>
                                    <tr>
                                        <td><input style="width:200px" type="text" name="config[item_id][<{$k}>][]" value="<{$value}>" class="config_item_id" autocomplete="on"></td>
                                        <td><input style="width:100px" type="number" name="config[item_count][<{$k}>][]" value="<{$row.config.item_count[$k][$key2]}>" class="config_item_count"></td>
                                        <td>
                                            <select name="config[career][<{$k}>][]" class="config_item_career">
                                                <option value="0" <{if $row.config.career[$k][$key2] eq 0}>selected<{/if}> >通用</option>
                                                <option value="1" <{if $row.config.career[$k][$key2] eq 1}>selected<{/if}> >刀</option>
                                                <option value="2" <{if $row.config.career[$k][$key2] eq 2}>selected<{/if}> >弓</option>
                                                <option value="3" <{if $row.config.career[$k][$key2] eq 3}>selected<{/if}> >法</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" value="删除道具" class="gbutton delitem" title="删除道具">
                                            <input type="button" value="复制道具" class="gbutton copyitem" title="复制道具">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="1"
                                       add_amount_num="1" add_grade_num="<{$k}>">
                            </td>
                            <td>
                                <input class="config_item_price_old" style="width: 80px" type="number" name="config[price_old][<{$k}>]" value="<{$row.config.price_old[$k]}>" />
                            </td>
                            <td>
                                <input class="config_item_price_now" style="width: 80px" type="number" name="config[price_now][<{$k}>]" value="<{$row.config.price_now[$k]}>" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <{/foreach}>
            <tr class="normal-price">
                <td style="text-align: center">
                    大礼包
                </td>
                <td class="add-normal-grad" add-normal-num="1">
                    <table>
                        <tbody>
                        <tr></tr>
                        <tr>
                            <td style="width: 100px;border-bottom: 1px solid #000000">礼包名称</td>
                            <td style="border-bottom: 1px solid #000000">礼包内容</td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                原价
                            </td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                现价
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="big_price_name" value="<{$row.award.big_price_name}>" /></td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr></tr>
                                    <tr>
                                        <td>物品ID</td>
                                        <td>数量</td>
                                        <td>职业</td>
                                        <td>操作</td>
                                    </tr>
                                    <{foreach from=$row.award.item_id key=ky item=vl}>
                                    <tr>
                                        <td>
                                            <input style="width:200px" type="text" name="item_id[]" value="<{$vl}>" class="config_item_id" autocomplete="on">
                                        </td>
                                        <td>
                                            <input style="width:100px" type="number" name="item_count[]" value="<{$row.award.item_count[$ky]}>" class="config_item_count">
                                        </td>
                                        <td>
                                            <select name="career[]" class="item_career">
                                                <option value="0" <{if $row.award.career[$ky] eq 0}>selected<{/if}> >通用</option>
                                                <option value="1" <{if $row.award.career[$ky] eq 1}>selected<{/if}> >刀</option>
                                                <option value="2" <{if $row.award.career[$ky] eq 2}>selected<{/if}> >弓</option>
                                                <option value="3" <{if $row.award.career[$ky] eq 3}>selected<{/if}> >法</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" value="删除道具" class="gbutton delitem" title="删除道具">
                                            <input type="button" value="复制道具" class="gbutton copyitem" title="复制道具">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton b-additem" add_item_num="1"
                                       add_amount_num="1" add_grade_num="1">
                            </td>
                            <td>
                                <input class="item_price_old" style="width: 80px" type="number" name="price_old" value="<{$row.award.price_old}>" />
                            </td>
                            <td>
                                <input class="item_price_now" style="width: 80px" type="number" name="price_now" value="<{$row.award.price_now}>">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.config.item_id}>" >
                </td>
            </tr>
            <!-- 标记 -->
            </tbody>
        </table>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>

            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->

            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
