<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }
    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }
    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    #checkboxServerList .select {
        background-color: #dcd8d8;
    }
    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    .cur {
        background-color: beige;
    }


    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }
    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1035.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>" >幸运大转盘</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>"></td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            </tbody>

            <tr>
                <td style="width:150px;">开启等级</td>
                <td><input type="text" name="openLvl" value="<{$row.openLvl}>" style="width:200px;"></td>
            </tr>
            <tr>
                <td>奖池元宝数</td>
                <td>
                    <input type="number" class="read_only" readonly="readonly" value="10000">
                </td>
            </tr>
            <tr>
                <td>抽一次需要的元宝</td>
                <td>
                    <input type="number" value="<{$row.award.once_gold}>" name="once_gold" />
                </td>
            </tr>
            <tr>
                <td>抽十次需要的元宝</td>
                <td>
                    <input type="number" value="<{$row.award.tentimes_gold}>" name="tentimes_gold">
                </td>
            </tr>
            <tr>
                <td>获奖记录</td>
                <td>
                    <input type="number" class="read_only" readonly="readonly" value="50">
                </td>
            </tr>
            <tr>
                <td>抽一次可获积分</td>
                <td>
                    <input type="number" name="once_score" value="<{$row.award.once_gold}>">
                </td>
            </tr>
            <tr>
                <td>单次抽奖进入奖池的元宝</td>
                <td>
                    <input type="number" name="input_gold" value="<{$row.award.input_gold}>">
                </td>
            </tr>
            <tr>
                <td>新服</td>
                <td>
                    <input type="number" name="new_server" value="<{$row.award.new_server}>">&nbsp;&nbsp;天
                </td>
            </tr>
        </table>
        <table class="itable itable-color">
            <tbody id ="load">
            <tr>
                <td style="width: 10%">普通奖励</td>
                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">

                        <tbody class="body-item">
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table class="itemTable">
                                                <thead>
                                                <tr>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>权重</td>
                                                    <td>角色</td>
                                                    <td>操作</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <{foreach from=$row.config.item_id key=key item=item_id}>
                                                <tr>
                                                    <td>
                                                        <input style="width:200px" type="text" name="config[item_id][]" class="config_item_id" value="<{$item_id}>" autocomplete="on">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="config[item_count][]" value="<{$row.config.item_count[$key]}>" class="config_item_count" />
                                                    </td>
                                                    <td>
                                                        <input type="text" name="config[weight][]" value="<{$row.config.weight[$key]}>" class="config_item_weight" />
                                                    </td>
                                                    <td>
                                                        <select name="config[career][]" class="config_item_career">
                                                            <option value="0" <{if $row.config.career[$key] eq 0}>selected<{/if}> >通用</option>
                                                            <option value="1" <{if $row.config.career[$key] eq 1}>selected<{/if}> >刀</option>
                                                            <option value="2" <{if $row.config.career[$key] eq 2}>selected<{/if}> >弓</option>
                                                            <option value="3" <{if $row.config.career[$key] eq 3}>selected<{/if}> >法</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="button" value="删除" class="gbutton delitem" title="删除该行">
                                                        <input type="button" value="复制" class="gbutton copyitem" title="复制该行">
                                                    </td>
                                                </tr>
                                                <{/foreach}>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>
            <tr class="tr-extra">
                <td>积分兑换</td>
                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">
                        <thead>
                        <tr>
                            <th style="width: 16%">条件</th>
                            <th>奖励</th>
                            <th>删除</th>
                        </tr>
                        </thead>
                        <tbody class="body-item">
                        <tr>
                          <!--  <td style="width: 100px">
                                <input type="number" style="width: 80px" class="score" name="score_num" value="<{$row.award.score_num}>" />&nbsp;&nbsp;积分
                            </td>-->
                            <td>
                                <table>
                                    <tdbody>
                                        <tr>
                                            <td>
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <td width="">积分</td>
                                                        <td width="">物品ID</td>
                                                        <td width="">数量</td>
                                                        <td width="">职业</td>
                                                        <td width="">操作</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <{foreach from=$row.itemId key=key item=item_id}>
                                                    <tr>
                                                        <td>
                                                            <input style="width: 80px" type="text" name="score_num[]" value="<{$row.score_num[$key]}>" class="score" >
                                                        </td>
                                                        <td>
                                                            <input style="width: 200px" type="text" name="item_id[]" value="<{$item_id}>" class="item_id" autocomplete="on">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="item_count[]" value="<{$row.itemCount[$key]}>" class="item_count">
                                                        </td>
                                                        <td>
                                                            <select name="career[]">
                                                                <option value="0" <{if $row.career[$key] eq 0}>selected<{/if}> >通用</option>
                                                                <option value="1" <{if $row.career[$key] eq 1}>selected<{/if}> >刀</option>
                                                                <option value="2" <{if $row.career[$key] eq 2}>selected<{/if}> >弓</option>
                                                                <option value="3" <{if $row.career[$key] eq 3}>selected<{/if}> >法</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="button" value="删除" class="gbutton delitem" title="删除该行">
                                                            <input type="button" value="复制" class="gbutton copyitem" title="复制该行">
                                                        </td>
                                                    </tr>
                                                    <{/foreach}>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tdbody>
                                </table>
                                <input type="button" value="添加物品奖励" class="gbutton c-extra-additem" add_amount_num="1" add_grad_num="1">
                            </td>
                            <td>
                                <input type="button" value="删除该条件" class="gbutton del-reward" />
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.config.item_id}>" >
                </td>
            </tr>
            <!-- 标记 -->
            </tbody>
        </table>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>

            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->

            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
