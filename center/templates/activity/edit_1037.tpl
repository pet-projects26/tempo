<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
    }
    .w-80{
        width: 80px;
        margin: 5px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1037.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">摇摇乐</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            <tr>
                <td>摇一次所需幸运宝符或元宝</td>
                <td>
                    道具ID &nbsp;<input type="text" class="roll_one_id config_item_id" name="roll_one_id" style="width: 250px" value="<{$row.award.open_hour}>" />
                    道具数量 &nbsp;<input type="number" min="0" name="roll_one_num" style="width: 80px" value="<{$row.award.item_id|default:1}>" >
                    元宝数量 &nbsp;<input type="number" min="0" name="gold_one_num" style="width: 80px" value="<{$row.award.close_hour}>">
                </td>
            </tr>
            <tr>
                <td>摇十次所需幸运宝符或元宝</td>
                <td>
                    道具ID &nbsp;<input type="text" class="roll_ten_id config_item_id" name="roll_ten_id" style="width: 250px" value="<{$row.award.open_min}>" />
                    道具数量 &nbsp;<input type="number" min="0" name="roll_ten_num" style="width: 80px"  value="<{$row.award.item_count|default:1}>">
                    元宝数量 &nbsp;<input type="number" min="0" name="gold_ten_num" style="width: 80px" value="<{$row.award.close_min}>">
                </td>
            </tr>
            <tr>
                <td>怪物掉落</td>
                <td>
                    怪物的等级 &nbsp;<input type="number" min="0" value="<{$row.award.monsterId}>" style="width:110px;" name="monsterId"> 掉落ID &nbsp;<input type="text" readonly="readonly" style="background-color: #c3c3c3" value="<{$row.award.dropItemId}>" name="dropItemId">
                </td>
            </tr>
            <{foreach from=$row.award.grade key=level item=info}>
                <tr>
                    <td style="width:150px">
                        <label>世界等级</label>
                        <input type="text" name="grade[<{$level}>]" value="<{$info}>" class="w-120 grade">
                        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                    </td>
                    <td>
                        <table>
                            <tbody>
                            <tr>
                                <td style="text-align: center;border: 1px solid #000" width="10%">大奖</td>
                                <td>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td width="30%" class="td_title">道具ID</td>
                                            <td width="15%" class="td_title">数量</td>
                                            <td width="15%" class="td_title">角色</td>
                                            <td style="border-bottom: 1px solid #000">权重</td>
                                        </tr>
                                        <{foreach from=$row.award.top.item_id[$level] key=top_key item=top_item}>
                                            <tr>
                                                <td style="text-align: center"><input type="text" style="width: 250px" name="top[item_id][<{$level}>][]" class="item_id" value="<{$top_item}>"></td>
                                                <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="top[count][<{$level}>][]" class="count" value="<{$row.award.top.count[$level][$top_key]}>"></td>
                                                <td style="text-align: center">
                                                    <select name="top[career][<{$level}>][]" class="career">
                                                        <option value="0" <{if $row.award.top.career[$level][$top_key] eq 0}>selected<{/if}> >通用</option>
                                                        <option value="1" <{if $row.award.top.career[$level][$top_key] eq 1}>selected<{/if}> >刀</option>
                                                        <option value="2" <{if $row.award.top.career[$level][$top_key] eq 2}>selected<{/if}> >弓</option>
                                                        <option value="3" <{if $row.award.top.career[$level][$top_key] eq 3}>selected<{/if}> >法</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="number" style="width: 100px" name="top[weight][<{$level}>][]" class="weight" min="0" value="<{$row.award.top.weight[$level][$top_key]}>" />
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center;border: 1px solid #000" width="10%">普通奖励</td>
                                <td>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td width="30%" class="td_title">道具ID</td>
                                            <td width="15%" class="td_title">数量</td>
                                            <td width="15%" class="td_title">角色</td>
                                            <td width="15%" class="td_title">权重</td>
                                            <td style="border-bottom: 1px solid #000">操作</td>
                                        </tr>
                                        <{foreach from=$row.award.day.item_id[$level] key=day_key item=day_item}>
                                            <tr>
                                                <td style="text-align: center"><input type="text" style="width: 250px" name="day[item_id][<{$level}>][]" class="item_id" value="<{$day_item}>"></td>
                                                <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="day[count][<{$level}>][]" class="count" value="<{$row.award.day.count[$level][$day_key]}>"></td>
                                                <td style="text-align: center">
                                                    <select name="day[career][<{$level}>][]" class="career">
                                                        <option value="0" <{if $row.award.day.career[$level][$day_key] eq 0}>selected<{/if}> >通用</option>
                                                        <option value="1" <{if $row.award.day.career[$level][$day_key] eq 1}>selected<{/if}> >刀</option>
                                                        <option value="2" <{if $row.award.day.career[$level][$day_key] eq 2}>selected<{/if}> >弓</option>
                                                        <option value="3" <{if $row.award.day.career[$level][$day_key] eq 3}>selected<{/if}> >法</option>
                                                    </select>
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="number" style="width: 100px" name="day[weight][<{$level}>][]" class="weight" min="0" value="<{$row.award.day.weight[$level][$day_key]}>" />
                                                </td>
                                                <td>
                                                    <input type="button" class="gbutton add_item" value="复制" title="复制">
                                                    <input type="button" class="gbutton del_item" value="删除" title="删除">
                                                </td>
                                            </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                    <input type="button" class="gbutton add_day_item" value="添加普通奖励" add_item_num="1"
                                           add_comsume_num="1" add_grade_num="<{$level}>">
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center;border: 1px solid #000" width="10%">累计奖励</td>
                                <td>
                                    <table>
                                        <tbody>
                                        <tr></tr>
                                        <!--标记-->
                                        <tr style="display: none;">
                                            <td colspan="2">
                                                <input type="button" value="天数标记"  id="comsume_data" comsume_data="<{getMaxKey arr=$row.award.config.vip_level[$level]}>" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="12%" class="td_title">个人次数</td>
                                            <td width="60%" class="td_title">奖励配置</td>
                                            <td style="border-bottom: 1px solid #000">操作</td>
                                        </tr>
                                        <{foreach from=$row.award.config.vip_level[$level] key=v_key item=v_level}>
                                            <tr>
                                                <td style="text-align: center"><input type="number" class="vip_level" name="config[vip_level][<{$level}>][<{$v_key}>]" value="<{$v_level}>" min="0" style="width: 100px"></td>
                                                <td>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td width="40%" class="td_title">道具ID</td>
                                                            <td width="15%" class="td_title">数量</td>
                                                            <td width="15%" class="td_title">角色</td>
                                                            <td style="border-bottom: 1px solid #000">操作</td>
                                                        </tr>
                                                        <{foreach from=$row.award.config.item_id[$level][$v_key] key=key2 item=item_id}>
                                                        <tr>
                                                            <td style="text-align: center"><input type="text" style="width: 250px" name="config[item_id][<{$level}>][<{$v_key}>][]" value="<{$item_id}>" class="item_id"></td>
                                                            <td style="text-align: center"><input type="number" min="0" style="width: 100px" name="config[count][<{$level}>][<{$v_key}>][]" value="<{$row.award.config.count[$level][$v_key][$key2]}>" class="count"></td>
                                                            <td style="text-align: center">
                                                                <select name="config[career][<{$level}>][<{$v_key}>][]" class="career">
                                                                    <option value="0" <{if $row.award.day.career[$level][$v_key][$key2] eq 0}>selected<{/if}> >通用</option>
                                                                    <option value="1" <{if $row.award.day.career[$level][$v_key][$key2] eq 1}>selected<{/if}> >刀</option>
                                                                    <option value="2" <{if $row.award.day.career[$level][$v_key][$key2] eq 2}>selected<{/if}> >弓</option>
                                                                    <option value="3" <{if $row.award.day.career[$level][$v_key][$key2] eq 3}>selected<{/if}> >法</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="button" style="margin: 3px 0" class="gbutton add_item" value="复制" title="复制">
                                                                <input type="button" style="margin: 3px 0" class="gbutton del_item" value="删除" title="删除">
                                                            </td>
                                                        </tr>
                                                        <{/foreach}>
                                                        </tbody>
                                                    </table>
                                                    <input type="button" class="gbutton add_amount_item" title="添加" value="添加道具奖励" add_item_num="1"
                                                           add_comsume_num="<{$v_key}>" add_grade_num="<{$level}>">
                                                </td>
                                                <td>
                                                    <input type="button" style="margin: 3px 0" class="gbutton copy_amount_level" title="复制" value="复制该等级奖励">
                                                    <input type="button" style="margin: 3px 0" class="gbutton del_amount_level" title="删除" value="删除该等级奖励">
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                        </tbody>
                                    </table>
                                </td>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <{/foreach}>
            </tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->
            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
        </table>
    </form>
</div>
