<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
        text-align: center;
    }
    .w-80{
        width: 80px;
        margin: 5px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1039.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">幸运宝箱</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            <tr></tr>
                <{foreach from=$row.award.grade key=level item=info}>
            <tr>
                <td style="width:150px">
                    <label>世界等级</label>
                    <input type="text" name="grade[<{$level}>]" value="<{$info}>" class="w-120 grade">
                    <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                    <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                </td>
                <td>
                    <table class="" style="border: 2px solid #FFFFFF" border="1" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td width="15%">累计充值目标</td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="30%" class="td_title">充值金额</td>
                                        <td style="border-bottom: 1px solid #000000;" align="char">奖励配置</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <input type="number" name="comsume[<{$level}>][1]" class="comsume ui-autocomplete-input" autocomplete="on" style="width: 120px" value="<{$row.award.config[$level][1]}>">
                                        </td>
                                        <td>
                                            道具ID&nbsp;&nbsp;<input type="text" style="width: 200px;background-color: #c3c3c3" class="item_id ui-autocomplete-input" readonly="readonly" autocomplete="on" name="item_id[<{$level}>][1]" value="3200030000012/金钥匙">
                                            数量&nbsp;&nbsp;<input type="number" class="w-80 item_count" name="item_count[<{$level}>][1]" value="<{$row.award.item_count[$level][1]}>">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>累计消费目标</td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="30%" class="td_title">消费金额</td>
                                        <td style="border-bottom: 1px solid #000000;" align="char">奖励配置</td>
                                    </tr>
                                    <!--配置部分-->
                                    <tr>
                                        <td style="text-align: center"><input type="number" name="comsume[<{$level}>][2]" class="comsume" style="width:120px;" value="<{$row.award.config[$level][2]}>"></td>
                                        <td>
                                            道具ID&nbsp;&nbsp;<input type="text" style="width: 200px;background-color: #c3c3c3" class="item_id" readonly="readonly" name="item_id[<{$level}>][2]" value="3200030000013/银钥匙">
                                            数量&nbsp;&nbsp;<input type="number" class="w-80 item_count" name="item_count[<{$level}>][2]" value="<{$row.award.item_count[$level][2]}>">
                                        </td>
                                    </tr>
                                    <!--------->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>宝箱开出元宝条件</td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="30%" class="td_title">元宝个数</td>
                                        <td width="30%" class="td_title">权重</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <!--配置部分--->
                                    <{foreach from=$row.award.mapRange[$level][1] key=rkey item=rinfo}>
                                    <tr>
                                        <td style="text-align: center">
                                            <input type="number" class="w-80 mapRange" name="mapRange[<{$level}>][1][]" value="<{$rinfo}>"/>-
                                            <input type="number" class="w-80 mapRange" name="mapRange[<{$level}>][2][]" value="<{$row.award.mapRange[$level][2][$rkey]}>"/>
                                        </td>
                                        <td style="text-align: center">
                                            <input type="number" class="weight" name="weight[<{$level}>][]" value="<{$row.award.weight[$level][$rkey]}>">
                                        </td>
                                        <td>
                                            <input type="button" class="add_item gbutton" title="复制" value="复制">
                                            <input type="button" class="del_item gbutton" title="删除" value="删除">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    <!------------>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton add_gold_pic" add_item_num="0" add_amount_num="0" add_grade_num="<{$level}>" value="添加配置条件">
                            </td>
                        </tr>

                        <tr>
                            <td>开启最多宝箱奖励</td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="30%" class="td_title">道具ID</td>
                                        <td width="14%" class="td_title">数量</td>
                                        <td width="14%" class="td_title">角色</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <!--配置部分-->
                                    <{foreach from=$row.award.itemId[$level] key=ikey item=mitem}>
                                    <tr>
                                        <td style="text-align: center"><input type="text" class="itemId" style="width: 200px" name="itemId[<{$level}>][]" value="<{$mitem}>"></td>
                                        <td style="text-align: center"><input type="number" name="count[<{$level}>][]" class="w-80 count" value="<{$row.award.count[$level][$ikey]}>"></td>
                                        <td style="text-align: center">
                                            <select name="career[<{$level}>][]" class="career">
                                            <option value="0" <{if $row.award.career[$level][$ikey] eq 0}>selected<{/if}> >通用</option>
                                            <option value="1" <{if $row.award.career[$level][$ikey] eq 1}>selected<{/if}> >刀</option>
                                            <option value="2" <{if $row.award.career[$level][$ikey] eq 2}>selected<{/if}> >弓</option>
                                            <option value="3" <{if $row.award.career[$level][$ikey] eq 3}>selected<{/if}> >法</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="add_item gbutton" title="复制" value="复制">
                                            <input type="button" class="del_item gbutton" title="删除" value="删除">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    <!-------->
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton add_box_pic" add_item_num="0" add_amount_num="0" add_grade_num="<{$level}>" value="添加配置条件">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            <tr>
                <{/foreach}>

            </tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->
            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
        </table>
    </form>
</div>