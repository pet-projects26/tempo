<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
    }
    .w-80{
        width: 80px;
        margin: 5px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1040.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">充值返利</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            <tr></tr>
                <{foreach from=$row.award.grade key=level item=info}>
                    <tr>
                        <td style="width:150px">
                            <label>世界等级</label>
                            <input type="text" name="grade[<{$level}>]" value="<{$info}>" class="w-120 grade">
                            <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                            <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                        </td>
                        <td>
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="10%" class="td_title">返利次数</td>
                                                <td width="10%" class="td_title">单笔充值</td>
                                                <td width="22%" class="td_title">充值范围</td>
                                                <td width="10%" class="td_title">返利%(只是显示,一般配置最低的)</td>
                                                <td width="25%" class="td_title">返利百分比（游戏内按照下面顺序显示返利比例）</td>
                                                <td class="td_title">操作</td>
                                            </tr>
                                            <{foreach from=$row.award.itemId[$level] key=key item=info}>
                                                <tr>
                                                    <td style="text-align: center"><input type="number" value="<{$info}>" class="item_id" name="itemId[<{$level}>][]" placeholder="填写-1为无限制"></td>
                                                    <td style="text-align: center"><input type="number" value="<{$row.award.buyCount[$level][$key]}>" class="w-80 buyCount" name="buyCount[<{$level}>][]" placeholder="元"></td>
                                                    <td style="text-align: center">
                                                        <input type="number" class="w-80 random1" value="<{$row.award.random[$level][1][$key]}>" name="random[<{$level}>][1][]" placeholder="元">~
                                                        <input type="number" class="w-80 random1" value="<{$row.award.random[$level][2][$key]}>" name="random[<{$level}>][2][]" placeholder="元">
                                                    </td>
                                                    <td style="text-align: center"><input type="number" class="w-80 config" name="config[<{$level}>][]" placeholder="返利" value="<{$row.award.config[$level][$key]}>"></td>
                                                    <td style="text-align: center">
                                                        <textarea style="width: 300px;height: 50px" class="item_count" name="count[<{$level}>][]" placeholder="如：第1次返利10%，则填写[10]&#10如：第1次返利10%，第2次返利20%，则填写[10,20]"><{$row.award.count[$level][$key]}></textarea>
                                                    </td>
                                                    <td>
                                                        <input type="button" class="gbutton copy_comsume_level" title="复制" value="复制此栏目">
                                                        <input type="button" class="gbutton del_comsume_level" title="删除" value="删除此栏目">
                                                    </td>
                                                </tr>
                                            <{/foreach}>
                                            </tbody>
                                        </table>
                                        <input type="button" class="gbutton c-additem" add_item_num="1" add_comsume_num="1" add_grade_num="<{$level}>" title="添加" value="添加充值档次">
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                        </td>
                    </tr>
                <{/foreach}>
            </tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->
            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
        </table>
    </form>
</div>