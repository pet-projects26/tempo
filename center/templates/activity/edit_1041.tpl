<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1041.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">消费有礼</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>

            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            </tbody>
        </table>
        <table class="itable itable-color">
            <tbody>
            <{foreach from=$row.award.grade key=level item=info}>
                <tr>
                    <td style="width: 150px">
                        <label>世界等级</label>
                        <input type="text" name="grade[<{$level}>]" value="<{$info}>" class="w-120 grade">
                        <input type="button" value="删除世界等级" class="gbutton del-level w-120" />
                        <input type="button" value="复制世界等级" class="gbutton c-copy-level w-120" />
                    </td>
                    <td>
                        <table>
                            <tbody>
                            <tr style="display: none;">
                                <td colspan="2">
                                    <input type="button" value="元宝等级标记"  id="comsume_data" comsume_data="<{getMaxKey arr=$row.award.config[$level]}>" >
                                </td>
                            </tr>
                            <tr>
                                <td style="width:100px;border-bottom: 1px solid #000000">消费元宝数</td>
                                <td style="border-bottom: 1px solid #000000">配置奖励</td>
                                <td style="width:22%;border-bottom: 1px solid #000000">操作</td>
                            </tr>
                            <{foreach from=$row.award.itemId[$level] key=keys item=item_info}>
                                <tr>
                                    <td>
                                        <input class="comsume" type="number" style="width: 100px" name="comsume[<{$level}>][<{$keys}>]" value="<{$row.award.config[$level][$keys]}>">
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr></tr>
                                            <tr>
                                                <td style="width: 28%">物品ID</td>
                                                <td>数量</td>
                                                <td>角色</td>
                                                <td>操作</td>
                                            </tr>
                                                <{foreach from=$item_info key=key2 item=itemMsg}>
                                                    <tr>
                                                        <td width="25%"><input type="text" style="width: 200px" name="itemId[<{$level}>][<{$keys}>][]" class="item_id ui-autocomplete-input" value="<{$itemMsg}>"></td>
                                                        <td width="10%"><input type="number" class="item_count" name="count[<{$level}>][<{$keys}>][]" value="<{$row.award.count[$level][$keys][$key2]}>"></td>
                                                        <td>
                                                            <select name="career[<{$level}>][<{$keys}>][]" class="career">
                                                                <option value="0" <{if $row.award.career[$level][$keys][$key2] eq 0}>selected<{/if}> >通用</option>
                                                                <option value="1" <{if $row.award.career[$level][$keys][$key2] eq 1}>selected<{/if}> >刀</option>
                                                                <option value="2" <{if $row.award.career[$level][$keys][$key2] eq 2}>selected<{/if}> >弓</option>
                                                                <option value="3" <{if $row.award.career[$level][$keys][$key2] eq 3}>selected<{/if}> >法</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="button" class="gbutton add_item" name="add_item" value="复制" />
                                                            <input type="button" class="gbutton del_item" name="del_item" value="删除">
                                                        </td>
                                                    </tr>
                                                <{/foreach}>
                                            </tbody>
                                        </table>
                                        <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="1"
                                               add_comsume_num="<{$keys}>" add_grade_num="<{$level}>">
                                    </td>
                                    <td>
                                        <input type="button" class="gbutton copy_comsume_level" value="复制消费等级" />
                                        <input type="button" class="gbutton del_comsume_level" value="删除消费等级" />
                                    </td>
                                </tr>
                            <{/foreach}>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
            <{/foreach}>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->
            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>