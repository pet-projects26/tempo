<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
    }
    .reward-type{
        display: none;
    }
    .chose{
        display: block;
    }
    .all-checked {
        vertical-align: middle;
    }
    .select{
        background-color: #36ca95;
    }
    .highlight {
        background: yellow;
        color: red;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1042.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">堆雪人</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            <tr>
                <td>掉落</td>
                <td>
                    怪物等级 &nbsp;<input type="number" name="monsterLevel" value="<{$row.award.monsterLevel}>" >
                    掉落Id &nbsp;<input type="number" name="dropItemId" value="<{$row.award.dropItemId | default:10007}>">
                </td>
            </tr>
            <tr>
                <td>元宝捐献所需金额</td>
                <td><input type="number" name="freeGold" aria-valuemin="0" value="<{$row.award.freeGold}>"></td>
            </tr>

            <tr>
                <td>材料提交奖励（游戏内展现）</td>
                <td>
                    <table style="border-collapse: collapse;border-spacing: 0">
                        <tbody>
                        <tr>
                            <td width="18%">道具ID</td>
                            <td width="8%">角色</td>
                            <td>操作</td>
                        </tr>
                        <{foreach from=$row.award.config.item_id key=con_key item=con_item}>
                        <tr>
                            <td width="18%"><input type="text" class="config_item_id" name="config[item_id][]" style="width: 230px" value="<{$con_item}>"></td>
                            <td>
                                <select class="career" name="config[career][]">
                                    <option <{if $row.award.config.career[$con_key] eq 0}>selected<{/if}> value="0">通用</option>
                                    <option <{if $row.award.config.career[$con_key] eq 1}>selected<{/if}> value="1">刀</option>
                                    <option <{if $row.award.config.career[$con_key] eq 2}>selected<{/if}> value="2">弓</option>
                                    <option <{if $row.award.config.career[$con_key] eq 3}>selected<{/if}> value="3">法</option>
                                </select>
                            </td>
                            <td><input type="button" class="del_item gbutton" value="删除"></td>
                        </tr>
                        <{/foreach}>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton add_show_item" value="添加显示奖励" />
                </td>
            </tr>
            <{foreach from=$row.award.grade key=level item=info}>
            <tr>
                <td style="width:150px">
                    <label>世界等级</label>

                    <input type="text" name="grade[<{$level}>]" value="<{$info}>" class="w-120 grade">
                    <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                    <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                </td>
                <td>
                    <table style="border: 2px solid #FFFFFF" border="1">
                        <tbody>
                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center">提交材料奖励</td>
                            <td>
                                <table border="0">
                                    <tbody>
                                    <tr></tr>
                                    <!--标记-->
                                    <tr style="display: none;">
                                        <td colspan="2">
                                            <input type="button" value="天数标记"  id="drop_data" drop_data="<{getMaxKey arr=$row.award.day.drop_id[$level]}>" >
                                        </td>
                                    </tr>
                                    <!--标记-->
                                    <tr>
                                        <td width=18%" class="td_title">掉落道具ID</td>
                                        <td width="65%" class="td_title">奖励配置</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.day.item_id[$level] key=day_key item=day_itemInfo}>
                                    <tr>
                                        <td style="text-align: center"><input type="text" class="item_id drop" name="day[drop_id][<{$level}>][<{$day_key}>]" min="0" style="width: 200px" value="<{$row.award.day.drop_id[$level][$day_key]}>"></td>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td width="40%" class="td_title">道具ID</td>
                                                    <td width="15%" class="td_title">数量</td>
                                                    <td width="15%" class="td_title">角色</td>
                                                    <td width="15%" class="td_title">权重</td>
                                                    <td style="border-bottom: 1px solid #000">操作</td>
                                                </tr>
                                                <{foreach from=$day_itemInfo key=k item=day_item}>
                                                <tr>
                                                    <td style="text-align: center"><input type="text" style="width: 250px" name="day[item_id][<{$level}>][<{$day_key}>][]" value="<{$day_item}>" class="item_id"></td>
                                                    <td style="text-align: center"><input type="number" style="width: 100px" name="day[count][<{$level}>][<{$day_key}>][]" value="<{$row.award.day.count[$level][$day_key][$k]}>" class="count"></td>
                                                    <td style="text-align: center;">
                                                        <select class="career" name="day[career][<{$level}>][<{$day_key}>][]">
                                                            <option <{if $row.award.day.career[$level][$day_key][$k] eq 0}>selected<{/if}> value="0">通用</option>
                                                            <option <{if $row.award.day.career[$level][$day_key][$k] eq 1}>selected<{/if}> value="1">刀</option>
                                                            <option <{if $row.award.day.career[$level][$day_key][$k] eq 2}>selected<{/if}> value="2">弓</option>
                                                            <option <{if $row.award.day.career[$level][$day_key][$k] eq 3}>selected<{/if}> value="3">法</option>
                                                        </select>
                                                    </td>
                                                    <td style="text-align: center"><input type="number" style="width: 100px" name="day[weight][<{$level}>][<{$day_key}>][]" value="<{$row.award.day.weight[$level][$day_key][$k]}>" class="weight"></td>
                                                    <td>
                                                        <input type="button" class="gbutton add_item" value="复制" />
                                                        <input type="button" class="gbutton del_item" value="删除" />
                                                    </td>
                                                </tr>
                                                <{/foreach}>
                                                </tbody>
                                            </table>
                                            <input type="button" class="gbutton add_drop_item" title="添加" value="添加道具奖励" add_item_num="1"
                                                   add_drop_num="<{$day_key}>" add_grade_num="<{$level}>">
                                        </td>
                                        <td>
                                            <input type="button" style="margin: 3px 3px" class="gbutton copy_drop_level" title="复制" value="复制该等级">
                                            <input type="button" style="margin: 3px 3px" class="gbutton del_drop_level" title="删除" value="删除该等级">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center">花费元宝提交一次</td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td width="10%" class="td_title">权重</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.gold.item_id[$level] key=gold_key item=gold_itemInfo}>
                                    <tr>
                                        <td style="text-align: center"><input type="text" value="<{$gold_itemInfo}>" style="width: 250px" class="item_id" name="gold[item_id][<{$level}>][]""></td>
                                        <td style="text-align: center"><input type="number" value="<{$row.award.gold.count[$level][$gold_key]}>" style="width: 100px" class="count" name="gold[count][<{$level}>][]"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="gold[career][<{$level}>][]">
                                                <option <{if $row.award.gold.career[$level][$gold_key] eq 0}>selected<{/if}> value="0">通用</option>
                                                <option <{if $row.award.gold.career[$level][$gold_key] eq 1}>selected<{/if}> value="1">刀</option>
                                                <option <{if $row.award.gold.career[$level][$gold_key] eq 2}>selected<{/if}> value="2">弓</option>
                                                <option <{if $row.award.gold.career[$level][$gold_key] eq 3}>selected<{/if}> value="3">法</option>
                                            </select>
                                        </td>
                                        <td style="text-align: center"><input type="number" value="<{$row.award.gold.weight[$level][$gold_key]}>" style="width: 100px" name="gold[weight][<{$level}>][]" class="weight"></td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制" />
                                            <input type="button" class="gbutton del_item" value="删除" />
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton add_gold_item" value="添加道具奖励" add_item_num="1"
                                       add_drop_num="1" add_grade_num="<{$level}>">
                            </td>
                        </tr>

                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center">个人累计奖励</td>
                            <td>
                                <table>
                                    <tbody>
                                    <!--标记-->
                                    <tr style="display: none;">
                                        <td colspan="2">
                                            <input type="button" value="天数标记"  id="top_data" top_data="<{getMaxKey arr=$row.award.top.per[$level]}>" >
                                        </td>
                                    </tr>
                                    <!--标记-->
                                    <tr>
                                        <td width=12%" class="td_title">个人累计次数</td>
                                        <td width="68%" class="td_title">奖励配置</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.top.item_id[$level] key=top_key item=top_itemInfo}>
                                    <tr>
                                        <td style="text-align: center"><input class="per" type="number" name="top[per][<{$level}>][<{$top_key}>]" min="0" style="width: 150px" value="<{$row.award.top.per[$level][$top_key]}>"></td>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td width="40%" class="td_title">道具ID</td>
                                                    <td width="15%" class="td_title">数量</td>
                                                    <td width="15%" class="td_title">角色</td>
                                                    <td width="15%" class="td_title">权重</td>
                                                    <td style="border-bottom: 1px solid #000">操作</td>
                                                </tr>
                                                <{foreach from=$top_itemInfo key=keys item=top_item}>
                                                <tr>
                                                    <td style="text-align: center"><input type="text" name="top[item_id][<{$level}>][<{$top_key}>][]" style="width: 250px" class="item_id" value="<{$top_item}>"></td>
                                                    <td style="text-align: center"><input type="number" name="top[count][<{$level}>][<{$top_key}>][]" style="width: 100px" class="count" value="<{$row.award.top.count[$level][$top_key][$keys]}>"></td>
                                                    <td style="text-align: center">
                                                        <select class="career" name="top[career][<{$level}>][<{$top_key}>][]">
                                                            <option <{if $row.award.top.career[$level][$top_key][$keys] eq 0}>selected<{/if}> value="0">通用</option>
                                                            <option <{if $row.award.top.career[$level][$top_key][$keys] eq 1}>selected<{/if}> value="1">刀</option>
                                                            <option <{if $row.award.top.career[$level][$top_key][$keys] eq 2}>selected<{/if}> value="2">弓</option>
                                                            <option <{if $row.award.top.career[$level][$top_key][$keys] eq 3}>selected<{/if}> value="3">法</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="button" class="gbutton add_item" value="复制" />
                                                        <input type="button" class="gbutton del_item" value="删除" />
                                                    </td>
                                                </tr>
                                                <{/foreach}>
                                                </tbody>
                                            </table>
                                            <input type="button" class="gbutton add_per_item" title="添加" value="添加道具奖励" add_item_num="1"
                                                   add_per_num="<{$top_key}>" add_grade_num="<{$level}>">
                                        </td>
                                        <td>
                                            <input type="button" style="margin: 3px 3px" class="gbutton copy_per_level" title="复制" value="复制该等级">
                                            <input type="button" style="margin: 3px 3px" class="gbutton del_per_level" title="删除" value="删除该等级">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    <!--全服累计0-->
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                            <tr>
                                <td width="10%" style="border: 1px solid #000;text-align: center">全服累计奖励</td>
                                <td>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td width="10%" style="text-align: center;border-bottom: 1px solid #000">奖励类型</td>
                                            <td width="70%" style="text-align: center;border-bottom: 1px solid #000">奖励配置</td>
                                            <td style="border-bottom: 1px solid #000">操作</td>
                                        </tr>
                                        <{foreach from=$row.award.level[0].num[$level] key=level_key item=level_item}>
                                        <tr>
                                            <td style="text-align: center;">
                                                <select style="padding: 4px 2px;" class="chose-type">
                                                    <option value="0" selected>道具</option>
                                                </select>
                                            </td>
                                            <td class="options-type">
                                                <div class="reward-type chose" id="type_0" data-type="0">
                                                    <!--类型0-->
                                                    <table>
                                                        <tbody>
                                                        <!--标记-->
                                                        <tr style="display: none;">
                                                            <td colspan="2">
                                                                <input type="button" value="天数标记"  id="level_data" level_data="<{getMaxKey arr=$row.award.level[0].num[$level]}>" >
                                                            </td>
                                                        </tr>
                                                        <!--标记-->
                                                        <tr>
                                                            <td width="12%" class="td_title">累计次数</td>
                                                            <td class="td_title">奖励配置</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center"><input class="level_num" type="number" name="level[0][num][<{$level}>][<{$level_key}>]" min="0" style="width: 100px" value="<{$level_item}>"></td>
                                                            <td>
                                                                <table>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td width="40%" class="td_title">道具ID</td>
                                                                        <td width="15%" class="td_title">数量</td>
                                                                        <td width="15%" class="td_title">角色</td>
                                                                        <td style="border-bottom: 1px solid #000">操作</td>
                                                                    </tr>
                                                                    <{foreach from=$row.award.level[0].item_id[$level][$level_key] key=keys item=itemId}>
                                                                    <tr>
                                                                        <td style="text-align: center"><input class="level_item_id" type="text" name="level[0][item_id][<{$level}>][<{$level_key}>][]" style="width: 250px" value="<{$itemId}>"></td>
                                                                        <td style="text-align: center"><input class="level_count" type="number" name="level[0][count][<{$level}>][<{$level_key}>][]" style="width: 100px" value="<{$row.award.level[0].count[$level][$level_key][$keys]}>"></td>
                                                                        <td style="text-align: center">
                                                                            <select class="level_career" name="level[0][career][<{$level}>][<{$level_key}>][]">
                                                                                <option <{if $row.award.level[0].career[$level][$level_key][$keys] eq 0}>selected<{/if}> value="0">通用</option>
                                                                                <option <{if $row.award.level[0].career[$level][$level_key][$keys] eq 1}>selected<{/if}> value="1">刀</option>
                                                                                <option <{if $row.award.level[0].career[$level][$level_key][$keys] eq 2}>selected<{/if}> value="2">弓</option>
                                                                                <option <{if $row.award.level[0].career[$level][$level_key][$keys] eq 3}>selected<{/if}> value="3">法</option>
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <input style="margin: 3px" type="button" class="gbutton add_item" value="复制" />
                                                                            <input style="margin: 3px" type="button" class="gbutton del_item" value="删除" />
                                                                        </td>
                                                                    </tr>
                                                                    <{/foreach}>
                                                                    </tbody>
                                                                </table>
                                                                <input type="button" class="gbutton add_all_item" title="添加" value="添加道具奖励" add_item_num="1"
                                                                       add_all_num="<{$level_key}>" add_grade_num="<{$level}>">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="button" class="gbutton copy_type_item" title="复制" value="复制此类型奖励">
                                                <input type="button" class="gbutton del_type_item" title="删除" value="删除此类型奖励">
                                            </td>
                                        </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <{/foreach}>

            <tr class="foot_award">
                <td width="10%" style="border: 1px solid #000;text-align: center">全服累计奖励</td>
                <td>
                    <table>
                        <tbody>
                        <!--标记-->
                        <tr style="display: none;">
                            <td colspan="2">
                                <input type="button" value="天数标记"  id="level_data2" level_data2="<{getMaxKey arr=$row.award.level[1].num}>" >
                            </td>
                        </tr>
                        <!--标记-->
                        <tr>
                            <td width="10%" style="text-align: center;border-bottom: 1px solid #000">奖励类型</td>
                            <td width="70%" style="text-align: center;border-bottom: 1px solid #000">奖励配置</td>
                            <td style="border-bottom: 1px solid #000">操作</td>
                        </tr>
                        <!--全服累计1,2,3-->
                        <{foreach from=$row.award.level[1].num key=type_id item=type_info}>
                            <{if $type_info neq ""}>
                            <tr>
                                <td style="text-align: center;">
                                    <select style="padding: 4px 2px;" class="chose-type">
                                        <option selected value="1">经验</option>
                                        <option value="2">副本掉落</option>
                                        <option value="3">BOSS复活</option>
                                    </select>
                                </td>
                                <td class="options-type">
                                    <div class="reward-type chose" id="type_1" data-type="1">
                                        <!--类型1-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td class="td_title">奖励配置</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" name="level[1][num][<{$type_id}>]" value="<{$type_info}>" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div style="line-height: 30px">
                                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[1][time][<{$type_id}>]" min="0" placeholder="请填写大于0的数字" value="<{$row.award.level[1].time[$type_id]}>">&nbsp;秒<br/>
                                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[1][per][<{$type_id}>]" min="0" placeholder="请填写大于0的数字" value="<{$row.award.level[1].per[$type_id]}>">&nbsp;倍
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="reward-type" id="type_2" data-type="2">
                                        <!--类型2-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td width="25%" class="td_title">奖励配置</td>
                                                <td class="td_title">场景配置</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" name="level[2][num][99]" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div style="line-height: 30px">
                                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[2][time][99]" min="0" placeholder="请填写大于0的数字">&nbsp;秒<br/>
                                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[2][per][99]" min="0" placeholder="请填写大于0的数字">&nbsp;倍
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        <fieldset>
                                                            <legend>场景列表</legend>
                                                            <div id="pretime" style="line-height: 30px">
                                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[2][action][99][]" value="3"/>
                                                            <label class="all-checked">单人副本：法器副本、内丹副本、洗练石副本</label></span>,
                                                            <span class="action-are" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[2][action][99][]" value="4"/>
                                                            <label class="all-checked">双人副本：组队打宝、技能经验</label></span>,
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="reward-type" id="type_3" data-type="3">
                                        <!--类型3-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td class="td_title">BOSS类型</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" value="" name="level[3][num][99]" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div>
                                                        <fieldset>
                                                            <legend>场景列表</legend>
                                                            <div style="line-height: 30px">
                                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][99][]" value="1"/>
                                                            <label class="all-checked">世界BOSS</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][99][]" value="2"/>
                                                            <label class="all-checked">BOSS之家</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][99][]" value="3"/>
                                                            <label class="all-checked">虚无境地</label>
                                                            </span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <input type="button" class="gbutton copy_type2_item" title="复制" value="复制此类型奖励">
                                    <input type="button" class="gbutton del_type2_item" title="删除" value="删除此类型奖励">
                                </td>
                            </tr>
                        <{/if}>
                    <{/foreach}>
                        <{foreach from=$row.award.level[2].num key=type_id item=type_info}>
                            <{if $type_info neq ""}>
                            <tr>
                                <td style="text-align: center;">
                                    <select style="padding: 4px 2px;" class="chose-type">
                                        <option  value="1">经验</option>
                                        <option  selected value="2">副本掉落</option>
                                        <option  value="3">BOSS复活</option>
                                    </select>
                                </td>
                                <td class="options-type">
                                    <div class="reward-type" id="type_1" data-type="1">
                                        <!--类型1-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td class="td_title">奖励配置</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" name="level[1][num][99]" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div style="line-height: 30px">
                                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[1][time][99]" min="0" placeholder="请填写大于0的数字">&nbsp;秒<br/>
                                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[1][per][99]" min="0" placeholder="请填写大于0的数字">&nbsp;倍
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="reward-type chose" id="type_2" data-type="2">
                                        <!--类型2-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td width="25%" class="td_title">奖励配置</td>
                                                <td class="td_title">场景配置</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" name="level[2][num][<{$type_id}>]" value="<{$type_info}>" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div style="line-height: 30px">
                                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[2][time][<{$type_id}>]" min="0" placeholder="请填写大于0的数字" value="<{$row.award.level[2].time[$type_id]}>">&nbsp;秒<br/>
                                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[2][per][<{$type_id}>]" min="0" placeholder="请填写大于0的数字" value="<{$row.award.level[2].per[$type_id]}>">&nbsp;倍
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        <fieldset>
                                                            <legend>场景列表</legend>
                                                            <div id="pretime" style="line-height: 30px">
                                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" <{if in_array('3',$row.award.level[2].action[$type_id])}>checked="checked"<{/if}> name="level[2][action][<{$type_id}>][]" value="3"/>
                                                            <label class="all-checked">单人副本：法器副本、内丹副本、洗练石副本</label></span>,
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" <{if in_array('4',$row.award.level[2].action[$type_id])}>checked="checked"<{/if}> name="level[2][action][<{$type_id}>][]" value="4"/>
                                                            <label class="all-checked">双人副本：组队打宝、技能经验</label></span>,
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="reward-type" id="type_3" data-type="3">
                                        <!--类型3-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td class="td_title">BOSS类型</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" value="" name="level[3][num][99]" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div>
                                                        <fieldset>
                                                            <legend>场景列表</legend>
                                                            <div style="line-height: 30px">
                                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][99][]" value="1"/>
                                                            <label class="all-checked">世界BOSS</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][99][]" value="2"/>
                                                            <label class="all-checked">BOSS之家</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[3][action][99][]" value="3"/>
                                                            <label class="all-checked">虚无境地</label>
                                                            </span>
                                                            </span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <input type="button" class="gbutton copy_type2_item" title="复制" value="复制此类型奖励">
                                    <input type="button" class="gbutton del_type2_item" title="删除" value="删除此类型奖励">
                                </td>
                            </tr>
                            <{/if}>
                            <{/foreach}>
                        <{foreach from=$row.award.level[3].num key=type_id item=type_info}>
                            <{if $type_info neq ""}>
                            <tr>
                                <td style="text-align: center;">
                                    <select style="padding: 4px 2px;" class="chose-type">
                                        <option value="1">经验</option>
                                        <option value="2">副本掉落</option>
                                        <option selected value="3">BOSS复活</option>
                                    </select>
                                </td>
                                <td class="options-type">
                                    <div class="reward-type" id="type_1" data-type="1">
                                        <!--类型1-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td class="td_title">奖励配置</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" name="level[1][num][99]"  min="0" style="width: 100px"></td>
                                                <td>
                                                    <div style="line-height: 30px">
                                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[1][time][99]" min="0" placeholder="请填写大于0的数字" >&nbsp;秒<br/>
                                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[1][per][99]" min="0" placeholder="请填写大于0的数字" >&nbsp;倍
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="reward-type" id="type_2" data-type="2">
                                        <!--类型2-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td width="25%" class="td_title">奖励配置</td>
                                                <td class="td_title">场景配置</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" name="level[2][num][99]" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div style="line-height: 30px">
                                                        <span>时长 &nbsp;</span><input class="level_time" style="width: 120px" type="number" name="level[2][time][99]" min="0" placeholder="请填写大于0的数字">&nbsp;秒<br/>
                                                        <span>经验 &nbsp;</span><input class="level_per" style="width: 120px" type="number" name="level[2][per][99]" min="0" placeholder="请填写大于0的数字">&nbsp;倍
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        <fieldset>
                                                            <legend>场景列表</legend>
                                                            <div id="pretime" style="line-height: 30px">
                                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[2][action][99][]" value="3"/>
                                                            <label class="all-checked">单人副本：法器副本、内丹副本、洗练石副本</label></span>,
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" name="level[2][action][99][]" value="4"/>
                                                            <label class="all-checked">双人副本：组队打宝、技能经验</label></span>,
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="reward-type chose" id="type_3" data-type="3">
                                        <!--类型3-->
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="td_title">累计次数</td>
                                                <td class="td_title">BOSS类型</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center"><input class="level_num" type="number" value="<{$type_info}>" name="level[3][num][<{$type_id}>]" min="0" style="width: 100px"></td>
                                                <td>
                                                    <div>
                                                        <fieldset>
                                                            <legend>场景列表</legend>
                                                            <div style="line-height: 30px">
                                                                <!--功能开关列表-->
                                                            <span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" <{if in_array('1',$row.award.level[3].action[$type_id])}>checked="checked"<{/if}>  name="level[3][action][<{$type_id}>][]" value="1"/>
                                                            <label class="all-checked">世界BOSS</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" <{if in_array('2',$row.award.level[3].action[$type_id])}>checked="checked"<{/if}> name="level[3][action][<{$type_id}>][]" value="2"/>
                                                            <label class="all-checked">BOSS之家</label>
                                                            </span>,<span class="action-are select" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                                                            <input class="all-checked all_type" onclick="return false;" checked type="checkbox" <{if in_array('3',$row.award.level[3].action[$type_id])}>checked="checked"<{/if}> name="level[3][action][<{$type_id}>][]" value="3"/>
                                                            <label class="all-checked">虚无境地</label>
                                                            </span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <input type="button" class="gbutton copy_type2_item" title="复制" value="复制此类型奖励">
                                    <input type="button" class="gbutton del_type2_item" title="删除" value="删除此类型奖励">
                                </td>
                            </tr>
                            <{/if}>
                            <{/foreach}>

                        </tbody>
                    </table>
                </td>
            </tr>

        <tr>
            <td style="width:150px;">活动描述</td>
            <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
        </tr>
        <!-- 标记 -->
        <tr style="display: none;">
            <td colspan="2">
                <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
            </td>
        </tr>
        <!-- 标记 -->
        <tr>
            <td style="width:150px">活动开关</td>
            <td>
                <select name="state">
                    <option value="0">开</option>
                    <option value="1">关</option>
                </select>
            </td>
        </tr>

        <!-- 主要用于进行一个颜色错位 -->
        <tr></tr>
        <!-- 主要用于进行一个颜色错位 -->

        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/channelGroup_server_edit.tpl' }>
        <!-- 服务器，  -->
        <tr>
            <td colspan="2">
                <!-- 活动ID -->
                <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                <input type="hidden" class="id" name="id" value="<{$row.id}>">
                <!--活动ID  -->
                <input type="submit" class="gbutton" value="保存" id="charge">
                <input type="hidden" id="error" value="1">
            </td>
        </tr>
        </tbody>
        </table>
    </form>
</div>
<script language="javascript">
    $(".all_type").each(function(){
        if($(this).prop('checked')){
            $(this).parent('span').addClass('select');
        }else{
            return true;
        }
    })
</script>