<script type="text/javascript" src="templates/activity/activity.js"></script>
<script type="text/javascript" src="templates/activity/activity_1047.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">新年祈愿</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            <tr>
                <td>每天免费次数</td>
                <td><input type="number" name="freeGold" value="<{$row.award.freeGold|default:1}>"></td>
            </tr>
            <tr>
                <td>祈愿一次花费的元宝</td>
                <td><input type="number" name="once_gold" value="<{$row.award.once_gold}>"></td>
            </tr>
            <tr>
                <td>祈愿一次可获得积分</td>
                <td><input type="number" name="item_need_count" value="<{$row.award.item_need_count}>"></td>
            </tr>
            <tr>
                <td>祈愿十次花费元宝</td>
                <td><input type="number" name="tentimes_gold" value="<{$row.award.tentimes_gold}>"></td>
            </tr>
            <tr>
                <td>祈愿十次可获得积分</td>
                <td><input type="number" name="item_exchange" value="<{$row.award.item_exchange}>"></td>
            </tr>
            <{foreach from=$row.award.grade key=level item=world_level_num}>
            <tr>
                <td style="width:150px;text-align: center">
                    <label>世界等级</label>
                    <input type="text" name="grade[<{$level}>]" class="w-120 grade" value="<{$world_level_num}>">
                    <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                    <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                </td>
                <td>
                    <span style="margin: 0 144px;color: #ca1b36;font-size: 15px">PS:游戏内抽奖获得物品总数量最多配置21项！（时装+装备和通用道具）</span>
                    <table style="border: 1px solid #fff" border="1" >
                        <tbody>
                        <!--标记-->
                        <tr style="display: none;">
                            <td colspan="2">
                                <input type="button" value="天数标记"  id="add_grade_num" add_grade_num="<{$level}>" >
                            </td>
                        </tr>
                        <!--标记-->
                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center;">时装+装备</td>
                            <td>
                                <table>
                                    <tbody>
                                    <!--top-->
                                    <tr>
                                        <td width="30%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td width="15%" class="td_title">权重</td>
                                        <td width="10%" class="td_title">特效</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.top.item_id2[$level] key=i_key item=items}>
                                    <tr>
                                        <td style="text-align: center"><input type="text" name="top[item_id2][<{$level}>][]" style="width: 250px" class="item_id" value="<{$items}>"></td>
                                        <td style="text-align: center"><input type="text" name="top[count2][<{$level}>][]" value="<{$row.award.top.count2[$level][$i_key]}>" class="count"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[career2][<{$level}>][]">
                                                <option <{if $row.award.top.career2[$level][$i_key] eq 1}>selected<{/if}> value="1">刀</option>
                                                <option <{if $row.award.top.career2[$level][$i_key] eq 2}>selected<{/if}> value="2">弓</option>
                                            </select>
                                        </td>
                                        <td><input type="number" name="top[weight2][<{$level}>][]" class="weight" value="<{$row.award.top.weight2[$level][$i_key]}>"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[value2][<{$level}>][]">
                                                <option <{if $row.award.top.value2[$level][$i_key] eq 0}>selected<{/if}> value="0">无</option>
                                                <option <{if $row.award.top.value2[$level][$i_key] eq 1}>selected<{/if}> value="1">有</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制">
                                            <input type="button" class="gbutton del_item" value="删除">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton c-additem top_dress_btn" add_item_num="1" add_comsume_num="1" add_grade_num="<{$level}>" title="添加" value="添加时装+装备奖励">
                            </td>
                        </tr>

                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center;">通用道具</td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="30%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td width="15%" class="td_title">权重</td>
                                        <td width="10%" class="td_title">特效</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.top.item_id[$level] key=i_key item=items}>
                                    <tr>
                                        <td style="text-align: center"><input type="text" name="top[item_id][<{$level}>][]" value="<{$items}>" style="width: 250px" class="item_id"></td>
                                        <td style="text-align: center"><input type="text" name="top[count][<{$level}>][]" value="<{$row.award.top.count[$level][$i_key]}>" class="count"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[career][<{$level}>][]">
                                                <option <{if $row.award.top.career[$level][$i_key] eq 0}>selected<{/if}> value="0">通用</option>
                                                <option <{if $row.award.top.career[$level][$i_key] eq 1}>selected<{/if}> value="1">刀</option>
                                                <option <{if $row.award.top.career[$level][$i_key] eq 2}>selected<{/if}> value="2">弓</option>
                                            </select>
                                        </td>
                                        <td><input type="number" name="top[weight][<{$level}>][]" value="<{$row.award.top.weight[$level][$i_key]}>" class="weight"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="top[value][<{$level}>][]">
                                                <option <{if $row.award.top.vlaue[$level][$i_key] eq 0 }>selected<{/if}> value="0">无</option>
                                                <option <{if $row.award.top.vlaue[$level][$i_key] eq 1 }>selected<{/if}> value="1">有</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制">
                                            <input type="button" class="gbutton del_item" value="删除">
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton c-additem top_normal_btn" add_item_num="1" add_comsume_num="1" add_grade_num="<{$level}>" title="添加" value="添加通用奖励">
                            </td>
                        </tr>

                        </tbody>
                    </table>
            <tr>
            <{/foreach}>

            <tr class="foot_reward">
                <!--保底奖励-->
                <td style="text-align: center">
                    积分商店<br/>
                    奖励配置
                </td>
                <td>
                    <table style="border: 1px solid #fff" border="1">
                        <tbody>
                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center;">时装+装备</td>
                            <td>
                                <table>
                                    <tbody>
                                    <!--day-->
                                    <tr>
                                        <td width="30%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td width="15%" class="td_title">积分</td>
                                        <td width="10%" class="td_title">限制次数</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.day.item_id2 key=keys item=itemId}>
                                        <tr>
                                            <td style="text-align: center"><input type="text" name="day[item_id2][]" value="<{$itemId}>" style="width: 250px" class="item_id"></td>
                                            <td style="text-align: center"><input type="text" name="day[count2][]" value="<{$row.award.day.count2[$keys]}>" class="count"></td>
                                            <td style="text-align: center">
                                                <select class="career" name="day[career2][]">
                                                    <option <{if $row.award.day.career2[$keys] eq 1}>selected<{/if}> value="1">刀</option>
                                                    <option <{if $row.award.day.career2[$keys] eq 2}>selected<{/if}> value="2">弓</option>
                                                </select>
                                            </td>
                                            <td style="text-align: center"><input type="number" name="day[score2][]" value="<{$row.award.day.score2[$keys]}>" class="score"></td>
                                            <td style="text-align: center"><input type="number" name="day[limit2][]" value="<{$row.award.day.limit2[$keys]}>" placeholder="0:没有限制次数" class="limit"></td>
                                            <td>
                                                <input type="button" class="gbutton add_item" value="复制">
                                                <input type="button" class="gbutton del_item" value="删除">
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton day_dress_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加时装+装备奖励">
                            </td>
                        </tr>

                        <tr>
                            <td width="10%" style="border: 1px solid #000;text-align: center;">通用道具</td>
                            <td>
                                <table>
                                    <tbody>
                                    <!--day-->
                                    <tr>
                                        <td width="30%" class="td_title">道具ID</td>
                                        <td width="15%" class="td_title">数量</td>
                                        <td width="10%" class="td_title">角色</td>
                                        <td width="15%" class="td_title">积分</td>
                                        <td width="10%" class="td_title">限制次数</td>
                                        <td style="border-bottom: 1px solid #000">操作</td>
                                    </tr>
                                    <{foreach from=$row.award.day.item_id key=keys item=itemId}>
                                    <tr>
                                        <td style="text-align: center"><input type="text" name="day[item_id][]" style="width: 250px" value="<{$itemId}>" class="item_id"></td>
                                        <td style="text-align: center"><input type="text" name="day[count][]" value="<{$row.award.day.count[$keys]}>" class="count"></td>
                                        <td style="text-align: center">
                                            <select class="career" name="day[career][]">
                                                <option <{if $row.award.day.career[$keys] eq 0}>selected<{/if}> value="0">通用</option>
                                                <option <{if $row.award.day.career[$keys] eq 1}>selected<{/if}> value="1">刀</option>
                                                <option <{if $row.award.day.career[$keys] eq 2}>selected<{/if}> value="2">弓</option>
                                            </select>
                                        </td>
                                        <td style="text-align: center"><input type="number" name="day[score][]" value="<{$row.award.day.score[$keys]}>" class="score"></td>
                                        <td style="text-align: center"><input type="number" name="day[limit][]" value="<{$row.award.day.limit[$keys]}>" placeholder="0:没有限制次数" class="limit"></td>
                                        <td>
                                            <input type="button" class="gbutton add_item" value="复制">
                                            <input type="button" class="gbutton del_item" value="删除">
                                        </td>
                                    </tr>
                                   <{/foreach}>
                                    </tbody>
                                </table>
                                <input type="button" class="gbutton day_normal_btn" add_item_num="1" add_comsume_num="1" add_grade_num="1" title="添加" value="添加时装+装备奖励">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>

            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->
            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>