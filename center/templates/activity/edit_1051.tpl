<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .cur {
        background-color: beige;
    }

    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }

    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
    .td_title{
        border-bottom: 1px solid #000000;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1051.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>">每日登录</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;"
                           value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
                </td>
            </tr>
            <tr>
                <td style="width: 150px">活动标识</td>
                <td>
                    <select name="activity_sign">
                        <option
                        <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
                        <option
                        <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
                        <option
                        <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
                        <option
                        <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开服天数</td>
                <td>
                    <input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
                </td>
            </tr>
            <tr>
                <td>最低充值</td>
                <td><input type="number" name="buyCount" value="<{$row.award.buyCount}>"></td>
            </tr>

            <{foreach from=$row.award.grade key=level item=info}>
            <tr>
                <td style="width:150px">
                    <label>世界等级</label>

                    <input type="text" name="grade[<{$level}>]" value="<{$info}>" class="w-120 grade">
                    <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                    <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                </td>
                <td>
                    <table>
                        <tbody>
                        <tr></tr>
                        <tr>
                            <td width="30%" class="td_title">道具ID</td>
                            <td width="15%" class="td_title">数量</td>
                            <td width="10%" class="td_title">角色</td>
                            <td style="border-bottom:1px solid #000">操作</td>
                        </tr>
                        <{foreach from=$row.award.item_id[$level] key=k item=itemMsg}>
                        <tr>
                            <td style="text-align: center"><input type="text" name="item_id[<{$level}>][]" value="<{$itemMsg}>" style="width: 250px" class="item_id"></td>
                            <td style="text-align: center"><input type="number" name="count[<{$level}>][]" value="<{$row.award.count[$level][$k]}>" class="count"></td>
                            <td style="text-align: center">
                                <select name="career[<{$level}>][]" class="career">
                                    <option <{if $row.award.career[$level][$k] eq 0}>selected<{/if}> value="0">通用</option>
                                    <option <{if $row.award.career[$level][$k] eq 1}>selected<{/if}> value="1">刀</option>
                                    <option <{if $row.award.career[$level][$k] eq 2}>selected<{/if}> value="2">弓</option>
                                </select>
                            </td>
                            <td>
                                <input type="button" class="gbutton copy_item" value="复制">
                                <input type="button" class="gbutton del_item" value="删除">
                            </td>
                        </tr>
                        <{/foreach}>
                        </tbody>
                    </table>
                    <input type="button" class="gbutton c-additem top_dress_btn" add_item_num="1" add_comsume_num="1" add_grade_num="<{$level}>" title="添加" value="添加道具">
                </td>
            </tr>
            <{/foreach}>

            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->
            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>