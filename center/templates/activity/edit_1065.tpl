<script type="text/javascript" src="templates/activity/activity.js"></script>
<script type="text/javascript" src="templates/activity/activity_1065.js"></script>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.cur {
	background-color: beige;
}


.itable-color tbody > tr:nth-child(odd) {
  background: #f1f1b0;
} 
.del-level {
	margin: 5px 0px;
}

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
    <div class="type-list">
      <ul>
        <li class="aType cur" data-type="<{$row.act_id}>" ><{$row.act}></li>
      </ul>
    </div>
    <table class="itable itable-color">
      <tbody>
        <tr>
          <td style="width:150px;">活动名称</td>
          <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
        </tr>
        <tr>
          <td style="width:150px;">活动时间</td>
          <td>
          	<input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
            ~
            <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
           </td>
        </tr>
		<tr>
			<td style="width: 150px">活动标识</td>
			<td>
				<select name="activity_sign">
					<option <{if $row.activity_sign eq 0}>selected<{/if}> value="0">无标识</option>
					<option <{if $row.activity_sign eq 1}>selected<{/if}> value="1">推荐</option>
					<option <{if $row.activity_sign eq 2}>selected<{/if}> value="2">新</option>
					<option <{if $row.activity_sign eq 3}>selected<{/if}> value="3">限时</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>开服天数</td>
			<td>
				<input type="number" name="openSerDay" value="<{$row.open_ser_day}>"/>
			</td>
		</tr>
        
        <tr>
			<td style="width:150px;">
				<input type="button" value="添加条件" class="gbutton c-add-reward" add_amount_num="<{getMaxKey arr=$row.amount}>" >
			</td>

			<td>
				<table style="width:100%;" class = "table-activity-reward">
					<thead>
						<tr>
							<th style="width:80px;">条件</th>
							<th>条件</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="body-item">
						<{foreach from=$row.amount key=k item=v}>
						<tr>
							<td style="width:100px;"><input type="text" class="amount" name="amount[<{$k}>]" value="<{$v}>"></td>
							<td>
								<table >
								<tbody>
								<tr>
									<td>
										<table class="itemTable">
											<thead>
												<tr>
													<td>物品ID</td>
													<td>数量</td>
													<td>操作</td>
												</tr>
											</thead>
												
											<tbody>
											<{foreach from=$row.itemId[$k] key=k2 item=v2 }>
											<tr class="itemWidget">
												<td> 
													<input style="width:200px" type="text" name="item_id[<{$k}>][]" class="item_id" value="<{$v2}>" /> 
												</td>
												
												<td>
													<input type="text" name="item_count[<{$k}>][]" value="<{$row.itemCount[$k][$k2]}>" class="item_count"> 
												</td> 
												
												
												<td> 
													<input type="button" value="删除" class="gbutton delitem" title="删除该行"> 
												</td>								
											</tr>
											<{/foreach}>

											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
								</table>
								<input type="button" class="gbutton c-additem" value="添加物品条件" add_item_num="<{getMaxKey arr=$row.itemId[$k]}>" add_amount_num="<{$k}>">
							</td>
							
							<td>
								<input type=button class="gbutton del-reward"  value="删除该条件"/>
								<input type=button class="gbutton c-copy-reward"  value="复制该条件"/>
							</td>
							
						</tr>
						<{/foreach}>
					</tbody>
				</table>
			</td>
        </tr>
        
 <tr grade_num="1">
    <td style="width:150px;">
        累计天数配置
    </td>
    <td>
        <table style="width:100%;" class="table-activity-reward">
            <thead>
                <tr>
                    <th style="width:80px;">钻石</th>
                    <th>奖励</th>
                </tr>
            </thead>
            <tbody class="body-item">
                <tr>
                    <td style="width:100px;">
                        <input type="text" class="amount" name="amount2" value="<{$row.grade.amount2}>" placeholder="钻石">
                    </td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <table class="itemTable">
                                            <thead>
                                                <tr>
                                                    <td>天数</td>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>操作</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <{foreach from=$row.grade.day2.1 key=k2 item=v2 }>
												<tr class="itemWidget">
													<td> 
														<input style="width:200px" type="text" name="day2[1][]" class="day2" value="<{$v2}>" /> 
													</td>
													<td> 
														<input style="width:200px" type="text" name="item_id2[1][]" class="item_id" value="<{$row.grade.item_id2[1][$k2]}>" autocomplete="on"/> 
													</td>
												
													<td>
														<input type="text" name="item_count2[1][]" value="<{$row.grade.item_count2[1][$k2]}>" class="item_count"> 
													</td> 
												
													<td> 
														<input type="button" value="删除" class="gbutton delitem" title="删除该行"> 
													</td>								
												</tr>
											<{/foreach}>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="button" value="添加物品条件" class="gbutton c-additem2" add_item_num="0"
                        add_amount_num="1">
                    </td>
                   
                </tr>
            </tbody>
        </table>
    </td>
</tr>
        
        <tr>
          <td style="width:150px;">活动描述</td>
          <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
        </tr>
		
		<tr>
			<td style="width:150px">活动开关</td>
			<td>
				<select name="state">
					<option value="0" <{if $row.state == 0 }> selected <{/if}>>开</option>
					<option value="1" <{if $row.state == 1 }> selected <{/if}>>关</option>
				</select>
			</td>
		</tr>

		<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/channelGroup_server_edit.tpl' }>
        <!-- 服务器，  -->

        <tr>
          <td colspan="2">
          	<!-- 活动ID -->
			<input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
			<input type="hidden" name="act_id"  value="<{$row.act_id}>" >
			<input type="hidden" name="id" value="<{$row.id}>">
			<!--活动ID  -->

			<input type="submit" class="gbutton" value="保存" id="charge">
            <input type="hidden" id="error" value="1">
 		</td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
