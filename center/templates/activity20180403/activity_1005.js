// JavaScript Document
$(function(){

	//获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
		$( ".item_id" ).autocomplete({
            source: _goods
        });

    },'json');

	//更先die，然后再重监听 
	$('.c-additem').die("click");
	$('.c-additem').live('click', function(){
		
		var add_amount_num = $(this).attr('add_amount_num');
		add_amount_num = parseInt(add_amount_num);
		
		
		var item_id_name =  "item_id["+ add_amount_num +"][]";
		var item_count_name = "item_count["+ add_amount_num +"][]";
		var career_name = "career["+ add_amount_num +"][]";
		var weight_name = "weight["+ add_amount_num +"][]";
		
		var itemWidget = '<tr class="itemWidget">' +
									'<td>' +
										'<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  + 
									'</td>' +
									
									'<td>' +
										'<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
									'</td>' + 

									'<td>' +
										'<input type="text" name="'+ weight_name +'" value="1" class="item_count">' +
									'</td>' + 
									
									'<td>' + 
										'<select name="'+ career_name +'">' + 
											'<option value="0">通用</option>' + 
											'<option value="1">刀剑师</option>' + 
											'<option value="2">羽翎师</option>' +
										'<select>' + 
									'</td>' +
									
									'<td>' + 
											'<input type="button" value="删除" class="gbutton delitem" title="删除该行">' + 
									'</td>' +								
					'</tr>';
					
		//alert($(this).prev('table').length);
		$(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget); 
		
		//物品绑定
		$( ".item_id" ).autocomplete({
            source: _goods
        });

	});
	
	//删除某个物品
	$('.delitem').die('click');
	$('.delitem').live('click', function() {
		$(this).parent().parent().remove();
	});
	
	//删除某个条件

	$('.del-reward').die('click');
	$('.del-reward').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});

	//删除该档充值
	$('.del-level').die('click');
	$('.del-level').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});


	//复制该条件
	$('.c-copy-level').die('click');
	$('.c-copy-level').live('click', function() {
		$(this).hide();	

		var oTr = $(this).parent('td').parent('tr');
		var clone = oTr.clone();

		clone.find('td').eq(0).find('label').replaceWith('<label>老服奖励</label>');

		var str = "$1[2]";

		var reg = /(\w+)\[\d\]/g; 

		//额外奖励
		var OitemId = clone.find('.item_id');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		//额外奖励
		var OitemId = clone.find('.item_count');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		var OitemId = clone.find('.item_weight');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		var OitemId = clone.find('.input');
		$(OitemId).each(function(index) {
			var name = $(this).attr('name');

			var s = name.replace(reg, str);

			$(this).attr('name', s)
		});

		
		$(this).parent('td').parent('tr').parent('tbody').append(clone);

	    $(".item_id").autocomplete({
	        source: _goods
	    });
	});
	
	
});

