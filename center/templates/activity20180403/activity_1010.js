// JavaScript Document
$(function() {
	//获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
    },'json');

	//更先die，然后再重监听 
	$('.c-additem').die("click");
	$('.c-additem').live('click', function(){
		var item_need_name =  "item_need_count[]";
		var item_id_name =  "item_id[]";
		var item_count_name = "item_count[]";
		var item_exchange_name = "item_exchange[]";
		
		var itemWidget = '<tr class="itemWidget">' +
									'<td>' +
										'<input type="text" name="'+ item_need_name +' ">'  + 
									'</td>' +

									'<td>' +
										'<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  + 
									'</td>' +
									
									'<td>' +
										'<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
									'</td>'+

									'<td>' +
										'<input type="text" name="'+ item_exchange_name +'" value="1" class="item_weight">' +
									'</td>'+ 
									
									'<td>' + 
											'<input type="button" value="删除" class="gbutton delitem" title="删除该行">' + 
									'</td>' +								
					'</tr>';
					
		//alert($(this).prev('table').length);
		$(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget); 
		
		//物品绑定
		$( ".item_id" ).autocomplete({
            source: _goods
        });

	});
	
	//删除某个物品
	$('.delitem').die('click');
	$('.delitem').live('click', function() {
		$(this).parent().parent().remove();
	});
	
	//删除某个条件

	$('.del-reward').die('click');
	$('.del-reward').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});

	//复制该条件
	$('.c-copy-reward').die('click');
	$('.c-copy-reward').live('click', function() {
		//查找出最大条件
		
		var obj = $(this).parent('td').parent('tr').parent('tbody').parent('table').parent('td').prev('td').find('.c-add-reward');

		var add_grade_num = obj.attr('add_grade_num');

		var add_amount_num = obj.attr('add_amount_num');
		
		var num = parseInt(add_amount_num) + 1;

		var replace = "["+ add_grade_num + "]["+ num +"]";

		obj.attr('add_amount_num', num);

		var oTr = $(this).parent('td').parent('tr');
		var clone = oTr.clone();
		//替换相对应名称
		clone.find('.amount').attr('name', 'amount' + replace);
		clone.find('.item_id').attr('name', 'item_id' + replace + '[]');
		clone.find('.item_count').attr('name', 'item_count' + replace + '[]');
		clone.find('select').attr('name', 'career' + replace + '[]');

		$(this).parent('td').parent('tr').parent('tbody').append(clone);

		$( ".item_id" ).autocomplete({
            source: _goods
        });
	});
	
	//删除该档充值
	$('.del-level').die('click');
	$('.del-level').live('click', function() {
		$(this).parent('td').parent('tr').remove();
	});
	
	
});


//绑定时间
function addTime() {
	var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
	$('.start_time').datetimepicker(timepickerlang);
	$('.end_time').datetimepicker(timepickerlang);
}

