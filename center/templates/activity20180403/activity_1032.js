/** there have a bug for jquery select and textarea in clone */
(function (original) {
    jQuery.fn.clone = function () {
        var result = original.apply(this, arguments),
            my_textareas = this.find('textarea').add(this.filter('textarea')),
            result_textareas = result.find('textarea').add(result.filter('textarea')),
            my_selects = this.find('select').add(this.filter('select')),
            result_selects = result.find('select').add(result.filter('select'));

        for (var i = 0, l = my_textareas.length; i < l; ++i) $(result_textareas[i]).val($(my_textareas[i]).val());
        for (var i = 0, l = my_selects.length; i < l; ++i) result_selects[i].selectedIndex = my_selects[i].selectedIndex;

        return result;
    };
}) (jQuery.fn.clone);

/** js document */
$(function () {

    /**-----*/
    //获取物品配置
    var _goods = '';
    $.post('admin.php?ctrl=gift&act=getGoodsItem', '', function (data) {
        _goods = data;
        //物品绑定
        $(".item_id").autocomplete({
            source: _goods
        });

    }, 'json');
    ///使用原有.c-additem监听方案
    //关闭
    $(".c-addItem").die();
    $(".c-addItem").live('click', function () {
        var add_normal_num = $(this).attr('add_amount_num');//1
        var line_num = parseInt($(this).attr('add_list_num'));//行数
        var list_type = $(this).attr('list-type');
        var item_id_name =  "list["+list_type+"][normal]["+add_normal_num+"]["+line_num+"][itemId]";
        var item_count_name = "list["+list_type+"][normal]["+ add_normal_num +"]["+line_num+"][count]";
        var career_name = "list["+list_type+"][normal]["+ add_normal_num +"]["+line_num+"][career_name]";
        var weight_name = "list["+list_type+"][normal]["+ add_normal_num +"]["+line_num+"][weight]";
        $(this).attr('add_list_num',line_num+1);//更新最大行数
        var itemWidget = '<tr class="itemWidget">' +
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ weight_name +'" value="1" class="weight">' +
            '</td>' +

            '<td>' +
            '<select name="'+ career_name +'" class="career_name">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀剑师</option>' +
            '<option value="2">羽翎师</option>' +
            '<select>' +
            '</td>' +

            '<td>' +
            '<button type="button" class="gbutton delitem" title="删除该行">删除</button>' +
            '</td>' +
            '</tr>';
        //alert($(this).prev('table').length);
        $(this).parent('td').parent('tr.add_item_btn').before(itemWidget);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    //kill 单项复制
    $(".copyitem").die();
    $(".copyitem").live('click',function(event){
        //激活事件
        var parent = $(this).parent('td').parent('tr').parent('tbody').parent('table.itemTable').parent('td').parent('tr.list-num');
        var add_amount_num =parent.attr('list_num');
        add_amount_num = parseInt(add_amount_num)+1;//第二栏
        var tr_parent = $(this).parent('td').parent('tr');
        var clone = tr_parent.clone();
        var preg = /(\w+\[\d\]\[\w+\]\[\d\]\[\d\])\[\d\]+?/g;
        var str = "$1["+add_amount_num+"]";
        //道具ID
        var itemId = clone.children('td').children('input.item_id');

        if(typeof itemId !='undefined'){
            name_replace(itemId,preg,str)
        }else{
            return false;
        }

        //数量
        var count = clone.children('td').children('input.item_count');
        if(typeof count !='undefined'){
            name_replace(count,preg,str)
        }else{
            return false;
        }
        //角色
        var career = clone.children('td').children('select.career_name');
        if(typeof career !='undefined'){
            name_replace(career,preg,str)
        }else{
            return false;
        }
        parent.attr('list_num',add_amount_num);
        $(this).parent('td').parent('tr').parent('tbody').append(clone);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
        event.stopPropagation();
    });

    //删除普通奖励
    $(".del-normal").die();
    $(".del-normal").live('click',function(){
        var parent = $(this).parent('td');
        parent.prev('td').children('table.itemTable').children('tbody').children('tr.itemWidget').each(function(){
            $(this).remove()
        });
    });

    //复制普通奖励
    $(".copy-normal").die();
    $(".copy-normal").live('click',function(){
        var numParent = $(this).parent('td').parent('tr').parent('tbody').parent('table').parent('td.add-normal-grad');
        var max_grad_num = numParent.attr('add-normal-num');
        max_grad_num = parseInt(max_grad_num);//最大grad
        max_grad_num +=1;
        numParent.attr('add-normal-num',max_grad_num);//更新为最大grad
        var parent = $(this).parent('td').parent('tr');
        var clone = parent.clone();
        //物品ID
        var cloneTbody = clone.children('td').eq(0).children('table').children('tbody');
        var child = cloneTbody.children('tr.itemWidget');
        var preg = /(\w+\[\d\]\[\w+\])\[\d\]+?/g;
        var str = "$1["+max_grad_num+"]";
        child.each(function(){
            //修改每一项input的name
            //道具ID
            var per = $(this).children('td').children('input.item_id');
            if(typeof per != 'undefined' && per.attr('type') != 'button') {
                name_replace(per,preg,str)
            }else{
                return false;
            }
            //道具数量
            var count = $(this).children('td').children('input.item_count');
            if(typeof count != 'undefined' && count.attr('type') != 'button') {
                name_replace(count,preg,str)
            }else{
                return false;
            }
            //权重
            var weight = $(this).children('td').children('input.weight');
            if(typeof weight != 'undefined' && weight.attr('type') != 'button') {
                name_replace(weight,preg,str)
            }else{
                return false;
            }
            //修改每一项select的name
            var car = $(this).children('td').children('select.career_name');
            if(typeof car != 'undefined'){
                name_replace(car,preg,str);
            }else{
                return false;
            }
        });
        cloneTbody.children('tr.add_item_btn').children('td').children('input').attr('add_amount_num',max_grad_num);
        //onsole.log(clone);
        parent.parent('tbody').append(clone);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });

    });
    //复制单轮奖励
    $(".c-copy-level").die();
    $(".c-copy-level").live('click',function(){
        //激活事件
        var parent = $(this).parent('td').parent('tr');
        var numParent = parent.parent('tbody').parent('table').parent('td.add-roll-grad');
        var gradNum = numParent.attr('add-roll-num');//获取最大轮数
        var roll_num = parseInt(numParent.attr('roll_num'));//显示轮
        gradNum = parseInt(gradNum);
        gradNum +=1;

        numParent.attr('add-roll-num',gradNum);//更新轮数

        var clone = parent.clone();
        var child = clone.children('td').eq(1).children('table').children('tbody').children('tr');
        var preg = /(\w+\[\d\]\[\w+\])\[\d\]+?/g;
        var str = "$1["+gradNum+"]";
        //遍历修改
        child.each(function(event){
            var msg = $(this).children('td').eq(1);
            var childMsg = msg.children('table').children('tbody').children('tr');
            childMsg.each(function(){
                //循环每一福奖励
                var childTr  = $(this).children('td').children('table.itemTable').children('tbody').children('tr');

                childTr.each(function(event){
                    //遍历每一个tr
                    var childTd = $(this).children('td');
                    //物品ID
                    var item = childTd.children('input.item_id');
                    if(typeof item != 'undefined' && typeof item == 'object'){
                        name_replace(item,preg,str);
                    }else{
                        return false;//中断克隆
                    }

                    //道具数量
                    var item_count = childTd.children('input.item_count');
                    if(typeof item_count != 'undefined' && typeof item_count == 'object'){
                        name_replace(item_count,preg,str);
                    }else{
                        return false;//中断克隆
                    }
                    //角色
                    var career_name = childTd.children('select.career_name');
                    if(typeof career_name != 'undefined' && typeof career_name == 'object'){
                        name_replace(career_name,preg,str);
                    }else{
                        return false;//中断克隆
                    }

                });
                //event.stopPropagation();//停止事件冒泡
            })
        });

        roll_num += 1;
        numParent.attr('roll_num',roll_num);
        clone.children('td').eq(0).find('.roll-text').val('第'+roll_num+'轮奖励');
        clone.children('td').eq(0).find('.roll-hidden').val(roll_num);
        /*clone.children('td').eq(0).find('.c-copy-level').val('复制第'+roll_num+'轮奖励');
        clone.children('td').eq(0).find('.c-del-level').val('删除第'+roll_num+'轮奖励');*/
        parent.parent('tbody').children('tr.normal-price').before(clone);
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });

    //删除该轮奖励(数字后期优化)
    $(".c-del-level").die();
    $(".c-del-level").live('click',function(){
        //激活事件，删除活动后，轮数大于被删者全部--
        var list = $(this).parent('td').parent('tr');
        var listTr = list.parent('tbody').children('tr.big-price');
        var roll_num = list.parent('tbody').parent('table.new-ser-msg').parent('td.add-roll-grad').attr('roll_num');
        if(roll_num == 1){
            alert('至少保留一轮奖励');
            return false;
        }
        roll_num = parseInt(roll_num);
        roll_num--;
        var cut_num = parseInt($(this).parent('td').children('input.roll-hidden').val());
        listTr.each(function(event){
            var child = $(this).children('td').eq(0);
            var childTd = parseInt(child.find('.roll-hidden').val());//被记录的轮数
            if(childTd >= cut_num){
                childTd -=1;
                childTd = childTd == 0?1:childTd;
                child.find('.roll-hidden').val(childTd);//更新
                child.find('.roll-text').val('第'+childTd+'轮奖励');
            }
        });
        var parent =list.parent('tbody').parent('table.new-ser-msg').parent('td.add-roll-grad');
        var num = parseInt(parent.attr('roll_num'));
        num -= 1;
        parent.attr('roll_num',num);
        list.remove();
    });
    $(".item_id").autocomplete({
        source: _goods
    });

    //复制作为老服务奖励
    $(".c-copy-asOld").die();//kill
    $(".c-copy-asOld").live("click",function(){
        //可支持多次使用
        var parent = $(this).parent('td').parent('tr');
        var clone = parent.prev().clone();
        var Info = clone.children('td').children('table.new-ser-msg').children('tbody');
        var preg = /(\w+)\[\d\]+?/g;
        var str = "$1[2]";
        //大奖
        Info.children('tr.big-price').each(function(){
            var lableMsg = $(this).children('td :eq(0)').find('label.title-name');
            lableMsg.html('老服奖励');

            var TopInfo = $(this).children('td :eq(1)').children('table').children('tbody').children('tr');
            TopInfo.each(function(index){
                var childFu = $(this).children('td').eq(1).children('table').children('tbody').children('tr');
                childFu.each(function(index){
                    var childMsg = $(this).children('td').children('table').children('tbody').children('tr');
                    childMsg.each(function(index){
                        //每一行奖励
                        var childTd = $(this).children('td');
                        //物品ID
                        var item = childTd.children('input.item_id');
                        if(typeof item != 'undefined' && typeof item == 'object'){
                            name_replace(item,preg,str);
                        }else{
                            return false;//中断克隆
                        }

                        //道具数量
                        var item_count = childTd.children('input.item_count');
                        if(typeof item_count != 'undefined' && typeof item_count == 'object'){
                            name_replace(item_count,preg,str);
                        }else{
                            return false;//中断克隆
                        }
                        //角色
                        var career_name = childTd.children('select.career_name');
                        if(typeof career_name != 'undefined' && typeof career_name == 'object'){
                            name_replace(career_name,preg,str);
                        }else{
                            return false;//中断克隆
                        }
                        //select
                    })
                })
            })

        });
        //普通奖励
        Info.children('tr.normal-price').each(function(event){
            var childLine = $(this).children('td.add-normal-grad').children('table').children('tbody').children('tr');
            childLine.each(function(){
                var childs = $(this).children('td').eq('0').children('table.itemTable').children('tbody').children('tr.itemWidget');
                childs.each(function(){
                    var per = $(this).children('td').children('input.item_id');
                    if(typeof per != 'undefined' && per.attr('type') != 'button') {
                        name_replace(per,preg,str)
                    }else{
                        return false;
                    }
                    //道具数量
                    var count = $(this).children('td').children('input.item_count');
                    if(typeof count != 'undefined' && count.attr('type') != 'button') {
                        name_replace(count,preg,str)
                    }else{
                        return false;
                    }
                    //权重
                    var weight = $(this).children('td').children('input.weight');
                    if(typeof weight != 'undefined' && weight.attr('type') != 'button') {
                        name_replace(weight,preg,str)
                    }else{
                        return false;
                    }
                    //修改每一项select的name
                    var car = $(this).children('td').children('select.career_name');
                    if(typeof car != 'undefined'){
                        name_replace(car,preg,str);
                    }else{
                        return false;
                    }
                });

                var btn = $(this).children('td').eq('0').children('table.itemTable').children('tbody').children('tr.add_item_btn');
                btn.children('td').children('input.c-addItem').attr('list-type',2);
            })
        });
        parent.next().remove();
        parent.parent('tbody').append(clone);//复制老模板
        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });
    });
    //删除老服奖励
    $(".c-del-new").bind('click',function(){
        var parent = $(this).parent('td').parent('tr');
        parent.next('tr').remove();
    });

    /*
     * @param obj
     * @param preg
     * @param str
     * @return null
     * */
    function name_replace(obj,preg,str){
        if(!preg || !str){
            return false;
        }
        var name = obj.attr('name');
        if(typeof name != 'undefined'){
            name = name.replace(preg,str);
            obj.attr('name',name);
            return true;
        }else{
            return false;
        }
    }
});