// JavaScript Document
$(function(){
    //获取物品配置
    var _goods ='';
    $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
        _goods = data;
        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

        $( ".item_id" ).autocomplete({
            source: _goods
        });
    },'json');

    //添加充值条件
    $('.c-add-reward').die('click');
    $('.c-add-reward').live('click', function(){
        var grade_num  = $(this).attr('add_grade_num');
        grade_num = parseInt(grade_num);

        var amount_num = $(this).attr('add_amount_num');
        amount_num = parseInt(amount_num);

        var name = "amount["+ grade_num +"]["+ amount_num +"]";
        var vip_level = "vip_level["+grade_num+"]["+amount_num+"]";
        var  add = '<tr>' +
            '<td style="width:100px;"> ' +
            '<span>全服累计次数</span><br/>'+
            '<em style="color: #ca1b36;font-size: 8px">*所有世界等级该组配置一致</em>'+
            '<input type="number"  class="amount" name="'+ name +'" value=""  /><br/> ' +
            '<span>VIP等级</span><br>'+
            '<em style="color: #ca1b36;font-size: 8px">*所有世界等级该组配置一致</em>'+
            '<input type="number"  class="vip_level" name="'+vip_level+'" value="" />' +
            '</td>' +
            '<td>' +
            '<table>' +
            '<tbody> ' +
            '<tr>' +
            '<td>' +
            '<table class="itemTable">' +
            '<thead>' +
            '<tr>' +
            '<td width="">物品ID</td>' +
            '<td width="">数量</td>' +
            '<td width="">职业</td>' +
            '<td width="">操作</td>' +
            '</tr>' +
            '</thead>' +

            '<tbody></tbody>' +

            '</table>' +
            '</td>' +
            '</tr>' +
            '</tbody>' +
            '</table>' +
            '<input type="button" value="添加物品条件" class="gbutton c-extra-additem" add_amount_num="'+ amount_num +'"  add_grade_num="'+ grade_num +'" > ' +
            '</td>' +

            '<td>' +
            '<input type="button" value="删除该条件" class="gbutton del-reward" />' +
            '<input type="button" value="复制该条件" class="gbutton c-copy-reward" />' +
            '</td>'+

        '</tr>';

        var obj = $(this).parent('td').next('td').children('table').children('tbody').children('tr').eq(1).children('td').eq(1).children('table').children('tbody').append(add);
        amount_num = amount_num + 1;//更新amount
        $(this).attr('add_amount_num', amount_num);
    });


    //更先die，然后再重监听
    $('.c-additem').die("click");
    $('.c-additem').live('click', function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);


        var item_id_name =  "config[item_id]["+ add_grade_num +"][]";
        var item_count_name = "config[item_count][" + add_grade_num +"][]";
        var career_name = "config[career]["+ add_grade_num +"][]";

        var item_weight_name = "config[weight]["+ add_grade_num +"][]";

        var itemWidget = '<tr class="itemWidget">' +
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="config_item_id" autocomplete="on">'  +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_count_name +'" value="1" class="config_item_count">' +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_weight_name +'" value="1" class="config_item_weight">' +
            '</td>' +
            '<td>' +
            '<select name="'+ career_name +'" class="config_item_career">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀</option>' +
            '<option value="2">弓</option>' +
            '<option value="3">法</option>' +
            '<select>' +
            '</td>' +

            '<td>' +
            '<input type="button" value="删除" class="gbutton delitem" title="删除该行">' +
            '</td>' +
            '</tr>';

        //alert($(this).prev('table').length);
        $(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget);

        //物品绑定
        $( ".config_item_id" ).autocomplete({
            source: _goods
        });

    });

    //复制该世界等级
    $('.c-copy-level').die('click');
    $('.c-copy-level').live('click', function() {
        //查找出最大条件
        var obj = $('#add_grade_num');
        var add_grade_num = obj.attr('add_grade_num');

        add_grade_num= parseInt(add_grade_num) + 1;
        obj.attr('add_grade_num', add_grade_num);
        //配置奖励
        var replace = "["+ add_grade_num + "]";

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();
        clone.find('.grade').attr('name', 'grade['+ add_grade_num +']');
        clone.find('.c-add-reward').attr('add_grade_num',add_grade_num);
        clone.find('.c-extra-additem').attr('add_grade_num',add_grade_num);
        clone.find('.c-additem').attr('add_grade_num',add_grade_num);

        //配置奖励
        clone.find('.config_item_id').attr('name', 'config[item_id]' + replace + '[]');
        clone.find('.config_item_count').attr('name', 'config[item_count]' + replace + '[]');
        clone.find('.config_item_weight').attr('name', 'config[weight]' + replace + '[]');
        clone.find('.config_item_career').attr('name', 'config[career]' + replace + '[]');

        var str = "$1["+ add_grade_num +"]";

        var reg = /(\w+)\[\d\]/g;

        //额外奖励
        var OitemId = clone.find('.item_id');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.item_count');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('.amount');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });
        //VIP等级
        var OitemId = clone.find('.vip_level');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //额外奖励
        var OitemId = clone.find('select');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        $(this).parent('td').parent('tr').parent('tbody').append(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });

    //复制该条件
    $('.c-copy-reward').die('click');
    $('.c-copy-reward').live('click', function() {

        var obj = $(this).parent('td').parent('tr').parent('tbody').parent('table').parent('td').parent('tr').parent('tbody').parent('table').parent('td').prev('td');

        // var obj = obj.find('input').eq(1);
        var obj = obj.find('.c-add-reward');

        var add_grade_num = obj.attr('add_grade_num');


        var add_amount_num = obj.attr('add_amount_num');

        //额外奖励
        var oTr = $(this).parent('td').parent('tr');
        var clone = oTr.clone();

        var str = "$1["+ add_grade_num +"]["+ add_amount_num +"]";

        var reg = /(\w+)\[\d\]\[\d\]/g;

        //道具ID
        var OitemId = clone.find('.item_id');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //数量
        var OitemId = clone.find('.item_count');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //累计次数
        var OitemId = clone.find('.amount');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });
        //VIP等级
        var OitemId = clone.find('.vip_level');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });

        //职业
        var OitemId = clone.find('select');
        $(OitemId).each(function(index) {
            var name = $(this).attr('name');

            var s = name.replace(reg, str);

            $(this).attr('name', s)
        });
        var OcExtraAdditem = clone.find('.c-extra-additem');

        OcExtraAdditem.attr('add_amount_num', add_amount_num);

        add_amount_num = parseInt(add_amount_num) + 1;

        obj.attr('add_amount_num', add_amount_num);//更新累计奖励列数（公共更新）

        $(this).parent('td').parent('tr').parent('tbody').append(clone);

        //物品绑定
        $(".config_item_id").autocomplete({
            source: _goods
        });

        $(".item_id").autocomplete({
            source: _goods
        });
    });



    //更先die，然后再重监听
    $('.c-extra-additem').die("click");
    $('.c-extra-additem').live('click', function(){
        var add_grade_num = $(this).attr('add_grade_num');
        add_grade_num = parseInt(add_grade_num);

        var add_amount_num = $(this).attr('add_amount_num');
        add_amount_num = parseInt(add_amount_num);


        var item_id_name =  "item_id["+ add_grade_num +"]["+ add_amount_num +"][]";
        var item_count_name = "item_count[" + add_grade_num +"]["+ add_amount_num +"][]";
        var career_name = "career["+ add_grade_num +"]["+ add_amount_num +"][]";

        var itemWidget = '<tr class="itemWidget">' +
            '<td>' +
            '<input style="width:200px" type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  +
            '</td>' +

            '<td>' +
            '<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
            '</td>' +

            '<td>' +
            '<select name="'+ career_name +'">' +
            '<option value="0">通用</option>' +
            '<option value="1">刀</option>' +
            '<option value="2">弓</option>' +
            '<option value="3">法</option>' +
            '<select>' +
            '</td>' +

            '<td>' +
            '<input type="button" value="删除" class="gbutton delitem" title="删除该行">' +
            '</td>' +
            '</tr>';

        //alert($(this).prev('table').length);
        $(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget);

        //物品绑定
        $( ".item_id" ).autocomplete({
            source: _goods
        });

    });

    //删除某个物品
    $('.delitem').die('click');
    $('.delitem').live('click', function() {
        $(this).parent().parent().remove();
    });

    //删除某个条件
    $('.del-reward').die('click');
    $('.del-reward').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });

    //删除该档充值
    $('.del-level').die('click');
    $('.del-level').live('click', function() {
        $(this).parent('td').parent('tr').remove();
    });
});

