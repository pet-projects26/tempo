<tr>
    <td>新服(多少天作为新服)</td>
    <td>
        <input type="text" name="day" value="14" >
    </td>
</tr>

<tr>
    <td style="width:150px; text-align: center;">
        <label>新服奖励</label>
        <input type="button" value="复制作为老服奖励" class="gbutton w-120 c-copy-level">
        <script type="text/javascript" src="templates/activity/activity_1005.js"></script>
    </td>
    <td>
       
        <table style="width:100%;" class="table-activity-reward">
           
            <tbody class="body-item">
                <tr>
                    <td>开启条件</td>
                    <td><input type="text" name="buyCount[1]" value="500" class="input"></td>
                </tr>

                <tr>
                    <td>单份价格</td>
                    <td><input type="text" name="gold[1]" value="30" class="input"></td>
                </tr>

                <tr>
                    <td>重复开奖</td>
                    <td>
                        <input style="width: 100%" type="text" name="count[1]" value="[[1000,1],[2000,2],[3000,3],[4000,4],[5000,5]]" class="input">
                    </td>
                </tr>

                <tr>
                    <td style="width:100px;">
                        大奖
                    </td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <table class="itemTable">
                                            <thead>
                                                <tr>
                                                    <td>职业</td>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>权重</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>刀</td>
                                                    <td>
                                                        <input style="width:200px" type="text" name="top[1][item_id][1]" class="item_id">
                                                    </td> 

                                                    <td>
                                                        <input type="text" name="top[1][item_count][1]" class="item_count" value="1">
                                                    </td> 

                                                    <td>
                                                        <input type="text" name="top[1][item_weight][1]" class="item_weight" value="1">
                                                    </td>
                                                </tr>
                                               
                                                <tr>
                                                    <td>弓</td>
                                                    <td>
                                                        <input style="width:200px" type="text" name="top[1][item_id][2]" class="item_id">
                                                    </td> 

                                                    <td>
                                                        <input type="text" name="top[1][item_count][2]" class="item_count" value="1">
                                                    </td> 

                                                    <td>
                                                        <input type="text" name="top[1][item_weight][2]" class="item_weight" value="1">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>法</td>
                                                    <td>
                                                        <input style="width:200px" type="text" name="top[1][item_id][3]" class="item_id">
                                                    </td> 

                                                    <td>
                                                        <input type="text" name="top[1][item_count][3]" class="item_count" value="1">
                                                    </td> 

                                                    <td>
                                                        <input type="text" name="top[1][item_weight][3]" class="item_weight" value="1">
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                 <tr>
                    <td style="width:100px;">
                        保底奖励
                    </td>
                    <td >
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <table class="itemTable">
                                            <thead>
                                                <tr>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>权重</td>
                                                    <td>角色</td>
                                                    <td>操作</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="button" value="添加物品条件" class="gbutton c-additem" add_amount_num="1">
                    </td>
                </tr>

            </tbody>
        </table>
    </td>
</tr>