<tr>
    <td>鉴宝方式</td>
    <td>
        <input type="text" name="type" value =1>
        <label>/* 鉴宝方式 1：直接消耗元宝  2：消耗满一点数量 3: 1、2混合</label>
    </td>
</tr>

<tr>
    <td>鉴宝1次</td>
    <td>
        <input type="text" name="gold" value = 50>
        <label>元宝</label>
    </td>
</tr>

<tr>
    <td>消耗</td>
    <td>
        <input type="text" name="freeGold" value = 400>
        <label>元宝</label>
    </td>
</tr>

<tr>
    <td style="width:150px;">
        <label>幸运大奖</label>
        <script type="text/javascript" src="templates/activity/activity_1006.js"></script>
    </td>
    <td>
        <table class="itemTable">
            <thead>
                <tr>
                    <td>职业</td>
                    <td>物品ID</td>
                    <td>数量</td>
                    <td>权重</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>刀</td>
                    <td>
                        <input style="width:200px" type="text" name="amount[1][goods_id]" class="item_id" >
                    </td>
                    <td>
                        <input  type="text" name="amount[1][goods_num]" value="1" >
                    </td>
                    <td>
                        <input  type="text" name="amount[1][weight]" value="1" >
                    </td>
                </tr>

                 <tr>
                    <td>弓</td>
                     <td>
                        <input style="width:200px" type="text" name="amount[2][goods_id]" class="item_id" >
                    </td>
                    <td>
                        <input type="text" name="amount[2][goods_num]" value="1" >
                    </td>
                    <td>
                        <input type="text" name="amount[2][weight]" value="1" >
                    </td>
                </tr>

                <tr>
                    <td>法</td>
                     <td>
                        <input style="width:200px" type="text" name="amount[3][goods_id]" class="item_id" >
                    </td>
                    <td>
                        <input type="text" name="amount[3][goods_num]" value="1" >
                    </td>
                    <td>
                        <input type="text" name="amount[3][weight]" value="1" >
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
                       
</tr>


<tr>
    <td style="width:150px;">
        <label>保底奖励</label>
    </td>
    <td>
        <table>
            <tbody>
                <tr>
                    <td>
                        <table class="itemTable">
                            <thead>
                                <tr>
                                    <td>物品ID</td>
                                    <td>数量</td>
                                    <td>权重</td>
                                    <td>操作</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0">
    </td>
</tr>