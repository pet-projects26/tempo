<tr>
	<td>最低消费</td>
	<td><input type="text" name="minGold" style="width: 200px;"></td>
</tr>
<tr>
    <td style="width:150px">
    	<label>世界等级</label>
    	<input type="text" name="grade[1]" value="" class="w-120">
    	<script type="text/javascript" src="templates/activity/activity_1030.js"></script>
    	<input type="button" value="删除该世界等级" class="gbutton del-level w-120">
    	<input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
   	<td>
		<!-- 配置奖励  -->
			<table style="width:100%;" class="table-activity-reward">
			<thead>
				<tr>
					<th>名次</th>
					<th>职业</th>
					<th>物品ID</th>
					<th>物品数量</th>
				</tr>
			</thead>
			<tbody class="body-item">
				<!-- 第一名开始 -->
				<tr>
					<td rowspan="3">第一名</td>
					<td>刀</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][first][career1]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][first][career1]" value="1" class="item_count">
					</td>
				</tr>

				<tr>
					<td>剑</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][first][career2]]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][first][career2]" value="1" class="item_count">
					</td>
				</tr>

				<tr>
					<td>法</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][first][career3]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][first][career3]" value="1" class="item_count">
					</td>
				</tr>
				<!-- 第一名结束 -->

				<!-- 第二名开始 -->
				<tr>
					<td rowspan="3">第二名</td>
					<td>刀</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][second][career1]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][second][career1]" value="1" class="item_count">
					</td>
				</tr>

				<tr>
					<td>剑</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][second][career2]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][second][career2]" value="1" class="item_count">
					</td>
				</tr>

				<tr>
					<td>法</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][second][career3]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][second][career3]" value="1" class="item_count">
					</td>
				</tr>
				<!-- 第二名结束 -->

				<!-- 第二名开始 -->
				<tr>
					<td rowspan="3">第二名</td>
					<td>刀</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][third][career1]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][third][career1]" value="1" class="item_count">
					</td>
				</tr>

				<tr>
					<td>剑</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][third][career2]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][third][career2]" value="1" class="item_count">
					</td>
				</tr>

				<tr>
					<td>法</td>
					<td>
						<input style="width:200px" type="text" name="item_id[1][third][career3]" class="item_id ui-autocomplete-input" >
					</td>
					<td>
						<input type="text" name="item_count[1][third][career3]" value="1" class="item_count">
					</td>
				</tr>
				<!-- 第二名结束 -->



			</tbody>
		</table>
		<!-- 配置奖励 -->
	</td>
</tr>

<!-- 标记 -->
<tr style="display: none;">
	<td colspan="2">
		<input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
	</td>
</tr>
<!-- 标记 -->