<tr>
    <td style="width: 150px;">类型</td>
    <td>
    	<input type="text" name="form" value = "1" style="width: 200px;">
    </td>
</tr>

<tr>
    <td style="width: 150px;">短述</td>
    <td>
        <input type="text" name="shortDesc" value = "" style="width: 200px;">
    </td>
</tr>

<tr>
    <td style="width: 150px;">描述</td>
    <td>
        <textarea rows="10" cols="100" name="description"></textarea>
    </td>
</tr>


<tr>
    <td style="width: 150px;">等级</td>
    <td>
        <input type="text" name="level" value = "" style="width: 200px;">
    </td>
</tr>


<tr>
    <td style="width: 150px;">怪物等级</td>
    <td>
        <input type="text" name="monsterLevel" value = "0" style="width: 200px;">
    </td>
</tr>

<tr>
    <td style="width: 150px;">掉落机率</td>
    <td>
        <input type="text" name="random" value = "0" style="width: 200px;">
    </td>
</tr>

<tr>
    <td style="width: 150px;">iconId</td>
    <td>
        <input type="text" name="iconId" value = "3280040010008" class="item_id" style="width: 200px;">
    </td>
</tr>

<tr>
    <td style="width: 150px;">掉落物品</td>
    <td>
        <input type="text" name="dropItemId" value = "3280040010008" class="item_id" style="width: 200px;">
    </td>
</tr>

<tr>
    <td style="width: 150px;">怪物ID</td>
    <td>
        <input type="text" name="monsterId" value = "[]" style="width: 200px;">
        <script type="text/javascript" src="templates/activity/activity_1010.js"></script>
    </td>
</tr>

<tr>
    <td style="width: 150px;">地图范围</td>
    <td>
        <input type="text" name="mapRange" value = "[100100, 102000]" style="width: 200px;">
    </td>
</tr>


<tr>
    <td>奖励配置</td>
    <td>
        <table style="width:100%;" class="table-activity-reward">
            <thead>
            <tr>
                <th>奖励</th>
            </tr>
            </thead>
            <tbody class="body-item">
            <tr>
                <td>
                    <table>
                    <tbody>
                    <tr>
                        <td>
                            <table class="itemTable">
                            <thead>
                                <tr>
                                    <td>兑换消耗的物品个数</td>
                                    <td>兑换得到的物品</td>
                                    <td>得到的物品个数</td>
                                    <td>可兑换次数</td>
                                    <td>操作</td>
                                </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                    <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

