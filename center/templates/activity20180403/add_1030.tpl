<tr>
    <td style="width:150px">
    	<label>世界等级</label>
    	
    	<input type="text" name="grade[1]" value="" class="w-120">
    	<script type="text/javascript" src="templates/activity/activity_1030.js"></script>

    	<input type="button" value="添加额外奖励" class="gbutton c-add-reward w-120" add_grade_num="1" add_amount_num="1">
    	<input type="button" value="删除该世界等级" class="gbutton del-level w-120">
    	<input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
    </td>
   	<td>
   		<table>
   			<tr>
   				<td>配置奖励</td>
   				<td>
   					<!-- 配置奖励  -->
   						<table style="width:100%;" class="table-activity-reward">
						
							<tbody class="body-item">
								<tr>
									<td>
										<table>
											<tbody>
												<tr>
													<td>
														<table class="itemTable">
															<thead>
																<tr>
																	<td>物品ID</td>
																	<td>数量</td>
																	<td>权重</td>
																	<td>稀有</td>
																	<td>角色</td>
																	<td>操作</td>
																</tr>
															</thead>
															<tbody>
															</tbody>
														</table>
													</td>
													<td>
													</td>
												</tr>
											</tbody>
										</table>
										<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
									</td>
								</tr>
							</tbody>
						</table>
   					<!-- 配置奖励 -->
   				</td>
   			</tr>

   			<tr class="tr-extra">
   				<td>额外奖励</td>
   				<td>
   					<!-- 配置奖励  -->
   						<table style="width:100%;" class="table-activity-reward">
							<thead>
								<tr>
									<th>条件</th>
									<th>奖励</th>
									<th>删除</th>
								</tr>
							</thead>
							<tbody class="body-item">
							</tbody>
						</table>
   					<!-- 配置奖励 -->
   				</td>
   			</tr>
   		</table>
	</td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
	<td colspan="2">
		<input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
	</td>
</tr>
<!-- 标记 -->