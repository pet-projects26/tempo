<tr>
    <td style="width:auto;">新服(多少天作为新服)<input style="margin-left: 43px" type="text" name="day" value="14"> <span style="color: #FF0000">*支持配置</span></td>
</tr>
<tr>
    <td>一张牌元宝数量 <input style="margin-left: 75px" type="number" name="gold" value="20"> <span style="color: #FF0000">*支持配置</span></td>
</tr>
<tr>
    <td>牌总数量<input style="margin-left: 115px;background-color: #dddddd" readonly="readonly" type="number" name="totalCount" value="10"></td>
</tr>
<tr>
    <td>中福概率<input style="margin-left: 116px;background-color: #dddddd" readonly="readonly" type="number" name="per" value="0.1"></td>
</tr>
<tr>
    <td>中福规则<input type="text" readonly="readonly" value="[[1,1,3],[2,2,9],[3,7,10]]" style="width: 300px;margin-left: 116px;background-color: #dddddd"/></td>
</tr>
<tr>
    <td>
    <input style="width: 800px;background-color: #dddddd" readonly="readonly" type="text" value='["点击翻牌集福得超级大奖！","还差一点点，就能获得一福大奖","再来一个福即可获得二福大奖","还差最后一个福即可获得三福超级大奖"]'/>
    </td>
</tr>
<tr>
    <td colspan="2" class="add-roll-grad" roll_num = 1 add-roll-num="1"><!--标识轮-->
        <table class="new-ser-msg" style="padding: 0">
            <tbody>
            <tr class="big-price">
                <td style="width:150px; text-align: center;">
                    <label class="title-name">新服奖励</label>
                    <input type="hidden" value="1" class="roll-hidden"/>
                    <input type="text" value="第1轮奖励" class="roll-text"
                           style="width: 120px;text-align: center"/>
                    <input type="button" value="复制此轮奖励" class="gbutton w-120 c-copy-level" style="margin: 5px 14px">
                    <input type="button" value="删除此轮奖励" class="gbutton w-120 c-del-level" style="margin: 5px 14px">
                    <script type="text/javascript" src="templates/activity/activity_1032.js"></script>
                </td>
                <td>
                    <table style="width:100%;" class="table-activity-reward">
                        <tbody class="body-item">
                        <tr>
                            <td style="width:100px;">
                                1福奖励
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr class="list-num" list_num="1">
                                        <td>
                                            <table class="itemTable">
                                                <thead>
                                                <tr style="">
                                                    <td style="">物品ID</td>
                                                    <td>数量</td>
                                                    <td>角色</td>
                                                    <!--<td>新老服区分</td>-->
                                                    <td>操作</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><input style="width:200px" type="text" name="list[1][topPrize][1][1][1][itemId]" class="item_id" autocomplete="on"></td>
                                                    <td><input style="width:75px" type="number" name="list[1][topPrize][1][1][1][item_count]" value="1" class="item_count"></td>
                                                    <td>
                                                        <select name="list[1][topPrize][1][1][1][career_name]" class="career_name">
                                                            <option value="0">通用</option>
                                                            <option value="1">刀剑师</option>
                                                            <option value="2">羽翎师</option>
                                                        </select>
                                                    </td>
                                                    <!--<td><input style="width:75px" type="number"  name="list[1][topPrize][1][1][][cy_cut]"  value="1" class="cy_cut"/></td>-->
                                                    <!--<td><input style="width:75px" type="number" name="list[1][topPrize][1][1][][ser_day]" value="14" class="ser_day"/></td>-->
                                                    <td>
                                                        <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                        <input type="button" value="复制" class="gbutton copyitem" add_amount_num="1" title="复制">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                   add_amount_num="1">-->
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                2福奖励
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr class="list-num" list_num="1">
                                        <td>
                                            <table class="itemTable">
                                                <tbody>
                                                <tr>
                                                    <td><input style="width:200px" type="text" name="list[1][topPrize][1][2][1][itemId]" class="item_id" autocomplete="on"></td>
                                                    <td><input style="width:75px" type="number" name="list[1][topPrize][1][2][1][item_count]" value="1" class="item_count"></td>
                                                    <td>
                                                        <select name="list[1][topPrize][1][2][1][career_name]" class="career_name">
                                                            <option value="0">通用</option>
                                                            <option value="1">刀剑师</option>
                                                            <option value="2">羽翎师</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                        <input type="button" value="复制" class="gbutton copyitem" add_amount_num="1" title="复制">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                   add_amount_num="1">-->
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                3福奖励
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr class="list-num" list_num="1">
                                        <td>
                                            <table class="itemTable">
                                                <tbody>
                                                <tr>
                                                    <td><input style="width:200px" type="text" name="list[1][topPrize][1][3][1][itemId]" class="item_id" autocomplete="on"></td>
                                                    <td><input style="width:75px" type="number" name="list[1][topPrize][1][3][1][item_count]" value="1" class="item_count"></td>
                                                    <td>
                                                        <select name="list[1][topPrize][1][3][1][career_name]" class="career_name">
                                                            <option value="0">通用</option>
                                                            <option value="1">刀剑师</option>
                                                            <option value="2">羽翎师</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                        <input type="button" value="复制" class="gbutton copyitem" add_amount_num="1" title="复制">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                   add_amount_num="1">-->
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr class="normal-price">
                <td style="text-align: center">
                    普通奖励
                </td>
                <td class="add-normal-grad" add-normal-num="1">
                    <table>
                        <tbody>
                        <tr>
                            <td><!--标识-->
                                <table class="itemTable">
                                    <thead>
                                    <tr>
                                        <td>物品ID</td>
                                        <td>数量</td>
                                        <td>权重</td>
                                        <td>角色</td>
                                        <td>操作</td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="add_item_btn">
                                        <td><input type="button" value="添加物品条件" class="gbutton c-addItem" list-type="1" add_amount_num="1" add_list_num=1></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <input type="button" onclick="return confirm('确定删除该组条件？')" class="gbutton del-normal" value="删除该条件" />
                                <!--<input type="button" style="margin-top: 5px" class="gbutton copy-normal" value="复制该条件"/>-->
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr style="background-color: #f0f0f0">
    <td colspan="2">
        <input type="button" value="复制作为老服奖励" onclick="return confirm('确定复制新服奖励？')" class="gbutton w-120 c-copy-asOld"/>
        <input type="button" value="删除老服奖励" onclick="return confirm('确定删除老服奖励？')" class="gbutton w-120 c-del-new"/>
        <!--<input type="button" value="添加奖励" onclick="return confirm('确定添加集福奖励？')" class="gbutton w-120 add-reward"/>-->
    </td>
</tr>