<tr>
    <td colspan="2" class="add-roll-grad" roll_num=1 add-roll-num="1"><!--标识轮-->
        <table class="new-ser-msg" style="padding: 0">
            <tbody>
            <tr class="big-price">
                <td style="width:150px; text-align: center;">
                    <input type="button" value="添加礼包" class="gbutton w-120 c-add-level" style="margin: 5px 14px">
                    <input type="button" value="删除礼包" class="gbutton w-120 c-del-level" style="margin: 5px 14px">
                    <input type="button" value="复制礼包" class="gbutton w-120 c-copy-level" style="margin: 5px 14px">
                    <script type="text/javascript" src="templates/activity/activity_1034.js"></script>
                </td>
                <td>
                    <table style="width:100%;" class="table-activity-reward">
                        <tbody class="body-item">
                        <tr></tr>
                        <tr>
                            <td style="width:100px;border-bottom: 1px solid #000000">
                                礼包名称
                            </td>
                            <td style="border-bottom: 1px solid #000000">礼包内容</td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                原价
                            </td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                现价
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="price_name" type="text" name="config[price_name][1]">
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr></tr>
                                    <tr>
                                        <td>物品ID</td>
                                        <td>数量</td>
                                        <td>角色</td>
                                        <td>操作</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="1"
                                       add_amount_num="1" add_grade_num="1">
                            </td>
                            <td>
                                <input class="config_item_price_old" style="width: 80px" type="number" name="config[price_old][1]" />
                            </td>
                            <td>
                                <input class="config_item_price_now" style="width: 80px" type="number" name="config[price_now][1]">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="normal-price">
                <td style="text-align: center">
                    大礼包
                </td>
                <td class="add-normal-grad" add-normal-num="1">
                    <table>
                        <tbody>
                        <tr></tr>
                        <tr>
                            <td style="width: 100px;border-bottom: 1px solid #000000">礼包名称</td>
                            <td style="border-bottom: 1px solid #000000">礼包内容</td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                原价
                            </td>
                            <td style="width:8%;border-bottom: 1px solid #000000">
                                现价
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="big_price_name" /></td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr></tr>
                                    <tr>
                                        <td>物品ID</td>
                                        <td>数量</td>
                                        <td>职业</td>
                                        <td>操作</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton b-additem" add_item_num="1"
                                       add_amount_num="1" add_grade_num="1">
                            </td>
                            <td>
                                <input class="item_price_old" style="width: 80px" type="number" name="price_old" />
                            </td>
                            <td>
                                <input class="item_price_now" style="width: 80px" type="number" name="price_now">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1">
    </td>
</tr>
<!-- 标记 -->