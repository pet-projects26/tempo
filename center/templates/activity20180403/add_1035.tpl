<tr>
    <td>奖池元宝数</td>
    <td>
        <input type="number" class="read_only" readonly="readonly" value="10000">
    </td>
</tr>
<tr>
    <td>抽一次需要的元宝</td>
    <td>
        <input type="number" value="" name="once_gold" />
    </td>
</tr>
<tr>
    <td>抽十次需要的元宝</td>
    <td>
        <input type="number" value="" name="tentimes_gold">
    </td>
</tr>
<tr>
    <td>获奖记录</td>
    <td>
        <input type="number" class="read_only" readonly="readonly" value="50">
    </td>
</tr>
<tr>
    <td>抽一次可获积分</td>
    <td>
        <input type="number" name="once_score" value="">
    </td>
</tr>
<tr>
    <td>单次抽奖进入奖池的元宝</td>
    <td>
        <input type="number" name="input_gold" value="">
    </td>
</tr>
<tr>
    <td>新服</td>
    <td>
        <input type="number" name="new_server" value="">&nbsp;&nbsp;天
    </td>
</tr>
<tr>
    <td style="width:150px">
        <script type="text/javascript" src="templates/activity/activity_1035.js"></script>
        <span>奖励配置</span>
        <!--<input type="button" value="添加积分奖励" class="gbutton c-add-reward w-120" add_grade_num="1" add_amount_num="1">-->
        <!--<input type="button" value="删除该奖励" class="gbutton del-level w-120">-->
        <!--<input type="button" value="复制该奖励" class="gbutton w-120 c-copy-level">-->
    </td>
    <td>
        <table>
            <tr>
                <td style="width: 10%">普通奖励</td>
                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">

                        <tbody class="body-item">
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table class="itemTable">
                                                <thead>
                                                <tr>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>权重</td>
                                                    <td>角色</td>
                                                    <td>操作</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>

            <tr class="tr-extra">
                <td>积分兑换</td>
                <td>
                    <!-- 配置奖励  -->
                    <table style="width:100%;" class="table-activity-reward">
                        <thead>
                        <tr>
                            <th style="width: 16%">条件</th>
                            <th>奖励</th>
                            <th>删除</th>
                        </tr>
                        </thead>
                        <tbody class="body-item">
                            <tr>
                                <td style="width: 100px">
                                    <input type="number" style="width: 80px" class="score" name="score_num" value="" />&nbsp;&nbsp;积分
                                </td>
                                <td>
                                    <table>
                                        <tdbody>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <thead>
                                                        <tr>
                                                            <td width="">物品ID</td>
                                                            <td width="">数量</td>
                                                            <td width="">职业</td>
                                                            <td width="">操作</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tdbody>
                                    </table>
                                    <input type="button" value="添加物品奖励" class="gbutton c-extra-additem" add_amount_num="1" add_grad_num="1">
                                </td>
                                <td>
                                    <input type="button" value="删除该条件" class="gbutton del-reward" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- 配置奖励 -->
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- 标记 -->
<tr style="display: none;">
    <td colspan="2">
        <input type="button" value="标记" id="add_grade_num" add_grade_num="1" >
    </td>
</tr>
<!-- 标记 -->