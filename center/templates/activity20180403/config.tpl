<script type="text/javascript" src="templates/activity/activity.js"></script>
<script type="text/javascript" src="templates/activity/jquery.fix.clone.js"></script>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.cur {
	background-color: beige;
}


.itable-color tbody > tr:nth-child(odd) {
  background: #f1f1b0;
} 
.del-level {
	margin: 5px 0px;
}

.itable tr:nth-child(even) {
    background-color: #f0f0f0;
}

.w-120 {
	width: 120px;
	float: left;
    margin: 5px;
}
.read_only{
	background-color: #dddddd
}
</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
    <div class="type-list">
      <ul>
		<!--switch the first lam type to get the aType-->
      	<{if $big_act_id == 1}>
	        <li class="aType cur" data-type="1004" >首充团购</li>
	        <li class="aType" data-type="1002">单笔充值</li>
	        <li class="aType" data-type="1003">累积充值</li>
		  	<li class="aType" data-type="1033">扭蛋</li>
        <{elseif ($big_act_id == 2)}>
        	<li class="aType cur" data-type="1008" >烟花</li>
        	<li class="aType" data-type="1030" >砸蛋</li>
        	<li class="aType" data-type="1009" >排行榜</li>
	        <li class="aType" data-type="1006">幸运鉴宝</li>
	        <li class="aType" data-type="1005">云购</li>
		  	<li class="aType" data-type="1032">翻牌集福</li>
		  	<li class="aType" data-type="1034">聚划算</li>
		  	<li class="aType" data-type="1035">幸运大转盘</li>
        <{elseif ($big_act_id == 3)}>
        	<li class="aType cur" data-type="1007" >全民嗨</li>
        	<li class="aType" data-type="1010" >掉落活动</li>
        	<li class="aType" data-type="1020" >全民活动</li>
        <{/if}>
      </ul>
    </div>
    <table class="itable itable-color">
      	<tbody>
        <tr>
          <td style="width:150px;">活动名称</td>
          <td><input type="text" name="title" id="title" style="width:200px;"></td>
        </tr>
        
        <tr id="add_time">
          <td style="width:150px;">活动时间</td>
          <td><input type="text" name="start_time" id="start_time" style="width:200px;">
            ~
            <input type="text" name="end_time" id="end_time" style="width:200px;"></td>
        </tr>
     	</tbody>

     	<tr>
          <td style="width:150px;">开启等级</td>
          <td><input type="text" name="openLvl" value="1" style="width:200px;"></td>
        </tr>
    </table>
    	
    <table class="itable itable-color">
    
    	<tbody id ="load">
		<!--load the div switch type-->
    	<{if $big_act_id == 1}>
	        <script type="text/javascript">
				$('#load').load('templates/activity/add_1004.tpl');
			</script>
        <{elseif ($big_act_id == 2)}>
        	 <script type="text/javascript">
				$('#load').load('templates/activity/add_1008.tpl');
			</script>
        <{elseif ($big_act_id == 3)}>
        	 <script type="text/javascript">
				$('#load').load('templates/activity/add_1007.tpl');
			</script>
        <{/if}>
			
    	</tbody>
    </table>	

    <table class="itable itable-color">
      	<tbody>
        <tr>
          <td style="width:150px;">活动描述</td>
          <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"></textarea></td>
        </tr>
		
		<tr>
			<td style="width:150px">活动开关</td>
			<td>
				<select name="state">
					<option value="0">开</option>
					<option value="1">关</option>
				</select>
			</td>
		</tr>

		<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/channelGroup_server.tpl' }>
        <!-- 服务器，  -->

        <tr>
          <td colspan="2">
          	<!-- 活动ID -->
			<input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
			<input type="hidden" class="act_id" name="act_id"  >
			<!--活动ID  -->
			<input type="submit" class="gbutton" value="保存" id="charge">
            <input type="hidden" id="error" value="1">
 		</td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
