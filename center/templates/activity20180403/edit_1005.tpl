<script type="text/javascript" src="templates/activity/activity.js"></script>
<script type="text/javascript" src="templates/activity/activity_1005.js"></script>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.cur {
	background-color: beige;
}


.itable-color tbody > tr:nth-child(odd) {
  background: #f1f1b0;
} 
.del-level {
	margin: 5px 0px;
}

.itable tr:nth-child(even) {
    background-color: #f0f0f0;
}

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
    <div class="type-list">
      <ul>
        <li class="aType cur" data-type="<{$row.act_id}>" ><{$row.act}></li>
      </ul>
    </div>
    <table class="itable itable-color">
      <tbody>
        <tr>
          <td style="width:150px;">活动名称</td>
          <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
        </tr>
        <tr>
          <td style="width:150px;">活动时间</td>
          <td>
          	<input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
            ~
            <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
           </td>
        </tr>
    	</tbody>
    </table>

    <table class="itable itable-color">
    	<tbody id="load">
       	<!--  -->
       	<tr>
		    <td>新服(多少天作为新服)</td>
		    <td><input type="text" name="day" value="<{$row.award.day}>" ></td>
		</tr>
       	<{foreach from=$row.award.top key=k item=r }>
		<tr>
		    <td style="width:150px; text-align: center;">
		    	<{if $k == 1}>
		    		<label>新服奖励</label>
		    	<{else}>
		    		<label>老服奖励</label>
		    	<{/if}>

		    	
		    	<{if $row.topLen == 1 }>
		        <input type="button" value="复制作为老服奖励" class="gbutton w-120 c-copy-level">
		        <{/if}>
		    </td>
		    <td>
		       
		        <table style="width:100%;" class="table-activity-reward">
		           
		            <tbody class="body-item">
		                <tr>
		                    <td>开启条件</td>
		                    <td><input type="text" name="buyCount[<{$k}>]" value="<{$row.award.buyCount[$k]}>" class="input"></td>
		                </tr>

		                <tr>
		                    <td>单份价格</td>
		                    <td><input type="text" name="gold[<{$k}>]" value="<{$row.award.gold[$k]}>" class="input"></td>
		                </tr>

		                <tr>
		                    <td>重复开奖</td>
		                    <td>
		                        <input style="width: 100%" type="text" name="count[<{$k}>]" value="<{$row.award.count[$k]}>" class="input">
		                    </td>
		                </tr>

		                <tr>
		                    <td style="width:100px;">
		                        大奖
		                    </td>
		                    <td>
		                        <table>
		                            <tbody>
		                                <tr>
		                                    <td>
		                                        <table class="itemTable">
		                                            <thead>
		                                                <tr>
		                                                    <td>职业</td>
		                                                    <td>物品ID</td>
		                                                    <td>数量</td>
		                                                    <td>权重</td>
		                                                </tr>
		                                            </thead>
		                                            <tbody>
		                                            	<{foreach from=$r.item_id key=k2 item=v}>
		                                                <tr>
		                                                    <td>
		                                                    	<{if $k2 == 1}>
		                                                    	刀
		                                                    	<{elseif $k2 == 2}>
		                                                    	弓
		                                                    	<{elseif $k2 == 3}>
		                                                    	法
		                                                    	<{/if}>
		                                                    </td>
		                                                    <td>
		                                                        <input style="width:200px" type="text" name="top[<{$k}>][item_id][<{$k2}>]" class="item_id" value="<{$v}>">
		                                                    </td> 

		                                                    <td>
		                                                        <input type="text" name="top[<{$k}>][item_count][<{$k2}>]" class="item_count" value="<{$r.item_count[$k2]}>">
		                                                    </td> 

		                                                    <td>
		                                                        <input type="text" name="top[<{$k}>][item_weight][<{$k2}>]" class="item_weight" value="<{$r.item_weight[$k2]}>">
		                                                    </td>
		                                                </tr>
		                                                <{/foreach}>
		                                               
		                                            </tbody>
		                                        </table>
		                                    </td>
		                                </tr>
		                            </tbody>
		                        </table>
		                    </td>
		                </tr>

		                 <tr>
		                    <td style="width:100px;">
		                        保底奖励
		                    </td>
		                    <td>
		                        <table>
		                            <tbody>
		                                <tr>
		                                    <td>
		                                        <table class="itemTable">
		                                            <thead>
		                                                <tr>
		                                                    <td>物品ID</td>
		                                                    <td>数量</td>
		                                                    <td>权重</td>
		                                                    <td>角色</td>
		                                                    <td>操作</td>
		                                                </tr>
		                                            </thead>
		                                            <tbody>
		                                            	<{foreach from=$row.itemId[$k] key=k2 item=v}>
		                                            		<tr class="itemWidget">
																<td>
																	<input style="width:200px" type="text" name="item_id[<{$k}>][]" value="<{$v}>" class="item_id">
																</td>
																<td>
																	<input type="text" name="item_count[<{$k}>][]" value="<{$row.itemCount[$k][$k2]}>" class="item_count">
																</td>
																<td>
																	<input type="text" name="weight[<{$k}>][]" value="<{$row.weight[$k][$k2]}>" class="item_count">
																</td>
																<td>
																	<select name="career[<{$k}>][]">
																		<option value="0" <{if $row.career[$k][$k2] == 0}> seleted <{/if}>>通用</option>
																		<option value="1" <{if $row.career[$k][$k2] == 1}> selected <{/if}>>刀剑师</option>
																		<option value="2" <{if $row.career[$k][$k2] == 2}> seleteed 羽翎师 <{/if}> </option>
																	</select>
																</td>
																<td>
																	<input type="button" value="删除" class="gbutton delitem" title="删除该行">
																</td>
															</tr>
		                                            	<{/foreach}>
		                                            </tbody>
		                                        </table>
		                                    </td>
		                                    <td>
		                                    </td>
		                                </tr>
		                            </tbody>
		                        </table>
		                        <input type="button" value="添加物品条件" class="gbutton c-additem" add_amount_num="<{$k}>">
		                    </td>
		                </tr>

		            </tbody>
		        </table>
		    </td>
		</tr>
		<{/foreach}>
       	<!--  -->
       	</tbody>
    </table>

    <table class="itable itable-color">
        <tr>
          <td style="width:150px;">活动描述</td>
          <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
        </tr>
		
		<tr>
			<td style="width:150px">活动开关</td>
			<td>
				<select name="state">
					<option value="0" <{if $row.state == 0 }> selected <{/if}>>开</option>
					<option value="1" <{if $row.state == 1 }> selected <{/if}>>关</option>
				</select>
			</td>
		</tr>

		<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/channelGroup_server_edit.tpl' }>
        <!-- 服务器，  -->

        <tr>
          <td colspan="2">
          	<!-- 活动ID -->
			<input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
			<input type="hidden" name="act_id"  value="<{$row.act_id}>" >
			<input type="hidden" name="id" value="<{$row.id}>">
			<!--活动ID  -->

			<input type="submit" class="gbutton" value="保存" id="charge">
            <input type="hidden" id="error" value="1">
 		</td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
