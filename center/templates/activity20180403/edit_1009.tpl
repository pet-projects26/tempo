<script type="text/javascript" src="templates/activity/activity.js"></script>
<script type="text/javascript" src="templates/activity/activity_1030.js"></script>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.cur {
	background-color: beige;
}


.itable-color tbody > tr:nth-child(odd) {
  background: #f1f1b0;
} 
.del-level {
	margin: 5px 0px;
}

.itable tr:nth-child(even) {
    background-color: #f0f0f0;
}

.w-120 {
	width: 120px;
	float: left;
    margin: 5px;
}

</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
    <div class="type-list">
      <ul>
        <li class="aType cur" data-type="<{$row.act_id}>" ><{$row.act}></li>
      </ul>
    </div>
    <table class="itable itable-color">
      <tbody>
        <tr>
          <td style="width:150px;">活动名称</td>
          <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
        </tr>


      	<tr>
          <td style="width:150px;">活动时间</td>
          <td>
          	<input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
            ~
            <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
           </td>
        </tr>
    	</tbody>
    </table>

       
    <table class="itable itable-color">
       <tbody id="load">
       	<tr>
			<td>最低消费</td>
			<td><input type="text" name="minGold" value="<{$row.award.minGold}>" style="width: 200px;"></td>
		</tr>
       <{foreach from=$row.grade key=k item=v}>
       <tr>
		    <td style="width:150px">
		    	<label>世界等级</label>
		    	
		    	<input type="text" name="grade[<{$k}>]" value="<{$v}>" class="w-120 grade">

		    	<input type="button" value="添加额外奖励" class="gbutton c-add-reward w-120" add_grade_num="<{$k}>" add_amount_num="0">
		    	<input type="button" value="删除该世界等级" class="gbutton del-level w-120">
		    	<input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
		    </td>
			<td>
				<!-- 奖励开始 -->
				<table style="width:100%;" class="table-activity-reward">
					<thead>
						<tr>
							<th>名次</th>
							<th>职业</th>
							<th>物品ID</th>
							<th>物品数量</th>
						</tr>
					</thead>
					<tbody class="body-item">
						<!-- 第一名开始 -->
						<tr>
							<td rowspan="3">第一名</td>
							<td>刀</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][first][career1]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].first.career1}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][first][career1]" value="<{$row.itemCount[$k].first.career1}>" class="item_count">
							</td>
						</tr>

						<tr>
							<td>弓</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][first][career2]]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].first.career2}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][first][career2]" value="<{$row.itemCount[$k].first.career2}>" class="item_count">
							</td>
						</tr>

						<tr>
							<td>法</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][first][career3]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].first.career3}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][first][career3]" value="<{$row.itemCount[$k].first.career3}>" class="item_count">
							</td>
						</tr>
						<!-- 第一名结束 -->
						<!-- 第二名开始 -->
						<tr>
							<td rowspan="3">第二名</td>
							<td>刀</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][second][career1]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].second.career1}>" >
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][second][career1]" value="<{$row.itemCount[$k].second.career1}>" class="item_count">
							</td>
						</tr>

						<tr>
							<td>弓</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][second][career2]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].second.career2}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][second][career2]" value="<{$row.itemCount[$k].second.career2}>" class="item_count">
							</td>
						</tr>

						<tr>
							<td>法</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][second][career3]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].second.career3}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][second][career3]" value="<{$row.itemCount[$k].second.career3}>" class="item_count">
							</td>
						</tr>
						<!-- 第二名结束 -->

						<!-- 第三名开始 -->
						<tr>
							<td rowspan="3">第三名</td>
							<td>刀</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][third][career1]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].third.career1}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][third][career1]" value="<{$row.itemCount[$k].third.career1}>" class="item_count">
							</td>
						</tr>

						<tr>
							<td>弓</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][third][career2]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].third.career2}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][third][career2]" value="<{$row.itemCount[$k].third.career2}>" class="item_count">
							</td>
						</tr>

						<tr>
							<td>法</td>
							<td>
								<input style="width:200px" type="text" name="item_id[<{$k}>][third][career3]" class="item_id ui-autocomplete-input" value="<{$row.itemId[$k].third.career3}>">
							</td>
							<td>
								<input type="text" name="item_count[<{$k}>][third][career3]" value="<{$row.itemCount[$k].third.career3}>" class="item_count">
							</td>
						</tr>
						<!-- 第二名结束 -->
					</tbody>
				</table>
				<!-- 奖励结束 -->

			</td>
		</tr>
		<{/foreach}>
		<!-- 标记 -->
		<tr style="display: none;">
			<td colspan="2">
				<input type="button" value="标记" id="add_grade_num" add_grade_num=1 >
			</td>
		</tr>
		<!-- 标记 -->
		</tbody>
		</table>

		<table class="itable itable-color">
		<tbody>
        <tr>
          <td style="width:150px;">活动描述</td>
          <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
        </tr>
		
		<tr>
			<td style="width:150px">活动开关</td>
			<td>
				<select name="state">
					<option value="0" <{if $row.state == 0 }> selected <{/if}>>开</option>
					<option value="1" <{if $row.state == 1 }> selected <{/if}>>关</option>
				</select>
			</td>
		</tr>

		<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/channelGroup_server_edit.tpl' }>
        <!-- 服务器，  -->

        <tr>
          <td colspan="2">
          	<!-- 活动ID -->
			<input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
			<input type="hidden" name="act_id"  value="<{$row.act_id}>" >
			<input type="hidden" name="id" value="<{$row.id}>">
			<!--活动ID  -->

			<input type="submit" class="gbutton" value="保存" id="charge">
            <input type="hidden" id="error" value="1">
 		</td>
        </tr>
      </tbody>
    </table>
  </form>
</div>

