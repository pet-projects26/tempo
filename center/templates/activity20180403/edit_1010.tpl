<script type="text/javascript" src="templates/activity/activity.js"></script>
<script type="text/javascript" src="templates/activity/activity_1010.js"></script>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.cur {
	background-color: beige;
}


.itable-color tbody > tr:nth-child(odd) {
  background: #f1f1b0;
} 
.del-level {
	margin: 5px 0px;
}

.itable tr:nth-child(even) {
    background-color: #f0f0f0;
}

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
    <div class="type-list">
      <ul>
        <li class="aType cur" data-type="<{$row.act_id}>" ><{$row.act}></li>
      </ul>
    </div>
    <table class="itable itable-color">
      <tbody>
        <tr>
          <td style="width:150px;">活动名称</td>
          <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
        </tr>

        <tr>
          <td style="width:150px;">活动时间</td>
          <td>
          	<input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
            ~
            <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>">
           </td>
        </tr>

        <tr>
		    <td style="width: 150px;">类型</td>
		    <td>
		    	<input type="text" name="form" value = "1" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">短述</td>
		    <td>
		        <input type="text" name="shortDesc" value ="<{$row.award.shortDesc}>" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">描述</td>
		    <td>
		        <textarea rows="10" cols="100" name="description"><{$row.award.description}></textarea>
		    </td>
		</tr>


		<tr>
		    <td style="width: 150px;">等级</td>
		    <td>
		        <input type="text" name="level" value = "<{$row.award.level}>" style="width: 200px;">
		    </td>
		</tr>


		<tr>
		    <td style="width: 150px;">怪物等级</td>
		    <td>
		        <input type="text" name="monsterLevel" value = "<{$row.award.monsterLevel}>" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">掉落机率</td>
		    <td>
		        <input type="text" name="random" value = "<{$row.award.random}>" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">iconId</td>
		    <td>
		        <input type="text" name="iconId" value = "<{$row.award.iconId}>" class="item_id" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">掉落物品</td>
		    <td>
		        <input type="text" name="dropItemId" value = "<{$row.award.dropItemId}>" class="item_id" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">怪物ID</td>
		    <td>
		        <input type="text" name="monsterId" value = "<{$row.award.monsterId}>" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td style="width: 150px;">地图范围</td>
		    <td>
		        <input type="text" name="mapRange" value = "<{$row.award.mapRange}>" style="width: 200px;">
		    </td>
		</tr>
        

		<tr>
		    <td style="width: 150px;">活动开启等级</td>
		    <td>
		        <input type="text" name="openLvl" value = "<{$row.openLvl}>" style="width: 200px;">
		    </td>
		</tr>

		<tr>
		    <td>奖励配置</td>
		    <td>
		        <table style="width:100%;" class="table-activity-reward">
		            <thead>
		            <tr>
		                <th>奖励</th>
		            </tr>
		            </thead>
		            <tbody class="body-item">
		            <tr>
		                <td>
		                    <table>
		                    <tbody>
		                    <tr>
		                        <td>
		                            <table class="itemTable">
		                            <thead>
		                                <tr>
		                                    <td>兑换消耗的物品个数</td>
		                                    <td>兑换得到的物品</td>
		                                    <td>得到的物品个数</td>
		                                    <td>可兑换次数</td>
		                                    <td>操作</td>
		                                </tr>
		                            </thead>
		                            
		                            <tbody>
		                            <{foreach from=$row.itemId key=k item=v}>
		                            	<tr class="itemWidget">
											<td>
												<input type="text" name="item_need_count[]" class="item_id" value="<{$row.award.item_need_count[$k]}>"  >
											</td>
											<td>
												<input style="width:200px" type="text" name="item_id[]" class="item_id"  value="<{$v}>">
											</td>
											<td>
												<input type="text" name="item_count[]" value="<{$row.itemCount[$k]}>" class="item_count">
											</td>
											<td>
												<input type="text" name="item_exchange[]" value="<{$row.award.item_exchange[$k]}>" class="item_exchange">
											</td>
											<td>
												<input type="button" value="删除" class="gbutton delitem" title="删除该行">
											</td>
										</tr>
		                            <{/foreach}>
		                            </tbody>
		                            </table>
		                        </td>
		                    </tr>
		                    </tbody>
		                    </table>
		                    <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
		                </td>
		            </tr>
		            </tbody>
		        </table>
		    </td>
		</tr>
        
        <tr>
          <td style="width:150px;">活动描述</td>
          <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
        </tr>
		
		<tr>
			<td style="width:150px">活动开关</td>
			<td>
				<select name="state">
					<option value="0" <{if $row.state == 0 }> selected <{/if}>>开</option>
					<option value="1" <{if $row.state == 1 }> selected <{/if}>>关</option>
				</select>
			</td>
		</tr>

		<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/channelGroup_server_edit.tpl' }>
        <!-- 服务器，  -->

        <tr>
          <td colspan="2">
          	<!-- 活动ID -->
			<input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
			<input type="hidden" name="act_id"  value="<{$row.act_id}>" >
			<input type="hidden" name="id" value="<{$row.id}>">
			<!--活动ID  -->

			<input type="submit" class="gbutton" value="保存" id="charge">
            <input type="hidden" id="error" value="1">
 		</td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
