<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }
    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }
    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    #checkboxServerList .select {
        background-color: #dcd8d8;
    }
    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    .cur {
        background-color: beige;
    }


    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }
    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1032.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>" >翻牌集福</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>"></td>
            </tr>
            </tbody>

            <tr>
                <td style="width:150px;">开启等级</td>
                <td><input type="text" name="openLvl" value="1" style="width:200px;"></td>
            </tr>
        </table>

        <table class="itable itable-color">

            <tbody id ="load">
            <tr>
                <td style="width:auto;">新服(多少天作为新服)<input style="margin-left: 43px" type="text" name="day" value="<{$row.award.day}>"> <span style="color: #FF0000">*支持配置</span></td>
            </tr>
            <tr>
                <td>一张牌元宝数量 <input style="margin-left: 75px" type="number" name="gold" value="<{$row.award.gold}>"> <span style="color: #FF0000">*支持配置</span></td>
            </tr>
            <tr>
                <td>牌总数量<input style="margin-left: 115px;background-color: #dddddd" readonly="readonly" type="number" name="totalCount" value="10"></td>
            </tr>
            <tr>
                <td>中福概率<input style="margin-left: 116px;background-color: #dddddd" readonly="readonly" type="number" name="per" value="0.1"></td>
            </tr>
            <tr>
                <td>中福规则<input type="text" readonly="readonly" value="[[1,1,3],[2,2,9],[3,7,10]]" style="width: 300px;margin-left: 116px;background-color: #dddddd"/></td>
            </tr>
            <tr>
                <td>
                    <input style="width: 800px;background-color: #dddddd" readonly="readonly" type="text" value='["点击翻牌集福得超级大奖！","还差一点点，就能获得一福大奖","再来一个福即可获得二福大奖","还差最后一个福即可获得三福超级大奖"]'/>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="add-roll-grad" roll_num="<{$row.award.list[1].topPrize|@count}>" add-roll-num="<{$row.award.list[1].topPrize|@count}>"><!--标识轮-->
                    <table class="new-ser-msg" style="padding: 0">
                        <tbody>
                        <{foreach from=$row.award.list[1].topPrize key=k item=r name=foo}>
                        <tr class="big-price">
                            <td style="width:150px; text-align: center;">
                                <label class="title-name">新服奖励</label>
                                <input type="hidden" value="1" class="roll-hidden"/>
                                <!--$smarty.foreach.foo.iteration-->
                                <input type="text" value="第<{$smarty.foreach.foo.iteration}>轮奖励" class="roll-text"
                                       style="width: 120px;text-align: center"/>
                                <input type="button" value="复制此轮奖励" class="gbutton w-120 c-copy-level">
                                <input type="button" value="删除此轮奖励" class="gbutton w-120 c-del-level">
                            </td>
                            <td>
                                <table style="width:100%;" class="table-activity-reward">
                                    <tbody class="body-item">
                                    <tr>
                                        <td style="width:100px;">
                                            1福奖励
                                        </td>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr class="list-num" list_num="<{$r[1]|@count}>">
                                                    <td>
                                                        <table class="itemTable">
                                                            <thead>
                                                            <tr style="">
                                                                <td style="">物品ID</td>
                                                                <td>数量</td>
                                                                <td>角色</td>
                                                                <!--<td>新老服区分</td>-->
                                                                <td>操作</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <{foreach from=$r[1] key=kl item=msg name=footem}>
                                                            <tr>
                                                                <td><input style="width:200px" type="text" name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][1][<{$smarty.foreach.footem.iteration}>][itemId]" class="item_id" autocomplete="on" value="<{$msg.itemId}>"></td>
                                                                <td><input style="width:75px" type="number" name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][1][<{$smarty.foreach.footem.iteration}>][item_count]" value="<{$msg.item_count}>" class="item_count"></td>
                                                                <td>
                                                                    <select name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][1][<{$smarty.foreach.footem.iteration}>][career_name]" class="career_name">
                                                                        <option <{if $msg.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                        <option <{if $msg.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                        <option <{if $msg.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                    </select>
                                                                </td>
                                                                <!--<td><input style="width:75px" type="number"  name="list[1][topPrize][1][1][][cy_cut]"  value="1" class="cy_cut"/></td>-->
                                                                <!--<td><input style="width:75px" type="number" name="list[1][topPrize][1][1][][ser_day]" value="14" class="ser_day"/></td>-->
                                                                <td>
                                                                    <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                                    <input type="button" value="复制" class="gbutton copyitem" add_amount_num="<{$kl}>" title="复制">
                                                                </td>
                                                            </tr>
                                                            <{/foreach}>
                                                            </tbody>
                                                        </table>
                                                        <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                               add_amount_num="1">-->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            2福奖励
                                        </td>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr class="list-num" list_num="<{$r[2]|@count}>">
                                                    <td>
                                                        <table class="itemTable">
                                                            <tbody>
                                                            <{foreach from=$r[2] key=kl item=msg name=footem}>
                                                            <tr>
                                                                <td><input style="width:200px" type="text" name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][itemId]" value="<{$msg.itemId}>" class="item_id" autocomplete="on"></td>
                                                                <td><input style="width:75px" type="number" name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][item_count]" value="<{$msg.item_count | default:1}>" class="item_count"></td>
                                                                <td>
                                                                    <select name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][career_name]" class="career_name">
                                                                        <option <{if $msg.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                        <option <{if $msg.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                        <option <{if $msg.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                                    <input type="button" value="复制" class="gbutton copyitem" add_amount_num="<{$smarty.foreach.footem.iteration}>" title="复制">
                                                                </td>
                                                            </tr>
                                                            <{/foreach}>
                                                            </tbody>
                                                        </table>
                                                        <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                               add_amount_num="1">-->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            3福奖励
                                        </td>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr class="list-num" list_num="<{$r[3]|@count}>">
                                                    <td>
                                                        <table class="itemTable">
                                                            <tbody>
                                                            <{foreach from=$r[3] key=kl item=msg name=footem}>
                                                                <tr>
                                                                    <td><input style="width:200px" type="text" name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][3][<{$smarty.foreach.footem.iteration}>][itemId]" value="<{$msg.itemId}>" class="item_id" autocomplete="on"></td>
                                                                    <td><input style="width:75px" type="number" name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][3][<{$smarty.foreach.footem.iteration}>][item_count]" value="<{$msg.item_count | default:1}>" class="item_count"></td>
                                                                    <td>
                                                                        <select name="list[1][topPrize][<{$smarty.foreach.foo.iteration}>][3][<{$smarty.foreach.footem.iteration}>][career_name]" class="career_name">
                                                                            <option <{if $msg.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                            <option <{if $msg.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                            <option <{if $msg.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                                        <input type="button" value="复制" class="gbutton copyitem" add_amount_num="<{$smarty.foreach.footem.iteration}>" title="复制">
                                                                    </td>
                                                                </tr>
                                                            <{/foreach}>
                                                            </tbody>
                                                        </table>
                                                        <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                               add_amount_num="1">-->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <{/foreach}>
                        <tr class="normal-price">
                            <td style="text-align: center">
                                普通奖励
                            </td>
                            <td class="add-normal-grad" add-normal-num="<{if $row.ward.list[1].normal|@count eq 0}>1<{else}><{$row.ward.list[1].normal|@count}><{/if}>">
                                <table>
                                    <tbody>
                                    <{if $row.award.list[1].normal|@count neq 0}>
                                    <{foreach from=$row.award.list[1].normal key=nk item=msg}>
                                    <tr>
                                        <td><!--标识-->
                                            <table class="itemTable">
                                                <thead>
                                                <tr>
                                                    <td>物品ID</td>
                                                    <td>数量</td>
                                                    <td>权重</td>
                                                    <td>角色</td>
                                                    <td>操作</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <{foreach from=$msg key=kt item=item}>
                                                <tr class="itemWidget">
                                                    <td><input style="width:200px" type="text" name="list[1][normal][<{$nk}>][<{$kt}>][itemId]" value="<{$item.itemId}>" class="item_id" autocomplete="on"></td>
                                                    <td><input  type="text" name="list[1][normal][<{$nk}>][<{$kt}>][count]" value="<{$item.count}>" class="item_count" ></td>
                                                    <td><input  type="text" name="list[1][normal][<{$nk}>][<{$kt}>][weight]" value="<{$item.weight}>" class="weight"></td>
                                                    <td>
                                                        <select name="list[1][normal][<{$nk}>][<{$kt}>][career_name]" class="career_name">
                                                            <option <{if $item.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                            <option <{if $item.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                            <option <{if $item.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="gbutton delitem" title="删除该行">删除</button>
                                                    </td>
                                                </tr>
                                                <{/foreach}>
                                                <tr class="add_item_btn">
                                                    <td><input type="button" value="添加物品条件" class="gbutton c-addItem" list-type="1" add_amount_num="<{$nk}>" add_list_num=<{math equation="y+x" y=$msg|@count x=1}> ></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <input type="button" onclick="return confirm('确定删除该组条件？')" class="gbutton del-normal" value="删除该条件" />
                                            <!--<input type="button" style="margin-top: 5px" class="gbutton copy-normal" value="复制该条件"/>-->
                                        </td>
                                    </tr>
                                    <{/foreach}>
                                    <{else}>
                                        <tr>
                                            <td><!--标识-->
                                                <table class="itemTable">
                                                    <thead>
                                                    <tr>
                                                        <td>物品ID</td>
                                                        <td>数量</td>
                                                        <td>权重</td>
                                                        <td>角色</td>
                                                        <td>操作</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <tr class="add_item_btn">
                                                        <td><input type="button" value="添加物品条件" class="gbutton c-addItem" list-type="1" add_amount_num="1" add_list_num=1></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <input type="button" onclick="return confirm('确定删除该组条件？')" class="gbutton del-normal" value="删除该条件" />
                                                <!--<input type="button" style="margin-top: 5px" class="gbutton copy-normal" value="复制该条件"/>-->
                                            </td>
                                        </tr>
                                    <{/if}>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="background-color: #f0f0f0">
                <td colspan="2">
                    <input type="button" value="复制作为老服奖励" onclick="return confirm('确定复制新服奖励？')" class="gbutton w-120 c-copy-asOld"/>
                    <input type="button" value="删除老服奖励" onclick="return confirm('确定删除老服奖励？')" class="gbutton w-120 c-del-new"/>
                    <!--<input type="button" value="添加奖励" onclick="return confirm('确定添加集福奖励？')" class="gbutton w-120 add-reward"/>-->
                </td>
            </tr>
            <!--老服务器-->
            <{if $row.award.list[2]|@is_null neq 1}>
                <tr>
                    <td colspan="2" class="add-roll-grad" roll_num="<{$row.award.list[2].topPrize|@count}>" add-roll-num="<{$row.award.list[2].topPrize|@count}>"><!--标识轮-->
                        <table class="new-ser-msg" style="padding: 0">
                            <tbody>
                            <{foreach from=$row.award.list[2].topPrize key=k item=r name=foo}>
                                <tr class="big-price">
                                    <td style="width:150px; text-align: center;">
                                        <label class="title-name">老服奖励</label>
                                        <input type="hidden" value="1" class="roll-hidden"/>
                                        <input type="text" value="第<{$smarty.foreach.foo.iteration}>轮奖励" class="roll-text"
                                               style="width: 120px;text-align: center"/>
                                        <input type="button" value="复制此轮奖励" class="gbutton w-120 c-copy-level">
                                        <input type="button" value="删除此轮奖励" class="gbutton w-120 c-del-level">
                                    </td>
                                    <td>
                                        <table style="width:100%;" class="table-activity-reward">
                                            <tbody class="body-item">
                                            <tr>
                                                <td style="width:100px;">
                                                    1福奖励
                                                </td>
                                                <td>
                                                    <table>
                                                        <tbody>
                                                        <tr class="list-num" list_num="<{$r[1]|@count}>">
                                                            <td>
                                                                <table class="itemTable">
                                                                    <thead>
                                                                    <tr style="">
                                                                        <td style="">物品ID</td>
                                                                        <td>数量</td>
                                                                        <td>角色</td>
                                                                        <!--<td>新老服区分</td>-->
                                                                        <td>操作</td>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <{foreach from=$r[1] key=kl item=msg name=footem}>
                                                                        <tr>
                                                                            <td><input style="width:200px" type="text" name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][1][<{$smarty.foreach.footem.iteration}>][itemId]" class="item_id" autocomplete="on" value="<{$msg.itemId}>"></td>
                                                                            <td><input style="width:75px" type="number" name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][1][<{$smarty.foreach.footem.iteration}>][item_count]" value="<{$msg.item_count}>" class="item_count"></td>
                                                                            <td>
                                                                                <select name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][1][<{$smarty.foreach.footem.iteration}>][career_name]" class="career_name">
                                                                                    <option <{if $msg.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                                    <option <{if $msg.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                                    <option <{if $msg.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                                </select>
                                                                            </td>
                                                                            <!--<td><input style="width:75px" type="number"  name="list[1][topPrize][1][1][][cy_cut]"  value="1" class="cy_cut"/></td>-->
                                                                            <!--<td><input style="width:75px" type="number" name="list[1][topPrize][1][1][][ser_day]" value="14" class="ser_day"/></td>-->
                                                                            <td>
                                                                                <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                                                <input type="button" value="复制" class="gbutton copyitem" add_amount_num="<{$kl}>" title="复制">
                                                                            </td>
                                                                        </tr>
                                                                        <{/foreach}>
                                                                    </tbody>
                                                                </table>
                                                                <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                                       add_amount_num="1">-->
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">
                                                    2福奖励
                                                </td>
                                                <td>
                                                    <table>
                                                        <tbody>
                                                        <tr class="list-num" list_num="<{$r[2]|@count}>">
                                                            <td>
                                                                <table class="itemTable">
                                                                    <tbody>
                                                                    <{foreach from=$r[2] key=kl item=msg name=footem}>
                                                                        <tr>
                                                                            <td><input style="width:200px" type="text" name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][itemId]" value="<{$msg.itemId}>" class="item_id" autocomplete="on"></td>
                                                                            <td><input style="width:75px" type="number" name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][item_count]" value="<{$msg.item_count | default:1}>" class="item_count"></td>
                                                                            <td>
                                                                                <select name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][career_name]" class="career_name">
                                                                                    <option <{if $msg.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                                    <option <{if $msg.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                                    <option <{if $msm.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                                                <input type="button" value="复制" class="gbutton copyitem" add_amount_num="<{$kl}>" title="复制">
                                                                            </td>
                                                                        </tr>
                                                                        <{/foreach}>
                                                                    </tbody>
                                                                </table>
                                                                <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                                       add_amount_num="1">-->
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">
                                                    3福奖励
                                                </td>
                                                <td>
                                                    <table>
                                                        <tbody>
                                                        <tr class="list-num" list_num="<{$r[3]|@count}>">
                                                            <td>
                                                                <table class="itemTable">
                                                                    <tbody>
                                                                    <{foreach from=$r[3] key=kl item=msg name=footem}>
                                                                        <tr>
                                                                            <td><input style="width:200px" type="text" name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][3][<{$smarty.foreach.footem.iteration}>][itemId]" value="<{$msg.itemId}>" class="item_id" autocomplete="on"></td>
                                                                            <td><input style="width:75px" type="number" name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][3][<{$smarty.foreach.footem.iteration}>][item_count]" value="<{$msg.item_count | default:1}>" class="item_count"></td>
                                                                            <td>
                                                                                <select name="list[2][topPrize][<{$smarty.foreach.foo.iteration}>][2][<{$smarty.foreach.footem.iteration}>][career_name]" class="career_name">
                                                                                    <option <{if $msg.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                                    <option <{if $msg.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                                    <option <{if $msm.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" value="删除" class="gbutton delitem" title="删除">
                                                                                <input type="button" value="复制" class="gbutton copyitem" add_amount_num="<{$kl}>" title="复制">
                                                                            </td>
                                                                        </tr>
                                                                        <{/foreach}>
                                                                    </tbody>
                                                                </table>
                                                                <!--<input type="button" value="添加物品条件" class="gbutton n-additem"
                                                                       add_amount_num="1">-->
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <{/foreach}>
                            <tr class="normal-price">
                                <td style="text-align: center">
                                    普通奖励
                                </td>
                                <td class="add-normal-grad" add-normal-num="<{if $row.ward.list[2].normal|@count eq 0}>1<{else}><{$row.ward.list[2].normal|@count}><{/if}>">
                                    <table>
                                        <tbody>
                                        <{if $row.award.list[2].normal|@count neq 0}>
                                        <{foreach from=$row.award.list[2].normal key=nk item=msg}>
                                            <tr>
                                                <td><!--标识-->
                                                    <table class="itemTable">
                                                        <thead>
                                                        <tr>
                                                            <td>物品ID</td>
                                                            <td>数量</td>
                                                            <td>权重</td>
                                                            <td>角色</td>
                                                            <td>操作</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <{foreach from=$msg key=kt item=item}>
                                                            <tr class="itemWidget">
                                                                <td><input style="width:200px" type="text" name="list[2][normal][<{$nk}>][<{$kt}>][itemId]" value="<{$item.itemId}>" class="item_id" autocomplete="on"></td>
                                                                <td><input  type="text" name="list[2][normal][<{$nk}>][<{$kt}>][count]" value="<{$item.count}>" class="item_count" ></td>
                                                                <td><input  type="text" name="list[2][normal][<{$nk}>][<{$kt}>][weight]" value="<{$item.weight}>" class="weight"></td>
                                                                <td>
                                                                    <select name="list[2][normal][<{$nk}>][<{$kt}>][career_name]" class="career_name">
                                                                        <option <{if $item.career_name eq 0}>selected<{/if}> value="0">通用</option>
                                                                        <option <{if $item.career_name eq 1}>selected<{/if}> value="1">刀剑师</option>
                                                                        <option <{if $item.career_name eq 2}>selected<{/if}> value="2">羽翎师</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="gbutton delitem" title="删除该行">删除</button>
                                                                </td>
                                                            </tr>
                                                            <{/foreach}>
                                                        <tr class="add_item_btn">
                                                            <td><input type="button" value="添加物品条件" class="gbutton c-addItem" list-type="2" add_amount_num="<{$nk}>" add_list_num=<{math equation="y+x" y=$msg|@count x=1}> ></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <input type="button" onclick="return confirm('确定删除该组条件？')" class="gbutton del-normal" value="删除该条件" />
                                                    <!--<input type="button" style="margin-top: 5px" class="gbutton copy-normal" value="复制该条件"/>-->
                                                </td>
                                            </tr>
                                            <{/foreach}>
                                        <{else}>
                                            <tr>
                                                <td><!--标识-->
                                                    <table class="itemTable">
                                                        <thead>
                                                        <tr>
                                                            <td>物品ID</td>
                                                            <td>数量</td>
                                                            <td>权重</td>
                                                            <td>角色</td>
                                                            <td>操作</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <tr class="add_item_btn">
                                                            <td><input type="button" value="添加物品条件" class="gbutton c-addItem" list-type="2" add_amount_num="1" add_list_num=1></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <input type="button" onclick="return confirm('确定删除该组条件？')" class="gbutton del-normal" value="删除该条件" />
                                                    <!--<input type="button" style="margin-top: 5px" class="gbutton copy-normal" value="复制该条件"/>-->
                                                </td>
                                            </tr>
                                        <{/if}>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <{/if}>
            </tbody>
        </table>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>

            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->

            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
