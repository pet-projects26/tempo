<script type="text/javascript" src="templates/activity/activity.js"></script>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }
    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }
    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    #checkboxServerList .select {
        background-color: #dcd8d8;
    }
    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }
    .cur {
        background-color: beige;
    }


    .itable-color tbody > tr:nth-child(odd) {
        background: #f1f1b0;
    }
    .del-level {
        margin: 5px 0px;
    }

    .itable tr:nth-child(even) {
        background-color: #f0f0f0;
    }

    .w-120 {
        width: 120px;
        float: left;
        margin: 5px 14px;
    }
</style>
<script type="text/javascript" src="templates/activity/activity_1033.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
        <div class="type-list">
            <ul>
                <li class="aType cur" data-type="<{$row.act_id}>" >扭蛋</li>
            </ul>
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动名称</td>
                <td><input type="text" name="title" id="title" style="width:200px;" value="<{$row.title}>"></td>
            </tr>

            <tr id="add_time">
                <td style="width:150px;">活动时间</td>
                <td><input type="text" name="start_time" id="start_time" style="width:200px;" value="<{$row.start_time}>">
                    ~
                    <input type="text" name="end_time" id="end_time" style="width:200px;" value="<{$row.end_time}>"></td>
            </tr>
            </tbody>

            <tr>
                <td style="width:150px;">开启等级</td>
                <td><input type="text" name="openLvl" value="<{$row.openLvl}>" style="width:200px;"></td>
            </tr>
            <tr>
                <td>充值元宝</td>
                <td>
                    <input type="number" value="<{$row.charge_gold}>" name="charge_gold">
                </td>
            </tr>
            <tr>
                <td>扭蛋条目上限</td>
                <td>
                    <input type="number" readonly="readonly" value="50" style="background-color: #dddddd">
                </td>
            </tr>
        </table>

        <table class="itable itable-color">

            <tbody id ="load">
            <{foreach from=$row.grade key=k item=v}>
                <tr>
                    <td style="width:150px">
                        <label>世界等级</label>

                        <input type="text" name="grade[<{$k}>]" value="<{$v}>" class="w-120 grade">

                        <input type="button" value="添加累计奖励" class="gbutton c-add-reward w-120" add_grade_num="<{$k}>" add_amount_num="<{getLastKey arr=$row.amount[$k]}>">
                        <input type="button" value="删除该世界等级" class="gbutton del-level w-120">
                        <input type="button" value="复制该世界等级" class="gbutton w-120 c-copy-level">
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="width: 15%">配置奖励</td>
                                <td>
                                    <!-- 配置奖励  -->
                                    <table style="width:100%;" class="table-activity-reward">

                                        <tbody class="body-item">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table class="itemTable">
                                                                <thead>
                                                                <tr>
                                                                    <td>物品ID</td>
                                                                    <td>数量</td>
                                                                    <td>权重</td>
                                                                    <td>角色</td>
                                                                    <td>操作</td>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <{foreach from=$row.config.item_id[$k] key=k2 item=v2}>
                                                                    <tr class="itemWidget">
                                                                        <td>
                                                                            <input style="width:200px" type="text" name="config[item_id][<{$k}>][]" class="config_item_id ui-autocomplete-input" value="<{$v2}>">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="config[item_count][<{$k}>][]" value="<{$row.config.item_count[$k][$k2]}>" class="config_item_count">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="config[weight][<{$k}>][]" value="<{$row.config.weight[$k][$k2]}>" class="config_item_weight">
                                                                        </td>
                                                                        <td>
                                                                            <select name="config[career][<{$k}>][]" class="config_item_career">
                                                                                <option value="0" <{if $row.config.career[$k][$k2] == 0}> selected <{/if}> >通用</option>
                                                                                <option value="1" <{if $row.config.career[$k][$k2] == 1}> selected <{/if}> >刀</option>
                                                                                <option value="2" <{if $row.config.career[$k][$k2] == 2}> selected <{/if}> >弓</option>
                                                                                <option value="3" <{if $row.config.career[$k][$k2] == 3}> selected <{/if}> >法</option>
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <input type="button" value="删除" class="gbutton delitem" title="删除该行">
                                                                        </td>
                                                                    </tr>
                                                                    <{/foreach}>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="<{$k}>">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- 配置奖励 -->
                                </td>
                            </tr>

                            <tr class="tr-extra">
                                <td>
                                    累计奖励<br/>
                                    个人扭蛋<input type="text" readonly="readonly" style="text-align: center;color: #ca1b36;font-weight: 600;width: 30px;margin: 0 2px" value="1">次可领取<br/>
                                    <em style="font-size: 8px;color: #ca1b36">*所有世界等级累计奖励条件数目一致</em>
                                </td>
                                <td>
                                    <!-- 配置奖励  -->
                                    <table style="width:100%;" class="table-activity-reward">
                                        <thead>
                                        <tr>
                                            <th>条件</th>
                                            <th>奖励</th>
                                            <th>删除</th>
                                        </tr>
                                        </thead>
                                        <tbody class="body-item">
                                        <{foreach from=$row.amount[$k] key=k2 item=v2 }>
                                            <tr>
                                                <td>
                                                    <span>全服累计次数</span><br/>
                                                    <em style="color: #ca1b36;font-size: 8px">*所有世界等级该组配置一致</em>
                                                    <br/>
                                                    <input type="number"  class="amount" name="amount[<{$k}>][<{$k2}>]" value="<{$v2}>"/><br/>
                                                    <span>VIP等级</span><br/>
                                                    <em style="color: #ca1b36;font-size: 8px">*所有世界等级该组配置一致</em>
                                                    <br/>
                                                    <input type="number"  class="vip_level" name="vip_level[<{$k}>][<{$k2}>]" value="<{$row.vip_level[$k][$k2]}>" />
                                                </td>
                                                <td>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <table class="itemTable">
                                                                    <thead>
                                                                    <tr>
                                                                        <td width="">物品ID</td>
                                                                        <td width="">数量</td>
                                                                        <td width="">职业</td>
                                                                        <td width="">操作</td>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <{foreach from=$row.itemId[$k][$k2] key=k3 item=v3 }>
                                                                        <tr class="itemWidget">
                                                                            <td>
                                                                                <input style="width:200px" type="text" name="item_id[<{$k}>][<{$k2}>][]" class="item_id ui-autocomplete-input"  value="<{$v3}>" >
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" name="item_count[<{$k}>][<{$k2}>][]" value="<{$row.itemCount[$k][$k2][$k3]}>" class="item_count">
                                                                            </td>
                                                                            <td>
                                                                                <select name="career[<{$k}>][<{$k2}>][]">
                                                                                    <option value="0" <{if $row.career[$k][$k2][$k3] == 0 }> selected <{/if}> >通用</option>
                                                                                    <option value="1" <{if $row.career[$k][$k2][$k3] == 1 }> selected <{/if}> >刀</option>
                                                                                    <option value="2" <{if $row.career[$k][$k2][$k3] == 2 }> selected <{/if}> >弓</option>
                                                                                    <option value="3" <{if $row.career[$k][$k2][$k3] == 3 }> selected <{/if}> >法</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" value="删除" class="gbutton delitem" title="删除该行">
                                                                            </td>
                                                                        </tr>
                                                                        <{/foreach}>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <input type="button" value="添加物品条件" class="gbutton c-extra-additem" add_amount_num="<{$k2}>" add_grade_num="<{$k}>">
                                                </td>
                                                <td>
                                                    <input type="button" value="删除该条件" class="gbutton del-reward"><input type="button" value="复制该条件" class="gbutton c-copy-reward">
                                                </td>
                                            </tr>
                                            <{/foreach}>

                                        </tbody>
                                    </table>
                                    <!-- 配置奖励 -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <{/foreach}>
            <!-- 标记 -->
            <tr style="display: none;">
                <td colspan="2">
                    <input type="button" value="标记" id="add_grade_num" add_grade_num="<{getMaxKey arr=$row.grade}>" >
                </td>
            </tr>
            <!-- 标记 -->
            </tbody>
        </table>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">活动描述</td>
                <td><textarea name="content" id="content" style="width:600px;height:200px;margin:0;"><{$row.content}></textarea></td>
            </tr>

            <tr>
                <td style="width:150px">活动开关</td>
                <td>
                    <select name="state">
                        <option value="0">开</option>
                        <option value="1">关</option>
                    </select>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server_edit.tpl' }>
            <!-- 服务器，  -->

            <tr>
                <td colspan="2">
                    <!-- 活动ID -->
                    <input type="hidden" name="big_act_id"  value="<{$big_act_id}>">
                    <input type="hidden" class="act_id" name="act_id" value="<{$row.act_id}>">
                    <input type="hidden" class="id" name="id" value="<{$row.id}>">
                    <!--活动ID  -->
                    <input type="submit" class="gbutton" value="保存" id="charge">
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
