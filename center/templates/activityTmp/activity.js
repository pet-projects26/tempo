// JavaScript Document

    $(function(){
		 $('#checkboxChannelList li').click(function(){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
            getChannelSelected();
        });
        //全选
        $('#channelSelectAll').click(function(){
            if($(this).attr('set') == '0'){
                $('#checkboxChannelList li').each(function(){
                    if(!$(this).hasClass('select')){
                        $(this).addClass('select');
                    }
                });
                $(this).html('全不选');
                $(this).attr('set' , 1);
            }
            else{
                $('#checkboxChannelList li').each(function(){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                });
                $(this).html('全选');
                $(this).attr('set' , 0);
            }
            getChannelSelected();
        });
		
		
		
		
		 $('#checkboxServerList li').click(function(){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
            getServerSelected();
        });
        //全选
        $('#serverSelectAll').click(function(){
            if($(this).attr('set') == '0'){
                $('#checkboxServerList li').each(function(){
                    if(!$(this).hasClass('select')){
                        $(this).addClass('select');
                    }
                });
                $(this).html('全不选');
                $(this).attr('set' , 1);
            }
            else{
                $('#checkboxServerList li').each(function(){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                });
                $(this).html('全选');
                $(this).attr('set' , 0);
            }
            getServerSelected();
        });
		
		var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
		 $('#start_time').datetimepicker(timepickerlang);
		 $('#end_time').datetimepicker(timepickerlang);
   
		
		$('.aType').click(function(){
			$('.type-list ul li').removeClass('cur');
            $(this).addClass('cur');
           	$(this).attr("checked","checked");
		});
		
        /**
         * [添加充值档次]
         * @type {String}
         */
		
		
		
		//添加档次
		$("#addgrade").click(function(){
			var num = $(this).attr('num');
			num = parseInt(num);
			num = num + 1;
			
			var grade_name = "grade["+ num + "]";
			
			var amount_name = "amount[" + num +"][0]";
			
				var addgrade = '<tr grade_num=' + num +'> ' +
						'<td style="width:150px;">' +
							'<label>充值档次 </label> ' +
							'<input type="text" id="grade" name="' + name +'">' + 
							'<input type="button" value="添加条件" class="gbutton c-add-reward" add_grade_num='+ num +'  add_amount_num="0" > ' +
							'<input type="button" value="删除该档次" class="gbutton del-level" >' + 
						'</td> ' + 
						'<td> '  +
							'<table style="width:100%;" class= "table-activity-reward"> ' +
								'<thead>' +
									'<tr>' +
										'<th style="width:80px;">条件</th>' + 
										'<th>条件</th>' + 
										'<th>删除</th>' + 
									'</tr> ' + 
								'</thead>'   +
								
								'<tbody class="body-item"> ' + 
									'<tr>' +
										
										'<td style="width:100px;">' +
											'<input type="text" name="'+ amount_name +'" value="" /> ' +
										'</td>' +
										
										'<td>' + 
											'<table> ' + 
												'<tbody>' +
													'<tr>' +
														'<td>' +
															'<table class="itemTable">' + 
																'<thead>' +
																	'<tr>' + 
																		'<td width="">物品ID</td>' +
																		'<td width="">数量</td>' +
																		'<td width="">角色</td>'  +
																		'<td width="">操作</td>' +
																	'</tr>' +
																'</thead>' + 
																'<tbody> </tbody> ' + 
															'</table>' +
														'</td>' + 
														'<td></td>' + 
													'</tr>' +
												'</tbody>' +
											'</table>' +
											'<input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num='+ num +' >' + 
										'</td>' + 
										
										'<td>' +
											'<input type="button" value="删除该条件" class="gbutton delitem" >' +
										'</td>' +
									'</tr> ' + 
								'</tbody> ' + 
							'</table>' + 
						'</td>' + 
					'</tr>';		
			$(this).parent('td').parent('tr').before(addgrade);
			$(this).attr('num', num);
		});
		
		
		
		//添加充值条件
		$('.c-add-reward').die('click');
		$('.c-add-reward').live('click', function(){
			var grade_num  = $(this).attr('add_grade_num');
			grade_num = parseInt(grade_num);
			
			var amount_num = $(this).attr('add_amount_num');
			amount_num = parseInt(amount_num);
			amount_num = amount_num + 1;
			
			var name = "amount["+ grade_num +"]["+ amount_num +"]";
			var  add = '<tr>' +
						'<td style="width:100px;"> ' + 
							'<input type="text" name="'+ name +'" value=""> ' +
						'</td>' +
						'<td>' + 
							'<table>' +
								'<tbody> ' +  
									'<tr>' + 
										'<td>' +
											'<table class="itemTable">' +
												'<thead>' +
													'<tr>' + 
														'<td width="">物品ID</td>' +
														'<td width="">数量</td>' +
														'<td width="">角色</td>' + 
														'<td width="">操作</td>' +
													'</tr>' + 
												'</thead>' +
												
												'<tbody></tbody>' +
												
											'</table>' + 
										'</td>' +  
									'</tr>' +
								'</tbody>' + 
							'</table>' + 
							'<input type="button" value="添加物品条件" class="gbutton c-additem" add_amount_num="'+ amount_num +'"  add_grade_num="'+ grade_num +'" > ' +
						'</td>' + 
						
						'<td>' + 
							'<input type="button" value="删除该条件" class="gbutton del-reward" />' + 
						'</td>'
						
					'</tr>';
					
			$(this).parent('td').next('td').children('.table-activity-reward').eq(0).append(add);
            $(this).attr('add_amount_num', amount_num);
        });
		

       
		
		//更先die，然后再重监听 
		$('.c-additem').die("click");
		$('.c-additem').live('click', function(){
			var add_grade_num = $(this).attr('add_grade_num');
			add_grade_num = parseInt(add_grade_num);
			
			var add_amount_num = $(this).attr('add_amount_num');
			add_amount_num = parseInt(add_amount_num);
			
			
			var item_id_name =  "item_id["+ add_grade_num +"]["+ add_amount_num +"][]";
			var item_count_name = "item_count[" + add_grade_num +"]["+ add_amount_num +"][]";
			var career_name = "career["+ add_grade_num +"]["+ add_amount_num +"][]";
			
			var itemWidget = '<tr class="itemWidget">' +
										'<td>' +
											'<input type="text" name="'+ item_id_name +'" class="item_id" autocomplete="on">'  + 
										'</td>' +
										
										'<td>' +
											'<input type="text" name="'+ item_count_name +'" value="1" class="item_count">' +
										'</td>' + 
										
										'<td>' + 
											'<select name="'+ career_name +'">' + 
												'<option value="0">通用</option>' + 
												'<option value="1">刀剑师</option>' + 
												'<option value="2">羽翎师</option>' +
											'<select>' + 
										'</td>' +
										
										'<td>' + 
												'<input type="button" value="删除" class="gbutton delitem" title="删除该行">' + 
										'</td>' +								
						'</tr>';
						
			//alert($(this).prev('table').length);
			$(this).prev('table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0).children('table').eq(0).children('tbody').append(itemWidget); 
		});
		
		//删除某个物品
		$('.delitem').die('click');
		$('.delitem').live('click', function() {
			$(this).parent().parent().remove();
		});
		
		//删除某个条件
	
		$('.del-reward').die('click');
		$('.del-reward').live('click', function() {
			$(this).parent('td').parent('tr').remove();
		});
		
		//删除该档充值
		$('.del-level').die('click');
		$('.del-level').live('click', function() {
			$(this).parent('td').parent('tr').remove();
		});
		
		
		//提交
		$('#manual-mail-form').submit(function(event){
			var aid = $('input[name="aid"]').val();
			if (aid == '') {
				alert('请输入活动类型');
				return false;
			}


			
			var title = $('input[name="title"]').val();
			if (title == '') {
				alert('请输入名称');
				return false;
			}
			
			var start_time = $('input[name="start_time"]').val();
			if (start_time == '') {
				alert('请填入开始时间');
				return false;
			}
			
			var end_time  = $('input[name="end_time"]').val();
			
			if(end_time == '') {
				alert('请输入结束时间');
				return false;
			} 

			var  post = $(this).serialize();

			$.ajax({
				url: 'admin.php?ctrl=activityTmp&act=check_data',
				type: 'POST',
				dataType: 'JSON',
				data: $(this).serialize()
			}).done(function(data){
				var code = data['code'];

				var msg  = data['msg'];

				var str = '';

				switch (code) {
					case 200: //直接提交
						addActData(post);
						break;
					case 202: //确认后提交
						$.each(msg, function(index, val) {
							str += val + ' \r\n';
						});

						if (confirm(str)) { 
							//表单提交
							addActData(post);
						}
						break;
					default:
						alert(msg);
						break;
				}
				
			});
			
			return false;
        });
      
    });
	
	/**
	 * 添加活动
	 * @param {[string]} post [urlencode的字符串]
	 */
	function addActData(post) {
		$.ajax({
			url: 'admin.php?ctrl=activityTmp&act=add_data',
			type: 'POST',
			dataType: 'JSON',
			data: post
		}).done(function(data){
			var code = data['code'];

			var msg  = data['msg'];

			var str = '';
			if (code  == 200) {
				$.each(msg, function(index, val) {
					str += val + ' \r\n';
				});
				alert(str);
				return false;
			} else {
				alert(msg);
				return false;
			}
			//$.dialog.tips(data.msg);
		});
	}

   
	
	//每次选择改变时获取已选择的值
    function getChannelSelected(){
        var channel = new Array();
        $('#checkboxChannelList li').each(function(){
            if($(this).hasClass('select') && $(this).hasClass('name')){
                channel.push($(this).attr('lid'));
            }
        });
        var value = JSON.stringify(channel);
        $('#checkboxChannel').val(value);
    }
	
	
	
	//每次选择改变时获取已选择的值
    function getServerSelected(){
        var server = new Array();
        $('#checkboxServerList li').each(function(){
            if($(this).hasClass('select') && $(this).hasClass('name')){
                server.push($(this).attr('lid'));
            }
        });
        var value = JSON.stringify(server);		
        $('#checkboxServer').val(value);
    }
	
	//检查条件
	function ckeckCondition(obj, tips) {
		var mark = true;
		
		var len = obj.length;
		if (obj.length == 0) {
			return true;
		}
		
		obj.each(function(index) {
			var val = $(this).val();
			
			var i = index + 1;
			
			if (val == '') {
				alert('请输入' + 1 +  '的' + tips );
				mark = false;
				return false;
			}
			
			if (isNaN(val)) {
				alert('第' + 1 +  '的'+ tips +'不是整数，请输入整数' );
				mark = false;
				return false;
			}
		});
		
		return mark;
	}
