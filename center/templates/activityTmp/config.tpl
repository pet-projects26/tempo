<script type="text/javascript" src="templates/activityTmp/activity.js"></script>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.cur {
	background-color: beige;
}


.itable-color tbody > tr:nth-child(odd) {
  background: #f1f1b0;
} 
.del-level {
	margin: 5px 0px;
}

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form" onsubmit="return false;">
    <table class="itable itable-color">
      <tbody>
      	<tr>
      		<td style="width:150px;">活动类型</td>
          	<td>
          		<input type="text" name="aid"  style="width: 200px">
          	</td>
      	</tr>

        <tr>
          <td style="width:150px;">活动名称</td>
          <td><input type="text" name="title" id="title" style="width:200px;"></td>
        </tr>

        <tr>
          <td style="width:150px;">开始等级</td>
          <td><input type="text" name="openLvl" id="openLvl" value="1" style="width:200px;"></td>
        </tr>
        
        <tr>
          <td style="width:150px;">活动时间</td>
          <td><input type="text" name="start_time" id="start_time" style="width:200px;">
            ~
            <input type="text" name="end_time" id="end_time" style="width:200px;"></td>
        </tr>
        
        
        <tr>
          <td style="width:150px;">内容【json格式】</td>
          <td><textarea name="content" id="content" style="width:800px;height:350px;margin:0;"></textarea></td>
        </tr>

        <tr>
        	<td style="width: 150px">json美化工具</td>
        	<td><a href="http://tool.oschina.net/codeformat/json" target="_blank">josn美化工具</a></td>
        </tr>
		
		<tr>
			<td style="width:150px">活动开关</td>
			<td>
				<select name="state">
					<option value="0">开</option>
					<option value="1">关</option>
				</select>
			</td>
		</tr>
        
        <tr>
          <td ><input type="button"  class="gbutton" value="区服" id="server" /></td>
          <td id="checkboxServerList"><ul>
              <li id="serverSelectAll" set="0">全选</li>
              <{foreach from=$server item=item}>
              <li lid="<{$item.server_id}>" class="name"><{$item.name}></li>
              <{/foreach}>
            </ul></td>
        </tr>

        <tr>
          <td colspan="2">
			<input type="submit" class="gbutton" value="保存" id="charge">
            <input type="hidden" id="error" value="1">
			
			<input type="hidden" name="server"  value="" id="checkboxServer" />
			<input type="hidden" name="channel" value="" id="checkboxChannel" />
 		</td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
