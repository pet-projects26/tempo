<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="update-activity-form">
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">选择服务器</td>
                    <td>
                        <select name="server" id="server">
                            <option value="0">未选择</option>
                            <{foreach from=$server item=item}>
                            <option value="<{$item.ip}>:<{$item.gm_port}>"><{$item.name}>（<{$item.ip}>:<{$item.gm_port}>）</option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="更新全部活动"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('table.itable tr td,table input,table select').css('margin-top','3px').css('margin-bottom','3px');
        $('#update-activity-form').submit(function(event){
            $.ajax({
                url: 'admin.php?ctrl=activity&act=update_activity_action',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize()
            }).done(function(data){
                $.dialog.tips(data.msg);
            });
            return false;
        });
    });
</script>