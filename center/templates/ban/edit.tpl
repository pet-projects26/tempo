<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="ban-edit-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
            </div>
            <tbody>
                <tr>
                    <{include file='../plugin/channelGroup_server_radios.tpl' }>
                </tr>
                <tr>
                    <td style="width:150px;">封禁类型</td>
                    <td>
                        <{foreach from=$bantype key=key item=item}>
                        <label><input type="radio" name="type" value="<{$key}>" <{if $key == 1}>checked="checked"<{/if}> typename="<{$item.name}>"> <{$item.title}></label>&nbsp;&nbsp;
                        <{/foreach}>
                    </td>
                </tr>
                <tr id="time">
                    <td style="width:150px;">封禁时间</td>
                    <td>
                        <{foreach from=$bantime key=key item=item}>
                        <label><input type="radio" name="time" value="<{$key}>" <{if $key == 1800}>checked="checked"<{/if}>><{$item}></checked></label>&nbsp;&nbsp;
                        <{/foreach}>
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;">封禁理由</td>
                    <td>
                        <select name="reason" id="reason">
                            <{foreach from=$reason key=key item=item}>
                            <option value="<{$key}>"><{$item}></option>
                            <{/foreach}>
                        </select>
                        <span>
                            <input type="text" name="other" id="other_reason" style="width:410px;display:none;">
                            <input type="button" value="存为模板" id="reason_tpl" class="gbutton" style="display:none;">
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;"><span class="typename"><{$bantype[1].name}></span><br>(用换行隔开每个<span class="typename"><{$bantype[1].name}></span>)</td>
                    <td><textarea name="content" style="width:600px;height:300px;margin:0;" id="typecontent"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="提交"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        //根据选择的封禁类型改变名称
        $('input[name="type"]').click(function(){
            $('.typename').html($(this).attr('typename'));
        });
        //封禁理由选择其它时触发
        $('#reason').change(function(){
            if($(this).val() == 0){
                $('#other_reason').css('display' , 'inline').focus();
                //$('#reason_tpl').css('display' , 'inline');
            }
            else{
                $('#other_reason').css('display' , 'none').val('');
                //$('#reason_tpl').css('display' , 'none');
            }
        });
        //把填入的其它封禁理由存为模板
        $('#reason_tpl').click(function(){
            if($('#other_reason').val() == ''){
                $.dialog.tips('模板不能为空');
            }
            else{
                /*
                $.ajax({
                    url: 'admin.php?ctrl=ban&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
                */
            }
        });
        var error = $('#error');
        $('#ban-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=ban&act=edit_action&save=1',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //服务器
        var server = $('#server').val();
        if(server == 0){
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //封禁内容
        var typecontent = $('#typecontent').val();
        if(typecontent == ''){
            var typename = $('.typename').html();
            $.dialog.tips('未填写' + typename);
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>