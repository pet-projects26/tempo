<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-add-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
            </div>
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 每个渠道的标识不能重复<br>
                        2. 渠道标识只能由数字，小写字母，大写字母，下划线（_）和横杠（-）组成<br>
                        3. 可以为该渠道选择权限用户组,一旦选择了权限用户组,不可剔除,就算这里剔除成功了,<权限管理>那里也不会更新的,若要变更该渠道不属于该用户组,只能到<权限管理>那里重新选择,提交。<br>
                    </td>
                </tr>
               
                <tr>
                    <td style="width:150px;">标识</td>
                    <td><input type="text" id="channel_id" name="channel_id" value=""></td>
                </tr>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" id="name" name="name" value=""></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏id</td>
                    <td><input type="number" id="game_id" name="game_id" value=""></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏名</td>
                    <td><input type="text" id="game_name" name="game_name" value=""></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏简称</td>
                    <td><input type="text" id="game_abbreviation" name="game_abbreviation" value=""></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏SecretKey</td>
                    <td><input type="text" id="game_secret_key" name="game_secret_key" value="" style="width: 400px"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏cdn资源链接</td>
                    <td><input type="text" id="cdn_url" name="cdn_url" style="width: 400px"/></td>
                </tr>
                <tr>
                    <td >选择权限用户组</td>
                    <td>
                        <select name="groupList[]" id="groupList" multiple="true" style="width: 400px">
                            <{foreach from=$groupList key=key item=item}>
                            <option value="<{$item.groupid}>"><{$item.name}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
    $('#groupList').select2();
        var error = $('#error');
        $('#channel-add-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=channel&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //渠道编号
        var num = $('#num').val();
        if(num == ''){
            $.dialog.tips('未填写渠道编号');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //渠道标识
        var channel_id = $('#channel_id').val();
        if(channel_id == ''){
            $.dialog.tips('未填写渠道标识');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //渠道名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写渠道名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //cdn链接
        var cdn_url = $('#cdn_url').val();
        if (cdn_url == '') {
            $.dialog.tips('未填写cdn资源加载链接');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
    }
</script>