<{if $channel_id}>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-edit-form">
        <div class="hidden">
            <input type="hidden" id="old_channel_id" name="old_channel_id" value="<{$channel_id}>">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 每个渠道的标识不能重复<br>
                        2. 渠道标识只能由数字，小写字母，大写字母，下划线（_）和横杠（-）组成<br>
                        3. 渠道编号只能由数字组成，并且不能大于255，数字越小越好，编号在添加之后不能再做修改<br>
                        4. 可以为该渠道选择权限用户组,一旦选择了权限用户组,不可剔除,就算这里剔除成功了,<权限管理>那里也不会更新的,若要变更该渠道不属于该用户组,只能到<权限管理>那里重新选择,提交。   
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;">标识</td>
                    <td><input type="text" id="channel_id" name="channel_id" value="<{$channel_id}>"></td>
                </tr>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" id="name" name="name" value="<{$name}>"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏id</td>
                    <td><input type="number" id="game_id" name="game_id" value="<{$game_id}>"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏名</td>
                    <td><input type="text" id="game_name" name="game_name" value="<{$game_name}>"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏简称</td>
                    <td><input type="text" id="game_abbreviation" name="game_abbreviation" value="<{$game_abbreviation}>"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏SecretKey</td>
                    <td><input type="text" id="game_secret_key" name="game_secret_key" value="<{$game_secret_key}>" style="width: 400px"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏cdn资源链接</td>
                    <td><input type="text" id="cdn_url" name="cdn_url" value="<{$cdn_url}>" style="width: 400px"/></td>
                </tr>
                <tr>
            		<td >选择权限用户组</td>
            		<td>
                		<select name="groupList[]" id="groupList" multiple="true" style="width: 400px">
                    		<{foreach from=$groupList key=key item=item}>
                    			<option value="<{$item.groupid}>" <{if  in_array($item.groupid,$groupuser) }> selected <{/if}>><{$item.name}></option>
                    		<{/foreach}>
                		</select>
            		</td>
        		</tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<{else}>
请选择编辑的渠道
<{/if}>
<script type="text/javascript">
    $(function(){
    $('#groupList').select2();
        var error = $('#error');
        $('#channel-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=channel&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('select' , 0);
                    }
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        var old_channel_id = '<{$channel_id}>';
        var old_name = '<{$name}>';
        var channel_id = $('#channel_id').val();
        var name = $('#name').val();
        
        var old_groupuser = '<{$groupuserstr}>';
        var new_groupuser = $('#groupList').val();

        var old_game_id = '<$game_id>';
        var old_game_name = '<$game_name>';
        var old_game_abbreviation = '<$game_abbreviation>';
        var old_game_secret_key = '<$game_secret_key>';

        var new_game_id = $('#game_id').val();
        var new_game_name = $('#game_name').val();
        var new_game_abbreviation = $('#game_abbreviation').val();
        var new_game_secret_key = $('#game_secret_key').val();

        if(old_channel_id == channel_id && old_name == name  && old_groupuser ==  new_groupuser && old_game_id == new_game_id && old_game_name == new_game_name && old_game_abbreviation == new_game_abbreviation && old_game_secret_key == new_game_secret_key){
            $.dialog.tips('没有任何改变');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //渠道标识
        if(channel_id == ''){
            $.dialog.tips('未填写渠道标识');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //渠道名称
        if(name == ''){
            $.dialog.tips('未填写渠道名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //cdn链接
        var cdn_url = $('#cdn_url').val();
        if (cdn_url == '') {
            $.dialog.tips('未填写cdn资源加载链接');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
    }
</script>