<style>
    label.checkType :hover {
        color: #0079A2;
    }

    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form id="my_form" method="post">
        <div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin: 10px">
            <table class="itable itable-color">
                <!-- 渠道组 Start -->
                <tr class='p-tr'>
                    <td style="width:150px">渠道组</td>
                    <td>
		                <span class="p-span c-all c-channel-group p-checked span-click">
			                <input class="p-input" type="checkbox" name="channel_group[]" value="" checked id="channel-group-all"/>
			                <label for="channel-group-all" class="p-text">所有渠道组</label>
		                </span>

                        <{foreach from = $groups key = id item = row}>
			            <span class="p-span c-channel-group  c-channel-group-list span-click">
				            <input class="p-input" type="checkbox" name="channel_group[]" value="<{$id}>" id="channel_group-<{$id}>"/>
				            <label for="channel_group-<{$id}>" class="p-input"><{$row}></label>
			            </span>
                        <{/foreach}>
                    </td>
                </tr>
                <!-- 渠道组 End -->
                <!--包 start-->
                <tr class="p-tr server">
                    <td style="width:150px;">游戏包</td>
                    <td>
                        <fieldset>
                            <legend> 当前渠道组下所有游戏包 </legend>
	    	                <span class="p-span p-checked s-server-click">
                                <input class="p-input"  type="checkbox" name="package_id[]"  value="" id="server-all" checked />
				                <label for="server-all" class="p-input" >当前所选渠道组下所有游戏包</label>
			                </span>
                            <font style="color:#f00; font-weight: bold;">选择该选项后，代表选中当前所选的渠道组下所有游戏包</font>
                        </fieldset>

                        <{foreach from = $packages key = group_id  item = rows }>
                        <fieldset id="<{$group_id}>">
                            <legend> <{$groups[$group_id]}> </legend>
                            <{foreach from = $rows  key = package_id  item = name}>
		    	                <span class="p-span s-server-click" >
					                <input class="p-input"  type="checkbox" name="package_id[]"  value="<{$package_id}>" id="server-<{$package_id}>" />
					                <label for="server-<{$package_id}>" class="p-input" ><{$name.game_name}>--<{$name.package_id}></label>
				                </span>
                            <{/foreach}>
                        </fieldset>
                        <{/foreach}>
                    </td>
                </tr>
                <!--包 end-->
                <tr class="review-ban">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                            <{foreach from=$rv_param key=type_id item=param}>
                                <tr>
                                    <td width="10%"><{$rv_name[$type_id]}></td>
                                    <td>
                                        <select name="param[<{$type_id}>]">
                                            <option value="">默认</option>
                                            <{foreach from=$param key=id item=type_name}>
                                            <option value="<{$id}>"><{$type_name}></option>
                                            <{/foreach}>
                                        </select>
                                    </td>
                                </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-left: 10px">
            <label for="version_r">版本号：</label>
            <input id="version_r" name="review_num" class="version_r" type="text" value="<{$version}>" placeholder="请输入版本号"/>
        </div>
        <div style="clear: both;"></div>

        <button style="margin-top: 8px;margin-left: 12px" type="button" class="gbutton edit-btn">提交</button>
    </form>
</div>

<script>
    $(function () {
        //选择
        $().ready(function() {
            //所有渠道
            $('.c-all').unbind('click').bind('click', function() {
                var checked = $(this).find('input').attr('checked');

                var obj = $(this).find('input');

                var mark = obj.prop('checked');

                if (mark) {
                    $('.c-channel-group-list input').prop('checked', 0);
                    $('.c-channel-group-list input').parent('span').removeClass('p-checked');

                    $(this).addClass('p-checked');

                } else {
                    $(this).removeClass('p-checked');
                }

                $('.server fieldset').css('display' , 'block');
                $('.server fieldset').removeClass('show' );
                //自动选中全部游戏包
                $('.server input[value !=""]').prop('checked', 0);
                $('.server  input[value !=""]').parent('span').removeClass('p-checked');
                $('.server input[value=""]').prop('checked', 1);
                $('.server input[value=""]').parent('span').addClass('p-checked');

            });

            //其他渠道
            $('.c-channel-group-list input').unbind('click').bind('click', function() {
                var mark = $(this).prop('checked');
                if (mark) {
                    $(this).parent('span').addClass('p-checked');
                } else {
                    $(this).parent('span').removeClass('p-checked');
                }

                var allLen = $('.c-channel-group input').length;

                var checkLen = $('.c-channel-group-list input:checked').length;

                //如果都没有选中，则自动选中全选
                if (checkLen == 0) {
                    $('.c-channel-group input[value=""]').prop('checked', 1);
                    $('.c-channel-group input[value=""]').parent('span').addClass('p-checked');

                    //自动选中全部服务器
                    $('.server input[value !=""]').prop('checked', 0);
                    $('.server input[value !=""]').parent('span').removeClass('p-checked');
                    $('.server input[value=""]').prop('checked', 1);
                    $('.server input[value=""]').parent('span').addClass('p-checked');

                } else {
                    $('.c-channel-group input[value=""]').prop('checked', 0);
                    $('.c-channel-group input[value=""]').parent('span').removeClass('p-checked');

                }

                //根据渠道组筛选服务器
                InputValue=$(this).val();

                if($('#'+InputValue).hasClass('show')){

                    $('#'+InputValue).removeClass('show');
                } else{

                    $('#'+InputValue).addClass('show');
                }

                $('.server fieldset').each(function(){  //遍历child,如果有show 就让他显示，没有就隐藏
                    $('.server fieldset').eq(0).show();
                    if($(this).hasClass('show')){
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                })
                //如果没选中，则全部显示
                if(checkLen == 0){
                    $('.server fieldset ').show();
                }

            });


            //当前渠道的所有服
            $('.s-server-click input[value=""]').unbind('click').bind('click', function() {
                var mark = $(this).prop('checked');

                if (mark) {
                    $('.s-server-click  input[value != ""]').prop('checked', 0);
                    $('.s-server-click  input[value != ""]').parent('span').removeClass('p-checked');

                    $(this).parent('span').addClass('p-checked');
                } else {
                    $(this).parent('span').removeClass('p-checked');
                }
            });

            // 非全服的其他服
            $('.s-server-click  input[value != ""]').unbind('click').bind('click', function() {
                var obj = '.s-server-click';
                var mark = $(this).prop('checked');
                if (mark) {

                    $(this).parent('span').addClass('p-checked');
                } else {
                    $(this).parent('span').removeClass('p-checked');
                }

                var allLen = $(obj + ' input').length;

                var checkLen = $(obj + ' input[value != ""]:checked').length;

                //如果都没有选中，则自动选中全选
                if (checkLen == 0) {
                    $(obj + ' input[value=""]').prop('checked', 1);
                    $(obj + ' input[value=""]').parent('span').addClass('p-checked');
                } else {
                    $(obj + ' input[value=""]').prop('checked', 0);
                    $(obj + ' input[value=""]').parent('span').removeClass('p-checked');
                }

            });
        });

        $(".edit-btn").bind('click', function () {
            var channel_group = $("input[name='channel_review']:checked").val();
            var version = $(".version_r").val();
            if(confirm('确认批量编辑提审版本号？')) {
                $.ajax({
                    type: 'post',
                    url: 'admin.php?&ctrl=Review&act=add_handel',
                    data: $("#my_form").serialize(),
                    beforeSend: function () {
                        $("input.edit-btn").attr("disabled", "disabled");
                    },
                    success: function (data) {
                        var data = JSON.parse(data);
                        alert('code' + data.code + 'msg==' + data.msg);
                        window.location.reload();
                    },
                    complete: function () {
                        $("input.edit-btn").removeAttr("disabled");
                    },
                    error: function (data) {
                        console.log(data);
                    }
                })
            }
        });

        $(".checkType").bind('click', function () {
            var type = $("input#checkType");
            if (type.val() == 0) {
                type.val(1);
                $(".channel").prop('checked', true);
            } else {
                type.val(0);
                $('.channel').prop('checked', false);
            }
        })
    });
</script>