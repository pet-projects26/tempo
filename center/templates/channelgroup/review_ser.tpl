<style>
    label.checkType :hover {
        color: #0079A2;
    }

    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form id="my_form" method="post">
        <div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin: 10px">
            <table class="itable itable-color">
                <!-- 渠道组 Start -->
                <tr class='p-tr'>
                    <td style="width:150px">渠道组</td>
                    <td>
		                <span class="p-span c-all c-channel-group <{if $edit_id eq ''}> p-checked<{/if}> span-click">
			                <input style="width: 15px" class="p-input" type="checkbox" name="channel_group[]" value="" <{if $edit_id eq ''}>checked<{/if}> id="channel-group-all"/>
			                <label for="channel-group-all" class="p-text">所有渠道组</label>
		                </span>

                        <{foreach from = $groups key = id item = row}>
			            <span class="p-span c-channel-group <{if $id eq $edit_id}> p-checked <{/if}>  c-channel-group-list span-click">
				            <input style="width: 15px" class="p-input" type="checkbox" name="channel_group[]" <{if $id eq $edit_id}>checked<{/if}> value="<{$id}>" id="channel_group-<{$id}>"/>
				            <label for="channel_group-<{$id}>" class="p-input"><{$row}></label>
			            </span>
                        <{/foreach}>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-left: 10px">
            <label for="version_r">版本号：</label>
            <input id="version_r" name="review_num" class="version_r" type="text" value="<{$version}>" placeholder="请输入版本号"/>
        </div>
        <div style="clear: both;"></div>
        <button style="margin-top: 8px;margin-left: 12px" type="button" class="gbutton edit-btn">提交</button>
    </form>
</div>

<script>
    $(function () {
        //选择
        $().ready(function() {
            //所有渠道
            $('.c-all').unbind('click').bind('click', function() {
                var checked = $(this).find('input').attr('checked');

                var obj = $(this).find('input');

                var mark = obj.prop('checked');

                if (mark) {
                    $('.c-channel-group-list input').prop('checked', 0);
                    $('.c-channel-group-list input').parent('span').removeClass('p-checked');

                    $(this).addClass('p-checked');

                } else {
                    $(this).removeClass('p-checked');
                }

                $('.server fieldset').css('display' , 'block');
                $('.server fieldset').removeClass('show' );
                //自动选中全部游戏包
                $('.server input[value !=""]').prop('checked', 0);
                $('.server  input[value !=""]').parent('span').removeClass('p-checked');
                $('.server input[value=""]').prop('checked', 1);
                $('.server input[value=""]').parent('span').addClass('p-checked');

            });

            //其他渠道
            $('.c-channel-group-list input').unbind('click').bind('click', function() {
                var mark = $(this).prop('checked');
                if (mark) {
                    $(this).parent('span').addClass('p-checked');
                } else {
                    $(this).parent('span').removeClass('p-checked');
                }

                var allLen = $('.c-channel-group input').length;

                var checkLen = $('.c-channel-group-list input:checked').length;

                //如果都没有选中，则自动选中全选
                if (checkLen == 0) {
                    $('.c-channel-group input[value=""]').prop('checked', 1);
                    $('.c-channel-group input[value=""]').parent('span').addClass('p-checked');

                    //自动选中全部服务器
                    $('.server input[value !=""]').prop('checked', 0);
                    $('.server input[value !=""]').parent('span').removeClass('p-checked');
                    $('.server input[value=""]').prop('checked', 1);
                    $('.server input[value=""]').parent('span').addClass('p-checked');

                } else {
                    $('.c-channel-group input[value=""]').prop('checked', 0);
                    $('.c-channel-group input[value=""]').parent('span').removeClass('p-checked');

                }

                //根据渠道组筛选服务器
                InputValue=$(this).val();

                if($('#'+InputValue).hasClass('show')){

                    $('#'+InputValue).removeClass('show');
                } else{

                    $('#'+InputValue).addClass('show');
                }

                $('.server fieldset').each(function(){  //遍历child,如果有show 就让他显示，没有就隐藏
                    $('.server fieldset').eq(0).show();
                    if($(this).hasClass('show')){
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                })
                //如果没选中，则全部显示
                if(checkLen == 0){
                    $('.server fieldset ').show();
                }

            });

        });
        $(".edit-btn").bind('click', function () {
            var channel_group = $("input[name='channel_review']:checked").val();
            var version = $(".version_r").val();
            if(confirm('确认批量编辑提审版本号？')) {
                $.ajax({
                    type: 'post',
                    url: 'admin.php?&ctrl=Review&act=add_review_handel',
                    data: $("#my_form").serialize(),
                    beforeSend: function () {
                        $("input.edit-btn").attr("disabled", "disabled");
                    },
                    success: function (data) {
                        var data = JSON.parse(data);
                        alert('code' + data.code + 'msg==' + data.msg);
                        window.location.reload();
                    },
                    complete: function () {
                        $("input.edit-btn").removeAttr("disabled");
                    },
                    error: function (data) {
                        console.log(data);
                    }
                })
            }
        });
        $(".checkType").bind('click', function () {
            var type = $("input#checkType");
            if (type.val() == 0) {
                type.val(1);
                $(".channel").prop('checked', true);
            } else {
                type.val(0);
                $('.channel').prop('checked', false);
            }
        })
    });
</script>