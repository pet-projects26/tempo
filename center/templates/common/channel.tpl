<style type="text/css">
    #checkboxChannelList .select{ background-color:#dcd8d8; }
    #checkboxChannelList ul li{
        display:inline-block;
        width:150px;
        height:30px;
        line-height:30px;
        text-align:center;
        border:1px solid #cccccc;
        cursor:pointer;
        margin:0 !important;
        padding:0 !important;
        overflow:hidden;
    }
</style>
<div id="checkboxChannelList">
    <input type="hidden" value="[]" id="checkboxChannel">
    <ul>
        <li id="channelSelectAll" set="0">全选</li>
        <{foreach from=$checkboxChannel item=item}>
        <li lid="<{$item.channel_id}>" class="name"><{$item.name}></li>
        <{/foreach}>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        //单选
        $('#checkboxChannelList li').click(function(){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
            getChannelSelected();
        });
        //全选
        $('#channelSelectAll').click(function(){
            if($(this).attr('set') == '0'){
                $('#checkboxChannelList li').each(function(){
                    if(!$(this).hasClass('select')){
                        $(this).addClass('select');
                    }
                });
                $(this).html('全不选');
                $(this).attr('set' , 1);
            }
            else{
                $('#checkboxChannelList li').each(function(){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                });
                $(this).html('全选');
                $(this).attr('set' , 0);
            }
            getChannelSelected();
        });
    });

    //每次选择改变时获取已选择的值
    function getChannelSelected(){
        var channel = new Array();
        $('#checkboxChannelList li').each(function(){
            if($(this).hasClass('select') && $(this).hasClass('name')){
                channel.push($(this).attr('lid'));
            }
        });
        var value = JSON.stringify(channel);
        $('#checkboxChannel').val(value);
    }
</script>