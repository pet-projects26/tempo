 <div id="checkboxconpro" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>x</span><span class="sr-only"></span>
        </button>
        <h4 class="title-text">筛选</h4>
        <div class="text">
            <div class="toolTips">
                <div class="toolTips-select">
                    <ul>
                        <li><input type="button" class="allSelect gbutton" value="全选"></li>
                        <li><input type="button" class="unSelect gbutton" value="反选"></li>
                    </ul>
                </div>
            </div>
            <div id="modal-tabs" class="">
                <ul><li><a href="#tabs-1">物品来源</a></li></ul>
                <div id="tabs-1" class="tabs-content" <{if !$showGroup}>style="display:none;"<{/if}>>
                    <div id="group">
                        <ul>
                            <{foreach from=$list.group item=item}>
                                <li>
                                    <span class="name"><{$item.name}></span>
                                    <span class="lid"><{$item.id}></span>
                                </li>
                                <{/foreach}>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <p style="text-align:right;padding:0px 30px 10px 0px;">
                <button class="gbutton confirm">确定</button>
                <button class="gbutton" onclick="Custombox.close();">取消</button>
            </p>
        </div>
    </div>
<script type="text/javascript">
$(function(){
    $('#filter_').on('click' , function(event){  //todo=== 写一个物品来源筛选模板
        Custombox.open({
            target: '#checkboxconpro',
            effect: 'slide',
            width: 600,
            overlayOpacity: 0.4,
            overlaySpeed:5,
            speed: 1
        });
        event.preventDefault();
    });
    // tabs
    $("#modal-tabs").tabs({ selected:0 });
    // tab select
    $('#modal-tabs .tabs-content ul li').click(function(event){
        if (!$(this).hasClass('select')){
            $(this).addClass('select');
        } else {
            $(this).removeClass('select');
        }
    });
    $('.confirm').click(function(event){
         var tabData = new Array();
        $('.tabs-content').each(function(index , el){
            tabData[index] = new Array();
            $('li', this).each(function(i , v){
                if( $(this).hasClass('select')){
                    tabData[index].push( $('.lid' , this).html());
                }
            });
        });
        $('#checkboxScp').data(tabData);
        Custombox.close();
    });
    $('.allSelect').click(function(event){
        $("#modal-tabs .tabs-content").find('li').each(function(index , el){
            if(!$(this).is(':hidden')){
                $(this).addClass('select');
            }
        });
    });
    $('.unSelect').click(function(event){
        $("#modal-tabs .tabs-content").find('li').each(function(index , el){
            if(!$(this).is(':hidden')){
                if($(this).hasClass('select')){
                    $(this).removeClass('select');
                }
                else{
                    $(this).addClass('select');
                }
            }
        });
    });
});
function selectAll(id){
    $(id + ' .tabs-content ul li').click(function(event){
        if(!$(this).hasClass('select')){
            $(this).addClass('select');
        }
        else{
            $(this).removeClass('select');
        }
    });
}
</script>
