<style>
    .search_float{float:left; padding:5px;height:auto;}
    #server-list label{ display: inline-block;width: 200px;height: 18px;}
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all <{$other_data.tipsID}>" style="margin-bottom:8px;padding:10px;display:none;">
    <span style="float:right">
        <a class="close" href="#" onClick="$(this).parent().parent().slideUp();" style="position:absolute;top:10px;right:15px;background:#fff;border:none;padding:2px 5px;">X</a>
    </span>
    <div id="TipsCont"></div>
</div>
<!--
使用方法：
1. 在$other_data中传入标识唯一的tipsID作为class
2. 需要tips数据时：
    $('.tipsID').slideDown();
    $('.tipsID').children("#TipsCont").html(html);
 -->

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="<{$other_data.id}>">
    <div id="datatable_header"><{$other_data.header}></div>
    <div class="datatable_search" style="padding:5px;">
        <div class="search_content">
            <{if $other_data.search_header}>
            <div class="search_float"><{$other_data.search_header}></div>
            <{/if}>
            <{foreach from=$search_fields key=key item=item}>
            <div class="search_float <{if $item.type != 'html'}>search_field field_<{$key}><{/if}><{if $item.class}> <{$item.class}><{/if}>" type="<{$item.type}>">
                <{if $item.title}><{$item.title}>:&nbsp;&nbsp;<{/if}>
                <{if $item.type == 'input'}>
                    <input value="<{$item.value|default:''}>" <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;">
                <{elseif $item.type == 'checkbox'}>
                <label><input type="checkbox" value="<{$item.value|default:''}>"  style="margin-top: 4px" <{if $item.checked}>checked<{/if}>><span style="margin-top: -10px"><{$item.text}></span></label>
                <{elseif $item.type == 'date'}>
                <input value="<{$item.value|default:''}>" <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;" class="datepicker">&nbsp;&nbsp;
                <{elseif $item.type == 'range'}>
                <input <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;">&nbsp;~&nbsp;
                <input <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;">
                <{elseif $item.type == 'range_date'}>
                <input value='<{$item.value[0]}>' <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;" class="datepicker">&nbsp;~&nbsp;
                <input value='<{$item.value[1]}>' <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;" class="datepicker">
                <{elseif $item.type == 'range_time'}>
                <input type="text" value='<{$item.value[0]}>' <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;" class="datetimepicker">&nbsp;~&nbsp;
                <input type="text" value='<{$item.value[1]}>' <{if $item.size}>size="<{$item.size}>"<{/if}> style="width:<{$item.width}>px;" class="datetimepicker">
                <{elseif $item.type == 'select'}>
                <select style="width:200px !important;">
                <{foreach from=$item.value key=key item=it}>
                    <{if isset($item.default)}>
                        <option value="<{$key}>" <{if $item.default==$key}>selected='selected'<{/if}>><{$it}></option>
                    <{else}>
                        <option value="<{$key}>"><{$it}></option>
                    <{/if}>
                <{/foreach}>
                </select>
                <{elseif $item.type == 'selects'}>
                <select>
                <{foreach from=$item.value[0] key=key item=it}>
                    <option value="<{$key}>"><{$it}></option>
                <{/foreach}>
                </select>&nbsp;~&nbsp;
                <select>
                <{foreach from=$item.value[1] key=key item=it}>
                    <option value="<{$key}>"><{$it}></option>
                <{/foreach}>
                </select>
                <{elseif $item.type == 'scp'}>
                    <{$item.value}>
                <{/if}>
            </div>
            <{/foreach}>
            <{if $search_fields}>
            <div class="search_float"><input type="button" class="datatable_search_button gbutton" value="搜索"></div>
            <{/if}>
            <{if $other_data.search_footer}>
            <div class="search_float"><{$other_data.search_footer}></div>
            <{/if}>
            <{if $scp}>
            <div><{$scp}></div>
            <{/if}>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="margin:5px" id="table_header"><{if $other_data.table_header}><{$other_data.table_header}><{/if}></div>
    <table cellpadding="0" cellspacing="0" border="0" class="table_list">
        <thead>
            <tr height="44px">
            <{foreach from=$show_fields item=item}>
                <th <{if $item.width}>width="<{$item.width}>"<{/if}>><{$item.title}></th>
            <{/foreach}>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="<{$show_fields|count}>" class="dataTables_empty"></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <{foreach from=$show_fields item=item}>
                <th><{$item.title}></th>
                <{/foreach}>
            </tr>
        </tfoot>
    </table>
</div>
<script type="text/javascript">
$('select').select2();
$('.datepicker').datepicker();
var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
$('.datetimepicker').datetimepicker(timepickerlang);
$('#<{$other_data.id}> .datatable_search .title').toggle(function(){
    $('#<{$other_data.id}> .search_content').slideUp();
},function(){
    $('#<{$other_data.id}> .search_content').slideDown();
});

var $<{$other_data.id}> = $('#<{$other_data.id}> .table_list').dataTable({
    "bProcessing": true,
    "deferRender": true,
    "bServerSide": true,
    "iDisplayLength": <{$other_data.iDisplayLength}>,
    "bLengthChange": true,
    "bStateSave": <{if $other_data.bStateSave == true}>true<{else}>false<{/if}>,
    "bFilter": true,
    "sPaginationType": "full_numbers",
    "sAjaxSource": "<{$ajax_source}>",
    "aaSorting": [[ <{$other_data.sortCol}>, "<{$other_data.sortDir}>" ]],
    'bSort': <{$other_data.bSort}>,
    "oLanguage": { "sUrl": "style/js/dt_cn.t" },
    "aoColumns": [<{$aoColumns}>],
    "sDom": '<{if $other_data.sDom}><{$other_data.sDom}><{else}><"top"lp>rt<"bottom"ip><"clear"><{/if}>'
});

$('#<{$other_data.id}> .table_list tbody tr').live('mouseover', function(){
    $(this).addClass('row_selected');
}).live('mouseout' , function(){
    $(this).removeClass('row_selected');
});

$('#<{$other_data.id}> .datatable_search input').keydown(function (event){
    if(event.keyCode==13){
        $('#<{$other_data.id}> .datatable_search_button').trigger("click");
    }
});
function getFilter(){
    var data = new Array();
    $("#<{$other_data.id}> .search_field").each(function(){
        var type = $(this).attr('type');
        var value = '';
        if(type == 'input' || type == 'select'){
            value = $(this).find(type).val();
        }else if(in_array(type, ['range', 'range_date', 'range_time'])){
            var range = new Array();
            $(this).find('input').each(function(){
                range.push($(this).val());
            });
            value = range.join('|');
        }else if(type == "date"){
            value = $(this).find("input").val();
        }
        else if(type == 'checkbox' && $(this).find('input[type="checkbox"]')[0].checked){
            value = 1;
        }
        else if(type == 'selects'){
            var selects = new Array();
            $(this).find('select').each(function(){
                selects.push($(this).val());
            });
            value = selects.join('|');
        }
        else if(type == 'scp'){
            value = JSON.stringify($('#checkboxScp').data());
            $.cookie('scp', value);
        }
        data.push(value);
    });
    return data;
}
$('#<{$other_data.id}> .datatable_search_button').click(function(){
    $<{$other_data.id}>.fnMultiFilter(getFilter());
});
<{$other_data.js|default:''}>
</script>
<{$other_data.jsf|default:''}>
