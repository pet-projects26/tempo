<style type="text/css">
    #checkboxPackageList .select{ background-color:#dcd8d8; }
    #checkboxPackageList ul li{
        display:inline-block;
        width:150px;
        height:30px;
        line-height:30px;
        text-align:center;
        border:1px solid #cccccc;
        cursor:pointer;
        margin:0 !important;
        padding:0 !important;
        overflow:hidden;
    }
</style>
<div id="checkboxPackageList">
    <input type="hidden" value="[]" id="checkboxPackage">
    <ul>
        <li id="packageSelectAll" set="0">全选</li>
        <{foreach from=$checkboxPackage item=item}>
        <li lid="<{$item.channel_id}>" class="name"><{$item.name}></li>
        <{/foreach}>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        //单选
        $('#checkboxPackageList li').click(function(){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
            getPackageSelected();
        });
        //全选
        $('#packageSelectAll').click(function(){
            if($(this).attr('set') == '0'){
                $('#checkboxPackageList li').each(function(){
                    if(!$(this).hasClass('select')){
                        $(this).addClass('select');
                    }
                });
                $(this).html('全不选');
                $(this).attr('set' , 1);
            }
            else{
                $('#checkboxPackageList li').each(function(){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                });
                $(this).html('全选');
                $(this).attr('set' , 0);
            }
            getPackageSelected();
        });
    });

    //每次选择改变时获取已选择的值
    function getPackageSelected(){
        var pkg = new Array();
        $('#checkboxPackageList li').each(function(){
            if($(this).hasClass('select') && $(this).hasClass('name')){
                pkg.push($(this).attr('lid'));
            }
        });
        var value = JSON.stringify(pkg);
        $('#checkboxPackage').val(value);
    }
</script>