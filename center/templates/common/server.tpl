<style type="text/css">
    #checkboxServerList .select{ background-color:#dcd8d8; }
    #checkboxServerList ul li{
        display:inline-block;
        width:150px;
        height:30px;
        line-height:30px;
        text-align:center;
        border:1px solid #cccccc;
        cursor:pointer;
        margin:0 !important;
        padding:0 !important;
        overflow:hidden;
    }
</style>
<div id="checkboxServerList">
    <input type="hidden" value="[]" id="checkboxServer">
    <ul>
        <li id="serverSelectAll" set="0">全选</li>
        <{foreach from=$checkboxServer item=item}>
        <li lid="<{$item.server_id}>" class="name"><{$item.name}></li>
        <{/foreach}>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        //单选
        $('#checkboxServerList li').click(function(){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
            getServerSelected();
        });
        //全选
        $('#serverSelectAll').click(function(){
            if($(this).attr('set') == '0'){
                $('#checkboxServerList li').each(function(){
                    if(!$(this).hasClass('select')){
                        $(this).addClass('select');
                    }
                });
                $(this).html('全不选');
                $(this).attr('set' , 1);
            }
            else{
                $('#checkboxServerList li').each(function(){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                });
                $(this).html('全选');
                $(this).attr('set' , 0);
            }
            getServerSelected();
        });
    });

    //每次选择改变时获取已选择的值
    function getServerSelected(){
        var server = new Array();
        $('#checkboxServerList li').each(function(){
            if($(this).hasClass('select') && $(this).hasClass('name')){
                server.push($(this).attr('lid'));
            }
        });
        var value = JSON.stringify(server);
        $('#checkboxServer').val(value);
    }
</script>