<style type="text/css">
    .p-input {
        vertical-align:middle;
    }

    .p-text {
        vertical-align:middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }
    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-open-form">
       
        <table class="itable itable-color">
            <tbody>

                <tr>
                    <td style="width:150px;">名称</td>
                    <td>
                        <input type="input" name="name" id="name" value="<{$name}>"  readonly="readonly">
                    </td>
                </tr>

                <tr>
                    <td style="width:150px;">渠道组</td>
                    <td>
                       <{foreach from = $groups key = id item = row}>
                            <span class="p-span c-channel-group  c-channel-group-list span-click <{if in_array($id, $group)}> p-checked <{/if}>" >
                                <input class="p-input"  type="checkbox" name="channel_group[]"  value="<{$id}>"  id="channel_group-<{$id}>" <{if in_array($id, $group)}> checked <{/if}> />
                                <label for="channel_group-<{$id}>" class="p-input" ><{$row}></label>
                            </span> 
                        <{/foreach}>
                    </td>
                </tr>
                <tr>
                    <td>所属系统</td>
                    <td>
                        <select name="type" id="type">
                            <option value="1" <{if $type ==1}> selected  <{/if}> >IOS</option>
                            <option value="2" <{if $type ==2}> selected  <{/if}> >安卓</option>
                            <option value="3" <{if $type ==3}> selected  <{/if}> >混服</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><input type="button" value="编辑" class="gbutton"></td>
                    <td colspan="2"><input type="hidden" value="<{$ids}>" id="id" class="gbutton"></td>
                </tr>
                
            </tbody>
        </table>
    </form>
</div>

<script type="text/javascript">

    $('.p-input').unbind('click').bind('click', function() {

        var mark = $(this).prop('checked');
        if (mark) {
            $(this).parent('span').addClass('p-checked');
        } else {
            $(this).parent('span').removeClass('p-checked');
        }

    })

    $('.gbutton').click(function(){
        var obj = $(".p-input");
        var check_item = [];
        obj.each(function(){
            if($(this).prop('checked')){
                check_item.push($(this).val());
            }
        });

        var type = $('#type').val();
        var id = $('#id').val();
        var name = $('#name').val();

        if(check_item == ''){
            $.dialog.tips('请选择渠道组');
            return false;
        }
        $.post('admin.php?ctrl=cross&act=edit_data',{"groups":check_item,'type':type , 'id' : id,'name':name} , function(data){
            $.dialog.tips(data.msg);
            if(data.code == 1){
                $tabs.tabs('select',0);
                $tabs.tabs('load' , 0);
            }
        },'json');
        
    });
        

     
</script>