// JavaScript Document
$(function(){
      
		 num =0;
		//添加物品	
		$('#additem').click(function(){
			num =  num +1;
			var itemWidget = '<tr class="tr_t itemWidget'+num+'">';
			itemWidget += '<td style="width:150px;"><input type="text" num="'+num+'" name="item_id['+num+']" style="width:150px;" class="item_id"  value=""></td>';
			itemWidget += '<td style="width:150px;"><input type="text" name="item_price['+num+']" style="width:150px;" class="item_name"  value=""></td>';
			itemWidget += '<td style="width:150px;"><input type="text" name="item_count['+num+']" value="1" style="width:150px;" class="item_count"></td>';
			itemWidget += '<td style="width:150px;"><input type="text" name="start_time['+num+']" value="" style="width:150px;" class="start_time" ></td>';
			itemWidget += '<td style="width:150px;"><input type="text" name="end_time['+num+']" value="" style="width:150px;" class="end_time" ></td>';
			itemWidget += '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
			itemWidget += '<td><input type="button" value="复制" num="'+num+'" class="gbutton copyitem" title="复制该行"></td>';
			itemWidget += '</tr>';
			
			$('.itemTable').children('tbody').append(itemWidget);
			$( ".item_id" ).autocomplete({
      			source: allitem
    		});
			time();
			
		  
        });
		
		//删除物品
        $('table').delegate('.delitem' , 'click' , function(event){
			
            $(this).parent().parent().remove();
			
        });
		
		
		
		//复制物品
        $('.itemTable').on("click",'.copyitem',function(event){
			
          	var number = $(this).attr('num');
            var item_id  = $("input[name='item_id["+number+"]']" ) .val();
            var item_price = $("input[name='item_price["+number+"]']" ) .val();
            var item_count = $("input[name='item_count["+number+"]']" ) .val();
			
			var start_time = $("input[name='start_time["+number+"]']" ) .val();
			var end_time = $("input[name='end_time["+number+"]']" ) .val();
			
			num =  num +1; 
                
            var itemWidget = '<tr class="tr_t itemWidget'+num+'">';
            itemWidget += '<td style="width:150px;"><input type="text" num="'+num+'" name="item_id['+num+']" value="'+item_id+'" style="width:150px;" class="item_id" ></td>';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_price['+num+']" value="'+item_price+'" style="width:150px;" class="item_name" ></td>';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_count['+num+']" value="'+item_count+'" style="width:150px;" class="item_count"></td>';
			itemWidget += '<td style="width:150px;"><input type="text" name="start_time['+num+']" value="'+start_time+'" style="width:150px;" class="start_time" ></td>';
			itemWidget += '<td style="width:150px;"><input type="text" name="end_time['+num+']" value="'+end_time+'" style="width:150px;" class="end_time" ></td>';
            itemWidget += '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
            itemWidget += '<td><input type="button" value="复制" num="'+num+'" class="gbutton copyitem" title="复制该行"></td>';
            itemWidget += '</tr>';
          
           $(this).parents('.tr_t').after(itemWidget);
                  
        }); 
		
		//提交数据
        var error = $('#error');
        $('#manual-mail-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=dealgroup&act=deal_add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
					
                    $.dialog.tips(data.msg);
                })
            }
            return false;
        });
		//交易组的选择
		$('#checkboxgroupList li').click(function(){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
            getGroupSelected();
        });
        //全选
        $('#groupSelectAll').click(function(){
            if($(this).attr('set') == '0'){
                $('#checkboxgroupList li').each(function(){
                    if(!$(this).hasClass('select')){
                        $(this).addClass('select');
                    }
                });
                $(this).html('全不选');
                $(this).attr('set' , 1);
            }
            else{
                $('#checkboxgroupList li').each(function(){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                });
                $(this).html('全选');
                $(this).attr('set' , 0);
            }
            getGroupSelected();
        });
    });
	
	 //每次选择改变时获取已选择的值
    function getGroupSelected(){
        var channel = new Array();
        $('#checkboxgroupList li').each(function(){
            if($(this).hasClass('select') && $(this).hasClass('name')){
                channel.push($(this).attr('lid'));
            }
        });
        var value = JSON.stringify(channel);
        $('#checkboxGroup').val(value);
    }
	//检查表格
    function form_check(){
        var error = $('#error');
		var checkboxGroup = $('#checkboxGroup').val();
		
		if(checkboxGroup == '' || checkboxGroup == '[]'){
			$.dialog.tips('交易组不能为空');
			error.val(1);
            return false;	
		}else{
			error.val(0);	
		}
		
		$('.item_id').each(function(){
			
			var item_id  = $(this).val();
			var num = $(this).attr('num');
			var item_count = $('.item_count').eq(num).val();
			var item_name = $('.item_name').eq(num).val();
			
			reg=/^1.*/;
			icon = /^399.*/;
			if(reg.test(item_id) && item_count >1){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID'+item_id +'为装备物品，数量只能为1');
            	error.val(1);
            	return false;	
			}else if(icon.test(item_id)){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID'+item_id +'为货币，不能上架');
            	error.val(1);
            	return false;
			}else if(item_id.substr(8,1) != 0){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID'+item_id +'为绑定物品，不能上架');
            	error.val(1);
            	return false;
			}else if(item_id ==''){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID不能为空');
            	error.val(1);
            	return false;
			}else if(item_name =='' || item_name == 0){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，价格不能为空或者为0');
            	error.val(1);
            	return false;
			}else if(item_count ==''){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，数量不能为空或者为0');
            	error.val(1);
            	return false;
			}else{
				error.val(0);	
			}
		});
		
		
		
		

    }
	
	
	
	
	
	