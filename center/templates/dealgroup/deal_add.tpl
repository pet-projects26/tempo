<script type="text/javascript" src="templates/dealgroup/deal.js"></script>
<style>
#checkboxgroupList .select {
	background-color: #dcd8d8;
}
#checkboxgroupList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
}
</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="manual-mail-form">
    <table class="itable itable-color">
      <tbody>
        <tr>
          <td >交易组</td>
          <td id="checkboxgroupList"><ul>
              <li id="groupSelectAll" set="0">全选</li>
              <{foreach from=$group item=item}>
              <li lid="<{$item._id}>" class="name"><{$item.name}></li>
              <{/foreach}>
            </ul></td>
        </tr>
        <tr>
          <td style="width:150px;">模版</td>
          <td><select id="tpl" name="tpl">
              <{foreach from=$tpl item=i key=key}>
              <option value="<{$key}>"><{$i}></option>
              <{/foreach}>
            </select> <input type="text" name="group" value="" id="checkboxGroup" readonly /></td>
        </tr>
        <tr>
          <td style="width:150px;">添加物品</td>
          <td><table class="itemTable" style="width:600px;">
              <thead>
                <tr>
                  <th style="width:150px;">物品ID</th>
                  <th style="width:150px;">上架价格</th>
                  <th style="width:150px;">数量</th>
                  <th style="width:150px;">上架时间</th>
                  <th style="width:150px;">下架时间</th>
                  <th style="width:150px;" colspan="2">操作</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <input type="button" value="添加" class="gbutton" id="additem" title="添加填写一个物品的行"></td>
        </tr>
        <tr>
          <td colspan="2"><input type="submit" class="gbutton" value="发送" id="charge"> <input type="button" class="gbutton" value="取消" id="cancel" style="margin-left:20px;">
            &nbsp;&nbsp;
            <input type="hidden" id="error" value="1">
            &nbsp;&nbsp;
           
           </td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
<script>

function time(){
	timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'};
	$('.start_time').datetimepicker(timepickerlang);
	$('.end_time').datetimepicker(timepickerlang);
}

$(function(){
	
	allitem ='';
	$.post('admin.php?ctrl=dealgroup&act=getAllItem','',function(data){
		
		allitem = data;
		
				
	},'json');
		
	
	//当模版选择的时候加载模版的数据
	$("#tpl").change(function(){
		$('.itemTable').children('tbody').html("");
		var id = $('#tpl').val();
		if(id != ''){
			$.post('admin.php?ctrl=dealgroup&act=getTpl&id=' + id + '&num='+ num,'',function(data){
				num = data.num;
				$('.itemTable').children('tbody').append(data.html);
				time();
				$( ".item_id" ).autocomplete({
      				source: allitem
    			});
			},'json');	
		}
		
	});
	
	$("#cancel").click(function(){
		
		$( "#tabs" ).tabs({ selected: 0 });	
		
	});
	
	
	
});
</script>