<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;">
<div style="margin-top: 10px;">
  <input type="button" class="gbutton" id="checkall" value="全选">
  <input type="button" class="gbutton" id="unshelve" value="下架">

</div>

  <table cellpadding="0" cellspacing="0" border="0" class="oddeven px1">
    <thead>
      <tr height="44px">
        <th >物品ID</th>
        <th >物品名称</th>
        <th >价格</th>
        <th >数量</th>
        <th >上架时间</th>
        <th >下架时间</th>
        <th >状态</th>
      </tr>
    </thead>
    <tbody>
    <{foreach from=$item item=item key=key}>
   		<tr >
        <td class=".sorting_1"><input class="checked" type="checkbox" value="<{$item.uuid}>"><span><{$item.uuid}></span></td>
        <td><{$item.name}></td>
        <td><{$item.price}></td>
        <td><{$item.count}></td>
        <td><{$item.startTm}></td>
        <td><{$item.endTm}></td>
        <td><{$item.flag}></td>
      </tr>
    <{/foreach}>
    </tbody>
  </table>
</div>

<script>
$(function(){
  var check=1;
  $("#checkall").click(function(){
      if(check == 1){
        $(".checked").attr("checked", true);
        check =0; 
      }else{
        check=1;  
        $(".checked").attr("checked", false);       
      }
    });

  $('#unshelve').click(function(){
    var item = new Array();
    $('.checked:checked').each(function(){

      item.push($(this).val());  
    });
    if(item.length == 0){
      $.dialog.tips('请选择要下架的物品'); return false;   
    }

    if(confirm("确定要下架物品吗？")){
      $.ajax({
          url: 'admin.php?ctrl=dealgroup&act=unshelve',
          type: 'POST',
          dataType: 'JSON',
          data: {'item':item}
        }).done(function(data){
          $.dialog.tips('下架物品成功');
        })   
      } 


  });

  
      

});
</script>