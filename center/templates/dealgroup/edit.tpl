<{if $id}>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="cgroup-edit-form">
        <div class="hidden">
            <input type="hidden" id="id" name="id" value="<{$id}>">
            <input type="hidden" id="error" value="1">
            <input type="hidden" id="oldname" name="oldname" value="<{$name}>">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" id="name" name="name" value="<{$name}>"></td>
                </tr>
                <tr>
                    <td>
                        各渠道组包含的服务器<br>
                        <label style="position:relative;">
                            <input type="checkbox" id="checkAll">
                            <span style="position:absolute;top:-10px;width:100px;">全选</span>
                        </label>
                    </td>
                    <td>
                        <{foreach from=$arr key=channel_name item=channel}>
                        <{if $channel_name}>
                        <div class="channel_package">
                            <div style="padding:8px 0;color:#228b22;font-size:15px;" class="channel">
                                <label style="position:relative;">
                                    <input type="checkbox" class="cb_channel">
                                    <span style="position:absolute;top:-9px;left:20px;width:600px;"><{$channel_name}></span>
                                </label>
                            </div>
                            <div style="border-bottom:1px solid #000000;padding-bottom:4px;" class="package">
                                <{foreach from=$channel key=num item=server}>
                                <label style="margin-right:170px;position:relative;" class="cb_package">
                                    <input type="hidden" name="<{if $server.checked}>server[]<{/if}>" value="<{$server.server_id}>">
                                    <input type="checkbox" <{if $server.checked}>checked="checked"<{/if}>>
                                    <span style="position:absolute;top:-10px;width:160px;<{if $server.checked}>color:#ff0000;<{/if}>"><{$server.server_name}></span>
                                </label>
                                <{if ($num + 1) mod 7 == 0}><br><{/if}>
                                <{/foreach}>
                            </div>
                        </div>
                        <{/if}>
                        <{/foreach}>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<{else}>
请选择编辑的交易组
<{/if}>
<script type="text/javascript">
    $(function(){
        $('label').css('cursor','pointer');
        check();
        var error = $('#error');
        $('#cgroup-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=dealgroup&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code == 0 ){
                        $tabs.tabs('select', 2);
                    }else if(data.code == 1){
						$tabs.tabs('select', 0);	
					}
                });
            }
            return false;
        });
    });

    function all_check(){
        var all_check = 1;
        $('.cb_channel').each(function(){
            if(!$(this).attr('checked')){
                all_check = 0;
            }
        });
        if(all_check){
            $('#checkAll').attr('checked' , 'checked');
        }
        else{
            $('#checkAll').attr('checked' , false);
        }
    }

    function channel_check(){
        $('.channel_package').each(function(){
            var checked = 1;
            $(this).children('.package').children('.cb_package').each(function(){
                if(!$(this).children('input[type=checkbox]').attr('checked')){
                    checked = 0;
                }
            });
            if(checked){
                $(this).children().children().children('.cb_channel').attr('checked' , 'checked');
            }
            else{
                $(this).children().children().children('.cb_channel').attr('checked' , false);
            }
        });
    }

    function check(){
        channel_check();
        all_check();
        //单个包选中
        $('label.cb_package input[type=checkbox]').click(function(){
            channel_check();
            all_check();
            if($(this).attr('checked')){
                $(this).prev().attr('name' , 'server[]'); //提交时需要name属性获取值，所以选中的给他name属性
                $(this).next().css('color' , '#ff0000');
            }
            else{
                $(this).prev().attr('name' , '');
                $(this).next().css('color' , '#000000');
            }
        });
        //同一渠道所有包选中
        $('.cb_channel').click(function(){
            all_check();
            var label = $(this).parent().parent().next().children('label');
            if($(this).attr('checked')){
                label.children('input[type=hidden]').attr('name' , 'server[]');
                label.children('input[type=checkbox]').attr('checked' , 'checked');
                label.children('span').css('color' , '#ff0000');
            }
            else{
                label.children('input[type=hidden]').attr('name' , '');
                label.children('input').attr('checked' , false);
                label.children('span').css('color' , '#000000');
            }
        });
        //全选
        $('#checkAll').click(function(){
            if($(this).attr('checked')){
                $('.cb_channel').attr('checked' , 'checked');
                $('label.cb_package input[type=hidden]').attr('name' , 'server[]');
                $('label.cb_package input[type=checkbox]').attr('checked' , 'checked');
                $('label.cb_package span').css('color' , '#ff0000');
            }
            else{
                $('.cb_channel').attr('checked' , false);
                $('label.cb_package input[type=hidden]').attr('name' , '');
                $('label.cb_package input[type=checkbox]').attr('checked' , false);
                $('label.cb_package span').css('color' , '#000000');
            }
        });
    }

    function form_check(){
        var error = $('#error');
        //渠道组名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写渠道组名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>