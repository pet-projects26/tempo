<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" >
    <table class="itable itable-color">
      <tbody>
       
      <td style="width: 150px; margin-top: 3px; margin-bottom: 3px;">服务器名</td>
        <td><input id="server" name="server" /></td>
      <tr>
        <td colspan="2" style="margin-top: 3px; margin-bottom: 3px;"><input id="search" type="button" class="gbutton" value="搜索" style="margin-top: 3px; margin-bottom: 3px;"></td>
      </tr>
        </tbody>
      
    </table>
  </form>
</div>


<div id="data" class="ui-tabs ui-widget ui-widget-content ui-corner-all " style=" margin-top:10px;">

</div>
<script>
    $(function(){
		$('#search').click(function(){
			
			var server = $("#server").val();
			if(server == ''){
				alert('请输入要搜索的服务器');return false;	
			}
			
			$.post('admin.php?ctrl=dealgroup&act=search_action',{'server':server},function(data){
				$("#data").html(data.msg);	
			},'json')
		});
    
	});
</script> 
