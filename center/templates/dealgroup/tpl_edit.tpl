

<{if $id}>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    
    <form action="" class="fm" id="manual-mail-form">
        <table class="itable itable-color">
            <tbody>
            
            
           
            <tr>
                <td style="width:150px;">模版名称</td>
                <td><input type="text" name="title" id="title" value="<{$result.title}>" style="width:200px;"></td>
            </tr>
            
            <tr>
                <td style="width:150px;">类型 </td>
                <td>
                    <select id="type" name="type">

                        <option value="0" <{if $result.type eq 0 }> selected ="true" <{/if}>>老服</option> 
                        <option value="1" <{if $result.type eq 1 }> selected ="true" <{/if}>>新服</option> 
                        <option value="2" <{if $result.type eq 2 }> selected ="true" <{/if}>>合服</option>
                        <option value="3" <{if $result.type eq 3 }> selected ="true" <{/if}>>跨服</option>

                    </select>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">上架时间</td>
                <td><input type="text" name="start_time" value="<{$result.start_time}>" id="start_time"  style="width:200px;"> <span style=" margin-left:5px;"></span></td>
            </tr>
            
             <tr>
                <td style="width:150px;">下架时间</td>
                <td><input type="text" name="end_time" value="<{$result.end_time}>" id="end_time"  style="width:200px;"> <span style=" margin-left:5px;"></span></td>
            </tr>
           
           
           
        
            <tr>
                <td style="width:150px;">添加物品</td>
                <td>
                    <table class="itemTable" style="width:600px;">
                        <thead>
                            <tr>
                                <th style="width:150px;">物品ID</th>
                                <th style="width:150px;">上架价格</th>
                                <th style="width:150px;">数量</th>
                                <th  colspan="2">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        <{if $result.content != '' }>
                            <{foreach from=$result.content key=key  item=content }>
                                <tr class="tr_t itemWidget<{$key}>">
                                    <td style="width:150px;"><input type="text" num="<{$key}>" name="item_id[<{$key}>]" style="width:150px;" class="item_id" autocomplete="on" value="<{$content.item_id}>"></td>
                                    <td style="width:150px;"><input type="text" name="item_price[<{$key}>]" style="width:150px;" class="item_name" autocomplete="on" value="<{$content.item_price}>"></td>
                                    <td style="width:150px;"><input type="text" name="item_count[<{$key}>]" value="<{$content.item_count}>" style="width:150px;" class="item_count"></td>
                                    <td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>
                                    <td><input type="button" value="复制" num="<{$key}>" class="gbutton copyitem" title="复制该行"></td>
                                </tr>
                            <{/foreach}>
                            
                        <{/if}>

                        </tbody>
                    </table>
                    <input type="button" value="添加" class="gbutton" id="additem" title="添加填写一个物品的行">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="发送" id="charge">&nbsp;&nbsp;
                    <input type="hidden" id="error" value="1">&nbsp;&nbsp;
                    <input type="hidden" id="status" name="status" value="<{$type}>">
                    <input type="hidden" id="id" name="id" value="<{$id}>">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<{else}>
请选择编辑的模版
<{/if}>
<script type="text/javascript">
    $(function(){
		
		allitem ='';
		$.post('admin.php?ctrl=dealgroup&act=getAllItem','',function(data){
			
			allitem = data;
			$( ".item_id" ).autocomplete({
      			source: allitem
    		});
			
					
		},'json');
		
        var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
        $('#start_time').datetimepicker(timepickerlang);
		$('#end_time').datetimepicker(timepickerlang);

            num ='<{$num}>';
            
            $('#additem').click(function(){
              
            var itemWidget = '<tr class="tr_t itemWidget'+num+'">';
            itemWidget += '<td style="width:150px;"><input type="text" num="'+num+'" name="item_id['+num+']" style="width:150px;" class="item_id" autocomplete="on" value=""></td>';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_price['+num+']" style="width:150px;" class="item_name" autocomplete="on" value=""></td>';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_count['+num+']" value="1" style="width:150px;" class="item_count"></td>';
            itemWidget += '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
            itemWidget += '<td><input type="button" value="复制" num="'+num+'" class="gbutton copyitem" title="复制该行"></td>';
            itemWidget += '</tr>';
            
            $('.itemTable').children('tbody').append(itemWidget);
            num =  num +1;
			$( ".item_id" ).autocomplete({
      			source: allitem
    		});
          
        });

        $('table').delegate('.delitem' , 'click' , function(event){
            
            $(this).parent().parent().remove();
            
        });

        $('.itemTable').on("click",'.copyitem',function(event){
          //$td= $(this).parents('.tr_t').html();
          //$tr="<tr class='tr_t itemWidget"+num+"''>"+$td+"</tr>"
          
            var number = $(this).attr('num');
            var item_id  = $("input[name='item_id["+number+"]']" ) .val();
            var item_price = $("input[name='item_price["+number+"]']" ) .val();
            var item_count = $("input[name='item_count["+number+"]']" ) .val();
                
            var itemWidget = '<tr class="tr_t itemWidget'+num+'">';
            itemWidget += '<td style="width:150px;"><input type="text" num="'+num+'" name="item_id['+num+']" value="'+item_id+'" style="width:150px;" class="item_id" autocomplete="on"></td>';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_price['+num+']" value="'+item_price+'" style="width:150px;" class="item_name" autocomplete="on"></td>';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_count['+num+']" value="'+item_count+'" style="width:150px;" class="item_count"></td>';
            itemWidget += '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
            itemWidget += '<td><input type="button" value="复制" num="'+num+'" class="gbutton copyitem" title="复制该行"></td>';
            itemWidget += '</tr>';
          
           $(this).parents('.tr_t').after(itemWidget);
           num =  num +1;          
            
        }); 
    
        var error = $('#error');
        $('#manual-mail-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=dealgroup&act=tpl_add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                })
            }
            return false;
        });
    });

    function form_check(){
        var error = $('#error');
        var title = $('#title');
        if(title.val() == ''){
            $.dialog.tips('模版名称不能为空');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
		var start_time = $('#start_time').val();
		
		var end_time = $('#end_time').val();
		
		
		if(start_time != '' && end_time !=''  ){
			var start_time = Date.parse(new Date(start_time))/1000;
			var end_time = Date.parse(new Date(end_time))/1000;
			if(end_time < start_time){
				$.dialog.tips('下架时间不能小于上架时间');
				error.val(1);
				return false;
			}else{
				error.val(0);
			}
		}else{
			error.val(0);	
		}

       $('.item_id').each(function(){
			
			var item_id  = $(this).val();
			var num = $(this).attr('num');
			var item_count = $('.item_count').eq(num).val();
			
			var item_name = $('.item_name').eq(num).val();
			
			reg=/^1.*/;
			icon = /^399.*/;
			if(reg.test(item_id) && item_count >1){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID'+item_id +'为装备物品，数量只能为1');
            	error.val(1);
            	return false;	
			}else if(icon.test(item_id)){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID'+item_id +'为货币，不能上架');
            	error.val(1);
            	return false;
			}else if(item_id.substr(8,1) != 0){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID'+item_id +'为绑定物品，不能上架');
            	error.val(1);
            	return false;
			}else if(item_id ==''){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，物品ID不能为空');
            	error.val(1);
            	return false;
			}else if(item_name =='' || item_name == 0){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，价格不能为空或者为0');
            	error.val(1);
            	return false;
			}else if(item_count ==''){
				$.dialog.tips('第'+(parseInt(num)+1)+'个物品，数量不能为空或者为0');
            	error.val(1);
            	return false;
			}else{
				error.val(0);	
			}
		});

    }

       
   
</script>