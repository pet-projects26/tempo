<script type="text/javascript" src="templates/itemconfig/ajaxfileupload.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	
    <form action="" class="fm" id="manual-mail-form">
        <table class="itable itable-color">
            <tbody>
            
           <tr>
                <td style="width:150px;">注释:</td>
                <td>
                	1.导入时会先清除同种类型的道具模板,再重新导入新的。2.文件格式:excel表格,第一行为:道具id,价格,数量。3.若导入成功,模板那里没有显示道具数据,则请检查excel文件,重新导入
                </td>
            </tr>
           <tr>
                <td style="width:150px;">道具模板类型:</td>
                <td>
                	<select id="item_tpl" name="item_tpl">
                	    <{foreach from=$item_tpl item=item key=key}>
                    	   <option value="<{$key}>"><{$item}></option>
                    	<{/foreach}>
                    </select> <input type="button" class="gbutton" value="导出" id="export" style="margin-left:20px;">
                </td>
            </tr>
           <tr>
                <td style="width:150px;">批量导入物品</td>
                <td>
                	<div data-role="fieldcontain" class="upload-box">
                   	 <input type="file" id="file" name="file" value="上传" />        
                   	 <input type="button"  value="提交" id="button"  class="gbutton" />    
        			</div>
                </td>
            </tr>
           
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
   $('#button').click(function(){
		var file = $('#file').val();
		if(file == ''){
			 $.dialog.tips('请选择要上传的文件');return false;	
		}
		var item_tpl = $('#item_tpl').val();
		if(item_tpl == ''){
			 $.dialog.tips('请选择道具模板类型');return false;	
		}

		$.ajaxFileUpload({
	        url:'admin.php?ctrl=dealgroup&act=uploadCsv&item_tpl='+item_tpl,   //处理文件的脚本路径
	        type: 'post',       //提交的方式
	        secureuri :false,   //是否启用安全提交
	        fileElementId :'file',     //file控件ID
	        dataType : 'json',  //服务器返回的数据类型      
	        success : function (data){  //提交成功后自动执行的处理函数
	        	 $.dialog.tips(data.msg);
	        },
	        error: function(data){   //提交失败自动执行的处理函数
	            $.dialog.tips(data.msg);
	        }
	    })
	});
	
	
	 $('#export').click(function(){
		var item_tpl = $('#item_tpl').val();
		if(item_tpl == ''){
			 $.dialog.tips('请选择道具模板类型');return false;	
		}
		window.location.href = 'admin.php?ctrl=dealgroup&act=export&item_tpl='+item_tpl;
	});
</script>