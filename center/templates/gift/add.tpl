<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="gift-add-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>

            <{include file='../plugin/channelGroup_server.tpl' }>

            <!--<tr>
                <td style="width:150px;">礼包名称</td>
                <td><input type="text" name="name" id="name" style="width:300px;"></td>
            </tr>
            <tr>
                <td style="width:150px;">激活提示</td>
                <td>
                    <textarea name="tips" id="tips" style="width:600px;height:70px;margin:0;"></textarea>
                </td>
            </tr> -->
            <tr>
                <td style="width:150px;">新服自动同步</td>
                <td><input type="radio" class="checkbox" name="issync[]" value="1">是
                    |
                    <input type="radio" class="checkbox" name="issync[]" value="0" checked>否
                </td>
            </tr>
            <tr>
                <td style="width:150px;">说明</td>
                <td><input type="text" name="info" id="info" style="width:600px;"></td>
            </tr>
            <tr>
                <td style="width:150px;">添加物品</td>
                <td>
                    <table class="itemTable" style="width:600px;">
                        <thead>
                        <tr>
                            <th>物品ID/物品名称</th>
                            <th>数量</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <input type="button" value="添加" class="gbutton" id="additem" title="添加填写一个物品的行">
                </td>
            </tr>
            <tr>
                <td style="width:150px;">生效时间</td>
                <td><input type="text" name="open_time" id="open_time" class="datepicker"></td>
            </tr>
            <tr>
                <td style="width:150px;">过期时间</td>
                <td><input type="text" name="end_time" id="end_time" class="datepicker"></td>
            </tr>
            <tr>
                <td style="width:150px;">兑换个数/使用次数</td>
                <td><input type="text" name="times" id="times" value="1"></td>
            </tr>
            <tr>
                <td style="width:150px;">生成个数</td>
                <td><input type="text" name="num" id="num"></td>
            </tr>
            <!--
            <tr>
                <td style="width:150px;">追加生成个数</td>
                <td><input type="text" name="pnum" id="pnum"></td>
            </tr>
            -->
            <tr>
                <td style="width:150px;">重复兑换</td>
                <td>
                    <select id="type" name="type">
                        <{foreach from = $giftCDKEYType key = key item = rows}>
                        <option value="<{$key}>"><{$rows}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input id="add_action" type="submit" class="gbutton" value="添加"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        //获取物品配置
        _goods ='';
        $.post('admin.php?ctrl=gift&act=getGoodsItem','',function(data){
            _goods = data;
        },'json');


        var timepickerlang = { timeText:'时间' , hourText:'小时' , minuteText:'分钟' , currentText:'现在' , closeText:'确定' }
        $('.datepicker').datepicker(timepickerlang);

        var itemWidget = '<tr class="itemWidget">';
        itemWidget += '<td><input type="text" name="item_id[]" style="width:250px;" class="item_id" autocomplete="on"></td>';
        itemWidget += '<td><input type="number" name="item_count[]" value="1" style="width:150px;" class="item_count"></td>';
        itemWidget += '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
        itemWidget += '</tr>';
        $('#additem').click(function(){
            $('.itemTable').children('tbody').append(itemWidget);

            $( ".item_id" ).autocomplete({
                source: _goods
            });
        });

       

        $('#filter').click(function(){
            if($('#id').val() == 0){
                $.dialog.tips('未选择渠道');
                return false;
            }
        });

        $('#id').change(function(){
            var id = $(this).val();
            if(id != 0){
                $.ajax({
                    url: 'admin.php?ctrl=gift&act=scp_action',
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'html'
                }).done(function(html){
                    //autocomplete();
                    $('#server_list').html(html);
                    //Custombox.close();
                });
            }
        });


        $('table').delegate('.delitem' , 'click' , function(event){
            $(this).parent().parent().remove();
        });

        var error = $('#error');
        $('#gift-add-form').submit(function(event){
            form_check();

            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=gift&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    beforeSend:function(){
                        $("#add_action").attr({disabled:"disabled"});
                    },
                    complete:function(){
                        $("#add_action").removeAttr('disabled');
                    }
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if (data.status === 200) {
                        window.location.reload();
                    }
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        /*
        //礼包名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写礼包名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //激活提示
        var tips = $('#tips').val();
        if(tips == ''){
            $.dialog.tips('未填写激活提示');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }*/
        //说明
        var info = $('#info').val();
        if(info == ''){
            $.dialog.tips('未填写说明');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //生效时间
        var open_time = $('#open_time').val();
        if(open_time == ''){
            $.dialog.tips('未填写生效时间');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //过期时间
        var end_time = $('#end_time').val();
        if(end_time == ''){
            $.dialog.tips('未填写过期时间');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

    }
</script>