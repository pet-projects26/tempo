<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><{$web_name}></title>
    <link href="style/css/popover.css" rel="stylesheet"/>
    <link href="style/images/favicon.ico" type="image/x-icon" rel="icon">
    <link href="style/css/admin.css?ver=20130815" type="text/css" rel="stylesheet">
    <link href="style/jquery.qtip.custom/jquery.qtip.min.css" rel="stylesheet">
    <link href="style/css/smoothness/jquery-ui-1.8.23.custom.css" type="text/css" rel="stylesheet">
    <link href="style/css/demo_page.css" type="text/css" rel="stylesheet">
    <link href="style/css/demo_table.css" type="text/css" rel="stylesheet">
    <link href="style/css/jquery-select2.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style/jqueryTag/jquery.tag-editor.css">
    <link href="style/skin/skin.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style/custombox/css/custombox.min.css">
    <link rel="stylesheet" type="text/css" href="style/custombox/css/demo.css">
</head>
<body>
<div id="container">
    <div id="header">
        <div class="cl overflow" id="top"></div>
        <div class="cl" id="nav">
            <div class="left">
                <span id="title-name"><{$game_name}></span>
                <span id="retractmenu">收起菜单</span>
                <span id="togglemenu">隐藏菜单</span>
                <span id="allmenu">功能地图</span>
                <span id="logout" url="<{$login_url}>">退出登录</span>
            </div>
            <div class="right">
                <span>用户：  <{$_username}></span>
            </div>
        </div>
    </div>
    <div id="content">
        <div id="viewport-left">
            <div class="vl-box-1" id="analyse-basic">
                <{foreach from=$menuTree item=menu}>
                <div class="vl-box-2">
                    <{if $menu.url != ''}>
                    <div class="box-2-title top-menu-has-url click-menu" url="<{$menu.url}>"
                         id="menu_taxonomy_<{$menu_sub.id}>"><span><{$menu.name}></span></div>
                    <{else}>
                    <div class="box-2-title"><span><{$menu.name}></span></div>
                    <{/if}>
                    <ul>
                        <{foreach from=$menu.children item=menu_sub}>
                        <li><a class="click-menu" href="#" title="<{if $menu_sub.url}><{$menu_sub.url}><{/if}>"
                               url="<{if $menu_sub.url}><{$menu_sub.url}><{/if}>" id="menu_taxonomy_<{$menu_sub.id}>"
                               onclick="javascript:void(0);">
                                <{$menu_sub.name}>
                            </a></li>
                        <{/foreach}>
                    </ul>
                </div>
                <{/foreach}>
            </div>
        </div>
        <div id="main-content"></div>
    </div>
</div>
<div style="clear: both;"></div>
<div id="footer"></div>
<div id="top-back"></div>
<!-- 功能地图 -->
<div id="opacity"></div>
<div class="none" id="function-map">
    <{foreach from=$menuTree item=i}>
    <li>
        <span class="map-tit"
        <{if $i.url != ''}>style="cursor:pointer" onclick="$('#menu_taxonomy_<{$i_sub.id}>').click()"<{/if}>>
        <{$i.name}>
        </span>
        <{foreach from=$i.children item=i_sub}>
        <span class="map-con">
      <a class="map-menu" href="#" onclick="$('#menu_taxonomy_<{$i_sub.id}>').click()"
         url="<{if $i_sub.url}><{$i_sub.url}><{/if}>" id="menu_taxonomy_<{$i_sub.id}>"><{$i_sub.name}></a>
      </span>
        <{/foreach}>
    </li>
    <{/foreach}>
</div>
</button>
<script type="text/javascript" src="style/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="style/js/jquery-ui-1.8.23.custom.min.js"></script>
<!-- style/js/jquery-ui-1.8.23.custom.min.js -->
<script type="text/javascript" src="style/js/jquery.cookie.js"></script>
<!-- jquery-ui-timepicker-addon.js使日期选择精确到时间选择 -->
<script type="text/javascript" src="style/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="style/js/jquery.ui.datepicker-zh-CN.js"></script>
<!-- jquery.dataTables.min.js表格插件 -->
<script type="text/javascript" src="style/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="style/js/dataTables.plugins.js"></script>
<!-- jquery.jeditable.js可实现点击页面元素直接编辑 -->
<script type="text/javascript" src="style/js/jquery.jeditable.js"></script>
<!-- lhgdialog.min.js 实现弹出框效果-->
<script type="text/javascript" src="style/js/dialog/lhgdialog.min.js?skin=skin&self=true"></script>
<!-- highcharts.js charts图表-->

<!--<script type="text/javascript" src="style/js/highcharts/highcharts.js"></script>-->
<script type="text/javascript" src="style/js/highcharts/highcharts5.js"></script>
<script type="text/javascript" src="style/js/highcharts/modules/exporting.js"></script>

<!-- jquery.form.js 实现表单的ajax-->
<script type="text/javascript" src="style/js/jquery.form.min.js"></script>
<script type="text/javascript" src="style/js/common.js"></script>
<script type="text/javascript" src="style/js/FusionCharts.min.js"></script>
<script type="text/javascript" src="style/layer/layer.min.js"></script>
<script type="text/javascript" src="style/jquery.qtip.custom/jquery.qtip.min.js"></script>
<script type="text/javascript" src="style/custombox/js/custombox.min.js"></script>
<script type="text/javascript" src="style/custombox/js/legacy.min.js"></script>
<script type="text/javascript" src="style/jqueryTag/jquery.tag-editor.min.js"></script>
<script type="text/javascript" src="style/js/jquery-select2.min.js"></script>
<script type="text/javascript" src="style/js/validform.min.js"></script>
<script type="text/javascript" src="style/js/popover.js"></script>
<script type="text/javascript" src="style/js/msgpack.min.js"></script>
<!--<script type="text/javascript" src="style/js/websocket.js"></script>-->
<script type="text/javascript" src="style/js/jQuery-gDialog/jquery.gDialog.min.js"></script>
<script>
    $(function () {
        $(".tb_tips_popover").manhua_hoverTips({position : "b"});//改变了显示的位置参数
    })
</script>
</body>
</html>
