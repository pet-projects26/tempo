

<div id="pie"><{$pie}></div>

<div style="padding:5px">
  <table cellpadding="0" cellspacing="0" border="0" class="table_list dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
    <thead>
      <tr>
        <th rowspan="1" colspan="1">来源</th>
        <th rowspan="1" colspan="1">消耗金额</th>
        <th rowspan="1" colspan="1">消耗人数</th>
        <th rowspan="1" colspan="1">消耗占比</th>
      </tr>
    </thead>
    <tbody>
     
       <{foreach from=$detail item=item key=key }>
       <{if $key % 2 ==0 }> <tr class="odd"> <{else}> <tr class="even"><{/if}>
       
        <td><{$item.coin_source}></td>
        <td><{$item.sum_coin}></td>
        <td><{$item.num}></td>  
        <td><{$item.coin_percent}></td>
        </tr>
       <{/foreach}>
      
    </tbody>
  </table>
</div>