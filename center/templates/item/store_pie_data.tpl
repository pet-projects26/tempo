

<div id="pie"><{$pie}></div>

<div style="padding:5px">
  <table cellpadding="0" cellspacing="0" border="0" class="table_list dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
    <thead>
      <tr>
        <th rowspan="1" colspan="1">来源</th>
        <th rowspan="1" colspan="1">数量</th>
        <th rowspan="1" colspan="1">人数</th>
        <th rowspan="1" colspan="1">占比</th>
      </tr>
    </thead>
    <tbody>
     
       <{foreach from=$detail item=item key=key }>
        <tr class="odd"> 
       
        <td><{$item.item_id}></td>
        <td><{$item.item_num}></td>
        <td><{$item.role_num}></td>  
        <td><{$item.percent}></td>
        </tr>
       <{/foreach}>
      
    </tbody>
  </table>
</div>