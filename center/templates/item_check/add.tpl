<script type="text/javascript" src="templates/item_check/activity_1008.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-add-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
            </div>
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 元宝数量:只包括拍卖<br>
                        2. 绑元数量:不包括内部充值卡和充值赠送的绑元<br>
                        3. 内部元宝:只包括内部充值卡充值<br>
                        
                    </td>
                </tr>
                 <!-- 服务器, 渠道 Start -->
        			<{include file='../plugin/channelGroup_server.tpl' }>
        		 <!-- 服务器，  -->
               
                <tr>
                    <td style="width:150px;">扫描间隔</td>
                    <td>4个小时</td>
                </tr>
                <tr>
                    <td style="width:150px;">监控物品</td>
                   <td>  
       				 <table style="width:100%;" class="table-activity-reward">
            		<thead>
           				 <tr>
                             <td>元宝数量:<input type="text" name="gold[]" style="width:100px;"></td>
                         </tr>
           				 <tr>
                             <td>绑元数量:<input type="text" name="gold[]" style="width:100px;"> </td>
                         </tr>
           				 <tr>
                             <td>内部元宝:<input type="text" name="gold[]" style="width:100px;"> </td>
                         </tr>
            		</thead>
            <!--		<tbody class="body-item">
            		<tr>
               		 <td>
                    	<table>
                    	<tbody>
                    	<tr>
                        <td>
                            <table class="itemTable">
                            <thead>
                                <tr>
                                    <td>物品ID</td>
                                    <td>数量</td>
                                    <td>报警人数</td>
                                    <td>操作</td>
                                </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                            </table>
                          </td>
                         </tr>
                     </tbody>
                    </table>
                    <input type="button" value="添加物品条件" class="gbutton c-additem" add_item_num="0" add_amount_num="0" add_grade_num="1">
                  </td>
                </tr>
              </tbody> -->
           </table>
           </td>
           </tr>
                <tr>
            		<td >设置邮箱(多个时请以逗号,分割)</td>
            		<td>
                		<input type="text" name="mail" style="width:600px;">
            		</td>
        		</tr>
                <tr>
            		<td >邮件内容</td>
            		<td>
            			<input type="text" name="title" style="width:200px;">
                		<textarea name="content" style="width:400px;height:100px;" >在{$server}中,玩家:{$role_name}的{$type}在{$time}时间内产出{$count}个,超出报警值,请马上跟进。</textarea><span style="color:red;">内容请不要更改</span>                  
            		</td>
        		</tr>
                <tr id="white">
            		<td >白名单   <input type="button" value="添加"  class="gbutton xx_data_add" /> <input type="button" value="删除"  class="gbutton xx_data_reduce" /></td>
            		<td>
            		<div class="white_btn">
            		  <p>
            			<select name="server_white[]" class="server_white" >
            				<option value="">选择服务器</option>
            				 <{foreach from=$serverData key=key item=item}>
                            	<option value="<{$key}>"><{$item}></option>
                            <{/foreach}>
            			</select>
            			角色名:<input type="text" name="role_white[]" style="width:300px;"><span style="color:red;">(多个时请以逗号,分割)</span>
            		  </p>
            		 </div>
            		</td>
        		</tr>
        		
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
    
       $("#white").delegate(".xx_data_add","click",function(a){
        		$(this).parent().parent().find(".white_btn:last").append('<p><select name="server_white[]" class="server_white" ><option value="">选择服务器</option><{foreach from=$serverData key=key item=item}><option value="<{$key}>" ><{$item}></option><{/foreach}></select>角色名:<input type="text" name="role_white[]" style="width:300px;" value = ""><span style="color:red;">(多个时请以逗号,分割)</span></p>');
        		$('.server_white').select2();
        })
        
       $(".xx_data_reduce").click(function(){
       		var $elements = $(this).parent().parent().find(".white_btn").find("p");
    		var len = $elements.length;
    		if(len < 2){
    			alert("不可删除");
    			return false;
    		}else{
    			$(this).parent().parent().find(".white_btn").find("p:last").remove();
    		}
       })
    
    
    
    $('.server_white').select2();
       
        $('#channel-add-form').submit(function(event){
                $.ajax({
                    url: 'admin.php?ctrl=item_check&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
          
            return false;
        });
    });
    
</script>