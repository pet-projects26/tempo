
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="add-form">
    <div class="hidden">
      <input type="hidden" id="error" value="1">
    </div>
    <table class="itable itable-color">
      <tbody>
      
         <tr>
            <td style="width:150px">绑定ID</td>
            <td><input type="text" name="bind_item_id" id="bind_item_id"></td>
        </tr>
        
        <tr>
            <td style="width:150px">不绑定ID</td>
			<td>
                <input type="text" name="item_id" id="item_id">
			</td>
        </tr>

        <tr>
            <td style="width:150px">道具</td>
            <td><input type="text" name="item_name" id="item_name" ></td>
        </tr>

        <tr>
            <td style="width:150px">道具价值</td>
            <td><input type="text" name="item_value" id="item_value" ></td>
        </tr>

        <tr>
            <td style="width:150px">描述</td>
            <td><textarea name="item_info" id="item_info"></textarea></td>
        </tr>

        <tr>
            <td style="width:150px">非活动的产出途径</td>
            <td><textarea name="item_produce" id="item_produce"></textarea></td>
        </tr>

        <tr>
            <td style="width:150px">投放方式建议</td>
            <td><textarea name="item_advice" id="item_advice"></textarea></td>
        </tr>
        
        <tr>
          <td colspan="2"><input type="button" class="gbutton" value="添加">
          </td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
<script type="text/javascript">
$(function(){

    $('.gbutton').click(function(){

        $.ajax({
            url: 'admin.php?ctrl=itemconfig&act=add_action',
            type: 'POST',
            dataType: 'JSON',
            data: $('#add-form').serialize()
        }).done(function(data){

            $.dialog.tips(data.msg);
            if(data.code == 1){
                $tabs.tabs('select',0);
                $tabs.tabs('load' , 0);
            }
        })
    })
        
});
 	
</script>