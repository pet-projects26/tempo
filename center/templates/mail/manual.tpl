<style type="text/css">
    .p-input {
        vertical-align:middle;
    }

    .p-text {
        vertical-align:middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }

    .role_type {vertical-align:middle; margin-top:-2px; margin-bottom:1px;}

    .send_type {vertical-align:middle; margin-top:-2px; margin-bottom:1px;}

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-mail-form">

        <table class="itable itable-color">
            <tbody>
            <tr>
                <td colspan="2">
                    <div style="margin:15px 0;">
                        说明：<br>
                        1. 在添加货币一行，清空按钮的作用是清空 同时清空铜钱数量，元宝数量，绑定元宝数量三个输入框<br>

                        2. 发件人名称 和 标题名称 的中文字符和标点符号等加起来不能大于13个
                    </div>
                </td>
            </tr>

            <tr>
                <td style="width:150px;">填写收件人</td>
                <td>
                    <label style="cursor:pointer;"><input type="radio" name="role_type" class="role_type" value="0" checked>所有人</label>
                    <label style="cursor:pointer;"><input type="radio" name="role_type" class="role_type" value="1">指定角色ID</label>
                    <label style="cursor:pointer;"><input type="radio" name="role_type" class="role_type" value="2">指定账号名</label>
                    <br>

                    <textarea name="role" id="role" rows="15" cols="100" ></textarea>
                </td>
            </tr>

            <!-- 主要用于进行一个颜色错位 -->
            <tr ></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server.tpl' }>
            <!-- 服务器，  -->



            <!-- 服务器 Start  -->
            <tr id="server" style="display: none;">
                <td class="w-150">服务器</td>
                <td>

                    <{foreach from = $servers key = group_id  item = rows }>
                    <fieldset>
                        <legend> <{$groups[$group_id]}> </legend>
                        <{foreach from = $rows  key = sign  item = name}>
                        <span class="p-span s-server-click" >
                                <input class="p-input"  type="checkbox" name="server[]"  value="<{$sign}>" id="server-<{$sign}>" />
                                <span for="server-<{$sign}>" class="p-input" ><{$name}></span>
                            </span>
                        <{/foreach}>
                    </fieldset>
                    <{/foreach}>
                </td>
            </tr>
            <!-- 服务器 End -->


            <!-- 主要用于进行一个颜色错位 -->
            <tr ></tr>
            <!-- 主要用于进行一个颜色错位 -->


            <tr>
                <td style="width:150px;">填写发件人名称</td>
                <td><input type="text" name="sender" id="sender" style="width:200px;"></td>
            </tr>
            <tr>
                <td style="width:150px;">填写邮件标题</td>
                <td><input type="text" name="title" id="title" style="width:600px;"></td>
            </tr>
            <tr>
                <td style="width:150px;">填写邮件文本内容</td>
                <td><textarea name="content" id="mcontent" style="width:600px;height:200px;margin:0;"></textarea></td>
            </tr>
            <tr>
                <td style="width:150px;">添加资源</td>
                <td>
                    <table class="moneyTable" style="width:600px;">
                        <thead>
                        <tr>
                            <th style="width:150px;">资源</th>
                            <th style="width:150px;">数量</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <{foreach from = $mailSource key = source_key  item = rows }>
                        <tr>
                            <td><{$rows.name}></td>
                            <td style="width:150px;"><input type="number" name="source[<{$source_key}>]" id="<{$rows.keyName}>" class="source" style="width:150px;" value="0"></td>
                            <td><input type="button" class="gbutton reset" value="清空" data-value="<{$rows.keyName}>" ></td>
                        </tr>
                        <{/foreach}>
                        </tbody>
                    </table>
                </td>
            <tr>
                <td style="width:150px;">添加物品</td>
                <td>
                    <table class="itemTable" style="width:600px;">
                        <thead>
                        <tr>
                            <th>物品ID/物品名称</th>
                            <th>数量</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <input type="button" value="添加" class="gbutton" id="additem" title="添加填写一个物品的行">
                </td>
            </tr>
            <tr>
                <td style="width:150px;">定时发送</td>
                <td>
                    <label style="cursor:pointer;"><input type="radio" name="send_type" class="send_type" value="0" checked>不定时</label>
                    <label style="cursor:pointer;"><input type="radio" name="send_type" class="send_type" value="1">定时</label>
                    <br>
                    <span id="book_time_span" style="display: none;">预定时间:<input value="" type="text" name="book_time" id="book_time"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="发送" id="charge">&nbsp;&nbsp;<input type="hidden" id="error" value="1"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">

    $(function(){
        num=0;
        allitem ='';
        $.post('admin.php?ctrl=mail&act=getGoodsItem','',function(data){
            allitem = data;
        },'json');


        var timepickerlang = { timeText:'时间' , hourText:'小时' , minuteText:'分钟' , currentText:'现在' , closeText:'确定' };
        $('#book_time').datetimepicker(timepickerlang);


        $('table.itable tr td ,table input ,table select').css('margin-top' , '3px').css('margin-bottom' , '3px');
        $('#role').hide();

        $('.role_type').click(function(){
            $('.p-input').attr('checked' , false);
            $('.p-span').removeClass('p-checked');
            if ($(this).val() != 0) { //单选
                $('#role').show();
                $('#role').focus();
                $('.p-tr').hide();//隐藏渠道组
                $('#server').show();
            }
            else{ //多选
                //渠道组默认勾选全部
                $('.c-channel-group').eq(0).addClass('p-checked');
                $('.c-channel-group .p-input').eq(0).attr('checked' ,true);

                //服务器默认勾选全部
                $('.p-tr').eq(1).find('span').eq(0).addClass('p-checked');
                $('.p-tr').eq(1).find('span').eq(0).find('.p-input').attr('checked' ,true);

                $('#role').hide();
                $('.p-tr').show();//显示渠道组
                $('#server').hide();
            }
        });

        $('.send_type').click(function () {
            if($(this).val() == '1'){ //单选
                $('#book_time_span').show();
            } else {
                $('#book_time_span').hide();
            }
        });


        //指定角色id，服只能单选
        $('#server .p-input').click(function(){
            if(!$(this).hasClass('p-checked')){
                $('.p-input').attr("checked", false);
                $('.p-span').removeClass('p-checked');
                $(this).parent().addClass('p-checked');
                $(this).attr("checked", true);
                $(this).prev().attr('checked',true)

            }
            else{
                $(this).attr("checked", true);
                $(this).prev().attr("checked", false);

                $(this).parent().removeClass('p-checked');
            }

        });

        $('.reset').click(function(){

            var key = $(this).attr('data-value');

            $('#'+key).val(0);
        });

        $('#additem').click(function(){
            var itemWidget = '<tr class="itemWidget">';
            itemWidget += '<td style="width:150px;"><input type="text" name="item_id['+ num +']" style="width:150px;" class="item_id" id="item_id_'+num+'" autocomplete="on" num ="'+num+'"></td>';
            itemWidget += '<td style="width:150px;"><input type="number" name="item_count['+ num +']" value="1" style="width:150px;" class="item_count"></td>';
            itemWidget += '<td><input type="button" value="删除" class="gbutton delitem" title="删除该行"></td>';
            itemWidget += '</tr>';

            num = num+1;

            $('.itemTable').children('tbody').append(itemWidget);
            num = num + 1;
            $( ".item_id" ).autocomplete({
                source: allitem
            });
        });


        $('table').delegate('.delitem' , 'click' , function(event){
            num = num - 1;
            if (num < 0) num = 0;
            $(this).parent().parent().remove();
        });

        var error = $('#error');
        $('#manual-mail-form').submit(function (event) {
            form_check();
            if (error.val() == '1') {
                return false;
            }
            else {
                if(confirm('确定发送邮件？')) {
                    $.ajax({
                        url: 'admin.php?ctrl=mail&act=server_send_mail_action&save=1',
                        type: 'POST',
                        dataType: 'JSON',
                        data: $(this).serialize()
                    }).done(function (data) {
                        $.dialog.tips(data.msg);
                    })
                }
            }
            return false;
        });
    });

    function form_check(){

        var error = $('#error');
        //服务器
        /* var server = $('#server');
         if(server.val() == 0){
             $.dialog.tips('未选择服务器');
             error.val(1);
             return false;
         } else {
             error.val(0);
         }*/
        //角色ID
        var role = $('#role');
        var role_type = $('input[name="role_type"]:checked').val();

        var role_id = role.val();

        if (role_type == 1 || role_type == 2) {
            if (role_id == '') {
                $.dialog.tips('未填写收件人');
                error.val(1);
                return false;
            }

            /*if (isNaN(role_id)) {
                $.dialog.tips('角色ID不能为字符串');
                error.val(1);
                return false;
            }*/
        } else {
            error.val(0);
        }

        //发件人名称
        var sender = $('#sender');
        if(sender.val() == ''){
            $.dialog.tips('未填写发件人');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //邮件标题
        var title = $('#title');
        if(title.val() == ''){
            $.dialog.tips('未填写邮件标题');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //邮件内容
        var mcontent = $('#mcontent');
        if(mcontent.val() == ''){
            $.dialog.tips('未填写邮件内容');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        $(".source").each(function () {

            var source_count = $(this).val();

            if(isNaN(source_count)){
                $.dialog.tips('资源数量只能为数字');
                error.val(1);
                return false;
            }else{
                error.val(0);
            }
        });

        $(".item_id").each(function() {
            var item_id  = $(this).val();
            var num = $(this).attr('num');
            var item_count = $('.item_count').eq(num).val();
            console.log(item_id);
            console.log(num);
            console.log(item_count);
            if(item_id == ''){
                $.dialog.tips('物品不能为空');
                error.val(1);
                return false;
            }else{
                error.val(0);
            }
        });

        var book_time = $('#book_time').val();
        var send_type = $('input[name="send_type"]:checked').val();
        if(book_time == '' && send_type == 1){
            $.dialog.tips('请输入预定发送的时间');
            return false;
        }
    }
</script>