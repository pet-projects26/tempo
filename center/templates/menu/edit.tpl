<form method="post" class="form_menu_edit" action="admin.php?ctrl=menu&act=edit_submit&type=<{$type}>">
    <table class="itable itable-color">
      <tr id="show_id" style="<{if !$menu.id}>display:none<{/if}>;">
          <td>菜单id</td>
          <td>
          <input value="<{$menu.id}>" name="id" readonly="true"/>
          </td>
        </tr>
    <tr>
          <td>菜单名称</td>
          <td>
          <input type="text" value="<{$menu.name}>" name="name"/>
          </td>
        </tr>
        
        <tr>
          <td>链接地址</td>
          <td><input class="width-middle" type="text" value="<{$menu.url}>" name="url" /></td>
        </tr>
        
        <tr>
          <td>上级菜单</td>
          <td><select name="parent">
          <option value="">根</option>
          <{foreach from=$menuRoot item=tmp}>
          <option value="<{$tmp.id}>"><{$tmp.name}></option>
          <{/foreach}>
          
          </select></td>
        </tr>
<tr>
          <td>排序</td>
          <td><input type="text" value="<{$menu.weight}>" name="weight"/></td>
        </tr>
<tr>
          <td><input type="hidden"  name="type" value="<{$type}>" /></td>
          <td><input type="submit" class="gbutton"  name="submit" value="<{'提交'}>" /></td>
        </tr>
       
      </table>
 </form>
 
<script type="text/javascript">
$('.form_menu_edit').ajaxForm({
    complete: function(xhr){
        var id = JSON.parse(xhr.responseText);
        if(id > 0){
            if($('#show_id input[name="id"]').val()){
                $.dialog.alert('修改成功');
            }
            else{
                $('#show_id input[name="id"]').val(id);
                $('#show_id').show();
                $.dialog.alert('添加成功');
            }
        }
        else if(id == '-1'){
        	 $.dialog.alert('操作失败失败');
        }
        else{
        	 $.dialog.alert('未知错误');
        }
    }
});
</script>