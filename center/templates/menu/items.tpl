<style type="text/css">
.sortable-list-sub-<{$type}>{margin:5px 12px 5px 0;}
.sortable-list-sub-<{$type}> td{width:17%;} 
h3 a{font-size:12px;font-weight:normal;}
a.tabledrag-handle{
    background: url("style/images/draggable.png") no-repeat scroll 6px 7px transparent;
    height: 13px;
    margin: -0.4em 0.5em;
    padding: 0.42em 0.5em;
    width: 13px;
	display:block;
	cursor:move;
}
a.tabledrag-handle:hover {
    background-position:  6px -13px;
}
ul.sortable-list-root-tree h3,ul.sortable-list-root-leaf h3{ height:24px; line-height:24px; border-bottom:solid 1px #BECBD6;}
.oper-parent span.oper{margin-left:40px; display:none;}
.oper-parent:hover span.oper{ display:inline;}
</style>

<ul class="sortable-list-root-<{$type}>">
<{foreach from=$menuTree item=menu}>
<li id="<{$menu.id}>">
<div id="menu_line_<{$menu.id}>" class="oper-parent">
	<h3><{$menu.name}>
	<span class="oper"><a href="javascript:void(0)" onclick="menu_edit(<{$menu.id}>)">[编辑]</a>
	&nbsp;&nbsp;<a href="javascript:void(0)" onclick="menu_delete(<{$menu.id}>)">[删除]</a></span>
	</h3>
	
	<ul class="sortable-list-sub-<{$type}>">
		<{foreach from=$menu.children item=menu_sub}>
		<li id="<{$menu_sub.id}>">
		<table class="itable itable-color" id="menu_line_<{$menu_sub.id}>"><tr>
		  <td style="width:2%;">
		  <a class="tabledrag-handle" href="#" title="拖放重新排序">
		  &nbsp;
		  </a></td>
		  <td><{$menu_sub.id}></td>
		  <td class="edit" id="<{$menu_sub.id}>" field="name"><{$menu_sub.name}></td>
		  <td style="width:35%;" class="edit" id="<{$menu_sub.id}>" field="url"><{$menu_sub.url}></td>
		  <td id="<{$menu_sub.id}>">
		  <a href="javascript:void(0)" class="change_status" status="<{$menu_sub.status}>" title="改变菜单状态">
		  <{if $menu_sub.status == $smarty.const.MENU_STATUS_SHOW}>显示
		  <{else}><span style="color:#ff0000;">隐藏</span><{/if}>
		  </a>
		  </td>
		  <td><a href="javascript:void(0)" onclick="menu_delete(<{$menu_sub.id}>)">点击删除</a></td>
		</tr></table></li>
		<{/foreach}>
	</ul>
</div>
</li>
<{/foreach}>
<ul>
<br/><br/>
<input type="button" class="gbutton" id="save_menus_<{$type}>" value="保存"/>
<script type="text/javascript">
$('.sortable-list-root-<{$type}>').sortable();
$('.sortable-list-sub-<{$type}>').sortable({
	connectWith: ".sortable-list-sub-<{$type}>"
}).enableSelection();

function menu_delete(id){
	$.dialog.confirm('确定删除菜单?', function(){
		$.ajax({
	        type: "POST",
	        url: "admin.php?ctrl=menu&act=delete",
	        data: 'id='+id,
	        timeout: 20000,
	        error: function(){$.dialog.alert('超时');},
	        success: function(result){
	            if(result == 'success'){
	            	$.dialog.tips('删除成功');
	            	$('#menu_line_'+id).remove();
	            }else{
	            	$.dialog.alert('删除失败');
	            }
	        }
	    });
	}, function(){
	    $.dialog.tips('取消操作');
	});
}
$('#save_menus_<{$type}>').click(function(){
	var menu_root_ids;
	var menu_sub_ids = new Array();
	$('.sortable-list-root-<{$type}>').each(function(){
		menu_root_ids = $(this).sortable('toArray')
    });
	$('.sortable-list-sub-<{$type}>').each(function(){
		menu_sub_ids.push($(this).sortable('toArray'));
	});
	$.ajax({
        type: "POST",
        url: "admin.php?ctrl=menu&act=save_weight&type=<{$type}>",
        data: 'menu_root_ids='+menu_root_ids+'&menu_sub_ids='+menu_sub_ids.join('|'),
        timeout: 20000,
        error: function(){alert('error');},
        success: function(result){
            if(result == 1) alert('修改成功');
        }
    });
});

$('.edit').editable("admin.php?ctrl=menu&act=save_field", { 
    indicator : "<img src='style/images/indicator.gif'>",
    height    : "15px",
    submit    : "修改",
    cancel    : "取消",
    tooltip   : "点击修改...",
    style  : "inherit"
});

$('.change_status').click(function(){
	var $this = $(this);
	var id = $(this).parent().attr('id');
	var value = $(this).attr('status')==1? 2: 1;
	if(confirm('更改菜单显示状态？')) {
		$.ajax({
			type: "POST",
			url: "admin.php?ctrl=menu&act=save_field",
			data: 'id=' + id + '&field=status' + '&value=' + value,
			timeout: 20000,
			error: function (){alert('error');},
			success: function (result) {
				$this.attr('status', result);
				if (result == '1') {
					$this.html('显示');
				}
				if (result == '2') {
					$this.html('<span style="color:#ff0000;">隐藏</span>');
				}
				window.location.reload();
			}
		});
	}
});
function menu_edit(id){
	$menu_tabs_<{$type}>.tabs( "url" , 1 , "admin.php?ctrl=menu&act=edit&id="+id );
    $menu_tabs_<{$type}>.tabs( "select" , 1 );
}
</script>