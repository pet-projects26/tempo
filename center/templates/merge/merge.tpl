<style type="text/css">
.p-input {
	vertical-align: middle;
}
.p-text {
	vertical-align: middle;
}
.p-span {
	padding: 5px 10px;
	margin: 0px 3px;
}
.w-150 {
	width: 150px;
}
fieldset {
	padding: 5px;
	border: 2px solid #cc0;
	margin: 10px 0px;
}
.p-checked {
	background: #ff0;
	border-radius: 3px;
}

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="form">
    <table class="itable itable-color">
      <tbody>
        
        <!-- 母服 Start  -->
        <tr id="parent">
          <td class="w-150" >母服</td>
          <td><{foreach from = $servers key = group_id  item = rows }>
            <fieldset>
              <legend> <{$groups[$group_id]}> </legend>
              <{foreach from = $rows  key = sign  item = name}> 
              <span class="p-span s-server-click" >
                  <input class="p-input"  type="checkbox" name="parent[]"  value="<{$sign}>" id="parent-<{$sign}>" />
                  <span for="parent-<{$sign}>" class="p-input" ><{$name}></span> 
              </span> 
              <{/foreach}>
            </fieldset>
            <{/foreach}> </td>
        </tr>
        <!-- 母服 End --> 
        
        <!-- 子服 Start  -->
        <tr id="children">
          <td class="w-150" >子服</td>
          <td><fieldset id="group">
              <legend> 渠道组</legend>
              <{foreach from = $groups key = id item = row}> 
              <span class="p-span c-channel-group  c-channel-group-list span-click">
                <input class="p-input"  type="checkbox" name="channel_group[]"  value="<{$id}>"  id="channel_group-<{$id}>" />
                <span for="channel_group-<{$id}>" class="p-input" ><{$row}></span>
              </span> <{/foreach}>
            </fieldset>
            <{foreach from = $servers key = group_id  item = rows }>
            <fieldset id="<{$group_id}>" class="child">
              <legend> <{$groups[$group_id]}> </legend>
              <{foreach from = $rows  key = sign  item = name}> <span class="p-span p-server" >
              <input class="p-input"  type="checkbox" name="server[]"  value="<{$sign}>" id="server-<{$sign}>" />
              <span for="server-<{$sign}>" class="p-input" ><{$name}></span>
              </span> <{/foreach}>
            </fieldset>
            <{/foreach}> </td>
        </tr>
        <!-- 子服 End -->
        
        <tr>
          <td style="width:150px;">合服时间</td>
          <td><input type="text" name="time" id="time" style="width:200px;"></td>
        </tr>
         <tr>
          <td style="width:150px;">开服时间</td>
          <td><input type="text" name="start_time" id="start_time" style="width:200px;"></td>
        </tr>
        <tr>
          <td colspan="2"><input type="submit" class="gbutton" value="发送" id="send">
            &nbsp;&nbsp;
            <input type="hidden" id="error" value="1"></td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
<script type="text/javascript">
$(function(){
	var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
	$('#time').datetimepicker(timepickerlang);
    $('#start_time').datetimepicker(timepickerlang);
	
    //母服只能单选
    $('#parent .s-server-click').click(function(){
        $('#parent .s-server-click').removeClass('p-checked');
        $('#parent .s-server-click .p-input').attr("checked", false);
        if(!$(this).hasClass('p-checked')){ 
            $(this).addClass('p-checked');
            $(this).find('.p-input').attr("checked", true);
        }
       //母服勾选了的服，子服需要隐藏掉 
       var value =  $(this).find('input').val();
      
       $('.child span').show();
       $('.child').find('input[value = "'+value+'"]').parent().hide();
       
    })

    //子类勾选
    $('#group').on('click','.c-channel-group',function(){

        InputValue=$(this).find('input').val();

        if($(this).hasClass('p-checked')){
            //每次点击去掉所有的已经勾选了的
            $('#'+InputValue ).find('.p-server').removeClass('p-checked');
            $('#'+InputValue ).find('.p-server').find('input').attr('checked',false);
            
            $(this).removeClass('p-checked');
            $(this).find('input').prop('checked',false)
        }else{

            $(this).addClass('p-checked');
            $(this).find('input').prop('checked',true)
        }

      

 
        if($('#'+InputValue).hasClass('show')){
       
          $('#'+InputValue).removeClass('show');
        } else{

          $('#'+InputValue).addClass('show');
        }

        $('.child').each(function(){  //遍历child,如果有show 就让他显示，没有就隐藏
          if($(this).hasClass('show')){
            $(this).show();
          }else{
            $(this).hide();
          }
        })

        if($('.show').size()<1){
            $('.child').show(); //如果都没勾选，直接显示
        }

    })
    $('#children .p-server').click(function(){

        if($(this).hasClass('p-checked')){
            $(this).removeClass('p-checked');
            $(this).find('input').prop('checked',false);
        }else{
            $(this).addClass('p-checked');
            $(this).find('input').prop('checked',true);
        }
    })

    $('#form').submit(function(){

        var parent = $('#parent .p-checked').length;
        var children = $('input[name="server[]"]:checked').val();
        var time = $("#time").val();

        if(parent == 0){
            $.dialog.tips('请选择母服'); return false;
        }

        if(children == 'undefined'){
           $.dialog.tips('请选择子服'); return false; 
        }

        if(time == ''){
            $.dialog.tips('请选择合服时间'); return false;
        }

        if(start_time == ''){
            $.dialog.tips('请选择开服时间'); return false;
        }
       
        $.ajax({
            url: 'admin.php?ctrl=merge&act=save_merge',
            type: 'POST',
            dataType: 'JSON',
            data: $(this).serialize()
        }).done(function(data){
            $.dialog.tips(data.msg);
        }) 
         return false;
    });

});
</script>