<style>

    #checkboxgroupList .select {
        background-color: #dcd8d8;
    }

    #checkboxgroupList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }


    }

</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="add-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>


            <!-- 主要用于进行一个颜色错位 -->
            <tr></tr>
            <!-- 主要用于进行一个颜色错位 -->

            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server.tpl' }>
            <!-- 服务器，  -->


            <tr>
                <!--
                 <td style="width:150px">公告类型</td>
                 <td>
                     <select name="type">
                         <option value="0">新服公告</option>
                         <option value="1">通用公告</option>
                         <option value="2">活动公告</option>
                         <option value="3">合服公告</option>
                     </select>
                 </td> -->
            </tr>
            <!--
                     <tr>
                        <td style="width:150px">公告标题</td>
                        <td><input type="text" name="title" id="title"></td>
                    </tr> -->

            <tr>
                <td style="width:150px">公告内容</td>
                <td>
                    <textarea rows="20" cols="150" id="contents" name="contents"></textarea>
                </td>
            </tr>
            <!--
                     <tr>
                        <td style="width:150px">权重</td>
                        <td><input type="text" name="sort" id="sort" value="0">  <span>  从大到小的顺序</span></td>
                    </tr> -->

            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="添加">
                    <input type="hidden" name="status" value="0" id="status"/>
                    <input type="hidden" id="error" value="1">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function () {
        var error = $('#error');
        $('#add-form').submit(function (event) {
            form_check();
            if (error.val() == '1') {
                return false;
            } else {
                $.ajax({
                    url: 'admin.php?ctrl=notice&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function (data) {
                    $.dialog.tips(data.msg);
                })
            }
            return false;
        });
    });

    function form_check() {

        var error = $('#error');

        var contents = $('#contents').val();
        /*
        var title = $('#title').val();

        if (title == '') {
            $.dialog.tips('公告标题不能为空');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }*/

        if (contents == '') {
            $.dialog.tips('公告内容不能为空');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }


    }
</script>