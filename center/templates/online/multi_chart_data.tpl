

<style>
#multi_chart {
	min-width: 400px;
	height: 400px;
	padding: 20px;
	margin: 6px auto;
	border: solid 1px #ABABAB;
	border-radius: 5px;
}
</style>

<div id="multi_chart"><{$multi_chart}></div>

<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;">
  <table cellpadding="0" cellspacing="0" border="0" class="oddeven px1">
    <thead>
      <tr height="44px">
        <th >时间</th>
        <th >最大人数</th>
        <th >最小人数</th>
        <th >平均人数</th>
      </tr>
    </thead>
    <tbody>
     <{foreach from=$x key=key item=i}>
   		<tr>
        <td> <{$i}></td>
        <td> <{$y.0.$key}></td>
        <td> <{$y.2.$key}></td>
        <td> <{$y.1.$key}></td>
        </tr>
   <{/foreach}>
    </tbody>
    
  </table>
</div>
