<style>
    #real_chart{
        min-width:400px;
        height:400px;
        padding:20px;
        margin:6px auto;
        border:solid 1px #ABABAB;
        border-radius:5px;
    }
</style>
<div id="real_chart"><{$real_chart}></div>

<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;">
  <table cellpadding="0" cellspacing="0" border="0" class="oddeven px1">
    <thead>
      <tr height="44px">
        <th >时间</th>
        <th >在线角色数</th>
        <th >离线挂机角色数</th>
      </tr>
    </thead>
    <tbody>
    <{foreach from=$x key=key item=i}>
    	<tr>
            <td> <{$i}></td>
            <td> <{$y[0].$key}></td>
            <td> <{$y[1].$key}></td>
        </tr>
     <{/foreach}>
    </tbody>
  </table>
</div>