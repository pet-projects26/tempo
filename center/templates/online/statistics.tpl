<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">
  <div style="margin:5px;border:solid 1px #ABABAB;border-radius:5px;padding:5px 15px;">
    <div id="header" style="margin:10px 0 20px;">
      说明：<br>
      以玩家在线人数为纵坐标，时间轴为横坐标，拉出全天玩家在线情况的折线图<br>
    </div>
    <div id="search">
      <label> 日期：
        <input type="text" name="date" class="datepicker" style=" width:120px;">
      </label>
      &nbsp;
      <label> 时段：
        <select name="start_hour">
          <option value="0">0时</option>
          <option value="1">1时</option>
          <option value="2">2时</option>
          <option value="3">3时</option>
          <option value="4">4时</option>
          <option value="5">5时</option>
          <option value="6">6时</option>
          <option value="7">7时</option>
          <option value="8">8时</option>
          <option value="9">9时</option>
          <option value="10">10时</option>
          <option value="11">11时</option>
          <option value="12">12时</option>
          <option value="13">13时</option>
          <option value="14">14时</option>
          <option value="15">15时</option>
          <option value="16">16时</option>
          <option value="17">17时</option>
          <option value="18">18时</option>
          <option value="19">19时</option>
          <option value="20">20时</option>
          <option value="21">21时</option>
          <option value="22">22时</option>
          <option value="23">23时</option>
          <option value="24">24时</option>
        </select>
        ~
        <select name="end_hour">
          <option value="24">24时</option>
          <option value="23">23时</option>
          <option value="22">22时</option>
          <option value="21">21时</option>
          <option value="20">20时</option>
          <option value="19">19时</option>
          <option value="18">18时</option>
          <option value="17">17时</option>
          <option value="16">16时</option>
          <option value="15">15时</option>
          <option value="14">14时</option>
          <option value="13">13时</option>
          <option value="12">12时</option>
          <option value="11">11时</option>
          <option value="10">10时</option>
          <option value="9">9时</option>
          <option value="8">8时</option>
          <option value="7">7时</option>
          <option value="6">6时</option>
          <option value="5">5时</option>
          <option value="4">4时</option>
          <option value="3">3时</option>
          <option value="2">2时</option>
          <option value="1">1时</option>
          <option value="0">0时</option>
        </select>
      </label>
      &nbsp;
      <input type="button" id="filter" class="gbutton" value="筛选">
      <input type="button" class="gbutton" onclick="_online();" value="搜索">
    </div>
  </div>
  <div><{$scp}></div>
  <div id="total" style="margin:0 5px;"></div>
</div>
<script language="javascript">
    function _online(){
       // var type = $('[name=type]:checked').val();
       //var start_time = $('[name=start_time]').val();
        var date = $('[name=date]').val();
        var scp = JSON.stringify($('#checkboxScp').data());
		var start_hour=$('[name=start_hour]').val();
		var end_hour=$('[name=end_hour]').val();
        $.post('admin.php?ctrl=online&act=statistics_data' , {date:date,start_hour:start_hour,end_hour:end_hour,scp:scp} , function(html){
            $('#total').html(html);
        });
    }
    $('.datepicker').datepicker();
    _online();
</script>