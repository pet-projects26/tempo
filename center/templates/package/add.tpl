<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="package-add-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 每个包号不能重复<br>
                        2. 包号只能由数字组成，长度最长20位<br/>
                        3. 每个包名不能重复, 需要根据包名获取包号<br/>
                        4. 测试服版本号需要与测试包的版本号一致且需添加<span style="color: #ca1b36">IP白名单</span>，版本号请询问<span
                                style="color: #ca1b36">客户端</span>获取</br>
                        5.cdn资源加载链接必须填写
                    </td>
                    <!--<td>5. 提审服版本号为IOS包提审，审核通过后请<span style="color: #ca1b36">不填写</span></td> -->
                </tr>
                <tr>
                    <td style="width: 150px">选择平台</td>
                    <td>
                        <select id="platform" name="platform" class="chose-platform">
                            <option value="0">未选择</option>
                            <option value="1">安卓/越狱</option>
                            <option value="2">IOS</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;">选择渠道</td>
                    <td>
                        <select id="channel_id" name="channel_id">
                            <option value="0">未选择</option>
                            <{foreach from=$channels item=channel}>
                            <option value="<{$channel.channel_id}>"><{$channel.name}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>
                
                 <tr>
                    <td style="width:150px;">选择渠道组</td>
                    <td>
                        <select id="group_id" name="group_id">
                            <option value="0">未选择</option>
                            <{foreach from=$groups item=group}>
                            <option value="<{$group.id}>"><{$group.name}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td style="width:150px;">包号</td>
                    <td><input type="text" id="package_id" name="package_id" value=""></td>
                </tr>
                <tr>
                    <td style="width:150px;">包名</td>
                    <td><input type="text" id="package_name" name="package_name" value=""></td>
                </tr>

                <tr>
                    <td style="width: 150px">超链接</td>
                    <td><input type="text" id="url" name="url" style="width: 400px"/></td>
                </tr>

                <tr class="test-server-ban">
                    <td>测试服版本号</td>
                    <td>
                        <input type="text" id="test_ser_num" name="test_ser_num"  placeholder="填写请询问客户端">
                        <span style="margin-left: 10px;color: #ca1b36">*请保持与需要测试的包的版本号一致，通过后请清空</span>
                    </td>
                </tr>
                <tr class="review-ban">
                    <td style="width: 150px">提审版本号</td>
                    <td><input type="text" id="review_num" name="review_num" /><span style="margin-left: 6px;color: #ca1b36">*填入需要提审的版本号，非提审不填写</span></td>
                </tr>
                <!-- <tr class="review-type">
                    <td style="width: 150px">提审类型</td>
                    <td>
                    	 <select name="type" onchange="checkBtn(this.value)"  class = 'type'>
                             <option value="0">不提审</option>
                             <option value="1">提审1(原来的)</option>
                             <option value="2">提审2(新增的)</option>
                             <option value="3">提审3(新增的)</option>
                         </select>
                    </td>
                </tr>
                <tr class="review-ban review-ban1">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                            <{foreach from=$rv_param key=type_id item=param}>
                            <tr>
                                <td width="10%"><{$rv_name[$type_id]}></td>
                                <td>
                                    <select name="param[<{$type_id}>]">
                                        <option value="">默认</option>
                                    <{foreach from=$param key=id item=type_name}>
                                        <option value="<{$id}>"><{$type_name}></option>
                                    <{/foreach}>
                                    </select>
                                </td>
                            </tr>
                            <{/foreach}>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="review-ban2" style="display:none;">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                           
                            <tr>
                                <td width="10%">皮肤</td>
                                <td>
                                    <select name="param2[res]">
                                        <option value="">默认</option>
                                        <{foreach from=$rv_param2.res key=id item=type_name}>
                                        	<option value="<{$id}>"><{$type_name}></option>
                                        <{/foreach}>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">场景</td>
                                <td>
                                    <select name="param2[sceneid]">
                                        <option value="">默认</option>
                                        <{foreach from=$rv_param2.sceneid key=id item=type_name}>
                                        	<option value="<{$id}>"><{$type_name}></option>
                                        <{/foreach}>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">客户端(新手村)</td>
                                <td>
                                    <select name="param2[clent_sceneid]">
                                        <option value="">默认</option>
                                        <{foreach from=$rv_param2.clent_sceneid key=id item=type_name}>
                                        	<option value="<{$id}>"><{$type_name}></option>
                                        <{/foreach}>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="review-ban3" style="display:none;">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                           
                            <tr>
                                <td width="10%">皮肤</td>
                                <td>
                                    <select name="param3[res]">
                                        <option value="">默认</option>
                                        <{foreach from=$rv_param3.res key=id item=type_name}>
                                        	<option value="<{$id}>"><{$type_name}></option>
                                        <{/foreach}>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">客户端(新手村)</td>
                                <td>
                                    <select name="param3[clent_sceneid]">
                                        <option value="">默认</option>
                                        <{foreach from=$rv_param3.clent_sceneid key=id item=type_name}>
                                        	<option value="<{$id}>"><{$type_name}></option>
                                        <{/foreach}>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr> -->
                    <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">

    $(function(){

        //选择安卓
        $(document).ready(function(){
            $(".chose-platform").change(function(){
                var type = $(this).children("option:selected").val();
                var ban = $(".review-ban");
                var test_ban = $('.test-server-ban');
                var type2  =  $(".type").find("option:selected").val();
                if(type == 2){
                	$('.review-type').show();
                	if(type2 == 2){
                		$('.review-ban2').show();
                		$('.review-ban3').hide();
                	}else if(type2 == 3){
                		$('.review-ban3').show();
                		$('.review-ban2').hide();
                	}else{
                		ban.show();
                	}
                    test_ban.children('td').children('input').val('');
                    test_ban.hide();
                }else{
                	$('.review-type').hide();
                	$('.review-ban2').hide();
                	$('.review-ban3').hide();
                    ban.children('td').children('input').val('');
                    ban.hide();
                    test_ban.show();
                }
            })
        })

        var error = $('#error');
        $('#package-add-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
				var channel_id =$("#channel_id").find("option:selected").text();
				var group_id =$("#group_id").find("option:selected").text();
                var package_id = $("#package_id").val();
                var review_num = $("#review_num").val();
                var platform = $("#platform").find("option:selected").val();
                var test_ser_num = $("#test_ser_num").val();
                var str = '确定选择：\n'+'渠道：'+channel_id+'\n'+'渠道组：'+group_id+'\n'
                        +'包号：'+package_id+'\n'+'测试服版本号：'+test_ser_num;

                if (platform == 2) {
                    str += '提审版本号：' + review_num;
                }
			
				if(confirm(str)){
					$.ajax({
                    url: 'admin.php?ctrl=package&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
					}).done(function(data){
						$.dialog.tips(data.msg);
					})
				}
              return false;
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //平台
        var platform  = $("#platform").val();
        if(platform == 0){
            $.dialog.tips("请选择平台");
            error.val(1);
            return false;
        }else if(platform == 2){
            var review = $("#review_num").val();
            if(!review){
                //$.dialog.tips("选择ISO平台,请注意提审版本号！");
                //error.val(0);
            }
        }
        //渠道
        var channel_id = $('#channel_id').val();
        if(channel_id == '0'){
            $.dialog.tips('未选择渠道');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
		
		var group_id = $('#group_id').val();
        if(group_id == '0'){
            $.dialog.tips('未选择渠道组');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
		
        //包号
        var package_id = $('#package_id').val();
        if(package_id == ''){
            $.dialog.tips('未填写包号');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //包名
        var package_name = $('#package_name').val();
        if (package_name == '') {
            $.dialog.tips('未填写包名');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
    }
    
 function checkBtn(type){
	if(type == 2){
		$('.review-ban2').show();
		$('.review-ban1').hide();
		$('.review-ban3').hide();
	}else if(type == 3){
		$('.review-ban3').show();
		$('.review-ban1').hide();
		$('.review-ban2').hide();
	}else{
		$('.review-ban1').show();
		$('.review-ban2').hide();
		$('.review-ban3').hide();
	}
	
}
</script>