<style type="text/css">
.w-box-list {
	margin: 5px 0px;
}
.w-box-list h5 {
	border: 1px dashed #cc0;
	padding: 5px;
	margin-bottom: 5px;
	text-align: center;
}
.modal-demo1 .ui-tabs-panel ul li {
    display: inline-block;
    width: 200px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    border: 1px solid #ccc;
    cursor: pointer;
    margin: 0px !important;
    padding: 0px !important;
    overflow: hidden;
}
</style>

<div>

 渠道组：
 	<select name="group" id="group">
    	<{foreach from=$selectgroup key=key item=item}>
    	<option value="<{$key}>"><{$item}></option>
        <{/foreach}>
    </select>
 包：<input type="text" name="package" id="package" />
 
 <input type="button" class="gbutton" id="search"  value="搜索"/>

</div>

<div class="modal-demo1">
  <div class="ui-tabs-panel">
    <div > <{foreach from = $group item = row key = group_id}>
      <div class="w-box-list ">
        <h5><{$row.name}></h5>
        <ul>
          <{foreach from=$row.package item=item}>
          <li> <span class="name"><{$item}></span> </li>
          <{/foreach}>
        </ul>
      </div>
      <{/foreach}>
    </div>
  </div>
</div>

<script>
$(function(){
	$('#search').click(function(){
		var group = $('#group').val();
		var package = $('#package').val();
		$.post('admin.php?ctrl=package&act=ajax_package&group='+group+'&package='+package , '' , function(data){
			$('.modal-demo1').html(data);	
		});
	});	
})

</script>
