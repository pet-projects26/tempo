<form class="permit_setting_form" method="post" action="admin.php?ctrl=permit&act=group_permit_save">
<table class="itable itable-color">
<thead>
    <tr>
        <th>渠道权限设置：</th>
        <td><label>
            <input type="checkbox" name="all" value="1" <{if $usergroup.channel=='all' }>checked<{/if}>/>所有渠道权限</label>
            (<font color="red">如果勾选所有权限，则直接认为具有所有权限</font>)
        </td>
    </tr>
</thead>
<tbody>
            <{foreach name=cl from=$channelList key=key item=item}>
            <tr><td><label><input name="<{$key}>" class="channel cp" type="checkbox" value="<{$key}>" <{if $item.checked}>checked<{/if}>><{$item.name}></label>
                <{if $smarty.foreach.cl.iteration == 1}>(如果勾选渠道，则认为具有该渠道的所有权限)<{/if}>
                </td><td>
                <{foreach from=$item.package key=k item=v}>
                    <label><input class="<{$key}> cp package" name="channel[<{$key}>][]" type="checkbox" value="<{$k}>" <{if $v}>checked<{/if}>><{$k}></label>
                <{/foreach}>
            </td></tr>
            <{/foreach}>
    <tr>
        <td><input type="hidden" name="groupid" value="<{$usergroup.groupid}>"/>
        <input type="hidden" name="type" value="channel"/>
        </td>
        <td><input type="submit" class="gbutton" value="提交" name="submit" style="float:right;"/></td>
    </tr>
</tbody>
</table>
</form>
<script type="text/javascript">
    $(function() {
        $('input[name="all"]').click(function() {
            $('.cp').prop('checked', this.checked)
        })
        $('.channel').click(function() {
            var channel = $(this).val()
            $('.' + channel).prop('checked', $(this).prop('checked'))
        })

        $('.cp').click(function() {
            if (!$(this).prop('checked'))
                $('input[name="all"]').prop('checked', false)
        })
    })
</script>