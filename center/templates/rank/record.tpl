<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">



 <table class="itable itable-color">
  
	  <tbody >
	  	<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/server_single.tpl' }>
        <!-- 服务器，  -->

	    <tr class='p-tr'> 
	    	<td>排行类型：</td>
	    	<td>
			    <select name="type" id="type">
                    <{foreach from = $rankType key = id item = row}>
                    <option value="<{$id}>"><{$row.name}></option>
                    <{/foreach}>
			    </select>
	      	</td>
    	</tr>
	    
         <tr>
         	<td colspan="2">
         		<input type="button" class="gbutton" onclick="_online();" value="搜索">
         	</td>
         	
         </tr>
	  </tbody>
  
  </table>
	
</div>


<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;" id="all">
  <table cellpadding="0" cellspacing="0" border="0" class="oddeven px1">
    <thead>
      <tr height="44px">
          <th>角色名</th>
          <th>VIP等级</th>
          <th>VIP经验</th>
        <th >排行名次</th>
          <th id="name">参数</th>
          <th>时间</th>
      </tr>
    </thead>
    <tbody id="tbody">
      </tbody>
  </table>
</div>
<script type="text/javascript">  
    $(function(){
	   $('.datepicker').datepicker({maxDate:0});
    });

    var rankType = [];

    getRankType();

    function getRankType() {
        $.ajaxSettings.async = false;
        $.post("admin.php?ctrl=rank&act=getRankType", '', function (data) {
            rankType = data;
        }, "json");
        $.ajaxSettings.async = true;
    }

	function _online(){
		var type=$("#type").val();
		var server= $(".p-checked input[name='server']").val();
		if(typeof(server) == "undefined"){
			$.dialog.tips('请选择服务器');	return false;		
		}
        var name = rankType[type].name;

        $.post("admin.php?ctrl=rank&act=record_data",{"server":server,"type":type}, function (data) {

            if (data.errno == 2) {
                $.dialog.tips(data.error);
                return false;
            }

			$("#name").html(name);
            $('#tbody').html(data);
		},"json");
	}

    /*
    function detail(time,event){
        var color=$(event.currentTarget);
        var type=$("#type").val();
        var server= $(".p-checked input[name='server']").val();
        if(typeof(server) == "undefined"){
            $.dialog.tips('请选择服务器');	return false;
        }
        $.post("admin.php?ctrl=rank&act=getdata",{"server":server,"type":type,"time":time},function(data){
			$('#tbody').html(data);
			$('#hour a span').css('color','black');
			color.css('color','red');
		},"json");

	}*/
</script>