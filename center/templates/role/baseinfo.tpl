<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;">
  <table border="0" id="baseinfo">
    <tbody>
      <tr class="even">
        <td width="10%" style="padding: 5px;">角色ID</td>
        <td width="20%" id="role_id" style="padding: 5px;"><{$info.id}></td>
        <td width="10%" style="padding: 5px;">账号</td>
        <td width="20%" id="account" style="padding: 5px;"><{$info.account}></td>
      </tr>
      <tr class="odd">
        <td width="10%" style="padding: 5px;">角色名</td>
        <td width="20%" style="padding: 5px;"><{$info.name}></td>
        <td width="10%" style="padding: 5px;">等级</td>
        <td width="20%" style="padding: 5px;"><{$info.level}></td>
      </tr>
      <tr class="even">
        <td width="10%" style="padding: 5px;">VIP等级</td>
        <td width="20%" style="padding: 5px;"><{$info.vip}></td>
         <td width="10%" style="padding: 5px;">战斗力</td>
        <td width="20%" style="padding: 5px;"><{$info.ce}></td>
      </tr>
      
      <!-- <tr class="odd">
        <td width="10%" style="padding: 5px;">帐号权限</td>
        <td width="20%" style="padding: 5px;">还没做</td>
        <td width="10%" style="padding: 5px;">帮派</td>
        <td width="20%" style="padding: 5px;">还没做</td>
      </tr>-->
        <tr class="odd">
       <td width="10%" style="padding: 5px;">渠道</td>
        <td width="20%" style="padding: 5px;"><{$info.channel}></td>
        <td width="10%" style="padding: 5px;">包</td>
        <td width="20%" style="padding: 5px;"><{$info.package}></td>
      </tr>
       
      <tr class="even">
        <td width="10%" style="padding: 5px;">创号时间</td>
        <td width="20%" style="padding: 5px;"><{$info.create_tm}></td>
        <td width="10%" style="padding: 5px;">职业</td>
        <td width="20%" style="padding: 5px;"><{$info.occ}></td>
      </tr>
      <tr class="odd">
        <td width="10%" style="padding: 5px;">经验值</td>
        <td width="20%" style="padding: 5px;"><{$info.exp}></td>
        <td width="10%" style="padding: 5px;">充值元宝</td>
        <td width="20%" style="padding: 5px;"><{$info.diamond}></td>
      </tr>
      <tr class="even">
        <td width="10%" style="padding: 5px;">剩余铜钱</td>
        <td width="20%" style="padding: 5px;"><{$info.tongqian}></td>
        <td width="10%" style="padding: 5px;">剩余元宝</td>
        <td width="20%" style="padding: 5px;"><{$info.yuanbao}></td>
      </tr>
      <tr class="odd">
        <td width="10%" style="padding: 5px;">剩余绑元</td>
        <td width="20%" style="padding: 5px;"><{$info.bangyuan}></td>
        <td width="10%" style="padding: 5px;">剩余积分</td>
        <td width="20%" style="padding: 5px;"><{$info.jifen}></td>
      </tr>

      <tr class="even">
        <td width="10%" style="padding: 5px;">主武器</td>
        <td width="20%" style="padding: 5px;"><{$equip.1}>  <{$gemdetail.1}></td>
        <td width="10%" style="padding: 5px;">副武器</td>
        <td width="20%" style="padding: 5px;"><{$equip.2}>  <{$gemdetail.2}></td>
      </tr>
      <tr class="odd">
        <td width="10%" style="padding: 5px;">项链</td>
        <td width="20%" style="padding: 5px;"><{$equip.3}>  <{$gemdetail.3}></td>
        <td width="10%" style="padding: 5px;">左戒指</td>
        <td width="20%" style="padding: 5px;"><{$equip.4}>  <{$gemdetail.4}></td>
      </tr>
      <tr class="even">
        <td width="10%" style="padding: 5px;">右戒指</td>
        <td width="20%" style="padding: 5px;"><{$equip.5}>  <{$gemdetail.5}></td>
        <td width="10%" style="padding: 5px;">头盔</td>
        <td width="20%" style="padding: 5px;"><{$equip.6}>  <{$gemdetail.6}></td>
      </tr>
      <tr class="odd">
        <td width="10%" style="padding: 5px;">衣服</td>
        <td width="20%" style="padding: 5px;"><{$equip.7}>  <{$gemdetail.7}></td>
        <td width="10%" style="padding: 5px;">腰带</td>
        <td width="20%" style="padding: 5px;"><{$equip.8}>  <{$gemdetail.8}></td>
      </tr>
      <tr class="even">
        <td width="10%" style="padding: 5px;">护腿</td>
        <td width="20%" style="padding: 5px;"><{$equip.9}>  <{$gemdetail.9}></td>
        <td width="10%" style="padding: 5px;">鞋子</td>
        <td width="20%" style="padding: 5px;"><{$equip.10}>  <{$gemdetail.10}></td>
      </tr>

      <tr class="odd">
        <td width="10%" style="padding: 5px;">坐骑魂石</td>
        <td width="20%" style="padding: 5px;"><{$ride}></td>
        <td width="10%" style="padding: 5px;">法器魂石</td>
        <td width="20%" style="padding: 5px;"><{$multiplier}></td>
      </tr>

    <tr class="even">
        <td width="10%" style="padding: 5px;">血量</td>
        <td width="20%" style="padding: 5px;"><{$info.hp}></td>
        <td width="10%" style="padding: 5px;">最高攻击力</td>
        <td width="20%" style="padding: 5px;"><{$info.attackList}></td>
    </tr>
    
    <tr class="odd">
        <td width="10%" style="padding: 5px;">防御力</td>
        <td width="20%" style="padding: 5px;"><{$info.totalDefense}></td>
        <td width="10%" style="padding: 5px;">改名前的角色</td>
        <td width="20%" style="padding: 5px;"><{$info.rename}></td>
    </tr>
    </tbody>
  </table>
</div>
