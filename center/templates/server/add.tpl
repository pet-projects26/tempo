<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="server-add-form" style="clear:both;width:100%;">
        <div class="hidden">
            <input type="hidden" value="1" id="error">
        </div>
        <div id="tables">
            <div>
                <table class="itable itable-color">
                    <tbody>
                        <tr>
                            <td>
                                说明：<br>
                                1. 红色标题的选项为必填选项<br>
                                2. 【认证密钥】 是访问该服务器时的验证密钥<br>
                                3. 【api地址】 由部署新服务器的人员提供<br>
                                4. 【未开服提醒指】 当玩家在服务器列表中选择未开服的服务器时返回提醒的文本<br>
                                5. 【编号】 只能由数字组成，并且不能大于1000，数字越小越好，编号在添加之后不能再做修改<br>
                                6. 【服务器标识】 只能由数字，小写字母，大写字母，下划线（_）和横杠（-）组成
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="float:left;width:50%;">
                <table class="itable itable-color serverbase">
                    <tbody>
                        <tr><td colspan="2" style="color:#228b22;font-weight:bold;">基础信息</td></tr>
                        
                        <tr><td style="color:#ff0000;">服号</td><td><input type="text" name="num" id="num"></td></tr>
                        
                        <tr><td style="color:#ff0000;">标识</td><td><input type="text" name="server_id" id="server_id"></td></tr>
                        <tr><td style="color:#ff0000;">名称</td><td><input type="text" name="name" id="name"></td></tr>
                        <tr>
                            <td>分区</td>
                            <td>
                                <select name="zone" id="zone">
                                    <option value="0">未选择</option>
                                    <{foreach from=$zone item=item}>
                                    <option value="<{$item.id}>"><{$item.name}></option>
                                    <{/foreach}>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>类型</td>
                            <td>
                                <select name="type" id="type">
                                    <option value="1">正常服</option>
                                    <option value="2">测试服</option>
                                    <option value="3">合服</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>状态</td>
                            <td>
                                <select name="status" id="status">
                                    <{foreach from=$status key=key item=item}>
                                    <option value="<{$key}>" <{if $key eq 0}>selected="selected"<{/if}>><{$item}></option>
                                    <{/foreach}>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>是否显示</td>
                            <td>
                                <select name="display" id="display">
                                    <option value="0">不显示</option>
                                    <option value="1">显示</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>提审</td>
                            <td>
                                <input type="radio" name="review" id="no_rv" class="ver" <{if $server.review eq 0}>checked="checked"<{/if}> value="0" /><label class="label">否</label>
                                <input type="radio" name="review" id="yes_rv" class="ver" <{if $server.review eq 1}>checked="checked"<{/if}> value="1" /><label class="label">是</label>
                            </td>
                        </tr>
                        
                        <!-- <tr><td>设置为第几服</td><td><input type="text" name="sort" id="sort"></td></tr> -->

                        <tr><td>开服时间</td><td><input type="text" name="open_time" id="open_time" class="datepicker"></td></tr>
                        <tr><td>关服时间</td><td><input type="text" name="close_time" id="close_time" class="datepicker"></td></tr>
                        <tr><td>ip</td><td><input type="text" name="ip" id="ip"></td></tr>
                        <tr><td>最大承载量</td><td><input type="text" name="max_online" id="max_online" value="3000"></td></tr>
                        <tr><td>域名</td><td><input type="text" name="domain" id="domain"></td></tr>
                        <tr><td>api地址</td><td><input type="text" name="api_url" id="api_url"></td></tr>
                        <tr><td>未开服提醒</td><td><input type="text" name="tips" id="tips"></td></tr>
                        <!--<tr><td style="visibility:hidden;">empty</td><td></td></tr>-->
                    </tbody>
                </table>
            </div>
            <div style="float:left;width:50%;">
                <table class="itable itable-color database">
                    <tbody>
                        <tr><td colspan="2" style="color:#228b22;font-weight:bold;">连接信息</td></tr>
                        <tr style="display: none;"><td>mongodb服ip</td><td><input type="hidden" name="mongo_host" id="mongo_host" value="127.0.0.1"></td></tr>
                        <tr style="display: none;"><td>mongodb端口</td><td><input type="hidden" name="mongo_port" id="mongo_port" value="27017"></td></tr>
                        <tr style="display: none;"><td>mongodb用户名</td><td><input type="hidden" name="mongo_user" id="mongo_user"></td></tr>
                        <tr style="display: none;"><td>mongodb密码</td><td><input type="hidden" name="mongo_passwd" id="mongo_passwd"></td></tr>
                        <tr style="display: none;"><td>mongodb默认连接数据库</td><td><input type="hidden" name="mongo_db" id="mongo_db"></td></tr>

                        <tr><td>redis服ip</td><td><input type="text" name="redis_host" id="redis_host" value="127.0.0.1"></td></tr>
                        <tr><td>redis端口</td><td><input type="text" name="redis_port" id="redis_port" value="27017"></td></tr>
                        <tr><td>redis密码</td><td><input type="text" name="redis_passwd" id="redis_passwd"></td></tr>
                        <tr><td>redisKey前缀</td><td><input type="text" name="redis_prefix" id="redis_prefix" ></td></tr>
                        <tr><td>redis默认连接数据库</td><td><input type="text" name="redis_db" id="redis_db"></td></tr>

                        <tr><td>websocket服ip</td><td><input type="text" name="websocket_host" id="websocket_host" value="127.0.0.1"></td></tr>
                        <tr><td>websocket端口</td><td><input type="text" name="websocket_port" id="websocket_port" value="9502"></td></tr>

                        <tr><td>mysql服ip</td><td><input type="text" name="mysql_host" id="mysql_host" value="127.0.0.1"></td></tr>
                        <tr><td>mysql端口</td><td><input type="text" name="mysql_port" id="mysql_port" value="3306"></td></tr>
                        <tr><td>mysql用户名</td><td><input type="text" name="mysql_user" id="mysql_user"></td></tr>
                        <tr><td>mysql密码</td><td><input type="text" name="mysql_passwd" id="mysql_passwd"></td></tr>
                        <tr><td>mysql默认连接数据库</td><td><input type="text" name="mysql_db" id="mysql_db"></td></tr>
                        <tr><td>mysql数据表前缀</td><td><input type="text" name="mysql_prefix" id="mysql_prefix"></td></tr>
                        <tr><td>认证密钥</td><td><input type="text" name="mdkey" id="mdkey"></td></tr>
                        <tr><td>gm端口</td><td><input type="text" name="gm_port" id="gm_port"></td></tr>
                        <tr style="color:#ff0000;"><td>登录服ip</td><td><input type="text" name="login_host" id="login_host"></td></tr>
                        <tr style="color:#ff0000;"><td>登录服端口</td><td><input type="text" name="login_port" id="login_port"></td></tr>
                    </tbody>
                </table>
            </div>
            <div>
                <table class="itable itable-color">
                    <tbody>
                    <tr>
                        <td><input type="submit" value="添加" class="gbutton"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
<script>
    $(function(){
        $('input[type=text]').css('width' , '150px');
        $('input[name=api_url]').css('width' , '400px');
        $('input[name=domain]').css('width' , '400px');
        $('input[name=tips]').css('width' , '400px');
        $('#server-add-form').css('height' , $('#tables').css('height'));
        var timepickerlang = { timeText:'时间' , hourText:'小时' , minuteText:'分钟' , currentText:'现在' , closeText:'确定' }
        $('.datepicker').datetimepicker(timepickerlang);

        var error = $('#error');
        $('#server-add-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=server&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //服务器编号
        var num = $('#num').val();
        if(num == ''){
            $.dialog.tips('未填写服务器编号');
            error.val(1);
            return false;
        } else {

            if (isNaN(num)) {
                $.dialog.tips('服务器编号请输入整数');
                error.val(1);
                return false;
            }

            num = parseInt(num);

            if (num < 0 || num > 1000) {
                $.dialog.tips('服务器编号请输入 1 - 1000 之间');
                error.val(1);
                return false;
            }


            error.val(0);
        }
        //服务器标识
        var server_id = $('#server_id').val();
        if(server_id == ''){
            $.dialog.tips('未填写服务器标识');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //服务器名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写服务器名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>