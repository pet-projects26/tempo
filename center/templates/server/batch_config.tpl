<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/animate.min.css">
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/jquery.gDialog.css">
<style type="text/css">
    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-hupdate-form">
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td colspan="2">
                    <div style="margin:15px 0;">
                        说明：<br>
                        1. 在添加字符串的输入框填好字符串后 点击 添加按钮 或者 按下键盘的Enter键 可添加字符串<br>
                        2. 删除按钮 用于删除最后一个加入的字符串
                    </div>
                </td>
            </tr>
            <!-- 渠道组 Start -->
            <tr class='p-tr'>
                <td style="width:150px">渠道组</td>
                <td>
                    <{foreach from = $groups key = id item = row}>
                    <span class="p-span c-channel-group  c-channel-group-list span-click">
				<input class="p-input" type="radio" name="channel_group[]" value="<{$id}>" id="channel_group-<{$id}>"/>
				<label for="channel_group-<{$id}>" class="p-input"><{$row}></label>
			</span>
                    <{/foreach}>
                </td>
            </tr>

            <!-- 渠道组 End -->

            <!-- 服务器 Start  -->
            <tr class="p-tr server">
                <td class="w-150">服务器</td>
                <td>
                    <fieldset>
                        <legend> 当前渠道组下所有服务器</legend>
                        <span class="p-span s-server-click">
				<input class="p-input" type="checkbox" name="server[]" value="" id="server-all"/>
				<label for="server-all" class="p-input">当前所选渠道组下所有服务器</label>
			</span>
                        <font style="color:#f00; font-weight: bold;">选择该选项后，代表选中当前所选的渠道组下所有服务器</font>
                    </fieldset>
                    <{foreach from = $servers key = group_id  item = rows }>
                    <fieldset class="server-box" id="<{$group_id}>" style="display: none">
                        <legend> <{$groups[$group_id]}></legend>
                        <{foreach from = $rows  key = sign  item = name}>
                        <span class="p-span s-server-click">
					<input class="p-input" type="checkbox" name="server[]" value="<{$sign}>" id="server-<{$sign}>"/>
					<label for="server-<{$sign}>" class="p-input"><{$name}></label>
				</span>
                        <{/foreach}>
                    </fieldset>
                    <{/foreach}>
                </td>
            </tr>
            <!-- 服务器 End -->
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="发送">
                    <input type="hidden" value="1" id="error">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript" src="style/js/jQuery-gDialog/jquery.gDialog.min.js"></script>
<script type="text/javascript">
    $(function () {

        var error = $('#error');
        $('#manual-hupdate-form').submit(function (event) {
            form_check();
            if (error.val() == '1') {
                return false;
            }
            else {

                $.ajax({
                    url: 'admin.php?ctrl=server&act=batch_config_action&save=1',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    success: function (res) {
                        if (res.code == 200) {
                            $.gDialog.alert('成功', {
                                title: '操作成功!',
                            });
                        } else {
                            $.gDialog.alert(res.msg, {
                                title: '操作失败!',
                            });
                        }
                    },
                    error: function () {
                        $.gDialog.alert('错误', {
                            title: '出现错误,请打开Network查看报错信息!',
                        });
                    }
                })
                return false;
            }
        });
    });


    function form_check() {
        var error = $('#error');
        //服务器
        var server = $('#server');
        if (server.val() == 0) {
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
    }

    $().ready(function () {
        //其他渠道
        $('.c-channel-group-list input').unbind('click').bind('click', function () {
            var mark = $(this).prop('checked');
            $(".c-channel-group-list").removeClass('p-checked');
            if (mark) {
                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }

            var checkLen = $('.c-channel-group-list input:checked').length;
            $(".server-fieldset").removeClass('server-fieldset');
            //根据渠道组筛选服务器
            InputValue = $(this).val();

            $('#' + InputValue).addClass('show');
            $("#" + InputValue).siblings().removeClass('show');
            $('.server fieldset').each(function () {  //遍历child,如果有show 就让他显示，没有就隐藏
                $('.server fieldset').eq(0).show();
                if ($(this).hasClass('show')) {
                    $(this).show();
                } else {
                    //清除选择
                    $(this).children('span').removeClass('p-checked');
                    $(this).children('span').find('.p-input').prop('checked', 0);
                    $(this).hide();
                }
            });
        });


        //当前渠道的所有服
        $('.s-server-click input[value=""]').unbind('click').bind('click', function () {
            var mark = $(this).prop('checked');

            if (mark) {
                $('.s-server-click  input[value != ""]').prop('checked', 0);
                $('.s-server-click  input[value != ""]').parent('span').removeClass('p-checked');

                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }
        });

        // 非全服的其他服
        $('.s-server-click  input[value != ""]').unbind('click').bind('click', function () {
            var obj = '.s-server-click';
            var mark = $(this).prop('checked');
            if (mark) {

                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }

            var allLen = $(obj + ' input').length;

            var checkLen = $(obj + ' input[value != ""]:checked').length;

            //如果都没有选中，则自动选中全选
            if (checkLen == 0) {
                $(obj + ' input[value=""]').prop('checked', 1);
                $(obj + ' input[value=""]').parent('span').addClass('p-checked');
            } else {
                $(obj + ' input[value=""]').prop('checked', 0);
                $(obj + ' input[value=""]').parent('span').removeClass('p-checked');
            }

        });
    });
</script>