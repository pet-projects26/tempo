<{if $server_id}>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-open-form">
        <div class="hidden">
            <input type="hidden" name="server_id" value="<{$server_id}>">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">选择渠道组</td>
                    <td>
                        <{foreach from=$group item=item}>
                        <label class="label <{if $item.id == $id}> p-checked<{/if}> >" for='group-<{$item.id}>'>
                            <input class="ver" type="radio" name="id" value="<{$item.id}>" <{if $item.id == $id}> checked<{/if}> id="group-<{$item.id}>">
                            <font class="ver" ><{$item.name}></font>
                        </label>
                        <{/foreach}>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">操作</td>
                    <td><input type="submit" class="gbutton" value="保存"></td>
                </tr>
                <tr>
                    <td colspan="2" style="color:#228b22;font-weight:bold;text-align:center;">渠道组信息</td>
                </tr>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 红色部分为当前服务器开通的渠道组
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;color:#228b22;">渠道组名称</td>
                    <td style="font-weight:bold;color:#228b22;">渠道包含的包</td>
                </tr>
               
                <{foreach from=$package key=groupId item=item}>
                <tr <{if ($id == $groupId)}> class="red" <{/if}> >
                    <td><{if (isset($group[$groupId].name))}> <{$group[$groupId].name}> <{else}> <{$groupId}> <{/if}></td>
                    <td>
                        <{foreach from=$item key=channelName item=packages}>
                            <div>
                                <span style="width: 150px; float:left;"><{$channelName}>：</span>
                                <span><{','|implode:$packages}></span>
                            </div>
                        <{/foreach}>
                    </td>
                </tr>
                <{/foreach}>

            </tbody>
        </table>
    </form>
</div>
<{else}>
请选择开通渠道的服务器
<{/if}>
<script>
    $(function(){
        var error = $('#error');
        $('#channel-open-form').submit(function(event){
            var v = $('input[name="id"]:checked').val();
            if(typeof v == 'undefined') {
                $.dialog.tips('请先择渠道组');
                return false;
            }

            $.ajax({
                url: 'admin.php?ctrl=channelgroup&act=open_action',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize()
            }).done(function(data){
                $.dialog.tips(data.msg);
                if(data.code){
                    $tabs.tabs('load' , 3);
                }
            });
            return false;
        });
    });


    function form_check(){
        var error = $('#error');
        //渠道组
        var id = $('#id').val();
        if(id == 0){
            $.dialog.tips('未选择渠道组');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }

    //绑定点击渠道组
    $('input[name="id"]').unbind('click').bind('click', function() {
        $('.label').removeClass('p-checked');
        $(this).parent('.label').addClass('p-checked');
    });
</script>