<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/animate.min.css">
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/jquery.gDialog.css">
<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">
    <table class="itable itable-color">

        <tbody>
        <tr>
            <td colspan="2">
                <div style="margin:15px 0;">
                    说明：<br>
                    点击刷新会刷新redis中的服务器数据<br>
                </div>
            </td>
        </tr>
        <!-- 主要用于进行一个颜色错位 -->
        <tr></tr>
        <!-- 主要用于进行一个颜色错位 -->
        <tr>
            <td colspan="2">
                <input type="button" class="gbutton" id="clean" value="刷新">
            </td>
        </tr>
        </tbody>

    </table>

</div>
<script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker({maxDate:0});
    });

    $('#clean').click(function () {

        //根据切换的服务器查询服务器状态
        $.gDialog.confirm("确认要刷新服务器缓存吗?", {
            title: "操作确认",
            onSubmit: function () {
                $.ajax({
                    url: 'admin.php?ctrl=server&act=del_redis_server_action',
                    type: 'POST',
                    timeout: 0,
                    dataType: 'JSON',
                    data: {'save': 1},
                    success(res) {
                        if (res.state) {
                            $.gDialog.alert('成功刷新缓存', {
                                title: '成功!',
                            });
                        } else {
                            $.gDialog.alert(res.msg, {
                                title: '失败!',
                            });
                        }
                    }
                })
            },
            onCancel: function () {

            }
        });
    });

</script>