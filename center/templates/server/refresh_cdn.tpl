<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/animate.min.css">
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/jquery.gDialog.css">
<style type="text/css">
    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-hupdate-form">
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td colspan="2">
                    <div style="margin:15px 0;">
                        说明：<br>
                        1. 在添加字符串的输入框填好字符串后 点击 添加按钮 或者 按下键盘的Enter键 可添加字符串<br>
                        2. 删除按钮 用于删除最后一个加入的字符串
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">刷新类型</td>
                <td>
                    <select name="refreshType" id="type">
                        <option value="1">刷新url</option>
                        <option value="2">刷新目录</option>
                    </select>
                </td>
            </tr>
            <tr id="dirTypeDiv" style="display: none">
                <td style="width:150px;">目录刷新类型</td>
                <td>
                    <select name="dirType" id="dirType">
                        <option value="1">刷新目录变更资源</option>
                        <option value="2">刷新目录全部资源</option>
                    </select>
                </td>
            </tr>
            <tr id="addstr_tr">
                <td style="width:150px;">添加刷新url/目录</td>
                <td>
                    <input type="text" id="str" style="width:200px;">
                    <input type="button" id="addStr" class="gbutton" value="添加">
                    <input type="button" id="delStr" class="gbutton" value="删除">
                </td>
            </tr>
            <tr id="strlist_tr">
                <td style="width:150px;">已添加url/目录</td>
                <td>
                    <div id="strlist"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="发送">
                    <input type="hidden" value="1" id="error">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript" src="style/js/jQuery-gDialog/jquery.gDialog.min.js"></script>
<script type="text/javascript">
    $(function () {

        $('#type').change(function () {

            var type = $(this).val();

            switch (type) {
                case '2':
                    $('#dirTypeDiv').show();
                    break;
                case '1':
                    $('#dirTypeDiv').hide();
                    break;
                default:
                    $('#dirTypeDiv').hide();
                    break;
            }
        });


        $('#str').keypress(function (event) {
            var keycode = event.which;
            if (keycode == 13) {
                addStr();
                return false;
            }
        });

        $('#addStr').click(function () {
            addStr();
        });

        $('#delStr').click(function () {
            delStr();
        });

        var error = $('#error');
        $('#manual-hupdate-form').submit(function (event) {
            form_check();
            console.log(error.val());
            if (error.val() === '1') {
                return false;
            }
            else {

                $.ajax({
                    url: 'admin.php?ctrl=server&act=refresh_cdn_action&save=1',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    success: function (res) {
                        $.gDialog.alert(res.message, {
                            title: '操作结果'
                        });
                    }
                });
            }
        });
    });

    function addStr() {
        var str = $('#str').val();
        if (str == '') {
            $.dialog.tips('url/目录不能为空');
            return false;
        }
        var strArr = [];
        $("input[name='str[]']").each(function (index, item) {
                strArr[index] = $(this).val();
            }
        );

        var html = '<div style="margin:5px 0 10px 0;"><input class="strlist" name="str[]" style="width:200px;" type="text" value="' + str + '"></div>';
        $('#strlist').append(html);
        var strlength = $('#strlist').children('div').length;
        $('#strlist').children('div').last().append('<span>&nbsp;（第' + strlength + '个）</span> ');
        $('#str').val('');
        $('#str').focus();
    }

    function delStr() {
        $('#strlist').children('div').last().remove();
    }

    function form_check() {
        var error = $('#error');

        //字符串列表
        var strlength = $('#strlist').children('div').length;
        if (strlength == 0) {
            $.dialog.tips('没有添加任何url/目录');
            error.val(1);
        }
        else {
            error.val(0);
        }

        $('.strlist').each(function () {
            if ($(this).val() == '') {
                $.dialog.tips('已添加url/目录 中 存在空的输入框');
                error.val(1);
            }
            else {
                error.val(0);
            }
        });

    }

</script>