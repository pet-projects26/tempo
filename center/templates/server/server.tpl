<style type="text/css">
.w-box-list {
	margin: 5px 0px;
}
h5 {
	border: 1px dashed #cc0;
	padding: 5px;
	margin: 10px 0px;
	text-align: center;
}
.contents {
	/*border: 2px solid #2799DC; */
	padding: 5px;
	margin-bottom: 10px;
}
</style>

<div> 渠道组：
  <select name="group" id="group">
    <{foreach from=$group  item=item}>
    <option value="<{$item.id}>"><{$item.name}></option>
    <{/foreach}>
  </select>
  渠道：
  <select name="channel" id="channel">
    <{foreach from=$channel  item=item}>
    <option value="<{$item.num}>"><{$item.name}></option>
    <{/foreach}>
  </select>
  开服时间：
  <input type="text" id="start_time" />
  ~
  <input type="text" id="end_time" />
  状态：
  <select name="serverStatus" id="serverStatus">
    <{foreach from=$serverStatus key=key  item=item}>
    <option value="<{$key}>"><{$item}></option>
    <{/foreach}>
  </select>
  名称：
  <input type="text" name="name" id="name" />
  <input type="button" class="gbutton" id="search"  value="搜索"/>
</div>
<div id="server"> <{foreach from = $rss item =item key =key}>
  <div class="contents" >
    <h5><{$key}></h5>
    <table cellpadding="0" cellspacing="0" border="1" class="table_list dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info" >
      <thead>
        <tr height="44px" role="row">
          <th width="80" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="编号" style="width: 74px;">编号</th>
          <th width="81" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="渠号" style="width: 75px;">渠号</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="名称" style="width: 111px;">名称</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="标识" style="width: 111px;">标识</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="分区" style="width: 111px;">分区</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="认证密钥" style="width: 111px;">认证密钥</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="状态" style="width: 111px;">状态</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="开服时间" style="width: 112px;">开服时间</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="开服天数" style="width: 112px;">开服天数</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="显示状态" style="width: 112px;">显示状态</th>
          <th width="300" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="操作" style="width: 277px;">操作</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
      <{foreach from = $item item =i key =key}>
      <tr class="odd"><!--<{if $key % 2 == 0}> odd <{else}> even <{/if}>-->
        <td class=" sorting_1"><{$i.num}></td>
        <td class=""><{$i.channel_num}></td>
        <td class=""><{$i.name}></td>
        <td class=""><{$i.server_id}></td>
        <td class=""><{$i.zone}></td>
        <td class=""><{$i.mdkey}></td>
        <td class=""><{$i.status}></td>
        <td class=""><{$i.open_time}></td>
        <td class=""><{$i.day}></td>
        <td class=""><{$i.display}></td>
        <td class=""><{$i.caozuo}></td>
      </tr>
      <{/foreach}>
        </tbody>
      
    </table>
  </div>
  <{/foreach}> </div>
<script>
$(function(){
	
	timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'};
	$('#start_time').datetimepicker(timepickerlang);
	$('#end_time').datetimepicker(timepickerlang);
	
	$('#search').click(function(){
		var group = $('#group').val();
		var channel = $('#channel').val();
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		var serverStatus = $('#serverStatus').val();
		var name = $('#name').val();
		
		$.post('admin.php?ctrl=server&act=ajax_server', {'group' : group , 'channel' : channel , 'start_time' : start_time , 'end_time' : end_time , 'serverStatus' : serverStatus , 'name' : name , }, function(data){
			$('#server').html(data);	
		});
	});	
})

</script> 