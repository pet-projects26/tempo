<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="server-status-edit-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">状态</td>
                <td>
                    <select id="status" name="status">
                        <{foreach from=$status key=key item=item}>
                        <option value="<{$key}>"><{$item}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
           
            <!-- 服务器, 渠道 Start -->
            <{include file='../plugin/channelGroup_server.tpl' }>
            <!-- 服务器，  -->


            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('label').css('cursor','pointer');
        check();

        var error = $('#error');
        $('#server-status-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=server&act=status_edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('load' , 4);
                    }
                })
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //状态
        var status = $('#status').val();
        if(status == '0'){
            $.dialog.tips('未选择状态');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }

    //检测
    function check(){
        return false;

        //单个服务器选中
        $('.cb_server').click(function(){
            all_check();
            if($(this).attr('checked')){
                $(this).prev().attr('name' , 'server[]'); //提交时需要name属性获取值，所以选中的给他name属性
                $(this).next().css('color' , '#ff0000');
            }
            else{
                $(this).prev().attr('name' , '');
                $(this).next().css('color' , '#000000');
            }
        });
        //全选
        $('#checkAll').click(function(){
            if($(this).attr('checked')){
                $('.cb_server').attr('checked' , 'checked');
                $('.cb_server').prev().attr('name' , 'server[]');
                $('.cb_server').next().css('color' , '#ff0000');
            }
            else{
                $('.cb_server').attr('checked' , false);
                $('.cb_server').prev().attr('name' , '');
                $('.cb_server').next().css('color' , '#000000');
            }
        });
    }

    function all_check(){
        var all_check = 1;
        $('.cb_server').each(function(){
            if(!$(this).attr('checked')){
                all_check = 0;
            }
        });
        if(all_check){
            $('#checkAll').attr('checked' , 'checked');
        }
        else{
            $('#checkAll').attr('checked' , false);
        }
    }
</script>