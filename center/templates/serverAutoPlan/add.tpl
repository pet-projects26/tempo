<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="package-add-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 选择自动开服的渠道组<br>
                        2. 开服类型: 1: 创角数或付费数满足条件 2: 创角数与付费数都满足条件 3:根据单服自己的开服时间<br/>
                    </td>
                </tr>
                 <tr>
                    <td style="width:150px;">选择渠道组</td>
                    <td>
                        <select id="group_id" name="group_id">
                            <option value="0">未选择</option>
                            <{foreach from=$groups item=group}>
                            <option value="<{$group.id}>"><{$group.name}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td style="width:150px;">说明</td>
                    <td><input type="text" id="name" name="name" value="" style="width: 400px"></td>
                </tr>
                <tr>
                    <td style="width:150px;">选择开服类型</td>
                    <td>
                        <select id="type" name="type">
                            <option value="0">未选择</option>
                            <{foreach from=$AutoPlanType  key = key item=val}>
                            <option value="<{$key}>"><{$val}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td style="width: 150px">创角数</td>
                    <td><input type="number" id="create_role_num" name="create_role_num" value="0" style="width: 400px"/></td>
                </tr>

                <tr>
                    <td style="width: 150px">付费人数</td>
                    <td><input type="number" id="pay_num" name="pay_num" value="0" style="width: 400px"/></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">

    $(function(){

        var error = $('#error');
        $('#package-add-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
				var group_id =$("#group_id").find("option:selected").text();
				var name = $("#name").val();
                var type = $("#type").find("option:selected").val();
                var create_role_num = $("#create_role_num").val();
                var pay_num = $("#pay_num").val();
                var str = '确定选择：\n'+'渠道组：'+group_id+'\n'
                        +'说明：'+name+'\n'+'类型：'+type + '\n' +'创角数: '+ create_role_num + '\n'+'付费人数: '+ pay_num;

				if(confirm(str)){
					$.ajax({
                    url: 'admin.php?ctrl=serverAutoPlan&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
					}).done(function(data){
						$.dialog.tips(data.msg);
					})
				}
              return false;
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
		var group_id = $('#group_id').val();
        if(group_id == '0'){
            $.dialog.tips('未选择渠道组');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //说明
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写说明');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //类型
        var type = $('#type').val();
        if (type == '') {
            $.dialog.tips('未选择类型');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }

        //创角数
        var create_role_num = $('#create_role_num').val();
        //付费人数
        var pay_num = $('#pay_num').val();

        if(type == 1) {
            if (create_role_num === 0 && pay_num === 0) {
                $.dialog.tips('填写错误');
                error.val(1);
                return false;
            }
        } else if (type == 2) {
            if (create_role_num === 0 || pay_num === 0) {
                $.dialog.tips('填写错误');
                error.val(1);
                return false;
            }
        } else {
            error.val(0);
        }
    }

</script>