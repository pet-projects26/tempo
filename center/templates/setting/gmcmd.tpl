<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-gmcmd-form">
        <table class="itable itable-color">
            <tbody>
            <{include file='../plugin/channelGroup_server_radios.tpl' }>
            <tr>
                <td style="width:150px;">操作类型</td>
                <td>
                    <select name="type" id="type">
                        <option value="0">未选择</option>
                        <option value="1">角色ID</option>
                        <option value="2">角色区间ID</option>
                    </select>
                </td>
            </tr>
            <tr id="role_1" style="display: none;">
                <td style="width:150px;">角色ID</td>
                <td><input type="number" id="role_id" name="role_id" style="width:200px;"></td>
            </tr>
            <tr id="role_2" style="display: none;">
                <td style="width:150px;">角色区间ID</td>
                <td><input type="number" id="role_id_start" name="role_id_start" style="width:200px;">
                    -
                    <input type="number" id="role_id_end" name="role_id_end" style="width:200px;"></td>

            </tr>
            <tr>
                <td style="width:150px;">GM指令</td>
                <td><input type="text" id="cmd" name="cmd" style="width:200px;"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="发送">
                    <input type="hidden" value="1" id="error">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){

        $('#type').change(function () {

            var type = $(this).val();

            switch (type) {
                case '1':
                    $('#role_1').show();
                    $('#role_2').hide();
                    break;
                case '2':
                    $('#role_1').hide();
                    $('#role_2').show();
                    break;
                default:
                    $('#role_1').show();
                    $('#role_2').hide();
                    break;
            }
        });

        var error = $('#error');
        $('#manual-gmcmd-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=setting&act=gmcmd_action&save=1',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                })
            }
            return false;
        });
    });

    function form_check(){
        var error = $('#error');
        //服务器
        var server = $('#server');
        if(server.val() == 0){
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //角色ID
        var role_id = $('#role_id');
        var role_id_start = $('#role_id_start');
        var role_id_end = $('#role_id_end');

        var type = $('#type').val();

        if (type == 0) {
            $.dialog.tips('未选择操作类型');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

        if (type == 1) {
            if (role_id.val() == 0) {
                $.dialog.tips('未填写角色ID');
                error.val(1);
                return false;
            }
            else {
                error.val(0);
            }
        } else if (type == 2) {
            if (role_id_start.val() == 0 || role_id_end.val() == 0) {
                $.dialog.tips('请正确填写角色区间id');
                error.val(1);
                return false;
            }
            else {
                error.val(0);
            }
        }


        //GM指令
        var cmd = $('#cmd');
        if(cmd.val() == 0){
            $.dialog.tips('未填写GM指令');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>