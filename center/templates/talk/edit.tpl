<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm"  id="form" method="post">
    	<input type="hidden"  name="id" class="id" value="<{$data.id}>">
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input  type="text"  name="name" class="name" value="<{$data.name}>"> 必填项</td>
                </tr>
                <tr>
                    <td style="width:150px;">说明</td>
                    <td><textarea style="width:400px;height:100px;" name="content" class="content"><{$data.content}></textarea></td>
                </tr>
               
                <tr>
                    <td colspan="2"><input id="gbutton" type="button" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
    
</div>
<script type="text/javascript">
    
	$("#gbutton").click(function(){
		
		var name = $(".name").val();
		var id = $(".id").val();
		var content = $(".content").val();
		if(!name){
			alert('名称不能为空');
			return false;
		}
		$.ajax({
			url: 'admin.php?ctrl=talk&act=editsensit_action',
			type: 'POST',
			dataType: 'JSON',
			data:{'name':name,'id':id,'content':content},
		}).done(function(data){
			$.dialog.tips(data.msg);
			return false;
		})	
			
	});
</script>