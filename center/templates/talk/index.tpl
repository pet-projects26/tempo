
  <script src="templates/talk/jquery.pagination.js"></script>
  <style type="text/css">
    #contents tr{
      height: 80px;
    }
    input[type='radio']{
      vertical-align:middle;
    }
    .M-box a{
      background-color: #ddd;
      border: 1px solid #aaa;
      padding: 2px 5px;
      margin: 0 3px;
      cursor: pointer;
      border-radius: 2px;
      color: #333 !important;
    }

  </style>
  <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form  >
  <table class="itable itable-color">
  
	  <tbody >
	  	<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <{include file='../plugin/server.tpl' }>
        <!-- 服务器，  -->

        <tr>
          <td style="width:150px;">角色名</td>
          <td><input type="text" name="role_name" id="role_name" style="width:200px;"></td>
        </tr>
        <tr>
          <td style="width:150px;">关键字</td>
          <td><input type="text" name="keyword" id="keyword" style="width:200px;"></td>
        </tr>

        <tr>
          <td style="width:150px;">频道</td>
          <td>
          	<select name="type" id="type">
          		<option value="" >所有</option>
          		<option value="1" >喇叭</option>
          		<option value="2" >世界</option>
          		<option value="3" >组队</option>
          		<option value="4" >仙盟</option>
          		<option value="9" >世界入侵</option>
          		<option value="10" >区域入侵</option>
          		<option value="11" >私聊</option>
          	</select>
          </td>
        </tr>
         <tr>
         	<td>
		        <input type="button" class="gbutton" value="查询" id="check">    
		        <input type="button" class="gbutton" value="停止刷新" id="stop">
	        </td>
	        
	        <td>
		        <input type="button" class="gbutton" value="导出" id="export">
	        </td>
		</tr>
	  </tbody>
  
  </table>
  </form>
  </div>

  <div id="contents" style="margin-top: 10px;" class="ui-tabs ui-widget ui-widget-content ui-corner-all ">
    <table>
      <thead>
        <tr>
          <th>服务器</th>
          <th>渠道(包)</th>
          <th>频道</th>
          <th>角色名</th>
          <th>账号</th>
          <th>等级</th>
          <th>充值金额</th>
          <th>聊天内容</th>
          <th>时间</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        
      </tbody>
    </table>

  </div>
  <div class="M-box" style="margin-top: 10px; "></div>

  <div id = "ban" style="display: none;" >

      <div style="margin-top: 5px;">
        <input type="radio" name="type" value="1" checked="checked" > 封帐号  
        <input type="radio" name="type" value="2" > 封角色  
         <input type="radio" name="type" value="3" > 封IP  
        <input type="radio" name="type" value="4" > 封MAC  
        <input type="radio" name="type" value="5" > 禁言 
      </div>
       <div style="margin-top: 10px;">
          封禁原因:
          <select name="reason" id="reason">
              <option value="6">利用漏洞</option>
              <option value="5">辱骂他人</option>
              <option value="4">发布卖元宝信息</option>
              <option value="3">游戏刷屏</option>
              <option value="2">游戏拉人</option>
              <option value="1">游戏捣乱</option>
              <option value="0">其它</option>
          </select>

          封禁时间： 
          <select name="time" id="time">
              <option value="1800">30分钟</option>
              <option value="10800">3小时</option>
              <option value="43200">12小时</option>
              <option value="86400">1天</option>
              <option value="259200">3天</option>
              <option value="2592000">30天</option>
              <option value="0">永久</option>
          </select>
      </div>
      
      <div style="margin-top: 10px;"><input type="button" class="gbutton queding" value="确定">
      </div>

  </div>
  <script type="text/javascript">
  	$(function(){

      var content=20,//显示条数
      $p=0;//默认第一页
      
      var len,page;

      //获取数据
      function ajax_get($p){
        //获取数据  
        len=arr.length;//总共多少条 
        page=Math.ceil(len/content); //总共多少页 
        //显示内容
        begin=$p*content; //起始数据
        end=begin+content;//结束数据=起始数据+内页显示条数
        //如果结束数据大于总数，直接起始数据+总数据/每页显示取余数
        if(end>len){
          end=begin+(len%content)
        } 
        html = '';
        num = 0;
        
        for (var n =begin; n<end; n++) {

          if(num %2 ==0){

            html += '<tr class = "odd" >'; 

          } else{

            html  += '<tr class = "even" >';
          }

          html += '<td>'+arr[n]['server']+'</td>';
          html += '<td>'+arr[n]['channel']+'_' +arr[n]['package'] +'</td>';
          
          if(arr[n]['type'] == 1){

             html += '<td>喇叭</td>';

          }else if(arr[n]['type'] == 2){

             html += '<td>世界</td>';

          }else if(arr[n]['type'] == 3){

             html += '<td>组队</td>';

          }else if(arr[n]['type'] == 4){

             html += '<td>仙盟</td>';

          }else if(arr[n]['type'] == 9){

             html += '<td>世界入侵</td>';
          }else if(arr[n]['type'] == 10){

             html += '<td>区域入侵</td>';
          }else if(arr[n]['type'] == 11){

             html += '<td>私聊</td>';
          }else{
             html +='<td>未知频道</td>';
          }

          html += '<td>'+arr[n]['role_name']+'</td>';
          html += '<td>'+arr[n]['account']+'</td>';
          html += '<td>'+arr[n]['role_level']+'</td>';
          html += '<td>'+arr[n]['money']+'</td>';
          html += '<td>'+arr[n]['content']+'</td>';
          html += '<td>'+   new Date(parseInt(arr[n]['create_time']) * 1000).toLocaleString().replace(/:\d{1,2}$/,' ') +'</td>';
          html += '<td><span class = "ban" server = "'+arr[n]['server']+'" role_name = "'+arr[n]['role_name']+'" style="cursor:pointer">封禁</span></td>';
          html += '</tr>';
          num++;

        } 
        $(" #contents table  tbody").html(html);
        
        //如果是第一页，需要初始化分页
        if($p == 0){
            //初始化数据
            $('.M-box').pagination({
              pageCount:page, 
              coping:true,
              homePage:'首页',
              endPage:'末页',
              prevContent:'上页',
              nextContent:'下页',
            });
            $('.M-box').on('click' ,'a', function(){
              $p=$.trim($('.active').text());  
              ajax_get($p-1);
            });
        }

      }
      
      //弹出封禁框
      $('.ban').die().live('click',function(){
          server = $(this).attr('server');
          role_name = $(this).attr('role_name');
          $.dialog({
            title : '封禁', 
            width:400,
            max: false,
            min: false,
            content: $('#ban').html(),
          });
      });

      //确定封禁
      $('.queding').die().live('click' , function(){

        type = $("input[name = 'type']:checked").val();
        reason = $('#reason').val();
        time = $('#time').val();

        $.post('admin.php?ctrl=talk&act=ban',{'type':type,'server':server,'role_name':role_name},function(data){
             
              if(data.content == '' || data.content == 0){

                $.dialog.tips('封禁信息错误');return false;
              }else{
                $.post('admin.php?ctrl=ban&act=edit_action',{'type':type,'server':data.server,'content':data.content,'time':time , 'reason': reason},function(msg){
                    $.dialog.tips(msg.msg);
                },'json');
              }
        },'json');

      });

      stop = 0;

      //查询数据
  		$('#check').click(function(){

        ref = clearInterval(ref);
  			//请求数据
  			data();

  			//执行定时
  			setval();

        stop = 0;
        $('#stop').val('停止刷新');
  		});
  		  		
  		$('#stop').click(function(){
  			//关闭定时任务或者开启定时任务
  			if(stop == 0){ //关闭定时任务
        
  				ref = clearInterval(ref);
  				stop = 1;
  				$('#stop').val('开启刷新');
  			}else{ //开启定时

  				setval();
  				stop = 0;
  				$('#stop').val('停止刷新');
  			}
  		})

  		var ref = '';
  		//定时任务
  		function setval(){
  			ref = setInterval(function(){
    		
          data();

			  },60000);//每分钟执行一次
  		}
  		//获取数据源
  		function data(){

        var len = $('.server .p-checked').size();

        if(len == 0 ){
          
          $.dialog.tips('请选择要查询的服务器');
          return false;

        }
        if(len >5){
          $.dialog.tips('服务器最多只能勾选五个');
          return false;
        }
        $.ajax({
            url: 'admin.php?ctrl=talk&act=data_action',
            type: 'post',
            data:  $("form").serialize(),
            dataType: 'json'
          }).done(function(data){
            
            arr = data;
           
            ajax_get(0);
            
        }); 

  		}  		
  		
  		
  		
  		
  		$('#export').click(function(){
  		     
  		    var obj=$("input[name='server[]']");  
            server = [];
            for(k in obj){
                if(obj[k].checked)
                   server.push(obj[k].value);
            }
  		    var type = $("#type").val();
            var reason = $('#reason').val();
            var role_name = $('#role_name').val();
  		    window.location.href = 'admin.php?ctrl=talk&act=data_action&export=1&server='+server+'&type='+type+'&reason='+reason+'&role_name='+role_name;
  		})
  	})
  
  </script>
