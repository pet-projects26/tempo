<script type="text/javascript" src="templates/itemconfig/ajaxfileupload.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	
    <form action="" class="fm" id="manual-mail-form">
        <table class="itable itable-color">
            <tbody>
            
           <tr>
                <td style="width:150px;">注释:</td>
                <td>
                	1.导入时导名称即可,每一行有字数限制,不要重复导入以前的数据。2.文件格式:excel表格,第一行为:名称,不读取。3.若导入成功,列表那里没有显示数据,则请检查excel文件,重新导入
                </td>
            </tr>
          
           <tr>
                <td style="width:150px;">批量导入敏感字</td>
                <td>
                	<div data-role="fieldcontain" class="upload-box">
                   	 <input type="file" id="file" name="file" value="上传" />        
                   	 <input type="button"  value="提交" id="button"  class="gbutton" />    
        			</div>
                </td>
            </tr>
           
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
   $('#button').click(function(){
		var file = $('#file').val();
		
		if(file == ''){
			 $.dialog.tips('请选择要上传的文件');return false;	
		}

		$.ajaxFileUpload({
	        url:'admin.php?ctrl=talk&act=uploadCsv',   //处理文件的脚本路径
	        type: 'post',       //提交的方式
	        secureuri :false,   //是否启用安全提交
	        fileElementId :'file',     //file控件ID
	        dataType : 'json',  //服务器返回的数据类型      
	        success : function (data){  //提交成功后自动执行的处理函数
	        	 $.dialog.tips(data.msg);
	        },
	        error: function(data){   //提交失败自动执行的处理函数
	            $.dialog.tips(data.msg);
	        }
	    })
	});
	
</script>