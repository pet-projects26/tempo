<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm"  id="form" method="post">
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">时间(分钟)</td>
                    <td><input  type="text"  name="time" class="time" value="<{$time}>"></td>
                </tr>
               
                <tr>
                    <td colspan="2"><input id="gbutton" type="button" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
    
</div>
<script type="text/javascript">
    
	$("#gbutton").click(function(){
		
		var time = $(".time").val();
		if(!time){
			alert('时间不能为空');
			return false;
		}
		$.ajax({
			url: 'admin.php?ctrl=tooldeal&act=add_data',
			type: 'POST',
			dataType: 'JSON',
			data: {'time':time}
		}).done(function(data){
			if(data == true){
				$.dialog.tips('插入成功');
			}else{
				$.dialog.tips('插入失败');	
			}
			return false;
		})	
			
	});
</script>