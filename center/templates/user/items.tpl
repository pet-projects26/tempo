<table id="user_list" class="itable">
  <thead>
    <tr>
	  <th>用户名</th>
	  <th>姓名</th>
	  <th>部门</th>
	  <th>用户组</th>
	  <th>创建时间</th>
	  <th>最后登录时间</th>
	  <th>最后登录ip</th>
	  <th>过期时间</th>
	  <th>email</th>
	  <th>电话</th>
	  <th>账号状态</th>
	  <th>操作</th>
    </tr>
  </thead>
  <tbody>
    
      <{foreach from=$list item=item}>
      <tr>
      <td><{$item.username}></td>
      <td><{$item.name}></td>
      <td class="edit" id="<{$item.uid}>" field="department"><{$item.department}></td>
      <td class="edit_group" id="<{$item.uid}>" field="groupid"><{$item.groupname}></td>
      <td><{$item.created_format}></td>
      <td><{$item.last_login_time_format}></td>
      <td><{$item.last_login_ip}></td>
      <td><a uid="<{$item.uid}>" class="delay_account" href="javascript:void(0)" title="点击延长账号使用期限"><{$item.expiration_format}></td>
      <td class="edit" id="<{$item.uid}>" field="email"><{$item.email}></td>
      <td class="edit" id="<{$item.uid}>" field="tel"><{$item.tel}></td>
      <td><{$item.status_msg}></td>
      <td>
      <{if $item.status <> $smarty.const.USER_STATUS_NORMAL }>
      <a href="javascript:void(0)" onclick="forbid_user(<{$item.uid}>, 1)" title="封禁帐号"><font color="red">解</font></a>
      <{else}>
      <a href="javascript:void(0)" onclick="forbid_user(<{$item.uid}>, 2)" title="解封帐号">封</a>
      <{/if}>
      | 
      <a href="javascript:void(0)" onclick="del_user(<{$item.uid}>)" title="删除帐号">删</a> |
      <a href="javascript:void(0)" onclick="reset_password(<{$item.uid}>)" title="重置密码">密</a> |
      <a href="javascript:void(0)" onclick="clear_login_err('<{$item.username}>')" title="清空错误登录次数">清</a>
      </td>
      </tr>
      <{/foreach}>
    
  </tbody>
</table>
<script type="text/javascript">
$("#user_list tr").hover(
    function () {
       $(this).addClass("hover");
    },
    function () {
       $(this).removeClass("hover");
    }
);
var tTable = $("#user_list").dataTable({
  "oLanguage": {
    "sUrl": "style/js/dt_cn.t"
  },
  "aaSorting": [
	[ 3, "asc" ]
  ],
  "iDisplayLength":100,
  "sPaginationType": "full_numbers",
  "sDom": '<"top"lfp>rt<"bottom"ip><"clear">',
  "bFilter": true
});

$('.edit').editable("admin.php?ctrl=user&act=save_field", { 
    indicator : "<img src='style/images/indicator.gif'>",
    //loadurl   : "admin.php?ctrl=menu&act=get_field",
    height    : "15px",
    submit    : "修改",
    cancel    : "取消",
    tooltip   : "点击修改...",
    style  : "inherit"
});

$('.edit_group').editable("admin.php?ctrl=user&act=save_field", { 
    indicator : "<img src='style/images/indicator.gif'>",
    type   : 'select',
    data : <{$groupNames}>,
    //loadurl   : "admin.php?ctrl=menu&act=get_field",
    height    : "15px",
    submit    : "修改",
    cancel    : "取消",
    tooltip   : "点击修改...",
    style  : "inherit"
});

function forbid_user(uid, status){
    if(confirm('确定操作该账号?')){
        $.ajax({
          type: "POST",
          url: "admin.php?ctrl=user&act=save_field",
          data: "field=status&value="+status+"&id="+uid,
          timeout: 20000,
          error: function(){alert('error');},
          success: function(result){
               $tabs.tabs('load', 0);
          }
        });
    }
}

$('.delay_account').click(function(){
	var uid = $(this).attr('uid');
	var expiration_format = $(this).html();
	var this_element = $(this);
	$.ajax({
	       type: "POST",
	       url: "admin.php?ctrl=user&act=delay_account",
	       data: "uid="+uid+"&expiration_format="+expiration_format,
	       timeout: 20000,
	       error: function(){alert('error');},
	       success: function(result){
	    	   this_element.html(result);
	       }
	    });
});

function del_user(uid){
	if(confirm('确定删除该账号?')){
		$.ajax({
	      type: "POST",
	      url: "admin.php?ctrl=user&act=delete",
	      data: "uid="+uid,
	      timeout: 20000,
	      error: function(){alert('error');},
	      success: function(result){
	    	   $tabs.tabs('load', 0);
	      }
	    });
	}
}
function reset_password(uid){
    if(confirm('确定重置密码?')){
        $.ajax({
          type: "POST",
          url: "admin.php?ctrl=user&act=reset_password",
          data: "uid="+uid,
          timeout: 20000,
          error: function(){alert('error');},
          success: function(result){
        	  $.dialog.alert(result);
          }
        });
    }
}
function clear_login_err(username){
    if(confirm('确定清空错误登录次数?')){
        $.ajax({
          type: "POST",
          url: "admin.php?ctrl=user&act=clear_login_err",
          data: "username="+username,
          timeout: 20000,
          error: function(){alert('error');},
          success: function(result){
        	  $.dialog.alert(result);
          }
        });
    }
}

</script>