<form id="version-form">
<table class="itable">
    <tr>
      <td width="75px;">服务器标识</td>
      <td><{$server_flag}></td>
    </tr>
    <tr>
      <td>检查方式</td>
      <td>
        <select name="type" id="type">
          <option value="1">大于等级</option>
          <option value="2">小于等于</option>
          <option value="3">包含</option>
        </select>
      </td>
    </tr>
    <tr>
      <td>版本号</td>
      <td id="version-content"><input type="text" name="version"  value="<{$info.version}>" style="width:350px;height:25px;" /></td>
    </tr>
    <tr>
      <td>操作</td>
      <td><button class="gbutton">保存</button></td>
    </tr>
</table>
  <input type="hidden" name="action" value="submit">
  <input type="hidden" name="flag" value="<{$server_flag}>">
  <input type="hidden" name="id" value="<{$info.id}>">
</form>
 <style>
 .txt{margin-left:8px;margin-right:2px;}
 </style>
<script type="text/javascript">
$(function() {
  var type  = '<{$info.type}>';
  $('#type').change(function(event) {
      if( $(this).val() == 1 || $(this).val() == 2  ){
        $('#version-content').html('<input type="text" name="version"  value="<{$info.version}>" style="width:350px;height:25px;" />')
      }else{
        $('#version-content').html('<input type="text" name="version"  value="<{$info.version}>" style="width:350px;height:25px;" />&nbsp;<span>多个版本用#号隔开</span>');
      }
  });
  if( type ){
    $('#type option[value="'+type+'"]').prop('selected', 'true');
  } 
  $('#version-form').submit(function(event) { 
    $.ajax({
      url: 'admin.php?ctrl=server&act=version',
      type: 'POST',
      dataType: 'JSON',
      data: $(this).serialize() ,
    })
    .done(function( response ) {
      if( response.stase == true ){
        $.dialog.alert( response.msg );
      }else{
        $.dialog.alert( response.msg );
      }
    })
    return false ;
  });

});
</script>