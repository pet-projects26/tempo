<style type="text/css">
    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }

    .big-input {
        width: 500px;
    }
</style>
<script type="text/javascript" src="templates/vipnotice/vipnotice.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form id="notice-push-form">
        <table class="itable itable-color" level-num=1>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> 推送方式</legend>
                    <span class="p-span push-type">
					<input class="p-input push-type" checked type="checkbox" name="type[]" value="2"/>
					<label>邮件推送</label>
				    </span>
                    <span class="p-span push-type">
					<input class="p-input push-type" checked type="checkbox" name="type[]" value="1"/>
					<label>公告变更</label>
				    </span>
                    </fieldset>
                </td>
            </tr>
            <tr class="notice-setting">
                <td>
                    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                        <table>
                            <tbody>
                            <tr>
                                <td>开启档次</td>
                                <td>
                                    <input type="number" name="first_level[0]"/>&nbsp;&nbsp;元
                                </td>
                            </tr>
                            <tr>
                                <td>邮件标题</td>
                                <td>
                                    <input name="mail_title[0]" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>邮件内容</td>
                                <td>
                                    <textarea name="content[0]" class="content"
                                              style="width:600px;height:200px;margin:0;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>联系方式</td>
                                <td>
                                    <input type="text" name="connect[0]" class="big-input">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td>
                    <input type="button" class="gbutton del-level" value="删除该档次">
                </td>
            </tr>
            <tr class="add-level-btn">
                <td colspan="2">
                    <input type="button" class="gbutton add-level" value="添加充值档次"/>
                </td>
            </tr>

            <!-- 渠道组 Start -->
            <tr class='p-tr'>
                <td>
                    <fieldset>
                        <legend>渠道组</legend>
                    <{foreach from = $groups key = id item = row}>
			<span class="p-span c-channel-group  c-channel-group-list span-click">
				<input class="p-input" type="radio" name="channel_group" value="<{$id}>"
                       id="channel_group-<{$id}>"/>
				<label for="channel_group-<{$id}>" class="p-input"><{$row}></label>
			</span>
                    <{/foreach}>
                        </fieldset>
                </td>
            </tr>

            <!-- 渠道组 End -->

            <!-- 服务器 Start  -->

            <!-- 服务器 End -->
        </table>
    </form>
    <input id="commits" type="button" class="gbutton commits" value="确定">
</div>