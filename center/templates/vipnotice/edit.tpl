<style type="text/css">
    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }

    .big-input {
        width: 500px;
    }
</style>
<script type="text/javascript" src="templates/vipnotice/vipnotice.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form id="notice-push-form">
        <input type="hidden" name="id" value="<{$id}>">
        <table class="itable itable-color" level-num="<{getLastKey arr=$row.level}>">
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> 推送方式</legend>
                    <span class="p-span push-type">
					<input class="p-input push-type"<{if in_array(2,$row.type)}> checked <{/if}> type="checkbox" name="type[]" value="2"/>
					<label>邮件推送</label>
				    </span>
                    <span class="p-span push-type">
					<input class="p-input push-type" <{if in_array(1,$row.type)}> checked<{/if}>  type="checkbox" name="type[]" value="1"/>
					<label>公告变更</label>
				    </span>
                    </fieldset>
                </td>
            </tr>
            <{foreach from=$row.level key=key item=info}>
            <tr class="notice-setting">
                <td>
                    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                        <table>
                            <tbody>
                            <tr>
                                <td>开启档次</td>
                                <td>
                                    <input type="number" value="<{$info}>" name="first_level[<{$key}>]"/>&nbsp;&nbsp;元
                                </td>
                            </tr>
                            <tr>
                                <td>邮件标题</td>
                                <td>
                                    <input value="<{$row.title[$key]}>" name="mail_title[<{$key}>]" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>邮件内容</td>
                                <td>
                                    <textarea name="content[<{$key}>]" class="content"
                                              style="width:600px;height:200px;margin:0;"><{$row.content[$key]}></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>联系方式</td>
                                <td>
                                    <input type="text" value="<{$row.connect[$key]}>" name="connect[<{$key}>]" class="big-input">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td>
                    <input type="button" class="gbutton del-level" value="删除该档次">
                </td>
            </tr>
            <{/foreach}>
            <tr class="add-level-btn">
                <td colspan="2">
                    <input type="button" class="gbutton add-level" value="添加充值档次"/>
                </td>
            </tr>

            <!-- 渠道组 Start -->
            <tr class='p-tr channel_group'>
                <td>
                    <fieldset>
                        <legend> 当前渠道组下所有服务器</legend>
                    <{foreach from = $groups key = id item = row}>
			<span class="p-span c-channel-group  c-channel-group-list span-click <{if in_array($id, $data.agent)}> p-checked <{/if}>">
				<input class="p-input"  type="radio" name="channel_group"  value="<{$id}>" <{if in_array($id, $data.agent)}> checked <{else}> disabled="disabled" <{/if}>  id="channel_group-<{$id}>" />
				<label for="channel_group-<{$id}>" class="p-input" ><{$row}></label>
			</span>
                    <{/foreach}>
                    </fieldset>
                </td>
            </tr>

            <!-- 渠道组 End -->

            <!-- 服务器 Start  -->

            <!-- 服务器 End -->
        </table>
    </form>
    <input id="commits" type="button" class="gbutton commits" value="确定">
</div>