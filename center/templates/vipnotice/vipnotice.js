//document javascript for vipnotice
//添加充值档次
$().ready(function() {
    //其他渠道
    $('.c-channel-group-list input').unbind('click').bind('click', function() {
        var mark = $(this).prop('checked');
        $(".c-channel-group-list").removeClass('p-checked');
        if (mark) {
            $(this).parent('span').addClass('p-checked');
        } else {
            $(this).parent('span').removeClass('p-checked');
        }

        var checkLen = $('.c-channel-group-list input:checked').length;
        $(".server-fieldset").removeClass('server-fieldset');
        //根据渠道组筛选服务器
        InputValue=$(this).val();

        $('#'+InputValue).addClass('show');
        $("#"+InputValue).siblings().removeClass('show');
        $('.server fieldset').each(function () {  //遍历child,如果有show 就让他显示，没有就隐藏
            $('.server fieldset').eq(0).show();
            if ($(this).hasClass('show')) {
                $(this).show();
            } else {
                //清除选择
                $(this).children('span').removeClass('p-checked');
                $(this).children('span').find('.p-input').prop('checked',0);
                $(this).hide();
            }
        });
    });


    //当前渠道的所有服
    $('.s-server-click input[value=""]').unbind('click').bind('click', function() {
        var mark = $(this).prop('checked');

        if (mark) {
            $('.s-server-click  input[value != ""]').prop('checked', 0);
            $('.s-server-click  input[value != ""]').parent('span').removeClass('p-checked');

            $(this).parent('span').addClass('p-checked');
        } else {
            $(this).parent('span').removeClass('p-checked');
        }
    });

    // 非全服的其他服
    $('.s-server-click  input[value != ""]').unbind('click').bind('click', function() {
        var obj = '.s-server-click';
        var mark = $(this).prop('checked');
        if (mark) {

            $(this).parent('span').addClass('p-checked');
        } else {
            $(this).parent('span').removeClass('p-checked');
        }

        var allLen = $(obj + ' input').length;

        var checkLen = $(obj + ' input[value != ""]:checked').length;

        //如果都没有选中，则自动选中全选
        if (checkLen == 0) {
            $(obj + ' input[value=""]').prop('checked', 1);
            $(obj + ' input[value=""]').parent('span').addClass('p-checked');
        } else {
            $(obj + ' input[value=""]').prop('checked', 0);
            $(obj + ' input[value=""]').parent('span').removeClass('p-checked');
        }

    });
});
var push_type = [];
push_type[2]='邮件推送';
push_type[1]='公告变更';
$(".add-level").die();
$(".add-level").live('click',function(){
    var obj = $(this).parent('td').parent('tr').parent('tbody').parent('table');
    var level_num = obj.attr('level-num');
    level_num = parseInt(level_num);
    var first_level_name = 'first_level['+level_num+']';
    var mail_title_name = 'mail_title['+level_num+']';
    var content_name = 'content['+level_num+']';
    var connect_name = 'connect['+level_num+']';

    var htm = '<tr class="notice-setting">' +
            '<td>' +
                '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">' +
                    '<table>' +
                        '<tbody>' +
                            '<tr>' +
                                '<td>开启档次</td>'+
                                '<td>' +
                                '<input type="number" name="'+first_level_name+'" />'+
                                '</td>'+
                            '</tr>'+
                            '<tr>' +
                                '<td>邮件标题</td>'+
                                '<td>' +
                                    '<input type="text" name="'+mail_title_name+'">'+
                                '</td>'+
                            '</tr>'+
                            '<tr>' +
                                '<td>邮件内容</td>'+
                                '<td>' +
                                    '<textarea name="'+content_name+'" class="content" style="width:600px;height:200px;margin:0;"></textarea>'+
                                '</td>'+
                            '</tr>'+
                            '<tr>' +
                                '<td>联系方式</td>'+
                                '<td>' +
                                    '<input type="text" name="'+connect_name+'" class="big-input">'+
                                '</td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
            '</td>'+
            '<td><input id="commit" type="button" class="gbutton del-level" value="删除该档次"></td>'+
        '</tr>';
    level_num = level_num+1;
    obj.attr('level-num',level_num);
    $(this).parent('td').parent('tr').before(htm);//追加
});
//删除该档次
$(".del-level").die();
$(".del-level").live('click',function(){
    $(this).parent('td').parent('tr').remove();
});
//确认提交
$(".commits").die();
$(".commits").live('click',function(){
    var tips = '';
    var type = [];
    var channel_name = [];
    $(".push-type").each(function(){
        if($(this).prop('checked')){
            type.push($(this).val())
        }
    });
    var group = $("input[name='channel_group']:checked").parent('span');
    var group_name = group.children('label').html();
    if(typeof group_name == 'undefined'){
        alert('请选择渠道组');
        return false;
    }
    $(type).each(function(i,n){
        tips += push_type[n]+" , ";
    });
    tips = '推送方式为：'+tips+"\r";
    tips += '渠道组为：'+group_name;
    $.ajax({
        type:'post',
        url:'admin.php?ctrl=vipnotice&act=add_action',
        data:$("#notice-push-form").serialize(),
        dataType:'json',
        beforeSend:function(){
            $("#commits").attr({disabled:"disabled"});
            alert(tips);
            //此处应做一个加载框
        },
        success:function(data){
            $.dialog({
                'title':data.title,
                'max':false,
                'width':false,
                'content':data.html
            });
        },
        complete:function(){
            $("#commits").removeAttr('disabled');
        }
    })
});