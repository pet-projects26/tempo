<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="white-edit-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
            </div>
            <tbody>
            
            <tr>
                <td style="width:150px;">IP<br>(一行一个IP)</td>
                <td><textarea name="ip" style="width:600px;height:300px;margin:0;" id="ip"></textarea></td>
            </tr>
            
             <tr>
              	<td style="width:150px;">说明</td>
                <td ><input type="text"  name="contents" ></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
            </tr>
            
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        var error = $('#error');
        $('#white-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=whiteip&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
       
        //封禁内容
        var ip = $('#ip').val();
        if(ip == ''){
            $.dialog.tips('未填写IP');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>