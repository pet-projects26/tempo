<{if $id}>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="zone-edit-form">
        <div class="hidden">
            <input type="hidden" id="id" name="id" value="<{$id}>">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td colspan="2">
                    说明：<br>
                    1. 选择分区的渠道组<br>
                    2. 排序为倒叙 数字越大排序越前 <br/>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">选择渠道组</td>
                <td>
                    <select id="group_id" name="group_id">
                        <option value="0">未选择</option>
                        <{foreach from=$groups item=group}>
                        <option value="<{$group.id}>" <{if $group.id == $group_id}>selected="selected"<{/if}> ><{$group.name}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">名称</td>
                <td><input type="text" id="name" name="name" value="<{$name}>"></td>
            </tr>
            <tr>
                <td style="width:150px;">分区备注</td>
                <td><input type="text" id="remark" name="remark"  value="<{$remark}>"></td>
            </tr>
            <tr>
                <td style="width:150px;">cdn链接</td>
                <td><input type="text" id="cdn_url" name="cdn_url" value="<{$cdn_url}>"></td>
            </tr>
            <tr>
                <td style="width:150px;">排序</td>
                <td><input type="text" id="sort" name="sort" value="<{$sort}>"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<{else}>
请选择编辑的分区
<{/if}>
<script type="text/javascript">
    $(function(){
        var error = $('#error');
        $('#zone-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{

                var group_id =$("#group_id").find("option:selected").text();
                var name = $("#name").val();
                var remark = $("#remark").val();
                var cdn_url = $("#cdn_url").val();
                var sort = $("#sort").val();
                var str = '确定选择：\n'+'渠道组：'+group_id+'\n'
                    +'分区名：'+name+'\n'+'分区备注：'+ remark + '\n' +'cdn链接: '+ cdn_url + '\n'+'排序: '+ sort;

                if(confirm(str)) {
                    $.ajax({
                        url: 'admin.php?ctrl=zone&act=edit_action',
                        type: 'POST',
                        dataType: 'JSON',
                        data: $(this).serialize()
                    }).done(function (data) {
                        $.dialog.tips(data.msg);
                        if (data.code) {
                            $tabs.tabs('select', 0);
                        }
                    })
                }
            }
            return false;
        });
    });

    function form_check(){
        var error = $('#error');
        //分区名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写分区名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //分区备注
        var remark = $('#remark').val();

        if (remark == '') {
            $.dialog.tips('未填写分区备注');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        //cdn
        var cdn_url = $('#cdn_url').val();

        if (cdn_url == '') {
            $.dialog.tips('未填写分区加载的cdn资源链接');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }

        var sort = $('#sort').val();

        if (sort === 0) {
            $.dialog.tips('未填写分区排序');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>