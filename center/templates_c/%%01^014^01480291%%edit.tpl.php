<?php /* Smarty version 2.6.27, created on 2018-10-31 16:26:21
         compiled from zone/edit.tpl */ ?>
<?php if ($this->_tpl_vars['id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="zone-edit-form">
        <div class="hidden">
            <input type="hidden" id="id" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td style="width:150px;">名称</td>
                <td><input type="text" id="name" name="name" value="<?php echo $this->_tpl_vars['name']; ?>
"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择编辑的分区
<?php endif; ?>
<script type="text/javascript">
    $(function(){
        var error = $('#error');
        $('#zone-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=zone&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('select' , 0);
                    }
                })
            }
            return false;
        });
    });
    
    function form_check(){
        var error = $('#error');
        //分区名称
        var old_name = '<?php echo $this->_tpl_vars['name']; ?>
';
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写分区名称');
            error.val(1);
            return false;
        }
        else if(old_name == name){
            $.dialog.tips('分区名称没有改变');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>