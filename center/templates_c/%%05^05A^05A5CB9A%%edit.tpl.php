<?php /* Smarty version 2.6.27, created on 2018-10-20 17:32:32
         compiled from menu/edit.tpl */ ?>
<form method="post" class="form_menu_edit" action="admin.php?ctrl=menu&act=edit_submit&type=<?php echo $this->_tpl_vars['type']; ?>
">
    <table class="itable itable-color">
      <tr id="show_id" style="<?php if (! $this->_tpl_vars['menu']['id']): ?>display:none<?php endif; ?>;">
          <td>菜单id</td>
          <td>
          <input value="<?php echo $this->_tpl_vars['menu']['id']; ?>
" name="id" readonly="true"/>
          </td>
        </tr>
    <tr>
          <td>菜单名称</td>
          <td>
          <input type="text" value="<?php echo $this->_tpl_vars['menu']['name']; ?>
" name="name"/>
          </td>
        </tr>
        
        <tr>
          <td>链接地址</td>
          <td><input class="width-middle" type="text" value="<?php echo $this->_tpl_vars['menu']['url']; ?>
" name="url" /></td>
        </tr>
        
        <tr>
          <td>上级菜单</td>
          <td><select name="parent">
          <option value="">根</option>
          <?php $_from = $this->_tpl_vars['menuRoot']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tmp']):
?>
          <option value="<?php echo $this->_tpl_vars['tmp']['id']; ?>
"><?php echo $this->_tpl_vars['tmp']['name']; ?>
</option>
          <?php endforeach; endif; unset($_from); ?>
          
          </select></td>
        </tr>
<tr>
          <td>排序</td>
          <td><input type="text" value="<?php echo $this->_tpl_vars['menu']['weight']; ?>
" name="weight"/></td>
        </tr>
<tr>
          <td><input type="hidden"  name="type" value="<?php echo $this->_tpl_vars['type']; ?>
" /></td>
          <td><input type="submit" class="gbutton"  name="submit" value="<?php echo '提交'; ?>
" /></td>
        </tr>
       
      </table>
 </form>
 
<script type="text/javascript">
$('.form_menu_edit').ajaxForm({
    complete: function(xhr){
        var id = JSON.parse(xhr.responseText);
        if(id > 0){
            if($('#show_id input[name="id"]').val()){
                $.dialog.alert('修改成功');
            }
            else{
                $('#show_id input[name="id"]').val(id);
                $('#show_id').show();
                $.dialog.alert('添加成功');
            }
        }
        else if(id == '-1'){
        	 $.dialog.alert('操作失败失败');
        }
        else{
        	 $.dialog.alert('未知错误');
        }
    }
});
</script>