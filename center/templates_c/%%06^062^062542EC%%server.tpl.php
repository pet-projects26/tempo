<?php /* Smarty version 2.6.27, created on 2018-10-22 18:11:59
         compiled from server/server.tpl */ ?>
<style type="text/css">
.w-box-list {
	margin: 5px 0px;
}
h5 {
	border: 1px dashed #cc0;
	padding: 5px;
	margin: 10px 0px;
	text-align: center;
}
.contents {
	/*border: 2px solid #2799DC; */
	padding: 5px;
	margin-bottom: 10px;
}
</style>

<div> 渠道组：
  <select name="group" id="group">
    <?php $_from = $this->_tpl_vars['group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
    <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
    <?php endforeach; endif; unset($_from); ?>
  </select>
  渠道：
  <select name="channel" id="channel">
    <?php $_from = $this->_tpl_vars['channel']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
    <option value="<?php echo $this->_tpl_vars['item']['num']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
    <?php endforeach; endif; unset($_from); ?>
  </select>
  开服时间：
  <input type="text" id="start_time" />
  ~
  <input type="text" id="end_time" />
  状态：
  <select name="serverStatus" id="serverStatus">
    <?php $_from = $this->_tpl_vars['serverStatus']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
    <option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
    <?php endforeach; endif; unset($_from); ?>
  </select>
  名称：
  <input type="text" name="name" id="name" />
  <input type="button" class="gbutton" id="search"  value="搜索"/>
</div>
<div id="server"> <?php $_from = $this->_tpl_vars['rss']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
  <div class="contents" >
    <h5><?php echo $this->_tpl_vars['key']; ?>
</h5>
    <table cellpadding="0" cellspacing="0" border="1" class="table_list dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info" >
      <thead>
        <tr height="44px" role="row">
          <th width="80" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="编号" style="width: 74px;">编号</th>
          <th width="81" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="渠号" style="width: 75px;">渠号</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="名称" style="width: 111px;">名称</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="标识" style="width: 111px;">标识</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="分区" style="width: 111px;">分区</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="认证密钥" style="width: 111px;">认证密钥</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="状态" style="width: 111px;">状态</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="开服时间" style="width: 112px;">开服时间</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="开服天数" style="width: 112px;">开服天数</th>
          <th width="120" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="显示状态" style="width: 112px;">显示状态</th>
          <th width="300" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="操作" style="width: 277px;">操作</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php $_from = $this->_tpl_vars['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
      <tr class="odd"><!--<?php if ($this->_tpl_vars['key'] % 2 == 0): ?> odd <?php else: ?> even <?php endif; ?>-->
        <td class=" sorting_1"><?php echo $this->_tpl_vars['i']['num']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['channel_num']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['name']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['server_id']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['zone']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['mdkey']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['status']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['open_time']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['day']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['display']; ?>
</td>
        <td class=""><?php echo $this->_tpl_vars['i']['caozuo']; ?>
</td>
      </tr>
      <?php endforeach; endif; unset($_from); ?>
        </tbody>
      
    </table>
  </div>
  <?php endforeach; endif; unset($_from); ?> </div>
<script>
$(function(){
	
	timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'};
	$('#start_time').datetimepicker(timepickerlang);
	$('#end_time').datetimepicker(timepickerlang);
	
	$('#search').click(function(){
		var group = $('#group').val();
		var channel = $('#channel').val();
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		var serverStatus = $('#serverStatus').val();
		var name = $('#name').val();
		
		$.post('admin.php?ctrl=server&act=ajax_server', {'group' : group , 'channel' : channel , 'start_time' : start_time , 'end_time' : end_time , 'serverStatus' : serverStatus , 'name' : name , }, function(data){
			$('#server').html(data);	
		});
	});	
})

</script> 