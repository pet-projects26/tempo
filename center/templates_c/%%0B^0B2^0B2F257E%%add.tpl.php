<?php /* Smarty version 2.6.27, created on 2018-10-30 14:57:00
         compiled from channelgroup/add.tpl */ ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="cgroup-add-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" id="name" name="name" value=""></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        var error = $('#error');
        $('#cgroup-add-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=channelgroup&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //渠道组名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写渠道组名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>