<?php /* Smarty version 2.6.27, created on 2018-10-19 14:30:37
         compiled from itemconfig/upload.tpl */ ?>
<script type="text/javascript" src="templates/itemconfig/ajaxfileupload.js"></script>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  
	 <div data-role="fieldcontain" class="upload-box">
               
                    <input type="file" id="file" name="file" value="上传" />        

                    <input type="button"  value="提交" id="button"  class="gbutton" />    
               
        </div>
                     
</div>
<div class="file" >
            
        </div> 
<script>
$(function(){
	
	$('#button').click(function(){
		var file = $('#file').val();
		if(file == ''){
			 $.dialog.tips('请选择要上传的文件');return false;	
		}

		$.ajaxFileUpload({
	        url:'admin.php?ctrl=itemconfig&act=uploadCsv',   //处理文件的脚本路径
	        type: 'post',       //提交的方式
	        secureuri :false,   //是否启用安全提交
	        fileElementId :'file',     //file控件ID
	        dataType : 'json',  //服务器返回的数据类型      
	        success : function (data){  //提交成功后自动执行的处理函数
	      
	            $.dialog.tips(data.msg);
	        },
	        error: function(data){   //提交失败自动执行的处理函数
	            $.dialog.tips(data.msg);
	        }
	    })
	});
});
</script> 