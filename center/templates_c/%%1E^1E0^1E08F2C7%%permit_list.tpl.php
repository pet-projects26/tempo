<?php /* Smarty version 2.6.27, created on 2019-03-19 15:45:29
         compiled from permit/permit_list.tpl */ ?>
<style type="text/css">
    #permit_list_table td, th

    {text-align:left;}
</style>

<table class="itable">
    <tr>
        <button id="synchronize" class="gbutton">一键同步Menu权限</button>
    </tr>
    <tr></tr>
</table>

<form id="permit_add_<?php echo $this->_tpl_vars['type']; ?>
" method="post" action="admin.php?ctrl=permit&act=permit_add">
    <table class="itable" style="width:80%">
        <tr>
            <td>分组:</td>
            <td>
                <select name="group">
                    <option value="">请选择</option>
                    <?php $_from = $this->_tpl_vars['menuRoot']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tmp']):
?>
                    <option value="<?php echo $this->_tpl_vars['tmp']['id']; ?>
"><?php echo $this->_tpl_vars['tmp']['name']; ?>
</option>
                    <?php endforeach; endif; unset($_from); ?>
                </select>
            </td>
            <td rowspan="2">模块列表:
                <textarea type="text" name="modules" style="width:300px;height:60px;"></textarea></td>
            <td rowspan="2"><input type="submit" class="gbutton" name="submit" value="添加"/></td>
        </tr>
        <tr>
            <td>名称:</td>
            <td><input type="text" name="name"/></td>
        </tr>
    </table>
    <input type="hidden" name="type" value="<?php echo $this->_tpl_vars['type']; ?>
"/>
</form>
<hr/>
<table id="permit_list_table_<?php echo $this->_tpl_vars['type']; ?>
" class="itable">
    <tr>
        <th width="30%">名称</th>
        <th width="20%">模块</th>
        <th width="30%">操作</th>
    </tr>

    <?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group'] => $this->_tpl_vars['item']):
?>
    <tr>
        <td colspan="3"><h2><?php echo $this->_tpl_vars['group']; ?>
</h2></td>
    </tr>
    <?php $_from = $this->_tpl_vars['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['row']):
?>
    <tr id="permit_line_<?php echo $this->_tpl_vars['row']['pid']; ?>
">
        <td class="edit" id="<?php echo $this->_tpl_vars['row']['pid']; ?>
" field="name"><?php echo $this->_tpl_vars['row']['name']; ?>
</td>
        <td class="edit_textarea" id="<?php echo $this->_tpl_vars['row']['pid']; ?>
" field="modules"><?php echo $this->_tpl_vars['row']['modules_show']; ?>
</td>
        <td><a href="javascript:void(0)" onclick="permit_delete(<?php echo $this->_tpl_vars['row']['pid']; ?>
)">删除</a></td>
    </tr>
    <?php endforeach; endif; unset($_from); ?>
    <tr>
        <td colspan="3">
            <hr/>
        </td>
    </tr>
    <?php endforeach; endif; unset($_from); ?>
</table>


<script type="text/javascript">

    $('#synchronize').click(function () {
        $.dialog.confirm('你确定要同步和央服菜单的权限吗？', function () {
            $.ajax({
                type: "GET",
                url: "admin.php?ctrl=permit&act=permit_synchronize",
                data: {},
                error: function (){$.dialog.alert('同步失败');},
                success: function (result) {
                    console.log(result);
                    if (result == 1) {
                        $.dialog.tips('同步成功');
                    } else {
                        $.dialog.alert('同步失败');
                    }
                }
            });
        }, function () {
            $.dialog.tips('取消操作');
        });
    });

    // $('#permit_add_<?php echo $this->_tpl_vars['type']; ?>
 input[name="group"]').focus();
    $('#permit_list_table_<?php echo $this->_tpl_vars['type']; ?>
 tr:even').addClass('even');
    $('#permit_list_table_<?php echo $this->_tpl_vars['type']; ?>
 tr:odd').addClass('odd');
    $("#permit_list_table_<?php echo $this->_tpl_vars['type']; ?>
 tr").hover(
        function () {
            $(this).addClass("hover");
        },
        function () {
            $(this).removeClass("hover");
        }
    );
    $('#permit_add_<?php echo $this->_tpl_vars['type']; ?>
').ajaxForm({
        complete: function (xhr) {
            var msg = xhr.responseText;
            $.dialog.alert(msg);
            if (msg == '添加成功') {
            <?php if ($this->_tpl_vars['type'] == 'center'): ?>
                $tabs.tabs("load", 0);
            <?php else: ?>
                $tabs.tabs("load", 1);
            <?php endif; ?>
            }
        }
    });


    $('#permit_list_table_<?php echo $this->_tpl_vars['type']; ?>
 .edit').editable("admin.php?ctrl=permit&act=save_field", {
        indicator: "<img src='style/images/indicator.gif'>",
        height: "15px",
        submit: "修改",
        cancel: "取消",
        tooltip: "点击修改...",
        style: "inherit"
    });

    $('#permit_list_table_<?php echo $this->_tpl_vars['type']; ?>
 .edit_textarea').editable("admin.php?ctrl=permit&act=save_field", {
        indicator: "<img src='style/images/indicator.gif'>",
        height: "55px",
        submit: "修改",
        cancel: "取消",
        tooltip: "点击修改...",
        type: "textarea",
        loadurl: "admin.php?ctrl=permit&act=get_module_textarea",
        style: "inherit"
    });

    function permit_delete(pid) {
        $.dialog.confirm('你确定要删除这个吗？', function () {
            $.ajax({
                type: "POST",
                url: "admin.php?ctrl=permit&act=permit_delete",
                data: 'pid=' + pid,
                timeout: 20000,
                error: function (){$.dialog.alert('超时');},
                success: function (result) {
                    if (result == 'success') {
                        $.dialog.tips('删除成功');
                        $('#permit_line_' + pid).remove();
                    } else {
                        $.dialog.alert('删除失败');
                    }

                }
            });
        }, function () {
            $.dialog.tips('取消操作');
        });
    }



</script>