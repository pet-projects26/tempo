<?php /* Smarty version 2.6.27, created on 2018-10-29 10:51:40
         compiled from app/edit.tpl */ ?>
<?php if ($this->_tpl_vars['app_id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="app-edit-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
                <input type="hidden" id="old_app_id" name="old_app_id" value="<?php echo $this->_tpl_vars['app_id']; ?>
">
            </div>
            <tbody>
            <tr>
                <td colspan="2">
                    说明：<br>
                    1. 应用标识可以由数字，大小写字母，下划线（_）和点（.）组成
                </td>
            <tr>
                <td style="width:150px;">标识</td>
                <td><input type="text" id="app_id" name="app_id" value="<?php echo $this->_tpl_vars['app_id']; ?>
"></td>
            </tr>
            <tr>
                <td style="width:150px;">名称</td>
                <td><input type="text" id="name" name="name" value="<?php echo $this->_tpl_vars['name']; ?>
"></td>
            </tr>
            <tr>
                <td style="width:150px;">API密码</td>
                <td><input type="text" id="secret" name="secret" value="<?php echo $this->_tpl_vars['secret']; ?>
"></td>
            </tr>
            <tr>
                <td style="width:150px;">平台</td>
                <td>
                    <div style="padding-top:7px;">
                        <?php $_from = $this->_tpl_vars['platform']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                        <label style="margin-right:100px;position:relative;">
                            <input type="hidden" value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['item']['checked']): ?>name="platform[]"<?php endif; ?>>
                            <input type="checkbox" class="cb_platform" <?php if ($this->_tpl_vars['item']['checked']): ?>checked="checked"<?php endif; ?>>
                            <span style="position:absolute;top:-10px;width:90px;"><?php echo $this->_tpl_vars['item']['name']; ?>
</span>
                        </label>
                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择要编辑的应用
<?php endif; ?>
<script type="text/javascript">
    $(function(){
        var error = $('#error');
        $('#app-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=app&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('select' , 2);
                    }
                });
            }
            return false;
        });
        $('.cb_platform').click(function(){
            if($(this).attr('checked') == 'checked'){
                $(this).prev('input[type=hidden]').attr('name' , 'platform[]');
            }
            else{
                $(this).prev('input[type=hidden]').attr('name' , '');
            }
        });
    });
    function form_check(){
        var error = $('#error');
        //应用标识
        var app_id = $('#app_id').val();
        if(app_id == ''){
            $.dialog.tips('未填写应用标识');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //应用名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写应用名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //API密码
        var secret = $('#secret').val();
        if(secret == ''){
            $.dialog.tips('未填写应用API密码');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //平台
        var error_flag = 0;
        $('.cb_platform').each(function(){
            if($(this).attr('checked') == 'checked'){
                error_flag ++;
            }
        });
        if(error_flag == 0){
            $.dialog.tips('未选择平台');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>