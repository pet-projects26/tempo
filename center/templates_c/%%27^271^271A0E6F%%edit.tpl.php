<?php /* Smarty version 2.6.27, created on 2019-05-20 22:04:17
         compiled from white/edit.tpl */ ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="white-edit-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
            </div>
            <tbody>
            <tr>
                <td style="width:150px;">选择服务器</td>
                <td>
                    <select name="server" id="server">
                        <option value="0">未选择</option>
                        <?php $_from = $this->_tpl_vars['server']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                        <option value="<?php echo $this->_tpl_vars['item']['server_id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">IP<br>(用英文逗号隔开每个IP)</td>
                <td><textarea name="ip" style="width:600px;height:300px;margin:0;" id="ip"></textarea></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="添加"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        var error = $('#error');
        $('#white-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=white&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //服务器
        var server = $('#server').val();
        if(server == 0){
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //封禁内容
        var ip = $('#ip').val();
        if(ip == ''){
            $.dialog.tips('未填写IP');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>