<?php /* Smarty version 2.6.27, created on 2018-12-07 17:14:35
         compiled from online/statistics_chart_data.tpl */ ?>
<style>
    #real_chart{
        min-width:400px;
        height:400px;
        padding:20px;
        margin:6px auto;
        border:solid 1px #ABABAB;
        border-radius:5px;
    }
</style>
<div id="real_chart"><?php echo $this->_tpl_vars['real_chart']; ?>
</div>

<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;">
  <table cellpadding="0" cellspacing="0" border="0" class="oddeven px1">
    <thead>
      <tr height="44px">
        <th >时间</th>
        <th >在线角色数</th>
        <!--<th >离线挂机角色数</th> -->
      </tr>
    </thead>
    <tbody>
    <?php $_from = $this->_tpl_vars['x']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
    	<tr>
            <td> <?php echo $this->_tpl_vars['i']; ?>
</td>
            <td> <?php echo $this->_tpl_vars['y'][0][$this->_tpl_vars['key']]; ?>
</td>
        </tr>
     <?php endforeach; endif; unset($_from); ?>
    </tbody>
  </table>
</div>