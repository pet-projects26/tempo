<?php /* Smarty version 2.6.27, created on 2018-10-30 14:56:59
         compiled from channelgroup/edit.tpl */ ?>
<?php if ($this->_tpl_vars['id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="cgroup-edit-form">
        <div class="hidden">
            <input type="hidden" id="id" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" id="name" name="name" value="<?php echo $this->_tpl_vars['name']; ?>
"></td>
                </tr>
                <!-- <tr>
                    <td>
                        各渠道包含的包<br>
                        <label style="position:relative;">
                            <input type="checkbox" id="checkAll">
                            <span style="position:absolute;top:-10px;width:100px;">全选</span>
                        </label>
                    </td>
                    <td>
                        <?php $_from = $this->_tpl_vars['packages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['channel_name'] => $this->_tpl_vars['channel']):
?>
                        <?php if ($this->_tpl_vars['channel_name']): ?>
                        <div class="channel_package">
                            <div style="padding:8px 0;color:#228b22;font-size:15px;" class="channel">
                                <label style="position:relative;">
                                    <input type="checkbox" class="cb_channel">
                                    <span style="position:absolute;top:-9px;left:20px;width:600px;"><?php echo $this->_tpl_vars['channel_name']; ?>
</span>
                                </label>
                            </div>
                            <div style="border-bottom:1px solid #000000;padding-bottom:4px;" class="package">
                                <?php $_from = $this->_tpl_vars['channel']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['package']):
?>
                                <label style="margin-right:170px;position:relative;" class="cb_package">
                                    <input type="hidden" name="<?php if ($this->_tpl_vars['package']['checked']): ?>package[]<?php endif; ?>" value="<?php echo $this->_tpl_vars['package']['value']; ?>
">
                                    <input type="checkbox" <?php if ($this->_tpl_vars['package']['checked']): ?>checked="checked"<?php endif; ?>>
                                    <span style="position:absolute;top:-10px;width:160px;<?php if ($this->_tpl_vars['package']['checked']): ?>color:#ff0000;<?php endif; ?>"><?php echo $this->_tpl_vars['package']['name']; ?>
</span>
                                </label>
                                <?php if (( $this->_tpl_vars['num'] + 1 ) % 7 == 0): ?><br><?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; endif; unset($_from); ?>
                    </td>
                </tr> -->
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择编辑的渠道组
<?php endif; ?>
<script type="text/javascript">
    $(function(){
        $('label').css('cursor','pointer');
        check();
        var error = $('#error');
        $('#cgroup-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=channelgroup&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('select', 3);
                    }
                });
            }
            return false;
        });
    });

    function all_check(){
        var all_check = 1;
        $('.cb_channel').each(function(){
            if(!$(this).attr('checked')){
                all_check = 0;
            }
        });
        if(all_check){
            $('#checkAll').attr('checked' , 'checked');
        }
        else{
            $('#checkAll').attr('checked' , false);
        }
    }

    function channel_check(){
        $('.channel_package').each(function(){
            var checked = 1;
            $(this).children('.package').children('.cb_package').each(function(){
                if(!$(this).children('input[type=checkbox]').attr('checked')){
                    checked = 0;
                }
            });
            if(checked){
                $(this).children().children().children('.cb_channel').attr('checked' , 'checked');
            }
            else{
                $(this).children().children().children('.cb_channel').attr('checked' , false);
            }
        });
    }

    function check(){
        channel_check();
        all_check();
        //单个包选中
        $('label.cb_package input[type=checkbox]').click(function(){
            channel_check();
            all_check();
            if($(this).attr('checked')){
                $(this).prev().attr('name' , 'package[]'); //提交时需要name属性获取值，所以选中的给他name属性
                $(this).next().css('color' , '#ff0000');
            }
            else{
                $(this).prev().attr('name' , '');
                $(this).next().css('color' , '#000000');
            }
        });
        //同一渠道所有包选中
        $('.cb_channel').click(function(){
            all_check();
            var label = $(this).parent().parent().next().children('label');
            if($(this).attr('checked')){
                label.children('input[type=hidden]').attr('name' , 'package[]');
                label.children('input[type=checkbox]').attr('checked' , 'checked');
                label.children('span').css('color' , '#ff0000');
            }
            else{
                label.children('input[type=hidden]').attr('name' , '');
                label.children('input').attr('checked' , false);
                label.children('span').css('color' , '#000000');
            }
        });
        //全选
        $('#checkAll').click(function(){
            if($(this).attr('checked')){
                $('.cb_channel').attr('checked' , 'checked');
                $('label.cb_package input[type=hidden]').attr('name' , 'package[]');
                $('label.cb_package input[type=checkbox]').attr('checked' , 'checked');
                $('label.cb_package span').css('color' , '#ff0000');
            }
            else{
                $('.cb_channel').attr('checked' , false);
                $('label.cb_package input[type=hidden]').attr('name' , '');
                $('label.cb_package input[type=checkbox]').attr('checked' , false);
                $('label.cb_package span').css('color' , '#000000');
            }
        });
    }

    function form_check(){
        var error = $('#error');
        //渠道组名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写渠道组名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>