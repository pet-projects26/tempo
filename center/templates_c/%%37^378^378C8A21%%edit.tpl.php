<?php /* Smarty version 2.6.27, created on 2021-02-27 22:24:42
         compiled from maintenance/edit.tpl */ ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-hupdate-form">
        <table class="itable itable-color">
            <tbody>
            
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server_edit.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            
            <tr>
            	<td>版本号</td>
            	<td><input type="text" name="version" id="banben" value="<?php echo $this->_tpl_vars['version']; ?>
">
        
            	</td>

            </tr>

             <tr >
            	<td>渠道版本号</td>
            	<td id="qudaobanben"></td>

            </tr>
            <tr>
            	<td>维护开始时间</td>
            	<td><input type="text" name="starttime" class="time" value="<?php echo $this->_tpl_vars['starttime']; ?>
"></td>
            </tr>
            <tr>
            	<td>维护结束时间</td>
            	<td><input type="text" name="endtime" class="time" value="<?php echo $this->_tpl_vars['endtime']; ?>
"></td>
               
            </tr>

            <tr>
                <td colspan="2"><input type="button" class="gbutton" value="发送" id="charge"></td>
                <td><input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
"></td>

            </tr>
            </tbody>
        </table>
    </form>
</div>

<script type="text/javascript">

$(function(){
	timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'};
	$('.time').datetimepicker(timepickerlang);

	$('#charge').click(function(){
		$.post('admin.php?ctrl=maintenance&act=edit_action',$("form").serialize() , function(data){
			
			$.dialog.tips(data.msg);
			if(data.ret ==1){
				$tabs.tabs('select' , 0);
                $tabs.tabs('load' , 0);
			}
		},'json');
	})

	$("#banben").blur(function(){
		$.post('admin.php?ctrl=maintenance&act=check_version',$("form").serialize() , function(data){
			
			$('#qudaobanben').html(data);
		});
	})

		
	

	
})
	

</script>