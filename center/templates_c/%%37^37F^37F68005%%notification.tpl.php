<?php /* Smarty version 2.6.27, created on 2018-10-19 14:27:03
         compiled from push/notification.tpl */ ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="notification-add-form">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
                <input type="hidden" id="platform">
            </div>
            <tbody>
            <tr>
                <td colspan="2">
                    说明：<br>
                     1. 离线消息保留时长 指一条给某个用户的推送，如果该用户当前不在线，则会保存为离线消息，待该用户下次上线时继续推送给他。
                    可以通过该值为指定离线消息的时长。即在该时长范围内用户上线会继续收到推送，否则过期。默认时长为 1 天。最长为 10 天。
                    可以设置为 0，则表示不保留离线消息，即只有当前在线的用户才可以收到，所有不在线的都不会收到。<br>
                    2.定速推送 指把原本尽可能快的推送速度，降低下来，给定的n分钟内，均匀地向这次推送的目标用户推送。最大值为1400。未设置则不是定速推送。
                </td>
            <tr>
                <td style="width:150px;">应用</td>
                <td>
                    <select id="app" name="app">
                        <option value="0">未选择</option>
                        <?php $_from = $this->_tpl_vars['apps']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                        <option value="<?php echo $this->_tpl_vars['item']['app_id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                </td>
            </tr>
            <!--
            <tr>
                <td>推送目标</td>
                <td></td>
            </tr>
            -->
            <tr>
                <td style="width:150px;">平台</td>
                <td>
                    <div id="platform" style="padding-top:7px;">
                        <?php $_from = $this->_tpl_vars['platform']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                        <label style="margin-right:100px;position:relative;">
                            <input type="hidden" value="<?php echo $this->_tpl_vars['key']; ?>
">
                            <input type="checkbox" class="cb_platform" disabled="disabled">
                            <span style="position:absolute;top:-10px;width:90px;"><?php echo $this->_tpl_vars['item']; ?>
</span>
                        </label>
                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">标题</td>
                <td><input type="text" name="title" id="title" style="width:600px;"></td>
            </tr>
            <tr>
                <td style="width:150px;">推送内容</td>
                <td><textarea id="notice" name="notice" style="width:600px;height:200px;margin:0;"></textarea></td>
            </tr>
            <tr>
                <td style="width:150px;">发送时间</td>
                <td>
                    <div style="margin:5px;">
                        <label style="margin-right:10px;"><input type="radio" id="now" name="time" value="0"><span>立即推送</span></label>
                        <label style="margin-right:10px;"><input type="radio" id="cron" name="time" value="1"><span>定时推送</span></label>
                        <label style="margin-right:10px;"><input type="radio" id="limit" name="time" value="2"><span>定速推送</span></label>
                        <span class="cron">推送将在每天 <input type="text" class="timepicker" placeholder="选择定时推送时间"> 完成</span>
                        <span class="limit">推送将分布在 <input type="text" name="big_push_duration" placeholder="不能超过1440分钟" style="width:120px;"> 分钟内完成</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>离线消息保留时长</td>
                <td>
                    <select name="time_to_live" id="time_to_live">
                        <option value="86400" selected="selected">默认(1天)</option>
                        <option value="0">不保留</option>
                        <option value="60">1分钟</option>
                        <option value="600">10分钟</option>
                        <option value="3600">1小时</option>
                        <option value="10800">3小时</option>
                        <option value="43200">12小时</option>
                        <option value="259200">3天</option>
                        <option value="864000">10天</option>
                        <option value="-1">自定义</option>
                    </select>
                    <span id="custom"><input type="text" placeholder="自定义时间" style="width:80px;"> 秒</span>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="gbutton" value="提交"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $(function(){
        $('.timepicker').timepicker({
            timeOnlyTitle: '选择时间',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            currentText: '现在',
            closeText: '确定'
        });

        $('#now').attr('checked' , 'checked');
        $('.cron , .limit , #custom').css('display' , 'none');
        $('#now').click(function(){
            $('.cron , .limit').css('display' , 'none');
            $('.cron input').attr('name' , '');
            $('.limit input').attr('name' , '');
        });
        $('#cron').click(function(){
            $('.cron').css('display' , 'inline');
            $('.limit').css('display' , 'none');
            $('.cron input').attr('name' , 'time');
            $('.limit input').attr('name' , '');
        });
        $('#limit').click(function(){
            $('.limit').css('display' , 'inline');
            $('.cron').css('display' , 'none');
            $('.limit input').attr('name' , 'time');
            $('.cron input').attr('name' , '');
        });
        $('#time_to_live').change(function(){
            if($(this).val() < 0){
                $('#custom').css('display' , 'inline');
            }
        });
        $('#app').change(function(){
            var app = $('#app').val();
            if(app != 0){
                $.ajax({
                    url: 'admin.php?ctrl=app&act=getPlatform&aid=' + app,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    if(data.code){
                        if(data.platform){
                            $('.cb_platform').each(function(){
                                if($(this).prev().val() == data.platform){
                                    $(this).attr('disabled' , false).attr('checked' , 'checked');
                                    $(this).prev().attr('name' , 'platform[]');
                                }
                                else{
                                    $(this).attr('disabled' , 'disabled').attr('checked' , false);
                                    $(this).prev().attr('name' , '');
                                }
                            });
                        }
                        else{
                            $('.cb_platform').attr('disabled' , false).attr('checked' , 'checked');
                            $('.cb_platform').prev().attr('name' , 'platform[]');
                        }
                    }
                    else{
                        $.dialog.tips(data.msg);
                        return false;
                    }
                });
            }
            else{
                $('.cb_platform').attr('disabled' , 'disabled').attr('checked' , false);
            }
        });
        $('.cb_platform').change(function(){
            if($(this).attr('checked') == 'checked'){
                $(this).prev('input[type=hidden]').attr('name' , 'platform[]');
            }
            else{
                $(this).prev('input[type=hidden]').attr('name' , '');
            }
        });

        var error = $('#error');
        $('#notification-add-form').submit(function(event){
            var content = { platform:[] , audience: 'all' , notification:{ } , options:{ } };
            var platform;
            var title = $('#title').val(); //标题
            var notice = $('#notice').val(); //推送内容
            $('.cb_platform').each(function(){
                if($(this).attr('checked') == 'checked'){
                    platform =  $(this).next('span').html();
                    platform = platform.toLowerCase();
                    content.platform.push(platform); //平台
                    if(platform == 'android'){
                        content.notification.android = { };
                        content.notification.android.title = title;
                        content.notification.android.alert = notice;
                    }
                    else if(platform == 'ios'){
                        content.notification.ios = { };
                        content.notification.ios.title = title;
                        content.notification.ios.alert = notice;
                    }
                }
            });

            content.options.time_to_live = parseInt($('#time_to_live').val()); //离线消息保留时长

            var time_type = $('input[name=time]:checked').val();
            if(time_type == 1){
                content.cron = $('.cron input').val();  //定时
            }
            else if(time_type == 2){
                content.options.big_push_duration = parseInt($('.limit input').val()); //定速推送时长
            }

            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                var app = $('#app').val();
                $.post('admin.php?ctrl=push&act=notification&submit=1' , { content: JSON.stringify(content) , app: app } , function(data){
                    $.dialog.tips('已添加');
                });
            }
            return false;
        });
    });

    function form_check(){
        var error = $('#error');
        //应用
        var app = $('#app').val();
        if(app == 0){
            $.dialog.tips('未选择应用');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //平台
        var error_flag = 0;
        $('.cb_platform').each(function(){
            if($(this).attr('checked') == 'checked'){
                error_flag ++;
            }
        });
        if(error_flag == 0){
            $.dialog.tips('未选择平台');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //标题
        var title = $('#title').val();
        if(title == ''){
            $.dialog.tips('未填写标题');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //推送内容
        var notice = $('#notice').val();
        if(notice == ''){
            $.dialog.tips('未填写推送内容');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //定时，定速
        var time_type = $('input[name=time]:checked').val();
        if(time_type == 1){
            var cron = $('#time input').val();  //定时
            if(cron == ''){
                $.dialog.tips('未填写定时推送时间');
                error.val(1);
                return false;
            }
            else{
                error.val(0);
            }
        }
        else if(time_type == 2){
            var limit = $('.limit input').val(); //定速
            if(limit == ''){
                $.dialog.tips('未填写定速推送时间');
                error.val(1);
                return false;
            }
            else{
                error.val(0);
            }
        }
    }
</script>