<?php /* Smarty version 2.6.27, created on 2018-10-22 17:15:56
         compiled from permit/server_permit.tpl */ ?>
<form class="permit_setting_form" method="post" action="admin.php?ctrl=permit&act=group_permit_save">
<table class="itable itable-color">
<thead>
    <tr>
        <th>渠道权限设置：</th>
        <td><label>
            <input type="checkbox" name="all" value="1" <?php if ($this->_tpl_vars['usergroup']['channel'] == 'all'): ?>checked<?php endif; ?>/>所有渠道权限</label>
            (<font color="red">如果勾选所有权限，则直接认为具有所有权限</font>)
        </td>
    </tr>
</thead>
<tbody>
            <?php $_from = $this->_tpl_vars['channelList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['cl'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['cl']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['cl']['iteration']++;
?>
            <tr><td><label><input name="<?php echo $this->_tpl_vars['key']; ?>
" class="channel cp" type="checkbox" value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['item']['checked']): ?>checked<?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</label>
                <?php if ($this->_foreach['cl']['iteration'] == 1): ?>(如果勾选渠道，则认为具有该渠道的所有权限)<?php endif; ?>
                </td><td>
                <?php $_from = $this->_tpl_vars['item']['package']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
                    <label><input class="<?php echo $this->_tpl_vars['key']; ?>
 cp package" name="channel[<?php echo $this->_tpl_vars['key']; ?>
][]" type="checkbox" value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if ($this->_tpl_vars['v']): ?>checked<?php endif; ?>><?php echo $this->_tpl_vars['k']; ?>
</label>
                <?php endforeach; endif; unset($_from); ?>
            </td></tr>
            <?php endforeach; endif; unset($_from); ?>
    <tr>
        <td><input type="hidden" name="groupid" value="<?php echo $this->_tpl_vars['usergroup']['groupid']; ?>
"/>
        <input type="hidden" name="type" value="channel"/>
        </td>
        <td><input type="submit" class="gbutton" value="提交" name="submit" style="float:right;"/></td>
    </tr>
</tbody>
</table>
</form>
<script type="text/javascript">
    $(function() {
        $('input[name="all"]').click(function() {
            $('.cp').prop('checked', this.checked)
        })
        $('.channel').click(function() {
            var channel = $(this).val()
            $('.' + channel).prop('checked', $(this).prop('checked'))
        })

        $('.cp').click(function() {
            if (!$(this).prop('checked'))
                $('input[name="all"]').prop('checked', false)
        })
    })
</script>