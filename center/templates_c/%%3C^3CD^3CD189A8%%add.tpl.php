<?php /* Smarty version 2.6.27, created on 2019-05-07 21:31:49
         compiled from giftOrder/add.tpl */ ?>
<style>
    .type-list {
        overflow: hidden;
        margin-bottom: 30px;
    }

    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    #checkboxChannelList .select {
        background-color: #dcd8d8;
    }

    #checkboxChannelList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    #checkboxServerList .select {
        background-color: #dcd8d8;
    }

    #checkboxServerList ul li {
        display: inline-block;
        width: 150px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #cccccc;
        cursor: pointer;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
    }

    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<style>
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }

    .p-input {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .highlight {
        background: yellow;
        color: red;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }

    .all-checked {
        vertical-align: middle;
    }

    .select {
        background-color: #36ca95;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-order-form">
        <table class="itable itable-color">
            <tbody>
            <tr></tr>
            <!-- 渠道组 Start -->
            <!-- 渠道组 Start -->
            <tr class='p-tr'>
                <td style="width:150px">渠道组</td>
                <td>
                    <div id="pretime">
                        <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['row']):
?>
                        <span class="p-span c-channel-group  c-channel-group-list span-click action-are"
                              style="cursor: pointer">
				<input class="p-input" type="radio" name="channel_group" value="<?php echo $this->_tpl_vars['id']; ?>
" id="channel_group-<?php echo $this->_tpl_vars['id']; ?>
"/>
				<label for="channel_group-<?php echo $this->_tpl_vars['id']; ?>
" class="p-input"><?php echo $this->_tpl_vars['row']; ?>
</label>
			</span>
                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                </td>
            </tr>

            <!-- 渠道组 End -->

            <!-- 服务器 Start  -->
            <tr class="p-tr server">
                <td class="w-150">服务器</td>
                <td>
                    <?php $_from = $this->_tpl_vars['servers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['rows']):
?>
                    <fieldset class="server-fieldset" id="<?php echo $this->_tpl_vars['group_id']; ?>
" style="display: none">
                        <legend> <?php echo $this->_tpl_vars['groups'][$this->_tpl_vars['group_id']]; ?>
</legend>
                        <?php $_from = $this->_tpl_vars['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sign'] => $this->_tpl_vars['name']):
?>
                        <span class="p-span s-server-click">
					<input class="p-input" type="radio" name="server" value="<?php echo $this->_tpl_vars['sign']; ?>
" id="server-<?php echo $this->_tpl_vars['sign']; ?>
"/>
					<label for="server-<?php echo $this->_tpl_vars['sign']; ?>
" class="p-input"><?php echo $this->_tpl_vars['name']; ?>
</label>
				</span>
                        <?php endforeach; endif; unset($_from); ?>
                    </fieldset>
                    <?php endforeach; endif; unset($_from); ?>
                </td>
            </tr>
            <!-- 服务器 End -->
            <tr>
                <td style="width:150px;">填写角色id</td>
                <td>
                    <input type="text" value="" name="role_id" id="role_id" style="width:140px;">
                </td>
            </tr>
            <tr>
                <td style="width:150px;">选择档位</td>
                <td id="charge">
                    <select name="item" id="item" style="width:140px;">
                        <option value="0">未查询充值档位</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">填写平台订单号</td>
                <td>
                    <input type="text" value="" name="corder_num" id="corder_num" style="width:140px;">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="添加" id="charge">
                    <input type="hidden" id="error" value="1">
                    <input type="hidden" id="checkboxServer" name="checkboxServer" value=""/>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">

    $().ready(function () {
        //其他渠道
        $('.c-channel-group-list input').unbind('click').bind('click', function () {
            var mark = $(this).prop('checked');
            $(".c-channel-group-list").removeClass('p-checked');
            if (mark) {
                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }

            var checkLen = $('.c-channel-group-list input:checked').length;
            //$(".server-fieldset").removeClass('server-fieldset');
            //根据渠道组筛选服务器
            InputValue = $(this).val();
            var len = $('#' + InputValue).length;
            if (typeof len == 'undefined' || len == 0) {
                $(".server fieldset").each(function () {
                    //清除选择
                    console.log($(this));
                    $(this).children('span').removeClass('p-checked');
                    $(this).children('span').find('.p-input').prop('checked', 0);
                    $(this).hide();
                });
            } else {
                $('#' + InputValue).addClass('show');
                $("#" + InputValue).siblings().removeClass('show');
                $('.server fieldset').each(function () {  //遍历child,如果有show 就让他显示，没有就隐藏
                    //$('.server fieldset').eq(0).show();
                    if ($(this).hasClass('show')) {
                        $(this).show();
                    } else {
                        //清除选择
                        $(this).children('span').removeClass('p-checked');
                        $(this).children('span').find('.p-input').prop('checked', 0);
                        $(this).hide();
                    }
                });
            }
        });
        // 非全服的其他服
        $('.s-server-click  input[value != ""]').unbind('click').bind('click', function () {
            var obj = '.p-input';
            var mark = $(this).prop('checked');
            console.log($(this).val());

            //获取该服的充值档位
            getCharge($(this).val());

            if (mark) {
                $(this).parent('span').addClass('p-checked').siblings().removeClass('p-checked');
            }
            var checkLen = $(obj + ' input[value != ""]:checked').length;

            //如果都没有选中，则自动选中全选
            if (checkLen == 0) {
                $(obj + ' input[value=""]').prop('checked', 1);
                $(obj + ' input[value=""]').parent('span').addClass('p-checked');
            } else {
                $(obj + ' input[value=""]').prop('checked', 0);
                $(obj + ' input[value=""]').parent('span').removeClass('p-checked');
            }

        });
    });

    $(function () {
        $('table.itable tr td,table input,table select').css('margin-top', '3px').css('margin-bottom', '3px');
        var error = $('#error');
        $('#manual-order-form').submit(function (event) {
            form_check();
            if (error.val() == '1') {
                return false;
            }
            else {
                $.ajax({
                    type: 'post',
                    url: 'admin.php?ctrl=giftOrder&act=add_action',
                    data: $(this).serialize(),
                    dataType: 'json',
                    beforeSend: function () {
                        $("#charge").attr({disabled:"disabled"});
                    },
                    success: function (data) {
                        $.dialog.tips(data.msg);
                    },
                    complete: function () {
                        setTimeout(function (){ 
                                $("#charge").removeAttr('disabled');
                                }, 1000);
                    }
                })
            }
            return false;
        });
    });

    function form_check() {
        var error = $('#error');
        //服务器

        var server = $("input[name='server']").val();

        if (server == '') {
            $.dialog.tips('服务器不能为空');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }
        //角色ID
        var role = $('#role_id');
        if (role.val() == '') {
            $.dialog.tips('未填写角色id');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
        //充值包
        var item = $('#item');
        if (item.val() == '0') {
            $.dialog.tips('未选择充值包');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }

        //平台订单号
        var corder_num = $('#corder_num');

        if (corder_num.val() == '') {
            $.dialog.tips('未填写平台订单号');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }

    }

    var chargeServer = {};

    //获取充值包 并且写入html中
    function getCharge(server) {

        //判断chargeServer里是否有 当前server的charge
        var charge = chargeServer[server];

        var chargeHtml = $('#charge');

        var formatCharge = "<select name=\"item\" id=\"item\" style=\"width:140px;\">\n" +
            "            <option value=\"0\">未查询充值档位</option>\n" +
            "            </select>";

        chargeHtml.html(formatCharge);

        if (!charge) {
            //如果不存在 , 则ajax获取
            $.ajax({
                type: 'post',
                url: 'admin.php?ctrl=charge&act=getServerCharge',
                data:{'server': server},
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.code == 400) {
                        $.dialog.tips(data.msg);
                        return
                    }
                    charge = data.charge;
                    chargeServer[server] = charge;
                }
            })
        }

        formatCharge = " <select name=\"item\" id=\"item\" style=\"width:140px;\">\n" +
            "            <option value=\"0\">未选择</option>";

        //遍历写在充值档位html里
        $.each(charge, function () {
            formatCharge += "<option value='" + this.index + "'>" + this.index + "-" + this.name + "</option>";
        });

        formatCharge += "</select>";

        chargeHtml.html(formatCharge);
    }
</script>