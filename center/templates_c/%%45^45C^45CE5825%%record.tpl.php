<?php /* Smarty version 2.6.27, created on 2019-03-08 15:00:33
         compiled from rank/record.tpl */ ?>
<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">



 <table class="itable itable-color">
  
	  <tbody >
	  	<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/server_single.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <!-- 服务器，  -->

	    <tr class='p-tr'> 
	    	<td>排行类型：</td>
	    	<td>
			    <select name="type" id="type">
                    <?php $_from = $this->_tpl_vars['rankType']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['row']):
?>
                    <option value="<?php echo $this->_tpl_vars['id']; ?>
"><?php echo $this->_tpl_vars['row']['name']; ?>
</option>
                    <?php endforeach; endif; unset($_from); ?>
			    </select>
	      	</td>
    	</tr>
	    
         <tr>
         	<td colspan="2">
         		<input type="button" class="gbutton" onclick="_online();" value="搜索">
         	</td>
         	
         </tr>
	  </tbody>
  
  </table>
	
</div>


<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;" id="all">
  <table cellpadding="0" cellspacing="0" border="0" class="oddeven px1">
    <thead>
      <tr height="44px">
          <th>角色名</th>
          <th>VIP等级</th>
          <th>VIP经验</th>
        <th >排行名次</th>
          <th id="name">参数</th>
          <th>时间</th>
      </tr>
    </thead>
    <tbody id="tbody">
      </tbody>
  </table>
</div>
<script type="text/javascript">  
    $(function(){
	   $('.datepicker').datepicker({maxDate:0});
    });

    var rankType = [];

    getRankType();

    function getRankType() {
        $.ajaxSettings.async = false;
        $.post("admin.php?ctrl=rank&act=getRankType", '', function (data) {
            rankType = data;
        }, "json");
        $.ajaxSettings.async = true;
    }

	function _online(){
		var type=$("#type").val();
		var server= $(".p-checked input[name='server']").val();
		if(typeof(server) == "undefined"){
			$.dialog.tips('请选择服务器');	return false;		
		}
        var name = rankType[type].name;

        $.post("admin.php?ctrl=rank&act=record_data",{"server":server,"type":type}, function (data) {

            if (data.errno == 2) {
                $.dialog.tips(data.error);
                return false;
            }

			$("#name").html(name);
            $('#tbody').html(data);
		},"json");
	}

    /*
    function detail(time,event){
        var color=$(event.currentTarget);
        var type=$("#type").val();
        var server= $(".p-checked input[name='server']").val();
        if(typeof(server) == "undefined"){
            $.dialog.tips('请选择服务器');	return false;
        }
        $.post("admin.php?ctrl=rank&act=getdata",{"server":server,"type":type,"time":time},function(data){
			$('#tbody').html(data);
			$('#hour a span').css('color','black');
			color.css('color','red');
		},"json");

	}*/
</script>