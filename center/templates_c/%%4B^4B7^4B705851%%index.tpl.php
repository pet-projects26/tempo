<?php /* Smarty version 2.6.27, created on 2018-12-03 14:49:41
         compiled from shell2/index.tpl */ ?>
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/animate.min.css">
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/jquery.gDialog.css">
<style type="text/css">
    .p-input {
        vertical-align:middle;
    }

    .p-text {
        vertical-align:middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
    button { border:0;}
    .container { margin:50px auto; max-width:728px;text-align:center;font-family:Arial;}
    .btn {background-color:#ED5565; color:#fff; padding:20px; margin:10px 30px; border-radius:5px; border-bottom:3px solid #DA4453;}
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <!--<form action="" class="fm" id="manual-hupdate-form"> -->
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td colspan="2">
                   当前node服务器状态为: <input type="submit"  id="node_status" disabled value="已启动">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" data-type="1" id="start" value="启动">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" data-type="2" id="close" value="关闭">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" data-type="3" id="close" value="更新SVN">
                </td>
            </tr>
            </tbody>
        </table>
    <!--</form> -->
</div>

<script type="text/javascript">

    $(function () {

        $('#node_status').val('已启动').css('color', 'green');

        var node_status = <?php echo $this->_tpl_vars['node_status']; ?>
;

        if (node_status != 1) {
            $('#node_status').val('已关闭').css('color', 'red');
        }

    });

    $('.gbutton').click(function() {

        var type = $(this).attr('data-type');

        var str = $(this).val();

        $.gDialog.confirm("确认要"+ str +"node?", {
            title: "操作确认",
            onSubmit: function() {
                $.ajax({
                    url: 'admin.php?ctrl=shell2&act=action',
                    type: 'POST',
                    timeout: 0,
                    dataType: 'JSON',
                    data: {type : type},
                    success(res){

                        if (res.node_status == 1) {
                            $('#node_status').val('已启动').css('color', 'green');
                        } else {
                            $('#node_status').val('已关闭').css('color', 'red');
                        }

                        $.gDialog.alert(res.message, {
                            title: res.title,
                        });
                    }
                })
            }
        });

    });
</script>