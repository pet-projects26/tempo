<?php /* Smarty version 2.6.27, created on 2018-10-20 10:01:35
         compiled from role/logout.tpl */ ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-logout-form">
        <div class="hidden">
            <input type="hidden" value="1" id="error">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td colspan="2">
                        <div style="margin:15px 0;">
                            说明：<br>
                            1. 填写角色ID，点击下线发送强制玩家下线的指令
                        </div>
                    </td>
                </tr>
                <tr>
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server_radios.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </tr>
                <tr>
                    <td style="width:150px;">角色ID</td>
                    <td><input type="text" id="role_id" name="role_id" style="width:200px;"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" class="gbutton" value="下线">
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $(function(){
        var error = $('#error');
        $('#manual-logout-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=role&act=manual_logout_action&save=1',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //服务器
        var server = $('#server');
        if(server.val() == 0){
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //角色ID
        var role_id = $('#role_id').val();
        if(role_id == ''){
            $.dialog.tips('未填写角色ID');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>