<?php /* Smarty version 2.6.27, created on 2018-10-19 10:54:34
         compiled from online/multi_chart.tpl */ ?>
<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">
    <div style="margin:5px;border:solid 1px #ABABAB;border-radius:5px;padding:4px 15px;line-height:34px;">
        <label>
            日期：
            <input type="radio" name="type" value="1" checked="checked">
            <input type="text" name="start_time" class="datepicker" style=" width:120px;" value="<?php echo $this->_tpl_vars['start_time']; ?>
"> ~
            <input type="text" name="end_time" class="datepicker" style=" width:120px;" value="<?php echo $this->_tpl_vars['end_time']; ?>
">
        </label>&nbsp;
        <label><input type="radio" name="type" value="2">最近60天</label>&nbsp;
        <label><input type="radio" name="type" value="3">最近90天</label>&nbsp;
        <input type="button" id="filter" class="gbutton" value="筛选">
        <input type="button" class="gbutton" onclick="_online();" value="搜索">
    </div>
    <div><?php echo $this->_tpl_vars['scp']; ?>
</div>
    <div id="total" style="margin:0 5px;"></div>
</div>
<script language="javascript">
    function _online(){
        var type = $('[name=type]:checked').val();
        var start_time = $('[name=start_time]').val();
        var end_time = $('[name=end_time]').val();
        var scp = JSON.stringify($('#checkboxScp').data());
        $.post('admin.php?ctrl=online&act=multi_data' , {type:type,start_time:start_time,end_time:end_time,scp:scp} , function(html){
            $('#total').html(html);
        });
    }
    $('.datepicker').datepicker();
    _online();
</script>