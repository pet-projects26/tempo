<?php /* Smarty version 2.6.27, created on 2019-03-14 16:23:12
         compiled from setting/cleansql.tpl */ ?>
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/animate.min.css">
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/jquery.gDialog.css">
<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">
 <table class="itable itable-color">
  
	  <tbody >
      <tr>
          <td colspan="2">
              <div style="margin:15px 0;">
                  说明：<br>
                  该功能只作测试使用, 使用后会清空对应服务器的数据库, 请谨慎使用<br>
              </div>
          </td>
      </tr>
	  	<!-- 主要用于进行一个颜色错位 -->
		<tr></tr>
		<!-- 主要用于进行一个颜色错位 -->
        
        <!-- 服务器, 渠道 Start -->
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/server_single.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <!-- 服务器，  -->
         <tr>
         	<td colspan="2">
         		<input type="button" class="gbutton" id="clean" value="清除">
         	</td>
         	
         </tr>
	  </tbody>
  
  </table>
	
</div>
<script type="text/javascript">
    $(function(){
	   $('.datepicker').datepicker({maxDate:0});
    });

/*
	function _clean(){
		var server= $(".p-checked input[name='server']").val();
		if(typeof(server) == "undefined"){
			$.dialog.tips('请选择服务器');
			return false;
		}
        var name = rankType[type].name;

        $.post("admin.php?ctrl=setting&act=cleansql_action",{"server":server,"save":1}, function (data) {


		},"json");
	}
*/
    $('#clean').click(function () {

        var server= $(".p-checked input[name='server']").val();
        if(typeof(server) == "undefined"){
            $.dialog.tips('请选择服务器');
            return false;
        }
        //根据切换的服务器查询服务器状态
        $.gDialog.confirm("确认要清除该服务器吗?", {
            title: "操作确认",
            onSubmit: function () {
                $.ajax({
                    url: 'admin.php?ctrl=setting&act=cleansql_action',
                    type: 'POST',
                    timeout: 0,
                    dataType: 'JSON',
                    data: {"server":server,"save":1},
                    success(res) {
                        console.log(res);

                        if (res.state) {
                            $.gDialog.alert('成功清空数据库', {
                                title: '成功!',
                            });
                        } else {
                            $.gDialog.alert(res.msg, {
                                title: '失败!',
                            });
                        }
                    }
                })
            },
            onCancel: function () {

            }
        });
    });

</script>