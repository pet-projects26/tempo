<?php /* Smarty version 2.6.27, created on 2018-10-29 10:53:15
         compiled from channel/edit.tpl */ ?>
<?php if ($this->_tpl_vars['channel_id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-edit-form">
        <div class="hidden">
            <input type="hidden" id="old_channel_id" name="old_channel_id" value="<?php echo $this->_tpl_vars['channel_id']; ?>
">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 每个渠道的标识不能重复<br>
                        2. 渠道标识只能由数字，小写字母，大写字母，下划线（_）和横杠（-）组成<br>
                        3. 渠道编号只能由数字组成，并且不能大于255，数字越小越好，编号在添加之后不能再做修改<br>
                        4. 可以为该渠道选择权限用户组,一旦选择了权限用户组,不可剔除,就算这里剔除成功了,<权限管理>那里也不会更新的,若要变更该渠道不属于该用户组,只能到<权限管理>那里重新选择,提交。   
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;">标识</td>
                    <td><input type="text" id="channel_id" name="channel_id" value="<?php echo $this->_tpl_vars['channel_id']; ?>
"></td>
                </tr>
                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" id="name" name="name" value="<?php echo $this->_tpl_vars['name']; ?>
"></td>
                </tr>
                <tr>
            		<td >选择权限用户组</td>
            		<td>
                		<select name="groupList[]" id="groupList" multiple="true" style="width: 400px">
                    		<?php $_from = $this->_tpl_vars['groupList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    			<option value="<?php echo $this->_tpl_vars['item']['groupid']; ?>
" <?php if (in_array ( $this->_tpl_vars['item']['groupid'] , $this->_tpl_vars['groupuser'] )): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                    		<?php endforeach; endif; unset($_from); ?>
                		</select>
            		</td>
        		</tr>
                <tr>
                    <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择编辑的渠道
<?php endif; ?>
<script type="text/javascript">
    $(function(){
    $('#groupList').select2();
        var error = $('#error');
        $('#channel-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=channel&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('select' , 0);
                    }
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        var old_channel_id = '<?php echo $this->_tpl_vars['channel_id']; ?>
';
        var old_name = '<?php echo $this->_tpl_vars['name']; ?>
';
        var channel_id = $('#channel_id').val();
        var name = $('#name').val();
        
        var old_groupuser = '<?php echo $this->_tpl_vars['groupuserstr']; ?>
';
        var new_groupuser = $('#groupList').val();

        if(old_channel_id == channel_id && old_name == name  && old_groupuser ==  new_groupuser){
            $.dialog.tips('没有任何改变');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //渠道标识
        if(channel_id == ''){
            $.dialog.tips('未填写渠道标识');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //渠道名称
        if(name == ''){
            $.dialog.tips('未填写渠道名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>