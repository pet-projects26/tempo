<?php /* Smarty version 2.6.27, created on 2019-06-13 15:37:30
         compiled from userinfo/index.tpl */ ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-gmcmd-form">
        <table class="itable itable-color">
            <tbody>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server_radios.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <tr>
                <td style="width:150px;">选择搜索类型</td>
                <td>
                    <select name="search_type" id="search_type">
                        <option value="0">未选择</option>
                        <option value="1">玩家账号</option>
                        <option value="2">角色ID</option>
                        <option value="3">角色名</option>
                    </select>
                </td>
            </tr>
            <tr id="search_type_1" style="display: none">
                <td style="width:150px;">玩家账号</td>
                <td><input type="text" id="account" name="account" style="width:200px;"></td>
            </tr>
            <tr id="search_type_2" style="display: none">
                <td style="width:150px;">角色ID</td>
                <td><input type="number" id="role_id" name="role_id" style="width:200px;"></td>
            </tr>
            <tr id="search_type_3" style="display: none">
                <td style="width:150px;">角色名</td>
                <td><input type="text" id="role_id" name="role_name" style="width:200px;"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="查询">
                    <input type="hidden" value="1" id="error">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="select_info" style="display: none">
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 10px;">
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td>
                    登陆玩家账号:
                    <button type="button" class="gbutton" onclick="loginAccount()">登陆</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!--角色基本信息-->
        <table class="itable itable-color">
            <caption>基本信息</caption>
            <tbody>
            <tr>
                <td style="width:150px;">账号</td>
                <td style="width:150px;">角色名</td>
                <td style="width:150px;">ID</td>
                <td style="width:150px;">等级</td>
                <td style="width:150px;">转生数</td>
                <td style="width:150px;">战力</td>
                <td style="width:150px;">仙位</td>
                <td style="width:150px;">剩余金币</td>
                <td style="width:150px;">剩余元宝</td>
                <td style="width: 150px">绑定元宝</td>
                <td style="width:150px;">充值金额</td>
                <td style="width:150px;">VIP等级</td>
                <td style="width:150px;">仙府等级</td>
            </tr>

            <tr>
                <td id="game_account"></td>
                <td id="game_name"></td>
                <td id="id"></td>
                <td id="level"></td>
                <td id="eraLvleraNum"></td>
                <td id="power"></td>
                <td id="xianwei"></td>
                <td id="copper"></td>
                <td id="gold"></td>
                <td id="bind_gold"></td>
                <td id="money">未知</td>
                <td id="VipLvl"></td>
                <td id="xianfuLvl"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色武器信息-->
        <table class="itable itable-color">
            <caption>角色装备信息</caption>
            <tbody>
            <tr>
                <td style="width: 50px"></td>
                <td style="width:150px;">武器</td>
                <td style="width:150px;">帽子</td>
                <td style="width:150px;">衣服</td>
                <td style="width:150px;">护手</td>
                <td style="width:150px;">鞋子</td>
                <td style="width:150px;">腰带</td>
                <td style="width:150px;">项链</td>
                <td style="width:150px;">手镯</td>
                <td style="width:150px;">戒指</td>
                <td style="width:150px;">玉佩</td>
            </tr>
            <tr>
                <td>名称</td>
                <td id="weapon_name">2</td>
                <td id="hats_name">3</td>
                <td id="clothes_name">4</td>
                <td id="hand_name">5</td>
                <td id="shoes_name">6</td>
                <td id="belt_name">7</td>
                <td id="necklace_name">8</td>
                <td id="bangle_name">9</td>
                <td id="ring_name">10</td>
                <td id="jude_name">11</td>
            </tr>
            <tr>
                <td>强化</td>
                <td id="weapon_strong">2</td>
                <td id="hats_strong">3</td>
                <td id="clothes_strong">4</td>
                <td id="hand_strong">5</td>
                <td id="shoes_strong">6</td>
                <td id="belt_strong">7</td>
                <td id="necklace_strong">8</td>
                <td id="bangle_strong">9</td>
                <td id="ring_strong">10</td>
                <td id="jude_strong">11</td>
            </tr>
            <tr>
                <td>仙石</td>
                <td id="weapon_gems">2</td>
                <td id="hats_gems">3</td>
                <td id="clothes_gems">4</td>
                <td id="hand_gems">5</td>
                <td id="shoes_gems">6</td>
                <td id="belt_gems">7</td>
                <td id="necklace_gems">8</td>
                <td id="bangle_gems">9</td>
                <td id="ring_gems">10</td>
                <td id="jude_gems">11</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色灵宠信息-->
        <table class="itable itable-color">
            <caption>角色灵宠信息</caption>
            <tbody>
            <tr>
                <td style="width:150px;">培养等级</td>
                <td style="width:150px;">进阶阶数</td>
                <td style="width:300px;">修炼等级</td>
                <td style="width:300px;">幻化列表</td>
                <td style="width:300px;">法阵列表</td>
            </tr>
            <tr>
                <td id="pet_level">2</td>
                <td id="pet_grade">3</td>
                <td id="pet_refineList">4</td>
                <td id="pet_magicShowList">5</td>
                <td id="pet_fanzhenList">5</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色仙器信息-->
        <table class="itable itable-color">
            <caption>角色仙器信息</caption>
            <tbody>
            <tr>
                <td style="width:150px;">培养等级</td>
                <td style="width:150px;">进阶阶数</td>
                <td style="width:300px;">修炼等级</td>
                <td style="width:300px;">幻化列表</td>
                <td style="width:300px;">法阵列表</td>
            </tr>
            <tr>
                <td id="ride_level">2</td>
                <td id="ride_grade">3</td>
                <td id="ride_refineList">4</td>
                <td id="ride_magicShowList">5</td>
                <td id="ride_fanzhenList">5</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色神兵信息-->
        <table class="itable itable-color">
            <caption>角色神兵信息</caption>
            <tbody>
            <tr>
                <td style="width:150px;">神兵等级</td>
                <td style="width:500px;">神兵幻化</td>
                <td style="width:300px;">神兵附魂</td>

            </tr>
            <tr>
                <td id="shenbing_level"></td>
                <td id="shenbing_showList"></td>
                <td id="shenbing_refineList"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色仙翼信息-->
        <table class="itable itable-color">
            <caption>角色仙翼信息</caption>
            <tbody>
            <tr>
                <td style="width:150px;">仙翼等级</td>
                <td style="width:500px;">仙翼幻化</td>
                <td style="width:300px;">仙翼附魂</td>
            </tr>
            <tr>
                <td id="wing_level"></td>
                <td id="wing_showList"></td>
                <td id="wing_refineList"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色仙法信息-->
        <table class="itable itable-color" style="width: 500px">
            <caption>角色仙法信息</caption>
            <tbody id="role_skill_body">
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色法宝信息-->
        <table class="itable itable-color" style="width: 500px;">
            <caption>角色法宝信息</caption>
            <tbody id="role_amulet_body">
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色金身信息-->
        <table class="itable itable-color" style="width: 500px">
            <caption>角色金身信息</caption>
            <tbody id="role_soul_body">
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色符文信息-->
        <table class="itable itable-color" style="width: 500px">
            <caption>角色符文信息</caption>
            <tbody id="role_rune_body">
            </tbody>
        </table>
    </div>

    <!-- 仙府资源 -->
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--仙府灵兽信息-->
        <table class="itable itable-color" style="width: 500px">
            <caption>角色仙府灵兽信息</caption>
            <tbody id="role_xianfu_animal_body">
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色仙府灵兽图鉴信息-->
        <table class="itable itable-color" style="width: 500px;">
            <caption>角色仙府灵兽图鉴信息</caption>
            <tbody id="role_xianfu_illBook_body">
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色风水信息-->
        <table class="itable itable-color" style="width: 500px;">
            <caption>角色风水信息</caption>
            <tbody id="role_xianfu_fengShui_body">
            </tbody>
        </table>
    </div>


    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色副本信息-->
        <table class="itable itable-color" style="width: 500px">
            <caption>角色副本信息</caption>
            <tbody id="role_zones_body">
            </tbody>
        </table>
    </div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <!--角色背包信息-->
        <table class="itable itable-color" style="width: 500px">
            <caption>角色道具背包信息</caption>
            <tbody id="role_bag_body">
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript">

    $('#search_type').change(function () {

        var type = $(this).val();

        switch (type) {
            case '1':
                $('#search_type_1').show();
                $('#search_type_2').hide();
                $('#search_type_3').hide();
                break;
            case '2':
                $('#search_type_1').hide();
                $('#search_type_2').show();
                $('#search_type_3').hide();
                break;
            case '3':
                $('#search_type_1').hide();
                $('#search_type_2').hide();
                $('#search_type_3').show();
                break;
            default:
                $('#search_type_1').hide();
                $('#search_type_2').hide();
                $('#search_type_3').hide();
                break;
        }
    });
    var roleData = {};

    $(function () {
        var error = $('#error');
        $('#manual-gmcmd-form').submit(function (event) {
            form_check();
            if (error.val() == '1') {
                return false;
            }
            else {
                $('#select_info').hide();
                $.ajax({
                    url: 'admin.php?ctrl=userinfo&act=index_data',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function (data) {
                    if (data.errno === 2) {
                        $.dialog.tips(data.error);
                        return false;
                    }
                    roleData = data;
                    showMsg();
                })
            }
            return false;
        });
    });

    function form_check() {
        var error = $('#error');
        //服务器
        var server = $('#server');
        if (server.val() == 0) {
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

        var search_type = $('#search_type').val();
        //角色ID
        var role_id = $('#role_id').val();
        var account = $('#account').val();
        var role_name = $('#role_name').val();
        if (search_type == 0) {
            $.dialog.tips('未选择搜索类型');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

        if (search_type == 1 && account == '') {
            $.dialog.tips('未填写玩家账号');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

        if (search_type == 2 && role_id == '') {
            $.dialog.tips('未填写角色id');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

        if (search_type == 3 && role_name == '') {
            $.dialog.tips('未填写角色名');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }
    }

    function showMsg() {

        if (roleData.length === 0 || roleData.errno === 2) {
            return false;
        }

        data = roleData;
        //角色基本信息
        $('#game_account').text(data.role_info.account);
        $('#game_name').text(data.role_info.name);
        $('#id').text(data.role_info.id);
        $('#level').text(data.role_info.level);
        $('#eraLvleraNum').text(data.role_info.eraLvleraNum);
        $('#power').text(data.role_info.power);
        $('#copper').text(data.role_info.copper);
        $('#gold').text(data.role_info.gold);
        $('#bind_gold').text(data.role_info.bind_gold);
        $('#xianwei').text(data.role_info.xianwei_name);
        $('#VipLvl').text(data.role_info.vip_level);
        $('#money').text(data.role_info.money);
        $('#xianfuLvl').text(data.role_info.xianfu_level);

        //装备
        var equip = ['weapon', 'hats', 'clothes', 'hand', 'shoes', 'belt', 'necklace', 'bangle', 'ring', 'jude'];
        $.each(equip, function () {
            var equip_str = this;

            $('#' + equip_str + '_name').text(data.role_equip.EquipGrids[equip_str].itemName_str);
            $('#' + equip_str + '_strong').text(data.role_equip.EquipGrids[equip_str].strongLvl_str);
            $('#' + equip_str + '_gems').text(data.role_equip.EquipGrids[equip_str].gems_str);
        });

        //仙器灵宠
        var animal = ['pet', 'ride'];

        $.each(animal, function () {
            var animal_str = this;
            var animal_data = data['role_' + animal_str];

            $('#' + animal_str + '_level').text(animal_data.level);
            $('#' + animal_str + '_grade').text(animal_data.rankStar);
            $('#' + animal_str + '_refineList').html(animal_data.refineList_str);
            $('#' + animal_str + '_magicShowList').html(animal_data.magicShow_str);
            $('#' + animal_str + '_fanzhenList').html(animal_data.fazhenList_str);
        });

        //仙法
        var skill_html_str = "        <tr>\n" +
            "            <td style=\"width:150px;\">绝学-技能名</td>\n" +
            "            <td>等级</td>\n" +
            "        </tr>";

        var role_skill_body = $('#role_skill_body');

        var skill = data.role_skill.Skills;

        if (skill.length != 0) {
            $.each(skill, function () {
                skill_html_str += "        <tr>\n" +
                    "            <td>" + this.name + "</td>\n" +
                    "            <td>" + this.level + "</td>\n" +
                    "        </tr>";
            })
        } else {
            skill_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未学习仙法</td>\n" +
                "        </tr>";
        }

        role_skill_body.html(skill_html_str);

        //法宝
        var amulet = data.role_amulet.typeList;

        var role_amulet_body = $('#role_amulet_body');

        var amulet_html_str = '';

        if (amulet && amulet.length != 0) {
            $.each(amulet, function () {

                var _This = this;

                amulet_html_str += '            <table class="itable itable-color" style="width: 500px">\n' +
                    '                <tbody>\n' +
                    '                <tr style="background: yellow">\n' +
                    '                    <td style="color: red">《' + _This.typeName + '》</td><td></td>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td style="width:150px;">法宝名</td>\n' +
                    '                    <td>等级</td>\n' +
                    '                </tr>';

                $.each(_This.refineList, function () {

                    var _That = this;

                    amulet_html_str += '<tr>\n' +
                        '                    <td >' + _That.name + '</td>\n' +
                        '                    <td>' + _That.level + '</td>\n' +
                        '                </tr>';


                });

                amulet_html_str += '</tr>\n' +
                    '                </tbody>\n' +
                    '            </table>';

            });
        } else {
            amulet_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未拥有法宝</td>\n" +
                "        </tr>";
        }

        role_amulet_body.html(amulet_html_str);

        //金身
        var soul_html_str = "        <tr>\n" +
            "            <td style=\"width:150px;\">金身名</td>\n" +
            "            <td>等级</td>\n" +
            "        </tr>";

        var role_soul_body = $('#role_soul_body');

        var soul = data.role_soul.refineList;

        if (soul && soul.length != 0) {
            $.each(soul, function () {
                soul_html_str += "        <tr>\n" +
                    "            <td>" + this.typeName + "</td>\n" +
                    "            <td>" + this.rankLayer + "</td>\n" +
                    "        </tr>";
            })
        } else {
            soul_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未学习金身</td>\n" +
                "        </tr>";
        }

        role_soul_body.html(soul_html_str);

        //符文
        var rune_html_str = "        <tr>\n" +
            "            <td style=\"width:150px;\">符文槽</td>\n" +
            "            <td>符文信息</td>\n" +
            "        </tr>";

        var role_rune_body = $('#role_rune_body');

        var rune = data.role_rune.slot;

        if (rune && rune.length != 0) {
            $.each(rune, function () {
                rune_html_str += "        <tr>\n" +
                    "            <td>" + this.id + "</td>\n" +
                    "            <td>" + this.itemName + "</td>\n" +
                    "        </tr>";
            })
        } else {
            rune_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未镶嵌符文</td>\n" +
                "        </tr>";
        }

        role_rune_body.html(rune_html_str);

        //仙府灵兽
        var animal_html_str = "        <tr>\n" +
            "            <td style=\"width:150px;\">灵兽名</td>\n" +
            "            <td>灵兽等级</td>\n" +
            "        </tr>";

        var role_animal_body = $('#role_xianfu_animal_body');

        var animal = data.role_xianfu_animalInfo;

        if (animal && animal.length != 0) {
            $.each(animal, function () {
                animal_html_str += "        <tr>\n" +
                    "            <td>" + this.name + "</td>\n" +
                    "            <td>" + this.level + "</td>\n" +
                    "        </tr>";
            })
        } else {
            animal_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未激活灵兽</td>\n" +
                "        </tr>";
        }

        role_animal_body.html(animal_html_str);

        //灵兽图鉴
        var illBook = data.role_xianfu_illBookInfo;

        var role_illBook_body = $('#role_xianfu_illBook_body');

        var illBook_html_str = '';

        if (illBook && illBook.length != 0) {
            $.each(illBook, function () {

                var _This = this;

                illBook_html_str += '            <table class="itable itable-color" style="width: 500px">\n' +
                    '                <tbody>\n' +
                    '                <tr style="background: yellow">\n' +
                    '                    <td style="color: red">《' + _This.qualityName + '》</td><td></td>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td style="width:150px;">图鉴名</td>\n' +
                    '                    <td>等级</td>\n' +
                    '                </tr>';

                $.each(_This.list, function () {

                    var _That = this;

                    illBook_html_str += '<tr>\n' +
                        '                    <td >' + _That.name + '</td>\n' +
                        '                    <td>' + _That.level + '</td>\n' +
                        '                </tr>';


                });

                illBook_html_str += '</tr>\n' +
                    '                </tbody>\n' +
                    '            </table>';

            });
        } else {
            illBook_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未拥有灵兽图鉴</td>\n" +
                "        </tr>";
        }

        role_illBook_body.html(illBook_html_str);

        //仙府灵兽风水
        var fengShui = data.role_xianfu_fengShuiInfo.decorate;

        var role_fengShui_body = $('#role_xianfu_fengShui_body');

        var fengShui_html_str = '';

        if (fengShui && fengShui.length != 0) {
            $.each(fengShui, function () {

                var _This = this;

                fengShui_html_str += '            <table class="itable itable-color" style="width: 500px">\n' +
                    '                <tbody>\n' +
                    '                <tr style="background: yellow">\n' +
                    '                    <td style="color: red">《' + _This.typeName + '》</td><td></td>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td style="width:150px;">物件名</td>\n' +
                    '                    <td>等级</td>\n' +
                    '                </tr>';

                $.each(_This.list, function () {

                    var _That = this;

                    fengShui_html_str += '<tr>\n' +
                        '                    <td >' + _That.name + '</td>\n' +
                        '                    <td>' + _That.level + '</td>\n' +
                        '                </tr>';


                });

                fengShui_html_str += '</tr>\n' +
                    '                </tbody>\n' +
                    '            </table>';

            });
        } else {
            fengShui_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未拥有风水物件</td>\n" +
                "        </tr>";
        }

        role_fengShui_body.html(fengShui_html_str);


        //神兵仙翼
        var shenbing = ['shenbing', 'wing'];

        $.each(shenbing, function () {
            var shenbing_str = this;
            var shenbing_data = data['role_' + shenbing_str];

            if (shenbing_data) {
                $('#' + shenbing_str + '_level').text(shenbing_data.level);
                $('#' + shenbing_str + '_refineList').html(shenbing_data.refineList_str);
                $('#' + shenbing_str + '_showList').html(shenbing_data.showList_str);
            }
        });

        //副本挑战
        var zones_html_str = "        <tr>\n" +
            "            <td style=\"width:150px;\">副本名</td>\n" +
            "            <td>最高挑战记录</td>\n" +
            "        </tr>";

        var role_zones_body = $('#role_zones_body');

        var zones = data.role_zones;

        if (zones && zones.length != 0) {
            $.each(zones, function () {
                zones_html_str += "        <tr>\n" +
                    "            <td>" + this.name + "</td>\n" +
                    "            <td>" + this.max_layers + "</td>\n" +
                    "        </tr>";
            })
        } else {
            zones_html_str += "        <tr>\n" +
                "            <td colspan='2'>暂未挑战副本</td>\n" +
                "        </tr>";
        }

        role_zones_body.html(zones_html_str);

        //道具背包信息
        var bag_html_str = "        <tr>\n" +
            "            <td style=\"width:150px;\">道具名</td>\n" +
            "            <td>数量</td>\n" +
            "        </tr>";

        var role_bag_body = $('#role_bag_body');

        var bag = data.role_item_bagInfo.items;

        if (bag && bag.length != 0) {
            $.each(bag, function () {
                bag_html_str += "        <tr>\n" +
                    "            <td>" + this.itemName + "</td>\n" +
                    "            <td>" + this.count + "</td>\n" +
                    "        </tr>";
            })
        } else {
            bag_html_str += "        <tr>\n" +
                "            <td colspan='2'>道具背包暂时没有物品</td>\n" +
                "        </tr>";
        }

        role_bag_body.html(bag_html_str);

        $('#select_info').show();
    }

    function loginAccount() {

        if (roleData.length === 0 || roleData.errno === 2) {
            return false;
        }

        var package = roleData.role_info.package;
        var account = roleData.role_info.account;

        if (!package || !account) {
            $.dialog.tips('角色信息有误');
        }

        $.ajax({
            url: 'admin.php?ctrl=userinfo&act=login_account',
            type: 'POST',
            dataType: 'JSON',
            data: {'package' : package, 'account' : account}
        }).done(function (data) {
            if (data.errCode !== 200) {
                $.dialog.tips(data.msg);
                return false;
            }

            data.urlQuery.selected_server = JSON.stringify(data.urlQuery.selected_server);

            var arr = [];
            for (var x in data.urlQuery) {
                arr.push(x + "=" + data.urlQuery[x]);
            }

            var url = data.data.cdn + "loading.html?" + arr.join("&");

            console.log(url);

            window.open(url);

        })
    }

</script>