<?php /* Smarty version 2.6.27, created on 2018-10-18 16:40:13
         compiled from server/data.tpl */ ?>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.p-input {
        vertical-align:middle;
    }

.p-text {
    vertical-align:middle;
}

.p-span {
    padding: 5px 10px;
    margin: 0px 3px;
}

.w-150 {
    width: 150px;
}

fieldset {
    padding: 5px;
    border: 2px solid #cc0;
    margin: 10px 0px;
}

.p-checked {
    background: #ff0;
    border-radius: 3px;
}
</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" >
    <table class="itable itable-color">
      <tbody>
      <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server_radios.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
      <td style="width: 150px; margin-top: 3px; margin-bottom: 3px;">内容</td>
        <td><textarea rows="10" cols="100" id="sql"></textarea></td>
      <tr>
        <td colspan="2" style="margin-top: 3px; margin-bottom: 3px;"><input id="search" type="button" class="gbutton" value="搜索" style="margin-top: 3px; margin-bottom: 3px;">
         <input type="hidden" id="checkboxServer" name="checkboxServer" value="" />  
        </td>
      </tr>
        </tbody>
      
    </table>
  </form>
</div>


<div id="data" class="ui-tabs ui-widget ui-widget-content ui-corner-all " style=" margin-top:10px;">

</div>


<script>
    $(function(){
        $('#search').click(function(){
            var sql = $("#sql").val();
            var server = $("input[name='server']:checked").val();
            if(!server){
                alert('请选择服务器');return false;   
            }else if(sql == ''){
                alert('请输入sql语句');return false; 
            }
            
            $.post('admin.php?ctrl=sql&act=data',{'sql':sql , 'server' : server},function(data){
                $("#data").html(data.msg);  
            },'json')
        });
    });
</script> 