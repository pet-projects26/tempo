<?php /* Smarty version 2.6.27, created on 2019-04-29 20:50:51
         compiled from common/scp.tpl */ ?>
<style type="text/css">
    .w-box-list {
        margin: 5px 0px;
    }
    .w-box-list h5 {
        border: 1px dashed #cc0;
        padding: 5px;
        margin-bottom: 5px;
        text-align: center;
    }
    .lgroup {
        display: none
    }
</style>
<div id="checkboxScp" class="modal-demo">

    <div class="toolTips" style="margin:10px 20px;">
        <div class="toolTips-search">
            名 称 : <input type="text" id="searchData" placeholder=" 关 键 字" >
        </div>
    </div>
    <div id="modal-tabs" class="text">
        <ul>
            <?php if ($this->_tpl_vars['showGroup'] == 1): ?>
            <li><a class="li-spc" id="channal-group" num = 1 href="#tabs-1">渠道组</a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['showChannel'] == 1): ?>
            <li><a class="li-scp" id="channel" href="#tabs-2">渠道</a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['showServer'] == 1): ?>
            <li><a class="li-scp" id="server" href="#tabs-3">服务器</a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['showPackage'] == 1): ?>
            <li><a class="li-scp" id="package" href="#tabs-4">渠道_包名</a></li>
            <?php endif; ?>
        </ul>
        <div id="tabs-1" class="tabs-content" <?php if (! $this->_tpl_vars['showGroup']): ?>style="display:none;"<?php endif; ?>>
        <div id="group">
            <ul>
                <?php $_from = $this->_tpl_vars['list']['group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <li> <span class="name"><?php echo $this->_tpl_vars['item']['name']; ?>
</span> <span class="lid"><?php echo $this->_tpl_vars['item']['id']; ?>
</span> </li>
                <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>
    </div>
    <div id="tabs-2" class="tabs-content" <?php if (! $this->_tpl_vars['showChannel']): ?>style="display:none;"<?php endif; ?>>
    <div id="channel"> <?php $_from = $this->_tpl_vars['list']['channel']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['row']):
?>
        <div class="w-box-list group-<?php echo $this->_tpl_vars['group_id']; ?>
">
            <h5><?php echo $this->_tpl_vars['list']['group'][$this->_tpl_vars['group_id']]['name']; ?>
</h5>
            <ul>
                <?php $_from = $this->_tpl_vars['row']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <li> <span class="name"><?php echo $this->_tpl_vars['item']['name']; ?>
</span> <span class="lid"><?php echo $this->_tpl_vars['item']['channel_id']; ?>
</span> <span class="lgroup"><?php echo $this->_tpl_vars['item']['group_id']; ?>
</span> </li>
                <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>
        <?php endforeach; endif; unset($_from); ?> </div>
</div>
<div id="tabs-3" class="tabs-content" <?php if (! $this->_tpl_vars['showServer']): ?>style="display:none;"<?php endif; ?>>
<div id="server"> <?php $_from = $this->_tpl_vars['list']['server']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['row']):
?>
    <div class="w-box-list group-<?php echo $this->_tpl_vars['group_id']; ?>
">
        <h5><?php echo $this->_tpl_vars['list']['group'][$this->_tpl_vars['group_id']]['name']; ?>
</h5>
        <ul>
            <?php $_from = $this->_tpl_vars['row']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
            <li> <span class="name">S<?php echo $this->_tpl_vars['item']['num']; ?>
-<?php echo $this->_tpl_vars['item']['name']; ?>
</span> <span class="lid"><?php echo $this->_tpl_vars['item']['server_id']; ?>
</span> <span class="lgroup"><?php echo $this->_tpl_vars['item']['group_id']; ?>
</span> </li>
            <?php endforeach; endif; unset($_from); ?>
        </ul>
    </div>
    <?php endforeach; endif; unset($_from); ?> </div>
</div>
<div id="tabs-4" class="tabs-content" <?php if (! $this->_tpl_vars['showPackage']): ?>style="display:none;"<?php endif; ?>>
<div id="package"> <?php $_from = $this->_tpl_vars['list']['package']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['row']):
?>
    <div class="w-box-list group-<?php echo $this->_tpl_vars['group_id']; ?>
">
        <h5><?php echo $this->_tpl_vars['list']['group'][$this->_tpl_vars['group_id']]['name']; ?>
</h5>
        <ul>
            <?php $_from = $this->_tpl_vars['row']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
            <li> <span class="name"><?php echo $this->_tpl_vars['item']['channel_name']; ?>
_<?php echo $this->_tpl_vars['item']['name']; ?>
</span> <span class="lid"><?php echo $this->_tpl_vars['item']['package_id']; ?>
</span> <span class="lgroup"><?php echo $this->_tpl_vars['item']['group_id']; ?>
</span> </li>
            <?php endforeach; endif; unset($_from); ?>
        </ul>
    </div>
    <?php endforeach; endif; unset($_from); ?> </div>
</div>
</div>
<div class="modal-footer">
    <p style="text-align:right;padding:0px 30px 10px 0px;"> <?php if ($this->_tpl_vars['select'] != 'radio'): ?>
        <button class="allSelect gbutton">全选</button>
        <button class="unSelect gbutton">反选</button>
        <?php endif; ?>
        <button class="gbutton confirm">确定</button>
    </p>
</div>
<script type="text/javascript">
    $(function(){
        leng = 0;
        $('#filter').on('click' , function(event){
            Custombox.open({
                target: '#checkboxScp',
                effect: 'slide',
                width: 800,
                overlayOpacity: 0.4,
                overlaySpeed:5,
                speed: 1
            });
            event.preventDefault();
        });
        // tabs
        $("#modal-tabs").tabs({ selected:0 });


        var _select = "<?php echo $this->_tpl_vars['select']; ?>
";
        // tab select
        $('#modal-tabs .tabs-content  ul li').click(function(event){
            if (!$(this).hasClass('select')){

                /*if (_select == 'radio') {
                    $('#modal-tabs .tabs-content  ul li').removeClass('select');
                }*/
                $(this).addClass('select');
            } else {
                $(this).removeClass('select');
            }
        });
        //把选择的值转化成json
        $('.confirm').click(function(event){
            var tabData = new Array();
            $('.tabs-content').each(function(index , el){
                tabData[index] = new Array();
                $('li', this).each(function(i , v){
                    if($(this).hasClass('select')){
                        tabData[index].push( $('.lid' , this).html());
                    }
                });
            });
            var _selectserver = "<?php echo $this->_tpl_vars['selectserver']; ?>
";
            if(leng < 1 && _selectserver == 1){
                alert('请选择服务器');return false;
            }
            $('#checkboxScp').data(tabData);

            Custombox.close();
        });
        //渠道组
        $('#group li').click(function(index) {
            var num = $(this).attr('num');

            var id = $(this).attr('id');

            if (id == 'channel-group') {
                $(this).attr('num', 1);
            }

            if (num == 1) {
                $('.w-box-list'). hide();
                $('.w-box-list li').removeClass('select');
            }

            var data = new Array();

            //获取

            $('#group li').each(function(index) {

                if($(this).hasClass('select')){
                    var lid  = ( $('.lid' , this).html());
                    var  show = ".group-" + lid;
                    data.push(lid);
                }
            });

            var len = data.length;

            if (len >= 1) {
                $('.w-box-list'). hide();
                $('.w-box-list li').removeClass('select');

                $.each(data, function(index, groupid) {
                    var show = ".group-" + groupid;
                    $(show).show();
                });
            }else{
                $('.w-box-list'). show();
            }
        });

        //渠道
        $('#group li').click(function(index) {
            var channel = new Array();

            $('#channel li').each(function(index) {
                if($(this).hasClass('select')){
                    var lid  = ( $('.lgroup' , this).html());
                    var  show = ".group-" + lid;
                    channel.push(lid);
                }
            });

            var len = channel.length;

            if (len >= 1) {
                $('#server .w-box-list'). hide();
                $('#server .w-box-list li').removeClass('select');

                $('#package .w-box-list'). hide();
                $('#package .w-box-list li').removeClass('select');

                $.each(channel, function(index, groupid) {
                    var show = ".group-" + groupid;
                    $('#server '+show).show();
                    $('#package '+show).show();
                });
            }
        });

        //服务器
        $('#server li').click(function(index) {
            var server = new Array();
            if (_select == 'radio') {
                $('#modal-tabs .tabs-content #server ul li').removeClass('select');
                $(this).addClass('select');
            }
            //$(this).addClass('select');

            $('#server li').each(function(index) {
                if($(this).hasClass('select')){
                    var lid  = ( $('.lgroup' , this).html());
                    var  show = ".group-" + lid;
                    server.push(lid);
                }
            });

            leng = server.length;

            if (leng>= 1) {
                $('#package .w-box-list'). hide();
                $('#package .w-box-list li').removeClass('select');

                $.each(server, function(index, groupid) {
                    var show = ".group-" + groupid;
                    $('#package '+show).show();
                });
            }else{
                $('#package .w-box-list'). show();
            }
        });

        //全选
        $('.allSelect').click(function(event){
            $("#modal-tabs .tabs-content").find('li').each(function(index , el){
                if(!$(this).is(':hidden')){
                    $(this).addClass('select');
                }
            });
        });
        //反选
        $('.unSelect').click(function(event){
            $("#modal-tabs .tabs-content").find('li').each(function(index , el){
                if(!$(this).is(':hidden')){
                    if($(this).hasClass('select')){
                        $(this).removeClass('select');
                    }
                    else{
                        $(this).addClass('select');
                    }
                }
            });
        });
    });
    function selectAll(id){
        $(id + ' .tabs-content ul li').click(function(event){
            if(!$(this).hasClass('select')){
                $(this).addClass('select');
            }
            else{
                $(this).removeClass('select');
            }
        });
    }

    $('#searchData').keyup(function(event) {
        if( event.keyCode == 13 || event.keyCode == 8){
            var text  = $(this).val();
            //var index = $("#modal-tabs").tabs('option','selected');
            var con = $("#modal-tabs .ui-state-active").find('a').html();
            var index;
            if(con == '渠道组'){
                index = 0;
            }else if(con == '渠道'){
                index = 1;
            }else if(con == '服务器'){
                index = 2;
            }else if(con == '渠道_包号'){
                index = 3;
            }
            $("#modal-tabs .tabs-content").eq(index).find('.name').each(function(index, el) {
                if( $(this).html().indexOf(text) < 0  ){
                    $(this).parents('li').hide();
                }else{
                    $(this).parents('li').show();
                }
            });
        }
    });
</script> 