<?php /* Smarty version 2.6.27, created on 2018-10-19 14:27:26
         compiled from user/items.tpl */ ?>
<table id="user_list" class="itable">
  <thead>
    <tr>
	  <th>用户名</th>
	  <th>姓名</th>
	  <th>部门</th>
	  <th>用户组</th>
	  <th>创建时间</th>
	  <th>最后登录时间</th>
	  <th>最后登录ip</th>
	  <th>过期时间</th>
	  <th>email</th>
	  <th>电话</th>
	  <th>账号状态</th>
	  <th>操作</th>
    </tr>
  </thead>
  <tbody>
    
      <?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
      <tr>
      <td><?php echo $this->_tpl_vars['item']['username']; ?>
</td>
      <td><?php echo $this->_tpl_vars['item']['name']; ?>
</td>
      <td class="edit" id="<?php echo $this->_tpl_vars['item']['uid']; ?>
" field="department"><?php echo $this->_tpl_vars['item']['department']; ?>
</td>
      <td class="edit_group" id="<?php echo $this->_tpl_vars['item']['uid']; ?>
" field="groupid"><?php echo $this->_tpl_vars['item']['groupname']; ?>
</td>
      <td><?php echo $this->_tpl_vars['item']['created_format']; ?>
</td>
      <td><?php echo $this->_tpl_vars['item']['last_login_time_format']; ?>
</td>
      <td><?php echo $this->_tpl_vars['item']['last_login_ip']; ?>
</td>
      <td><a uid="<?php echo $this->_tpl_vars['item']['uid']; ?>
" class="delay_account" href="javascript:void(0)" title="点击延长账号使用期限"><?php echo $this->_tpl_vars['item']['expiration_format']; ?>
</td>
      <td class="edit" id="<?php echo $this->_tpl_vars['item']['uid']; ?>
" field="email"><?php echo $this->_tpl_vars['item']['email']; ?>
</td>
      <td class="edit" id="<?php echo $this->_tpl_vars['item']['uid']; ?>
" field="tel"><?php echo $this->_tpl_vars['item']['tel']; ?>
</td>
      <td><?php echo $this->_tpl_vars['item']['status_msg']; ?>
</td>
      <td>
      <?php if ($this->_tpl_vars['item']['status'] <> @USER_STATUS_NORMAL): ?>
      <a href="javascript:void(0)" onclick="forbid_user(<?php echo $this->_tpl_vars['item']['uid']; ?>
, 1)" title="封禁帐号"><font color="red">解</font></a>
      <?php else: ?>
      <a href="javascript:void(0)" onclick="forbid_user(<?php echo $this->_tpl_vars['item']['uid']; ?>
, 2)" title="解封帐号">封</a>
      <?php endif; ?>
      | 
      <a href="javascript:void(0)" onclick="del_user(<?php echo $this->_tpl_vars['item']['uid']; ?>
)" title="删除帐号">删</a> |
      <a href="javascript:void(0)" onclick="reset_password(<?php echo $this->_tpl_vars['item']['uid']; ?>
)" title="重置密码">密</a> |
      <a href="javascript:void(0)" onclick="clear_login_err('<?php echo $this->_tpl_vars['item']['username']; ?>
')" title="清空错误登录次数">清</a>
      </td>
      </tr>
      <?php endforeach; endif; unset($_from); ?>
    
  </tbody>
</table>
<script type="text/javascript">
$("#user_list tr").hover(
    function () {
       $(this).addClass("hover");
    },
    function () {
       $(this).removeClass("hover");
    }
);
var tTable = $("#user_list").dataTable({
  "oLanguage": {
    "sUrl": "style/js/dt_cn.t"
  },
  "aaSorting": [
	[ 3, "asc" ]
  ],
  "iDisplayLength":100,
  "sPaginationType": "full_numbers",
  "sDom": '<"top"lfp>rt<"bottom"ip><"clear">',
  "bFilter": true
});

$('.edit').editable("admin.php?ctrl=user&act=save_field", { 
    indicator : "<img src='style/images/indicator.gif'>",
    //loadurl   : "admin.php?ctrl=menu&act=get_field",
    height    : "15px",
    submit    : "修改",
    cancel    : "取消",
    tooltip   : "点击修改...",
    style  : "inherit"
});

$('.edit_group').editable("admin.php?ctrl=user&act=save_field", { 
    indicator : "<img src='style/images/indicator.gif'>",
    type   : 'select',
    data : <?php echo $this->_tpl_vars['groupNames']; ?>
,
    //loadurl   : "admin.php?ctrl=menu&act=get_field",
    height    : "15px",
    submit    : "修改",
    cancel    : "取消",
    tooltip   : "点击修改...",
    style  : "inherit"
});

function forbid_user(uid, status){
    if(confirm('确定操作该账号?')){
        $.ajax({
          type: "POST",
          url: "admin.php?ctrl=user&act=save_field",
          data: "field=status&value="+status+"&id="+uid,
          timeout: 20000,
          error: function(){alert('error');},
          success: function(result){
               $tabs.tabs('load', 0);
          }
        });
    }
}

$('.delay_account').click(function(){
	var uid = $(this).attr('uid');
	var expiration_format = $(this).html();
	var this_element = $(this);
	$.ajax({
	       type: "POST",
	       url: "admin.php?ctrl=user&act=delay_account",
	       data: "uid="+uid+"&expiration_format="+expiration_format,
	       timeout: 20000,
	       error: function(){alert('error');},
	       success: function(result){
	    	   this_element.html(result);
	       }
	    });
});

function del_user(uid){
	if(confirm('确定删除该账号?')){
		$.ajax({
	      type: "POST",
	      url: "admin.php?ctrl=user&act=delete",
	      data: "uid="+uid,
	      timeout: 20000,
	      error: function(){alert('error');},
	      success: function(result){
	    	   $tabs.tabs('load', 0);
	      }
	    });
	}
}
function reset_password(uid){
    if(confirm('确定重置密码?')){
        $.ajax({
          type: "POST",
          url: "admin.php?ctrl=user&act=reset_password",
          data: "uid="+uid,
          timeout: 20000,
          error: function(){alert('error');},
          success: function(result){
        	  $.dialog.alert(result);
          }
        });
    }
}
function clear_login_err(username){
    if(confirm('确定清空错误登录次数?')){
        $.ajax({
          type: "POST",
          url: "admin.php?ctrl=user&act=clear_login_err",
          data: "username="+username,
          timeout: 20000,
          error: function(){alert('error');},
          success: function(result){
        	  $.dialog.alert(result);
          }
        });
    }
}

</script>