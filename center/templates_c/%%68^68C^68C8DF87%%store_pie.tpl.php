<?php /* Smarty version 2.6.27, created on 2018-10-19 16:06:01
         compiled from item/store_pie.tpl */ ?>
<div id="agent_tabs" class="itabs ui-tabs ui-widget ui-widget-content ui-corner-all">
  <div style="margin:5px;border:solid 1px #ABABAB;border-radius:5px;padding:5px 15px;">
    <div id="header" style="margin:10px 0 20px;"> </div>
    <div id="search">
      <label> 日期：
        <input type="text" name="start_time" class="datepicker" style=" width:120px;" id="start_time">
        ~
        <input type="text" name="end_time" class="datepicker" style=" width:120px;" id="end_time">
      </label>
      &nbsp;
      <label> 类型：
        <select name="type" id="type">
          <option value="4">商城购买</option>
      
        </select>
      </label>
      &nbsp;
      <input type="button" id="filter" class="gbutton" value="筛选">
      <input type="button" class="gbutton" onclick="_online();" value="搜索">
    </div>
  </div>
  <div><?php echo $this->_tpl_vars['scp']; ?>
</div>
  <div id="total" style="margin:0 5px;"></div>
</div>
<script language="javascript">
    function _online(){
		var type= $('#type').val();
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		
		if(start_time !='' && end_time != ''){
			var start = Date.parse(new Date(start_time))/1000;
			var end = Date.parse(new Date(end_time))/1000;
			if(start > end){
				 $.dialog.tips('结束时间不能小于开始时间');	
				 return false;
			}
		}
		
        var scp = JSON.stringify($('#checkboxScp').data());
		
		//调用一个方法，显示load;
		loading(1);
		
        $.post('admin.php?ctrl=item&act=store_pie_data' , {start_time:start_time,end_time:end_time,scp:scp,type:type} , function(html){
			
			//再次调用，取消load;
			loading(0);
            $('#total').html(html);
        });
    }
    $('.datepicker').datepicker();
  	
	function loading(type){
		if(type ==1){//显示加载
			$('#total').html('正在加载...');
		}else{//取消加载
			$('#total').html('加载完成');	
		}
	}
  
	
</script>