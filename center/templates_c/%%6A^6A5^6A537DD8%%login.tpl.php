<?php /* Smarty version 2.6.27, created on 2018-10-18 16:19:01
         compiled from login.tpl */ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>后台登录</title>
<style type="text/css">
body{font-size:12px;color:#555555;}
.wrap{background:url("style/images/login/login_bg2.jpg") no-repeat scroll center 0 transparent;height: 640px;margin-left: auto;margin-right: auto;width: 1136px;}
.head{padding:40px 0 0 0px;}
.logo2{background:url("style/images/login/login2.png") no-repeat scroll left top transparent;width: 200px; height:80px;margin-left: auto;margin-right: auto;margin-bottom:10px;}
.logo{background:url("style/images/login_logo.png") no-repeat scroll left top transparent;height: 64px;overflow: hidden;width: 210px; margin-left: auto;margin-right: auto;}
.logo span{text-align:center;margin:10 5px;font-size:25px;font-weight:bolder;}
.login th {
  color:black;
  font-size: 16px;
  font-weight:bolder;
  height: 35px;
  line-height: 31px;
  list-style: none outside none;
  padding-right: 5px;
  text-align: right;
}
.li{ padding:0 6px;vertical-align:middle; border-radius:4px;}
input {border: 1px solid #ABADB3;border-radius: 4px;color: #444;font-size: 14px; height:28px;line-height:28px;vertical-align: middle;}
input[type="checkbox"],input[type="radio"]{border:none;}
input:focus {border: 1px solid #B7D4EA;box-shadow: 0 0 8px #B7D4EA; background:#EDF5F5;}
td img {
    vertical-align: middle;
}
#login_button{background:#E8EFFC; border:#6C6C6C solid 1px; color:#000;height:32px;line-height:28px; cursor:pointer;}
#login_button:hover{background:#D6E3FA;box-shadow: 0 0 4px #B7D4EA;}

</style>
<script type="text/javascript" src="style/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript">
function verifyStatus(){
    var name = $('#username').val();
    $.getJSON('?act=user_status&username='+name, function(ret){
        if(ret.verify){
            addVerify();
        }else{
            removeVerify();
        }
    });
}
function addVerify(){
    $('#verify_display').show();
    $('#verify').removeAttr('disabled');
}
function removeVerify(){
    $('#verify_display').hide();
    $('#verify').attr('disabled', 'disabled');
}
</script>
</head>
<body>
<div class="wrap">
  <div class="head">
    <div class="logo2"></div>
    <div class="logo"></div>
  </div>
  <div class="login">
  <form action="<?php echo $this->_tpl_vars['login_url']; ?>
" name="login" method="post">
  <input type="hidden" value="<?php echo $this->_tpl_vars['login_token']; ?>
" name="login_token">
    <table cellspacing="0" cellpadding="0" style="margin:40px auto !important;padding-right:80px;">
      <tbody><tr class="li">
        <th><label for="username">管理员账号:</label></th>
        <td><input type="text" name="username" id="username" style="width:180px;"></td>
      </tr>
      <tr class="li">
        <th><label for="password">密码:</label></th>
        <td><input type="password" name="password" id="password" style="width:180px;"></td>
      </tr>
      <tr>
        <th>&nbsp;</th>
        <td><input style="width:5em;" type="submit" id="login_button" value="登&nbsp;&nbsp;录"></td>
      </tr>
      <tr>
        <th>&nbsp;</th>
        <td><font color="red"><?php echo $this->_tpl_vars['msg']; ?>
</font></td>
      </tr>
    </tbody></table>
  </form>
  </div>
</div>
<script>
    $(window).load(function(){
        $('#username').change(function(){
            verifyStatus();
        });
        $('#username').focus();
        $('#verification_code').click(function(){
            $(this).attr('src', 'admin.php?ctrl=index&act=verification_code&'+Math.random());
        });
        verifyStatus();
    });
</script>
</body>
</html>