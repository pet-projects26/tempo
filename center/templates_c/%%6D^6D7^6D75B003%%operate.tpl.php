<?php /* Smarty version 2.6.27, created on 2018-11-08 17:44:04
         compiled from action/operate.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'action/operate.tpl', 28, false),)), $this); ?>
<!--单选，显示所有渠道组-->
<!--显示查询框-->
<!--通过RPC请求服务端，得到被被关闭的功能项，table显示所有列表[开启][关闭]-->
<style>
    .all-checked {
        vertical-align: middle;
    }
    .select{
        background-color: #36ca95;
    }
    .highlight {
        background: yellow;
        color: red;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <span>查找：</span>
    <input id="txtKey" type="text" value="" placeholder="请输入功能名称" />
    <input id="btnSearch" type="button" class="gbutton" value="确定"/>
    <form class="fm" id="cgroup-edit-form">
        <div>
            <fieldset>
                <legend>功能开关列表</legend>
               <span>
                   <input class="all-checked" type="checkbox" id="check-all"/>
                   <label for="check-all" class="check-all">全选</label>
               </span>
                <strong>当前功能开关数：<?php echo count($this->_tpl_vars['action_list']); ?>
</strong>
                <div id="pretime" style="line-height: 30px">
                    <!--功能开关列表-->
                    <?php $_from = $this->_tpl_vars['action_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['foo'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['foo']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['msg']):
        $this->_foreach['foo']['iteration']++;
?>
                    <span class="action-are" style="border: 1px solid #24d5ff;padding: 3px;margin: 5px 3px">
                    <input class="all-checked" type="checkbox" name="action[]" value="<?php echo $this->_tpl_vars['k']; ?>
"/>
                    <label class="all-checked"><?php echo $this->_tpl_vars['msg']; ?>
</label>
                    </span>,
                    <?php endforeach; endif; unset($_from); ?>
                </div>
            </fieldset>
        </div>
        <div>
            <table class="itable itable-color">
                <tr>
                    <td>功能开关key</td>
                    <td>
                        <input type="text" name="unview_key" value="" placeholder="请输入功能开关KEY">
                        <span style="color: #cc1e2f">*功能开列表未能找到时，请联系服务端获取KEY</span>
                    </td>
                </tr>
                <tr></tr>
                <tr>
                    <td width="10%">功能开关</td>
                    <td>
                        <select name="status">
                            <option value="2">开启</option>
                            <option value="3">关闭</option>
                        </select>
                    </td>
                </tr>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </table>
        </div>
        <button type="button" class="gbutton set-action">提交</button>
    </form>
</div>
<script>
    $(function(){
        //点选
        $(".action-are").bind('click',function(){
            var child = $(this).children('input.all-checked');
            if($(this).hasClass('select')){
                //选中状态下
                child.attr('checked',false);
                $(this).removeClass('select');
            }else{
                $(this).addClass('select');
                child.attr('checked',true);
            }
        });

        //全选
        $("input#check-all").change('click',function(){
            if($(this).prop('checked')){
                $("input.all-checked").attr('checked',true);
                $("span.action-are").addClass('select');
            }else{
                $("input.all-checked").attr('checked',false);
                $("span.action-are").removeClass('select');
            }
        });

        var pretime = $('#pretime').html();
        var flag = 0;
        $('#btnSearch').click(function () {
            $('#pretime').html(pretime);
            var searchText = $('#txtKey').val();
            if (searchText.length == 0) {
                alert('请输入搜索关键词!');
                $('#txtKey').focus();
                return false;
            }
            var regExp = new RegExp(searchText, 'g');
            $('#pretime span.action-are').each(function () {
                var html = $(this).html();
                var _time = $(this).parent().find("strong").text();
                var newHtml = html.replace(regExp, '<span class="highlight">' + searchText + '</span>');
                $(this).html(newHtml);
                flag = 1;

            });
            if (flag) {
                if ($(".highlight").size() > 1) {
                    var _top = $(".highlight").eq(0).offset().top;
                    $("html,body").animate({
                        "scrollTop": _top
                    }, 500)
                }
            }
        });
        $(".set-action").click(function(event){
            var action = [];
            var unviewkey = $("input[name='unview_key']").val();
            $("input.all-checked").each(function(){
                if($(this).prop('checked')){
                    action.push($(this).val());
                }
            });
            if(action.length == 0 && !unviewkey){
                alert('请选择功能！');
                return false;
            }
            $.ajax({
                type:'post',
                url:'admin.php?&ctrl=action&act=handel',
                dataType: 'JSON',
                data:$("#cgroup-edit-form").serialize(),
                beforeSend:function(){
                    alert('确认修改')
                },
                success:function(data){
                    console.log(data);
                    $.dialog({
                        'title':data.title,
                        'max':false,
                        'min':false,
                        'content':data.html
                    });
                    $tabs.tabs('load' , 0);
                },
                error:function(){

                }
            })
        })
    })
</script>