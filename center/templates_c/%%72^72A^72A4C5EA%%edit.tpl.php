<?php /* Smarty version 2.6.27, created on 2019-06-26 20:17:22
         compiled from notice/edit.tpl */ ?>
<style>
#checkboxgroupList .select {
	background-color: #dcd8d8;
}
#checkboxgroupList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
}
</style>
<?php if ($this->_tpl_vars['id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" id="add-form">
    <div class="hidden">
      <input type="hidden" id="error" value="1">
    </div>
    <table class="itable itable-color">
      <tbody>
        <tr>
          
        </tr>

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server_edit.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <tr>
            <!--
          <td style="width:150px">公告类型</td>
          <td><select name="type">
              <option value="0" <?php if ($this->_tpl_vars['result']['type'] == 0): ?> selected ="true" <?php endif; ?>>新服公告
              </option>
              <option value="1" <?php if ($this->_tpl_vars['result']['type'] == 1): ?> selected ="true" <?php endif; ?>>通用公告
              </option>
              <option value="2" <?php if ($this->_tpl_vars['result']['type'] == 2): ?> selected ="true" <?php endif; ?>>活动公告
              </option>
            </select></td>
        </tr>
         <tr>
            <td style="width:150px">公告标题</td>
            <td><input type="text" name="title" id="title" value="<?php echo $this->_tpl_vars['result']['title']; ?>
"></td>
        </tr>-->
        <tr>
          <td style="width:150px">公告内容</td>
          <td><textarea rows="10" cols="100" id="contents" name="contents"><?php echo $this->_tpl_vars['result']['contents']; ?>
</textarea></td>
        </tr>
        <!--
        <tr>
            <td style="width:150px">权重</td>
            <td><input type="text" name="sort" id="sort" value=" <?php echo $this->_tpl_vars['result']['sort']; ?>
"> <span>  从大到小的顺序</span></td>
        </tr> -->
        <tr>
            <td colspan="2"><input type="submit" class="gbutton" value="编辑">

                <input type="hidden" name="status" value="<?php echo $this->_tpl_vars['result']['status']; ?>
" id="status"/>
            <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
" id="id" />
            <input type="hidden" id="error" value="1"></td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
<?php else: ?>
请选择编辑的模版
<?php endif; ?> 
<script type="text/javascript">
	  $(function(){
		
		
		
		var error = $('#error');
        $('#add-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=notice&act=add_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                })
            }
            return false;
        });
		
		
 });
 	
	 function form_check(){
		
        var error = $('#error');

        var contents = $('#contents').val();
         /*
         if(title == ''){
             $.dialog.tips('公告标题不能为空');
             error.val(1);
             return false;
         }
         else{
             error.val(0);
         }*/

        if(contents == ''){
            $.dialog.tips('公告内容不能为空');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }

</script>