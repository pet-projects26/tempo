<?php /* Smarty version 2.6.27, created on 2018-10-18 16:20:40
         compiled from common/tabs.tpl */ ?>
<div id="<?php echo $this->_tpl_vars['id']; ?>
">
    <ul>
    <?php $_from = $this->_tpl_vars['tabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
    <li><a href="<?php echo $this->_tpl_vars['item']['url']; ?>
"><?php echo $this->_tpl_vars['item']['title']; ?>
</a></li>
    <?php endforeach; endif; unset($_from); ?>
    </ul>
    <div id="tabs-1"></div>
</div>
<script type="text/javascript">
var $tabs = $("#<?php echo $this->_tpl_vars['id']; ?>
").tabs();
$('.ui-state-default a').click(function(){ // 每次点击清除jqueryui的缓存，防止hide里面的id重复产生bug
    $('.ui-tabs-panel:hidden').empty();
});
</script>