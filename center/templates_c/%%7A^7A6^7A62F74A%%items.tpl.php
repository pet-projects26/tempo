<?php /* Smarty version 2.6.27, created on 2018-10-20 17:32:20
         compiled from menu/items.tpl */ ?>
<style type="text/css">
.sortable-list-sub-<?php echo $this->_tpl_vars['type']; ?>
{margin:5px 12px 5px 0;}
.sortable-list-sub-<?php echo $this->_tpl_vars['type']; ?>
 td{width:17%;} 
h3 a{font-size:12px;font-weight:normal;}
a.tabledrag-handle{
    background: url("style/images/draggable.png") no-repeat scroll 6px 7px transparent;
    height: 13px;
    margin: -0.4em 0.5em;
    padding: 0.42em 0.5em;
    width: 13px;
	display:block;
	cursor:move;
}
a.tabledrag-handle:hover {
    background-position:  6px -13px;
}
ul.sortable-list-root-tree h3,ul.sortable-list-root-leaf h3{ height:24px; line-height:24px; border-bottom:solid 1px #BECBD6;}
.oper-parent span.oper{margin-left:40px; display:none;}
.oper-parent:hover span.oper{ display:inline;}
</style>

<ul class="sortable-list-root-<?php echo $this->_tpl_vars['type']; ?>
">
<?php $_from = $this->_tpl_vars['menuTree']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu']):
?>
<li id="<?php echo $this->_tpl_vars['menu']['id']; ?>
">
<div id="menu_line_<?php echo $this->_tpl_vars['menu']['id']; ?>
" class="oper-parent">
	<h3><?php echo $this->_tpl_vars['menu']['name']; ?>

	<span class="oper"><a href="javascript:void(0)" onclick="menu_edit(<?php echo $this->_tpl_vars['menu']['id']; ?>
)">[编辑]</a>
	&nbsp;&nbsp;<a href="javascript:void(0)" onclick="menu_delete(<?php echo $this->_tpl_vars['menu']['id']; ?>
)">[删除]</a></span>
	</h3>
	
	<ul class="sortable-list-sub-<?php echo $this->_tpl_vars['type']; ?>
">
		<?php $_from = $this->_tpl_vars['menu']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu_sub']):
?>
		<li id="<?php echo $this->_tpl_vars['menu_sub']['id']; ?>
">
		<table class="itable itable-color" id="menu_line_<?php echo $this->_tpl_vars['menu_sub']['id']; ?>
"><tr>
		  <td style="width:2%;">
		  <a class="tabledrag-handle" href="#" title="拖放重新排序">
		  &nbsp;
		  </a></td>
		  <td><?php echo $this->_tpl_vars['menu_sub']['id']; ?>
</td>
		  <td class="edit" id="<?php echo $this->_tpl_vars['menu_sub']['id']; ?>
" field="name"><?php echo $this->_tpl_vars['menu_sub']['name']; ?>
</td>
		  <td style="width:35%;" class="edit" id="<?php echo $this->_tpl_vars['menu_sub']['id']; ?>
" field="url"><?php echo $this->_tpl_vars['menu_sub']['url']; ?>
</td>
		  <td id="<?php echo $this->_tpl_vars['menu_sub']['id']; ?>
">
		  <a href="javascript:void(0)" class="change_status" status="<?php echo $this->_tpl_vars['menu_sub']['status']; ?>
" title="改变菜单状态">
		  <?php if ($this->_tpl_vars['menu_sub']['status'] == @MENU_STATUS_SHOW): ?>显示
		  <?php else: ?><span style="color:#ff0000;">隐藏</span><?php endif; ?>
		  </a>
		  </td>
		  <td><a href="javascript:void(0)" onclick="menu_delete(<?php echo $this->_tpl_vars['menu_sub']['id']; ?>
)">点击删除</a></td>
		</tr></table></li>
		<?php endforeach; endif; unset($_from); ?>
	</ul>
</div>
</li>
<?php endforeach; endif; unset($_from); ?>
<ul>
<br/><br/>
<input type="button" class="gbutton" id="save_menus_<?php echo $this->_tpl_vars['type']; ?>
" value="保存"/>
<script type="text/javascript">
$('.sortable-list-root-<?php echo $this->_tpl_vars['type']; ?>
').sortable();
$('.sortable-list-sub-<?php echo $this->_tpl_vars['type']; ?>
').sortable({
	connectWith: ".sortable-list-sub-<?php echo $this->_tpl_vars['type']; ?>
"
}).enableSelection();

function menu_delete(id){
	$.dialog.confirm('确定删除菜单?', function(){
		$.ajax({
	        type: "POST",
	        url: "admin.php?ctrl=menu&act=delete",
	        data: 'id='+id,
	        timeout: 20000,
	        error: function(){$.dialog.alert('超时');},
	        success: function(result){
	            if(result == 'success'){
	            	$.dialog.tips('删除成功');
	            	$('#menu_line_'+id).remove();
	            }else{
	            	$.dialog.alert('删除失败');
	            }
	        }
	    });
	}, function(){
	    $.dialog.tips('取消操作');
	});
}
$('#save_menus_<?php echo $this->_tpl_vars['type']; ?>
').click(function(){
	var menu_root_ids;
	var menu_sub_ids = new Array();
	$('.sortable-list-root-<?php echo $this->_tpl_vars['type']; ?>
').each(function(){
		menu_root_ids = $(this).sortable('toArray')
    });
	$('.sortable-list-sub-<?php echo $this->_tpl_vars['type']; ?>
').each(function(){
		menu_sub_ids.push($(this).sortable('toArray'));
	});
	$.ajax({
        type: "POST",
        url: "admin.php?ctrl=menu&act=save_weight&type=<?php echo $this->_tpl_vars['type']; ?>
",
        data: 'menu_root_ids='+menu_root_ids+'&menu_sub_ids='+menu_sub_ids.join('|'),
        timeout: 20000,
        error: function(){alert('error');},
        success: function(result){
            if(result == 1) alert('修改成功');
        }
    });
});

$('.edit').editable("admin.php?ctrl=menu&act=save_field", { 
    indicator : "<img src='style/images/indicator.gif'>",
    height    : "15px",
    submit    : "修改",
    cancel    : "取消",
    tooltip   : "点击修改...",
    style  : "inherit"
});

$('.change_status').click(function(){
	var $this = $(this);
	var id = $(this).parent().attr('id');
	var value = $(this).attr('status')==1? 2: 1;
	if(confirm('更改菜单显示状态？')) {
		$.ajax({
			type: "POST",
			url: "admin.php?ctrl=menu&act=save_field",
			data: 'id=' + id + '&field=status' + '&value=' + value,
			timeout: 20000,
			error: function (){alert('error');},
			success: function (result) {
				$this.attr('status', result);
				if (result == '1') {
					$this.html('显示');
				}
				if (result == '2') {
					$this.html('<span style="color:#ff0000;">隐藏</span>');
				}
				window.location.reload();
			}
		});
	}
});
function menu_edit(id){
	$menu_tabs_<?php echo $this->_tpl_vars['type']; ?>
.tabs( "url" , 1 , "admin.php?ctrl=menu&act=edit&id="+id );
    $menu_tabs_<?php echo $this->_tpl_vars['type']; ?>
.tabs( "select" , 1 );
}
</script>