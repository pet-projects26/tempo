<?php /* Smarty version 2.6.27, created on 2018-10-18 16:20:45
         compiled from sql/activity.tpl */ ?>
<style>
.type-list {
	overflow: hidden;
	margin-bottom: 30px;
}
.type-list ul li {
	float: left;
	width: 125px;
	margin: 3px;
	height: 30px;
	border: 1px solid #ccc;
	text-align: center;
	line-height: 30px;
	cursor: pointer;
	border-radius: 2px;
}
#checkboxChannelList .select {
	background-color: #dcd8d8;
}
#checkboxChannelList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
#checkboxServerList .select {
	background-color: #dcd8d8;
}
#checkboxServerList ul li {
	display: inline-block;
	width: 150px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: 1px solid #cccccc;
	cursor: pointer;
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden;
}
.p-input {
		vertical-align:middle;
	}

.p-text {
	vertical-align:middle;
}

.p-span {
	padding: 5px 10px;
	margin: 0px 3px;
}

.w-150 {
	width: 150px;
}

fieldset {
	padding: 5px;
	border: 2px solid #cc0;
	margin: 10px 0px;
}

.p-checked {
	background: #ff0;
	border-radius: 3px;
}
</style>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
  <form action="" class="fm" >
    <table class="itable itable-color">
      <tbody>
       
       <tr>
        <td style="width: 150px; margin-top: 3px; margin-bottom: 3px;">服务器列表</td>
       <td>
       	<?php $_from = $this->_tpl_vars['servers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['rows']):
?> 
		<fieldset>
		    <legend> <?php echo $this->_tpl_vars['groups'][$this->_tpl_vars['group_id']]; ?>
 </legend>
		    <?php $_from = $this->_tpl_vars['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sign'] => $this->_tpl_vars['name']):
?>
		    	<span class="p-span s-server-click" >
					<input class="p-input"  type="checkbox" name="server[]"  value="<?php echo $this->_tpl_vars['sign']; ?>
" id="server-<?php echo $this->_tpl_vars['sign']; ?>
" />
					<label for="server-<?php echo $this->_tpl_vars['sign']; ?>
" class="p-input" ><?php echo $this->_tpl_vars['name']; ?>
</label>
				</span> 
		    <?php endforeach; endif; unset($_from); ?>
		</fieldset>
		<?php endforeach; endif; unset($_from); ?>
       </td>
       </tr>

      <tr>
        <td colspan="2" style="margin-top: 3px; margin-bottom: 3px;"><input id="search" type="button" class="gbutton" value="搜索" style="margin-top: 3px; margin-bottom: 3px;">
        <input type="hidden" id="checkboxServer" name="checkboxServer" value="" />        </td>
      </tr>
        </tbody>
      
    </table>
  </form>
</div>


<div id="data" class="ui-tabs ui-widget ui-widget-content ui-corner-all " style=" margin-top:10px;">

</div>
<script>
server = new Array();
    $(function(){
		$('#search').click(function(){
	
			var server  = $("#checkboxServer").val();
			if(server == ''){
				alert('服务器不能为空');return false;	
			}
			
			$.post('admin.php?ctrl=sql&act=activity_data',{'server' : server},function(data){
				$("#data").html(data.msg);	
			},'json')
		});
		$('.p-input').click(function(){
			 if(!$(this).hasClass('select')){
				$('.p-input').attr("checked", false); 
				$('.p-input').removeClass('select');
				$('.p-span').removeClass('p-checked');

                $(this).addClass('select');
                $(this).parent().addClass('p-checked');
				$(this).attr("checked", true); 
            }
            else{
				$(this).attr("checked", false); 
                $(this).removeClass('select');
                $(this).parent().removeClass('p-checked');
            }
			getServerSelected();
		});
		
	});
	
	//每次选择改变时获取已选择的值
	function getServerSelected(){
        server = new Array();
        $('.p-input').each(function(){
            if($(this).hasClass('select')){
				
                server.push($(this).val());
            }
        });
      
        $('#checkboxServer').val(server);
    }

    function _view(id){
    	var html = $('.'+id).html();
		$.dialog({
	        title: '活动详情',
	        max: false,
	        min: false,
	        width:1000,
	        content: html
	    });
		
    }
	
		
</script> 