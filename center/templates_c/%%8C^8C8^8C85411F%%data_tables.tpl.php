<?php /* Smarty version 2.6.27, created on 2018-10-23 15:04:23
         compiled from common/data_tables.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'common/data_tables.tpl', 30, false),array('modifier', 'count', 'common/data_tables.tpl', 93, false),)), $this); ?>
<style>
    .search_float{float:left; padding:5px;height:auto;}
    #server-list label{ display: inline-block;width: 200px;height: 18px;}
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all <?php echo $this->_tpl_vars['other_data']['tipsID']; ?>
" style="margin-bottom:8px;padding:10px;display:none;">
    <span style="float:right">
        <a class="close" href="#" onClick="$(this).parent().parent().slideUp();" style="position:absolute;top:10px;right:15px;background:#fff;border:none;padding:2px 5px;">X</a>
    </span>
    <div id="TipsCont"></div>
</div>
<!--
使用方法：
1. 在$other_data中传入标识唯一的tipsID作为class
2. 需要tips数据时：
    $('.tipsID').slideDown();
    $('.tipsID').children("#TipsCont").html(html);
 -->

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="<?php echo $this->_tpl_vars['other_data']['id']; ?>
">
    <div id="datatable_header"><?php echo $this->_tpl_vars['other_data']['header']; ?>
</div>
    <div class="datatable_search" style="padding:5px;">
        <div class="search_content">
            <?php if ($this->_tpl_vars['other_data']['search_header']): ?>
            <div class="search_float"><?php echo $this->_tpl_vars['other_data']['search_header']; ?>
</div>
            <?php endif; ?>
            <?php $_from = $this->_tpl_vars['search_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
            <div class="search_float <?php if ($this->_tpl_vars['item']['type'] != 'html'): ?>search_field field_<?php echo $this->_tpl_vars['key']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['item']['class']): ?> <?php echo $this->_tpl_vars['item']['class']; ?>
<?php endif; ?>" type="<?php echo $this->_tpl_vars['item']['type']; ?>
">
                <?php if ($this->_tpl_vars['item']['title']): ?><?php echo $this->_tpl_vars['item']['title']; ?>
:&nbsp;&nbsp;<?php endif; ?>
                <?php if ($this->_tpl_vars['item']['type'] == 'input'): ?>
                    <input value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['value'])) ? $this->_run_mod_handler('default', true, $_tmp, '') : smarty_modifier_default($_tmp, '')); ?>
" <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;">
                <?php elseif ($this->_tpl_vars['item']['type'] == 'checkbox'): ?>
                <label><input type="checkbox" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['value'])) ? $this->_run_mod_handler('default', true, $_tmp, '') : smarty_modifier_default($_tmp, '')); ?>
"  style="margin-top: 4px" <?php if ($this->_tpl_vars['item']['checked']): ?>checked<?php endif; ?>><span style="margin-top: -10px"><?php echo $this->_tpl_vars['item']['text']; ?>
</span></label>
                <?php elseif ($this->_tpl_vars['item']['type'] == 'date'): ?>
                <input value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['value'])) ? $this->_run_mod_handler('default', true, $_tmp, '') : smarty_modifier_default($_tmp, '')); ?>
" <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;" class="datepicker">&nbsp;&nbsp;
                <?php elseif ($this->_tpl_vars['item']['type'] == 'range'): ?>
                <input <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;">&nbsp;~&nbsp;
                <input <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;">
                <?php elseif ($this->_tpl_vars['item']['type'] == 'range_date'): ?>
                <input value='<?php echo $this->_tpl_vars['item']['value'][0]; ?>
' <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;" class="datepicker">&nbsp;~&nbsp;
                <input value='<?php echo $this->_tpl_vars['item']['value'][1]; ?>
' <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;" class="datepicker">
                <?php elseif ($this->_tpl_vars['item']['type'] == 'range_time'): ?>
                <input type="text" value='<?php echo $this->_tpl_vars['item']['value'][0]; ?>
' <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;" class="datetimepicker">&nbsp;~&nbsp;
                <input type="text" value='<?php echo $this->_tpl_vars['item']['value'][1]; ?>
' <?php if ($this->_tpl_vars['item']['size']): ?>size="<?php echo $this->_tpl_vars['item']['size']; ?>
"<?php endif; ?> style="width:<?php echo $this->_tpl_vars['item']['width']; ?>
px;" class="datetimepicker">
                <?php elseif ($this->_tpl_vars['item']['type'] == 'select'): ?>
                <select style="width:200px !important;">
                <?php $_from = $this->_tpl_vars['item']['value']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['it']):
?>
                    <?php if (isset ( $this->_tpl_vars['item']['default'] )): ?>
                        <option value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['item']['default'] == $this->_tpl_vars['key']): ?>selected='selected'<?php endif; ?>><?php echo $this->_tpl_vars['it']; ?>
</option>
                    <?php else: ?>
                        <option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['it']; ?>
</option>
                    <?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
                </select>
                <?php elseif ($this->_tpl_vars['item']['type'] == 'selects'): ?>
                <select>
                <?php $_from = $this->_tpl_vars['item']['value'][0]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['it']):
?>
                    <option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['it']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
                </select>&nbsp;~&nbsp;
                <select>
                <?php $_from = $this->_tpl_vars['item']['value'][1]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['it']):
?>
                    <option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['it']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
                </select>
                <?php elseif ($this->_tpl_vars['item']['type'] == 'scp'): ?>
                    <?php echo $this->_tpl_vars['item']['value']; ?>

                <?php endif; ?>
            </div>
            <?php endforeach; endif; unset($_from); ?>
            <?php if ($this->_tpl_vars['search_fields']): ?>
            <div class="search_float"><input type="button" class="datatable_search_button gbutton" value="搜索"></div>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['other_data']['search_footer']): ?>
            <div class="search_float"><?php echo $this->_tpl_vars['other_data']['search_footer']; ?>
</div>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['scp']): ?>
            <div><?php echo $this->_tpl_vars['scp']; ?>
</div>
            <?php endif; ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="margin:5px" id="table_header"><?php if ($this->_tpl_vars['other_data']['table_header']): ?><?php echo $this->_tpl_vars['other_data']['table_header']; ?>
<?php endif; ?></div>
    <table cellpadding="0" cellspacing="0" border="0" class="table_list">
        <thead>
            <tr height="44px">
            <?php $_from = $this->_tpl_vars['show_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <th <?php if ($this->_tpl_vars['item']['width']): ?>width="<?php echo $this->_tpl_vars['item']['width']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['item']['title']; ?>
</th>
            <?php endforeach; endif; unset($_from); ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="<?php echo ((is_array($_tmp=$this->_tpl_vars['show_fields'])) ? $this->_run_mod_handler('count', true, $_tmp) : count($_tmp)); ?>
" class="dataTables_empty"></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <?php $_from = $this->_tpl_vars['show_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <th><?php echo $this->_tpl_vars['item']['title']; ?>
</th>
                <?php endforeach; endif; unset($_from); ?>
            </tr>
        </tfoot>
    </table>
</div>
<script type="text/javascript">
$('select').select2();
$('.datepicker').datepicker();
var timepickerlang = {timeText: '时间', hourText: '小时', minuteText: '分钟', currentText: '现在', closeText: '确定'}
$('.datetimepicker').datetimepicker(timepickerlang);
$('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .datatable_search .title').toggle(function(){
    $('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .search_content').slideUp();
},function(){
    $('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .search_content').slideDown();
});

var $<?php echo $this->_tpl_vars['other_data']['id']; ?>
 = $('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .table_list').dataTable({
    "bProcessing": true,
    "deferRender": true,
    "bServerSide": true,
    "iDisplayLength": <?php echo $this->_tpl_vars['other_data']['iDisplayLength']; ?>
,
    "bLengthChange": true,
    "bStateSave": <?php if ($this->_tpl_vars['other_data']['bStateSave'] == true): ?>true<?php else: ?>false<?php endif; ?>,
    "bFilter": true,
    "sPaginationType": "full_numbers",
    "sAjaxSource": "<?php echo $this->_tpl_vars['ajax_source']; ?>
",
    "aaSorting": [[ <?php echo $this->_tpl_vars['other_data']['sortCol']; ?>
, "<?php echo $this->_tpl_vars['other_data']['sortDir']; ?>
" ]],
    'bSort': <?php echo $this->_tpl_vars['other_data']['bSort']; ?>
,
    "oLanguage": { "sUrl": "style/js/dt_cn.t" },
    "aoColumns": [<?php echo $this->_tpl_vars['aoColumns']; ?>
],
    "sDom": '<?php if ($this->_tpl_vars['other_data']['sDom']): ?><?php echo $this->_tpl_vars['other_data']['sDom']; ?>
<?php else: ?><"top"lp>rt<"bottom"ip><"clear"><?php endif; ?>'
});

$('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .table_list tbody tr').live('mouseover', function(){
    $(this).addClass('row_selected');
}).live('mouseout' , function(){
    $(this).removeClass('row_selected');
});

$('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .datatable_search input').keydown(function (event){
    if(event.keyCode==13){
        $('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .datatable_search_button').trigger("click");
    }
});
function getFilter(){
    var data = new Array();
    $("#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .search_field").each(function(){
        var type = $(this).attr('type');
        var value = '';
        if(type == 'input' || type == 'select'){
            value = $(this).find(type).val();
        }else if(in_array(type, ['range', 'range_date', 'range_time'])){
            var range = new Array();
            $(this).find('input').each(function(){
                range.push($(this).val());
            });
            value = range.join('|');
        }else if(type == "date"){
            value = $(this).find("input").val();
        }
        else if(type == 'checkbox' && $(this).find('input[type="checkbox"]')[0].checked){
            value = 1;
        }
        else if(type == 'selects'){
            var selects = new Array();
            $(this).find('select').each(function(){
                selects.push($(this).val());
            });
            value = selects.join('|');
        }
        else if(type == 'scp'){
            value = JSON.stringify($('#checkboxScp').data());
            $.cookie('scp', value);
        }
        data.push(value);
    });
    return data;
}
$('#<?php echo $this->_tpl_vars['other_data']['id']; ?>
 .datatable_search_button').click(function(){
    $<?php echo $this->_tpl_vars['other_data']['id']; ?>
.fnMultiFilter(getFilter());
});
<?php echo ((is_array($_tmp=@$this->_tpl_vars['other_data']['js'])) ? $this->_run_mod_handler('default', true, $_tmp, '') : smarty_modifier_default($_tmp, '')); ?>

</script>
<?php echo ((is_array($_tmp=@$this->_tpl_vars['other_data']['jsf'])) ? $this->_run_mod_handler('default', true, $_tmp, '') : smarty_modifier_default($_tmp, '')); ?>
