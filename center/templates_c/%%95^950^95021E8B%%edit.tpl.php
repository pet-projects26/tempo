<?php /* Smarty version 2.6.27, created on 2019-04-30 15:59:08
         compiled from broadcast/edit.tpl */ ?>
<?php if ($this->_tpl_vars['data']['id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm"  id="form" method="post">
        <table class="itable itable-color">
            <div class="hidden">
                <input type="hidden" id="error" value="1">
            </div>
            <tbody>
            
                <input type="hidden" id="id"  name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" />
                <tr>
                    <td style="width:150px;">开始时间</td>
                    <td><input class="datepicker" type="text" id="starttime" name="starttime" value="<?php echo $this->_tpl_vars['data']['starttime']; ?>
"></td>
                </tr>
                <tr>
                    <td style="width:150px;">结束时间</td>
                    <td><input type="text" class="datepicker" id="endtime" name="endtime" value="<?php echo $this->_tpl_vars['data']['endtime']; ?>
"></td>
                </tr>
                <tr>
                    <td style="width:150px;">显示</td>
                    <td><select name="type" id="type">
						<option value="0">未选择</option>
						<?php $_from = $this->_tpl_vars['broadCastType']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['rows']):
?>
						<option value="<?php echo $this->_tpl_vars['rows']['type']; ?>
" <?php if ($this->_tpl_vars['data']['type'] == $this->_tpl_vars['rows']['type']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['rows']['name']; ?>
</option>
						<?php endforeach; endif; unset($_from); ?>
                    </select></td>
                </tr>
                <tr>
                    <td style="width:150px;">间隔时间</td>
                    <td><input style="margin-right: 5px"  type="text" id="intervaltime" name="intervaltime" value="<?php echo $this->_tpl_vars['data']['intervaltime']; ?>
">秒</td>
                </tr>
                 <tr>
                    <td >内容</td>
                    <td><textarea style="height:500px; width:600px;" name="contents" id="contents"><?php echo $this->_tpl_vars['data']['content']; ?>
</textarea></td>
                </tr>
                <tr>
                    <td colspan="2"><input id="gbutton" type="button" class="gbutton" value="添加"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择编辑的广播
<?php endif; ?>
<script type="text/javascript">
    $(function(){
        var timepickerlang = { timeText:'时间' , hourText:'小时' , minuteText:'分钟' , currentText:'现在' , closeText:'确定' }
        $('.datepicker').datetimepicker(timepickerlang);
    });
	$("#gbutton").click(function(){
		var id =$("#id").val();
		if(id == ''){id=0;}
		var starttime=$("#starttime").val();
		var endtime=$("#endtime").val();
		var intervaltime=$("#intervaltime").val();
		var contents=$("#contents").val();
		var type=$("#type").val();

		if (type == 0) {
            $.dialog.tips('显示类型不能为空');	return false;
        }
		if(starttime == ''){
			$.dialog.tips('开始时间不能为空');	return false;
		}
		if(endtime == ''){
			$.dialog.tips('结束时间不能为空');	return false;
		}
		starttime=parseInt(new Date(starttime).getTime())/1000
		endtime=parseInt(new Date(endtime).getTime())/1000
		
		if(endtime <= starttime){
			$.dialog.tips('结束时间不能小于开始时间');return false;	
		}
		if(endtime - starttime<intervaltime){
			$.dialog.tips('间隔时间不能大于结束时间与开始时间的差');return false;	
		}
		if(contents==''){
			$.dialog.tips('内容不能为空');return false;		
		}
		$.ajax({
			url: 'admin.php?ctrl=broadcast&act=add_data&id=' + id,
			type: 'POST',
			dataType: 'JSON',
			data: {'id':id,'starttime':starttime,'endtime':endtime,'intervaltime':intervaltime,'content':contents,'type':type}
		}).done(function(data){
			if(data !==0){
				$.dialog.tips('修改成功');
				$tabs.tabs('select' , 0);
				$tabs.tabs('url' , 0 , 'admin.php?ctrl=broadcast&act=record' );
               
			}else{
				$.dialog.tips('修改失败');
			}
			
		})	
			
	});
</script>