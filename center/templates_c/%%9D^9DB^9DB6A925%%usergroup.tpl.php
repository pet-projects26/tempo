<?php /* Smarty version 2.6.27, created on 2018-10-22 17:15:56
         compiled from permit/usergroup.tpl */ ?>
<div style="margin-bottom:20px;">
<select id="group_select">
	<option value="">选择..</option>
	<?php $_from = $this->_tpl_vars['groupList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	<option value="<?php echo $this->_tpl_vars['item']['groupid']; ?>
" <?php if ($this->_tpl_vars['item']['groupid'] == $this->_tpl_vars['usergroup']['groupid']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
	<?php endforeach; endif; unset($_from); ?>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;
<input id="update_all_group" type="button" value="更新所有用户组权限" class="gbutton"/>
</div>
<div id="permit_setting">
    <h3><a href="#">中央后台权限</a></h3>
    <div>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "permit/center_permit.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </div>
    <h3><a href="#">渠道权限</a></h3>
    <div>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "permit/server_permit.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </div>
</div>   
<script type="text/javascript">
$('#permit_setting').accordion({
    active: 0,
    collapsible: true,
    animated: false,
    clearStyle: true,
});
$('#group_select').change(function(){
	$tabs.tabs( "url" , 1 , "admin.php?ctrl=permit&act=usergroup&groupid="+$(this).val() );
    $tabs.tabs( "load" , 1 );
});

$('.permit_setting_form').ajaxForm({
    complete: function(xhr) {
        var msg =xhr.responseText;
        $.dialog.alert(msg);
    }
});

$('.check_all').click(function(){
    var checked = $(this).attr('checked');
    var value;
    if(checked){
        value = true;
    }else{
        value = false;
    }
    $(this).parents('table').find('input[type="checkbox"]').attr('checked', value);
});

$('.check_row').click(function(){
    var checked = $(this).attr('checked');
    var value;
    if(checked){
        value = true;
    }else{
        value = false;
    }
    $(this).parents('tr').find('input[type="checkbox"]').attr('checked', value);
});

$('.check_cancle').click(function(){
    var checked = $(this).attr('checked');
    if(!checked){
        $(this).parents('table').find('input.check_all').attr('checked', false);
        $(this).parents('tr').find('th input[type="checkbox"]').attr('checked', false);
    }
});
$("#update_all_group").click(function(){
	$.ajax({
        type: "POST",
        url: "admin.php?ctrl=permit&act=update_all_group",
        timeout: 20000,
        error: function(){$.dialog.alert('超时');},
        success: function(result){
            $.dialog.tips(result);
        }
    });
});
</script>