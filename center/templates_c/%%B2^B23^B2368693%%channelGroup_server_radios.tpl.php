<?php /* Smarty version 2.6.27, created on 2018-10-18 16:40:13
         compiled from ../plugin/channelGroup_server_radios.tpl */ ?>
<style>
    .type-list ul li {
        float: left;
        width: 125px;
        margin: 3px;
        height: 30px;
        border: 1px solid #ccc;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        border-radius: 2px;
    }
    .p-input {
        vertical-align:middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }
    .highlight {
        background: yellow;
        color: red;
    }
    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
    .all-checked {
        vertical-align: middle;
    }
    .select{
        background-color: #36ca95;
    }
</style>
<script>
    $().ready(function() {
        //其他渠道
        $('.c-channel-group-list input').unbind('click').bind('click', function() {
            var mark = $(this).prop('checked');
            $(".c-channel-group-list").removeClass('p-checked');
            if (mark) {
                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }

            var checkLen = $('.c-channel-group-list input:checked').length;
            //$(".server-fieldset").removeClass('server-fieldset');
            //根据渠道组筛选服务器
            InputValue=$(this).val();
            var len =  $('#'+InputValue).length;
            if(typeof len == 'undefined' || len == 0){
                $(".server fieldset").each(function(){
                    //清除选择
                    console.log($(this));
                    $(this).children('span').removeClass('p-checked');
                    $(this).children('span').find('.p-input').prop('checked',0);
                    $(this).hide();
                });
            }else {
                $('#' + InputValue).addClass('show');
                $("#" + InputValue).siblings().removeClass('show');
                $('.server fieldset').each(function () {  //遍历child,如果有show 就让他显示，没有就隐藏
                    //$('.server fieldset').eq(0).show();
                    if ($(this).hasClass('show')) {
                        $(this).show();
                    } else {
                        //清除选择
                        $(this).children('span').removeClass('p-checked');
                        $(this).children('span').find('.p-input').prop('checked', 0);
                        $(this).hide();
                    }
                });
            }
        });
        // 非全服的其他服
        $('.s-server-click  input[value != ""]').unbind('click').bind('click', function() {
            var obj = '.p-input';
            var mark = $(this).prop('checked');
            console.log($(this));
            if (mark) {
                $(this).parent('span').addClass('p-checked').siblings().removeClass('p-checked');
            }
            var checkLen = $(obj + ' input[value != ""]:checked').length;

            //如果都没有选中，则自动选中全选
            if (checkLen == 0) {
                $(obj + ' input[value=""]').prop('checked', 1);
                $(obj + ' input[value=""]').parent('span').addClass('p-checked');
            } else {
                $(obj + ' input[value=""]').prop('checked', 0);
                $(obj + ' input[value=""]').parent('span').removeClass('p-checked');
            }

        });
    });

</script>
<tr></tr>
<!-- 渠道组 Start -->
<!-- 渠道组 Start -->
<tr class='p-tr'>
    <td style="width:150px">渠道组</td>
    <td>
        <div id="pretime">
            <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['row']):
?>
			<span class="p-span c-channel-group  c-channel-group-list span-click action-are" style="cursor: pointer">
				<input class="p-input" type="radio" name="channel_group" value="<?php echo $this->_tpl_vars['id']; ?>
" id="channel_group-<?php echo $this->_tpl_vars['id']; ?>
"/>
				<label for="channel_group-<?php echo $this->_tpl_vars['id']; ?>
" class="p-input"><?php echo $this->_tpl_vars['row']; ?>
</label>
			</span>
            <?php endforeach; endif; unset($_from); ?>
        </div>
    </td>
</tr>

<!-- 渠道组 End -->

<!-- 服务器 Start  -->
<tr class="p-tr server">
    <td class="w-150">服务器</td>
    <td>
        <?php $_from = $this->_tpl_vars['servers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['rows']):
?>
        <fieldset class="server-fieldset" id="<?php echo $this->_tpl_vars['group_id']; ?>
" style="display: none">
            <legend> <?php echo $this->_tpl_vars['groups'][$this->_tpl_vars['group_id']]; ?>
 </legend>
            <?php $_from = $this->_tpl_vars['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sign'] => $this->_tpl_vars['name']):
?>
		    	<span class="p-span s-server-click" >
					<input class="p-input"  type="radio" name="server"  value="<?php echo $this->_tpl_vars['sign']; ?>
" id="server-<?php echo $this->_tpl_vars['sign']; ?>
" />
					<label for="server-<?php echo $this->_tpl_vars['sign']; ?>
" class="p-input" ><?php echo $this->_tpl_vars['name']; ?>
</label>
				</span>
            <?php endforeach; endif; unset($_from); ?>
        </fieldset>
        <?php endforeach; endif; unset($_from); ?>
    </td>
</tr>
<!-- 服务器 End -->