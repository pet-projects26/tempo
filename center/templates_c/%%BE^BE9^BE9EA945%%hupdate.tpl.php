<?php /* Smarty version 2.6.27, created on 2019-03-28 18:12:02
         compiled from setting/hupdate.tpl */ ?>
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/animate.min.css">
<link rel="stylesheet" type="text/css" href="style/css/jQuery-gDialog/jquery.gDialog.css">
<style type="text/css">
    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-hupdate-form">
        <table class="itable itable-color">
            <tbody>
            <tr>
                <td colspan="2">
                    <div style="margin:15px 0;">
                        说明：<br>
                        1. 在添加字符串的输入框填好字符串后 点击 添加按钮 或者 按下键盘的Enter键 可添加字符串<br>
                        2. 删除按钮 用于删除最后一个加入的字符串
                    </div>
                </td>
            </tr>
            <!-- 渠道组 Start -->
            <tr class='p-tr'>
                <td style="width:150px">渠道组</td>
                <td>
                    <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['row']):
?>
                    <span class="p-span c-channel-group  c-channel-group-list span-click">
				<input class="p-input" type="radio" name="channel_group[]" value="<?php echo $this->_tpl_vars['id']; ?>
" id="channel_group-<?php echo $this->_tpl_vars['id']; ?>
"/>
				<label for="channel_group-<?php echo $this->_tpl_vars['id']; ?>
" class="p-input"><?php echo $this->_tpl_vars['row']; ?>
</label>
			</span>
                    <?php endforeach; endif; unset($_from); ?>
                </td>
            </tr>

            <!-- 渠道组 End -->

            <!-- 服务器 Start  -->
            <tr class="p-tr server">
                <td class="w-150">服务器</td>
                <td>
                    <fieldset>
                        <legend> 当前渠道组下所有服务器</legend>
                        <span class="p-span s-server-click">
				<input class="p-input" type="checkbox" name="server[]" value="" id="server-all"/>
				<label for="server-all" class="p-input">当前所选渠道组下所有服务器</label>
			</span>
                        <font style="color:#f00; font-weight: bold;">选择该选项后，代表选中当前所选的渠道组下所有服务器</font>
                    </fieldset>
                    <?php $_from = $this->_tpl_vars['servers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group_id'] => $this->_tpl_vars['rows']):
?>
                    <fieldset class="server-box" id="<?php echo $this->_tpl_vars['group_id']; ?>
" style="display: none">
                        <legend> <?php echo $this->_tpl_vars['groups'][$this->_tpl_vars['group_id']]; ?>
</legend>
                        <?php $_from = $this->_tpl_vars['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sign'] => $this->_tpl_vars['name']):
?>
                        <span class="p-span s-server-click">
					<input class="p-input" type="checkbox" name="server[]" value="<?php echo $this->_tpl_vars['sign']; ?>
" id="server-<?php echo $this->_tpl_vars['sign']; ?>
"/>
					<label for="server-<?php echo $this->_tpl_vars['sign']; ?>
" class="p-input"><?php echo $this->_tpl_vars['name']; ?>
</label>
				</span>
                        <?php endforeach; endif; unset($_from); ?>
                    </fieldset>
                    <?php endforeach; endif; unset($_from); ?>
                </td>
            </tr>
            <!-- 服务器 End -->
            <tr>
                <td style="width:150px;">操作类型</td>
                <td>
                    <select name="type" id="type">
                        <option value="0">未选择</option>
                        <?php $_from = $this->_tpl_vars['hotUpdateOperate']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['rows']):
?>
                        <option value="<?php echo $this->_tpl_vars['rows'][0]; ?>
"><?php echo $this->_tpl_vars['rows'][1]; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width:150px;">选择协议</td>
                <td>
                    <select name="netinfo" id="netinfo">
                        <option value="0">未选择</option>
                        <?php $_from = $this->_tpl_vars['hotUpdateType']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['rows']):
?>
                        <option value="<?php echo $this->_tpl_vars['rows']; ?>
"><?php echo $this->_tpl_vars['key']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                </td>
            </tr>
            <tr style="display: none;" id="addstr_tr">
                <td style="width:150px;">添加字符串</td>
                <td>
                    <input type="text" id="str" style="width:200px;">
                    <input type="button" id="addStr" class="gbutton" value="添加">
                    <input type="button" id="delStr" class="gbutton" value="删除">
                </td>
            </tr>
            <tr style="display: none" id="strlist_tr">
                <td style="width:150px;">已添加字符串</td>
                <td>
                    <div id="strlist"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="发送">
                    <input type="hidden" value="1" id="error">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript" src="style/js/jQuery-gDialog/jquery.gDialog.min.js"></script>
<script type="text/javascript">
    $(function () {

        $('#type').change(function () {

            var type = $(this).val();

            console.log(type);

            switch (type) {
                case '34603008':
                    $('#addstr_tr').show();
                    $('#strlist_tr').show();
                    break;
                case '34603009':
                    $('#addstr_tr').hide();
                    $('#strlist_tr').hide();
                    break;
                default:
                    $('#addstr_tr').hide();
                    $('#strlist_tr').hide();
                    break;
            }
        });


        $('#str').keypress(function (event) {
            var keycode = event.which;
            if (keycode == 13) {
                addStr();
                return false;
            }
        });

        $('#addStr').click(function () {
            addStr();
        });

        $('#delStr').click(function () {
            delStr();
        });

        var error = $('#error');
        $('#manual-hupdate-form').submit(function (event) {
            form_check();
            if (error.val() == '1') {
                return false;
            }
            else {

                $.ajax({
                    url: 'admin.php?ctrl=setting&act=hupdate_action&save=1',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    success: function (res) {
                        if (res.code == 200) {
                            $.gDialog.alert('成功', {
                                title: '操作成功!',
                            });
                        }
                    },
                    error: function () {
                        $.gDialog.alert('错误', {
                            title: '出现错误,请打开Network查看报错信息!',
                        });
                    }
                })


                // var server = [];
                // $("input[name='server[]']:checked").each(function(index,item){
                //         server[index] = $(this).val();
                //     }
                // );
                //
                // var netinfo = $('#netinfo').val();
                //
                // var str = [];
                //
                // $("input[name='str[]']").each(function(index,item){
                //         str[index] = $(this).val();
                //     }
                // );

                //循环去获取websocket配置后连接websocket
                /*$.ajax({
                    url: 'admin.php?ctrl=setting&act=websorcketConfig',
                    type: 'POST',
                    dataType: 'JSON',
                    data: ,
                }).done(function(data){
                    if (!data) {
                        $.gDialog.alert('请检查服务器配置是否配置了websocket', {
                            title: '错误',
                        });
                    }

                    var returnArr = [];
                    // $.each(data, function (index, item) {
                    //     if (item.websocket_host == '') {
                    //         //$.dialog.alert('请检查服务器为'+index+'的服务器配置的websocket_host');
                    //         $.gDialog.alert('请检查服务器为'+index+'的服务器配置的websocket_host', {
                    //             title: '错误',
                    //         });
                    //     }
                    //     if (item.websocket_port == '') {
                    //         //$.dialog.alert('请检查服务器为'+index+'的服务器配置的websocket_port');
                    //         $.gDialog.alert('请检查服务器为'+index+'的服务器配置的websocket_port', {
                    //             title: '错误',
                    //         });
                    //     }
                    //
                    //     Socket.index = $.websocket({
                    //         domain: item.websocket_host,   //这是与服务器的域名或IP
                    //         port: item.websocket_port,                  //这是服务器端口号
                    //         // protocol:"text",            //这东西可有可无,组合起来就是 ws://www.qhnovel.com:8080/test
                    //         onOpen:function(event){
                    //             // alert("已经与服务端握手,onOpen可省略不写");
                    //             console.log(index, 1);
                    //             Socket.index.send(netinfo, str);
                    //         },
                    //         onError:function(event){
                    //             // alert("发生了错误,onError可省略不写");
                    //             //$.dialog.alert(Socket1.szServer + '服务器发送时发送错误');
                    //
                    //             $.gDialog.alert(Socket1.szServer + '服务器发送时发送错误', {
                    //                 title: '错误',
                    //             });
                    //
                    //         },
                    //         onSend:function(msg){
                    //             console.log(index, 2);
                    //             // alert("发送数据额外的代码,可省略不写");
                    //         },
                    //         onMessage:function(result,nTime){
                    //             console.log(index, 3);
                    //             console.log(result);
                    //             if(result.length == 1 && result[0] == 0) {
                    //                 returnArr.push(index);
                    //             }
                    //         }
                    //     });
                    //
                    //     console.log(index, 4);
                    // });

                    $.each(data, function (index, item) {
                        if (item.websocket_host == '') {
                            $.dialog.alert('请检查服务器为'+index+'的服务器配置的websocket_host');
                        }
                        if (item.websocket_port == '') {
                            $.dialog.alert('请检查服务器为'+index+'的服务器配置的websocket_port');
                        }

                        //实例化websocket并发送协议字符串
                        var protocol = (window.location.protocol == 'http:') ? 'ws:' : 'wss:';

                        var host = protocol + item.websocket_host + ':' + item.websocket_port;

                        window.WebSocket = window.WebSocket || window.MozWebSocket;
                        if(!window.WebSocket) { // 检测浏览器支持
                            this.error('Error: WebSocket is not supported .');
                            return;
                        }

                        var ws = new WebSocket(host);

                        ws.binaryType = 'arraybuffer';

                        ws.onopen = function(evt) {

                            var content = null;
                            var size = 0;

                            if (str != null) {
                                content = serializeMsgPack(str);
                                size = content.byteLength;
                            }

                            var buffer =new Uint8Array(2 + 4 + size);
                            var stream = new DataView(buffer.buffer);
                            var offset = 0;
                            stream.setUint16(offset, buffer.byteLength, true);
                            offset += 2;
                            stream.setUint32(offset, netinfo, true);
                            offset += 4;
                            if (content != null) {
                                buffer.set(content, offset);
                            }

                            ws.send(buffer);
                        };

                        ws.onmessage = function(evt) {
                            var data = evt.data;
                            var stream = new DataView(data);
                            var offset = 0;
                            var size = stream.getUint16(offset, true);
                            offset += 2;
                            var opcode = stream.getUint32(offset, true);
                            offset += 4;
                            var bytes = new Uint8Array(data, offset);
                            var tuple = deserializeMsgPack(bytes);

                            console.log(tuple);
                            if (tuple.length == 1 && tuple[0] == 0) {
                                returnArr.push(tuple);
                                ws.close();
                            }
                        };
                    })

                    var time = null;
                    var millisecond = 1000 + server.length * 200; //一次性定时器的毫秒数

                    time = setTimeout(function(){
                        if (returnArr.length == server.length) {
                            $.gDialog.alert('所有服务都已发送成功!', {
                                title: '成功',
                            });
                        } else {

                            var num = server.length - returnArr.length;

                            $.gDialog.alert('有'+ num + '个服务器错误!', {
                                title: '错误',
                            });
                        }
                    }, millisecond);

                });
            }*/
                return false;
            }
        });
    });

    function addStr() {
        var str = $('#str').val();
        if (str == '') {
            $.dialog.tips('字符串不能为空');
            return false;
        }
        var strArr = [];
        $("input[name='str[]']").each(function (index, item) {
                strArr[index] = $(this).val();
            }
        );

        // if (strArr.length == 1) {
        //     $.dialog.tips('只能添加一个字符串');
        //     return false;
        // }

        var html = '<div style="margin:5px 0 10px 0;"><input class="strlist" name="str[]" style="width:200px;" type="text" value="' + str + '"></div>';
        $('#strlist').append(html);
        var strlength = $('#strlist').children('div').length;
        $('#strlist').children('div').last().append('<span>&nbsp;（第' + strlength + '个）</span> ');
        $('#str').val('');
        $('#str').focus();
    }

    function delStr() {
        $('#strlist').children('div').last().remove();
    }

    function form_check() {
        var error = $('#error');
        //服务器
        var server = $('#server');
        if (server.val() == 0) {
            $.dialog.tips('未选择服务器');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }

        var type = $('#type').val();

        if (type == 0) {
            $.dialog.tips('未选择操作类型');
            error.val(1);
            return false;
        } else {
            error.val(0);
        }

        //网络编号
        var netinfo = $('#netinfo');
        if (netinfo.val() == 0) {
            $.dialog.tips('未选择协议');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }

        //字符串列表
        if (type == '34603008') {
            var strlength = $('#strlist').children('div').length;
            if (strlength == 0) {
                $.dialog.tips('没有添加任何字符串');
                error.val(1);
                return false;
            }
            else {
                error.val(0);
            }


            $('.strlist').each(function () {
                if ($(this).val() == '') {
                    $.dialog.tips('已添加字符串 中 存在空的输入框');
                    error.val(1);
                    return false;
                }
                else {
                    error.val(0);
                }
            });
        }
    }

    $().ready(function () {
        //其他渠道
        $('.c-channel-group-list input').unbind('click').bind('click', function () {
            var mark = $(this).prop('checked');
            $(".c-channel-group-list").removeClass('p-checked');
            if (mark) {
                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }

            var checkLen = $('.c-channel-group-list input:checked').length;
            $(".server-fieldset").removeClass('server-fieldset');
            //根据渠道组筛选服务器
            InputValue = $(this).val();

            $('#' + InputValue).addClass('show');
            $("#" + InputValue).siblings().removeClass('show');
            $('.server fieldset').each(function () {  //遍历child,如果有show 就让他显示，没有就隐藏
                $('.server fieldset').eq(0).show();
                if ($(this).hasClass('show')) {
                    $(this).show();
                } else {
                    //清除选择
                    $(this).children('span').removeClass('p-checked');
                    $(this).children('span').find('.p-input').prop('checked', 0);
                    $(this).hide();
                }
            });
        });


        //当前渠道的所有服
        $('.s-server-click input[value=""]').unbind('click').bind('click', function () {
            var mark = $(this).prop('checked');

            if (mark) {
                $('.s-server-click  input[value != ""]').prop('checked', 0);
                $('.s-server-click  input[value != ""]').parent('span').removeClass('p-checked');

                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }
        });

        // 非全服的其他服
        $('.s-server-click  input[value != ""]').unbind('click').bind('click', function () {
            var obj = '.s-server-click';
            var mark = $(this).prop('checked');
            if (mark) {

                $(this).parent('span').addClass('p-checked');
            } else {
                $(this).parent('span').removeClass('p-checked');
            }

            var allLen = $(obj + ' input').length;

            var checkLen = $(obj + ' input[value != ""]:checked').length;

            //如果都没有选中，则自动选中全选
            if (checkLen == 0) {
                $(obj + ' input[value=""]').prop('checked', 1);
                $(obj + ' input[value=""]').parent('span').addClass('p-checked');
            } else {
                $(obj + ' input[value=""]').prop('checked', 0);
                $(obj + ' input[value=""]').parent('span').removeClass('p-checked');
            }

        });
    });
</script>