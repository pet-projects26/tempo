<?php /* Smarty version 2.6.27, created on 2018-10-23 17:52:46
         compiled from vipnotice/edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'getLastKey', 'vipnotice/edit.tpl', 38, false),)), $this); ?>
<style type="text/css">
    .p-input {
        vertical-align: middle;
    }

    .p-text {
        vertical-align: middle;
    }

    .p-span {
        padding: 5px 10px;
        margin: 0px 3px;
    }

    .w-150 {
        width: 150px;
    }

    fieldset {
        padding: 5px;
        border: 2px solid #cc0;
        margin: 10px 0px;
    }

    .p-checked {
        background: #ff0;
        border-radius: 3px;
    }

    .big-input {
        width: 500px;
    }
</style>
<script type="text/javascript" src="templates/vipnotice/vipnotice.js"></script>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form id="notice-push-form">
        <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
">
        <table class="itable itable-color" level-num="<?php echo get_last_key(array('arr' => $this->_tpl_vars['row']['level']), $this);?>
">
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> 推送方式</legend>
                    <span class="p-span push-type">
					<input class="p-input push-type"<?php if (in_array ( 2 , $this->_tpl_vars['row']['type'] )): ?> checked <?php endif; ?> type="checkbox" name="type[]" value="2"/>
					<label>邮件推送</label>
				    </span>
                    <span class="p-span push-type">
					<input class="p-input push-type" <?php if (in_array ( 1 , $this->_tpl_vars['row']['type'] )): ?> checked<?php endif; ?>  type="checkbox" name="type[]" value="1"/>
					<label>公告变更</label>
				    </span>
                    </fieldset>
                </td>
            </tr>
            <?php $_from = $this->_tpl_vars['row']['level']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['info']):
?>
            <tr class="notice-setting">
                <td>
                    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                        <table>
                            <tbody>
                            <tr>
                                <td>开启档次</td>
                                <td>
                                    <input type="number" value="<?php echo $this->_tpl_vars['info']; ?>
" name="first_level[<?php echo $this->_tpl_vars['key']; ?>
]"/>&nbsp;&nbsp;元
                                </td>
                            </tr>
                            <tr>
                                <td>邮件标题</td>
                                <td>
                                    <input value="<?php echo $this->_tpl_vars['row']['title'][$this->_tpl_vars['key']]; ?>
" name="mail_title[<?php echo $this->_tpl_vars['key']; ?>
]" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>邮件内容</td>
                                <td>
                                    <textarea name="content[<?php echo $this->_tpl_vars['key']; ?>
]" class="content"
                                              style="width:600px;height:200px;margin:0;"><?php echo $this->_tpl_vars['row']['content'][$this->_tpl_vars['key']]; ?>
</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>联系方式</td>
                                <td>
                                    <input type="text" value="<?php echo $this->_tpl_vars['row']['connect'][$this->_tpl_vars['key']]; ?>
" name="connect[<?php echo $this->_tpl_vars['key']; ?>
]" class="big-input">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td>
                    <input type="button" class="gbutton del-level" value="删除该档次">
                </td>
            </tr>
            <?php endforeach; endif; unset($_from); ?>
            <tr class="add-level-btn">
                <td colspan="2">
                    <input type="button" class="gbutton add-level" value="添加充值档次"/>
                </td>
            </tr>

            <!-- 渠道组 Start -->
            <tr class='p-tr channel_group'>
                <td>
                    <fieldset>
                        <legend> 当前渠道组下所有服务器</legend>
                    <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['row']):
?>
			<span class="p-span c-channel-group  c-channel-group-list span-click <?php if (in_array ( $this->_tpl_vars['id'] , $this->_tpl_vars['data']['agent'] )): ?> p-checked <?php endif; ?>">
				<input class="p-input"  type="radio" name="channel_group"  value="<?php echo $this->_tpl_vars['id']; ?>
" <?php if (in_array ( $this->_tpl_vars['id'] , $this->_tpl_vars['data']['agent'] )): ?> checked <?php else: ?> disabled="disabled" <?php endif; ?>  id="channel_group-<?php echo $this->_tpl_vars['id']; ?>
" />
				<label for="channel_group-<?php echo $this->_tpl_vars['id']; ?>
" class="p-input" ><?php echo $this->_tpl_vars['row']; ?>
</label>
			</span>
                    <?php endforeach; endif; unset($_from); ?>
                    </fieldset>
                </td>
            </tr>

            <!-- 渠道组 End -->

            <!-- 服务器 Start  -->

            <!-- 服务器 End -->
        </table>
    </form>
    <input id="commits" type="button" class="gbutton commits" value="确定">
</div>