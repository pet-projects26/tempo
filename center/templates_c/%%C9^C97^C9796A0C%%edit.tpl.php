<?php /* Smarty version 2.6.27, created on 2018-10-22 17:15:42
         compiled from user/edit.tpl */ ?>
<form method="post" id="form_user_edit" action="admin.php?ctrl=user&act=add_submit">
  <table class="itable">
    <tr>
      <td width="80px"></td>
      <td id="add_user_msg"></td>
      <td></td>
    </tr>
    <tr>
      <td>用户名:</td>
      <td><input type="text" value="" name="username"/><font color="red">*</font></td>
      <td></td>
    </tr>
    
    
    <tr>
      <td>用户组:</td>
      <td><select name="groupid">
      <option value="">选择...</option>
      <?php $_from = $this->_tpl_vars['groupNames']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['n']):
?>
      <option value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['n']; ?>
</option>
      <?php endforeach; endif; unset($_from); ?>
      </select><font color="red">*</font>
      </td>
      <td></td>
    </tr>

    <tr>
      <td>姓名:</td>
      <td><input type="text" name="name"><span style="color:red;">*</span></td>
      <td></td>
    </tr>
    <tr>
      <td>部门</td>
      <td><input type="text" name="department"/></td>
      <td></td>
    </tr>
    <tr>
      <td>电话</td>
      <td><input type="text" name="tel"/></td>
      <td></td>
    </tr>
    <tr>
      <td>Email</td>
      <td><input type="text" name="email"/></td>
      <td></td>
    </tr>
    <tr>
      <td>有效期至</td>
      <td><input type="text" name="expiration" class="datepicker" value="<?php echo $this->_tpl_vars['expiration']; ?>
"/></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>
        <input type="submit" class="gbutton" value="提交">
        <input type="reset" class="gbutton" value="重置">
      </td>
      <td></td>
    </tr>
  </table>
  
</form>
<script type="text/javascript">
$( ".datepicker" ).datepicker();
$('#form_user_edit').ajaxForm({
    complete: function(xhr) {
        var result = xhr.responseText;
        $('#form_user_edit reset').trigger('click');
        $.dialog.alert(result);
    }
});
</script>