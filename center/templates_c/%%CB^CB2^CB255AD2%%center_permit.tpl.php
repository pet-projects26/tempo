<?php /* Smarty version 2.6.27, created on 2018-10-22 17:15:56
         compiled from permit/center_permit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'in_array', 'permit/center_permit.tpl', 7, false),)), $this); ?>
<form class="permit_setting_form" method="post" action="admin.php?ctrl=permit&act=group_permit_save">
<table class="itable itable-color">
<thead>
    <tr>
        <th width="100px;">中央后台权限：</th>
        <td><label>
            <input type="checkbox" class="check_all" name="permit_all" value="all" <?php if ($this->_tpl_vars['usergroup']['center_modules_data'] && ((is_array($_tmp='all')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['usergroup']['center_modules_data']) : in_array($_tmp, $this->_tpl_vars['usergroup']['center_modules_data']))): ?>checked<?php endif; ?>/>所有权限</label>
            (<font color="red">如果勾选所有权限，则直接认为具有所有权限</font>)
        </td>
    </tr>
</thead>
<tbody>
    <?php $_from = $this->_tpl_vars['centerPermitList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group'] => $this->_tpl_vars['item']):
?>
    <tr>
        <th><label>
            &nbsp;&nbsp;<input type="checkbox" class="check_row check_cancle" <?php if ($this->_tpl_vars['usergroup']['center_modules_data'] && ((is_array($_tmp='all')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['usergroup']['center_modules_data']) : in_array($_tmp, $this->_tpl_vars['usergroup']['center_modules_data']))): ?>checked<?php endif; ?>/>&nbsp;&nbsp;<?php echo $this->_tpl_vars['group']; ?>

        </label></th>
        <td>
        <?php $_from = $this->_tpl_vars['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['row']):
?>
            <div style="width:160px;min-height:22px;float:left;font-size:12px;"><label>
            <input class="check_cancle" type="checkbox" name="permits[]" value="<?php echo $this->_tpl_vars['row']['pid']; ?>
" <?php if (( $this->_tpl_vars['usergroup']['center_permit_data'] && ((is_array($_tmp=$this->_tpl_vars['row']['pid'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['usergroup']['center_permit_data']) : in_array($_tmp, $this->_tpl_vars['usergroup']['center_permit_data'])) ) || ( $this->_tpl_vars['usergroup']['center_modules_data'] && ((is_array($_tmp='all')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['usergroup']['center_modules_data']) : in_array($_tmp, $this->_tpl_vars['usergroup']['center_modules_data'])) )): ?>checked<?php endif; ?>/>
            <?php echo $this->_tpl_vars['name']; ?>
</label></div>
        <?php endforeach; endif; unset($_from); ?>
        </td>
       </tr>
    <?php endforeach; endif; unset($_from); ?>
    <tr>
        <td><input type="hidden" name="groupid" value="<?php echo $this->_tpl_vars['usergroup']['groupid']; ?>
"/>
        <input type="hidden" name="type" value="center"/>
        </td>
        <td><input type="submit" class="gbutton" value="提交" name="submit" style="float:right;"/></td>
    </tr>
</tbody>
</table>
</form>