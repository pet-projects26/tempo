<?php /* Smarty version 2.6.27, created on 2019-04-15 23:34:35
         compiled from package/edit.tpl */ ?>
<?php if ($this->_tpl_vars['package_id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="package-edit-form">
        <div class="hidden">
            <input type="hidden" id="old_package_id" name="old_package_id" value="<?php echo $this->_tpl_vars['package_id']; ?>
">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 每个包号不能重复<br>
                        2. 包号只能由数字组成，长度最长20位<br/>
                        3. 每个包名不能重复, 需要根据包名获取包号<br/>
                        4. 测试服版本号需要与测试包的版本号一致且需添加<span style="color: #ca1b36">IP白名单</span>，版本号请询问<span
                                style="color: #ca1b36">客户端</span>获取</br>
                        5.cdn资源加载链接必须填写
                    </td>
                    <!-- <td>
                         5. 提审服版本号为IOS包提审，审核通过后请<span style="color: #ca1b36">不填写</span>
                     </td> -->
                </tr>
                <tr>
                    <td style="width: 150px">选择平台</td>
                    <td>
                        <select id="platform" class="chose-platform" disabled="true">
                            <option <?php if ($this->_tpl_vars['platform'] == 0): ?>selected="selected"<?php endif; ?> value="0" >未选择</option>
                            <option <?php if ($this->_tpl_vars['platform'] == 1): ?>selected="selected"<?php endif; ?> value="1">安卓/越狱</option>
                            <option <?php if ($this->_tpl_vars['platform'] == 2): ?>selected="selected"<?php endif; ?> value="2">IOS</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;">选择渠道</td>
                    <td>
                        <select id="channel_id" name="channel_id">
                            <option value="0">未选择</option>
                            <?php $_from = $this->_tpl_vars['channels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['channel']):
?>
                            <option value="<?php echo $this->_tpl_vars['channel']['channel_id']; ?>
" <?php if ($this->_tpl_vars['channel_id'] == $this->_tpl_vars['channel']['channel_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['channel']['name']; ?>
</option>
                            <?php endforeach; endif; unset($_from); ?>
                        </select>
                    </td>
                </tr>
                
                 <tr>
                    <td style="width:150px;">选择渠道组</td>
                    <td>
                        <select id="group_id" name="group_id">
                            <option value="0">未选择</option>
                            <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group']):
?>
                            <option value="<?php echo $this->_tpl_vars['group']['id']; ?>
" <?php if ($this->_tpl_vars['group_id'] == $this->_tpl_vars['group']['id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['group']['name']; ?>
</option>
                            <?php endforeach; endif; unset($_from); ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td style="width:150px;">包号</td>
                    <td><input type="text" id="package_id" name="package_id" value="<?php echo $this->_tpl_vars['package_id']; ?>
"></td>
                </tr>
                <tr>
                    <td style="width:150px;">包名</td>
                    <td><input type="text" id="package_name" name="package_name" value="<?php echo $this->_tpl_vars['package_name']; ?>
"></td>
                </tr>
                <tr>
                    <td style="width:150px;">游戏名</td>
                    <td><input type="text" id="game_name" name="game_name" value="<?php echo $this->_tpl_vars['game_name1']; ?>
"></td>
                </tr>
                <tr>
                <td style="width: 150px">超链接</td>
                <td><input type="text" id="url" name="url" value="<?php echo $this->_tpl_vars['url']; ?>
" /></td>
                </tr>
                <tr>
                    <td style="width: 150px">cdn链接</td>
                    <td><input type="text" id="cdn_url" name="cdn_url" value="<?php echo $this->_tpl_vars['cdn_url']; ?>
"/></td>
                </tr>
                <tr class="test-server-ban">
                    <td>测试服版本号</td>
                    <td>
                        <input type="text" id="test_ser_num" name="test_ser_num" value="<?php echo $this->_tpl_vars['test_server_num']; ?>
"  placeholder="填写请询问客户端">
                        <span style="margin-left: 6px;color: #ca1b36">*请保持与需要测试的包的版本号一致，通过后请清空</span>
                    </td>
                </tr>
                <tr class="review-ban"
                <?php if ($this->_tpl_vars['platform'] == 1): ?>style="display:none"<?php endif; ?> >
                <td style="width: 150px">提审版本号</td>
                <td><input type="text" id="review_num" name="review_num" value="<?php echo $this->_tpl_vars['review_num']; ?>
" /><span style="margin-left: 10px;color: #ca1b36">*填入需要提审的版本号，非提审不填写</span></td>
                </tr>
                <!-- <?php if ($this->_tpl_vars['platform'] == 2): ?>
                <tr class="review-type">
                    <td style="width: 150px">提审类型</td>
                    <td>
                    	 <select name="type" onchange="checkBtn(this.value)"  class = 'type'>
                             <option value="0" <?php if ($this->_tpl_vars['type'] == 0): ?>selected<?php endif; ?> >不提审</option>
                             <option value="1" <?php if ($this->_tpl_vars['type'] == 1): ?>selected<?php endif; ?>>提审1(原来的)</option>
                             <option value="2" <?php if ($this->_tpl_vars['type'] == 2): ?>selected<?php endif; ?>>提审2(新增的)</option>
                             <option value="3" <?php if ($this->_tpl_vars['type'] == 3): ?>selected<?php endif; ?>>提审3(新增的)</option>
                         </select>
                    </td>
                </tr>
                <tr class="review-ban review-ban1">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                            <?php $_from = $this->_tpl_vars['rv_param']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['type_id'] => $this->_tpl_vars['params']):
?>
                                <tr>
                                    <td width="10%"><?php echo $this->_tpl_vars['rv_name'][$this->_tpl_vars['type_id']]; ?>
</td>
                                    <td>
                                        <select name="param[<?php echo $this->_tpl_vars['type_id']; ?>
]">
                                            <option value="">默认</option>
                                            <?php $_from = $this->_tpl_vars['params']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['type_name']):
?>
                                            <option <?php if ($this->_tpl_vars['id'] == $this->_tpl_vars['param'][$this->_tpl_vars['type_id']]): ?>selected<?php endif; ?> value="<?php echo $this->_tpl_vars['id']; ?>
"><?php echo $this->_tpl_vars['type_name']; ?>
</option>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </select>
                                    </td>
                                </tr>
                                <?php endforeach; endif; unset($_from); ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
                <tr class="review-ban2" style="display:none;">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                           
                            <tr>
                                <td width="10%">皮肤</td>
                                <td>
                                    <select name="param2[res]">
                                        <option value="">默认</option>
                                        <?php $_from = $this->_tpl_vars['rv_param2']['res']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['type_name']):
?>
                                        	<option value="<?php echo $this->_tpl_vars['id']; ?>
" <?php if ($this->_tpl_vars['id'] == $this->_tpl_vars['param2']['res']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['type_name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">场景</td>
                                <td>
                                    <select name="param2[sceneid]">
                                        <option value="">默认</option>
                                        <?php $_from = $this->_tpl_vars['rv_param2']['sceneid']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['type_name']):
?>
                                        	<option value="<?php echo $this->_tpl_vars['id']; ?>
" <?php if ($this->_tpl_vars['id'] == $this->_tpl_vars['param2']['sceneid']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['type_name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">客户端(新手村)</td>
                                <td>
                                    <select name="param2[clent_sceneid]">
                                        <option value="">默认</option>
                                        <?php $_from = $this->_tpl_vars['rv_param2']['clent_sceneid']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['type_name']):
?>
                                        	<option value="<?php echo $this->_tpl_vars['id']; ?>
" <?php if ($this->_tpl_vars['id'] == $this->_tpl_vars['param2']['clent_sceneid']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['type_name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="review-ban3" style="display:none;">
                    <td>提审需要替换素材</td>
                    <td>
                        <table>
                            <tbody>
                           
                            <tr>
                                <td width="10%">皮肤</td>
                                <td>
                                    <select name="param3[res]">
                                        <option value="">默认</option>
                                        <?php $_from = $this->_tpl_vars['rv_param3']['res']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['type_name']):
?>
                                        	<option value="<?php echo $this->_tpl_vars['id']; ?>
" <?php if ($this->_tpl_vars['id'] == $this->_tpl_vars['param3']['res']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['type_name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">客户端(新手村)</td>
                                <td>
                                    <select name="param3[clent_sceneid]">
                                        <option value="">默认</option>
                                        <?php $_from = $this->_tpl_vars['rv_param3']['clent_sceneid']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['type_name']):
?>
                                        	<option value="<?php echo $this->_tpl_vars['id']; ?>
" <?php if ($this->_tpl_vars['id'] == $this->_tpl_vars['param3']['clent_sceneid']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['type_name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php endif; ?> -->
                    <td colspan="2"><input type="submit" class="gbutton" value="保存"></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择编辑的包
<?php endif; ?>
<script type="text/javascript">
    /*
      if(<?php echo $this->_tpl_vars['type']; ?>
 == 2){
  		$('.review-ban2').show();
		$('.review-ban1').hide();
		$('.review-ban3').hide();
  }else if(<?php echo $this->_tpl_vars['type']; ?>
 == 3){
  		$('.review-ban2').hide();
		$('.review-ban1').hide();
		$('.review-ban3').show();
  
  }else{
  		$('.review-ban1').show();
		$('.review-ban2').hide();
		$('.review-ban3').hide();
  }
  */
  
	
    //选择安卓
    $(document).ready(function(){
        $(".chose-platform").change(function(){
            var type = $(this).children("option:selected").val();
            var ban = $(".review-ban");
            var test_ban = $('.test-server-ban');
            var type2  =  $(".type").find("option:selected").val();
            if(type == 2){
                $('.review-type').show();
                	if(type2 == 2){
                		$('.review-ban2').show();
                		$('.review-ban3').hide();
                	}else if(type2 == 3){
                		$('.review-ban3').show();
                		$('.review-ban2').hide();
                	}else{
                		ban.show();
                	}
                test_ban.hide();
            }else{
            	$('.review-type').hide();
                $('.review-ban2').hide();
                $('.review-ban3').hide();
                ban.children('td').children('input').val('');
                ban.hide();
                test_ban.show();
            }
        })
    })
    $(function(){
        var error = $('#error');
        $('#package-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                var channel_id =$("#channel_id").find("option:selected").text();
                var group_id =$("#group_id").find("option:selected").text();
                var package_id = $("#package_id").val();
                var review_num = $("#review_num").val();
                var platform = $("#platform").find("option:selected").val();
                var test_ser_num = $("#test_ser_num").val();
                var str = '确定选择：\n'+'渠道：'+channel_id+'\n'+'渠道组：'+group_id+'\n'
                        +'包号：'+package_id+'\n'+'测试服版本号：'+test_ser_num;


                if (platform == 2) {
                     str += '提审版本号：'+review_num;
                }
			
				if(confirm(str)){
					$.ajax({
						url: 'admin.php?ctrl=package&act=edit_action',
						type: 'POST',
						dataType: 'JSON',
						data: $(this).serialize()
					}).done(function(data){
						$.dialog.tips(data.msg);
						if(data.code){
							$tabs.tabs('select' , 0);
						}
					})
				}
			 return false;	
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        var old_channel_id = '<?php echo $this->_tpl_vars['channel_id']; ?>
';
        var old_package_id = $('#old_package_id').val(); 
        var channel_id = $('#channel_id').val();
        var package_id = $('#package_id').val();
		
		var old_group_id = '<?php echo $this->_tpl_vars['group_id']; ?>
';
		var group_id = $('#group_id').val();
		
		
		
        /*if(old_channel_id == channel_id && old_package_id == package_id && old_group_id == group_id){
            $.dialog.tips('没有任何改变');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }*/
        //平台
        var platform  = $("#platform").val();
        if(platform == 0){
            $.dialog.tips("请选择平台");
            error.val(1);
            return false;
        }else if(platform == 2){
            var review = $("#review_num").val();
            if(!review){
                //$.dialog.tips("选择ISO平台,请注意提审版本号！");
                //error.val(0);
            }
        }
        //渠道
        if(channel_id == '0'){
            $.dialog.tips('未选择渠道');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //包号
        if(package_id == ''){
            $.dialog.tips('未填写包号');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
        //包名
        var package_name = $('#package_name').val();
        if (package_name == '') {
            $.dialog.tips('未填写包名');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
        //cdn链接
        var cdn_url = $('#cdn_url').val();
        if (cdn_url == '') {
            $.dialog.tips('未填写cdn资源加载链接');
            error.val(1);
            return false;
        }
        else {
            error.val(0);
        }
    }
    
function checkBtn(type){
	
	if(type == 2){
		$('.review-ban2').show();
		$('.review-ban1').hide();
		$('.review-ban3').hide();
	}else if(type == 3){
		$('.review-ban3').show();
		$('.review-ban1').hide();
		$('.review-ban2').hide();
	}else{
		$('.review-ban1').show();
		$('.review-ban2').hide();
		$('.review-ban3').hide();
	}
	
}
</script>