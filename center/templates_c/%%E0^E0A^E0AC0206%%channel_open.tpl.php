<?php /* Smarty version 2.6.27, created on 2019-02-18 10:07:15
         compiled from server/channel_open.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'implode', 'server/channel_open.tpl', 46, false),)), $this); ?>
<?php if ($this->_tpl_vars['server_id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-open-form">
        <div class="hidden">
            <input type="hidden" name="server_id" value="<?php echo $this->_tpl_vars['server_id']; ?>
">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">选择渠道组</td>
                    <td>
                        <?php $_from = $this->_tpl_vars['group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                        <label class="label <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['id']): ?> p-checked<?php endif; ?> >" for='group-<?php echo $this->_tpl_vars['item']['id']; ?>
'>
                            <input class="ver" type="radio" name="id" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['id']): ?> checked<?php endif; ?> id="group-<?php echo $this->_tpl_vars['item']['id']; ?>
">
                            <font class="ver" ><?php echo $this->_tpl_vars['item']['name']; ?>
</font>
                        </label>
                        <?php endforeach; endif; unset($_from); ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">操作</td>
                    <td><input type="submit" class="gbutton" value="保存"></td>
                </tr>
                <tr>
                    <td colspan="2" style="color:#228b22;font-weight:bold;text-align:center;">渠道组信息</td>
                </tr>
                <tr>
                    <td colspan="2">
                        说明：<br>
                        1. 红色部分为当前服务器开通的渠道组
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;color:#228b22;">渠道组名称</td>
                    <td style="font-weight:bold;color:#228b22;">渠道包含的包</td>
                </tr>
               
                <?php $_from = $this->_tpl_vars['package']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupId'] => $this->_tpl_vars['item']):
?>
                <tr <?php if (( $this->_tpl_vars['id'] == $this->_tpl_vars['groupId'] )): ?> class="red" <?php endif; ?> >
                    <td><?php if (( isset ( $this->_tpl_vars['group'][$this->_tpl_vars['groupId']]['name'] ) )): ?> <?php echo $this->_tpl_vars['group'][$this->_tpl_vars['groupId']]['name']; ?>
 <?php else: ?> <?php echo $this->_tpl_vars['groupId']; ?>
 <?php endif; ?></td>
                    <td>
                        <?php $_from = $this->_tpl_vars['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['channelName'] => $this->_tpl_vars['packages']):
?>
                            <div>
                                <span style="width: 150px; float:left;"><?php echo $this->_tpl_vars['channelName']; ?>
：</span>
                                <span><?php echo ((is_array($_tmp=',')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['packages']) : implode($_tmp, $this->_tpl_vars['packages'])); ?>
</span>
                            </div>
                        <?php endforeach; endif; unset($_from); ?>
                    </td>
                </tr>
                <?php endforeach; endif; unset($_from); ?>

            </tbody>
        </table>
    </form>
</div>
<?php else: ?>
请选择开通渠道的服务器
<?php endif; ?>
<script>
    $(function(){
        var error = $('#error');
        $('#channel-open-form').submit(function(event){
            var v = $('input[name="id"]:checked').val();
            if(typeof v == 'undefined') {
                $.dialog.tips('请先择渠道组');
                return false;
            }

            $.ajax({
                url: 'admin.php?ctrl=channelgroup&act=open_action',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize()
            }).done(function(data){
                $.dialog.tips(data.msg);
                if(data.code){
                    $tabs.tabs('load' , 3);
                }
            });
            return false;
        });
    });


    function form_check(){
        var error = $('#error');
        //渠道组
        var id = $('#id').val();
        if(id == 0){
            $.dialog.tips('未选择渠道组');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }

    //绑定点击渠道组
    $('input[name="id"]').unbind('click').bind('click', function() {
        $('.label').removeClass('p-checked');
        $(this).parent('.label').addClass('p-checked');
    });
</script>