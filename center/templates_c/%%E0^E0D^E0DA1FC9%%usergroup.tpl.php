<?php /* Smarty version 2.6.27, created on 2018-10-18 16:27:32
         compiled from usergroup/usergroup.tpl */ ?>
<table id="usergroup_table" style="width:100%;">
  <tr>
    <td width="40%" valign="top">
        <input type="button" class="gbutton" onclick="add_usergroup()" value="添加">
        <table id="usergroup_list" class="itable" style="text-align:left;margin-top:15px;">
            <tr>
                <th width="30%">名称</th>
                <th width="30%">描述</th>
                <th width="40%">操作</th>
            </tr>
            <?php $_from = $this->_tpl_vars['groupList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
            <tr>
                <td><?php echo $this->_tpl_vars['item']['name']; ?>
</td>
                <td><?php echo $this->_tpl_vars['item']['description']; ?>
</td>
                <td>
                    <input type="button" class="gbutton" onclick="edit_usergroup(<?php echo $this->_tpl_vars['item']['groupid']; ?>
)" value="编辑">
                    <input type="button" class="gbutton" onclick="delete_usergroup(<?php echo $this->_tpl_vars['item']['groupid']; ?>
)" value="删除">
                </td>
            </tr>
            <?php endforeach; endif; unset($_from); ?>
        </table>
    </td>
    <td id="usergroup_edit_content"></td>
  </tr>
</table>
<select style="display:none;" id="usergroup_select">
    <option value="">请选择..</option>
    <?php $_from = $this->_tpl_vars['groupList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
        <option value="<?php echo $this->_tpl_vars['item']['groupid']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
    <?php endforeach; endif; unset($_from); ?>
</select>
<script type="text/javascript">
$('#usergroup_edit_content').load('admin.php?ctrl=usergroup&act=edit');
$('#usergroup_list tr:even').addClass('even');
$('#usergroup_list tr:odd').addClass('odd');
$("#usergroup_list tr").hover(
    function () {
       $(this).addClass("hover");
    },
    function () {
       $(this).removeClass("hover");
    }
);
function edit_usergroup(groupid){
	$('#usergroup_edit_content').load('admin.php?ctrl=usergroup&act=edit&groupid='+groupid);
}

function delete_usergroup(groupid){
	if(confirm('确定删除用户组')){
        $.ajax({
            type: "POST",
            url: "admin.php?ctrl=usergroup&act=delete",
            data: 'groupid='+groupid,
            timeout: 20000,
	        error: function(){$.dialog.alert('超时');},
            success: function(result){
                if(result == 'success'){
                	$usergroup_tabs.tabs('load', 0);
                }else{
                    $.dialog.alert('删除失败');
                }
              
            }
        });
    }
}
function add_usergroup(){
    $('#usergroup_edit_content').load('admin.php?ctrl=usergroup&act=edit');
}
</script>