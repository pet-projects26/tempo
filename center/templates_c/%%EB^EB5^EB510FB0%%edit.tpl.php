<?php /* Smarty version 2.6.27, created on 2019-04-15 21:50:33
         compiled from server/edit.tpl */ ?>
<?php if ($this->_tpl_vars['server']['server_id']): ?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="server-edit-form" style="clear:both;width:100%;">
        <div class="hidden">
            <input type="hidden" value="1" id="error">
            <input type="hidden" name="old_server_id" id="old_server_id" value="<?php echo $this->_tpl_vars['server']['server_id']; ?>
">
        </div>
        <div id="tables">
            <div>
                <table class="itable itable-color">
                    <tbody>
                        <tr>
                            <td>
                                说明：<br>
                                1. 红色标题的选项为必填选项<br>
                                2. 认证密钥 是访问该服务器时的验证密钥<br>
                                3. 未开服提醒指 当玩家在服务器列表中选择未开服的服务器时返回提醒的文本<br>
                                4. 服务器标识只能由数字，小写字母，大写字母，下划线（_）和横杠（-）组成
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="float:left;width:50%;">
                <table class="itable itable-color serverbase">
                    <tbody>
                        <tr><td colspan="2" style="color:#228b22;font-weight:bold;">基础信息</td></tr>
                        <tr><td style="color:#ff0000;">编号</td><td><input type="text" name="num" id="num" placeholder="<?php echo $this->_tpl_vars['server']['num']; ?>
" readonly></td></tr>
                        <tr>
                            <td style="color:#ff0000;">标识</td>
                            <td><input type="text" name="server_id" id="server_id" value="<?php echo $this->_tpl_vars['server']['server_id']; ?>
"
                                       readonly></td>
                        </tr>
                        <tr>
                            <td style="color:#ff0000;">名称</td>
                            <td><input type="text" name="name" id="name" value="<?php echo $this->_tpl_vars['server']['name']; ?>
" readonly></td>
                        </tr>
                        <tr>
                            <td>分区</td>
                            <td>
                                <select name="zone" id="zone">
                                    <option value="0">未选择</option>
                                    <?php $_from = $this->_tpl_vars['zone']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                    <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['server']['zone'] == $this->_tpl_vars['item']['id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                                    <?php endforeach; endif; unset($_from); ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>类型</td>
                            <td>
                                <select name="type" id="type" disabled="disabled">
                                    <option value="0">未选择</option>
                                    <option value="1" <?php if ($this->_tpl_vars['server']['type'] == 1): ?>selected="selected"<?php endif; ?>>正常服</option>
                                    <option value="3" <?php if ($this->_tpl_vars['server']['type'] == 3): ?>selected="selected"<?php endif; ?>>测试服</option>
                                    <option value="2" <?php if ($this->_tpl_vars['server']['type'] == 2): ?>selected="selected"<?php endif; ?>>合服</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>状态</td>
                            <td>
                                <select name="status" id="status">
                                    <?php $_from = $this->_tpl_vars['status']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['server']['status'] == $this->_tpl_vars['key']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['item']; ?>
</option>
                                    <?php endforeach; endif; unset($_from); ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>是否显示</td>
                            <td>
                                <select name="display" id="display">
                                    <option value="0" <?php if ($this->_tpl_vars['server']['display'] == 0): ?>selected="selected"<?php endif; ?>>不显示</option>
                                    <option value="1" <?php if ($this->_tpl_vars['server']['display'] == 1): ?>selected="selected"<?php endif; ?>>显示</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>提审</td>
                            <td>
                                <input type="radio" name="review" id="no_rv" class="ver" <?php if ($this->_tpl_vars['server']['review'] == 0): ?>checked="checked"<?php endif; ?> value="0" /><label class="label">否</label>
                                <input type="radio" name="review" id="yes_rv" class="ver" <?php if ($this->_tpl_vars['server']['review'] == 1): ?>checked="checked"<?php endif; ?> value="1" /><label class="label">是</label>
                            </td>
                        </tr>
                        <!-- <tr><td>设置为第几服</td><td><input type="text" name="sort" id="sort" value="<?php echo $this->_tpl_vars['server']['sort']; ?>
"></td></tr> -->
                        <tr>
                            <td>开服时间</td>
                            <td>
                                <input type="text" name="open_time" id="open_time" class="datepicker" value="<?php if ($this->_tpl_vars['server']['open_time'] == 0): ?>0<?php else: ?><?php echo $this->_tpl_vars['server']['open_time']; ?>
<?php endif; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>自动显示服务器时间</td>
                            <td>
                                <input type="text" name="display_time" id="display_time" class="datepicker" value="<?php if ($this->_tpl_vars['server']['display_time'] == 0): ?>0<?php else: ?><?php echo $this->_tpl_vars['server']['display_time']; ?>
<?php endif; ?>">
                            </td>
                        </tr>
                        <tr><td>关服时间</td><td><input type="text" name="close_time" id="close_time" class="datepicker" value="<?php echo $this->_tpl_vars['server']['close_time']; ?>
"></td></tr>
                        <tr><td>ip</td><td><input type="text" name="ip" id="ip" value="<?php echo $this->_tpl_vars['server_config']['ip']; ?>
"></td></tr>
                        <tr><td>最大承载量</td><td><input type="text" name="max_online" id="max_online" value="<?php echo $this->_tpl_vars['server']['max_online']; ?>
"></td></tr>
                        <tr><td>域名</td><td><input type="text" name="domain" id="domain" value="<?php echo $this->_tpl_vars['server_config']['domain']; ?>
"></td></tr>
                        <tr><td>api地址</td><td><input type="text" name="api_url" id="api_url" value="<?php echo $this->_tpl_vars['server_config']['api_url']; ?>
"></td></tr>
                        <tr><td>configs路径</td><td><input type="text" name="configs_path" id="configs_path" value="<?php echo $this->_tpl_vars['server_config']['configs_path']; ?>
"></td></tr>
                        <tr><td>未开服提醒</td><td><input type="text" name="tips" id="tips" value="<?php echo $this->_tpl_vars['server']['tips']; ?>
"></td></tr>
                        <!--<tr><td style="visibility:hidden;">empty</td><td></td></tr>-->
                    </tbody>
                </table>
            </div>
            <div style="float:left;width:50%;">
                <table class="itable itable-color database">
                    <tbody>
                        <tr><td colspan="2" style="color:#228b22;font-weight:bold;">连接信息</td></tr>
                        <tr style="display: none;"><td>mongodb服ip</td><td><input type="hidden" name="mongo_host" id="mongo_host" value="<?php echo $this->_tpl_vars['server_config']['mongo_host']; ?>
"></td></tr>
                        <tr style="display: none;"><td>mongodb端口</td><td><input type="hidden" name="mongo_port" id="mongo_port" value="<?php echo $this->_tpl_vars['server_config']['mongo_port']; ?>
"></td></tr>
                        <tr style="display: none;"><td>mongodb用户名</td><td><input type="hidden" name="mongo_user" id="mongo_user" value="<?php echo $this->_tpl_vars['server_config']['mongo_user']; ?>
"></td></tr>
                        <tr style="display: none;"><td>mongodb密码</td><td><input type="hidden" name="mongo_passwd" id="mongo_passwd" value="<?php echo $this->_tpl_vars['server_config']['mongo_passwd']; ?>
"></td></tr>
                        <tr style="display: none;"><td>mongodb默认连接数据库</td><td><input type="hidden" name="mongo_db" id="mongo_db" value="<?php echo $this->_tpl_vars['server_config']['mongo_db']; ?>
"></td></tr>

                        <tr><td>redis服ip</td><td><input type="text" name="redis_host" id="redis_host"  value="<?php echo $this->_tpl_vars['server_config']['redis_host']; ?>
"></td></tr>
                        <tr><td>redis端口</td><td><input type="text" name="redis_port" id="redis_port" value="<?php echo $this->_tpl_vars['server_config']['redis_port']; ?>
"></td></tr>
                        <tr><td>redis密码</td><td><input type="text" name="redis_passwd" id="redis_passwd"  value="<?php echo $this->_tpl_vars['server_config']['redis_passwd']; ?>
"></td></tr>
                        <tr><td>redisKey前缀</td><td><input type="text" name="redis_prefix" id="redis_prefix"  value="<?php echo $this->_tpl_vars['server_config']['redis_prefix']; ?>
"></td></tr>
                        <tr><td>redis默认连接数据库</td><td><input type="text" name="redis_db" id="redis_db"  value="<?php echo $this->_tpl_vars['server_config']['redis_db']; ?>
"></td></tr>

                        <tr><td>websocket服ip</td><td><input type="text" name="websocket_host" id="websocket_host"  value="<?php echo $this->_tpl_vars['server_config']['websocket_host']; ?>
"></td></tr>
                        <tr><td>websocket端口</td><td><input type="text" name="websocket_port" id="websocket_port"  value="<?php echo $this->_tpl_vars['server_config']['websocket_port']; ?>
"></td></tr>

                        <tr><td>mysql服ip</td><td><input type="text" name="mysql_host" id="mysql_host" value="<?php echo $this->_tpl_vars['server_config']['mysql_host']; ?>
"></td></tr>
                        <tr><td>mysql端口</td><td><input type="text" name="mysql_port" id="mysql_port" value="<?php echo $this->_tpl_vars['server_config']['mysql_port']; ?>
"></td></tr>
                        <tr><td>mysql用户名</td><td><input type="text" name="mysql_user" id="mysql_user" value="<?php echo $this->_tpl_vars['server_config']['mysql_user']; ?>
"></td></tr>
                        <tr><td>mysql密码</td><td><input type="text" name="mysql_passwd" id="mysql_passwd" value="<?php echo $this->_tpl_vars['server_config']['mysql_passwd']; ?>
"></td></tr>
                        <tr><td>mysql默认连接数据库</td><td><input type="text" name="mysql_db" id="mysql_db" value="<?php echo $this->_tpl_vars['server_config']['mysql_db']; ?>
"></td></tr>
                        <tr><td>mysql数据表前缀</td><td><input type="text" name="mysql_prefix" id="mysql_prefix" value="<?php echo $this->_tpl_vars['server_config']['mysql_prefix']; ?>
"></td></tr>
                        <tr><td>认证密钥</td><td><input type="text" name="mdkey" id="mdkey" value="<?php echo $this->_tpl_vars['server_config']['mdkey']; ?>
"></td></tr>
                        <tr><td>gm端口</td><td><input type="text" name="gm_port" id="gm_port" value="<?php echo $this->_tpl_vars['server_config']['gm_port']; ?>
"></td></tr>
                        <tr style="color:#ff0000;"><td>登录服ip</td><td><input type="text" name="login_host" id="login_host" value="<?php echo $this->_tpl_vars['server_config']['login_host']; ?>
"></td></tr>
                        <tr style="color:#ff0000;"><td>登录服端口</td><td><input type="text" name="login_port" id="login_port" value="<?php echo $this->_tpl_vars['server_config']['login_port']; ?>
"></td></tr>
                        <tr><td>提审服版本号</td><td><input type="text" name="channel_rv" value="<?php echo $this->_tpl_vars['server_config']['channel_rv']; ?>
"></td></tr>
                    </tbody>
                </table>
            </div>
            <div>
                <table class="itable itable-color">
                    <tbody>
                        <tr>
                            <td><input type="submit" value="保存" class="gbutton"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
<?php else: ?>
请选择编辑的服务器
<?php endif; ?>
<script>
    $(function(){
        $('input[type=text]').css('width' , '150px');
        $('input[name=api_url]').css('width' , '400px');
        $('input[name=configs_path]').css('width' , '400px');
        $('input[name=domain]').css('width' , '400px');
        $('input[name=tips]').css('width' , '400px');
        $('#server-edit-form').css('height' , $('#tables').css('height'));
        var timepickerlang = { timeText:'时间' , hourText:'小时' , minuteText:'分钟' , currentText:'现在' , closeText:'确定' };
        $('.datepicker').datetimepicker(timepickerlang);

        var error = $('#error');
        $('#server-edit-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    url: 'admin.php?ctrl=server&act=edit_action',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                }).done(function(data){
                    $.dialog.tips(data.msg);
                    if(data.code){
                        $tabs.tabs('select' , 0);
                    }
                });
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //服务器标识
        var server_id = $('#server_id').val();
        if(server_id == ''){
            $.dialog.tips('未填写服务器标识');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //服务器名称
        var name = $('#name').val();
        if(name == ''){
            $.dialog.tips('未填写服务器名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>