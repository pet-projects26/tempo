<?php /* Smarty version 2.6.27, created on 2019-06-22 15:48:46
         compiled from feedback/reply.tpl */ ?>
<div style="padding:0 12px; padding-bottom:10px; margin:6px 0; border: solid 1px #ABABAB;border-radius: 5px;background: #FAFAFA;">
  <table border="0" id="baseinfo">
    <thead>
      <th>回复内容</th>
      <th>回复时间</th>
    </thead>

    <tbody>
      <?php $_from = $this->_tpl_vars['reply']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['row']):
?>
      <tr class="even">
        <td  style="padding: 5px;"><?php echo $this->_tpl_vars['row']['reply']; ?>
</td>
        <td  style="padding: 5px;"><?php echo $this->_tpl_vars['row']['create_time']; ?>
</td>
      </tr>
      <?php endforeach; endif; unset($_from); ?>
    </tbody>
  </table>
</div>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="gift-add-form">
        <div class="hidden">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
              <tr>
                  <td style="width:150px;">回复</td>
                  <td>
                      <textarea name="tips" id="tips" style="width:600px;height:70px;margin:0;"></textarea>
                  </td>
              </tr>
              <tr>
                  <td style="width:150px;"></td>
                  <td>
                      <input type="button" value="提交" class="gbutton" id="additem" >
                  </td>
              </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('#additem').click(function(){

            $.post(
                'admin.php?ctrl=feedback&act=post_reply',
                {
                  'text' : $('textarea').val(),
                  'fid' : <?php echo $this->_tpl_vars['fid']; ?>
,
                  'server_id' : '<?php echo $this->_tpl_vars['server_id']; ?>
',
                  'type' : '<?php echo $this->_tpl_vars['type']; ?>
',
                  'role_id': '<?php echo $this->_tpl_vars['role_id']; ?>
',
                'uuid'
        :
            '<?php echo $this->_tpl_vars['uuid']; ?>
'
                },
                function(data) {
                  data = eval('('+ data +')');
                  var code = data['code'];
                  var time = data['time'];


                  if (code == 0) {
                    var tr = "<tr>" 
                           + '<td>' + $('textarea').val() + '</td>'
                           + '<td>' + time + '</td>'
                           + '</tr>';
                           
                    $('#baseinfo tbody').append(tr);
                    $.dialog.tips('添加成功');
                  } else {
                    $.dialog.tips('添加失败');
                  }

                }
            ); 
        });

    });
</script>