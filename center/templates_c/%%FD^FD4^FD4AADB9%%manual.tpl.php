<?php /* Smarty version 2.6.27, created on 2018-11-08 11:28:22
         compiled from chargetrue/manual.tpl */ ?>
<style>
.type-list {
    overflow: hidden;
    margin-bottom: 30px;
}
.type-list ul li {
    float: left;
    width: 125px;
    margin: 3px;
    height: 30px;
    border: 1px solid #ccc;
    text-align: center;
    line-height: 30px;
    cursor: pointer;
    border-radius: 2px;
}
#checkboxChannelList .select {
    background-color: #dcd8d8;
}
#checkboxChannelList ul li {
    display: inline-block;
    width: 150px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    border: 1px solid #cccccc;
    cursor: pointer;
    margin: 0 !important;
    padding: 0 !important;
    overflow: hidden;
}
#checkboxServerList .select {
    background-color: #dcd8d8;
}
#checkboxServerList ul li {
    display: inline-block;
    width: 150px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    border: 1px solid #cccccc;
    cursor: pointer;
    margin: 0 !important;
    padding: 0 !important;
    overflow: hidden;
}
.p-input {
        vertical-align:middle;
    }

.p-text {
    vertical-align:middle;
}

.p-span {
    padding: 5px 10px;
    margin: 0px 3px;
}

.w-150 {
    width: 150px;
}

fieldset {
    padding: 5px;
    border: 2px solid #cc0;
    margin: 10px 0px;
}

.p-checked {
    background: #ff0;
    border-radius: 3px;
}
</style>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="manual-order-form">
        <table class="itable itable-color">
            <tbody>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => '../plugin/channelGroup_server_radios.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <tr>
                <td style="width:150px;">填写角色名称</td>
                <td>
                    <input type="text" value="" name="role_name" id="role_name" style="width:140px;">
                </td>
            </tr>
            <tr>
                <td style="width:150px;">选择充值包</td>
                <td>
                    <select name="item" id="item" style="width:140px;">
                        <option value="0">未选择</option>
                        <?php $_from = $this->_tpl_vars['charge']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
                            <option value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="gbutton" value="添加" id="charge">
                    <input type="hidden" id="error" value="1">
                    <input type="hidden" id="checkboxServer" name="checkboxServer" value="" />
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('table.itable tr td,table input,table select').css('margin-top','3px').css('margin-bottom','3px');
        var error = $('#error');
        $('#manual-order-form').submit(function(event){
            form_check();
            if(error.val() == '1'){
                return false;
            }
            else{
                $.ajax({
                    type:'post',
                    url:'admin.php?ctrl=chargetrue&act=manual_action',
                    data:$(this).serialize(),
                    dataType:'json',
                    beforeSend:function(){
                        $("#charge").attr({disabled:"disabled"});
                        
                    },
                    success:function(data){
                        $.dialog.tips(data.msg);
                    },
                    complete:function(){
                        setTimeout(function(){ 
                            $("#charge").removeAttr('disabled');
                        },1000); 
                    }
                })
            }
            return false;
        });
    });
    function form_check(){
        var error = $('#error');
        //服务器
        
        var server  = $("input[name='server']").val();
      
        if(server == ''){
            $.dialog.tips('服务器不能为空');
            error.val(1);
            return false; 
        }else{
            error.val(0);
        }
        //角色ID
        var role = $('#role_name');
        if(role.val() == ''){
            $.dialog.tips('未填写角色名称');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
        //充值包
        var item = $('#item');
        if(item.val() == '0'){
            $.dialog.tips('未选择充值包');
            error.val(1);
            return false;
        }
        else{
            error.val(0);
        }
    }
</script>