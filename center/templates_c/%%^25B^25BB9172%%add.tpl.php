<?php /* Smarty version 2.6.27, created on 2018-10-22 18:11:27
         compiled from plan/add.tpl */ ?>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <form action="" class="fm" id="channel-open-form">
        <div class="hidden">
            <input type="hidden" name="server_id" value="<?php echo $this->_tpl_vars['server_id']; ?>
">
            <input type="hidden" id="error" value="1">
        </div>
        <table class="itable itable-color">
            <tbody>
                <tr>
                    <td style="width:150px;">选择渠道组</td>
                    <td>
                        <?php $_from = $this->_tpl_vars['group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                        <label class="label <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['id']): ?> p-checked<?php endif; ?> >" for='group-<?php echo $this->_tpl_vars['item']['id']; ?>
'>
                            <input class="ver" type="radio" name="group" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['id']): ?> checked<?php endif; ?> id="group-<?php echo $this->_tpl_vars['item']['id']; ?>
">
                            <font class="ver" ><?php echo $this->_tpl_vars['item']['name']; ?>
</font>
                        </label>
                        <?php endforeach; endif; unset($_from); ?>
                    </td>
                </tr>

                <tr>
                    <td style="width:150px;">服号</td>
                    <td><input type="number" name="num" id="num"></td>
                </tr>

                <tr>
                    <td style="width:150px;">标识</td>
                    <td><input type="text" name="server_id" id="server_id"></td>
                </tr>

                <tr>
                    <td style="width:150px;">名称</td>
                    <td><input type="text" name="name" id="name"></td>
                </tr>

                <tr>
                    <td style="width:150px;">分区</td>
                    <td>
                        <select name="zone" id="zone">
                            <option value="0">未选择</option>
                            <?php $_from = $this->_tpl_vars['zone']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                 <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                            <?php endforeach; endif; unset($_from); ?>
                        </select>
                    </td>
                </tr>
                <!--提审事宜-->
                <tr>
                    <td>提审</td>
                    <td>
                        <input class="ver" id="no_rv" type="radio" checked="checked" name="review" value="0" /><label class="label" for="no_rv">否</label>
                        <input class="ver" id="yes_rv" type="radio" name="review" value="1" /><label class="label" for="yes_rv">是</label>
                    </td>
                </tr>
                <tr>
                    <td style="width:150px;">开服时间</td>
                    <td><input value="0" type="text" name="open_time" id="open_time"></td>
                </tr>
                <tr>
                    <td style="width: 150px">自动显示时间</td>
                    <td><input value="0" type="text" name="display_time" id="display_time"></td>
                </tr>
                
                 <tr>
                    <td style="width:150px;">未开服提醒</td>
                    <td><input type="text" name="tips" id="tips" style="width:300px;"></td>
                </tr>

                <tr>
                    <td colspan="2"><input type="submit" value="添加" class="gbutton"></td>
                </tr>
                
            </tbody>
        </table>
    </form>
</div>

<script>
    $(function(){

        var timepickerlang = { timeText:'时间' , hourText:'小时' , minuteText:'分钟' , currentText:'现在' , closeText:'确定' };
        $('#open_time').datetimepicker(timepickerlang);
        $('#display_time').datetimepicker(timepickerlang);
        var error = $('#error');
        $('#channel-open-form').submit(function(event){
            var v = $('input[name="group"]:checked').val();
            if(typeof v == 'undefined') {
                $.dialog.tips('请选择渠道组');
                return false;
            }

            var num = $('#num').val();
            if(num == ''){
                $.dialog.tips('请输入服号');
                return false; 
            }
            var server_id = $('#server_id').val();
            if(server_id == ''){
                $.dialog.tips('请输入标识');
                return false; 
            }

            var name = $('#name').val();
            if(name == ''){
                $.dialog.tips('请输入名称');
                return false; 
            }

            var zone = $('#zone').val();
            if(zone == 0){
                $.dialog.tips('请选择分区');
                return false; 
            }

            var open_time = $('#open_time').val();
            if(open_time == ''){
                $.dialog.tips('请输入开服时间');
                return false; 
            }
            var review = $("input[name='review'] :checked").val();
            $.ajax({
                url: 'admin.php?ctrl=plan&act=add_action',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize()
            }).done(function(data){
                //console.log(data);return false;
                $.dialog.tips(data.msg);
                if(data.code == 200){
                    $tabs.tabs('load' , 0);
                }
            });
            return false;
        });
    });


    //绑定点击渠道组
    $('input[name="group"]').unbind('click').bind('click', function() {
        $('.label').removeClass('p-checked');
        $(this).parent('.label').addClass('p-checked');
    });
</script>