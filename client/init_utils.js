window.onerror = function (event, source, fileno, columnNumber, error) {
    alert(error.toString() + "\n" + source + "\n" + fileno + ":" + columnNumber + "\n" + error ? error.stack : "");
};

window.onload = function () {
    // onServerInit(document.getElementById("serverSelect"));
    if (localStorage.user) {
        document.getElementById("user").value = localStorage.user;
    }
};

// 发起支付，实际是向后台发送支付信息，由后台生成并返回订单，然后再向平台发起支付
function ask_pay(server, roleId, roleName, roleLevel, roleCareer, productId, price) {
}

// 后台创建角色
function player_create(role_id, role_name, role_level) {
}

// 后台登录
function player_login(role_id, role_name, role_level) {
}

// 后台角色升级
function player_level_up(role_id, role_name, role_level) {
}

// 埋点
function record_step(step, role_id) {
}

/*
var packageList = {

};

function onPackgeInit(packageSelect) {
    let options = packageList;
    for (let i = 0; i < options.length; ++i) {
        let option = options[i];
        let element = document.createElement("option");
        element.value = "";
        element.text = option.name;
        serverSelect.add(element);
    }

    if (localStorage.selectedServer) {
        serverSelect.selectedIndex = parseInt(localStorage.selectedServer);
    } else {
        serverSelect.selectedIndex = 0;
    }

    onServerChange(serverSelect);
}

function onServerChange(serverSelect) {
    let options = server_list;
    localStorage.selectedServer = serverSelect.selectedIndex;
    let server = options[serverSelect.selectedIndex];
    if(!server){
        server = options[0];
        alert("server错误............" + serverSelect.selectedIndex);
    }
    document.getElementById("ip").value = server.server_addr;
    document.getElementById("port").value = server.server_port;
    document.getElementById("entryId").value = server.channel_num;
}
*/
var backstage = "https://game.9day-game.com/jthy/api/sdk/game.php/";
function onEnter() {

    var pack = document.getElementById("package").value;

    if (pack.length == 0) {
        window.alert("包不能为空");
        return;
    }

    var user = document.getElementById("user").value;
    if (user.length == 0) {
        window.alert("用户名不能为空");
        return;
    }

    var node = document.getElementById("login");
    document.body.removeChild(node);
    localStorage.user = user;

    var qrcode = document.getElementById("qrcode");
    document.body.removeChild(qrcode);

    var str = window.btoa("game/getTestServer/" + pack + "/" + user);
    //ajax到后台获取服务器数据
    sendXHR(backstage + str, null, function (data) {
        //console.log(data); return
        if (!data) return;
        if (data.errCode === 200) {
            var JiaData = {};
            var data = data.data;
            var server = data.s;
            JiaData.account = data.user.openId;
            JiaData.uid = data.user.openId;
            JiaData.channel = data.channel;
            JiaData.package = data.package;
            JiaData.mac = '';
            JiaData.cdn = data.cdn;
            JiaData.rvtype = 0;
            JiaData.newregister = data.newregister;

            var serverSelect = {
                'server_id': server.server_id,
                'name': server.name,
                'server_num': parseInt(server.server_num),
                'channel_num': parseInt(server.channel_num),
                'server_addr': server.server_addr,
                'server_port': parseInt(server.server_port),
                'status': parseInt(server.status),
                'tick': parseInt(server.tick),
                'sign': server.sign,
                'package': parseInt(data.package)
            };

            JiaData.selected_server = JSON.stringify(serverSelect);

            jumpLoading(JiaData);
        } else {
            alert(data.msg);
        }
    });
}

function getPackages() {
    var str = window.btoa("test/getPackages");

    sendXHR(backstage + str, null, function (data) {

    });
}

// 通用发送请求方法
function sendXHR(url, data, callBack) {
    //alert(url);
    var xhr = new XMLHttpRequest();
    xhr.responseType = "json";
    xhr.onload = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 301)) {
            if (callBack) callBack(xhr.response);
        }
    };
    console.log(url);
    xhr.open("GET", url);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    //xhr.withCredentials = true;
    xhr.send(data);
}

//跳转到加载页
function jumpLoading(data) {
    var arr = [];
    for (var x in data) {
        arr.push(x + "=" + data[x]);
    }
    window.location.href = "loading.php?" + arr.join("&");
}
