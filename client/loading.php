<?php
/*
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers:Authorization');
header("Access-Control-Allow-Methods: GET, POST, DELETE");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, X-Requested-With, Cache-Control,Authorization");
*/
require_once '../api/conf/main.php';

$cdn = getParam('cdn', "https://test1.9day-game.com/game/bin/");
$os = getParam('os', 1); //1是安卓2是苹果
$channel = getParam('channel');

$backstage = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . "/api/sdk/game.php/";

$isHttps = is_https();

switch ($channel) {
    case '9130':
        $sdkHost = $isHttps ? _9130Controller::$jsSdkHost['https'] : _9130Controller::$jsSdkHost['http'];
        break;
    case 'fante':
        $sdkHost = $isHttps ? _9130Controller::$jsSdkHost['https'] : _9130Controller::$jsSdkHost['http'];
        // $sdkHost = $isHttps ? 'aksdk.js' :'aksdk.js';
        break;
    default:
        break;
}


$loading_js = $cdn . 'loading.js?v=' . time();
?>


<html>
<head id="head">
    <meta charset='utf-8'/>
    <title>九州</title>
    <meta name='renderer' content='webkit'>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover">
    <meta name='apple-mobile-web-app-capable' content='yes' />
    <meta name='full-screen' content='true' />
    <meta name='x5-fullscreen' content='true' />
    <meta name='360-fullscreen' content='true' />
    <meta http-equiv='expires' content='0' />
    <meta name='laya' screenorientation='portrait' />
    <!-- uc强制竖屏 -->
    <meta name="screen-orientation" content="portrait">
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta http-equiv='expires' content='0' />
    <meta http-equiv='Cache-Control' content='no-siteapp' />
    <meta name="full-screen" content="yes">
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app">
</head>
<body id="body">

<script type="text/javascript">
    "use strict";
    var DEBUG = true;
    var cdn = "<?php echo $cdn?>";
</script>

<script type="text/javascript" src="<?php echo $loading_js ?>"></script>
<script type="application/javascript">
    "use strict";
    //判断utf8_ba4函数是否存在
    if (typeof utf8_to_b64 === 'undefined' || typeof utf8_to_b64 !== 'function') {
        var utf8_to_b64 = function (str) {
            return window.btoa(unescape(encodeURIComponent(str)));
        }
    }

    function getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return decodeURI(r[2]);
        return null;
    }

    var tparams = {};
    tparams.channel = "<?php echo $channel?>";
    tparams.userId = getQueryString('uid');
    tparams.account = getQueryString('account');
    tparams.backstage = "<?php echo $backstage?>";
    tparams.cdn = "<?php echo $cdn?>";
    tparams.package = parseInt(getQueryString('package'));
    tparams.mac = 0;
    tparams.rvtype = parseInt(getQueryString('rvtype'));
    tparams.newregister = parseInt(getQueryString('newregister'));
    tparams.selectedServer = JSON.parse(getQueryString("selected_server"));


    <?php if ($channel == '9130' || $channel == 'fante'): ?>
    var sdk = window.document.createElement("script");
    sdk.language = "JavaScript";
    sdk.type = "text/javascript";
    sdk.onload = sdkInitedHandler;
    sdk.src = "<?php echo $sdkHost?>";
    document.head.appendChild(sdk);
    <?php else: ?>
    sdkInitedHandler();
    <?php endif; ?>

    // sdk初始化完成
    function sdkInitedHandler(){
        <?php if ($channel == '9130' || $channel == 'fante'): ?>
        AKSDK.onLogout(logoutCallBack);
        <?php endif; ?>
        enterGame();
    }

    // 发起支付，实际是向后台发送支付信息，由后台生成并返回订单，然后再向平台发起支付
    function ask_pay(roleId, roleName, roleLevel, roleCareer, productId, price) {
        var str = utf8_to_b64("game/getOrder/" + modules.login.LoginModel.instance.selectedServer.server_id + "/" + tparams.channel + "/" + roleId + "/" + roleName + "/" + productId + "/" + tparams.account + "/" + roleLevel + "/" + price + "/" + tparams.package + "/" + roleCareer + "/" + tparams.os);
        //var str = "game/getOrder/" + modules.login.LoginModel.instance.selectedServer.server_id + "/" + tparams.channel + "/" + roleId + "/" + roleName + "/" + productId + "/" + tparams.account + "/" + roleLevel + "/" + price + "/" + tparams.package + "/" + roleCareer + "/" + tparams.os;
        sendXHR(tparams.backstage + str, null, askPayCallBack);
    }

    // 支付后台回调，生成订单信息和要发给平台的信息
    function askPayCallBack(data) {
        if (data && data.errCode === 200) {
            <?php if ($channel == '9130' || $channel == 'fante'): ?>
            AKSDK.pay(data.data, platformPayCallBack);
            <?php endif; ?>
        } else {
            modules.common.CommonUtil.alert("提示", data.errMsg);
        }
    }

    // 平台支付回调
    function platformPayCallBack(status, data) {
        if (status === 0) {	// 支付成功
            //alert("支付成功");
            modules.common.CommonUtil.alert("提示", "支付成功");
        } else if (status === 1) {		// 支付失败
            modules.common.CommonUtil.alert("提示", "支付失败");
        } else if (status === 2) {		// 支付取消
            modules.common.CommonUtil.alert("提示", "支付取消");
        }
    }

    // 登出
    function logout() {
        <?php if ($channel == '9130' || $channel == 'fante'): ?>
        AKSDK.logout(logoutCallBack);
        <?php endif; ?>
    }

    // 登出回调
    function logoutCallBack(status) {
        window.location.href = tparams.cdn + "switch_account.html";
    }

    // 后台创建角色
    function player_create(role_id, role_name, role_level) {
        <?php if ($channel == '9130' || $channel == 'fante'): ?>
        //alert("logCreateRole.........." + role_id + "   " + role_name + "   " + role_level);
        AKSDK.logCreateRole(tparams.serverId, tparams.serverName, role_id, role_name, role_level);
        <?php endif; ?>
    }

    // 后台登录
    function player_login(role_id, role_name, role_level) {
        <?php if ($channel == '9130' || $channel == 'fante'): ?>
        // alert("logEnterGame.........." + role_id + "   " + role_name + "   " + role_level);
        AKSDK.logEnterGame(tparams.serverId, tparams.serverName, role_id, role_name, role_level);
        <?php endif; ?>
    }

    // 后台角色升级
    function player_level_up(role_id, role_name, role_level) {
        // alert("player_level_up.........." + role_id + "    " + role_name + "   " + role_level);
        <?php if ($channel == '9130' || $channel == 'fante'): ?>
        // alert("上报到9130");
        AKSDK.logRoleUpLevel(tparams.serverId, tparams.serverName, role_id, role_name, role_level);
        <?php endif; ?>
    }

    // 后台分享
    function player_share(id, name, type) {
        alert("share: " + type);
    }

    // 后台收藏
    function player_collect(id, name) {
        alert("player_collect");
    }

    // 通用发送请求方法
    function sendXHR(url, data, callBack) {
        //alert(url);
        var xhr = new XMLHttpRequest();
        xhr.responseType = "json";
        xhr.onload = function () {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 301)) {
                if (callBack) callBack(xhr.response);
            }
        };
        xhr.open("GET", url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        //xhr.withCredentials = true;
        xhr.send(data);
    }
    // 埋点
    function record_step(step, role_id) {
        return false;
    }

    // 请求服务器列表
    function req_server_list() {
        if (tparams.channel != null && tparams.package != null) {
            //alert("req_server_list............." + step + "   " + role_id);
            var str = utf8_to_b64("game/allServer/" + tparams.package + "/" + tparams.account + "/" + tparams.rvtype);
            sendXHR(tparams.backstage + str, null, reqServerListCallBack);
        } else {
            reqServerListCallBack(getQueryString("server_data"));
        }
    }
    // 请求服务器列表回调
    function reqServerListCallBack(data){
        let errCode = data.errCode;
        if(errCode === 200){        // 成功
            modules.login.LoginModel.instance.serverZoneNames = data.z;
            modules.login.LoginModel.instance.serverZones = data.s;
        }else{
            modules.login.LoginModel.instance.hasReq = false;
            alert(data.errMsg);
        }
    }


</script>

<!--<script type="text/javascript">-->
<!--enterGame();-->
<!--</script>-->
</body>
</html>
