<?php

/*
 * 将login和loading合为一个不做跳转
 * */

require_once '../api/conf/main.php';

$info = (new BaseEntrance())->init();

if ($info['code'] == 400) {
    Helper::log($info, 'BaseEntrance', 'dieApi[Entrance]');
    echo "<script>alert('".$info["msg"]."')</script>";
    exit;
}

$select_server_cdn = $info['data']['select_server_cdn'];
$select_server_js = $info['data']['select_server_js'];
$sdkHost = $info['data']['sdk_host'];
$mac = $info['data']['mac'];
$channel = $info['data']['channel'];
$package = $info['data']['package'];
$backstage = $info['data']['backstage'];
$version = $info['data']['version'];
$is_9130 = $info['data']['is_9130'];
$is_9187 = $info['data']['is_9187'];
$sdk_name = $info['data']['sdk_name'];
?>
<html>
<head id="head">
    <meta charset='utf-8'/>
    <title>九州</title>
    <meta name='renderer' content='webkit'>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover">
    <meta name='apple-mobile-web-app-capable' content='yes'/>
    <meta name='full-screen' content='true'/>
    <meta name='x5-fullscreen' content='true'/>
    <meta name='360-fullscreen' content='true'/>
    <meta http-equiv='expires' content='0'/>
    <meta name='laya' screenorientation='portrait'/>
    <!-- uc强制竖屏 -->
    <meta name="screen-orientation" content="portrait">
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta http-equiv='expires' content='0'/>
    <meta http-equiv='Cache-Control' content='no-siteapp'/>
    <meta name="full-screen" content="yes">
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app">
</head>
<body id="body">

<script type="text/javascript">
    "use strict";
    var DEBUG = true;
    var tparams = {};
    tparams.cdn = "<?php echo $select_server_cdn?>";
</script>
<script type="text/javascript" src="<?php echo $select_server_js ?>"></script>
<script type="application/javascript">
    "use strict";

    //判断utf8_ba4函数是否存在
    if (typeof utf8_to_b64 === 'undefined' || typeof utf8_to_b64 !== 'function') {
        var utf8_to_b64 = function (str) {
            return window.btoa(unescape(encodeURIComponent(str)));
            //return encodeURIComponent(window.btoa(str));
        }
    }

    var platform = "<?php echo $channel?>";
    tparams.channel = platform;
    tparams.package = "<?php echo $package?>";
    tparams.backstage = "<?php echo $backstage ?>";
    tparams.version = "<?php echo $version?>";
    tparams.mac = "<?php echo $mac?>";
    tparams.os = "<?php echo $os ?>";
    tparams.sdk_name = "<?php echo $sdk_name ?>";

    <?php if ($sdkHost): ?>
    var sdk = window.document.createElement("script");
    sdk.language = "JavaScript";
    sdk.type = "text/javascript";
    sdk.onload = sdkInitedHandler;
    sdk.src = "<?php echo $sdkHost?>";
    document.head.appendChild(sdk);
    <?php else: ?>
    alert('params error');
    <?php endif; ?>

    // sdk初始化完成后的处理
    function sdkInitedHandler() {
        <?php if ($is_9130 || $is_9187): ?>
        AKSDK.logLoadingFinish();
            <?php if ($is_9130): ?>
                AKSDK.login(_9130loginCallBack);
            <?php elseif ($is_9187): ?>
                AKSDK.login(_9187loginCallBack);
            <?php endif; ?>
        <?php endif; ?>
    }

    function _9130loginCallBack(status, data) {
        if (status === 0) {
            var str = utf8_to_b64("9130Check/run/" + data.account + "/" + data.userid + "/" + data.token + "/" + tparams.package + "/" + tparams.version);
            // 拿到平台信息后去校验
            sendXHR(tparams.backstage + str, null, checkTokenCallBack);
        } else {
            alert("登录/注册失败");
        }
    }

    function _9187loginCallBack(status, data) {
        if (status === 0) {
            var str = utf8_to_b64("9187Check/run/" + data.account + "/" + data.userid + "/" + data.token + "/" + tparams.package + "/" + tparams.version);
            // 拿到平台信息后去校验
            sendXHR(tparams.backstage + str, null, checkTokenCallBack);
        } else {
            alert("登录/注册失败");
        }
    }

    function checkTokenCallBack(value) {
        if (!value) return;
        if (value.state === 1) {
            var data = value.data;
            //服务器信息
            var server = data.s;
            tparams.channel = data.channel;
            tparams.userId = data.user.openId;
            tparams.account = data.user.openId;
            tparams.rvtype = data.rvtype;
            tparams.newregister = parseInt(data.newregister);
            tparams.package = parseInt(data.package);
            tparams.sign = data.sign;
            // 选中的服务器
            tparams.selectedServer = {
                'server_id': server.server_id,
                'name': server.name,
                'server_num': parseInt(server.server_num),
                'channel_num': parseInt(server.channel_num),
                'server_addr': server.server_addr,
                'server_port': parseInt(server.server_port),
                'status': parseInt(server.status),
                'tick': parseInt(server.tick),
                'sign': server.sign,
                'package': tparams.package,
                'cdn': server.cdn
            }

            sdkInited();
        } else {
            alert(value.msg);
        }
    }
    // sdk初始化完成
    function sdkInited() {
        <?php if ($is_9130 || $is_9187): ?>
        AKSDK.onLogout(logoutCallBack);
        <?php endif; ?>
        gotoSelectServer();
    }

    // 进入选服
    function gotoSelectServer(){
        if (window.selectServerView) {
            window.selectServerView.open();
        }
    }

    function ask_pay(roleId, roleName, roleLevel, roleCareer, productId, price) {
        var str = utf8_to_b64("game/getOrder/" + modules.login.LoginModel.instance.selectedServer.server_id + "/" + tparams.channel + "/" + roleId + "/" + roleName + "/" + productId + "/" + tparams.account + "/" + roleLevel + "/" + price + "/" + tparams.package + "/" + roleCareer + "/" + tparams.sign + "/" + tparams.os + "/" + tparams.sdk_name);
        sendXHR(tparams.backstage + str, null, askPayCallBack);
    }

    // 支付后台回调，生成订单信息和要发给平台的信息
    function askPayCallBack(data) {
        if(data) {
            if(data.errCode === 200) {
                <?php if ($is_9130 || $is_9187): ?>
                AKSDK.pay(data.data, platformPayCallBack);
                <?php endif; ?>
            } else {
                modules.common.CommonUtil.alert("提示", data.errMsg);
            }
        }
    }

    // 平台支付回调
    function platformPayCallBack(status, data) {
        if (status === 0) {	// 支付成功
            //alert("支付成功");
        } else if (status === 1) {		// 支付失败
            //modules.common.CommonUtil.alert("提示", "支付失败");
        } else if (status === 2) {		// 支付取消
            //modules.common.CommonUtil.alert("提示", "支付取消");
        }
    }

    // 登出
    function logout() {
        <?php if ($is_9130 || $is_9187): ?>
        AKSDK.logout(logoutCallBack);
        <?php endif; ?>
    }

    // 登出回调
    function logoutCallBack(status) {
        window.location.href = tparams.cdn + "switch_account.html";
    }

    // 后台创建角色
    function player_create(role_id, role_name, role_level) {
        // alert("player_create.........." + role_id + "   " + role_name + "   " + role_level);
        <?php if ($is_9130 || $is_9187): ?>
        AKSDK.logCreateRole(modules.login.LoginModel.instance.selectedServer.server_id, modules.login.LoginModel.instance.selectedServer.name, role_id, role_name, role_level);
        <?php endif; ?>
    }

    // 后台登录
    function player_login(role_id, role_name, role_level) {
        //alert("player_login.........." + role_id + "   " + role_name + "   " + role_level);
        <?php if ($is_9130 || $is_9187): ?>
        AKSDK.logEnterGame(modules.login.LoginModel.instance.selectedServer.server_id, modules.login.LoginModel.instance.selectedServer.name, role_id, role_name, role_level);
        <?php endif; ?>
    }

    // 后台角色升级
    function player_level_up(role_id, role_name, role_level) {
        // alert("player_level_up.........." + role_id + "    " + role_name + "   " + role_level);
        <?php if ($is_9130 || $is_9187): ?>
        // alert("上报到9130");
        AKSDK.logRoleUpLevel(modules.login.LoginModel.instance.selectedServer.server_id, modules.login.LoginModel.instance.selectedServer.name, role_id, role_name, role_level);
        <?php endif; ?>
    }

    //分享
    function player_share(role_id, callback) {
        <?php if ($is_9130 || $is_9187): ?>
        //status:0分享成功
        //status:1 分享失败
        var shareData = {
            'role_id': role_id,
            'server_id': modules.login.LoginModel.instance.selectedServer.server_id
        };
        //先默认返回成功测试
        //callback(0, {});

        AKSDK.share(shareData, function (status) {
            console.log('分享返回:');
            console.log(status);
            callback(status, {});
        });

        <?php else:?>
        callback(1, {});
        <?php endif; ?>
    }

    // 关注
    function player_follow(role_id, callback) {
        <?php if ($is_9130 || $is_9187): ?>
        var followData = {
            'role_id': role_id,
            'server_id': modules.login.LoginModel.instance.selectedServer.server_id
        };
        //先默认返回成功测试
       // callback(0, {});

        AKSDK.follow(followData, function (status) {
            console.log('关注返回:');
            console.log(status);

            if (status === 0) {
                account_info(3, {'isFollow': 1});
            }

            callback(status, {});
        });
        <?php else:?>
        callback(1, {});
        <?php endif; ?>
    }

    //实名认证
    function player_realname(callback) {
        <?php if ($is_9130 || $is_9187): ?>
        //callback(0, {});
            AKSDK.realName(function (state, data) {
                console.log('实名认证返回:');
                console.log(data);

                if (state === 0) {
                    account_info(1, data);
                }

                callback(state, data);

            });
        <?php else:?>
            callback(1, {});
        <?php endif; ?>
    }

    //手机绑定
    function player_bindphone(callback) {
        <?php if ($is_9130 || $is_9187): ?>
        //callback(0, {});
            AKSDK.bindPhone(function (state, data) {
                console.log('手机绑定返回:');
                console.log(data);

                if (state === 0) {
                    account_info(2, data);
                }

                callback(state, data);
            });
        <?php else:?>
            callback(1, {});
        <?php endif; ?>
    }

    // 后台收藏
    function player_collect(id, name) {
        alert("player_collect");
    }

    // 通用发送请求方法
    function sendXHR(url, data, callBack) {
        //alert(url);
        var xhr = new XMLHttpRequest();
        xhr.responseType = "json";
        xhr.onload = function () {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 301)) {
                if (callBack) callBack(xhr.response);
            }
        };
        xhr.open("GET", url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        //xhr.withCredentials = true;
        xhr.send(data);
    }

    // 埋点
    function record_step(step, role_id) {
        if (tparams.channel != null && tparams.package != null) {
            var serverTmpId = 0;
            var str;
            if (tparams.newregister === 1) {
                serverTmpId = tparams.selectedServer.server_id;
            }
            if (step > 3) {
                serverTmpId = modules.login.LoginModel.instance.selectedServer.server_id;
            }

            str = utf8_to_b64("game/node/" + tparams.channel + "/" + serverTmpId + "/" + tparams.package + "/" + step + "/" + tparams.account + "/" + tparams.sign + "/" + role_id + "/" + tparams.mac);
            //str = "game/node/" + tparams.channel + "/" + serverTmpId + "/" + tparams.package + "/" + step + "/" + tparams.account + "/" + role_id + "/" + tparams.mac;
            sendXHR(tparams.backstage + str, null, null);
        }
    }

    function account_info(type, info = {}) {
        if (tparams.channel != null && tparams.package != null) {
            info = JSON.parse(info);
            var str = utf8_to_b64('game/accountInfo/' + tparams.channel + '/' + tparams.package + '/' + tparams.account + '/' + type + '/' + info + "/" + tparams.sign);
            //str = "game/node/" + tparams.channel + "/" + serverTmpId + "/" + tparams.package + "/" + step + "/" + tparams.account + "/" + role_id + "/" + tparams.mac;
            sendXHR(tparams.backstage + str, null, null);
        }
    }

    // 请求服务器列表
    function req_server_list() {
        if (tparams.channel != null && tparams.package != null) {
            //alert("req_server_list............." + step + "   " + role_id);
            var str = utf8_to_b64("game/allServer/" + tparams.channel + "/" + tparams.package + "/" + tparams.account + "/" + tparams.rvtype + "/" + tparams.sign);
            sendXHR(tparams.backstage + str, null, reqServerListCallBack);
        }
    }

    // 请求服务器列表回调
    function reqServerListCallBack(data) {
        let errCode = data.errCode;
        if (errCode === 200) {        // 成功
            tparams.serverZoneNames = data.z;
            tparams.serverZones = data.s;
            selectServerView.refreshServerList();
        }
    }

</script>
</body>
</html>

