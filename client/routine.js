"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);
        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var JpegError = (function JpegErrorClosure() {
    function JpegError(msg) {
        this.message = 'JPEG error: ' + msg;
    }
    JpegError.prototype = new Error();
    JpegError.prototype.name = 'JpegError';
    JpegError.constructor = JpegError;
    return JpegError;
})();
var DNLMarkerError = (function DNLMarkerErrorClosure() {
    function DNLMarkerError(message, scanLines) {
        this.message = message;
        this.scanLines = scanLines;
    }
    DNLMarkerError.prototype = new Error();
    DNLMarkerError.prototype.name = 'DNLMarkerError';
    DNLMarkerError.constructor = DNLMarkerError;
    return DNLMarkerError;
})();
var EOIMarkerError = (function EOIMarkerErrorClosure() {
    function EOIMarkerError(message) {
        this.message = message;
    }
    EOIMarkerError.prototype = new Error();
    EOIMarkerError.prototype.name = 'EOIMarkerError';
    EOIMarkerError.constructor = EOIMarkerError;
    return EOIMarkerError;
})();
var JpegImage = (function JpegImageClosure() {
    var dctZigZag = new Uint8Array([
        0,
        1, 8,
        16, 9, 2,
        3, 10, 17, 24,
        32, 25, 18, 11, 4,
        5, 12, 19, 26, 33, 40,
        48, 41, 34, 27, 20, 13, 6,
        7, 14, 21, 28, 35, 42, 49, 56,
        57, 50, 43, 36, 29, 22, 15,
        23, 30, 37, 44, 51, 58,
        59, 52, 45, 38, 31,
        39, 46, 53, 60,
        61, 54, 47,
        55, 62,
        63
    ]);
    var dctCos1 = 4017;
    var dctSin1 = 799;
    var dctCos3 = 3406;
    var dctSin3 = 2276;
    var dctCos6 = 1567;
    var dctSin6 = 3784;
    var dctSqrt2 = 5793;
    var dctSqrt1d2 = 2896;
    function JpegImage(_a) {
        var _b = _a === void 0 ? {} : _a, _c = _b.decodeTransform, decodeTransform = _c === void 0 ? null : _c, _d = _b.colorTransform, colorTransform = _d === void 0 ? -1 : _d;
        this._decodeTransform = decodeTransform;
        this._colorTransform = colorTransform;
    }
    function buildHuffmanTable(codeLengths, values) {
        var k = 0, code = [], i, j, length = 16;
        while (length > 0 && !codeLengths[length - 1]) {
            length--;
        }
        code.push({ children: [], index: 0, });
        var p = code[0], q;
        for (i = 0; i < length; i++) {
            for (j = 0; j < codeLengths[i]; j++) {
                p = code.pop();
                p.children[p.index] = values[k];
                while (p.index > 0) {
                    p = code.pop();
                }
                p.index++;
                code.push(p);
                while (code.length <= i) {
                    code.push(q = { children: [], index: 0, });
                    p.children[p.index] = q.children;
                    p = q;
                }
                k++;
            }
            if (i + 1 < length) {
                code.push(q = { children: [], index: 0, });
                p.children[p.index] = q.children;
                p = q;
            }
        }
        return code[0].children;
    }
    function getBlockBufferOffset(component, row, col) {
        return 64 * ((component.blocksPerLine + 1) * row + col);
    }
    function decodeScan(data, offset, frame, components, resetInterval, spectralStart, spectralEnd, successivePrev, successive, parseDNLMarker) {
        if (parseDNLMarker === void 0) { parseDNLMarker = false; }
        var mcusPerLine = frame.mcusPerLine;
        var progressive = frame.progressive;
        var startOffset = offset, bitsData = 0, bitsCount = 0;
        function readBit() {
            if (bitsCount > 0) {
                bitsCount--;
                return (bitsData >> bitsCount) & 1;
            }
            bitsData = data[offset++];
            if (bitsData === 0xFF) {
                var nextByte = data[offset++];
                if (nextByte) {
                    if (nextByte === 0xDC && parseDNLMarker) {
                        offset += 2;
                        var scanLines = (data[offset++] << 8) | data[offset++];
                        if (scanLines > 0 && scanLines !== frame.scanLines) {
                            throw new DNLMarkerError('Found DNL marker (0xFFDC) while parsing scan data', scanLines);
                        }
                    }
                    else if (nextByte === 0xD9) {
                        throw new EOIMarkerError('Found EOI marker (0xFFD9) while parsing scan data');
                    }
                    throw new JpegError("unexpected marker " + ((bitsData << 8) | nextByte).toString(16));
                }
            }
            bitsCount = 7;
            return bitsData >>> 7;
        }
        function decodeHuffman(tree) {
            var node = tree;
            while (true) {
                node = node[readBit()];
                if (typeof node === 'number') {
                    return node;
                }
                if (typeof node !== 'object') {
                    throw new JpegError('invalid huffman sequence');
                }
            }
        }
        function receive(length) {
            var n = 0;
            while (length > 0) {
                n = (n << 1) | readBit();
                length--;
            }
            return n;
        }
        function receiveAndExtend(length) {
            if (length === 1) {
                return readBit() === 1 ? 1 : -1;
            }
            var n = receive(length);
            if (n >= 1 << (length - 1)) {
                return n;
            }
            return n + (-1 << length) + 1;
        }
        function decodeBaseline(component, offset) {
            var t = decodeHuffman(component.huffmanTableDC);
            var diff = t === 0 ? 0 : receiveAndExtend(t);
            component.blockData[offset] = (component.pred += diff);
            var k = 1;
            while (k < 64) {
                var rs = decodeHuffman(component.huffmanTableAC);
                var s = rs & 15, r = rs >> 4;
                if (s === 0) {
                    if (r < 15) {
                        break;
                    }
                    k += 16;
                    continue;
                }
                k += r;
                var z = dctZigZag[k];
                component.blockData[offset + z] = receiveAndExtend(s);
                k++;
            }
        }
        function decodeDCFirst(component, offset) {
            var t = decodeHuffman(component.huffmanTableDC);
            var diff = t === 0 ? 0 : (receiveAndExtend(t) << successive);
            component.blockData[offset] = (component.pred += diff);
        }
        function decodeDCSuccessive(component, offset) {
            component.blockData[offset] |= readBit() << successive;
        }
        var eobrun = 0;
        function decodeACFirst(component, offset) {
            if (eobrun > 0) {
                eobrun--;
                return;
            }
            var k = spectralStart, e = spectralEnd;
            while (k <= e) {
                var rs = decodeHuffman(component.huffmanTableAC);
                var s = rs & 15, r = rs >> 4;
                if (s === 0) {
                    if (r < 15) {
                        eobrun = receive(r) + (1 << r) - 1;
                        break;
                    }
                    k += 16;
                    continue;
                }
                k += r;
                var z = dctZigZag[k];
                component.blockData[offset + z] =
                    receiveAndExtend(s) * (1 << successive);
                k++;
            }
        }
        var successiveACState = 0, successiveACNextValue;
        function decodeACSuccessive(component, offset) {
            var k = spectralStart;
            var e = spectralEnd;
            var r = 0;
            var s;
            var rs;
            while (k <= e) {
                var offsetZ = offset + dctZigZag[k];
                var sign = component.blockData[offsetZ] < 0 ? -1 : 1;
                switch (successiveACState) {
                    case 0:
                        rs = decodeHuffman(component.huffmanTableAC);
                        s = rs & 15;
                        r = rs >> 4;
                        if (s === 0) {
                            if (r < 15) {
                                eobrun = receive(r) + (1 << r);
                                successiveACState = 4;
                            }
                            else {
                                r = 16;
                                successiveACState = 1;
                            }
                        }
                        else {
                            if (s !== 1) {
                                throw new JpegError('invalid ACn encoding');
                            }
                            successiveACNextValue = receiveAndExtend(s);
                            successiveACState = r ? 2 : 3;
                        }
                        continue;
                    case 1:
                    case 2:
                        if (component.blockData[offsetZ]) {
                            component.blockData[offsetZ] += sign * (readBit() << successive);
                        }
                        else {
                            r--;
                            if (r === 0) {
                                successiveACState = successiveACState === 2 ? 3 : 0;
                            }
                        }
                        break;
                    case 3:
                        if (component.blockData[offsetZ]) {
                            component.blockData[offsetZ] += sign * (readBit() << successive);
                        }
                        else {
                            component.blockData[offsetZ] =
                                successiveACNextValue << successive;
                            successiveACState = 0;
                        }
                        break;
                    case 4:
                        if (component.blockData[offsetZ]) {
                            component.blockData[offsetZ] += sign * (readBit() << successive);
                        }
                        break;
                }
                k++;
            }
            if (successiveACState === 4) {
                eobrun--;
                if (eobrun === 0) {
                    successiveACState = 0;
                }
            }
        }
        function decodeMcu(component, decode, mcu, row, col) {
            var mcuRow = (mcu / mcusPerLine) | 0;
            var mcuCol = mcu % mcusPerLine;
            var blockRow = mcuRow * component.v + row;
            var blockCol = mcuCol * component.h + col;
            var offset = getBlockBufferOffset(component, blockRow, blockCol);
            decode(component, offset);
        }
        function decodeBlock(component, decode, mcu) {
            var blockRow = (mcu / component.blocksPerLine) | 0;
            var blockCol = mcu % component.blocksPerLine;
            var offset = getBlockBufferOffset(component, blockRow, blockCol);
            decode(component, offset);
        }
        var componentsLength = components.length;
        var component, i, j, k, n;
        var decodeFn;
        if (progressive) {
            if (spectralStart === 0) {
                decodeFn = successivePrev === 0 ? decodeDCFirst : decodeDCSuccessive;
            }
            else {
                decodeFn = successivePrev === 0 ? decodeACFirst : decodeACSuccessive;
            }
        }
        else {
            decodeFn = decodeBaseline;
        }
        var mcu = 0, fileMarker;
        var mcuExpected;
        if (componentsLength === 1) {
            mcuExpected = components[0].blocksPerLine * components[0].blocksPerColumn;
        }
        else {
            mcuExpected = mcusPerLine * frame.mcusPerColumn;
        }
        var h, v;
        while (mcu < mcuExpected) {
            var mcuToRead = resetInterval ?
                Math.min(mcuExpected - mcu, resetInterval) : mcuExpected;
            for (i = 0; i < componentsLength; i++) {
                components[i].pred = 0;
            }
            eobrun = 0;
            if (componentsLength === 1) {
                component = components[0];
                for (n = 0; n < mcuToRead; n++) {
                    decodeBlock(component, decodeFn, mcu);
                    mcu++;
                }
            }
            else {
                for (n = 0; n < mcuToRead; n++) {
                    for (i = 0; i < componentsLength; i++) {
                        component = components[i];
                        h = component.h;
                        v = component.v;
                        for (j = 0; j < v; j++) {
                            for (k = 0; k < h; k++) {
                                decodeMcu(component, decodeFn, mcu, j, k);
                            }
                        }
                    }
                    mcu++;
                }
            }
            bitsCount = 0;
            fileMarker = findNextFileMarker(data, offset);
            if (fileMarker && fileMarker.invalid) {
                warn('decodeScan - unexpected MCU data, current marker is: ' +
                    fileMarker.invalid);
                offset = fileMarker.offset;
            }
            var marker = fileMarker && fileMarker.marker;
            if (!marker || marker <= 0xFF00) {
                throw new JpegError('marker was not found');
            }
            if (marker >= 0xFFD0 && marker <= 0xFFD7) {
                offset += 2;
            }
            else {
                break;
            }
        }
        fileMarker = findNextFileMarker(data, offset);
        if (fileMarker && fileMarker.invalid) {
            warn('decodeScan - unexpected Scan data, current marker is: ' +
                fileMarker.invalid);
            offset = fileMarker.offset;
        }
        return offset - startOffset;
    }
    function quantizeAndInverse(component, blockBufferOffset, p) {
        var qt = component.quantizationTable, blockData = component.blockData;
        var v0, v1, v2, v3, v4, v5, v6, v7;
        var p0, p1, p2, p3, p4, p5, p6, p7;
        var t;
        if (!qt) {
            throw new JpegError('missing required Quantization Table.');
        }
        for (var row = 0; row < 64; row += 8) {
            p0 = blockData[blockBufferOffset + row];
            p1 = blockData[blockBufferOffset + row + 1];
            p2 = blockData[blockBufferOffset + row + 2];
            p3 = blockData[blockBufferOffset + row + 3];
            p4 = blockData[blockBufferOffset + row + 4];
            p5 = blockData[blockBufferOffset + row + 5];
            p6 = blockData[blockBufferOffset + row + 6];
            p7 = blockData[blockBufferOffset + row + 7];
            p0 *= qt[row];
            if ((p1 | p2 | p3 | p4 | p5 | p6 | p7) === 0) {
                t = (dctSqrt2 * p0 + 512) >> 10;
                p[row] = t;
                p[row + 1] = t;
                p[row + 2] = t;
                p[row + 3] = t;
                p[row + 4] = t;
                p[row + 5] = t;
                p[row + 6] = t;
                p[row + 7] = t;
                continue;
            }
            p1 *= qt[row + 1];
            p2 *= qt[row + 2];
            p3 *= qt[row + 3];
            p4 *= qt[row + 4];
            p5 *= qt[row + 5];
            p6 *= qt[row + 6];
            p7 *= qt[row + 7];
            v0 = (dctSqrt2 * p0 + 128) >> 8;
            v1 = (dctSqrt2 * p4 + 128) >> 8;
            v2 = p2;
            v3 = p6;
            v4 = (dctSqrt1d2 * (p1 - p7) + 128) >> 8;
            v7 = (dctSqrt1d2 * (p1 + p7) + 128) >> 8;
            v5 = p3 << 4;
            v6 = p5 << 4;
            v0 = (v0 + v1 + 1) >> 1;
            v1 = v0 - v1;
            t = (v2 * dctSin6 + v3 * dctCos6 + 128) >> 8;
            v2 = (v2 * dctCos6 - v3 * dctSin6 + 128) >> 8;
            v3 = t;
            v4 = (v4 + v6 + 1) >> 1;
            v6 = v4 - v6;
            v7 = (v7 + v5 + 1) >> 1;
            v5 = v7 - v5;
            v0 = (v0 + v3 + 1) >> 1;
            v3 = v0 - v3;
            v1 = (v1 + v2 + 1) >> 1;
            v2 = v1 - v2;
            t = (v4 * dctSin3 + v7 * dctCos3 + 2048) >> 12;
            v4 = (v4 * dctCos3 - v7 * dctSin3 + 2048) >> 12;
            v7 = t;
            t = (v5 * dctSin1 + v6 * dctCos1 + 2048) >> 12;
            v5 = (v5 * dctCos1 - v6 * dctSin1 + 2048) >> 12;
            v6 = t;
            p[row] = v0 + v7;
            p[row + 7] = v0 - v7;
            p[row + 1] = v1 + v6;
            p[row + 6] = v1 - v6;
            p[row + 2] = v2 + v5;
            p[row + 5] = v2 - v5;
            p[row + 3] = v3 + v4;
            p[row + 4] = v3 - v4;
        }
        for (var col = 0; col < 8; ++col) {
            p0 = p[col];
            p1 = p[col + 8];
            p2 = p[col + 16];
            p3 = p[col + 24];
            p4 = p[col + 32];
            p5 = p[col + 40];
            p6 = p[col + 48];
            p7 = p[col + 56];
            if ((p1 | p2 | p3 | p4 | p5 | p6 | p7) === 0) {
                t = (dctSqrt2 * p0 + 8192) >> 14;
                t = (t < -2040) ? 0 : (t >= 2024) ? 255 : (t + 2056) >> 4;
                blockData[blockBufferOffset + col] = t;
                blockData[blockBufferOffset + col + 8] = t;
                blockData[blockBufferOffset + col + 16] = t;
                blockData[blockBufferOffset + col + 24] = t;
                blockData[blockBufferOffset + col + 32] = t;
                blockData[blockBufferOffset + col + 40] = t;
                blockData[blockBufferOffset + col + 48] = t;
                blockData[blockBufferOffset + col + 56] = t;
                continue;
            }
            v0 = (dctSqrt2 * p0 + 2048) >> 12;
            v1 = (dctSqrt2 * p4 + 2048) >> 12;
            v2 = p2;
            v3 = p6;
            v4 = (dctSqrt1d2 * (p1 - p7) + 2048) >> 12;
            v7 = (dctSqrt1d2 * (p1 + p7) + 2048) >> 12;
            v5 = p3;
            v6 = p5;
            v0 = ((v0 + v1 + 1) >> 1) + 4112;
            v1 = v0 - v1;
            t = (v2 * dctSin6 + v3 * dctCos6 + 2048) >> 12;
            v2 = (v2 * dctCos6 - v3 * dctSin6 + 2048) >> 12;
            v3 = t;
            v4 = (v4 + v6 + 1) >> 1;
            v6 = v4 - v6;
            v7 = (v7 + v5 + 1) >> 1;
            v5 = v7 - v5;
            v0 = (v0 + v3 + 1) >> 1;
            v3 = v0 - v3;
            v1 = (v1 + v2 + 1) >> 1;
            v2 = v1 - v2;
            t = (v4 * dctSin3 + v7 * dctCos3 + 2048) >> 12;
            v4 = (v4 * dctCos3 - v7 * dctSin3 + 2048) >> 12;
            v7 = t;
            t = (v5 * dctSin1 + v6 * dctCos1 + 2048) >> 12;
            v5 = (v5 * dctCos1 - v6 * dctSin1 + 2048) >> 12;
            v6 = t;
            p0 = v0 + v7;
            p7 = v0 - v7;
            p1 = v1 + v6;
            p6 = v1 - v6;
            p2 = v2 + v5;
            p5 = v2 - v5;
            p3 = v3 + v4;
            p4 = v3 - v4;
            p0 = (p0 < 16) ? 0 : (p0 >= 4080) ? 255 : p0 >> 4;
            p1 = (p1 < 16) ? 0 : (p1 >= 4080) ? 255 : p1 >> 4;
            p2 = (p2 < 16) ? 0 : (p2 >= 4080) ? 255 : p2 >> 4;
            p3 = (p3 < 16) ? 0 : (p3 >= 4080) ? 255 : p3 >> 4;
            p4 = (p4 < 16) ? 0 : (p4 >= 4080) ? 255 : p4 >> 4;
            p5 = (p5 < 16) ? 0 : (p5 >= 4080) ? 255 : p5 >> 4;
            p6 = (p6 < 16) ? 0 : (p6 >= 4080) ? 255 : p6 >> 4;
            p7 = (p7 < 16) ? 0 : (p7 >= 4080) ? 255 : p7 >> 4;
            blockData[blockBufferOffset + col] = p0;
            blockData[blockBufferOffset + col + 8] = p1;
            blockData[blockBufferOffset + col + 16] = p2;
            blockData[blockBufferOffset + col + 24] = p3;
            blockData[blockBufferOffset + col + 32] = p4;
            blockData[blockBufferOffset + col + 40] = p5;
            blockData[blockBufferOffset + col + 48] = p6;
            blockData[blockBufferOffset + col + 56] = p7;
        }
    }
    function buildComponentData(frame, component) {
        var blocksPerLine = component.blocksPerLine;
        var blocksPerColumn = component.blocksPerColumn;
        var computationBuffer = new Int16Array(64);
        for (var blockRow = 0; blockRow < blocksPerColumn; blockRow++) {
            for (var blockCol = 0; blockCol < blocksPerLine; blockCol++) {
                var offset = getBlockBufferOffset(component, blockRow, blockCol);
                quantizeAndInverse(component, offset, computationBuffer);
            }
        }
        return component.blockData;
    }
    function findNextFileMarker(data, currentPos, startPos) {
        if (startPos === void 0) { startPos = currentPos; }
        function peekUint16(pos) {
            return (data[pos] << 8) | data[pos + 1];
        }
        var maxPos = data.length - 1;
        var newPos = startPos < currentPos ? startPos : currentPos;
        if (currentPos >= maxPos) {
            return null;
        }
        var currentMarker = peekUint16(currentPos);
        if (currentMarker >= 0xFFC0 && currentMarker <= 0xFFFE) {
            return {
                invalid: null,
                marker: currentMarker,
                offset: currentPos,
            };
        }
        var newMarker = peekUint16(newPos);
        while (!(newMarker >= 0xFFC0 && newMarker <= 0xFFFE)) {
            if (++newPos >= maxPos) {
                return null;
            }
            newMarker = peekUint16(newPos);
        }
        return {
            invalid: currentMarker.toString(16),
            marker: newMarker,
            offset: newPos,
        };
    }
    JpegImage.prototype = {
        width: 0,
        height: 0,
        parse: function (data, _a) {
            var _b = (_a === void 0 ? {} : _a).dnlScanLines, dnlScanLines = _b === void 0 ? null : _b;
            function readUint16() {
                var value = (data[offset] << 8) | data[offset + 1];
                offset += 2;
                return value;
            }
            function readDataBlock() {
                var length = readUint16();
                var endOffset = offset + length - 2;
                var fileMarker = findNextFileMarker(data, endOffset, offset);
                if (fileMarker && fileMarker.invalid) {
                    warn('readDataBlock - incorrect length, current marker is: ' +
                        fileMarker.invalid);
                    endOffset = fileMarker.offset;
                }
                var array = data.subarray(offset, endOffset);
                offset += array.length;
                return array;
            }
            function prepareComponents(frame) {
                var mcusPerLine = Math.ceil(frame.samplesPerLine / 8 / frame.maxH);
                var mcusPerColumn = Math.ceil(frame.scanLines / 8 / frame.maxV);
                for (var i = 0; i < frame.components.length; i++) {
                    component = frame.components[i];
                    var blocksPerLine = Math.ceil(Math.ceil(frame.samplesPerLine / 8) *
                        component.h / frame.maxH);
                    var blocksPerColumn = Math.ceil(Math.ceil(frame.scanLines / 8) *
                        component.v / frame.maxV);
                    var blocksPerLineForMcu = mcusPerLine * component.h;
                    var blocksPerColumnForMcu = mcusPerColumn * component.v;
                    var blocksBufferSize = 64 * blocksPerColumnForMcu *
                        (blocksPerLineForMcu + 1);
                    component.blockData = new Int16Array(blocksBufferSize);
                    component.blocksPerLine = blocksPerLine;
                    component.blocksPerColumn = blocksPerColumn;
                }
                frame.mcusPerLine = mcusPerLine;
                frame.mcusPerColumn = mcusPerColumn;
            }
            var offset = 0;
            var jfif = null;
            var adobe = null;
            var frame, resetInterval;
            var numSOSMarkers = 0;
            var quantizationTables = [];
            var huffmanTablesAC = [], huffmanTablesDC = [];
            var fileMarker = readUint16();
            if (fileMarker !== 0xFFD8) {
                throw new JpegError('SOI not found');
            }
            fileMarker = readUint16();
            markerLoop: while (fileMarker !== 0xFFD9) {
                var i, j, l;
                switch (fileMarker) {
                    case 0xFFE0:
                    case 0xFFE1:
                    case 0xFFE2:
                    case 0xFFE3:
                    case 0xFFE4:
                    case 0xFFE5:
                    case 0xFFE6:
                    case 0xFFE7:
                    case 0xFFE8:
                    case 0xFFE9:
                    case 0xFFEA:
                    case 0xFFEB:
                    case 0xFFEC:
                    case 0xFFED:
                    case 0xFFEE:
                    case 0xFFEF:
                    case 0xFFFE:
                        var appData = readDataBlock();
                        if (fileMarker === 0xFFE0) {
                            if (appData[0] === 0x4A && appData[1] === 0x46 &&
                                appData[2] === 0x49 && appData[3] === 0x46 &&
                                appData[4] === 0) {
                                jfif = {
                                    version: { major: appData[5], minor: appData[6], },
                                    densityUnits: appData[7],
                                    xDensity: (appData[8] << 8) | appData[9],
                                    yDensity: (appData[10] << 8) | appData[11],
                                    thumbWidth: appData[12],
                                    thumbHeight: appData[13],
                                    thumbData: appData.subarray(14, 14 +
                                        3 * appData[12] * appData[13]),
                                };
                            }
                        }
                        if (fileMarker === 0xFFEE) {
                            if (appData[0] === 0x41 && appData[1] === 0x64 &&
                                appData[2] === 0x6F && appData[3] === 0x62 &&
                                appData[4] === 0x65) {
                                adobe = {
                                    version: (appData[5] << 8) | appData[6],
                                    flags0: (appData[7] << 8) | appData[8],
                                    flags1: (appData[9] << 8) | appData[10],
                                    transformCode: appData[11],
                                };
                            }
                        }
                        break;
                    case 0xFFDB:
                        var quantizationTablesLength = readUint16();
                        var quantizationTablesEnd = quantizationTablesLength + offset - 2;
                        var z;
                        while (offset < quantizationTablesEnd) {
                            var quantizationTableSpec = data[offset++];
                            var tableData = new Uint16Array(64);
                            if ((quantizationTableSpec >> 4) === 0) {
                                for (j = 0; j < 64; j++) {
                                    z = dctZigZag[j];
                                    tableData[z] = data[offset++];
                                }
                            }
                            else if ((quantizationTableSpec >> 4) === 1) {
                                for (j = 0; j < 64; j++) {
                                    z = dctZigZag[j];
                                    tableData[z] = readUint16();
                                }
                            }
                            else {
                                throw new JpegError('DQT - invalid table spec');
                            }
                            quantizationTables[quantizationTableSpec & 15] = tableData;
                        }
                        break;
                    case 0xFFC0:
                    case 0xFFC1:
                    case 0xFFC2:
                        if (frame) {
                            throw new JpegError('Only single frame JPEGs supported');
                        }
                        readUint16();
                        frame = {};
                        frame.extended = (fileMarker === 0xFFC1);
                        frame.progressive = (fileMarker === 0xFFC2);
                        frame.precision = data[offset++];
                        var sofScanLines = readUint16();
                        frame.scanLines = dnlScanLines || sofScanLines;
                        frame.samplesPerLine = readUint16();
                        frame.components = [];
                        frame.componentIds = {};
                        var componentsCount = data[offset++], componentId;
                        var maxH = 0, maxV = 0;
                        for (i = 0; i < componentsCount; i++) {
                            componentId = data[offset];
                            var h = data[offset + 1] >> 4;
                            var v = data[offset + 1] & 15;
                            if (maxH < h) {
                                maxH = h;
                            }
                            if (maxV < v) {
                                maxV = v;
                            }
                            var qId = data[offset + 2];
                            l = frame.components.push({
                                h: h,
                                v: v,
                                quantizationId: qId,
                                quantizationTable: null,
                            });
                            frame.componentIds[componentId] = l - 1;
                            offset += 3;
                        }
                        frame.maxH = maxH;
                        frame.maxV = maxV;
                        prepareComponents(frame);
                        break;
                    case 0xFFC4:
                        var huffmanLength = readUint16();
                        for (i = 2; i < huffmanLength;) {
                            var huffmanTableSpec = data[offset++];
                            var codeLengths = new Uint8Array(16);
                            var codeLengthSum = 0;
                            for (j = 0; j < 16; j++, offset++) {
                                codeLengthSum += (codeLengths[j] = data[offset]);
                            }
                            var huffmanValues = new Uint8Array(codeLengthSum);
                            for (j = 0; j < codeLengthSum; j++, offset++) {
                                huffmanValues[j] = data[offset];
                            }
                            i += 17 + codeLengthSum;
                            ((huffmanTableSpec >> 4) === 0 ?
                                huffmanTablesDC : huffmanTablesAC)[huffmanTableSpec & 15] =
                                buildHuffmanTable(codeLengths, huffmanValues);
                        }
                        break;
                    case 0xFFDD:
                        readUint16();
                        resetInterval = readUint16();
                        break;
                    case 0xFFDA:
                        var parseDNLMarker = (++numSOSMarkers) === 1 && !dnlScanLines;
                        readUint16();
                        var selectorsCount = data[offset++];
                        var components = [], component;
                        for (i = 0; i < selectorsCount; i++) {
                            var componentIndex = frame.componentIds[data[offset++]];
                            component = frame.components[componentIndex];
                            var tableSpec = data[offset++];
                            component.huffmanTableDC = huffmanTablesDC[tableSpec >> 4];
                            component.huffmanTableAC = huffmanTablesAC[tableSpec & 15];
                            components.push(component);
                        }
                        var spectralStart = data[offset++];
                        var spectralEnd = data[offset++];
                        var successiveApproximation = data[offset++];
                        try {
                            var processed = decodeScan(data, offset, frame, components, resetInterval, spectralStart, spectralEnd, successiveApproximation >> 4, successiveApproximation & 15, parseDNLMarker);
                            offset += processed;
                        }
                        catch (ex) {
                            if (ex instanceof DNLMarkerError) {
                                warn(ex.message + " -- attempting to re-parse the JPEG image.");
                                return this.parse(data, { dnlScanLines: ex.scanLines, });
                            }
                            else if (ex instanceof EOIMarkerError) {
                                warn(ex.message + " -- ignoring the rest of the image data.");
                                break markerLoop;
                            }
                            throw ex;
                        }
                        break;
                    case 0xFFDC:
                        offset += 4;
                        break;
                    case 0xFFFF:
                        if (data[offset] !== 0xFF) {
                            offset--;
                        }
                        break;
                    default:
                        if (data[offset - 3] === 0xFF &&
                            data[offset - 2] >= 0xC0 && data[offset - 2] <= 0xFE) {
                            offset -= 3;
                            break;
                        }
                        var nextFileMarker = findNextFileMarker(data, offset - 2);
                        if (nextFileMarker && nextFileMarker.invalid) {
                            warn('JpegImage.parse - unexpected data, current marker is: ' +
                                nextFileMarker.invalid);
                            offset = nextFileMarker.offset;
                            break;
                        }
                        throw new JpegError('unknown marker ' + fileMarker.toString(16));
                }
                fileMarker = readUint16();
            }
            this.width = frame.samplesPerLine;
            this.height = frame.scanLines;
            this.jfif = jfif;
            this.adobe = adobe;
            this.components = [];
            for (i = 0; i < frame.components.length; i++) {
                component = frame.components[i];
                var quantizationTable = quantizationTables[component.quantizationId];
                if (quantizationTable) {
                    component.quantizationTable = quantizationTable;
                }
                this.components.push({
                    output: buildComponentData(frame, component),
                    scaleX: component.h / frame.maxH,
                    scaleY: component.v / frame.maxV,
                    blocksPerLine: component.blocksPerLine,
                    blocksPerColumn: component.blocksPerColumn,
                });
            }
            this.numComponents = this.components.length;
        },
        _getLinearizedBlockData: function (width, height, isSourcePDF) {
            if (isSourcePDF === void 0) { isSourcePDF = false; }
            var scaleX = this.width / width, scaleY = this.height / height;
            var component, componentScaleX, componentScaleY, blocksPerScanline;
            var x, y, i, j, k;
            var index;
            var offset = 0;
            var output;
            var numComponents = this.components.length;
            var dataLength = width * height * numComponents;
            var data = new Uint8ClampedArray(dataLength);
            var xScaleBlockOffset = new Uint32Array(width);
            var mask3LSB = 0xfffffff8;
            for (i = 0; i < numComponents; i++) {
                component = this.components[i];
                componentScaleX = component.scaleX * scaleX;
                componentScaleY = component.scaleY * scaleY;
                offset = i;
                output = component.output;
                blocksPerScanline = (component.blocksPerLine + 1) << 3;
                for (x = 0; x < width; x++) {
                    j = 0 | (x * componentScaleX);
                    xScaleBlockOffset[x] = ((j & mask3LSB) << 3) | (j & 7);
                }
                for (y = 0; y < height; y++) {
                    j = 0 | (y * componentScaleY);
                    index = blocksPerScanline * (j & mask3LSB) | ((j & 7) << 3);
                    for (x = 0; x < width; x++) {
                        data[offset] = output[index + xScaleBlockOffset[x]];
                        offset += numComponents;
                    }
                }
            }
            var transform = this._decodeTransform;
            if (!isSourcePDF && numComponents === 4 && !transform) {
                transform = new Int32Array([
                    -256, 255, -256, 255, -256, 255, -256, 255
                ]);
            }
            if (transform) {
                for (i = 0; i < dataLength;) {
                    for (j = 0, k = 0; j < numComponents; j++, i++, k += 2) {
                        data[i] = ((data[i] * transform[k]) >> 8) + transform[k + 1];
                    }
                }
            }
            return data;
        },
        get _isColorConversionNeeded() {
            if (this.adobe) {
                return !!this.adobe.transformCode;
            }
            if (this.numComponents === 3) {
                if (this._colorTransform === 0) {
                    return false;
                }
                return true;
            }
            if (this._colorTransform === 1) {
                return true;
            }
            return false;
        },
        _convertYccToRgb: function convertYccToRgb(data) {
            var Y, Cb, Cr;
            for (var i = 0, length = data.length; i < length; i += 3) {
                Y = data[i];
                Cb = data[i + 1];
                Cr = data[i + 2];
                data[i] = Y - 179.456 + 1.402 * Cr;
                data[i + 1] = Y + 135.459 - 0.344 * Cb - 0.714 * Cr;
                data[i + 2] = Y - 226.816 + 1.772 * Cb;
            }
            return data;
        },
        _convertYcckToRgb: function convertYcckToRgb(data) {
            var Y, Cb, Cr, k;
            var offset = 0;
            for (var i = 0, length = data.length; i < length; i += 4) {
                Y = data[i];
                Cb = data[i + 1];
                Cr = data[i + 2];
                k = data[i + 3];
                data[offset++] = -122.67195406894 +
                    Cb * (-6.60635669420364e-5 * Cb + 0.000437130475926232 * Cr -
                        5.4080610064599e-5 * Y + 0.00048449797120281 * k -
                        0.154362151871126) +
                    Cr * (-0.000957964378445773 * Cr + 0.000817076911346625 * Y -
                        0.00477271405408747 * k + 1.53380253221734) +
                    Y * (0.000961250184130688 * Y - 0.00266257332283933 * k +
                        0.48357088451265) +
                    k * (-0.000336197177618394 * k + 0.484791561490776);
                data[offset++] = 107.268039397724 +
                    Cb * (2.19927104525741e-5 * Cb - 0.000640992018297945 * Cr +
                        0.000659397001245577 * Y + 0.000426105652938837 * k -
                        0.176491792462875) +
                    Cr * (-0.000778269941513683 * Cr + 0.00130872261408275 * Y +
                        0.000770482631801132 * k - 0.151051492775562) +
                    Y * (0.00126935368114843 * Y - 0.00265090189010898 * k +
                        0.25802910206845) +
                    k * (-0.000318913117588328 * k - 0.213742400323665);
                data[offset++] = -20.810012546947 +
                    Cb * (-0.000570115196973677 * Cb - 2.63409051004589e-5 * Cr +
                        0.0020741088115012 * Y - 0.00288260236853442 * k +
                        0.814272968359295) +
                    Cr * (-1.53496057440975e-5 * Cr - 0.000132689043961446 * Y +
                        0.000560833691242812 * k - 0.195152027534049) +
                    Y * (0.00174418132927582 * Y - 0.00255243321439347 * k +
                        0.116935020465145) +
                    k * (-0.000343531996510555 * k + 0.24165260232407);
            }
            return data.subarray(0, offset);
        },
        _convertYcckToCmyk: function convertYcckToCmyk(data) {
            var Y, Cb, Cr;
            for (var i = 0, length = data.length; i < length; i += 4) {
                Y = data[i];
                Cb = data[i + 1];
                Cr = data[i + 2];
                data[i] = 434.456 - Y - 1.402 * Cr;
                data[i + 1] = 119.541 - Y + 0.344 * Cb + 0.714 * Cr;
                data[i + 2] = 481.816 - Y - 1.772 * Cb;
            }
            return data;
        },
        _convertCmykToRgb: function convertCmykToRgb(data) {
            var c, m, y, k;
            var offset = 0;
            var scale = 1 / 255;
            for (var i = 0, length = data.length; i < length; i += 4) {
                c = data[i] * scale;
                m = data[i + 1] * scale;
                y = data[i + 2] * scale;
                k = data[i + 3] * scale;
                data[offset++] = 255 +
                    c * (-4.387332384609988 * c + 54.48615194189176 * m +
                        18.82290502165302 * y + 212.25662451639585 * k -
                        285.2331026137004) +
                    m * (1.7149763477362134 * m - 5.6096736904047315 * y -
                        17.873870861415444 * k - 5.497006427196366) +
                    y * (-2.5217340131683033 * y - 21.248923337353073 * k +
                        17.5119270841813) -
                    k * (21.86122147463605 * k + 189.48180835922747);
                data[offset++] = 255 +
                    c * (8.841041422036149 * c + 60.118027045597366 * m +
                        6.871425592049007 * y + 31.159100130055922 * k -
                        79.2970844816548) +
                    m * (-15.310361306967817 * m + 17.575251261109482 * y +
                        131.35250912493976 * k - 190.9453302588951) +
                    y * (4.444339102852739 * y + 9.8632861493405 * k -
                        24.86741582555878) -
                    k * (20.737325471181034 * k + 187.80453709719578);
                data[offset++] = 255 +
                    c * (0.8842522430003296 * c + 8.078677503112928 * m +
                        30.89978309703729 * y - 0.23883238689178934 * k -
                        14.183576799673286) +
                    m * (10.49593273432072 * m + 63.02378494754052 * y +
                        50.606957656360734 * k - 112.23884253719248) +
                    y * (0.03296041114873217 * y + 115.60384449646641 * k -
                        193.58209356861505) -
                    k * (22.33816807309886 * k + 180.12613974708367);
            }
            return data.subarray(0, offset);
        },
        getData: function (width, height, forceRGB, isSourcePDF) {
            if (forceRGB === void 0) { forceRGB = false; }
            if (isSourcePDF === void 0) { isSourcePDF = false; }
            if (this.numComponents > 4) {
                throw new JpegError('Unsupported color mode');
            }
            var data = this._getLinearizedBlockData(width, height, isSourcePDF);
            if (this.numComponents === 1 && forceRGB) {
                var dataLength = data.length;
                var rgbData = new Uint8ClampedArray(dataLength * 3);
                var offset = 0;
                for (var i = 0; i < dataLength; i++) {
                    var grayColor = data[i];
                    rgbData[offset++] = grayColor;
                    rgbData[offset++] = grayColor;
                    rgbData[offset++] = grayColor;
                }
                return rgbData;
            }
            else if (this.numComponents === 3 && this._isColorConversionNeeded) {
                return this._convertYccToRgb(data);
            }
            else if (this.numComponents === 4) {
                if (this._isColorConversionNeeded) {
                    if (forceRGB) {
                        return this._convertYcckToRgb(data);
                    }
                    return this._convertYcckToCmyk(data);
                }
                else if (forceRGB) {
                    return this._convertCmykToRgb(data);
                }
            }
            return data;
        },
    };
    return JpegImage;
})();
(function () {
    'use strict';
    var l = void 0, aa = this;
    function r(c, d) { var a = c.split("."), b = aa; !(a[0] in b) && b.execScript && b.execScript("var " + a[0]); for (var e; a.length && (e = a.shift());)
        !a.length && d !== l ? b[e] = d : b = b[e] ? b[e] : b[e] = {}; }
    ;
    var t = "undefined" !== typeof Uint8Array && "undefined" !== typeof Uint16Array && "undefined" !== typeof Uint32Array && "undefined" !== typeof DataView;
    function v(c) { var d = c.length, a = 0, b = Number.POSITIVE_INFINITY, e, f, g, h, k, m, n, p, s, x; for (p = 0; p < d; ++p)
        c[p] > a && (a = c[p]), c[p] < b && (b = c[p]); e = 1 << a; f = new (t ? Uint32Array : Array)(e); g = 1; h = 0; for (k = 2; g <= a;) {
        for (p = 0; p < d; ++p)
            if (c[p] === g) {
                m = 0;
                n = h;
                for (s = 0; s < g; ++s)
                    m = m << 1 | n & 1, n >>= 1;
                x = g << 16 | p;
                for (s = m; s < e; s += k)
                    f[s] = x;
                ++h;
            }
        ++g;
        h <<= 1;
        k <<= 1;
    } return [f, a, b]; }
    ;
    function w(c, d) {
        this.g = [];
        this.h = 32768;
        this.d = this.f = this.a = this.l = 0;
        this.input = t ? new Uint8Array(c) : c;
        this.m = !1;
        this.i = y;
        this.r = !1;
        if (d || !(d = {}))
            d.index && (this.a = d.index), d.bufferSize && (this.h = d.bufferSize), d.bufferType && (this.i = d.bufferType), d.resize && (this.r = d.resize);
        switch (this.i) {
            case A:
                this.b = 32768;
                this.c = new (t ? Uint8Array : Array)(32768 + this.h + 258);
                break;
            case y:
                this.b = 0;
                this.c = new (t ? Uint8Array : Array)(this.h);
                this.e = this.z;
                this.n = this.v;
                this.j = this.w;
                break;
            default: throw Error("invalid inflate mode");
        }
    }
    var A = 0, y = 1, B = { t: A, s: y };
    w.prototype.k = function () {
        for (; !this.m;) {
            var c = C(this, 3);
            c & 1 && (this.m = !0);
            c >>>= 1;
            switch (c) {
                case 0:
                    var d = this.input, a = this.a, b = this.c, e = this.b, f = d.length, g = l, h = l, k = b.length, m = l;
                    this.d = this.f = 0;
                    if (a + 1 >= f)
                        throw Error("invalid uncompressed block header: LEN");
                    g = d[a++] | d[a++] << 8;
                    if (a + 1 >= f)
                        throw Error("invalid uncompressed block header: NLEN");
                    h = d[a++] | d[a++] << 8;
                    if (g === ~h)
                        throw Error("invalid uncompressed block header: length verify");
                    if (a + g > d.length)
                        throw Error("input buffer is broken");
                    switch (this.i) {
                        case A:
                            for (; e +
                                g > b.length;) {
                                m = k - e;
                                g -= m;
                                if (t)
                                    b.set(d.subarray(a, a + m), e), e += m, a += m;
                                else
                                    for (; m--;)
                                        b[e++] = d[a++];
                                this.b = e;
                                b = this.e();
                                e = this.b;
                            }
                            break;
                        case y:
                            for (; e + g > b.length;)
                                b = this.e({ p: 2 });
                            break;
                        default: throw Error("invalid inflate mode");
                    }
                    if (t)
                        b.set(d.subarray(a, a + g), e), e += g, a += g;
                    else
                        for (; g--;)
                            b[e++] = d[a++];
                    this.a = a;
                    this.b = e;
                    this.c = b;
                    break;
                case 1:
                    this.j(ba, ca);
                    break;
                case 2:
                    for (var n = C(this, 5) + 257, p = C(this, 5) + 1, s = C(this, 4) + 4, x = new (t ? Uint8Array : Array)(D.length), S = l, T = l, U = l, u = l, M = l, F = l, z = l, q = l, V = l, q = 0; q < s; ++q)
                        x[D[q]] =
                            C(this, 3);
                    if (!t) {
                        q = s;
                        for (s = x.length; q < s; ++q)
                            x[D[q]] = 0;
                    }
                    S = v(x);
                    u = new (t ? Uint8Array : Array)(n + p);
                    q = 0;
                    for (V = n + p; q < V;)
                        switch (M = E(this, S), M) {
                            case 16:
                                for (z = 3 + C(this, 2); z--;)
                                    u[q++] = F;
                                break;
                            case 17:
                                for (z = 3 + C(this, 3); z--;)
                                    u[q++] = 0;
                                F = 0;
                                break;
                            case 18:
                                for (z = 11 + C(this, 7); z--;)
                                    u[q++] = 0;
                                F = 0;
                                break;
                            default: F = u[q++] = M;
                        }
                    T = t ? v(u.subarray(0, n)) : v(u.slice(0, n));
                    U = t ? v(u.subarray(n)) : v(u.slice(n));
                    this.j(T, U);
                    break;
                default: throw Error("unknown BTYPE: " + c);
            }
        }
        return this.n();
    };
    var G = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15], D = t ? new Uint16Array(G) : G, H = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 258, 258], I = t ? new Uint16Array(H) : H, J = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 0, 0], K = t ? new Uint8Array(J) : J, L = [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577], da = t ? new Uint16Array(L) : L, ea = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12,
        13, 13], N = t ? new Uint8Array(ea) : ea, O = new (t ? Uint8Array : Array)(288), P, fa;
    P = 0;
    for (fa = O.length; P < fa; ++P)
        O[P] = 143 >= P ? 8 : 255 >= P ? 9 : 279 >= P ? 7 : 8;
    var ba = v(O), Q = new (t ? Uint8Array : Array)(30), R, ga;
    R = 0;
    for (ga = Q.length; R < ga; ++R)
        Q[R] = 5;
    var ca = v(Q);
    function C(c, d) { for (var a = c.f, b = c.d, e = c.input, f = c.a, g = e.length, h; b < d;) {
        if (f >= g)
            throw Error("input buffer is broken");
        a |= e[f++] << b;
        b += 8;
    } h = a & (1 << d) - 1; c.f = a >>> d; c.d = b - d; c.a = f; return h; }
    function E(c, d) { for (var a = c.f, b = c.d, e = c.input, f = c.a, g = e.length, h = d[0], k = d[1], m, n; b < k && !(f >= g);)
        a |= e[f++] << b, b += 8; m = h[a & (1 << k) - 1]; n = m >>> 16; if (n > b)
        throw Error("invalid code length: " + n); c.f = a >> n; c.d = b - n; c.a = f; return m & 65535; }
    w.prototype.j = function (c, d) { var a = this.c, b = this.b; this.o = c; for (var e = a.length - 258, f, g, h, k; 256 !== (f = E(this, c));)
        if (256 > f)
            b >= e && (this.b = b, a = this.e(), b = this.b), a[b++] = f;
        else {
            g = f - 257;
            k = I[g];
            0 < K[g] && (k += C(this, K[g]));
            f = E(this, d);
            h = da[f];
            0 < N[f] && (h += C(this, N[f]));
            b >= e && (this.b = b, a = this.e(), b = this.b);
            for (; k--;)
                a[b] = a[b++ - h];
        } for (; 8 <= this.d;)
        this.d -= 8, this.a--; this.b = b; };
    w.prototype.w = function (c, d) { var a = this.c, b = this.b; this.o = c; for (var e = a.length, f, g, h, k; 256 !== (f = E(this, c));)
        if (256 > f)
            b >= e && (a = this.e(), e = a.length), a[b++] = f;
        else {
            g = f - 257;
            k = I[g];
            0 < K[g] && (k += C(this, K[g]));
            f = E(this, d);
            h = da[f];
            0 < N[f] && (h += C(this, N[f]));
            b + k > e && (a = this.e(), e = a.length);
            for (; k--;)
                a[b] = a[b++ - h];
        } for (; 8 <= this.d;)
        this.d -= 8, this.a--; this.b = b; };
    w.prototype.e = function () { var c = new (t ? Uint8Array : Array)(this.b - 32768), d = this.b - 32768, a, b, e = this.c; if (t)
        c.set(e.subarray(32768, c.length));
    else {
        a = 0;
        for (b = c.length; a < b; ++a)
            c[a] = e[a + 32768];
    } this.g.push(c); this.l += c.length; if (t)
        e.set(e.subarray(d, d + 32768));
    else
        for (a = 0; 32768 > a; ++a)
            e[a] = e[d + a]; this.b = 32768; return e; };
    w.prototype.z = function (c) { var d, a = this.input.length / this.a + 1 | 0, b, e, f, g = this.input, h = this.c; c && ("number" === typeof c.p && (a = c.p), "number" === typeof c.u && (a += c.u)); 2 > a ? (b = (g.length - this.a) / this.o[2], f = 258 * (b / 2) | 0, e = f < h.length ? h.length + f : h.length << 1) : e = h.length * a; t ? (d = new Uint8Array(e), d.set(h)) : d = h; return this.c = d; };
    w.prototype.n = function () { var c = 0, d = this.c, a = this.g, b, e = new (t ? Uint8Array : Array)(this.l + (this.b - 32768)), f, g, h, k; if (0 === a.length)
        return t ? this.c.subarray(32768, this.b) : this.c.slice(32768, this.b); f = 0; for (g = a.length; f < g; ++f) {
        b = a[f];
        h = 0;
        for (k = b.length; h < k; ++h)
            e[c++] = b[h];
    } f = 32768; for (g = this.b; f < g; ++f)
        e[c++] = d[f]; this.g = []; return this.buffer = e; };
    w.prototype.v = function () { var c, d = this.b; t ? this.r ? (c = new Uint8Array(d), c.set(this.c.subarray(0, d))) : c = this.c.subarray(0, d) : (this.c.length > d && (this.c.length = d), c = this.c); return this.buffer = c; };
    function W(c, d) { var a, b; this.input = c; this.a = 0; if (d || !(d = {}))
        d.index && (this.a = d.index), d.verify && (this.A = d.verify); a = c[this.a++]; b = c[this.a++]; switch (a & 15) {
        case ha:
            this.method = ha;
            break;
        default: throw Error("unsupported compression method");
    } if (0 !== ((a << 8) + b) % 31)
        throw Error("invalid fcheck flag:" + ((a << 8) + b) % 31); if (b & 32)
        throw Error("fdict flag is not supported"); this.q = new w(c, { index: this.a, bufferSize: d.bufferSize, bufferType: d.bufferType, resize: d.resize }); }
    W.prototype.k = function () { var c = this.input, d, a; d = this.q.k(); this.a = this.q.a; if (this.A) {
        a = (c[this.a++] << 24 | c[this.a++] << 16 | c[this.a++] << 8 | c[this.a++]) >>> 0;
        var b = d;
        if ("string" === typeof b) {
            var e = b.split(""), f, g;
            f = 0;
            for (g = e.length; f < g; f++)
                e[f] = (e[f].charCodeAt(0) & 255) >>> 0;
            b = e;
        }
        for (var h = 1, k = 0, m = b.length, n, p = 0; 0 < m;) {
            n = 1024 < m ? 1024 : m;
            m -= n;
            do
                h += b[p++], k += h;
            while (--n);
            h %= 65521;
            k %= 65521;
        }
        if (a !== (k << 16 | h) >>> 0)
            throw Error("invalid adler-32 checksum");
    } return d; };
    var ha = 8;
    r("Zlib.Inflate", W);
    r("Zlib.Inflate.prototype.decompress", W.prototype.k);
    var X = { ADAPTIVE: B.s, BLOCK: B.t }, Y, Z, $, ia;
    if (Object.keys)
        Y = Object.keys(X);
    else
        for (Z in Y = [], $ = 0, X)
            Y[$++] = Z;
    $ = 0;
    for (ia = Y.length; $ < ia; ++$)
        Z = Y[$], r("Zlib.Inflate.BufferType." + Z, X[Z]);
}).call(this);
!function (t) { if ("object" == typeof exports && "undefined" != typeof module)
    module.exports = t();
else if ("function" == typeof define && define.amd)
    define([], t);
else {
    var r;
    r = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, r.msgpack = t();
} }(function () {
    return function t(r, e, n) { function i(f, u) { if (!e[f]) {
        if (!r[f]) {
            var a = "function" == typeof require && require;
            if (!u && a)
                return a(f, !0);
            if (o)
                return o(f, !0);
            var s = new Error("Cannot find module '" + f + "'");
            throw s.code = "MODULE_NOT_FOUND", s;
        }
        var c = e[f] = { exports: {} };
        r[f][0].call(c.exports, function (t) { var e = r[f][1][t]; return i(e ? e : t); }, c, c.exports, t, r, e, n);
    } return e[f].exports; } for (var o = "function" == typeof require && require, f = 0; f < n.length; f++)
        i(n[f]); return i; }({ 1: [function (t, r, e) { e.encode = t("./encode").encode, e.decode = t("./decode").decode, e.Encoder = t("./encoder").Encoder, e.Decoder = t("./decoder").Decoder, e.createCodec = t("./ext").createCodec, e.codec = t("./codec").codec; }, { "./codec": 10, "./decode": 12, "./decoder": 13, "./encode": 15, "./encoder": 16, "./ext": 20 }], 2: [function (t, r, e) { (function (Buffer) { function t(t) { return t && t.isBuffer && t; } r.exports = t("undefined" != typeof Buffer && Buffer) || t(this.Buffer) || t("undefined" != typeof window && window.Buffer) || this.Buffer; }).call(this, t("buffer").Buffer); }, { buffer: 29 }], 3: [function (t, r, e) { function n(t, r) { for (var e = this, n = r || (r |= 0), i = t.length, o = 0, f = 0; f < i;)
                o = t.charCodeAt(f++), o < 128 ? e[n++] = o : o < 2048 ? (e[n++] = 192 | o >>> 6, e[n++] = 128 | 63 & o) : o < 55296 || o > 57343 ? (e[n++] = 224 | o >>> 12, e[n++] = 128 | o >>> 6 & 63, e[n++] = 128 | 63 & o) : (o = (o - 55296 << 10 | t.charCodeAt(f++) - 56320) + 65536, e[n++] = 240 | o >>> 18, e[n++] = 128 | o >>> 12 & 63, e[n++] = 128 | o >>> 6 & 63, e[n++] = 128 | 63 & o); return n - r; } function i(t, r, e) { var n = this, i = 0 | r; e || (e = n.length); for (var o = "", f = 0; i < e;)
                f = n[i++], f < 128 ? o += String.fromCharCode(f) : (192 === (224 & f) ? f = (31 & f) << 6 | 63 & n[i++] : 224 === (240 & f) ? f = (15 & f) << 12 | (63 & n[i++]) << 6 | 63 & n[i++] : 240 === (248 & f) && (f = (7 & f) << 18 | (63 & n[i++]) << 12 | (63 & n[i++]) << 6 | 63 & n[i++]), f >= 65536 ? (f -= 65536, o += String.fromCharCode((f >>> 10) + 55296, (1023 & f) + 56320)) : o += String.fromCharCode(f)); return o; } function o(t, r, e, n) { var i; e || (e = 0), n || 0 === n || (n = this.length), r || (r = 0); var o = n - e; if (t === this && e < r && r < n)
                for (i = o - 1; i >= 0; i--)
                    t[i + r] = this[i + e];
            else
                for (i = 0; i < o; i++)
                    t[i + r] = this[i + e]; return o; } e.copy = o, e.toString = i, e.write = n; }, {}], 4: [function (t, r, e) { function n(t) { return new Array(t); } function i(t) { if (!o.isBuffer(t) && o.isView(t))
                t = o.Uint8Array.from(t);
            else if (o.isArrayBuffer(t))
                t = new Uint8Array(t);
            else {
                if ("string" == typeof t)
                    return o.from.call(e, t);
                if ("number" == typeof t)
                    throw new TypeError('"value" argument must not be a number');
            } return Array.prototype.slice.call(t); } var o = t("./bufferish"), e = r.exports = n(0); e.alloc = n, e.concat = o.concat, e.from = i; }, { "./bufferish": 8 }], 5: [function (t, r, e) { function n(t) { return new Buffer(t); } function i(t) { if (!o.isBuffer(t) && o.isView(t))
                t = o.Uint8Array.from(t);
            else if (o.isArrayBuffer(t))
                t = new Uint8Array(t);
            else {
                if ("string" == typeof t)
                    return o.from.call(e, t);
                if ("number" == typeof t)
                    throw new TypeError('"value" argument must not be a number');
            } return Buffer.from && 1 !== Buffer.from.length ? Buffer.from(t) : new Buffer(t); } var o = t("./bufferish"), Buffer = o.global, e = r.exports = o.hasBuffer ? n(0) : []; e.alloc = o.hasBuffer && Buffer.alloc || n, e.concat = o.concat, e.from = i; }, { "./bufferish": 8 }], 6: [function (t, r, e) { function n(t, r, e, n) { var o = a.isBuffer(this), f = a.isBuffer(t); if (o && f)
                return this.copy(t, r, e, n); if (c || o || f || !a.isView(this) || !a.isView(t))
                return u.copy.call(this, t, r, e, n); var s = e || null != n ? i.call(this, e, n) : this; return t.set(s, r), s.length; } function i(t, r) { var e = this.slice || !c && this.subarray; if (e)
                return e.call(this, t, r); var i = a.alloc.call(this, r - t); return n.call(this, i, 0, t, r), i; } function o(t, r, e) { var n = !s && a.isBuffer(this) ? this.toString : u.toString; return n.apply(this, arguments); } function f(t) { function r() { var r = this[t] || u[t]; return r.apply(this, arguments); } return r; } var u = t("./buffer-lite"); e.copy = n, e.slice = i, e.toString = o, e.write = f("write"); var a = t("./bufferish"), Buffer = a.global, s = a.hasBuffer && "TYPED_ARRAY_SUPPORT" in Buffer, c = s && !Buffer.TYPED_ARRAY_SUPPORT; }, { "./buffer-lite": 3, "./bufferish": 8 }], 7: [function (t, r, e) { function n(t) { return new Uint8Array(t); } function i(t) { if (o.isView(t)) {
                var r = t.byteOffset, n = t.byteLength;
                t = t.buffer, t.byteLength !== n && (t.slice ? t = t.slice(r, r + n) : (t = new Uint8Array(t), t.byteLength !== n && (t = Array.prototype.slice.call(t, r, r + n))));
            }
            else {
                if ("string" == typeof t)
                    return o.from.call(e, t);
                if ("number" == typeof t)
                    throw new TypeError('"value" argument must not be a number');
            } return new Uint8Array(t); } var o = t("./bufferish"), e = r.exports = o.hasArrayBuffer ? n(0) : []; e.alloc = n, e.concat = o.concat, e.from = i; }, { "./bufferish": 8 }], 8: [function (t, r, e) { function n(t) { return "string" == typeof t ? u.call(this, t) : a(this).from(t); } function i(t) { return a(this).alloc(t); } function o(t, r) { function n(t) { r += t.length; } function o(t) { a += w.copy.call(t, u, a); } r || (r = 0, Array.prototype.forEach.call(t, n)); var f = this !== e && this || t[0], u = i.call(f, r), a = 0; return Array.prototype.forEach.call(t, o), u; } function f(t) { return t instanceof ArrayBuffer || E(t); } function u(t) { var r = 3 * t.length, e = i.call(this, r), n = w.write.call(e, t); return r !== n && (e = w.slice.call(e, 0, n)), e; } function a(t) { return d(t) ? g : y(t) ? b : p(t) ? v : h ? g : l ? b : v; } function s() { return !1; } function c(t, r) { return t = "[object " + t + "]", function (e) { return null != e && {}.toString.call(r ? e[r] : e) === t; }; } var Buffer = e.global = t("./buffer-global"), h = e.hasBuffer = Buffer && !!Buffer.isBuffer, l = e.hasArrayBuffer = "undefined" != typeof ArrayBuffer, p = e.isArray = t("isarray"); e.isArrayBuffer = l ? f : s; var d = e.isBuffer = h ? Buffer.isBuffer : s, y = e.isView = l ? ArrayBuffer.isView || c("ArrayBuffer", "buffer") : s; e.alloc = i, e.concat = o, e.from = n; var v = e.Array = t("./bufferish-array"), g = e.Buffer = t("./bufferish-buffer"), b = e.Uint8Array = t("./bufferish-uint8array"), w = e.prototype = t("./bufferish-proto"), E = c("ArrayBuffer"); }, { "./buffer-global": 2, "./bufferish-array": 4, "./bufferish-buffer": 5, "./bufferish-proto": 6, "./bufferish-uint8array": 7, isarray: 34 }], 9: [function (t, r, e) { function n(t) { return this instanceof n ? (this.options = t, void this.init()) : new n(t); } function i(t) { for (var r in t)
                n.prototype[r] = o(n.prototype[r], t[r]); } function o(t, r) { function e() { return t.apply(this, arguments), r.apply(this, arguments); } return t && r ? e : t || r; } function f(t) { function r(t, r) { return r(t); } return t = t.slice(), function (e) { return t.reduce(r, e); }; } function u(t) { return s(t) ? f(t) : t; } function a(t) { return new n(t); } var s = t("isarray"); e.createCodec = a, e.install = i, e.filter = u; var c = t("./bufferish"); n.prototype.init = function () { var t = this.options; return t && t.uint8array && (this.bufferish = c.Uint8Array), this; }, e.preset = a({ preset: !0 }); }, { "./bufferish": 8, isarray: 34 }], 10: [function (t, r, e) { t("./read-core"), t("./write-core"), e.codec = { preset: t("./codec-base").preset }; }, { "./codec-base": 9, "./read-core": 22, "./write-core": 25 }], 11: [function (t, r, e) { function n(t) { if (!(this instanceof n))
                return new n(t); if (t && (this.options = t, t.codec)) {
                var r = this.codec = t.codec;
                r.bufferish && (this.bufferish = r.bufferish);
            } } e.DecodeBuffer = n; var i = t("./read-core").preset, o = t("./flex-buffer").FlexDecoder; o.mixin(n.prototype), n.prototype.codec = i, n.prototype.fetch = function () { return this.codec.decode(this); }; }, { "./flex-buffer": 21, "./read-core": 22 }], 12: [function (t, r, e) { function n(t, r) { var e = new i(r); return e.write(t), e.read(); } e.decode = n; var i = t("./decode-buffer").DecodeBuffer; }, { "./decode-buffer": 11 }], 13: [function (t, r, e) { function n(t) { return this instanceof n ? void o.call(this, t) : new n(t); } e.Decoder = n; var i = t("event-lite"), o = t("./decode-buffer").DecodeBuffer; n.prototype = new o, i.mixin(n.prototype), n.prototype.decode = function (t) { arguments.length && this.write(t), this.flush(); }, n.prototype.push = function (t) { this.emit("data", t); }, n.prototype.end = function (t) { this.decode(t), this.emit("end"); }; }, { "./decode-buffer": 11, "event-lite": 31 }], 14: [function (t, r, e) { function n(t) { if (!(this instanceof n))
                return new n(t); if (t && (this.options = t, t.codec)) {
                var r = this.codec = t.codec;
                r.bufferish && (this.bufferish = r.bufferish);
            } } e.EncodeBuffer = n; var i = t("./write-core").preset, o = t("./flex-buffer").FlexEncoder; o.mixin(n.prototype), n.prototype.codec = i, n.prototype.write = function (t) { this.codec.encode(this, t); }; }, { "./flex-buffer": 21, "./write-core": 25 }], 15: [function (t, r, e) { function n(t, r) { var e = new i(r); return e.write(t), e.read(); } e.encode = n; var i = t("./encode-buffer").EncodeBuffer; }, { "./encode-buffer": 14 }], 16: [function (t, r, e) { function n(t) { return this instanceof n ? void o.call(this, t) : new n(t); } e.Encoder = n; var i = t("event-lite"), o = t("./encode-buffer").EncodeBuffer; n.prototype = new o, i.mixin(n.prototype), n.prototype.encode = function (t) { this.write(t), this.emit("data", this.read()); }, n.prototype.end = function (t) { arguments.length && this.encode(t), this.flush(), this.emit("end"); }; }, { "./encode-buffer": 14, "event-lite": 31 }], 17: [function (t, r, e) { function n(t, r) { return this instanceof n ? (this.buffer = i.from(t), void (this.type = r)) : new n(t, r); } e.ExtBuffer = n; var i = t("./bufferish"); }, { "./bufferish": 8 }], 18: [function (t, r, e) { function n(t) { t.addExtPacker(14, Error, [u, i]), t.addExtPacker(1, EvalError, [u, i]), t.addExtPacker(2, RangeError, [u, i]), t.addExtPacker(3, ReferenceError, [u, i]), t.addExtPacker(4, SyntaxError, [u, i]), t.addExtPacker(5, TypeError, [u, i]), t.addExtPacker(6, URIError, [u, i]), t.addExtPacker(10, RegExp, [f, i]), t.addExtPacker(11, Boolean, [o, i]), t.addExtPacker(12, String, [o, i]), t.addExtPacker(13, Date, [Number, i]), t.addExtPacker(15, Number, [o, i]), "undefined" != typeof Uint8Array && (t.addExtPacker(17, Int8Array, c), t.addExtPacker(18, Uint8Array, c), t.addExtPacker(19, Int16Array, c), t.addExtPacker(20, Uint16Array, c), t.addExtPacker(21, Int32Array, c), t.addExtPacker(22, Uint32Array, c), t.addExtPacker(23, Float32Array, c), "undefined" != typeof Float64Array && t.addExtPacker(24, Float64Array, c), "undefined" != typeof Uint8ClampedArray && t.addExtPacker(25, Uint8ClampedArray, c), t.addExtPacker(26, ArrayBuffer, c), t.addExtPacker(29, DataView, c)), s.hasBuffer && t.addExtPacker(27, Buffer, s.from); } function i(r) { return a || (a = t("./encode").encode), a(r); } function o(t) { return t.valueOf(); } function f(t) { t = RegExp.prototype.toString.call(t).split("/"), t.shift(); var r = [t.pop()]; return r.unshift(t.join("/")), r; } function u(t) { var r = {}; for (var e in h)
                r[e] = t[e]; return r; } e.setExtPackers = n; var a, s = t("./bufferish"), Buffer = s.global, c = s.Uint8Array.from, h = { name: 1, message: 1, stack: 1, columnNumber: 1, fileName: 1, lineNumber: 1 }; }, { "./bufferish": 8, "./encode": 15 }], 19: [function (t, r, e) { function n(t) { t.addExtUnpacker(14, [i, f(Error)]), t.addExtUnpacker(1, [i, f(EvalError)]), t.addExtUnpacker(2, [i, f(RangeError)]), t.addExtUnpacker(3, [i, f(ReferenceError)]), t.addExtUnpacker(4, [i, f(SyntaxError)]), t.addExtUnpacker(5, [i, f(TypeError)]), t.addExtUnpacker(6, [i, f(URIError)]), t.addExtUnpacker(10, [i, o]), t.addExtUnpacker(11, [i, u(Boolean)]), t.addExtUnpacker(12, [i, u(String)]), t.addExtUnpacker(13, [i, u(Date)]), t.addExtUnpacker(15, [i, u(Number)]), "undefined" != typeof Uint8Array && (t.addExtUnpacker(17, u(Int8Array)), t.addExtUnpacker(18, u(Uint8Array)), t.addExtUnpacker(19, [a, u(Int16Array)]), t.addExtUnpacker(20, [a, u(Uint16Array)]), t.addExtUnpacker(21, [a, u(Int32Array)]), t.addExtUnpacker(22, [a, u(Uint32Array)]), t.addExtUnpacker(23, [a, u(Float32Array)]), "undefined" != typeof Float64Array && t.addExtUnpacker(24, [a, u(Float64Array)]), "undefined" != typeof Uint8ClampedArray && t.addExtUnpacker(25, u(Uint8ClampedArray)), t.addExtUnpacker(26, a), t.addExtUnpacker(29, [a, u(DataView)])), c.hasBuffer && t.addExtUnpacker(27, u(Buffer)); } function i(r) { return s || (s = t("./decode").decode), s(r); } function o(t) { return RegExp.apply(null, t); } function f(t) { return function (r) { var e = new t; for (var n in h)
                e[n] = r[n]; return e; }; } function u(t) { return function (r) { return new t(r); }; } function a(t) { return new Uint8Array(t).buffer; } e.setExtUnpackers = n; var s, c = t("./bufferish"), Buffer = c.global, h = { name: 1, message: 1, stack: 1, columnNumber: 1, fileName: 1, lineNumber: 1 }; }, { "./bufferish": 8, "./decode": 12 }], 20: [function (t, r, e) { t("./read-core"), t("./write-core"), e.createCodec = t("./codec-base").createCodec; }, { "./codec-base": 9, "./read-core": 22, "./write-core": 25 }], 21: [function (t, r, e) { function n() { if (!(this instanceof n))
                return new n; } function i() { if (!(this instanceof i))
                return new i; } function o() { function t(t) { var r = this.offset ? p.prototype.slice.call(this.buffer, this.offset) : this.buffer; this.buffer = r ? t ? this.bufferish.concat([r, t]) : r : t, this.offset = 0; } function r() { for (; this.offset < this.buffer.length;) {
                var t, r = this.offset;
                try {
                    t = this.fetch();
                }
                catch (t) {
                    if (t && t.message != v)
                        throw t;
                    this.offset = r;
                    break;
                }
                this.push(t);
            } } function e(t) { var r = this.offset, e = r + t; if (e > this.buffer.length)
                throw new Error(v); return this.offset = e, r; } return { bufferish: p, write: t, fetch: a, flush: r, push: c, pull: h, read: s, reserve: e, offset: 0 }; } function f() { function t() { var t = this.start; if (t < this.offset) {
                var r = this.start = this.offset;
                return p.prototype.slice.call(this.buffer, t, r);
            } } function r() { for (; this.start < this.offset;) {
                var t = this.fetch();
                t && this.push(t);
            } } function e() { var t = this.buffers || (this.buffers = []), r = t.length > 1 ? this.bufferish.concat(t) : t[0]; return t.length = 0, r; } function n(t) { var r = 0 | t; if (this.buffer) {
                var e = this.buffer.length, n = 0 | this.offset, i = n + r;
                if (i < e)
                    return this.offset = i, n;
                this.flush(), t = Math.max(t, Math.min(2 * e, this.maxBufferSize));
            } return t = Math.max(t, this.minBufferSize), this.buffer = this.bufferish.alloc(t), this.start = 0, this.offset = r, 0; } function i(t) { var r = t.length; if (r > this.minBufferSize)
                this.flush(), this.push(t);
            else {
                var e = this.reserve(r);
                p.prototype.copy.call(t, this.buffer, e);
            } } return { bufferish: p, write: u, fetch: t, flush: r, push: c, pull: e, read: s, reserve: n, send: i, maxBufferSize: y, minBufferSize: d, offset: 0, start: 0 }; } function u() { throw new Error("method not implemented: write()"); } function a() { throw new Error("method not implemented: fetch()"); } function s() { var t = this.buffers && this.buffers.length; return t ? (this.flush(), this.pull()) : this.fetch(); } function c(t) { var r = this.buffers || (this.buffers = []); r.push(t); } function h() { var t = this.buffers || (this.buffers = []); return t.shift(); } function l(t) { function r(r) { for (var e in t)
                r[e] = t[e]; return r; } return r; } e.FlexDecoder = n, e.FlexEncoder = i; var p = t("./bufferish"), d = 2048, y = 65536, v = "BUFFER_SHORTAGE"; n.mixin = l(o()), n.mixin(n.prototype), i.mixin = l(f()), i.mixin(i.prototype); }, { "./bufferish": 8 }], 22: [function (t, r, e) { function n(t) { function r(t) { var r = s(t), n = e[r]; if (!n)
                throw new Error("Invalid type: " + (r ? "0x" + r.toString(16) : r)); return n(t); } var e = c.getReadToken(t); return r; } function i() { var t = this.options; return this.decode = n(t), t && t.preset && a.setExtUnpackers(this), this; } function o(t, r) { var e = this.extUnpackers || (this.extUnpackers = []); e[t] = h.filter(r); } function f(t) { function r(r) { return new u(r, t); } var e = this.extUnpackers || (this.extUnpackers = []); return e[t] || r; } var u = t("./ext-buffer").ExtBuffer, a = t("./ext-unpacker"), s = t("./read-format").readUint8, c = t("./read-token"), h = t("./codec-base"); h.install({ addExtUnpacker: o, getExtUnpacker: f, init: i }), e.preset = i.call(h.preset); }, { "./codec-base": 9, "./ext-buffer": 17, "./ext-unpacker": 19, "./read-format": 23, "./read-token": 24 }], 23: [function (t, r, e) { function n(t) { var r = k.hasArrayBuffer && t && t.binarraybuffer, e = t && t.int64, n = T && t && t.usemap, B = { map: n ? o : i, array: f, str: u, bin: r ? s : a, ext: c, uint8: h, uint16: p, uint32: y, uint64: g(8, e ? E : b), int8: l, int16: d, int32: v, int64: g(8, e ? A : w), float32: g(4, m), float64: g(8, x) }; return B; } function i(t, r) { var e, n = {}, i = new Array(r), o = new Array(r), f = t.codec.decode; for (e = 0; e < r; e++)
                i[e] = f(t), o[e] = f(t); for (e = 0; e < r; e++)
                n[i[e]] = o[e]; return n; } function o(t, r) { var e, n = new Map, i = new Array(r), o = new Array(r), f = t.codec.decode; for (e = 0; e < r; e++)
                i[e] = f(t), o[e] = f(t); for (e = 0; e < r; e++)
                n.set(i[e], o[e]); return n; } function f(t, r) { for (var e = new Array(r), n = t.codec.decode, i = 0; i < r; i++)
                e[i] = n(t); return e; } function u(t, r) { var e = t.reserve(r), n = e + r; return _.toString.call(t.buffer, "utf-8", e, n); } function a(t, r) { var e = t.reserve(r), n = e + r, i = _.slice.call(t.buffer, e, n); return k.from(i); } function s(t, r) { var e = t.reserve(r), n = e + r, i = _.slice.call(t.buffer, e, n); return k.Uint8Array.from(i).buffer; } function c(t, r) { var e = t.reserve(r + 1), n = t.buffer[e++], i = e + r, o = t.codec.getExtUnpacker(n); if (!o)
                throw new Error("Invalid ext type: " + (n ? "0x" + n.toString(16) : n)); var f = _.slice.call(t.buffer, e, i); return o(f); } function h(t) { var r = t.reserve(1); return t.buffer[r]; } function l(t) { var r = t.reserve(1), e = t.buffer[r]; return 128 & e ? e - 256 : e; } function p(t) { var r = t.reserve(2), e = t.buffer; return e[r++] << 8 | e[r]; } function d(t) { var r = t.reserve(2), e = t.buffer, n = e[r++] << 8 | e[r]; return 32768 & n ? n - 65536 : n; } function y(t) { var r = t.reserve(4), e = t.buffer; return 16777216 * e[r++] + (e[r++] << 16) + (e[r++] << 8) + e[r]; } function v(t) { var r = t.reserve(4), e = t.buffer; return e[r++] << 24 | e[r++] << 16 | e[r++] << 8 | e[r]; } function g(t, r) { return function (e) { var n = e.reserve(t); return r.call(e.buffer, n, S); }; } function b(t) { return new P(this, t).toNumber(); } function w(t) { return new R(this, t).toNumber(); } function E(t) { return new P(this, t); } function A(t) { return new R(this, t); } function m(t) { return B.read(this, t, !1, 23, 4); } function x(t) { return B.read(this, t, !1, 52, 8); } var B = t("ieee754"), U = t("int64-buffer"), P = U.Uint64BE, R = U.Int64BE; e.getReadFormat = n, e.readUint8 = h; var k = t("./bufferish"), _ = t("./bufferish-proto"), T = "undefined" != typeof Map, S = !0; }, { "./bufferish": 8, "./bufferish-proto": 6, ieee754: 32, "int64-buffer": 33 }], 24: [function (t, r, e) { function n(t) { var r = s.getReadFormat(t); return t && t.useraw ? o(r) : i(r); } function i(t) { var r, e = new Array(256); for (r = 0; r <= 127; r++)
                e[r] = f(r); for (r = 128; r <= 143; r++)
                e[r] = a(r - 128, t.map); for (r = 144; r <= 159; r++)
                e[r] = a(r - 144, t.array); for (r = 160; r <= 191; r++)
                e[r] = a(r - 160, t.str); for (e[192] = f(null), e[193] = null, e[194] = f(!1), e[195] = f(!0), e[196] = u(t.uint8, t.bin), e[197] = u(t.uint16, t.bin), e[198] = u(t.uint32, t.bin), e[199] = u(t.uint8, t.ext), e[200] = u(t.uint16, t.ext), e[201] = u(t.uint32, t.ext), e[202] = t.float32, e[203] = t.float64, e[204] = t.uint8, e[205] = t.uint16, e[206] = t.uint32, e[207] = t.uint64, e[208] = t.int8, e[209] = t.int16, e[210] = t.int32, e[211] = t.int64, e[212] = a(1, t.ext), e[213] = a(2, t.ext), e[214] = a(4, t.ext), e[215] = a(8, t.ext), e[216] = a(16, t.ext), e[217] = u(t.uint8, t.str), e[218] = u(t.uint16, t.str), e[219] = u(t.uint32, t.str), e[220] = u(t.uint16, t.array), e[221] = u(t.uint32, t.array), e[222] = u(t.uint16, t.map), e[223] = u(t.uint32, t.map), r = 224; r <= 255; r++)
                e[r] = f(r - 256); return e; } function o(t) { var r, e = i(t).slice(); for (e[217] = e[196], e[218] = e[197], e[219] = e[198], r = 160; r <= 191; r++)
                e[r] = a(r - 160, t.bin); return e; } function f(t) { return function () { return t; }; } function u(t, r) { return function (e) { var n = t(e); return r(e, n); }; } function a(t, r) { return function (e) { return r(e, t); }; } var s = t("./read-format"); e.getReadToken = n; }, { "./read-format": 23 }], 25: [function (t, r, e) { function n(t) { function r(t, r) { var n = e[typeof r]; if (!n)
                throw new Error('Unsupported type "' + typeof r + '": ' + r); n(t, r); } var e = s.getWriteType(t); return r; } function i() { var t = this.options; return this.encode = n(t), t && t.preset && a.setExtPackers(this), this; } function o(t, r, e) { function n(r) { return e && (r = e(r)), new u(r, t); } e = c.filter(e); var i = r.name; if (i && "Object" !== i) {
                var o = this.extPackers || (this.extPackers = {});
                o[i] = n;
            }
            else {
                var f = this.extEncoderList || (this.extEncoderList = []);
                f.unshift([r, n]);
            } } function f(t) { var r = this.extPackers || (this.extPackers = {}), e = t.constructor, n = e && e.name && r[e.name]; if (n)
                return n; for (var i = this.extEncoderList || (this.extEncoderList = []), o = i.length, f = 0; f < o; f++) {
                var u = i[f];
                if (e === u[0])
                    return u[1];
            } } var u = t("./ext-buffer").ExtBuffer, a = t("./ext-packer"), s = t("./write-type"), c = t("./codec-base"); c.install({ addExtPacker: o, getExtPacker: f, init: i }), e.preset = i.call(c.preset); }, { "./codec-base": 9, "./ext-buffer": 17, "./ext-packer": 18, "./write-type": 27 }], 26: [function (t, r, e) { function n(t) { return t && t.uint8array ? i() : m || E.hasBuffer && t && t.safe ? f() : o(); } function i() { var t = o(); return t[202] = c(202, 4, p), t[203] = c(203, 8, d), t; } function o() { var t = w.slice(); return t[196] = u(196), t[197] = a(197), t[198] = s(198), t[199] = u(199), t[200] = a(200), t[201] = s(201), t[202] = c(202, 4, x.writeFloatBE || p, !0), t[203] = c(203, 8, x.writeDoubleBE || d, !0), t[204] = u(204), t[205] = a(205), t[206] = s(206), t[207] = c(207, 8, h), t[208] = u(208), t[209] = a(209), t[210] = s(210), t[211] = c(211, 8, l), t[217] = u(217), t[218] = a(218), t[219] = s(219), t[220] = a(220), t[221] = s(221), t[222] = a(222), t[223] = s(223), t; } function f() { var t = w.slice(); return t[196] = c(196, 1, Buffer.prototype.writeUInt8), t[197] = c(197, 2, Buffer.prototype.writeUInt16BE), t[198] = c(198, 4, Buffer.prototype.writeUInt32BE), t[199] = c(199, 1, Buffer.prototype.writeUInt8), t[200] = c(200, 2, Buffer.prototype.writeUInt16BE), t[201] = c(201, 4, Buffer.prototype.writeUInt32BE), t[202] = c(202, 4, Buffer.prototype.writeFloatBE), t[203] = c(203, 8, Buffer.prototype.writeDoubleBE), t[204] = c(204, 1, Buffer.prototype.writeUInt8), t[205] = c(205, 2, Buffer.prototype.writeUInt16BE), t[206] = c(206, 4, Buffer.prototype.writeUInt32BE), t[207] = c(207, 8, h), t[208] = c(208, 1, Buffer.prototype.writeInt8), t[209] = c(209, 2, Buffer.prototype.writeInt16BE), t[210] = c(210, 4, Buffer.prototype.writeInt32BE), t[211] = c(211, 8, l), t[217] = c(217, 1, Buffer.prototype.writeUInt8), t[218] = c(218, 2, Buffer.prototype.writeUInt16BE), t[219] = c(219, 4, Buffer.prototype.writeUInt32BE), t[220] = c(220, 2, Buffer.prototype.writeUInt16BE), t[221] = c(221, 4, Buffer.prototype.writeUInt32BE), t[222] = c(222, 2, Buffer.prototype.writeUInt16BE), t[223] = c(223, 4, Buffer.prototype.writeUInt32BE), t; } function u(t) { return function (r, e) { var n = r.reserve(2), i = r.buffer; i[n++] = t, i[n] = e; }; } function a(t) { return function (r, e) { var n = r.reserve(3), i = r.buffer; i[n++] = t, i[n++] = e >>> 8, i[n] = e; }; } function s(t) { return function (r, e) { var n = r.reserve(5), i = r.buffer; i[n++] = t, i[n++] = e >>> 24, i[n++] = e >>> 16, i[n++] = e >>> 8, i[n] = e; }; } function c(t, r, e, n) { return function (i, o) { var f = i.reserve(r + 1); i.buffer[f++] = t, e.call(i.buffer, o, f, n); }; } function h(t, r) { new g(this, r, t); } function l(t, r) { new b(this, r, t); } function p(t, r) { y.write(this, t, r, !1, 23, 4); } function d(t, r) { y.write(this, t, r, !1, 52, 8); } var y = t("ieee754"), v = t("int64-buffer"), g = v.Uint64BE, b = v.Int64BE, w = t("./write-uint8").uint8, E = t("./bufferish"), Buffer = E.global, A = E.hasBuffer && "TYPED_ARRAY_SUPPORT" in Buffer, m = A && !Buffer.TYPED_ARRAY_SUPPORT, x = E.hasBuffer && Buffer.prototype || {}; e.getWriteToken = n; }, { "./bufferish": 8, "./write-uint8": 28, ieee754: 32, "int64-buffer": 33 }], 27: [function (t, r, e) { function n(t) { function r(t, r) { var e = r ? 195 : 194; _[e](t, r); } function e(t, r) { var e, n = 0 | r; return r !== n ? (e = 203, void _[e](t, r)) : (e = -32 <= n && n <= 127 ? 255 & n : 0 <= n ? n <= 255 ? 204 : n <= 65535 ? 205 : 206 : -128 <= n ? 208 : -32768 <= n ? 209 : 210, void _[e](t, n)); } function n(t, r) { var e = 207; _[e](t, r.toArray()); } function o(t, r) { var e = 211; _[e](t, r.toArray()); } function v(t) { return t < 32 ? 1 : t <= 255 ? 2 : t <= 65535 ? 3 : 5; } function g(t) { return t < 32 ? 1 : t <= 65535 ? 3 : 5; } function b(t) { function r(r, e) { var n = e.length, i = 5 + 3 * n; r.offset = r.reserve(i); var o = r.buffer, f = t(n), u = r.offset + f; n = s.write.call(o, e, u); var a = t(n); if (f !== a) {
                var c = u + a - f, h = u + n;
                s.copy.call(o, o, c, u, h);
            } var l = 1 === a ? 160 + n : a <= 3 ? 215 + a : 219; _[l](r, n), r.offset += n; } return r; } function w(t, r) { if (null === r)
                return A(t, r); if (I(r))
                return Y(t, r); if (i(r))
                return m(t, r); if (f.isUint64BE(r))
                return n(t, r); if (u.isInt64BE(r))
                return o(t, r); var e = t.codec.getExtPacker(r); return e && (r = e(r)), r instanceof l ? U(t, r) : void D(t, r); } function E(t, r) { return I(r) ? k(t, r) : void w(t, r); } function A(t, r) { var e = 192; _[e](t, r); } function m(t, r) { var e = r.length, n = e < 16 ? 144 + e : e <= 65535 ? 220 : 221; _[n](t, e); for (var i = t.codec.encode, o = 0; o < e; o++)
                i(t, r[o]); } function x(t, r) { var e = r.length, n = e < 255 ? 196 : e <= 65535 ? 197 : 198; _[n](t, e), t.send(r); } function B(t, r) { x(t, new Uint8Array(r)); } function U(t, r) { var e = r.buffer, n = e.length, i = y[n] || (n < 255 ? 199 : n <= 65535 ? 200 : 201); _[i](t, n), h[r.type](t), t.send(e); } function P(t, r) { var e = Object.keys(r), n = e.length, i = n < 16 ? 128 + n : n <= 65535 ? 222 : 223; _[i](t, n); var o = t.codec.encode; e.forEach(function (e) { o(t, e), o(t, r[e]); }); } function R(t, r) { if (!(r instanceof Map))
                return P(t, r); var e = r.size, n = e < 16 ? 128 + e : e <= 65535 ? 222 : 223; _[n](t, e); var i = t.codec.encode; r.forEach(function (r, e, n) { i(t, e), i(t, r); }); } function k(t, r) { var e = r.length, n = e < 32 ? 160 + e : e <= 65535 ? 218 : 219; _[n](t, e), t.send(r); } var _ = c.getWriteToken(t), T = t && t.useraw, S = p && t && t.binarraybuffer, I = S ? a.isArrayBuffer : a.isBuffer, Y = S ? B : x, C = d && t && t.usemap, D = C ? R : P, O = { boolean: r, function: A, number: e, object: T ? E : w, string: b(T ? g : v), symbol: A, undefined: A }; return O; } var i = t("isarray"), o = t("int64-buffer"), f = o.Uint64BE, u = o.Int64BE, a = t("./bufferish"), s = t("./bufferish-proto"), c = t("./write-token"), h = t("./write-uint8").uint8, l = t("./ext-buffer").ExtBuffer, p = "undefined" != typeof Uint8Array, d = "undefined" != typeof Map, y = []; y[1] = 212, y[2] = 213, y[4] = 214, y[8] = 215, y[16] = 216, e.getWriteType = n; }, { "./bufferish": 8, "./bufferish-proto": 6, "./ext-buffer": 17, "./write-token": 26, "./write-uint8": 28, "int64-buffer": 33, isarray: 34 }], 28: [function (t, r, e) { function n(t) { return function (r) { var e = r.reserve(1); r.buffer[e] = t; }; } for (var i = e.uint8 = new Array(256), o = 0; o <= 255; o++)
                i[o] = n(o); }, {}], 29: [function (t, r, e) {
                (function (r) {
                    "use strict";
                    function n() { try {
                        var t = new Uint8Array(1);
                        return t.__proto__ = { __proto__: Uint8Array.prototype, foo: function () { return 42; } }, 42 === t.foo() && "function" == typeof t.subarray && 0 === t.subarray(1, 1).byteLength;
                    }
                    catch (t) {
                        return !1;
                    } }
                    function i() { return Buffer.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823; }
                    function o(t, r) { if (i() < r)
                        throw new RangeError("Invalid typed array length"); return Buffer.TYPED_ARRAY_SUPPORT ? (t = new Uint8Array(r), t.__proto__ = Buffer.prototype) : (null === t && (t = new Buffer(r)), t.length = r), t; }
                    function Buffer(t, r, e) { if (!(Buffer.TYPED_ARRAY_SUPPORT || this instanceof Buffer))
                        return new Buffer(t, r, e); if ("number" == typeof t) {
                        if ("string" == typeof r)
                            throw new Error("If encoding is specified then the first argument must be a string");
                        return s(this, t);
                    } return f(this, t, r, e); }
                    function f(t, r, e, n) { if ("number" == typeof r)
                        throw new TypeError('"value" argument must not be a number'); return "undefined" != typeof ArrayBuffer && r instanceof ArrayBuffer ? l(t, r, e, n) : "string" == typeof r ? c(t, r, e) : p(t, r); }
                    function u(t) { if ("number" != typeof t)
                        throw new TypeError('"size" argument must be a number'); if (t < 0)
                        throw new RangeError('"size" argument must not be negative'); }
                    function a(t, r, e, n) { return u(r), r <= 0 ? o(t, r) : void 0 !== e ? "string" == typeof n ? o(t, r).fill(e, n) : o(t, r).fill(e) : o(t, r); }
                    function s(t, r) { if (u(r), t = o(t, r < 0 ? 0 : 0 | d(r)), !Buffer.TYPED_ARRAY_SUPPORT)
                        for (var e = 0; e < r; ++e)
                            t[e] = 0; return t; }
                    function c(t, r, e) { if ("string" == typeof e && "" !== e || (e = "utf8"), !Buffer.isEncoding(e))
                        throw new TypeError('"encoding" must be a valid string encoding'); var n = 0 | v(r, e); t = o(t, n); var i = t.write(r, e); return i !== n && (t = t.slice(0, i)), t; }
                    function h(t, r) { var e = r.length < 0 ? 0 : 0 | d(r.length); t = o(t, e); for (var n = 0; n < e; n += 1)
                        t[n] = 255 & r[n]; return t; }
                    function l(t, r, e, n) { if (r.byteLength, e < 0 || r.byteLength < e)
                        throw new RangeError("'offset' is out of bounds"); if (r.byteLength < e + (n || 0))
                        throw new RangeError("'length' is out of bounds"); return r = void 0 === e && void 0 === n ? new Uint8Array(r) : void 0 === n ? new Uint8Array(r, e) : new Uint8Array(r, e, n), Buffer.TYPED_ARRAY_SUPPORT ? (t = r, t.__proto__ = Buffer.prototype) : t = h(t, r), t; }
                    function p(t, r) { if (Buffer.isBuffer(r)) {
                        var e = 0 | d(r.length);
                        return t = o(t, e), 0 === t.length ? t : (r.copy(t, 0, 0, e), t);
                    } if (r) {
                        if ("undefined" != typeof ArrayBuffer && r.buffer instanceof ArrayBuffer || "length" in r)
                            return "number" != typeof r.length || H(r.length) ? o(t, 0) : h(t, r);
                        if ("Buffer" === r.type && Q(r.data))
                            return h(t, r.data);
                    } throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object."); }
                    function d(t) { if (t >= i())
                        throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + i().toString(16) + " bytes"); return 0 | t; }
                    function y(t) { return +t != t && (t = 0), Buffer.alloc(+t); }
                    function v(t, r) { if (Buffer.isBuffer(t))
                        return t.length; if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(t) || t instanceof ArrayBuffer))
                        return t.byteLength; "string" != typeof t && (t = "" + t); var e = t.length; if (0 === e)
                        return 0; for (var n = !1;;)
                        switch (r) {
                            case "ascii":
                            case "latin1":
                            case "binary": return e;
                            case "utf8":
                            case "utf-8":
                            case void 0: return q(t).length;
                            case "ucs2":
                            case "ucs-2":
                            case "utf16le":
                            case "utf-16le": return 2 * e;
                            case "hex": return e >>> 1;
                            case "base64": return X(t).length;
                            default:
                                if (n)
                                    return q(t).length;
                                r = ("" + r).toLowerCase(), n = !0;
                        } }
                    function g(t, r, e) { var n = !1; if ((void 0 === r || r < 0) && (r = 0), r > this.length)
                        return ""; if ((void 0 === e || e > this.length) && (e = this.length), e <= 0)
                        return ""; if (e >>>= 0, r >>>= 0, e <= r)
                        return ""; for (t || (t = "utf8");;)
                        switch (t) {
                            case "hex": return I(this, r, e);
                            case "utf8":
                            case "utf-8": return k(this, r, e);
                            case "ascii": return T(this, r, e);
                            case "latin1":
                            case "binary": return S(this, r, e);
                            case "base64": return R(this, r, e);
                            case "ucs2":
                            case "ucs-2":
                            case "utf16le":
                            case "utf-16le": return Y(this, r, e);
                            default:
                                if (n)
                                    throw new TypeError("Unknown encoding: " + t);
                                t = (t + "").toLowerCase(), n = !0;
                        } }
                    function b(t, r, e) { var n = t[r]; t[r] = t[e], t[e] = n; }
                    function w(t, r, e, n, i) { if (0 === t.length)
                        return -1; if ("string" == typeof e ? (n = e, e = 0) : e > 2147483647 ? e = 2147483647 : e < -2147483648 && (e = -2147483648), e = +e, isNaN(e) && (e = i ? 0 : t.length - 1), e < 0 && (e = t.length + e), e >= t.length) {
                        if (i)
                            return -1;
                        e = t.length - 1;
                    }
                    else if (e < 0) {
                        if (!i)
                            return -1;
                        e = 0;
                    } if ("string" == typeof r && (r = Buffer.from(r, n)), Buffer.isBuffer(r))
                        return 0 === r.length ? -1 : E(t, r, e, n, i); if ("number" == typeof r)
                        return r = 255 & r, Buffer.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? i ? Uint8Array.prototype.indexOf.call(t, r, e) : Uint8Array.prototype.lastIndexOf.call(t, r, e) : E(t, [r], e, n, i); throw new TypeError("val must be string, number or Buffer"); }
                    function E(t, r, e, n, i) { function o(t, r) { return 1 === f ? t[r] : t.readUInt16BE(r * f); } var f = 1, u = t.length, a = r.length; if (void 0 !== n && (n = String(n).toLowerCase(), "ucs2" === n || "ucs-2" === n || "utf16le" === n || "utf-16le" === n)) {
                        if (t.length < 2 || r.length < 2)
                            return -1;
                        f = 2, u /= 2, a /= 2, e /= 2;
                    } var s; if (i) {
                        var c = -1;
                        for (s = e; s < u; s++)
                            if (o(t, s) === o(r, c === -1 ? 0 : s - c)) {
                                if (c === -1 && (c = s), s - c + 1 === a)
                                    return c * f;
                            }
                            else
                                c !== -1 && (s -= s - c), c = -1;
                    }
                    else
                        for (e + a > u && (e = u - a), s = e; s >= 0; s--) {
                            for (var h = !0, l = 0; l < a; l++)
                                if (o(t, s + l) !== o(r, l)) {
                                    h = !1;
                                    break;
                                }
                            if (h)
                                return s;
                        } return -1; }
                    function A(t, r, e, n) { e = Number(e) || 0; var i = t.length - e; n ? (n = Number(n), n > i && (n = i)) : n = i; var o = r.length; if (o % 2 !== 0)
                        throw new TypeError("Invalid hex string"); n > o / 2 && (n = o / 2); for (var f = 0; f < n; ++f) {
                        var u = parseInt(r.substr(2 * f, 2), 16);
                        if (isNaN(u))
                            return f;
                        t[e + f] = u;
                    } return f; }
                    function m(t, r, e, n) { return G(q(r, t.length - e), t, e, n); }
                    function x(t, r, e, n) { return G(W(r), t, e, n); }
                    function B(t, r, e, n) { return x(t, r, e, n); }
                    function U(t, r, e, n) { return G(X(r), t, e, n); }
                    function P(t, r, e, n) { return G(J(r, t.length - e), t, e, n); }
                    function R(t, r, e) { return 0 === r && e === t.length ? Z.fromByteArray(t) : Z.fromByteArray(t.slice(r, e)); }
                    function k(t, r, e) { e = Math.min(t.length, e); for (var n = [], i = r; i < e;) {
                        var o = t[i], f = null, u = o > 239 ? 4 : o > 223 ? 3 : o > 191 ? 2 : 1;
                        if (i + u <= e) {
                            var a, s, c, h;
                            switch (u) {
                                case 1:
                                    o < 128 && (f = o);
                                    break;
                                case 2:
                                    a = t[i + 1], 128 === (192 & a) && (h = (31 & o) << 6 | 63 & a, h > 127 && (f = h));
                                    break;
                                case 3:
                                    a = t[i + 1], s = t[i + 2], 128 === (192 & a) && 128 === (192 & s) && (h = (15 & o) << 12 | (63 & a) << 6 | 63 & s, h > 2047 && (h < 55296 || h > 57343) && (f = h));
                                    break;
                                case 4: a = t[i + 1], s = t[i + 2], c = t[i + 3], 128 === (192 & a) && 128 === (192 & s) && 128 === (192 & c) && (h = (15 & o) << 18 | (63 & a) << 12 | (63 & s) << 6 | 63 & c, h > 65535 && h < 1114112 && (f = h));
                            }
                        }
                        null === f ? (f = 65533, u = 1) : f > 65535 && (f -= 65536, n.push(f >>> 10 & 1023 | 55296), f = 56320 | 1023 & f), n.push(f), i += u;
                    } return _(n); }
                    function _(t) { var r = t.length; if (r <= $)
                        return String.fromCharCode.apply(String, t); for (var e = "", n = 0; n < r;)
                        e += String.fromCharCode.apply(String, t.slice(n, n += $)); return e; }
                    function T(t, r, e) { var n = ""; e = Math.min(t.length, e); for (var i = r; i < e; ++i)
                        n += String.fromCharCode(127 & t[i]); return n; }
                    function S(t, r, e) { var n = ""; e = Math.min(t.length, e); for (var i = r; i < e; ++i)
                        n += String.fromCharCode(t[i]); return n; }
                    function I(t, r, e) { var n = t.length; (!r || r < 0) && (r = 0), (!e || e < 0 || e > n) && (e = n); for (var i = "", o = r; o < e; ++o)
                        i += V(t[o]); return i; }
                    function Y(t, r, e) { for (var n = t.slice(r, e), i = "", o = 0; o < n.length; o += 2)
                        i += String.fromCharCode(n[o] + 256 * n[o + 1]); return i; }
                    function C(t, r, e) { if (t % 1 !== 0 || t < 0)
                        throw new RangeError("offset is not uint"); if (t + r > e)
                        throw new RangeError("Trying to access beyond buffer length"); }
                    function D(t, r, e, n, i, o) { if (!Buffer.isBuffer(t))
                        throw new TypeError('"buffer" argument must be a Buffer instance'); if (r > i || r < o)
                        throw new RangeError('"value" argument is out of bounds'); if (e + n > t.length)
                        throw new RangeError("Index out of range"); }
                    function O(t, r, e, n) { r < 0 && (r = 65535 + r + 1); for (var i = 0, o = Math.min(t.length - e, 2); i < o; ++i)
                        t[e + i] = (r & 255 << 8 * (n ? i : 1 - i)) >>> 8 * (n ? i : 1 - i); }
                    function L(t, r, e, n) { r < 0 && (r = 4294967295 + r + 1); for (var i = 0, o = Math.min(t.length - e, 4); i < o; ++i)
                        t[e + i] = r >>> 8 * (n ? i : 3 - i) & 255; }
                    function M(t, r, e, n, i, o) { if (e + n > t.length)
                        throw new RangeError("Index out of range"); if (e < 0)
                        throw new RangeError("Index out of range"); }
                    function N(t, r, e, n, i) { return i || M(t, r, e, 4, 3.4028234663852886e38, -3.4028234663852886e38), K.write(t, r, e, n, 23, 4), e + 4; }
                    function F(t, r, e, n, i) { return i || M(t, r, e, 8, 1.7976931348623157e308, -1.7976931348623157e308), K.write(t, r, e, n, 52, 8), e + 8; }
                    function j(t) {
                        if (t = z(t).replace(tt, ""), t.length < 2)
                            return "";
                        for (; t.length % 4 !== 0;)
                            t += "=";
                        return t;
                    }
                    function z(t) { return t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, ""); }
                    function V(t) { return t < 16 ? "0" + t.toString(16) : t.toString(16); }
                    function q(t, r) { r = r || 1 / 0; for (var e, n = t.length, i = null, o = [], f = 0; f < n; ++f) {
                        if (e = t.charCodeAt(f), e > 55295 && e < 57344) {
                            if (!i) {
                                if (e > 56319) {
                                    (r -= 3) > -1 && o.push(239, 191, 189);
                                    continue;
                                }
                                if (f + 1 === n) {
                                    (r -= 3) > -1 && o.push(239, 191, 189);
                                    continue;
                                }
                                i = e;
                                continue;
                            }
                            if (e < 56320) {
                                (r -= 3) > -1 && o.push(239, 191, 189), i = e;
                                continue;
                            }
                            e = (i - 55296 << 10 | e - 56320) + 65536;
                        }
                        else
                            i && (r -= 3) > -1 && o.push(239, 191, 189);
                        if (i = null, e < 128) {
                            if ((r -= 1) < 0)
                                break;
                            o.push(e);
                        }
                        else if (e < 2048) {
                            if ((r -= 2) < 0)
                                break;
                            o.push(e >> 6 | 192, 63 & e | 128);
                        }
                        else if (e < 65536) {
                            if ((r -= 3) < 0)
                                break;
                            o.push(e >> 12 | 224, e >> 6 & 63 | 128, 63 & e | 128);
                        }
                        else {
                            if (!(e < 1114112))
                                throw new Error("Invalid code point");
                            if ((r -= 4) < 0)
                                break;
                            o.push(e >> 18 | 240, e >> 12 & 63 | 128, e >> 6 & 63 | 128, 63 & e | 128);
                        }
                    } return o; }
                    function W(t) { for (var r = [], e = 0; e < t.length; ++e)
                        r.push(255 & t.charCodeAt(e)); return r; }
                    function J(t, r) { for (var e, n, i, o = [], f = 0; f < t.length && !((r -= 2) < 0); ++f)
                        e = t.charCodeAt(f), n = e >> 8, i = e % 256, o.push(i), o.push(n); return o; }
                    function X(t) { return Z.toByteArray(j(t)); }
                    function G(t, r, e, n) { for (var i = 0; i < n && !(i + e >= r.length || i >= t.length); ++i)
                        r[i + e] = t[i]; return i; }
                    function H(t) { return t !== t; }
                    var Z = t("base64-js"), K = t("ieee754"), Q = t("isarray");
                    e.Buffer = Buffer, e.SlowBuffer = y, e.INSPECT_MAX_BYTES = 50, Buffer.TYPED_ARRAY_SUPPORT = void 0 !== r.TYPED_ARRAY_SUPPORT ? r.TYPED_ARRAY_SUPPORT : n(), e.kMaxLength = i(), Buffer.poolSize = 8192, Buffer._augment = function (t) { return t.__proto__ = Buffer.prototype, t; }, Buffer.from = function (t, r, e) { return f(null, t, r, e); }, Buffer.TYPED_ARRAY_SUPPORT && (Buffer.prototype.__proto__ = Uint8Array.prototype, Buffer.__proto__ = Uint8Array, "undefined" != typeof Symbol && Symbol.species && Buffer[Symbol.species] === Buffer && Object.defineProperty(Buffer, Symbol.species, { value: null, configurable: !0 })), Buffer.alloc = function (t, r, e) { return a(null, t, r, e); }, Buffer.allocUnsafe = function (t) { return s(null, t); }, Buffer.allocUnsafeSlow = function (t) { return s(null, t); }, Buffer.isBuffer = function (t) { return !(null == t || !t._isBuffer); }, Buffer.compare = function (t, r) { if (!Buffer.isBuffer(t) || !Buffer.isBuffer(r))
                        throw new TypeError("Arguments must be Buffers"); if (t === r)
                        return 0; for (var e = t.length, n = r.length, i = 0, o = Math.min(e, n); i < o; ++i)
                        if (t[i] !== r[i]) {
                            e = t[i], n = r[i];
                            break;
                        } return e < n ? -1 : n < e ? 1 : 0; }, Buffer.isEncoding = function (t) { switch (String(t).toLowerCase()) {
                        case "hex":
                        case "utf8":
                        case "utf-8":
                        case "ascii":
                        case "latin1":
                        case "binary":
                        case "base64":
                        case "ucs2":
                        case "ucs-2":
                        case "utf16le":
                        case "utf-16le": return !0;
                        default: return !1;
                    } }, Buffer.concat = function (t, r) { if (!Q(t))
                        throw new TypeError('"list" argument must be an Array of Buffers'); if (0 === t.length)
                        return Buffer.alloc(0); var e; if (void 0 === r)
                        for (r = 0, e = 0; e < t.length; ++e)
                            r += t[e].length; var n = Buffer.allocUnsafe(r), i = 0; for (e = 0; e < t.length; ++e) {
                        var o = t[e];
                        if (!Buffer.isBuffer(o))
                            throw new TypeError('"list" argument must be an Array of Buffers');
                        o.copy(n, i), i += o.length;
                    } return n; }, Buffer.byteLength = v, Buffer.prototype._isBuffer = !0, Buffer.prototype.swap16 = function () { var t = this.length; if (t % 2 !== 0)
                        throw new RangeError("Buffer size must be a multiple of 16-bits"); for (var r = 0; r < t; r += 2)
                        b(this, r, r + 1); return this; }, Buffer.prototype.swap32 = function () { var t = this.length; if (t % 4 !== 0)
                        throw new RangeError("Buffer size must be a multiple of 32-bits"); for (var r = 0; r < t; r += 4)
                        b(this, r, r + 3), b(this, r + 1, r + 2); return this; }, Buffer.prototype.swap64 = function () { var t = this.length; if (t % 8 !== 0)
                        throw new RangeError("Buffer size must be a multiple of 64-bits"); for (var r = 0; r < t; r += 8)
                        b(this, r, r + 7), b(this, r + 1, r + 6), b(this, r + 2, r + 5), b(this, r + 3, r + 4); return this; }, Buffer.prototype.toString = function () { var t = 0 | this.length; return 0 === t ? "" : 0 === arguments.length ? k(this, 0, t) : g.apply(this, arguments); }, Buffer.prototype.equals = function (t) { if (!Buffer.isBuffer(t))
                        throw new TypeError("Argument must be a Buffer"); return this === t || 0 === Buffer.compare(this, t); }, Buffer.prototype.inspect = function () { var t = "", r = e.INSPECT_MAX_BYTES; return this.length > 0 && (t = this.toString("hex", 0, r).match(/.{2}/g).join(" "), this.length > r && (t += " ... ")), "<Buffer " + t + ">"; }, Buffer.prototype.compare = function (t, r, e, n, i) { if (!Buffer.isBuffer(t))
                        throw new TypeError("Argument must be a Buffer"); if (void 0 === r && (r = 0), void 0 === e && (e = t ? t.length : 0), void 0 === n && (n = 0), void 0 === i && (i = this.length), r < 0 || e > t.length || n < 0 || i > this.length)
                        throw new RangeError("out of range index"); if (n >= i && r >= e)
                        return 0; if (n >= i)
                        return -1; if (r >= e)
                        return 1; if (r >>>= 0, e >>>= 0, n >>>= 0, i >>>= 0, this === t)
                        return 0; for (var o = i - n, f = e - r, u = Math.min(o, f), a = this.slice(n, i), s = t.slice(r, e), c = 0; c < u; ++c)
                        if (a[c] !== s[c]) {
                            o = a[c], f = s[c];
                            break;
                        } return o < f ? -1 : f < o ? 1 : 0; }, Buffer.prototype.includes = function (t, r, e) { return this.indexOf(t, r, e) !== -1; }, Buffer.prototype.indexOf = function (t, r, e) { return w(this, t, r, e, !0); }, Buffer.prototype.lastIndexOf = function (t, r, e) { return w(this, t, r, e, !1); }, Buffer.prototype.write = function (t, r, e, n) { if (void 0 === r)
                        n = "utf8", e = this.length, r = 0;
                    else if (void 0 === e && "string" == typeof r)
                        n = r, e = this.length, r = 0;
                    else {
                        if (!isFinite(r))
                            throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
                        r = 0 | r, isFinite(e) ? (e = 0 | e, void 0 === n && (n = "utf8")) : (n = e, e = void 0);
                    } var i = this.length - r; if ((void 0 === e || e > i) && (e = i), t.length > 0 && (e < 0 || r < 0) || r > this.length)
                        throw new RangeError("Attempt to write outside buffer bounds"); n || (n = "utf8"); for (var o = !1;;)
                        switch (n) {
                            case "hex": return A(this, t, r, e);
                            case "utf8":
                            case "utf-8": return m(this, t, r, e);
                            case "ascii": return x(this, t, r, e);
                            case "latin1":
                            case "binary": return B(this, t, r, e);
                            case "base64": return U(this, t, r, e);
                            case "ucs2":
                            case "ucs-2":
                            case "utf16le":
                            case "utf-16le": return P(this, t, r, e);
                            default:
                                if (o)
                                    throw new TypeError("Unknown encoding: " + n);
                                n = ("" + n).toLowerCase(), o = !0;
                        } }, Buffer.prototype.toJSON = function () { return { type: "Buffer", data: Array.prototype.slice.call(this._arr || this, 0) }; };
                    var $ = 4096;
                    Buffer.prototype.slice = function (t, r) { var e = this.length; t = ~~t, r = void 0 === r ? e : ~~r, t < 0 ? (t += e, t < 0 && (t = 0)) : t > e && (t = e), r < 0 ? (r += e, r < 0 && (r = 0)) : r > e && (r = e), r < t && (r = t); var n; if (Buffer.TYPED_ARRAY_SUPPORT)
                        n = this.subarray(t, r), n.__proto__ = Buffer.prototype;
                    else {
                        var i = r - t;
                        n = new Buffer(i, void 0);
                        for (var o = 0; o < i; ++o)
                            n[o] = this[o + t];
                    } return n; }, Buffer.prototype.readUIntLE = function (t, r, e) { t = 0 | t, r = 0 | r, e || C(t, r, this.length); for (var n = this[t], i = 1, o = 0; ++o < r && (i *= 256);)
                        n += this[t + o] * i; return n; }, Buffer.prototype.readUIntBE = function (t, r, e) { t = 0 | t, r = 0 | r, e || C(t, r, this.length); for (var n = this[t + --r], i = 1; r > 0 && (i *= 256);)
                        n += this[t + --r] * i; return n; }, Buffer.prototype.readUInt8 = function (t, r) { return r || C(t, 1, this.length), this[t]; }, Buffer.prototype.readUInt16LE = function (t, r) { return r || C(t, 2, this.length), this[t] | this[t + 1] << 8; }, Buffer.prototype.readUInt16BE = function (t, r) { return r || C(t, 2, this.length), this[t] << 8 | this[t + 1]; }, Buffer.prototype.readUInt32LE = function (t, r) { return r || C(t, 4, this.length), (this[t] | this[t + 1] << 8 | this[t + 2] << 16) + 16777216 * this[t + 3]; }, Buffer.prototype.readUInt32BE = function (t, r) { return r || C(t, 4, this.length), 16777216 * this[t] + (this[t + 1] << 16 | this[t + 2] << 8 | this[t + 3]); }, Buffer.prototype.readIntLE = function (t, r, e) { t = 0 | t, r = 0 | r, e || C(t, r, this.length); for (var n = this[t], i = 1, o = 0; ++o < r && (i *= 256);)
                        n += this[t + o] * i; return i *= 128, n >= i && (n -= Math.pow(2, 8 * r)), n; }, Buffer.prototype.readIntBE = function (t, r, e) { t = 0 | t, r = 0 | r, e || C(t, r, this.length); for (var n = r, i = 1, o = this[t + --n]; n > 0 && (i *= 256);)
                        o += this[t + --n] * i; return i *= 128, o >= i && (o -= Math.pow(2, 8 * r)), o; }, Buffer.prototype.readInt8 = function (t, r) { return r || C(t, 1, this.length), 128 & this[t] ? (255 - this[t] + 1) * -1 : this[t]; }, Buffer.prototype.readInt16LE = function (t, r) { r || C(t, 2, this.length); var e = this[t] | this[t + 1] << 8; return 32768 & e ? 4294901760 | e : e; }, Buffer.prototype.readInt16BE = function (t, r) { r || C(t, 2, this.length); var e = this[t + 1] | this[t] << 8; return 32768 & e ? 4294901760 | e : e; }, Buffer.prototype.readInt32LE = function (t, r) { return r || C(t, 4, this.length), this[t] | this[t + 1] << 8 | this[t + 2] << 16 | this[t + 3] << 24; }, Buffer.prototype.readInt32BE = function (t, r) { return r || C(t, 4, this.length), this[t] << 24 | this[t + 1] << 16 | this[t + 2] << 8 | this[t + 3]; }, Buffer.prototype.readFloatLE = function (t, r) { return r || C(t, 4, this.length), K.read(this, t, !0, 23, 4); }, Buffer.prototype.readFloatBE = function (t, r) { return r || C(t, 4, this.length), K.read(this, t, !1, 23, 4); }, Buffer.prototype.readDoubleLE = function (t, r) { return r || C(t, 8, this.length), K.read(this, t, !0, 52, 8); }, Buffer.prototype.readDoubleBE = function (t, r) { return r || C(t, 8, this.length), K.read(this, t, !1, 52, 8); }, Buffer.prototype.writeUIntLE = function (t, r, e, n) { if (t = +t, r = 0 | r, e = 0 | e, !n) {
                        var i = Math.pow(2, 8 * e) - 1;
                        D(this, t, r, e, i, 0);
                    } var o = 1, f = 0; for (this[r] = 255 & t; ++f < e && (o *= 256);)
                        this[r + f] = t / o & 255; return r + e; }, Buffer.prototype.writeUIntBE = function (t, r, e, n) { if (t = +t, r = 0 | r, e = 0 | e, !n) {
                        var i = Math.pow(2, 8 * e) - 1;
                        D(this, t, r, e, i, 0);
                    } var o = e - 1, f = 1; for (this[r + o] = 255 & t; --o >= 0 && (f *= 256);)
                        this[r + o] = t / f & 255; return r + e; }, Buffer.prototype.writeUInt8 = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 1, 255, 0), Buffer.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)), this[r] = 255 & t, r + 1; }, Buffer.prototype.writeUInt16LE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 2, 65535, 0), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = 255 & t, this[r + 1] = t >>> 8) : O(this, t, r, !0), r + 2; }, Buffer.prototype.writeUInt16BE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 2, 65535, 0), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = t >>> 8, this[r + 1] = 255 & t) : O(this, t, r, !1), r + 2; }, Buffer.prototype.writeUInt32LE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 4, 4294967295, 0), Buffer.TYPED_ARRAY_SUPPORT ? (this[r + 3] = t >>> 24, this[r + 2] = t >>> 16, this[r + 1] = t >>> 8, this[r] = 255 & t) : L(this, t, r, !0), r + 4; }, Buffer.prototype.writeUInt32BE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 4, 4294967295, 0), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = t >>> 24, this[r + 1] = t >>> 16, this[r + 2] = t >>> 8, this[r + 3] = 255 & t) : L(this, t, r, !1), r + 4; }, Buffer.prototype.writeIntLE = function (t, r, e, n) { if (t = +t, r = 0 | r, !n) {
                        var i = Math.pow(2, 8 * e - 1);
                        D(this, t, r, e, i - 1, -i);
                    } var o = 0, f = 1, u = 0; for (this[r] = 255 & t; ++o < e && (f *= 256);)
                        t < 0 && 0 === u && 0 !== this[r + o - 1] && (u = 1), this[r + o] = (t / f >> 0) - u & 255; return r + e; }, Buffer.prototype.writeIntBE = function (t, r, e, n) { if (t = +t, r = 0 | r, !n) {
                        var i = Math.pow(2, 8 * e - 1);
                        D(this, t, r, e, i - 1, -i);
                    } var o = e - 1, f = 1, u = 0; for (this[r + o] = 255 & t; --o >= 0 && (f *= 256);)
                        t < 0 && 0 === u && 0 !== this[r + o + 1] && (u = 1), this[r + o] = (t / f >> 0) - u & 255; return r + e; }, Buffer.prototype.writeInt8 = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 1, 127, -128), Buffer.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)), t < 0 && (t = 255 + t + 1), this[r] = 255 & t, r + 1; }, Buffer.prototype.writeInt16LE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 2, 32767, -32768), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = 255 & t, this[r + 1] = t >>> 8) : O(this, t, r, !0), r + 2; }, Buffer.prototype.writeInt16BE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 2, 32767, -32768), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = t >>> 8, this[r + 1] = 255 & t) : O(this, t, r, !1), r + 2; }, Buffer.prototype.writeInt32LE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 4, 2147483647, -2147483648), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = 255 & t, this[r + 1] = t >>> 8, this[r + 2] = t >>> 16, this[r + 3] = t >>> 24) : L(this, t, r, !0), r + 4; }, Buffer.prototype.writeInt32BE = function (t, r, e) { return t = +t, r = 0 | r, e || D(this, t, r, 4, 2147483647, -2147483648), t < 0 && (t = 4294967295 + t + 1), Buffer.TYPED_ARRAY_SUPPORT ? (this[r] = t >>> 24, this[r + 1] = t >>> 16, this[r + 2] = t >>> 8, this[r + 3] = 255 & t) : L(this, t, r, !1), r + 4; }, Buffer.prototype.writeFloatLE = function (t, r, e) { return N(this, t, r, !0, e); }, Buffer.prototype.writeFloatBE = function (t, r, e) { return N(this, t, r, !1, e); }, Buffer.prototype.writeDoubleLE = function (t, r, e) { return F(this, t, r, !0, e); }, Buffer.prototype.writeDoubleBE = function (t, r, e) { return F(this, t, r, !1, e); }, Buffer.prototype.copy = function (t, r, e, n) { if (e || (e = 0), n || 0 === n || (n = this.length), r >= t.length && (r = t.length), r || (r = 0), n > 0 && n < e && (n = e), n === e)
                        return 0; if (0 === t.length || 0 === this.length)
                        return 0; if (r < 0)
                        throw new RangeError("targetStart out of bounds"); if (e < 0 || e >= this.length)
                        throw new RangeError("sourceStart out of bounds"); if (n < 0)
                        throw new RangeError("sourceEnd out of bounds"); n > this.length && (n = this.length), t.length - r < n - e && (n = t.length - r + e); var i, o = n - e; if (this === t && e < r && r < n)
                        for (i = o - 1; i >= 0; --i)
                            t[i + r] = this[i + e];
                    else if (o < 1e3 || !Buffer.TYPED_ARRAY_SUPPORT)
                        for (i = 0; i < o; ++i)
                            t[i + r] = this[i + e];
                    else
                        Uint8Array.prototype.set.call(t, this.subarray(e, e + o), r); return o; }, Buffer.prototype.fill = function (t, r, e, n) { if ("string" == typeof t) {
                        if ("string" == typeof r ? (n = r, r = 0, e = this.length) : "string" == typeof e && (n = e, e = this.length), 1 === t.length) {
                            var i = t.charCodeAt(0);
                            i < 256 && (t = i);
                        }
                        if (void 0 !== n && "string" != typeof n)
                            throw new TypeError("encoding must be a string");
                        if ("string" == typeof n && !Buffer.isEncoding(n))
                            throw new TypeError("Unknown encoding: " + n);
                    }
                    else
                        "number" == typeof t && (t = 255 & t); if (r < 0 || this.length < r || this.length < e)
                        throw new RangeError("Out of range index"); if (e <= r)
                        return this; r >>>= 0, e = void 0 === e ? this.length : e >>> 0, t || (t = 0); var o; if ("number" == typeof t)
                        for (o = r; o < e; ++o)
                            this[o] = t;
                    else {
                        var f = Buffer.isBuffer(t) ? t : q(new Buffer(t, n).toString()), u = f.length;
                        for (o = 0; o < e - r; ++o)
                            this[o + r] = f[o % u];
                    } return this; };
                    var tt = /[^+\/0-9A-Za-z-_]/g;
                }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
            }, { "base64-js": 30, ieee754: 32, isarray: 34 }], 30: [function (t, r, e) {
                "use strict";
                function n(t) { var r = t.length; if (r % 4 > 0)
                    throw new Error("Invalid string. Length must be a multiple of 4"); return "=" === t[r - 2] ? 2 : "=" === t[r - 1] ? 1 : 0; }
                function i(t) { return 3 * t.length / 4 - n(t); }
                function o(t) { var r, e, i, o, f, u, a = t.length; f = n(t), u = new h(3 * a / 4 - f), i = f > 0 ? a - 4 : a; var s = 0; for (r = 0, e = 0; r < i; r += 4, e += 3)
                    o = c[t.charCodeAt(r)] << 18 | c[t.charCodeAt(r + 1)] << 12 | c[t.charCodeAt(r + 2)] << 6 | c[t.charCodeAt(r + 3)], u[s++] = o >> 16 & 255, u[s++] = o >> 8 & 255, u[s++] = 255 & o; return 2 === f ? (o = c[t.charCodeAt(r)] << 2 | c[t.charCodeAt(r + 1)] >> 4, u[s++] = 255 & o) : 1 === f && (o = c[t.charCodeAt(r)] << 10 | c[t.charCodeAt(r + 1)] << 4 | c[t.charCodeAt(r + 2)] >> 2, u[s++] = o >> 8 & 255, u[s++] = 255 & o), u; }
                function f(t) { return s[t >> 18 & 63] + s[t >> 12 & 63] + s[t >> 6 & 63] + s[63 & t]; }
                function u(t, r, e) { for (var n, i = [], o = r; o < e; o += 3)
                    n = (t[o] << 16) + (t[o + 1] << 8) + t[o + 2], i.push(f(n)); return i.join(""); }
                function a(t) { for (var r, e = t.length, n = e % 3, i = "", o = [], f = 16383, a = 0, c = e - n; a < c; a += f)
                    o.push(u(t, a, a + f > c ? c : a + f)); return 1 === n ? (r = t[e - 1], i += s[r >> 2], i += s[r << 4 & 63], i += "==") : 2 === n && (r = (t[e - 2] << 8) + t[e - 1], i += s[r >> 10], i += s[r >> 4 & 63], i += s[r << 2 & 63], i += "="), o.push(i), o.join(""); }
                e.byteLength = i, e.toByteArray = o, e.fromByteArray = a;
                for (var s = [], c = [], h = "undefined" != typeof Uint8Array ? Uint8Array : Array, l = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", p = 0, d = l.length; p < d; ++p)
                    s[p] = l[p], c[l.charCodeAt(p)] = p;
                c["-".charCodeAt(0)] = 62, c["_".charCodeAt(0)] = 63;
            }, {}], 31: [function (t, r, e) { function n() { if (!(this instanceof n))
                return new n; } !function (t) { function e(t) { for (var r in s)
                t[r] = s[r]; return t; } function n(t, r) { return u(this, t).push(r), this; } function i(t, r) { function e() { o.call(n, t, e), r.apply(this, arguments); } var n = this; return e.originalListener = r, u(n, t).push(e), n; } function o(t, r) { function e(t) { return t !== r && t.originalListener !== r; } var n, i = this; if (arguments.length) {
                if (r) {
                    if (n = u(i, t, !0)) {
                        if (n = n.filter(e), !n.length)
                            return o.call(i, t);
                        i[a][t] = n;
                    }
                }
                else if (n = i[a], n && (delete n[t], !Object.keys(n).length))
                    return o.call(i);
            }
            else
                delete i[a]; return i; } function f(t, r) { function e(t) { t.call(o); } function n(t) { t.call(o, r); } function i(t) { t.apply(o, s); } var o = this, f = u(o, t, !0); if (!f)
                return !1; var a = arguments.length; if (1 === a)
                f.forEach(e);
            else if (2 === a)
                f.forEach(n);
            else {
                var s = Array.prototype.slice.call(arguments, 1);
                f.forEach(i);
            } return !!f.length; } function u(t, r, e) { if (!e || t[a]) {
                var n = t[a] || (t[a] = {});
                return n[r] || (n[r] = []);
            } } "undefined" != typeof r && (r.exports = t); var a = "listeners", s = { on: n, once: i, off: o, emit: f }; e(t.prototype), t.mixin = e; }(n); }, {}], 32: [function (t, r, e) { e.read = function (t, r, e, n, i) { var o, f, u = 8 * i - n - 1, a = (1 << u) - 1, s = a >> 1, c = -7, h = e ? i - 1 : 0, l = e ? -1 : 1, p = t[r + h]; for (h += l, o = p & (1 << -c) - 1, p >>= -c, c += u; c > 0; o = 256 * o + t[r + h], h += l, c -= 8)
                ; for (f = o & (1 << -c) - 1, o >>= -c, c += n; c > 0; f = 256 * f + t[r + h], h += l, c -= 8)
                ; if (0 === o)
                o = 1 - s;
            else {
                if (o === a)
                    return f ? NaN : (p ? -1 : 1) * (1 / 0);
                f += Math.pow(2, n), o -= s;
            } return (p ? -1 : 1) * f * Math.pow(2, o - n); }, e.write = function (t, r, e, n, i, o) { var f, u, a, s = 8 * o - i - 1, c = (1 << s) - 1, h = c >> 1, l = 23 === i ? Math.pow(2, -24) - Math.pow(2, -77) : 0, p = n ? 0 : o - 1, d = n ? 1 : -1, y = r < 0 || 0 === r && 1 / r < 0 ? 1 : 0; for (r = Math.abs(r), isNaN(r) || r === 1 / 0 ? (u = isNaN(r) ? 1 : 0, f = c) : (f = Math.floor(Math.log(r) / Math.LN2), r * (a = Math.pow(2, -f)) < 1 && (f--, a *= 2), r += f + h >= 1 ? l / a : l * Math.pow(2, 1 - h), r * a >= 2 && (f++, a /= 2), f + h >= c ? (u = 0, f = c) : f + h >= 1 ? (u = (r * a - 1) * Math.pow(2, i), f += h) : (u = r * Math.pow(2, h - 1) * Math.pow(2, i), f = 0)); i >= 8; t[e + p] = 255 & u, p += d, u /= 256, i -= 8)
                ; for (f = f << i | u, s += i; s > 0; t[e + p] = 255 & f, p += d, f /= 256, s -= 8)
                ; t[e + p - d] |= 128 * y; }; }, {}], 33: [function (t, r, e) { (function (Buffer) { var t, r, n, i; !function (e) { function o(t, r, n) { function i(t, r, e, n) { return this instanceof i ? v(this, t, r, e, n) : new i(t, r, e, n); } function o(t) { return !(!t || !t[F]); } function v(t, r, e, n, i) { if (E && A && (r instanceof A && (r = new E(r)), n instanceof A && (n = new E(n))), !(r || e || n || g))
                return void (t.buffer = h(m, 0)); if (!s(r, e)) {
                var o = g || Array;
                i = e, n = r, e = 0, r = new o(8);
            } t.buffer = r, t.offset = e |= 0, b !== typeof n && ("string" == typeof n ? x(r, e, n, i || 10) : s(n, i) ? c(r, e, n, i) : "number" == typeof i ? (k(r, e + T, n), k(r, e + S, i)) : n > 0 ? O(r, e, n) : n < 0 ? L(r, e, n) : c(r, e, m, 0)); } function x(t, r, e, n) { var i = 0, o = e.length, f = 0, u = 0; "-" === e[0] && i++; for (var a = i; i < o;) {
                var s = parseInt(e[i++], n);
                if (!(s >= 0))
                    break;
                u = u * n + s, f = f * n + Math.floor(u / B), u %= B;
            } a && (f = ~f, u ? u = B - u : f++), k(t, r + T, f), k(t, r + S, u); } function P() { var t = this.buffer, r = this.offset, e = _(t, r + T), i = _(t, r + S); return n || (e |= 0), e ? e * B + i : i; } function R(t) { var r = this.buffer, e = this.offset, i = _(r, e + T), o = _(r, e + S), f = "", u = !n && 2147483648 & i; for (u && (i = ~i, o = B - o), t = t || 10;;) {
                var a = i % t * B + o;
                if (i = Math.floor(i / t), o = Math.floor(a / t), f = (a % t).toString(t) + f, !i && !o)
                    break;
            } return u && (f = "-" + f), f; } function k(t, r, e) { t[r + D] = 255 & e, e >>= 8, t[r + C] = 255 & e, e >>= 8, t[r + Y] = 255 & e, e >>= 8, t[r + I] = 255 & e; } function _(t, r) { return t[r + I] * U + (t[r + Y] << 16) + (t[r + C] << 8) + t[r + D]; } var T = r ? 0 : 4, S = r ? 4 : 0, I = r ? 0 : 3, Y = r ? 1 : 2, C = r ? 2 : 1, D = r ? 3 : 0, O = r ? l : d, L = r ? p : y, M = i.prototype, N = "is" + t, F = "_" + N; return M.buffer = void 0, M.offset = 0, M[F] = !0, M.toNumber = P, M.toString = R, M.toJSON = P, M.toArray = f, w && (M.toBuffer = u), E && (M.toArrayBuffer = a), i[N] = o, e[t] = i, i; } function f(t) { var r = this.buffer, e = this.offset; return g = null, t !== !1 && 0 === e && 8 === r.length && x(r) ? r : h(r, e); } function u(t) { var r = this.buffer, e = this.offset; if (g = w, t !== !1 && 0 === e && 8 === r.length && Buffer.isBuffer(r))
                return r; var n = new w(8); return c(n, 0, r, e), n; } function a(t) { var r = this.buffer, e = this.offset, n = r.buffer; if (g = E, t !== !1 && 0 === e && n instanceof A && 8 === n.byteLength)
                return n; var i = new E(8); return c(i, 0, r, e), i.buffer; } function s(t, r) { var e = t && t.length; return r |= 0, e && r + 8 <= e && "string" != typeof t[r]; } function c(t, r, e, n) { r |= 0, n |= 0; for (var i = 0; i < 8; i++)
                t[r++] = 255 & e[n++]; } function h(t, r) { return Array.prototype.slice.call(t, r, r + 8); } function l(t, r, e) { for (var n = r + 8; n > r;)
                t[--n] = 255 & e, e /= 256; } function p(t, r, e) { var n = r + 8; for (e++; n > r;)
                t[--n] = 255 & -e ^ 255, e /= 256; } function d(t, r, e) { for (var n = r + 8; r < n;)
                t[r++] = 255 & e, e /= 256; } function y(t, r, e) { var n = r + 8; for (e++; r < n;)
                t[r++] = 255 & -e ^ 255, e /= 256; } function v(t) { return !!t && "[object Array]" == Object.prototype.toString.call(t); } var g, b = "undefined", w = b !== typeof Buffer && Buffer, E = b !== typeof Uint8Array && Uint8Array, A = b !== typeof ArrayBuffer && ArrayBuffer, m = [0, 0, 0, 0, 0, 0, 0, 0], x = Array.isArray || v, B = 4294967296, U = 16777216; t = o("Uint64BE", !0, !0), r = o("Int64BE", !0, !1), n = o("Uint64LE", !1, !0), i = o("Int64LE", !1, !1); }("object" == typeof e && "string" != typeof e.nodeName ? e : this || {}); }).call(this, t("buffer").Buffer); }, { buffer: 29 }], 34: [function (t, r, e) { var n = {}.toString; r.exports = Array.isArray || function (t) { return "[object Array]" == n.call(t); }; }, {}] }, {}, [1])(1);
});
var base;
(function (base) {
    var background;
    (function (background) {
        var InitCommand = (function () {
            function InitCommand() {
                this.enableCache = false;
                this.enableCheck = false;
                this.host = "";
            }
            InitCommand.prototype.type = function () {
                return InitCommand.opcode;
            };
            InitCommand.prototype.readTo = function (tuple, args) {
                var offset = 0;
                this.enableCache = tuple[offset++];
                this.enableCheck = tuple[offset++];
                this.host = tuple[offset++];
            };
            InitCommand.prototype.writeTo = function (args) {
                var result = [];
                result.push(this.enableCache);
                result.push(this.enableCheck);
                result.push(this.host);
                return result;
            };
            InitCommand.opcode = 0;
            return InitCommand;
        }());
        background.InitCommand = InitCommand;
        var InitCompleteCommand = (function () {
            function InitCompleteCommand() {
            }
            InitCompleteCommand.prototype.type = function () {
                return InitCompleteCommand.opcode;
            };
            InitCompleteCommand.prototype.readTo = function (tuple, args) {
                var offset = 0;
                this.status = tuple[offset++];
            };
            InitCompleteCommand.prototype.writeTo = function (args) {
                var result = [];
                result.push(this.status);
                return result;
            };
            InitCompleteCommand.opcode = 1;
            return InitCompleteCommand;
        }());
        background.InitCompleteCommand = InitCompleteCommand;
        var LoadCompleteCommand = (function () {
            function LoadCompleteCommand() {
            }
            LoadCompleteCommand.prototype.type = function () {
                return LoadCompleteCommand.opcode;
            };
            LoadCompleteCommand.prototype.readTo = function (tuple, args) {
                var offset = 0;
                this.url = tuple[offset++];
                this.status = tuple[offset++];
                var index = tuple[offset++];
                if (index != 0) {
                    this.bytes = args[index];
                }
            };
            LoadCompleteCommand.prototype.writeTo = function (args) {
                var result = [];
                result.push(this.url);
                result.push(this.status);
                if (this.bytes != null) {
                    result.push(args.length);
                    args.push(this.bytes);
                }
                else {
                    result.push(0);
                }
                return result;
            };
            LoadCompleteCommand.opcode = 3;
            return LoadCompleteCommand;
        }());
        background.LoadCompleteCommand = LoadCompleteCommand;
        var LoadCommand = (function () {
            function LoadCommand() {
            }
            LoadCommand.prototype.type = function () {
                return LoadCommand.opcode;
            };
            LoadCommand.prototype.readTo = function (tuple, args) {
                var offset = 0;
                this.url = tuple[offset++];
                this.uncompress = tuple[offset];
            };
            LoadCommand.prototype.writeTo = function (args) {
                var result = [];
                result.push(this.url);
                result.push(this.uncompress);
                return result;
            };
            LoadCommand.opcode = 2;
            return LoadCommand;
        }());
        background.LoadCommand = LoadCommand;
    })(background = base.background || (base.background = {}));
})(base || (base = {}));
var utils;
(function (utils) {
    var ArrayUtils = (function () {
        function ArrayUtils() {
        }
        ArrayUtils.insertAt = function (array, index, obj) {
            array.push(obj);
            if (index < 0) {
                return;
            }
            for (var j = array.length - 1; j > index; --j) {
                obj = array[j];
                array[j] = array[j - 1];
                array[j - 1] = obj;
            }
        };
        ArrayUtils.removeAt = function (array, index) {
            var last = array.length - 1;
            if (index > last)
                return null;
            var obj = array[index];
            for (var j = index; j < last; ++j) {
                array[j] = array[j + 1];
            }
            array.length = last;
            return obj;
        };
        ArrayUtils.remove = function (array, obj) {
            var index = array.indexOf(obj);
            if (index === -1) {
                return -1;
            }
            var last = array.length - 1;
            for (var j = index; j < last; ++j) {
                array[j] = array[j + 1];
            }
            array.length = last;
            return index;
        };
        ArrayUtils.removeRange = function (array, index, count, result) {
            if (result === void 0) { result = null; }
            var last = array.length - count;
            if (index > last) {
                last = index;
            }
            if (result != null) {
                var size = (index + count) > array.length ? array.length : index + count;
                for (var i = index; i < size; ++i) {
                    result[i - index] = array[i];
                }
            }
            for (var j = index; j < last; ++j) {
                array[j] = array[j + count];
            }
            array.length = last;
        };
        ArrayUtils.removeAll = function (array, obj) {
            var index = 0;
            while ((index = array.indexOf(obj, index)) != -1) {
                var last = array.length - 1;
                for (var j = index; j < last; ++j) {
                    array[j] = array[j + 1];
                }
                array.length = last;
            }
        };
        ArrayUtils.unsafeRemoveAt = function (array, index) {
            var last = array.length - 1;
            if (index > last)
                return null;
            var obj = array[index];
            array[index] = array[last];
            array.length = last;
            return obj;
        };
        ArrayUtils.unsafeRemove = function (array, obj) {
            var index = array.indexOf(obj);
            if (index === -1) {
                return -1;
            }
            var last = array.length - 1;
            array[index] = array[last];
            array.length = last;
            return index;
        };
        ArrayUtils.disturb = function (array) {
            var size = array.length;
            for (var i = size - 1; i >= 0; --i) {
                var upper = i + 1;
                var index = Math.floor(Math.random() * upper) % upper;
                if (i != index) {
                    var temp = array[i];
                    array[i] = array[index];
                    array[index] = temp;
                }
            }
        };
        ArrayUtils.random = function (array, offset) {
            if (offset === void 0) { offset = 0; }
            var size = array.length - offset;
            var index = Math.floor(Math.random() * size) % size;
            return array[index + offset];
        };
        ArrayUtils.randomIndex = function (array, offset) {
            if (offset === void 0) { offset = 0; }
            var size = array.length - offset;
            var index = Math.floor(Math.random() * size) % size;
            return index + offset;
        };
        ArrayUtils.reverse = function (array) {
            for (var l = 0, r = array.length - 1; l < r; ++l, --r) {
                var temp = array[l];
                array[l] = array[r];
                array[r] = temp;
            }
        };
        return ArrayUtils;
    }());
    utils.ArrayUtils = ArrayUtils;
})(utils || (utils = {}));
var base;
(function (base) {
    var background;
    (function (background) {
        var ArrayUtils = utils.ArrayUtils;
        var ActorProxy = (function () {
            function ActorProxy(worker) {
                var _this = this;
                this.onMessage = function (message) {
                    var args = message.data;
                    if (args.length == 0) {
                        return;
                    }
                    var commands = msgpack.decode(new Uint8Array(args[0]));
                    var length = commands.length;
                    for (var i = 0; i < length; ++i) {
                        var command = commands[i];
                        var type = command[0];
                        var opcode = new _this._opcodes[type];
                        opcode.readTo(command[1], args);
                        var handlers = _this._opcodeHandlers[type];
                        var length_1 = handlers.length;
                        for (var i_1 = 0; i_1 < length_1; ++i_1) {
                            handlers[i_1](opcode);
                        }
                    }
                };
                this.onUpdate = function () {
                    if (_this._list.length == 0) {
                        return;
                    }
                    var commandBuffer = msgpack.encode(_this._list).buffer;
                    _this._args[0] = commandBuffer;
                    _this._worker.postMessage(_this._args, _this._args);
                    _this._args = new Array(1);
                    _this._list.length = 0;
                };
                this._worker = worker;
                this._args = new Array(1);
                this._list = new Array();
                this._worker.onmessage = this.onMessage;
                this._opcodeHandlers = [];
                this._opcodes = [];
            }
            ActorProxy.prototype.registerOpcode = function (type, command) {
                this._opcodes[type] = command;
            };
            ActorProxy.prototype.addCommandHandler = function (type, handler) {
                var handlers = this._opcodeHandlers[type];
                if (handlers == null) {
                    handlers = new Array();
                    this._opcodeHandlers[type] = handlers;
                }
                if (handlers.indexOf(handler) != -1) {
                    return;
                }
                handlers.push(handler);
            };
            ActorProxy.prototype.removeCommandHandler = function (type, handler) {
                var handlers = this._opcodeHandlers[type];
                if (handlers == null) {
                    return;
                }
                ArrayUtils.remove(handlers, handler);
            };
            ActorProxy.prototype.sendCommand = function (command) {
                if (this._list.length == 0) {
                    this._worker.setTimeout(this.onUpdate, 0);
                }
                var tuple = command.writeTo(this._args);
                this._list.push([command.type(), tuple]);
            };
            return ActorProxy;
        }());
        background.ActorProxy = ActorProxy;
    })(background = base.background || (base.background = {}));
})(base || (base = {}));
var system;
(function (system) {
    var IndexedDBCache = (function () {
        function IndexedDBCache() {
        }
        IndexedDBCache.openOnSuccess = function (event) {
            var request = event.target;
            IndexedDBCache.db = request.result;
            if (!IndexedDBCache.db.objectStoreNames.contains("cache")) {
                IndexedDBCache.db.createObjectStore("cache", { keyPath: "url" });
            }
            IndexedDBCache.initCallback(0);
        };
        IndexedDBCache.openOnUpgradeNeeded = function (event) {
            var request = event.target;
            IndexedDBCache.db = request.result;
            if (!IndexedDBCache.db.objectStoreNames.contains("cache")) {
                IndexedDBCache.db.createObjectStore("cache", { keyPath: "url" });
            }
            IndexedDBCache.initCallback(0);
        };
        IndexedDBCache.openOnError = function (event) {
            IndexedDBCache.initCallback(1);
        };
        IndexedDBCache.initCache = function (enableCache, callback) {
            IndexedDBCache.enableCache = enableCache;
            if (IndexedDBCache.enableCache) {
                var request = indexedDB.open("routine");
                IndexedDBCache.initCallback = callback;
                request.onsuccess = IndexedDBCache.openOnSuccess;
                request.onupgradeneeded = IndexedDBCache.openOnUpgradeNeeded;
                request.onerror = IndexedDBCache.openOnError;
            }
            else {
                callback(0);
            }
        };
        IndexedDBCache.writeCache = function (url, version, buffer) {
            if (IndexedDBCache.db == null) {
                return;
            }
            var store = IndexedDBCache.db.transaction("cache", "readwrite").objectStore("cache");
            store.put({ url: url, version: version, value: buffer });
        };
        IndexedDBCache.readCache = function (url, callback) {
            if (IndexedDBCache.db == null) {
                callback(url, 0, null);
                return;
            }
            var store = IndexedDBCache.db.transaction("cache").objectStore("cache");
            var request = store.get(url);
            request.onerror = function (event) {
                callback(url, 0, null);
            };
            request.onsuccess = function (event) {
                if (request.result) {
                    callback(url, request.result.version, request.result.value);
                }
                else {
                    callback(url, 0, null);
                }
            };
        };
        return IndexedDBCache;
    }());
    system.IndexedDBCache = IndexedDBCache;
})(system || (system = {}));
var system;
(function (system) {
    var HttpFileEntry = (function () {
        function HttpFileEntry() {
        }
        HttpFileEntry.allocEntry = function () {
            var result;
            if (HttpFileEntry._freeEntry != null) {
                result = this._freeEntry;
                this._freeEntry = this._freeEntry.next;
            }
            else {
                result = new HttpFileEntry();
            }
            result.live = 0;
            result.uncompress = 0;
            return result;
        };
        HttpFileEntry.freeEntry = function (entry) {
            entry.callback = null;
            entry.request = null;
            entry.request = null;
            entry.next = this._freeEntry;
            HttpFileEntry._freeEntry = entry;
        };
        return HttpFileEntry;
    }());
    var HttpFileSystem = (function () {
        function HttpFileSystem() {
        }
        HttpFileSystem.init = function (host, enableCheck, callback) {
            PngDecoder.init();
            if (host != "" && host[host.length - 1] != "/") {
                host += "/";
            }
            this._host = host;
            this._enableCheck = enableCheck;
            if (HttpFileSystem._versionDBStatus != 0) {
                return;
            }
            HttpFileSystem._versionDBStatus = 1;
            var entry = HttpFileSystem.createEntry("brother.big", 1, function (url, status, buffer) {
                if (buffer == null) {
                    callback(status);
                }
                if (HttpFileSystem._enableCheck) {
                    HttpFileSystem._versionPool = msgpack.decode(new Uint8Array(buffer));
                }
                HttpFileSystem._versionDBStatus = 2;
                for (var url_1 in HttpFileSystem._loaderPool) {
                    var entry_1 = HttpFileSystem._loaderPool[url_1];
                    HttpFileSystem.triggerEntry(entry_1);
                }
                callback(0);
            });
            HttpFileSystem.openEntry(entry);
        };
        HttpFileSystem.fromCache = function (url, version, buffer) {
            if (buffer != null) {
                if (HttpFileSystem._versionPool[url] == version) {
                    HttpFileSystem.onComplete(url, 0, buffer);
                    return;
                }
            }
            var entry = HttpFileSystem._loaderPool[url];
            HttpFileSystem.openEntry(entry);
        };
        HttpFileSystem.triggerEntry = function (entry) {
            var url = entry.url;
            if (HttpFileSystem._versionPool[url] != null) {
                system.IndexedDBCache.readCache(url, HttpFileSystem.fromCache);
            }
            else {
                HttpFileSystem.openEntry(entry);
            }
        };
        HttpFileSystem.read = function (url, uncompress, callback) {
            var entry = HttpFileSystem._loaderPool[url];
            if (entry != null) {
                return;
            }
            entry = HttpFileSystem.createEntry(url, uncompress, callback);
            if (HttpFileSystem._versionDBStatus != 2) {
                return;
            }
            HttpFileSystem.triggerEntry(entry);
        };
        HttpFileSystem.createEntry = function (url, uncompress, callback) {
            var entry = HttpFileEntry.allocEntry();
            entry.url = url;
            entry.uncompress = uncompress;
            entry.live = HttpFileSystem._liveLimit;
            entry.callback = callback;
            entry.status = 0;
            HttpFileSystem._loaderPool[url] = entry;
            return entry;
        };
        HttpFileSystem.openEntry = function (entry) {
            if (entry.status != 0) {
                return;
            }
            entry.startTime = Date.now();
            var url = entry.url;
            entry.status = 1;
            var request = entry.request = HttpFileSystem._processorQueue.length != 0 ? HttpFileSystem._processorQueue.pop() : new XMLHttpRequest();
            request.responseType = "arraybuffer";
            request.onload = function (e) {
                var request = e.target;
                if (request.status == 200 || request.status == 0) {
                    if (HttpFileSystem._versionPool[url] != null) {
                        system.IndexedDBCache.writeCache(url, HttpFileSystem._versionPool[url], request.response);
                    }
                    HttpFileSystem.onComplete(url, request.status, request.response);
                }
                else {
                    if (--entry.live == 0) {
                        HttpFileSystem.onComplete(url, request.status, null);
                    }
                    else {
                        request.abort();
                        var version_1 = HttpFileSystem._versionPool[url] || Date.now();
                        request.open("GET", "" + HttpFileSystem._host + url + "?v=" + version_1);
                        request.send();
                    }
                }
            };
            var version = HttpFileSystem._versionPool[url] || Date.now();
            request.open("GET", "" + HttpFileSystem._host + url + "?v=" + version);
            request.send();
        };
        HttpFileSystem.decodeImage = function (buffer) {
            if (this.isPng(new Uint8Array(buffer))) {
                buffer = PngDecoder.decode(new Uint8Array(buffer));
            }
            else {
                var p = new JpegImage();
                p.parse(new Uint8Array(buffer));
                var w = p.width;
                var h = p.height;
                var data = p.getData(w, h, true);
                var needConvert = (w % 2 != 0 || h % 2 != 0) && (data.byteLength / (w * h)) == 3;
                if (needConvert) {
                    buffer = new ArrayBuffer(8 + w * h * 4);
                }
                else {
                    buffer = new ArrayBuffer(8 + data.byteLength);
                }
                var view = new DataView(buffer);
                view.setUint32(0, w);
                view.setUint32(4, h);
                var stream = new Uint8Array(buffer, 8);
                if (needConvert) {
                    for (var i = 0; i < (w * h); ++i) {
                        stream[i * 4] = data[i * 3];
                        stream[i * 4 + 1] = data[i * 3 + 1];
                        stream[i * 4 + 2] = data[i * 3 + 2];
                        stream[i * 4 + 3] = 0xff;
                    }
                }
                else {
                    stream.set(data);
                }
            }
            return buffer;
        };
        HttpFileSystem.isPng = function (buffer) {
            var header = [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A];
            for (var i = 0; i < 8; ++i) {
                if (buffer[i] != header[i]) {
                    return false;
                }
            }
            return true;
        };
        HttpFileSystem.onComplete = function (url, status, buffer) {
            var entry = HttpFileSystem._loaderPool[url];
            delete HttpFileSystem._loaderPool[url];
            if (buffer != null) {
                switch (entry.uncompress) {
                    case 1: {
                        var data = new Zlib.Inflate(new Uint8Array(buffer)).decompress();
                        buffer = data.buffer.slice(data.byteOffset, data.byteLength);
                        break;
                    }
                }
                var name = url.toLowerCase();
                if (name.lastIndexOf(".png") == (name.length - 4) || name.lastIndexOf(".jpg") == (name.length - 4)) {
                    buffer = HttpFileSystem.decodeImage(buffer);
                }
            }
            var callback = entry.callback;
            if (entry.request != null) {
                entry.request.abort();
                HttpFileSystem._processorQueue.push(entry.request);
            }
            HttpFileEntry.freeEntry(entry);
            callback(url, status, buffer);
        };
        HttpFileSystem._processorQueue = new Array();
        HttpFileSystem._liveLimit = 3;
        HttpFileSystem._loaderPool = {};
        HttpFileSystem._versionPool = {};
        HttpFileSystem._versionDBStatus = 0;
        return HttpFileSystem;
    }());
    system.HttpFileSystem = HttpFileSystem;
})(system || (system = {}));
var ActorProxy = base.background.ActorProxy;
var LoadCommand = base.background.LoadCommand;
var LoadCompleteCommand = base.background.LoadCompleteCommand;
var HttpFileSystem = system.HttpFileSystem;
var InitCommand = base.background.InitCommand;
var InitCompleteCommand = base.background.InitCompleteCommand;
var IndexedDBCache = system.IndexedDBCache;
var actor = new ActorProxy(self);
actor.registerOpcode(InitCommand.opcode, InitCommand);
actor.registerOpcode(InitCompleteCommand.opcode, InitCompleteCommand);
actor.registerOpcode(LoadCommand.opcode, LoadCommand);
actor.registerOpcode(LoadCompleteCommand.opcode, LoadCompleteCommand);
actor.addCommandHandler(InitCommand.opcode, function (command) {
    var enableCheck = command.enableCheck;
    var host = command.host;
    IndexedDBCache.initCache(command.enableCache, function () {
        HttpFileSystem.init(host, enableCheck, function (status) {
            var command = new InitCompleteCommand();
            command.status = status;
            actor.sendCommand(command);
        });
    });
});
actor.addCommandHandler(LoadCommand.opcode, function (command) {
    var url = command.url;
    var uncompress = command.uncompress;
    HttpFileSystem.read(url, uncompress, function (url, status, buffer) {
        var command = new LoadCompleteCommand();
        command.url = url;
        command.status = status;
        command.bytes = buffer;
        actor.sendCommand(command);
    });
});
var ByteStream = (function () {
    function ByteStream() {
    }
    ByteStream.prototype.bytesAvailable = function () {
        return this.length - this.cursor;
    };
    ByteStream.prototype.getUint8 = function () {
        return this.input[this.cursor++];
    };
    ByteStream.prototype.getUint16 = function () {
        var result = this.view.getUint16(this.cursor, this.littleEndian);
        this.cursor += 2;
        return result;
    };
    ByteStream.prototype.getUint32 = function () {
        var result = this.view.getUint32(this.cursor, this.littleEndian);
        this.cursor += 4;
        return result;
    };
    ByteStream.prototype.getUTF = function (size) {
        var result = new Array(size);
        for (var i = 0; i < size; ++i) {
            result[i] = String.fromCharCode(this.input[this.cursor++]);
        }
        return result.join("");
    };
    ByteStream.prototype.getBytes = function (size) {
        var result = new Uint8Array(this.input.buffer, this.input.byteOffset + this.cursor, size);
        this.cursor += size;
        return result;
    };
    ByteStream.prototype.skip = function (size) {
        this.cursor += size;
    };
    ByteStream.prototype.open = function (input, littleEndian) {
        if (littleEndian === void 0) { littleEndian = false; }
        this.cursor = 8;
        this.length = input.byteLength;
        this.input = input;
        this.view = new DataView(input.buffer);
        this.littleEndian = littleEndian;
    };
    ByteStream.prototype.close = function () {
        this.input = null;
        this.view = null;
    };
    return ByteStream;
}());
var PngFileEntry = (function () {
    function PngFileEntry() {
        this.segments = [];
    }
    PngFileEntry.allocEntry = function (input, littleEndian) {
        if (littleEndian === void 0) { littleEndian = false; }
        var result;
        if (PngFileEntry._freeEntry != null) {
            result = this._freeEntry;
            this._freeEntry = this._freeEntry.next;
        }
        else {
            result = new PngFileEntry();
        }
        return result;
    };
    PngFileEntry.freeEntry = function (entry) {
        entry.next = this._freeEntry;
        PngFileEntry._freeEntry = entry;
        entry.palette = null;
        entry.segments.length = 0;
        entry.transparency = null;
    };
    return PngFileEntry;
}());
var PngDecoder = (function () {
    function PngDecoder() {
    }
    PngDecoder.init = function () {
        PngDecoder.sectionHandlers = {
            IHDR: PngDecoder.handlerIHDR,
            PLTE: PngDecoder.handlerPLTE,
            IDAT: PngDecoder.handlerIDAT,
            tRNS: PngDecoder.handlerTRNS,
        };
    };
    PngDecoder.handlerIHDR = function (entry, stream, chunkSize) {
        entry.width = stream.getUint32();
        entry.height = stream.getUint32();
        entry.bits = stream.getUint8();
        entry.colorType = stream.getUint8();
        entry.compressionMethod = stream.getUint8();
        entry.filterMethod = stream.getUint8();
        entry.interlaceMethod = stream.getUint8();
    };
    PngDecoder.handlerPLTE = function (entry, stream, chunkSize) {
        entry.palette = stream.getBytes(chunkSize);
    };
    PngDecoder.handlerIDAT = function (entry, stream, chunkSize) {
        entry.segments.push(stream.getBytes(chunkSize));
    };
    PngDecoder.handlerTRNS = function (entry, stream, chunkSize) {
        entry.transparency = stream.getBytes(chunkSize);
    };
    PngDecoder.decodePalette = function (entry) {
        var palette = entry.palette;
        var transparency = entry.transparency;
        var length = palette.length;
        var result = new Uint8Array(length / 3 * 4);
        var offset = 0;
        var cursor = 0;
        var size = transparency.byteLength;
        var index = 0;
        while (offset < length) {
            result[cursor++] = palette[offset++];
            result[cursor++] = palette[offset++];
            result[cursor++] = palette[offset++];
            result[cursor++] = (index < size) ? transparency[index++] : 255;
        }
        return result;
    };
    ;
    PngDecoder.mergeSegments = function (segments) {
        var byteLength = 0;
        for (var _i = 0, segments_1 = segments; _i < segments_1.length; _i++) {
            var segment = segments_1[_i];
            byteLength += segment.byteLength;
        }
        var bytes = new Uint8Array(byteLength);
        var offset = 0;
        for (var _a = 0, segments_2 = segments; _a < segments_2.length; _a++) {
            var segment = segments_2[_a];
            bytes.set(segment, offset);
            offset += segment.length;
        }
        return new Zlib.Inflate(bytes).decompress();
    };
    PngDecoder.calcPixelBytes = function (entry) {
        var channels = 3;
        if (entry.colorType & 4) {
            channels = 4;
        }
        if (entry.colorType == 3 && entry.transparency) {
            channels = 4;
        }
        return channels;
    };
    PngDecoder.calcIndexBytes = function (entry) {
        var channels = 1;
        switch (entry.colorType) {
            case 2: {
                channels = 3;
                break;
            }
            case 4: {
                channels = 2;
                break;
            }
            case 6: {
                channels = 4;
                break;
            }
        }
        var bits = channels * entry.bits;
        return (bits + 7) >> 3;
    };
    PngDecoder.decodePixels = function (entry) {
        var bytes = PngDecoder.mergeSegments(entry.segments);
        var length = bytes.byteLength;
        var height = entry.height;
        var pixelBytes = PngDecoder.calcIndexBytes(entry);
        var rowBytes = (length - height) / height;
        var rowOffset = rowBytes + 1;
        var i = 0;
        var cursor = 0;
        var a = 0, b = 0, c = 0, p = 0, pa = 0, pb = 0, pc = 0, pr = 0;
        while (cursor < length) {
            switch (bytes[cursor++]) {
                case 0: {
                    cursor += rowBytes;
                    break;
                }
                case 1: {
                    cursor += pixelBytes;
                    for (i = pixelBytes; i < rowBytes; ++i, ++cursor) {
                        bytes[cursor] = (bytes[cursor] + bytes[cursor - pixelBytes]) % 256;
                    }
                    break;
                }
                case 2: {
                    if (cursor != 1) {
                        for (i = 0; i < rowBytes; ++i, ++cursor) {
                            bytes[cursor] = (bytes[cursor] + bytes[cursor - rowOffset]) % 256;
                        }
                    }
                    break;
                }
                case 3: {
                    if (cursor == 1) {
                        cursor += pixelBytes;
                        for (i = pixelBytes; i < rowBytes; ++i, ++cursor) {
                            bytes[cursor] = (bytes[cursor] + (bytes[cursor - pixelBytes] >> 1)) % 256;
                        }
                    }
                    else {
                        for (i = 0; i < pixelBytes; ++i, ++cursor) {
                            bytes[cursor] = (bytes[cursor] + (bytes[cursor - rowOffset] >> 1)) % 256;
                        }
                        for (i = pixelBytes; i < rowBytes; ++i, ++cursor) {
                            bytes[cursor] = (bytes[cursor] + ((bytes[cursor - pixelBytes] + bytes[cursor - rowOffset]) >> 1)) % 256;
                        }
                    }
                    break;
                }
                case 4: {
                    if (pixelBytes == 1) {
                        if (cursor == 1) {
                            a = bytes[cursor++];
                            for (i = 1; i < rowBytes; ++i, ++cursor) {
                                pr = (a > 0) ? a : 0;
                                a = bytes[cursor] = (bytes[cursor] + pr) % 256;
                            }
                        }
                        else {
                            b = bytes[cursor - rowOffset];
                            p = b;
                            pa = p;
                            if (pa < 0) {
                                pa = -pa;
                            }
                            pc = p;
                            if (pc < 0) {
                                pc = -pc;
                            }
                            pr = (p <= 0) ? 0 : ((0 <= pc) ? b : 0);
                            a = bytes[cursor] = bytes[cursor] + pr;
                            cursor++;
                            for (i = 1; i < rowBytes; ++i, ++cursor) {
                                b = bytes[cursor - rowOffset], c = bytes[cursor - rowOffset - 1];
                                p = a + b - c;
                                pa = p - a;
                                if (pa < 0) {
                                    pa = -pa;
                                }
                                pb = p - b;
                                if (pb < 0) {
                                    pb = -pb;
                                }
                                pc = p - c;
                                if (pc < 0) {
                                    pc = -pc;
                                }
                                pr = (pa <= pb && pa <= pc) ? a : ((pb <= pc) ? b : c);
                                a = bytes[cursor] = (bytes[cursor] + pr) % 256;
                            }
                        }
                    }
                    else {
                        if (cursor == 1) {
                            cursor += pixelBytes;
                            b = c = 0;
                            for (i = pixelBytes; i < rowBytes; ++i, ++cursor) {
                                a = bytes[cursor - pixelBytes];
                                p = a + b - c;
                                pa = p - a;
                                if (pa < 0) {
                                    pa = -pa;
                                }
                                pb = p - b;
                                if (pb < 0) {
                                    pb = -pb;
                                }
                                pc = p - c;
                                if (pc < 0) {
                                    pc = -pc;
                                }
                                pr = (pa <= pb && pa <= pc) ? a : ((pb <= pc) ? b : c);
                                bytes[cursor] = (bytes[cursor] + pr) % 256;
                            }
                        }
                        else {
                            for (i = 0; i < pixelBytes; ++i, ++cursor) {
                                a = 0, b = bytes[cursor - rowOffset], c = 0;
                                p = a + b - c;
                                pa = p - a;
                                if (pa < 0) {
                                    pa = -pa;
                                }
                                pb = p - b;
                                if (pb < 0) {
                                    pb = -pb;
                                }
                                pc = p - c;
                                if (pc < 0) {
                                    pc = -pc;
                                }
                                pr = (pa <= pb && pa <= pc) ? a : ((pb <= pc) ? b : c);
                                bytes[cursor] = (bytes[cursor] + pr) % 256;
                            }
                            for (i = pixelBytes; i < rowBytes; ++i, ++cursor) {
                                a = bytes[cursor - pixelBytes], b = bytes[cursor - rowOffset], c = bytes[cursor - rowOffset - pixelBytes];
                                p = a + b - c;
                                pa = p - a;
                                if (pa < 0) {
                                    pa = -pa;
                                }
                                pb = p - b;
                                if (pb < 0) {
                                    pb = -pb;
                                }
                                pc = p - c;
                                if (pc < 0) {
                                    pc = -pc;
                                }
                                pr = (pa <= pb && pa <= pc) ? a : ((pb <= pc) ? b : c);
                                bytes[cursor] = (bytes[cursor] + pr) % 256;
                            }
                        }
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return bytes;
    };
    PngDecoder.decode = function (data) {
        var entry = PngFileEntry.allocEntry(data);
        var stream = new ByteStream();
        stream.open(data);
        while (stream.bytesAvailable() > 0) {
            var chunkSize = stream.getUint32();
            var section = stream.getUTF(4);
            var handler = PngDecoder.sectionHandlers[section];
            if (handler != null) {
                handler(entry, stream, chunkSize);
            }
            else {
                stream.skip(chunkSize);
            }
            var crc = stream.getUint32();
        }
        var pixels = PngDecoder.decodePixels(entry);
        var cursor = 0;
        var offset = 0;
        var width = entry.width;
        var height = entry.height;
        var buffer = new ArrayBuffer(width * height * PngDecoder.calcPixelBytes(entry) + 8);
        var result = new Uint8Array(buffer, 8);
        var view = new DataView(buffer, 0, 8);
        view.setUint32(0, width);
        view.setUint32(4, height);
        switch (entry.colorType) {
            case 3: {
                PngDecoder.fromPalette(entry, pixels, result);
                break;
            }
            case 2: {
                switch (entry.bits) {
                    case 8: {
                        for (var y = 0; y < height; ++y) {
                            offset++;
                            for (var x = 0; x < width; ++x) {
                                result[cursor++] = pixels[offset++];
                                result[cursor++] = pixels[offset++];
                                result[cursor++] = pixels[offset++];
                            }
                        }
                        break;
                    }
                    case 16: {
                        console.error("\u672A\u652F\u6301\u7684\u7C7B\u578B\uFF1A" + entry.colorType + ", " + entry.bits);
                        break;
                    }
                }
                break;
            }
            case 6: {
                switch (entry.bits) {
                    case 8: {
                        for (var y = 0; y < height; ++y) {
                            offset++;
                            for (var x = 0; x < width; ++x) {
                                result[cursor++] = pixels[offset++];
                                result[cursor++] = pixels[offset++];
                                result[cursor++] = pixels[offset++];
                                result[cursor++] = pixels[offset++];
                            }
                        }
                        break;
                    }
                    case 16: {
                        console.error("\u672A\u652F\u6301\u7684\u7C7B\u578B\uFF1A" + entry.colorType + ", " + entry.bits);
                        break;
                    }
                }
                break;
            }
            default: {
                console.error("\u672A\u652F\u6301\u7684\u7C7B\u578B\uFF1A" + entry.colorType + ", " + entry.bits);
                break;
            }
        }
        return buffer;
    };
    PngDecoder.fromPalette = function (entry, pixels, result) {
        var cursor = 0;
        var offset = 0;
        var width = entry.width;
        var height = entry.height;
        var palette = entry.palette;
        if (entry.transparency != null) {
            palette = PngDecoder.decodePalette(entry);
            switch (entry.bits) {
                case 1: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = (pixels[offset + (x >> 3)] & 0x01) * 4;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                            result[cursor++] = palette[index + 3];
                        }
                        offset += (width + 7) >> 3;
                    }
                    break;
                }
                case 2: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = (pixels[offset + (x >> 2)] & 0x03) * 4;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                            result[cursor++] = palette[index + 3];
                        }
                        offset += (width + 3) >> 2;
                    }
                    break;
                }
                case 4: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = (pixels[offset + (x >> 1)] & 0x0F) * 4;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                            result[cursor++] = palette[index + 3];
                        }
                        offset += (width + 1) >> 1;
                    }
                    break;
                }
                case 8: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = pixels[offset++] * 4;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                            result[cursor++] = palette[index + 3];
                        }
                    }
                    break;
                }
            }
        }
        else {
            switch (entry.bits) {
                case 1: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = (pixels[offset + (x >> 3)] & 0x01) * 3;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                        }
                        offset += (width + 7) >> 3;
                    }
                    break;
                }
                case 2: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = (pixels[offset + (x >> 2)] & 0x03) * 3;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                        }
                        offset += (width + 3) >> 2;
                    }
                    break;
                }
                case 4: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = (pixels[offset + (x >> 1)] & 0x0F) * 3;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                        }
                        offset += (width + 1) >> 1;
                    }
                    break;
                }
                case 8: {
                    for (var y = 0; y < height; ++y) {
                        offset++;
                        for (var x = 0; x < width; ++x) {
                            var index = pixels[offset++] * 3;
                            result[cursor++] = palette[index];
                            result[cursor++] = palette[index + 1];
                            result[cursor++] = palette[index + 2];
                        }
                    }
                    break;
                }
            }
        }
    };
    PngDecoder.sectionHandlers = {};
    return PngDecoder;
}());
var system;
(function (system) {
    var FileDecodeSystem = (function () {
        function FileDecodeSystem() {
        }
        return FileDecodeSystem;
    }());
    system.FileDecodeSystem = FileDecodeSystem;
})(system || (system = {}));
