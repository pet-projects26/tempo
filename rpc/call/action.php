<?php
include_once PHP_PB_DIR . 'php.php';
include_once PHP_PB_DIR . 'util.php';
include_once PHP_RPC_DIR . 'php.rpc.php';
function action($host,$port,$param){
    /*$host = '192.168.2.91';
    $port = '6864';*/
    if(!$host || !$port || empty($param)){
        exit();
    }

    $type = $param['type'];

    if(!isset($type) || !in_array($type,array(1,2,3))){
        exit();
    }
    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $temp = [];
    $code = null;
    switch($type){
        case 1:
            #get list
            //$ActionList = new \idlrpc\Sour_SV_B2UCommon();
            $backObj = new BackrpcList();
            $rpc->getBgCloseActionList_async($backObj);
            $client->rpcRead();
            $temp['msg'] = $backObj->code;
            break;
        case 2:
            $backObj = new BackrpcOpen();
            $action = new \msg\SActionList();
            $action->setActionlist($param['key']);//param type array()
            $rpc->openActionList_async($backObj,$action);
            $client->rpcRead();
            $code = (int)$backObj->code;//成功返回int 0
            break;
        case 3:
            $backObj = new BackrpcClose();
            $action = new \msg\SActionList();
            $action->setActionlist($param['key']);
            $rpc->closeActionList_async($backObj,$action);
            $client->rpcRead();
            $code = (int)$backObj->code;
            break;
        default:
            break;
    }
    return $code;
}

class BackrpcOpen extends \idlrpc\Back_SV_B2UCommon_openActionList{
    public $code = null;
    /**回调信息*/
    public function rpcResponse($msg){
        #more code for back
        $this->code = $msg->getCode();
    }
}

class BackrpcList extends \idlrpc\Back_SV_B2UCommon_getBgCloseActionList{
    public $code = null;

    public function rpcResponse($msg){
        #code for back
        $this->code = $msg->getActionlistList();
    }
}

class BackrpcClose extends \idlrpc\Back_SV_B2UCommon_closeActionList{
    public $code = null;

    /**回调信息*/
    public function rpcResponse($msg){
        #more code for back
        $this->code = $msg->getCode();
    }
}

?>