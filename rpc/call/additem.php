<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function additem($host , $port , $param){
	
    (empty($host) || empty($port)) && exit('item param error');
	
    foreach($param as $k => $v){
        ($v === '') && exit('item empty ' . $k);
    }

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
	
    $rpc = new \idlrpc\Sour_SV_B2TrCommon();
    $rpc->_session = $client->rpcBridging()->session();
	
	//设置值
	
	$BgTradeItemList= new \msg\BgTradeItemList(); 
	foreach($param as $k=>$v){
		$BgTradeItem = new \msg\BgTradeItem(); 
		$BgTradeItem->setUuid($v['uuid']);//唯一id
		$BgTradeItem->setItemId($v['itemId']); //物品id
		$BgTradeItem->setCount($v['count']);//数量
		$BgTradeItem->setPrice($v['price']);//价格
		
		$BgTradeItem->setGroupId($v['groupId']);//组id
		$BgTradeItem->setStartTm($v['startTm']);//上架时间
		$BgTradeItem->setEndTm($v['endTm']);//下架时间
		$BgTradeItemList->addList($BgTradeItem);
	}
	
    $backrpc = new Backrpc();
	
    $rpc->supplyItem_async($backrpc , $BgTradeItemList);
    $client->rpcRead();
	
	
    return $backrpc->code;
}
								
class Backrpc extends \idlrpc\Back_SV_B2TrCommon_supplyItem{
	
    public $code;
    public function rpcResponse($msg){
		
        $this->code = $msg->getCode();
    }
}