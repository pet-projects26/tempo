<?php
include_once PHP_PB_DIR . 'php.php';
include_once PHP_PB_DIR . 'util.php';
include_once PHP_RPC_DIR . 'php.rpc.php';

function ban($host , $port , $param){
    $delete = $param['delete'];
    $content = $param['content'];
    (empty($delete) || empty($content)) && exit('ban param error');
    \pblib\Protobuf::autoload();
    (empty($host) || empty($port)) && exit('rpc host or port param error');
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2LCommon();
    $rpc->_session = $client->rpcBridging()->session();
    if($delete == 2){
        $delbanlist = new \msg\DelBanList();
        foreach($content as $v){
            $delban = new \msg\DelBan();
            $delban->setId($v);
            $delbanlist->addDelBan($delban);
        }
        $backrpc = new DelBackrpc();
        $rpc->delBanList_async($backrpc , $delbanlist);
    }
    else if($delete == 1){
        $addbanlist = new \msg\AddBanList();
        $addbanlist->setType($param['type']);
        $addbanlist->setTime($param['time']);
        foreach($content as $k => $v){
            $addban = new \msg\addBan();
            $addban->setId($k);
            $addban->setName($v);
            $addbanlist->addAddBan($addban);
        }
        $backrpc = new AddBackrpc();
        $rpc->addBanList_async($backrpc , $addbanlist);
    }
    $client->rpcRead();
    return $backrpc->code;
}

class AddBackrpc extends \idlrpc\Back_SV_B2LCommon_addBanList{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class DelBackrpc extends \idlrpc\Back_SV_B2LCommon_delBanList{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}