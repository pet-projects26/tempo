<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function broadcast($host , $port , $param){
	
    (empty($host) || empty($port)) && exit('broadcast param error');
	
    foreach($param as $k => $v){
        ($v === '') && exit('broadcast empty ' . $k);
    }

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
	
    $rpc = new \idlrpc\Sour_SV_B2TCommon();
    $rpc->_session = $client->rpcBridging()->session();
	
	//设置值
	
	$BroadcastList= new \msg\BroadcastList(); 
	foreach($param as $k=>$v){
		$Broadcast = new \msg\Broadcast(); 
		$Broadcast->setId($v['id']);//id
		$Broadcast->setType($v['type']);//类型
		$Broadcast->setIntervaltime($v['intervaltime']);//间隔时间
		$Broadcast->setStarttime($v['starttime']);//开始时间
		$Broadcast->setEndtime($v['endtime']);//结束时间
		$Broadcast->setContent($v['content']);//内容
		$BroadcastList->setbroadcast($Broadcast);
	}

    $backrpc = new Backrpc();
    $rpc->sendBroadcast_async($backrpc , $BroadcastList);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2TCommon_sendBroadcast{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}