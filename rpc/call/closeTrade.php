<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function closeTrade($host , $port , $param){
	
    (empty($host) || empty($port)) && exit('Trade param error');
	
    foreach($param as $k => $v){
        ($v === '') && exit('Trade empty ' . $k);
    }

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
	
    $rpc = new \idlrpc\Sour_SV_B2TmCommon();
    $rpc->_session = $client->rpcBridging()->session();
	
	//设置值
	
	$ServerIdList= new \msg\ServerIdList(); 
	foreach($param as $k=>$v){
		$ServerId = new \msg\ServerId(); 
		$ServerId->setPId($v['pId']);//渠道id
		$ServerId->setSID($v['sId']); //服务器id
		$ServerIdList->addList($ServerId);
	}
	
    $backrpc = new Backrpc();
    $rpc->closeTrade_g_async($backrpc , $ServerIdList);
    $client->rpcRead();
	
    return $backrpc->code;
}
								
class Backrpc extends \idlrpc\Back_SV_B2TmCommon_closeTrade_g{
	
    public $code;
    public function rpcResponse($msg){
		
        $this->code = $msg->getCode();
    }
}