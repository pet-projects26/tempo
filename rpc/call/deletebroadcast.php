<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function deletebroadcast($host , $port , $param){
    (empty($host) || empty($port)) && exit('broadcast param error');
	
    foreach($param as $k => $v){
        ($v === '') && exit('broadcast empty ' . $k);
    }

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
	
    $rpc = new \idlrpc\Sour_SV_B2TCommon();
    $rpc->_session = $client->rpcBridging()->session();
	
	//设置值
	
	$BroadcastList= new \msg\DelBroadcastList(); 
	foreach($param as $k=>$v){
		$Broadcast = new \msg\DelBroadcast(); 
		$Broadcast->setId($v);//id
		$BroadcastList->setDelBroadcast($Broadcast);
	}
	
    $backrpc = new Backrpc();
    $rpc->delBroadcastList_async($backrpc , $BroadcastList);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2TCommon_delBroadcastList{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}