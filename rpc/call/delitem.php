<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function delitem($host , $port , $param){
	
    (empty($host) || empty($port)) && exit('item param error');
	
    foreach($param as $k => $v){
        ($v === '') && exit('item empty ' . $k);
    }

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
	
    $rpc = new \idlrpc\Sour_SV_B2TrCommon();
    $rpc->_session = $client->rpcBridging()->session();
	
	//设置值
	
	$BgTradeItemList= new \msg\BgTradeItemList(); 
	foreach($param as $k=>$v){
		$BgTradeItem = new \msg\BgTradeItem(); 
		$BgTradeItem->setUuid($v);//唯一id
		$BgTradeItemList->addList($BgTradeItem);
	}
    $backrpc = new Backrpc();
    $rpc->withdrawItem_async($backrpc , $BgTradeItemList);
    $client->rpcRead();
    return $backrpc->code;
}
								
class Backrpc extends \idlrpc\Back_SV_B2TrCommon_withdrawItem{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}