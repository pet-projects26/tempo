<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function gmcmd($host , $port , $param){
    $role_id = $param['role_id'];
    $cmd = $param['cmd'];
    (empty($role_id)) && (empty($cmd)) && exit('gmcmd param error');
    \pblib\Protobuf::autoload();
    $sgmcmd = new \msg\SGMCommand();
    $sgmcmd->setObjId($role_id);
    $sgmcmd->setCommand($cmd);

    (empty($host) || empty($port)) && exit('rpc host or port param error');
    $client = new Client($host , $port);

    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $backrpc = new Backrpc();
    $rpc->gMCommand_async($backrpc , $sgmcmd);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_gMCommand{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}