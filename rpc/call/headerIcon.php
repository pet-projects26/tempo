<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function headerIcon($host , $port , $param){
	
	
    $roleId = $param['roleId'];
    $version = $param['version'] + 1;
    
    (empty($roleId) || empty($version)) && exit('headIcon param error');
    \pblib\Protobuf::autoload();
    
    (empty($host) || empty($port)) && exit('rpc host or port param error');
    
    
    $bgHeadIconInfo = new \msg\BgHeadIconInfo();
    $bgHeadIconInfo->setObjId($roleId);
    $bgHeadIconInfo->setHeadVersion($version);
    
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $backrpc = new Backrpc();
    $rpc->updateHeadIcon_async($backrpc , $bgHeadIconInfo);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_updateHeadIcon{ 
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}