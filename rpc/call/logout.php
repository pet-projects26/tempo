<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function logout($host , $port , $param){
    $role_id = $param['role_id'];
    (empty($role_id)) && exit('logout param error');
    \pblib\Protobuf::autoload();
    $objid = new \msg\ObjId();
    $objid->setObjId($role_id);

    (empty($host) || empty($port)) && exit('rpc host or port param error');
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2LCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $backrpc = new Backrpc();
    $rpc->clearHuman_async($backrpc , $objid);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2LCommon_clearHuman{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}