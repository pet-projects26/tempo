<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function order($host , $port , $param){
    $order_num = $param['order_num'];
    $role_id = $param['role_id'];
    (empty($order_num) || empty($role_id)) && exit('order param error');
    \pblib\Protobuf::autoload();
    $recharge = new \msg\Recharge();
    $recharge->setOrderId($order_num);
    $recharge->setObjId($role_id);

    (empty($host) || empty($port)) && exit('rpc host or port param error');
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $backrpc = new Backrpc();
    $rpc->sendRechargeOrder_async($backrpc , $recharge);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_sendRechargeOrder{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}