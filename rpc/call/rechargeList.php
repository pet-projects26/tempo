<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function rechargeList($host , $port , $param){
   
    (empty($param) ) && exit('order param error');
    \pblib\Protobuf::autoload();

    $rechargeList = new \msg\RechargeList();
   
    foreach ($param['order'] as $key => $value) {
        $recharge = new \msg\Recharge();
        $recharge->setOrderId($value['order_num']);
        $recharge->setObjId($value['role_id']);
        $rechargeList->addList($recharge);
    }
   
    (empty($host) || empty($port)) && exit('rpc host or port param error');
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $backrpc = new Backrpc();




    $rpc->sendRechargeList_async($backrpc , $rechargeList);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_sendRechargeList{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}