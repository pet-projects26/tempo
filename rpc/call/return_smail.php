<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function return_smail($host , $port , $param){
    (empty($host) || empty($port)) && exit('smail param error');
    foreach($param as $k => $v){
        ($v === '') && exit('smail empty ' . $k);
    }


    //$objid = $param['role_id'];     //收件人
    $sender = $param['sender'];     //发件人
    $title = $param['title'];       //邮件标题
    $content = $param['content'];   //邮件内容
    $attach = isset($param['attach']) ? $param['attach'] : array();     //附件

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();

    //设置值
    $SEmailList = new \msg\SEmailList(); 
    

    foreach($attach as $k => $v){
        $semail = new \msg\SEmail();
        $semail->setObjIdList($v['role_id']);//设置邮件接受的角色ID
        $semail->setSender($sender);        //设置邮件发送人
        $semail->setTitle($title);          //设置邮件标题
        $semail->setTxt($content);          //设置邮件文本内容

        $semailattachment = new \msg\SEmailAttachment();        //附件
        $semailattachment->setGold($v['gold']);            //设置非绑定元宝

        $semail->setAm($semailattachment);  //设置附件

        $SEmailList->addEmailList($semail);

    }
    
    $backrpc = new Backrpc();
    $rpc->sendEmailList_async($backrpc , $SEmailList);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_sendEmailList{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}