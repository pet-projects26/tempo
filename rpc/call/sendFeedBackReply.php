<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function sendFeedBackReply($host , $port , $param){
    (empty($host) || empty($port)) && exit('reply param error');
    foreach($param as $k => $v){
        ($v === '') && exit('reply empty ' . $k);
    }
	
    $objid = $param['objId'];     //收件人
    $time = $param['time'];     //时间
    $type = $param['type'];       //类型
    $content = $param['content'];   //内容

    \pblib\Protobuf::autoload();

    $client = new Client($host , $port);


    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    
    $semail = new \msg\FeedBackReplyInfo();
    $semail->setObjId($objid);          //设置邮件接受的角色ID
    $semail->setTime($time);          //设置邮件标题
    $semail->setType($type);          //设置邮件标题
    $semail->setContent($content);          //设置邮件文本内容

    $backrpc = new Backrpc();
    $rpc->sendFeedBackReply_async($backrpc , $semail);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_sendFeedBackReply{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}