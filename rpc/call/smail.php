<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function smail($host , $port , $param){
    (empty($host) || empty($port)) && exit('smail param error');
    foreach($param as $k => $v){
        ($v === '') && exit('smail empty ' . $k);
    }


    $objid = $param['role_id'];     //收件人
    $sender = $param['sender'];     //发件人
    $title = $param['title'];       //邮件标题
    $content = $param['content'];   //邮件内容
    $attach = isset($param['attach']) ? $param['attach'] : array();     //附件

    \pblib\Protobuf::autoload();
    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    //设置值
    $semailattachment = new \msg\SEmailAttachment();        //附件
    if (!empty($attach)) {
        $semailattachment->setGold($attach['gold']);            //设置非绑定元宝
        $semailattachment->setBindGold($attach['bind_gold']);   //设置绑定元宝
        $semailattachment->setCopper($attach['copper']);        //设置铜钱数量
    }
   
    if(isset($attach['item'])){
        foreach($attach['item'] as $k => $v){                  //设置附件内容
            $sitem = new \msg\SItem();
            $sitem->setItemId($v['id']);
            $sitem->setCount($v['count']);
            $semailattachment->addItems($sitem);
        }
    }
    $semail = new \msg\SEmail();
    $semail->setObjIdList($objid);          //设置邮件接受的角色ID
    $semail->setSender($sender);        //设置邮件发送人
    $semail->setTitle($title);          //设置邮件标题
    $semail->setTxt($content);          //设置邮件文本内容
    $semail->setAm($semailattachment);  //设置附件

    $backrpc = new Backrpc();
    $rpc->sendEmail_async($backrpc , $semail);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_sendEmail{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}