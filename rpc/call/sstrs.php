<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function sstrs($host , $port , $param){
    $str = $param['str'];
	
    $netinfo = $param['netinfo'];
    $netinfos = array(1 , 2 , 3 , 4 , 5 , 6 ,7,8 );
    (empty($str) || empty($netinfo) || (!in_array($netinfo , $netinfos))) && exit('sstrs param error');
    \pblib\Protobuf::autoload();
    $stringList = new \msg\StringList();
	
    //$stringList->setCeilIndex(0);
    foreach($str as $v){
        $stringList->addList($v);
    }
	//print_r($stringList);exit;

    (empty($host) || empty($port)) && exit('rpc host or port param error');
    $client = new Client($host , $port);
    if($netinfo == '1'){
        $rpc = new \idlrpc\Sour_SV_B2UCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backurpc();
        $rpc->hotupdate_async($backrpc , $stringList);
        $client->rpcRead();
        return $backrpc->code;
    }
    else if($netinfo == '2'){
        $rpc = new \idlrpc\Sour_SV_B2MCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backcrpc();
        $rpc->hotupdate_async($backrpc , $stringList);
        $client->rpcRead();
        return $backrpc->code;
    }
    else if($netinfo == '3'){
        $rpc = new \idlrpc\Sour_SV_B2TsCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backtsrpc();
        $rpc->hotupdate_async($backrpc , $stringList);
        $client->rpcRead();
        return $backrpc->code;
    }
    else if($netinfo == '4'){
        $rpc = new \idlrpc\Sour_SV_B2TrCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backtrrpc();
        $rpc->hotupdate_async($backrpc , $stringList);
        $client->rpcRead();
        return $backrpc->code;
    }
    else if($netinfo == '5'){
        $rpc = new \idlrpc\Sour_SV_B2LCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backlrpc();
        $rpc->hotupdate_async($backrpc , $stringList);
        $client->rpcRead();
        return $backrpc->code;
    }
	else if($netinfo == '6'){
        $rpc = new \idlrpc\Sour_SV_B2UCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backcheckrpc();
		$int =  new \msg\Int();
		$int->setVal($str[0]);
		
        $rpc->check_async($backrpc ,$int );
        $client->rpcRead();
        return $backrpc->code;
    }
	else if($netinfo == '7'){
        $rpc = new \idlrpc\Sour_SV_B2TmCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backtmrpc();
        $rpc->hotupdate_async($backrpc ,$stringList );
        $client->rpcRead();
        return $backrpc->code;
    }
    else if($netinfo == '8'){
        $rpc = new \idlrpc\Sour_SV_B2GCommon();
        $rpc->_session = $client->rpcBridging()->session();
        $backrpc = new Backcrossrpc();
        $rpc->hotupdate_async($backrpc ,$stringList );
        $client->rpcRead();
        return $backrpc->code;
    }
}

class Backurpc extends \idlrpc\Back_SV_B2UCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backcrpc extends \idlrpc\Back_SV_B2MCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backtsrpc extends \idlrpc\Back_SV_B2TsCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backtrrpc extends \idlrpc\Back_SV_B2TrCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backlrpc extends \idlrpc\Back_SV_B2LCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backcheckrpc extends \idlrpc\Back_SV_B2UCommon_check{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backtmrpc extends \idlrpc\Back_SV_B2TmCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}

class Backcrossrpc extends \idlrpc\Back_SV_B2GCommon_hotupdate{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}