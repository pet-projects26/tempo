<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

function uactivity($host , $port){
    (empty($host) || empty($port)) && exit('rpc host or port param error');

    ini_set('display_errors' , 'On');
    error_reporting(E_ALL);

    \pblib\Protobuf::autoload();

    $client = new Client($host , $port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();
    $rpc->_session = $client->rpcBridging()->session();
    $backrpc = new Backrpc();
    $rpc->updateAllActivity_async($backrpc);
    $client->rpcRead();
    return $backrpc->code;
}

class Backrpc extends \idlrpc\Back_SV_B2UCommon_updateAllActivity{
    public $code;
    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}