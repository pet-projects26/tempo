<?php
include PHP_PB_DIR . 'php.php';
include PHP_PB_DIR . 'util.php';
include PHP_RPC_DIR . 'php.rpc.php';

/** 大R公告 */
/**
 * @param string $host
 * @param string $port
 * @param array $data
*/
function upRechMember($host,$port,$data){
    if(empty($host) || empty($port) || empty($data)){
        exit();
    }
    $type = $data['type'];
    $handel_type = $data['handel_type'];
    if(!isset($type) || !isset($handel_type) || !in_array($type,array('1','2','3'))
        || !in_array($handel_type,array('up','down'))){
        exit();
    }
    \pblib\Protobuf::autoload();
    $client = new Client($host,$port);
    $rpc = new \idlrpc\Sour_SV_B2UCommon();//UpRechMemeber
    $rpc->_session = $client->rpcBridging()->session();
    $code = null;//成功0，失败1
    switch($handel_type){
        case 'up': #上架
            #code for up
            $backObj = new BackRpcRes();
            $aciton  = new \msg\UpRechMember();
            $aciton->setType(intval($type));
            $rpc->upRechMember_async($backObj,$aciton);
            $client->rpcRead();
            $code = (int)$backObj->code;
            break;
        case 'down': #下架
            #code for down
            $backObj = new BackRpcRes();
            $aciton = new \msg\UpRechMember();
            $aciton->clearType();
            $rpc->upRechMember_async($backObj,$aciton);
            $client->rpcRead();
            $code = (int)$backObj->code;
            break;
        default:
            $code = 2;
            break;
    }
    return $code;
}

class BackRpcRes extends \idlrpc\Back_SV_B2UCommon_upRechMember{
    public $code = null;

    public function rpcResponse($msg){
        $this->code = $msg->getCode();
    }
}
?>