<?php

namespace idlrpc{

/*************************************** SV_B2LCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2LCommon_clearHuman extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2LCommon_addBanList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2LCommon_delBanList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2LCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2LCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2LCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_LOGIN);
	}
	public function clearHuman_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
	public function addBanList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 1, $backobj, $msg);
	}
	public function delBanList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 2, $backobj, $msg);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 3, $backobj, $msg);
	}
}



/*************************************** SV_B2LCommon end *************************************/

/*************************************** SV_B2UCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_sendRechargeOrder extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_sendEmail extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_gMCommand extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_updateAllActivity extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_sendFeedBackReply extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_check extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_getBgCloseActionList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\SActionList();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_openActionList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_closeActionList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_sendEmailList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_upRechMember extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_sendRechargeList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2UCommon_updateHeadIcon extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2UCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2UCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_USER);
	}
	public function sendRechargeOrder_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
	public function sendEmail_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 1, $backobj, $msg);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 2, $backobj, $msg);
	}
	public function gMCommand_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 3, $backobj, $msg);
	}
	public function updateAllActivity_async($backobj)
	{
		\Operator::invokeAsync($this, 4, $backobj, NULL);
	}
	public function sendFeedBackReply_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 5, $backobj, $msg);
	}
	public function check_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 6, $backobj, $msg);
	}
	public function getBgCloseActionList_async($backobj)
	{
		\Operator::invokeAsync($this, 7, $backobj, NULL);
	}
	public function openActionList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 8, $backobj, $msg);
	}
	public function closeActionList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 9, $backobj, $msg);
	}
	public function sendEmailList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 10, $backobj, $msg);
	}
	public function upRechMember_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 11, $backobj, $msg);
	}
	public function sendRechargeList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 12, $backobj, $msg);
	}
	public function updateHeadIcon_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 13, $backobj, $msg);
	}
}



/*************************************** SV_B2UCommon end *************************************/

/*************************************** SV_B2TCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TCommon_addBroadcastList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TCommon_delBroadcastList extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2TCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2TCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_MANAGER);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
	public function addBroadcastList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 1, $backobj, $msg);
	}
	public function delBroadcastList_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 2, $backobj, $msg);
	}
}



/*************************************** SV_B2TCommon end *************************************/

/*************************************** SV_B2MCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2MCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2MCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2MCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_CEIL);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
}



/*************************************** SV_B2MCommon end *************************************/

/*************************************** SV_B2TsCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TsCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2TsCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2TsCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_TRADE_S);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
}



/*************************************** SV_B2TsCommon end *************************************/

/*************************************** SV_B2TrCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TrCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TrCommon_supplyItem extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TrCommon_withdrawItem extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2TrCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2TrCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_TRADE_R);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
	public function supplyItem_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 1, $backobj, $msg);
	}
	public function withdrawItem_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 2, $backobj, $msg);
	}
}



/*************************************** SV_B2TrCommon end *************************************/

/*************************************** SV_B2TmCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TmCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TmCommon_openTrade_g extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TmCommon_closeTrade_g extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2TmCommon_updateGroup extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2TmCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2TmCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_TRADE_M);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
	public function openTrade_g_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 1, $backobj, $msg);
	}
	public function closeTrade_g_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 2, $backobj, $msg);
	}
	public function updateGroup_async($backobj)
	{
		\Operator::invokeAsync($this, 3, $backobj, NULL);
	}
}



/*************************************** SV_B2TmCommon end *************************************/

/*************************************** SV_B2GCommon head*************************************/

/*remote invoke back class：implements rpcResponse and rpcException*/
abstract class Back_SV_B2GCommon_hotupdate extends \RpcObject
{
	abstract public function rpcResponse($msg);
	public function rpcException($err){}
	public function __response($buf)
	{
		$msg = new \msg\RetCode();
		\Protocol::parseFromArray($msg, $buf);
		return $this->rpcResponse($msg);
	}
}


/*remote invoke sour class*/
class Sour_SV_B2GCommon extends \ProxyObject
{
	function __construct()
	{
		$this->_identity = "SV_B2GCommon";
		$this->_netNumber = \NetInfo::NET_NUMBER_toStruct(\NetInfo::NET_NUMBER_BG, \NetInfo::NET_NUMBER_CROSSMGR);
	}
	public function hotupdate_async($backobj, $msg)
	{
		\Operator::invokeAsync($this, 0, $backobj, $msg);
	}
}



/*************************************** SV_B2GCommon end *************************************/

}
?>
