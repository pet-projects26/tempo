<?php

namespace pblib\Protobuf\Codec\Binary;

class Unknown extends \pblib\Protobuf\Unknown
{
    public function __construct($tag, $type, $data)
    {
        $this->tag = $tag;
        $this->type = $type;
        $this->data = $data;
    }
}
