<?php

namespace pblib\Protobuf;

interface CodecInterface
{
    public function encode(\pblib\Protobuf\Message $message);
    public function decode(\pblib\Protobuf\Message $message, $data);
}