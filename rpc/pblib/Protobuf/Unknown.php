<?php

namespace pblib\Protobuf;

abstract class Unknown
{
    public $tag = 0;
    public $data = null;
}
