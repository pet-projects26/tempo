<?php
ini_set('display_errors', 'off');
error_reporting(0);
date_default_timezone_set('Asia/Shanghai');
defined('IN_WEB') OR define('IN_WEB' , true);
defined('ROOT') OR define('ROOT' , __DIR__);

$server_id = isset($_REQUEST['s']) ?  $_REQUEST['s'] : '';

if ($server_id) {
    define('CONFIG_SERVER_ID' , $server_id);
    //内网测试的服务器使用的配置，一套单服代码多份db配置
    require_once ROOT . '/config/db_' . $server_id . '.php';
} else {
    //外网正式服配置
    require_once ROOT . '/config/db.php';
}
require_once ROOT . '/includes/Util.php';

require_once ROOT . '/includes/Helper.php';


$sign = isset($_REQUEST['sign']) ? $_REQUEST['sign'] : '';
$unixtime = isset($_REQUEST['unixtime']) ? $_REQUEST['unixtime'] : 0;
$data = isset($_REQUEST['data']) ?  $_REQUEST['data'] : '';



if (empty($sign)) {
    die(json_encode(array('code' => '-1', 'msg' => 'no Sign')));
}

if (empty($unixtime)) {
    die(json_encode(array('code' => '-2', 'msg' => 'no unixtime')));
}

if (empty($data)) {
    die(json_encode(array('code' => '-3', 'msg' => 'no  data')));
}

$checkSign  = Helper::checkSign($_POST, MDKEY, $sign);

if (!$checkSign ) {
    die(json_encode(array('code' => '-4', 'msg' => 'error Sign')));
}



defined('DATA_DIR') OR  define('DATA_DIR' , ROOT . '/../bin/config/config/');
spl_autoload_register('autoload');
$dirs = array(
    get_include_path(),
    ROOT . '/includes',
    ROOT . '/controller',
    ROOT . '/model'
);


set_include_path(implode(PATH_SEPARATOR , $dirs));


$params  = Helper::authcode($data, 'DECODE');

$params = json_decode($params, true);

$_instance = new ComModel();

$return = array();

$i = 1;

foreach ($params as $method => $array) {
    
    $data = $array['data'];
    // $where = $array['where'];
    // 
    switch ($method) {
        case 'query':
            $return[$method] = $_instance->queryData($data);
            break;
        case 'update':
            $table = isset($array['table']) ? $array['table'] : '';
            if (empty($table)) {
                die(json_decode(array('code' => '-5', 'msg' => 'no table')));
            }

            $_instance->setTable($table); 
            $where = $array['where'];

            $return[$method] = $_instance->update($data, $where);
            break;
        case 'insert':
            $table = isset($array['table']) ? $array['table'] : '';
            if (empty($table)) {
                die(json_decode(array('code' => '-5', 'msg' => 'no table')));
            }

            $_instance->setTable($table);

            $return[$method] = $_instance->add($data);
            break;

        case 'delete':
            $table = isset($array['table']) ? $array['table'] : '';
            if (empty($table)) {
                die(json_decode(array('code' => '-5', 'msg' => 'no table')));
            }

            $where = $array['where'];

            $return[$method] = $_instance->delete($array['where']);

            break;
        default:
            # code...
            break;
    }
}

die(json_encode(array('code' => 1, 'msg' => 'success', 'data' => $return)));


function autoload($className){
    @include_once $className . '.php';
}


