<?php
define('MYSQL_HOST' , '127.0.0.1');
define('MYSQL_PORT' , 3306);
define('MYSQL_USER' , 'root');
define('MYSQL_PASSWD' , 'root');
define('MYSQL_DB' , 'game_1');
define('MYSQL_PREFIX' , 'ny_');
define('MONGO_HOST' , '');
define('MONGO_PORT' , 0);
define('MONGO_USER' , '');
define('MONGO_PASSWD' , '');
define('MONGO_DB' , '');
define('GM_PORT' , 8012);
define('MDKEY' , '');
define('OPEN_TIME' , 1564563600);
define('CLOSE_TIME' , 0);
define('REDIS_HOST' , '127.0.0.1');
define('REDIS_PORT' , 6379);
define('REDIS_PASSWD' , '');
define('REDIS_PREFIX' , '');
define('REDIS_DB' , 1);
define('WEBSOCKET_HOST' , '127.0.0.1');
define('WEBSOCKET_PORT' , 8012);
define('CONFIGS_PATH' , '/root/server/publish/configs');
