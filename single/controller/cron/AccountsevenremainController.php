<?php

class AccountsevenremainController extends CronController{
    private $sr;
    private $alg;
    private $ro;

    public function __construct(){
        $this->sr = new SevenremainModel();
        $this->alg = new AccLoginModel();
        $this->ro = new RoleModel();
    }

    public function run(array $argv = null){
        $date = date('Y-m-d' , time() - 300);//预防其他定时任务执行的php的执行时间过长，减300是预留一些时间
		$data = array();
		$result=$this->ro->getRoleByDate('eq' , $date); //获取今天创建的角色
		if($result){
			foreach($result as $k=>$v){
				$arr[]=$v['role_id'];	
			}
			$array=$this->ro->getAccByRole($arr); //获取今天创建的帐号
			$data['account_num']=count($array);
		}else{
			$data['account_num']=0;	
		}
		
		$seven_remain = $this->sr->getAccountSevenRemainByDate($date);
		if(!$seven_remain){
			$data['date'] = strtotime($date);
			$this->sr->setAccountSevenRemain($data);
		}
		else{
			$this->sr->setAccountSevenRemain($data , $date);
		}
		
		$this->updateSevenRemain();
        
    }

    //更新7日数据
    private function updateSevenRemain(){
		$time = time() - 300;
        $days = array('two' => 1 , 'three' => 2 , 'four' => 3 , 'five' => 4 , 'six' => 5 , 'seven' => 6 , 'eight' => 7 , 'nine' => 8, 'ten' => 9 ,'eleven' => 10 ,'twelve' => 11 ,'thirteen' => 12 ,'fourteen' => 13 ,'fifteen' => 14 ,'sixteen' => 15 ,'seventeen' => 16 ,'eighteen' =>17 ,'nineteen' =>18 ,'twenty' =>19 ,'twenty_one' =>20 ,'twenty_two' =>21 ,'twenty_three' =>22 ,'twenty_four' =>23 ,'twenty_five' =>24 ,'twenty_six' =>25 ,'twenty_seven' =>26 ,'twenty_eight' =>27 ,'twenty_nine' =>28 ,'thirty' =>29 ,); //第N天
        foreach($days as $k => $v){
            $date = date('Y-m-d' , $time - $v * 86400); //获取第N天前的日期
            $seven_remain = $this->sr->getAccountSevenRemainByDate($date);
            if($seven_remain){
                $roles = $this->ro->getRoleByDate('eq' , $date);
				if($roles){
					foreach($roles as $key=>$val){
						$arr[]=$val['role_id'];	
					}
					$array=$this->ro->getAccByRole($arr);	
				}
                if(!empty($array)){
                    $account_num = count($array);
					
                    $login_num = $this->alg->getAccountLogNumByDate(date('Y-m-d' , $time) , $array); //获取第N天前创建帐号在今天登录的情况
                    $data = array($k => $login_num . '(' . sprintf("%.2f" , $login_num / $account_num * 100) . '%)');
					
                }
                else{
                    $data = array($k => '0(0%)');
                }
                $this->sr->setAccountSevenRemain($data , $date);
            }
        }
    }
}