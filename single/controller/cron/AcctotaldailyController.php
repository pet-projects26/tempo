<?php

class AcctotaldailyController extends CronController{
    private $atd;

    public function __construct(){
        $this->atd = new AcctotaldailyModel();
        $this->ro = new RoleModel();
        $this->lg = new LoginModel();
        $this->od = new OrderModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
        $acctotaldaily = $this->atd->getAccTotaldailyByDate($date , array('id'));
        if(!$acctotaldaily){
            $data = array();
            $roles = $this->ro->getRoleByDate('eq' , $date);
            $account = $this->ro->getAccByRole($roles);
            $data['reg_num'] = count($account); //注册数
            $data['login_num'] = $this->lg->getLogNumByDate($date); //登录数
            $old_roles = $this->lg->getRoleOlTimeByDate($date , 1);
            $role_ids = array();
            if($old_roles){
                foreach($old_roles as $k => $v){
                    $role_ids[] = $k;
                }
            }
            $old_account = $this->ro->getAccByRole($role_ids);
            $data['old_num'] = count($old_account); //老玩家数
            $data['pay_num'] = $this->od->getOrderNumByDate($date); //充值人数
            $data['money'] = $this->od->getOrderMoneyByDate($date); //充值金额
            $data['money'] = $data['money'] ? $data['money'] : 0;
            $data['lc_percent'] = $data['pay_num'] && $data['login_num'] ? sprintf('%.2f' , $data['pay_num'] / $data['login_num'] * 100) . '%' : '0%'; //登录付费率
            $data['arpu'] = $data['money'] && $data['pay_num'] ? sprintf('%.2f' , $data['money'] / $data['pay_num']) : 0; //总AP
            $data['first_pay_num'] = $this->od->getOrderNumByDate($date , $account); //新充值人数
            $data['first_money'] = $this->od->getOrderMoneyByDate($date , $account); //新充值金额
            $data['first_money'] = $data['first_money'] ? $data['first_money'] : 0;
            $data['first_percent'] = $data['first_money'] && $data['first_pay_num'] ? sprintf('%.2f' , $data['first_money'] / $data['first_pay_num'] * 100) . '%' : '0%'; //新增付费率
            $data['first_arpu'] = $data['first_money'] && $data['reg_num'] ? sprintf('%.2f' , $data['first_money'] / $data['reg_num']) : 0; //新充值AP
            $data['old_pay_num'] = $this->od->getOrderNumByDate($date , $account); //老充值人数
            $data['old_money'] = $this->od->getOrderMoneyByDate($date , $account); //老充值金额
            $data['old_money'] = $data['old_money'] ? $data['old_money'] : 0;
            $data['old_percent'] = $data['old_pay_num'] && $data['old_num'] ? sprintf('%.2f' , $data['first_money'] / $data['first_pay_num'] * 100) . '%' : '0%'; //老付费率
            $data['old_arpu'] = $data['old_money'] && $data['old_pay_num'] ? sprintf('%.2f' , $data['first_money'] / $data['reg_num']) : 0; //老充值AP
            $data['reg_arpu'] = $data['money'] && $data['reg_num'] ? sprintf('%.2f' , $data['money'] / $data['reg_num']) : 0; //注册AP
            $data['date'] = $time;
            $this->atd->setAccTotaldaily($data);
        }
    }
}