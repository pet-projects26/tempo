<?php

class ActiveController extends CronController{
    private $a;
    private $acc;
    private $lg;

    public function __construct(){
        $this->a  = new ActiveModel();
        $this->acc = new AccLoginModel();
        $this->lg = new LoginModel();
    }

    public function run(array $argv = null){
        $time = time();
        $date = date('Y-m-d' , $time);
        $time = strtotime($date);
        $acctotaldaily = $this->a->getRowData($time);
        if(!$acctotaldaily){
            $data = array();

            $data['active_num'] = $this->acc->getAccountLogNumByDate($date); //活跃数
            $data['login_num'] = $this->lg->getLogTimesByDate($date); //登录次数

            $data['avg_login_num'] = round($data['login_num'] / $data['active_num'], 2);
            $acc_longtime_arr = $this->lg->getOlTimeByDate($date); //玩家在线总时长

            $acc_longtime = 0;

            if (!empty($acc_longtime_arr)) {
                foreach ($acc_longtime_arr as $value) {
                    $acc_longtime += $value;
                }
            }
            $data['avg_login_longtime'] = round($acc_longtime / $data['active_num'], 2); // 人均在线时长

            $data['new_acc_num'] = $this->acc->getNewAccountLogNumByDate($date, 0); //新增用户数

            $new_acc = $this->acc->getNewAccountByDate($date, 0);

            if (!empty($new_acc)) {
                //获取新增玩家的总登录次数
                $new_acc_login_num = $this->lg->getLogTimesByDate($date, $new_acc);
                //新增用户在线总时长
                $new_acc_login_longtime_arr  = $this->lg->getOlTimeByDate($date, $new_acc);
                $new_acc_login_longtime = 0;
                foreach ($new_acc_login_longtime_arr as $value) {
                    $new_acc_login_longtime += $value;
                }
            } else {
                $new_acc_login_num = 0;
                $new_acc_login_longtime = 0;
            }

            $data['new_acc_avg_login_num'] = round($new_acc_login_num / $data['new_acc_num'], 2); //新增用户平均登录次数

            $data['new_acc_avg_login_longtime'] = round($new_acc_login_longtime/ $data['new_acc_num'], 2); //新增用户平均登录时长

            $data['old_acc_num'] = $this->acc->getOldAccNum($date); //老玩家数

            $data['old_acc_num'] = $data['old_acc_num'] ? $data['old_acc_num'] : 0;

            if ($data['old_acc_num']) {
                //获取老玩家
                $old_acc = $this->acc->getOldAcc($date);

                $old_acc_login_num = $this->lg->getLogTimesByDate($date, $old_acc);

                $old_acc_login_longtime = $this->lg->getOlTimeByDate($date, $old_acc);

                $data['old_acc_avg_login_num'] = round($old_acc_login_num / $data['old_acc_num'], 2); //老用户平均登录次数

                $data['old_acc_avg_login_longtime'] = round($old_acc_login_longtime/ $data['old_acc_num'], 2); //老用户平均登录时长
            } else {
                $data['old_acc_avg_login_num'] = 0; //老用户平均登录次数

                $data['old_acc_avg_login_longtime'] = 0; //老用户平均登录时长
            }

            $data['create_time'] = $time;

            $this->a->setActive($data);
        }
    }
}