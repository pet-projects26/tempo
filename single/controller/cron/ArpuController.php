<?php
class ArpuController extends CronController{
    private $ap;
    private $cg;
    private $lg;
    private $ro;

    public function __construct(){
        $this->ap = new ArpuModel();
        $this->cg = new ChargeModel();
        $this->lg = new LoginModel();
        $this->ro = new RoleModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
        $arpu = $this->ap->getArpuByDate($date);
        if(!$arpu){
            $charges = $this->cg->getChargeByDate($date , array() , array('account' , 'money' , 'first'));
            $login_num = $this->lg->getLogNumByDate($date);
            $roles = $this->ro->getRoleByDate('eq' , $date);
            $role_ids = array();
            if(is_array($roles) && $roles){
                foreach($roles as $row){
                    $role_ids[] = $row['role_id'];
                }
            }
            $accounts = $this->ro->getAccByRole($role_ids);
            $reg_num = count($accounts); //注册数
            $sum_money = 0;
            $first_sum_money = 0;
            $accounts = array();
            $first_accounts = array();
            foreach($charges as $row){
                $sum_money += $row['money'];
                $accounts[] = $row['account'];
                if($row['first'] == 1){
                    $first_sum_money += $row['money'];
                    $first_accounts[] = $row['account'];
                }
            }
            $accounts = array_unique($accounts);
            $first_accounts = array_unique($first_accounts);
            $data = array(
                'date' => $time,
                'arpu' => $login_num ? sprintf("%.2f" , $sum_money / $login_num) : 0,
                'arppu' => $sum_money ? sprintf("%.2f" , $sum_money / count($accounts)) : 0,
                'first_arpu' => $first_sum_money ? sprintf("%.2f" , $first_sum_money / count($first_accounts)) : 0,
                'reg_arpu' => $reg_num ? sprintf("%.2f" , $sum_money / $reg_num) : 0
            );
            $this->ap->addArpu($data);
        }
    }
}