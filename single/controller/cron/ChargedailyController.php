<?php
class ChargedailyController extends CronController{
    private $cd;
    private $cg;
    private $ro;
    private $lg;

    public function __construct(){
        $this->cd = new ChargedailyModel();
        $this->cg = new ChargeModel();
        $this->ro = new RoleModel();
        $this->lg = new LoginModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
        $daily = $this->cd->getDailyByDate($date);
        if(!$daily){
            $charges = $this->cg->getChargeByDate($date , array() , array('account' , 'money' , 'first'));
            $login_num = $this->lg->getLogNumByDate($date);
            $role_num = $this->ro->getRoleByDate('ecount' , $date);
            $sum_money = 0;
            $first_sum_money = 0;
            $accounts = array();
            $first_accounts = array();
            $first_times = 0;
            foreach($charges as $row){
                $sum_money += $row['money'];
                $accounts[] = $row['account'];
                if($row['first'] == 1){
                    $first_sum_money += $row['money'];
                    $first_accounts[] = $row['account'];
                    $first_times += $row['first'];
                }
            }
            $accounts = array_unique($accounts);
            $first_accounts = array_unique($first_accounts);
            $data = array(
                'date' => $time,
                'money' => $sum_money,
                'pay_num' => count($accounts),
                'pay_times' => count($charges),
                'first_money' => $first_sum_money,
                'first_num' => count($first_accounts),
                'first_times' => $first_times,
                'arpu' => $login_num ? sprintf("%.2f" , $sum_money / $login_num) : 0,
                'role_num' => $role_num,
                'login_num' => $login_num
            );
            $this->cd->addDaily($data);
        }
    }
}