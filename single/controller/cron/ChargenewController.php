
<?php
class ChargenewController extends CronController{
    private $cn;
    private $cg;
    private $ro;

    public function __construct(){
        $this->cn = new ChargenewModel();
        $this->cg = new ChargeModel();
        $this->ro = new RoleModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
        $chargenew = $this->cn->getChargenewByDate($date);
        if(!$chargenew){
            $role = $this->ro->getRoleByDate('eq' , $date);//新增人数
            $new_accounts = array();
            foreach($role as $row){
                $new_accounts[] = $row['account'];
            }
            $new_accounts = array_unique($new_accounts);
            $charges = $this->cg->getChargeByDate($date , array() , array('account' , 'money') , 1);
            $sum_money = 0;
            $first_accounts = array();
            foreach($charges as $row){
                $sum_money += $row['money'];
                $first_accounts[] = $row['account'];
            }
            $first_accounts = array_unique($first_accounts);
            $data = array(
                'date' => $time,
                'first_num' => count($first_accounts),
                'new_num' => count($new_accounts),
                'new_money' => $sum_money,
                'new_percent' => count($new_accounts) ? sprintf("%.2f" , count($first_accounts) / count($new_accounts) * 100) . '%' : '0%'
            );
            $this->cn->addChargenew($data);
        }
    }
}