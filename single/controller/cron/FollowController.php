<?php

class FollowController extends CronController{
    private $fl;
    private $cg;

    public function __construct(){
        $this->fl = new FollowModel();
        $this->cg = new ChargeModel();
    }

    public function run(array $argv = null){
        if (empty($argv)) {
          
            $start_time = strtotime(date('Y-m-d')) - 86400;
            $end_time = $start_time;

        } elseif (!empty($argv[0]) && !isset($argv[1])) {
            
            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));
        
        } elseif (!empty($argv[0]) && !empty($argv[1])) {
            
            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }


        $num = ceil(($end_time - $start_time) / 86400) + 1;

        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
           
            //并发执行
            //$msg = CronBase::runmultiprocess('statLtv', 'stat', $serList, $start, $end);
            
            $this->setFollow($start);
        //
        }

        die();
       
    }

    public function setFollow($time){
        
        $date = date('Y-m-d' , $time);

        $follow = $this->fl->getFollowByDate($date);
        if(!$follow){
            $charges = $this->cg->getChargeByDate($date);
            $sum_money = 0;
            $accounts = array();
            foreach($charges as $row){
                $sum_money += $row['money'];
                $accounts[] = $row['account'];      //获取昨天有充值的玩家
            }
            $accounts = array_unique($accounts);
            $data = array(
                'date' => strtotime($date),
                'pay_num' => count($accounts),
                'money' => $sum_money
            );
            $this->fl->setFollow($data);
        }
        $this->updateFollow($date);
    }


    public function updateFollow($date){

        $time = strtotime($date) - 86400;        //获取昨天日期的时间戳
        $date = date('Y-m-d' , $time);
        $days = array('one' => 1 , 'two' => 2 , 'three' => 3 , 'four' => 4 , 'five' => 5 , 'six' => 6 , 'seven' => 7 , 'fourteen' => 14 , 'thirty' => 30); //第N天
        foreach($days as $k => $v){
            $old_date = date('Y-m-d' , $time - $v * 86400);  //获取昨天的前N天的日期

            $follow = $this->fl->getFollowByDate($old_date);
            if(count($follow) && $follow['money']){
                $charges = $this->cg->getChargeByDate($old_date , array() , array('account')); //获取第N天有充值的玩家
                $accounts = array();
                foreach($charges as $row){
                    $accounts[] = $row['account'];
                }
                $accounts = array_unique($accounts);
                $charges = $this->cg->getChargeByDate($date , $accounts , array('money')); //第N天有充值的玩家在昨天的充值情况
                $sum_money = 0;
                foreach($charges as $row){
                    $sum_money += $row['money'];
                }
                $data = array($k => $sum_money . '(' . sprintf("%.2f" , $sum_money / $follow['money'] * 100) . '%)');
            }
            else{
                $data = array($k => '0(0%)');
            }
            $this->fl->setFollow($data , $old_date);
        }
    }
}
