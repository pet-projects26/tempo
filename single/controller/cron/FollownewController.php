<?php

class FollownewController extends CronController{
    private $fn;
    private $ro;
    private $cg;

    public function __construct(){
        $this->fn = new FollownewModel();
        $this->ro = new RoleModel();
        $this->cg = new ChargeModel();
    }

    public function run(array $argv = null){
         if (empty($argv)) {
          
            $start_time = strtotime(date('Y-m-d'));
            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {
            
            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));
        
        } elseif (!empty($argv[0]) && !empty($argv[1])) {
            
            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }


        $num = ceil(($end_time - $start_time) / 86400) + 1;

        //执行命令
        for ($i = 0; $i < $num; $i++) {
            $start = $start_time + $i * 86400;
           
            //并发执行
            //$msg = CronBase::runmultiprocess('statLtv', 'stat', $serList, $start, $end);
            
            $this->setFollownew($start);
        //
        }

        die();
        
    }

    public function setFollownew($time){
        $date = date('Y-m-d' , $time);
        $follownew = $this->fn->getFollownewByDate($date);
        if(!$follownew){
            $role = $this->ro->getRoleByDate('eq' , $date);
            $accounts = array();
            foreach($role as $row){
                $accounts[] = $row['account'];
            }
            $accounts = array_unique($accounts);
            $charges = $this->cg->getChargeByDate($date , $accounts , array('account' , 'money'));
            $sum_money = 0;
            $pay_accounts = array();
            foreach($charges as $row){
                $sum_money += $row['money'];
                $pay_accounts[] = $row['account'];
            }
            $pay_accounts = array_unique($pay_accounts);
            $data = array(
                'date' => $time,
                'new_num' => count($pay_accounts),
                'money' => $sum_money
            );
            $this->fn->setFollownew($data);
        }
        $this->updateFollownew($date);
    }

    public function updateFollownew($date){
        $time = strtotime($date) - 86400;        //获取昨天日期的时间戳
        $date = date('Y-m-d' , $time);
        $days = array('one' => 1 , 'two' => 2 , 'three' => 3 , 'four' => 4 , 'five' => 5 , 'six' => 6 , 'seven' => 7 , 'fourteen' => 14 , 'thirty' => 30); //第N天
        foreach($days as $k => $v){
            $old_date = date('Y-m-d' , $time - $v * 86400);
            $follow = $this->fn->getFollownewByDate($old_date);
            if(count($follow) && $follow['money'] != 0){
                $role = $this->ro->getRoleByDate('eq' , $old_date);
                $accounts = array();
                foreach($role as $row){
                    $accounts[] = $row['account'];
                }
                $accounts = array_unique($accounts);
                $charges = $this->cg->getChargeByDate($old_date , $accounts , array('account')); //获取第N天有充值的玩家
                $pay_accounts = array();
                foreach($charges as $row){
                    $pay_accounts[] = $row['account'];
                }
                $pay_accounts = array_unique($pay_accounts);
                $charges = $this->cg->getChargeByDate($date , $pay_accounts , array('account' , 'money')); //第N天有充值的玩家在昨天的充值情况
                $sum_money = 0;
                $pay_accounts = array();
                foreach($charges as $row){
                    $sum_money += $row['money'];
                    $pay_accounts[] = $row['account'];
                }
                $data = array($k => $sum_money . '(' . count($pay_accounts) . ')');
            }
            else{
                $data = array($k => '0(0)');
            }
            $this->fn->setFollownew($data , $old_date);
        }
    }
}
