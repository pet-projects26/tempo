<?php
require_once(dirname(__FILE__).'/../../includes/Smtp.class.php');
/** 设置服务器提示，当挂机+在线人数上限达到1900时发送邮件给运营人员 */
class GameSerChangeTipsController extends CronController{

    private $commonTxt;
    private $smtpserver; //SMTP服务器
    private $smtpservrport; //SMTP服务器端口
    private $smtpmailto; //SMTP服务器的用户邮箱
    private $smtpusermail; //SMTP服务器的用户帐号，注：部分邮箱只需@前面的用户名
    private $smtppass; //SMTP服务器的用户密码
    private $role_num = 1900;


    /**305795548@qq.com
     * @mark 获取服务器配置信息，主要是读取开服时间
    */
    public function __construct(){
        /**-----------------配置信息--------------------------*/
        $this->smtpserver = 'smtp.163.com';
        $this->smtpservrport = 25;
        $this->smtpmailto = '453284325@qq.com';//运营人员邮箱
        $this->smtpusermail = '13790304207@163.com';
        $this->smtppass = '932000XTYisBOY';
        /**-----------------配置信息-------------------------*/

        $file = dirname(__FILE__).'/../../../server.j';//服务器配置文件
        if(!file_exists($file)){
            die("can't find the common file");
        }
        $serverInfo = self::removeCommon(file_get_contents($file));
        if(empty($serverInfo)) exit;
        $this->commonTxt = json_decode($serverInfo,true);//[server][server_info][start_time]
    }

    /**
     * @mark 执行方法，拟定仅开服当天执行该脚本
     * @param array $arr
    */
    public function run(array $arr = null){
        //获取当天时间
        $time = date('Ymd',time());
        $serverInfo = $this->commonTxt['server']['server_info'];
        $serverStarTime = $serverInfo['start_time'];
        if(empty($serverStarTime)) die("can't find the server start time");
        if($time != $serverStarTime){
            exit;     //不是开服当天
        }
        //判断是否为二次发送
        $logFile = './send_mail_record'.$serverStarTime.$serverInfo['server_id'].'.txt';
        $sendInfo = @file_get_contents($logFile);

        if(trim($sendInfo) == 'true'){
            //已经发送成功
            exit;
        }

        $smtp = new Smtp($this->smtpserver,$this->smtpservrport,true,$this->smtpusermail,$this->smtppass);
        $smtp->debug = false;//是否显示发送的调试信息
        $now = time();//当前时刻
        $dayStart = mktime('00','00','00',date('m'),date('d'),date('Y'));//当天开始
        $sql = "select MAX(`role_num` + `hook_num`) as num from ny_online_hook where
              `create_time` BETWEEN '".$dayStart."' AND '".$now."' and (`role_num` + `hook_num`) >= '".$this->role_num."'";
        //对该服务器用户注册量进行统计，开服当天
        //$sql = "select count(distinct(account)) as num from ny_role where create_time >='".strtotime($serverStarTime)."'";
        $roleNum = (new Model())->query($sql);

        $roleNum = $roleNum[0]['num'];
        //没有用户导量
        if(empty($roleNum)){
            exit("can't find any account");
        }
        $mailTitle = "切服提示！";
        $mailtype = "HTML";//邮件格式（HTML/TXT）,TXT为文本邮件
        $mailContent = '<span style="font-weight: 600">渠道组为：<em>'.$serverInfo['platform'].'</em></span><br/>'.
            '<span style="font-weight: 600">服务器序号为：<em>'.$serverInfo['server_id'].'</em></span><br/>'.
            '<span style="font-weight: 600">服务器标识为：<em>'.$serverInfo['server_name'].'</em></span><br/>'.
            '服务器玩家数目已经达到<span>'.$this->role_num.'</span>上限，当前玩家数为：<span style="color: #ca1b36;font-weight:400">'.$roleNum.'</span> 请尽快执行切换服务器操作！';

        $state = $smtp->sendmail($this->smtpmailto,$this->smtpusermail,$mailTitle,$mailContent,$mailtype);
        //error_log(json_encode($state)."\n\r",3,'error.log');//记录操作
        if($state == ''){
            $info = 'false';
        }else{
            $info = 'true';
        }
        file_put_contents('./send_mail_record'.$time.$serverInfo['server_id'].'.txt',trim($info));
    }

    /**
     * @mark 清除注释信息，获取完整json字符串
     * @param string $content
     * @return string
    */
    private static function removeCommon($content){
        return preg_replace("/(\/\*.*?\*\/)|(#.*?\n)|(\/\/.*?\n)/s" , '' , str_replace(array("\r\n" , "\r") , "\n" , $content));
    }
}
?>