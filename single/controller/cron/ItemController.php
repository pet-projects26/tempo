<?php

class ItemController extends CronController{
    private $pm;
    private $cp;
    private $item;

    public function __construct(){
        $this->pm = new PaymentModel();
        $this->cp = new CpModel();
        $this->item = new ItemModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
		//$time = strtotime('2017-05-27') - 86400;
        $date = date('Y-m-d' , $time);
        foreach(CDict::$coinType as $k => $row){
            $item = $this->item->getItemByDateType($date , $k);
            if(!$item){
                if($row['key'] == 'goods'){
                    $this->setGoods($date);
                }
                else{
					
                    $this->setCoin($date , $k);
                }
           }
        }
    }

    //设置货币
    public function setCoin($date , $type){
        $data = array();
		$lastday=date('Y-m-d',strtotime($date)-86400);
        $data['date'] = strtotime($date);
        $coin = CDict::$coinType[$type]['key'];
		
		$usesql = "select sum($coin) as $coin from ny_payment where type =0 and $coin>0 and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$use = $this->pm->query($usesql);
		$data['use_num'] = $use[0][$coin] ? $use[0][$coin] : 0;//消耗货币
		
		$usenumsql="select count(DISTINCT(role_id)) as role_num from ny_payment where type =0 and $coin>0 and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$usenum = $this->pm->query($usenumsql);
		$data['use_role_num'] = $usenum[0]['role_num'] ? $usenum[0]['role_num'] : 0;//参与消耗的角色数
		
		$getsql = "select sum($coin) as $coin from ny_payment where type =1 and $coin>0 and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$get = $this->pm->query($getsql);
		$data['get_num'] = $get[0][$coin] ? $get[0][$coin] : 0;//产出货币
		
		$getnumsql="select count(DISTINCT(role_id)) as role_num from ny_payment where type = 1 and $coin>0 and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$getnum = $this->pm->query($getnumsql);
		$data['get_role_num'] = $getnum[0]['role_num'] ? $getnum[0]['role_num'] : 0;//参与产出的角色数
		
		$inventorysql = "select inventory from ny_item where from_unixtime(date,'%Y-%m-%d') = '$lastday' and type= $type ";
		$lastinventory = $this->pm->query($inventorysql);
		$inventory = $lastinventory[0]['inventory'] ?  $lastinventory[0]['inventory'] : 0;
		$data['inventory']= $data['get_num'] -  $data['use_num'] + $inventory;
		
        $data['type'] = $type;
        $this->item->setItem($data);
    }

    //设置道具
    public function setGoods($date){
        $data = array();
		$lastday=date('Y-m-d',strtotime($date)-86400);
        $data['date'] = strtotime($date);
		
		$usesql = "select sum(item_num) as item_num from ny_consume_produce where type =0  and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$use = $this->pm->query($usesql);
		$data['use_num'] = $use[0]['item_num'] ? $use[0]['item_num'] : 0;//消耗道具
		
		
		$usenumsql = "select count(DISTINCT(role_id)) as role_num from ny_consume_produce where type =0  and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$usenum = $this->pm->query($usenumsql);
		$data['use_role_num'] = $usenum[0]['role_num'] ? $usenum[0]['role_num'] : 0;//参与消耗的角色数
		
		
		$getsql = "select sum(item_num) as item_num from ny_consume_produce where type =1  and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$get = $this->pm->query($getsql);
		$data['get_num'] = $get[0]['item_num'] ? $get[0]['item_num'] : 0;//产出货币
		
		$getnumsql="select count(DISTINCT(role_id)) as role_num from ny_consume_produce where type = 1  and from_unixtime(create_time,'%Y-%m-%d') = '$date'";
		$getnum = $this->pm->query($getnumsql);
		$data['get_role_num'] = $getnum[0]['role_num'] ? $getnum[0]['role_num'] : 0;//参与产出的角色数
		
		$inventorysql = "select inventory from ny_item where from_unixtime(date,'%Y-%m-%d') = '$lastday' and type = 4 ";
		$lastinventory = $this->pm->query($inventorysql);
		$inventory = $lastinventory[0]['inventory'] ?  $lastinventory[0]['inventory'] : 0;
		$data['inventory']= $data['get_num'] -  $data['use_num'] + $inventory;
		
        $data['type'] = 4;

        $this->item->setItem($data);
    }
}