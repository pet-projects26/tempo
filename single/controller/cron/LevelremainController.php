<?php

class LevelremainController extends CronController
{
    private $o;
    private $r;

    public function run(array $argv = null)
    {
        if (empty($argv)) {
            $time = time();
            $start_time = strtotime(date('Y-m-d'));

            //当天零点的时候会重跑昨天的数据
            if ($time - $start_time < 1200) {
                $start_time = $start_time - 86400;
            }

            $end_time = $start_time;
        } elseif (!empty($argv[0]) && !isset($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime(date('Y-m-d'));

        } elseif (!empty($argv[0]) && !empty($argv[1])) {

            $start_time = strtotime($argv[0]);
            $end_time = strtotime($argv[1]);
        }

        if ($start_time > $end_time) {
            die ('start time > end time');
        }

        $end_time = strtotime(date('Ymd', $start_time)) + 86399;

        //执行命令
        $msg = $this->stat($start_time, $end_time);
        echo $msg;
        die();

    }

    public function stat($start_time, $end_time)
    {

        $date = date('Y-m-d', $start_time);
        $time = strtotime($date);

        $nowTime = time();

        $nowDate = date('Y-m-d', $nowTime);

        if (strtotime($date) <= strtotime($nowDate)) {
            $nowTime = $time + 86400;
            $nowDate = date('Y-m-d', $nowTime);
        }

        $this->o = new LevelremainModel();

        $todayData = $this->o->getRowData($nowDate);

        if ($todayData) {
            $msg = 'Data already exists on the day';
            return $msg;
        }
        $this->r = new RoleModel();

        $data = array();

        $fields = array('count(role_id) as role_count');

        $method = $this->r->getRow($fields); //玩家总数量

        $fields = array('count(role_id) as level_count');
        $fields[] = 'role_level as level';
        $conditions['Extends']['GROUP'] = 'role_level';
        $conditions['WHERE'] = [];
        $rs = $this->r->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        unset($conditions['WHERE']);

        $start_time = $start_time * 1000;
        $end_time = $end_time * 1000;

        foreach ($rs as $k => $v) {
            $rs[$k]['role_count'] = $method['role_count'];

            $rs[$k]['level_rate'] = sprintf("%.2f", $v['level_count'] / $method['role_count'] * 100) . '%';

            $sql = "SELECT count(role_id) as level_loss_num FROM ny_role  WHERE  (last_login_time <= '$start_time'  or last_login_time >= '$end_time')  AND role_level = '" . $v['level'] . "' LIMIT 1";
            //查出当前等级流失数
            $level_loss_num = $this->r->query($sql);

            $rs[$k]['level_loss_num'] = $level_loss_num[0]['level_loss_num'];

            $rs[$k]['level_loss_rate'] = $level_loss_num[0]['level_loss_num'] ? sprintf("%.2f", $level_loss_num[0]['level_loss_num'] / $v['level_count'] * 100) . '%' : '0.00%';

            //查询两天前该等级人数获取滞留人数
            $where = [];
            $where['level::>='] = $v['level'];

            $LastTwoDay = $nowTime - (86400 * 2);

            $where["from_unixtime(create_time,'%Y-%m-%d')"] = date('Y-m-d', $LastTwoDay);

            $stay = $this->o->getRow('sum(level_count) as level_count', $where);

            $rs[$k]['stay_num'] = $stay['level_count'] ? $stay['level_count'] : 0;

            $rs[$k]['create_time'] = $nowTime;

            array_push($data, $rs[$k]);
        }


        if (!empty($data)) {
            if ($this->o->multiAdd($data)) {
                $msg = 'success';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'data is empty ~';
        }

        return $msg;
    }

}