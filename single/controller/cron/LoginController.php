<?php

class LoginController extends CronController{
    private $lg;
    private $ld;

    public function __construct(){
        $this->lg = new LoginModel();
        $this->ld = new LogindailyModel();
    }

    public function run(array $argv = null){
	    $time = time() - 1200;
		$date = date('Y-m-d' , $time);
        $login = $this->ld->getLogByDate($date);
		$data = array();
		
		$roles = $this->lg->getRoleOlTimeByDate($date);//角色在线时长
		
		//登录人数
		$data['lg_num'] = $this->lg->getRoleLogNumByDate($date);
		//登录次数
		$data['lg_times'] = $this->lg->getRoleLogTimesByDate($date);
		//平均登录次数
		$data['avg_lg_times'] = $data['lg_num'] ? sprintf("%.1f" , $data['lg_times'] / $data['lg_num']) : 0;
		//老角色数
		$old_roles = $this->lg->getRoleOlTimeByDate($date , 1);

		$data['old_num'] = count($old_roles);
		//老玩家平均在线时长
		$data['avg_old_oltime'] = empty($data['old_num']) ? 0 : round(array_sum($old_roles) / $data['old_num']);
		//活跃玩家数
		$data['atv_num'] = 0;
		//忠实玩家数
		$data['loy_num'] = 0;
		$two_roles = $this->lg->getRoleOlTimeByDate(date('Y-m-d' , $time - 86400));      //昨天有登录的玩家的在线时长
		
		$one_roles = $this->lg->getRoleOlTimeByDate(date('Y-m-d' , $time - 86400 * 2));  //前天有登录的玩家的在线时长
		
		foreach($roles as $role => $time){
			($time >= 1800) && $data['atv_num'] ++; //一天在线大于30分钟则算活跃玩家
			if(array_key_exists($role , $two_roles) && array_key_exists($role , $one_roles)){
				//如果是今天，昨天和前天都有登录的玩家，且这三天在线总时长大于等于5小时则算是忠实玩家
				$time += $two_roles[$role] + $one_roles[$role];
				($time >= 18000) && $data['loy_num'] ++;
			}
		}
		if(!$login){
			$data['date'] = strtotime($date);
			$this->ld->add($data);
		}
		else{
			$this->ld->update($data , array('date' => strtotime($date)));
		}
    }
}