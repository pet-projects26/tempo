<?php

class LtvController extends CronController{
    private $ltv;
    private $ro;
    private $cg;

    public function __construct(){
        $this->ltv = new LtvModel();
        $this->ro = new RoleModel();
        $this->cg = new ChargeModel();
        $this->db = new Model();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);

        $start_time = strtotime($date); 

        //查找出创角玩家数
        $role_num = $this->ro->getCreateRoleNum($date);
        $acc_num = $this->ro->getCreateAccountNum($date);



        //删除当天的数据
        $this->ltv->delRecordByDate($date);

        $arr = array(
            array('type' => 1,  'num' => $role_num, 'date' => $start_time),
            array('type' => 2,  'num' => $acc_num, 'date' => $start_time ),
        );

        foreach ($arr as $data) {
            $this->ltv->setRecord($data);
        }

        //更新
        $this->updateLtv($start_time);
    }

    /**
     * 更新ltv值 
     * @param int $time 当前统计时间
     *
     */
    private function updateLtv($time){
        $days = array(
            'one' => 1 , 'two' => 2 , 'three' => 3 , 'four' => 4 , 'five' => 5 , 'six' => 6 ,
            'seven' => 7 , 'fourteen' => 14 , 'thirty' => 30 , 'sixty' => 60 , 'ninety' => 90
        ); //第N天


        foreach($days as $k => $v){
            $num  = $v - 1;

            $reg_start = $time - $num * 86400; 
            $reg_end  = $reg_start + 86399;

            $charge_start = $reg_start;
            $charge_end = $time + 86399;

            //角色
            $sql = "SELECT SUM(money) AS money FROM ny_order AS charge  
                        LEFT JOIN ny_role AS role ON charge.role_id = role.role_id 
                    WHERE role.create_time BETWEEN {$reg_start} AND {$reg_end} 
                        AND charge.create_time BETWEEN {$charge_start} AND {$charge_end}";

            $moneyRes = $this->db->fetchOne($sql);
            $money = empty($moneyRes['money']) ? 0.00 : $moneyRes['money'];

            $sql = "UPDATE ny_ltv SET `{$k}` = {$money} WHERE `date` = {$reg_start} AND type = 1";
            $this->db->query($sql);

            //帐号

            $sql = "SELECT SUM(money) AS money FROM ny_order AS charge  
                        LEFT JOIN (
                            SELECT MIN(create_time) AS `time`, account FROM ny_role GROUP BY account 
                        ) AS tmp 
                        ON charge.account = tmp.account 
                    WHERE tmp.time BETWEEN {$reg_start} AND {$reg_end} 
                        AND charge.create_time BETWEEN {$charge_start} AND {$charge_end}";


            $moneyRes = $this->db->fetchOne($sql);
            $money = empty($moneyRes['money']) ? 0.00 : $moneyRes['money'];


            $sql = "UPDATE ny_ltv SET `{$k}` = {$money} WHERE `date` = {$reg_start} AND type = 2";
            $this->db->query($sql);
        }

        return true;
    }
}