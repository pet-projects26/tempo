<?php

class RankController extends CronController{
    public function __construct(){
        $this->r = new RankModel();
        $this->role = new RoleModel();
    }
    public function run(array $argv = null){
		
		$db=Util::mongoConn();

		$collection = $db->rank;
		$time=time();
		//等级
		$fields=array('name'=>true,'level'=>true);
		$where=array('level'=>array('$gt'=>0));
		$cursor=$collection->find($where)->fields($fields)->sort(array('level'=>-1,'levelTm'=>1))->limit(100);
		$levelarr=array();
		$leveli=1;
		foreach($cursor as $k=>$v){
			$levelarr[$leveli]['date']=$time;
			$levelarr[$leveli]['name']=$v['name'];
			$levelarr[$leveli]['rank']=$leveli;
			$levelarr[$leveli]['contents']=$v['level'];
			$levelarr[$leveli]['type']=0;
			$leveli++;
		}
		if(!empty($levelarr)){
			$this->r->multiAdd($levelarr);
		}
		
		//坐骑
		$fields=array('name'=>true,'rideGrade'=>true,'rideLevel'=>true);
		$where=array('rideGrade'=>array('$gt'=>0),'rideLevel'=>array('$gt'=>0));
		$cursor=$collection->find($where)->fields($fields)->sort(array('rideGrade'=>-1,'rideLevel'=>-1,'rideTm'=>1))->limit(100);
		$ridearr=array();
		$ridei=1;
		foreach($cursor as $k=>$v){
			$ridearr[$ridei]['date']=$time;
			$ridearr[$ridei]['name']=$v['name'];
			$ridearr[$ridei]['rank']=$ridei;
			$ridearr[$ridei]['contents']=$v['rideGrade'].'/'.$v['rideLevel'];
			$ridearr[$ridei]['type']=1;
			$ridei++;
		}

		if(!empty($ridearr)){
			$this->r->multiAdd($ridearr);
		}

		//宝石
		$fields=array('name'=>true,'gem'=>true);
		$where=array('gem'=>array('$gt'=>0));
		$cursor=$collection->find($where)->fields($fields)->sort(array('gem'=>-1,'gemTm'=>1))->limit(100);
		$gemarr=array();
		$gemi=1;
		foreach($cursor as $k=>$v){
			$gemarr[$gemi]['date']=$time;
			$gemarr[$gemi]['name']=$v['name'];
			$gemarr[$gemi]['rank']=$gemi;
			$gemarr[$gemi]['contents']=$v['gem'];
			$gemarr[$gemi]['type']=2;
			$gemi++;
		}

		if(!empty($gemarr)){
			$this->r->multiAdd($gemarr);
		}
		
		//战力
		$fields=array('name'=>true,'ce'=>true);
		$where=array('ce'=>array('$gt'=>0));
		$cursor=$collection->find($where)->fields($fields)->sort(array('ce'=>-1,'ceTm'=>1))->limit(100);
		$cearr=array();
		$cei=1;
		foreach($cursor as $k=>$v){
			$cearr[$cei]['date']=$time;
			$cearr[$cei]['name']=$v['name'];
			$cearr[$cei]['rank']=$cei;
			$cearr[$cei]['contents']=$v['ce'];
			$cearr[$cei]['type']=3;
			$cei++;
		}
		if(!empty($cearr)){
			$this->r->multiAdd($cearr);
		}
		
		//充值
		$fields=array('name'=>true,'diamond'=>true);
		$where=array('diamond'=>array('$gt'=>0));
		$cursor=$collection->find($where)->fields($fields)->sort(array('diamond'=>-1,'diamondTm'=>1))->limit(100);
		$diamondarr=array();
		$diamondi=1;
		foreach($cursor as $k=>$v){
			$diamondarr[$diamondi]['date']=$time;
			$diamondarr[$diamondi]['name']=$v['name'];
			$diamondarr[$diamondi]['rank']=$diamondi;
			$diamondarr[$diamondi]['contents']=$v['diamond'];
			$diamondarr[$diamondi]['type']=4;
			$diamondi++;
		}

		if(!empty($diamondarr)){
			$this->r->multiAdd($diamondarr);
		}

		//先删除之前存在的视图
		$delete_gold_sql = "drop view if exists view_payment_gold;";
		$rs = $this->r->query($delete_gold_sql);
		
		//创建消费元宝视图（因为数据量大，查询慢，所以改用视图）
		$sql = "create view view_payment_gold as  select role_name,sum(gold) as gold from ny_payment where type = 0  and gold>0 GROUP BY role_id order by  gold desc limit 100;";

		$rs = $this->r->query($sql);

		$sql = "select * from view_payment_gold";

		$gold=$this->r->query($sql);

		$goldarr = array();
		$goldi = 1;
		foreach($gold as $k=>$v){
			$goldarr[$goldi]['date']=$time;
			$goldarr[$goldi]['name']=$v['role_name'];
			$goldarr[$goldi]['rank']=$goldi;
			$goldarr[$goldi]['contents']=$v['gold'];
			$goldarr[$goldi]['type']=5;
			$goldi++;
		}

		if(!empty($goldarr)){
			$this->r->multiAdd($goldarr);
		}

		//先删除之前存在的视图
		$delete_bindgold_sql = "drop view if exists view_payment_bind_gold;";
		$this->r->query($delete_bindgold_sql);
		
		//消费绑元
		$sql="create view view_payment_bind_gold as select role_name,sum(bind_gold) as bind_gold from ny_payment where type = 0 and bind_gold>0 GROUP BY role_id order by  bind_gold desc limit 100;";

		$rs = $this->r->query($sql);

		$sql = "select * from view_payment_bind_gold";

		$bindgold=$this->r->query($sql);
		$bindgoldarr = array();
		$bindgoldi = 1;
		foreach($bindgold as $k=>$v){
			$bindgoldarr[$bindgoldi]['date']=$time;
			$bindgoldarr[$bindgoldi]['name']=$v['role_name'];
			$bindgoldarr[$bindgoldi]['rank']=$bindgoldi;
			$bindgoldarr[$bindgoldi]['contents']=$v['bind_gold'];
			$bindgoldarr[$bindgoldi]['type']=6;
			$bindgoldi++;
		}

		if(!empty($bindgoldarr)){
			$this->r->multiAdd($bindgoldarr);
		}

		//先删除之前存在的视图
		$delete_copper_sql = "drop view if exists view_payment_copper;";
		$this->r->query($delete_copper_sql);
		
		//消费铜钱
		$sql="create view view_payment_copper as  select role_name,sum(copper) as copper from ny_payment where type = 0 and copper>0 GROUP BY role_id order by  copper desc limit 100;";

		$rs = $this->r->query($sql);
		
		$sql = "select * from view_payment_copper";

		$copper=$this->r->query($sql);

		$copperarr = array();
		$copperi = 1;
		foreach($copper as $k=>$v){
			$copperarr[$copperi]['date']=$time;
			$copperarr[$copperi]['name']=$v['role_name'];
			$copperarr[$copperi]['rank']=$copperi;
			$copperarr[$copperi]['contents']=$v['copper'];
			$copperarr[$copperi]['type']=7;
			$copperi++;
		}

		if(!empty($copperarr)){

			$this->r->multiAdd($copperarr);
		}
		
		//内丹
		$neidansql = "select name,shenhun from ny_role where shenhun >0 order by shenhun desc limit 100";
		$neidan  = $this->role->query($neidansql);

		$neidanarr = array();
		$neidani = 1;
		foreach($neidan as $k=>$v){
			$neidanarr[$neidani]['date']=$time;
			$neidanarr[$neidani]['name']=$v['name'];
			$neidanarr[$neidani]['rank']=$neidani;
			$neidanarr[$neidani]['contents']=$v['shenhun'];
			$neidanarr[$neidani]['type']=8;
			$neidani++;
		}

		if(!empty($neidanarr)){

			$this->r->multiAdd($neidanarr);
		}

		//翅膀
		$wingsql = "select name,wing from ny_role where wing >0 order by wing desc limit 100";
		$wing  = $this->role->query($wingsql);

		$wingarr = array();
		$wingi = 1;
		foreach($wing as $k=>$v){
			$wingarr[$wingi]['date']=$time;
			$wingarr[$wingi]['name']=$v['name'];
			$wingarr[$wingi]['rank']=$wingi;
			$wingarr[$wingi]['contents']=$v['wing'];
			$wingarr[$wingi]['type']=9;
			$wingi++;
		}

		if(!empty($wingarr)){

			$this->r->multiAdd($wingarr);
		}

		//强化
		$strongsql = "select name,strong from ny_role where strong >0 order by strong desc limit 100";
		$strong  = $this->role->query($strongsql);

		$strongarr = array();
		$strongi = 1;
		foreach($strong as $k=>$v){
			$strongarr[$strongi]['date']=$time;
			$strongarr[$strongi]['name']=$v['name'];
			$strongarr[$strongi]['rank']=$strongi;
			$strongarr[$strongi]['contents']=$v['strongi'];
			$strongarr[$strongi]['type']=10;
			$strongi++;
		}

		if(!empty($strongarr)){

			$this->r->multiAdd($strongarr);
		}

		//坐骑魂石
		$zuojihunshisql = "select name,zuojihunshi from ny_role where zuojihunshi >0 order by zuojihunshi desc limit 100";
		$zuojihunshi  = $this->role->query($zuojihunshisql);

		$zuojihunshiarr = array();
		$zuojihunshii = 1;
		foreach($zuojihunshi as $k=>$v){
			$zuojihunshiarr[$zuojihunshii]['date']=$time;
			$zuojihunshiarr[$zuojihunshii]['name']=$v['name'];
			$zuojihunshiarr[$zuojihunshii]['rank']=$zuojihunshii;
			$zuojihunshiarr[$zuojihunshii]['contents']=$v['zuojihunshi'];
			$zuojihunshiarr[$zuojihunshii]['type']=11;
			$zuojihunshii++;
		}

		if(!empty($zuojihunshiarr)){

			$this->r->multiAdd($zuojihunshiarr);
		}

		//灵兽
		$beastsql = "select name,beast from ny_role where beast >0 order by beast desc limit 100";
		$beast  = $this->role->query($beastsql);

		$beastarr = array();
		$beasti = 1;
		foreach($beast as $k=>$v){
			$beastarr[$beasti]['date']=$time;
			$beastarr[$beasti]['name']=$v['name'];
			$beastarr[$beasti]['rank']=$beasti;
			$beastarr[$beasti]['contents']=$v['beast'];
			$beastarr[$beasti]['type']=12;
			$beasti++;
		}

		if(!empty($beastarr)){

			$this->r->multiAdd($beastarr);
		}
		
	}
}
