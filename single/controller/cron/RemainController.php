<?php

class RemainController extends CronController{
    private $rm;
    private $lg;
    private $cg;

    public function __construct(){
        $this->rm = new RemainModel();
        $this->lg = new LoginModel();
        $this->cg = new ChargeModel();
    }
    
    public function run(array $argv = null){

        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);

        $remain = $this->rm->getRemainByDate($date);
        if(!$remain){
            $pay_num = $this->cg->getChargeNumByDate($date);
            $data = array(
                'date' => strtotime($date),
                'pay_num' => $pay_num
            );
            $this->rm->setRemain($data);
        }
        $this->updateReamin();
    }

    /*
     * 更新旧数据
     * */
    private function updateReamin(){
        $time = strtotime(date('Y-m-d')) - 86400;        //获取昨天日期的时间戳
        $days = array('one' => 1 , 'two' => 2 , 'three' => 3 , 'four' => 4 , 'five' => 5 , 'six' => 6 , 'seven' => 7 , 'fourteen' => 14 , 'thirty' => 30); //第N天
        foreach($days as $k => $v){
            $date = date('Y-m-d' , $time - $v * 86400);         //获取第N天前的日期
            $remain = $this->rm->getRemainByDate($date);
            if(count($remain) && $remain['pay_num']){
                $charges = $this->cg->getChargeByDate($date); //获取第N天有充值的角色的ID
                $accounts = array();
                foreach($charges as $row){
                    $accounts[] = $row['account'];
                }
                $accounts = array_unique($accounts);
                $log_num = $this->lg->getLogNumByDate(date('Y-m-d' , $time) , $accounts); //第N天有充值的角色在昨天的登录情况
                $data = array($k => sprintf("%.2f" , $log_num / $remain['pay_num'] * 100) . '%');
            }
            else{
                $data = array($k => '0%');
            }
            $this->rm->setRemain($data , $date);
        }
    }
}