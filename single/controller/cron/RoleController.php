<?php

class RoleController extends CronController{
    private $ro;
    private $pm;
    private $lv;
    private $lg;
    private $rd;

    public function __construct(){
        $this->ro = new RoleModel();
        $this->pm = new PaymentModel();
        $this->lv = new LevelModel();
        $this->lg = new LoginModel();
        $this->rd = new RoledailyModel();
    }

    //每十分钟执行一次
    public function run(array $argv = null){
        $end_time = floor(time() / 10) * 10;
        $start_time = $end_time - 10 * 60;

        $start_time *= 1000;
        $end_time *= 1000;

        //10分钟内创建的角色
        $rsr = $this->ro->getRoleByDate('between' , $start_time , $end_time , array('id#asc'));
        $rsr && $this->rd->addRoles($rsr);
        //10分钟内的货币流水
        $rsc = $this->pm->getCoinsByDates($start_time , $end_time);
        $role_ids = array();
        $data = array();
        foreach($rsc as $row){
            $role_ids[] = $row['role_id'];
            $row['type'] = $row['type'] ? 1 : -1;
            $gold = $row['type'] * $row['gold'];
            $bind_gold = $row['type'] * $row['bind_gold'];
            $copper = $row['type'] * $row['copper'];
            $sorce = $row['type'] * $row['sorce'];

            $data[$row['role_id']]['gold'] += $gold;
            $data[$row['role_id']]['bind_gold'] += $bind_gold;
            $data[$row['role_id']]['copper'] += $copper;
            $data[$row['role_id']]['sorce'] += $sorce;
        }
        $rsrd = $this->rd->getRoles($role_ids , $fields = array('role_id' , 'gold' , 'bind_gold' , 'copper' , 'sorce'));
        foreach($rsrd as $row){
            $data[$row['role_id']]['gold'] += $row['gold'];
            $data[$row['role_id']]['bind_gold'] += $row['bind_gold'];
            $data[$row['role_id']]['copper'] += $row['copper'];
            $data[$row['role_id']]['sorce'] += $row['sorce'];
        }
        //10分钟内的等级流水
        $rsl = $this->lv->getMaxLevelsByDates($start_time , $end_time);
        foreach($rsl as $row){
            $data[$row['role_id']]['level'] = $row['level'];
        }
        //10分钟内的登录流水
        $rslg = $this->lg->getLastLogsByDates($start_time , $end_time);
        foreach($rslg as $row){
            $data[$row['role_id']]['last_login_time'] = $row['last_login_time'];
        }
        $this->rd->updateRoles($data);  //更新已存在的角色
    }
}
