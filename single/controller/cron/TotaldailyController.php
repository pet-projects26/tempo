<?php

class TotaldailyController extends CronController{
    private $td;
    private $ro;
    private $lg;
    private $od;
    private $ol;

    public function __construct(){
        $this->td = new TotaldailyModel();
        $this->ro = new RoleModel();
        $this->lg = new LoginModel();
        $this->od = new OrderModel();
        $this->ol = new OnlineModel();
    }

    public function run(array $argv = null){
        $time = strtotime(date('Y-m-d')) - 86400;
        $date = date('Y-m-d' , $time);
        $totaldaily = $this->td->getTotaldailyByDate($date , array('id'));
        if(!$totaldaily){
            $data = array();
            $roles = $this->ro->getRoleByDate('eq' , $date);
            $data['role_num'] = 0;
            $role_ids = array();
            if(is_array($roles) && $roles){
                $data['role_num'] = count($roles); //创角数
                foreach($roles as $row){
                    $role_ids[] = $row['role_id'];
                }
            }
           
			$sql ="select count(*) as count from (select min(create_time) as time from ny_role where create_time   GROUP BY account ) as a where FROM_UNIXTIME(time,'%Y-%m-%d') = '$date'";
			$accounts = $this->lg->query($sql);
            $data['reg_num'] = $accounts[0]['count']; //注册数
			
            $data['login_num'] = $this->lg->getRoleLogNumByDate($date); //登录数
            $data['atv_num'] = 0; //活跃数
            $roles = $this->lg->getRoleOlTimeByDate($date); //角色在线时长
            foreach($roles as $role => $long){
                ($long >= 1800) && $data['atv_num'] ++; //一天在线大于30分钟则算活跃玩家
            }
            $old_roles = $this->lg->getRoleOlTimeByDate($date , 1);
            $data['old_num'] = count($old_roles); //老角色数
            $data['pay_num'] = $this->od->getOrderRoleNumByDate($date); //充值人数
            $data['money'] = $this->od->getOrderRoleMoneyByDate($date); //充值金额
            $data['money'] = $data['money'] ? $data['money'] : 0;
            $data['lc_percent'] = $data['pay_num'] && $data['login_num'] ? sprintf('%.2f' , ($data['pay_num'] / $data['login_num'] * 100)) . '%' : '0%'; //登录付费率
            $data['arpu'] = $data['money'] && $data['pay_num'] ? sprintf('%.2f' , ($data['money'] / $data['pay_num'])) : '0'; //总AP
            $data['first_pay_num'] = $this->od->getOrderRoleNumByDate($date , $role_ids); //新充值人数
            $data['first_money'] = $this->od->getOrderRoleMoneyByDate($date , $role_ids); //新充值金额
            $data['first_money'] = $data['first_money'] ? $data['first_money'] : 0;
            $data['first_percent'] = $data['first_pay_num'] && $data['role_num'] ? sprintf('%.2f' , ($data['first_pay_num'] / $data['role_num'] * 100)) . '%' : '0%'; //新增付费率
            $data['first_arpu'] = $data['first_money'] && $data['first_pay_num'] ? sprintf('%.2f' , ($data['first_money'] / $data['first_pay_num'])) : '0'; //新充值AP
            $old_role_ids = array();
            if(is_array($old_roles) && $old_roles){
                foreach($old_roles as $k => $v){
                    $old_role_ids[] = $k;
                }
            }
			
			if(empty($old_role_ids)){
				$data['old_pay_num'] = 0;
				$data['old_money'] = 0;
			}else{
				$data['old_pay_num'] = $this->od->getOrderRoleNumByDate($date , $old_role_ids); //老充值人数
            	$data['old_money'] = $this->od->getOrderRoleMoneyByDate($date , $old_role_ids); //老充值金额	
			}
			
           
            $data['old_money'] = $data['old_money'] ? $data['old_money'] : 0;
            $data['old_percent'] = $data['old_pay_num'] && $data['old_num'] ? sprintf('%.2f' , ($data['old_pay_num'] / $data['old_num'] * 100)) . '%' : '0%'; //老付费率
            $data['old_arpu'] = $data['old_money'] && $data['old_pay_num'] ? sprintf('%.2f' , ($data['old_money'] / $data['old_pay_num'])) : '0'; //老充值AP
            $data['reg_arpu'] = $data['money'] && $data['reg_num'] ? sprintf('%.2f' , ($data['money'] / $data['login_num'])) : '0'; //登录AP
            $data['max_num'] = $this->ol->getMaxOnlineByDate($date); //最高在线数
            $data['avg_num'] = $this->ol->getAvgOnlineByDate($date); //平均在线数
            $data['date'] = $time;
            $this->td->setTotaldaily($data);
        }
        $this->updateTotaldaily();
    }

    public function updateTotaldaily(){
        $time = strtotime(date('Y-m-d')) - 86400;
        $days = array('two' => 1 , 'three' => 2 , 'four' => 3 , 'five' => 4 , 'six' => 5 , 'seven' => 6 , 'fifteen' => 14 , 'thirty' => 29); //第N天
        foreach($days as $k => $v){
            $old_date = date('Y-m-d' , $time - $v * 86400); //获取第N天前的日期
            $old_totaldaily = $this->td->getTotaldailyByDate($old_date , array('id'));
            if($old_totaldaily){
                $roles = $this->ro->getRoleByDate('eq' , $old_date);
                $role_ids = array();
                if(is_array($roles) && $roles){
                    foreach($roles as $row){
                        $role_ids[] = $row['role_id'];
                    }
                    $role_num = count($role_ids);
                    $login_num = $this->lg->getRoleLogNumByDate(date('Y-m-d' , $time) , $role_ids); //获取第N天前创建角色在今天登录的情况
                    $data = array($k => $login_num && $role_num ? sprintf('%.2f' , $login_num / $role_num * 100) . '%' : '0%');
                }
                else{
                    $data = array($k => '0%');
                }
                $this->td->setTotaldaily($data , $old_date);
            }
        }
    }
}
