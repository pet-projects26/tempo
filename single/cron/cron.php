<?php
//计划任务入口文件
//php cron.php foo 会执行 ./controller/cron/FooController.php

PHP_SAPI != 'cli' && exit();
//$argv[1] = 'levelremain';
//$argv[2] = '2019-04-08';
//$argv[3] = '2019-04-08';
//error_reporting(0);
ini_set('display_errors' , 'On');
error_reporting(E_ALL ^ E_STRICT);
date_default_timezone_set('Asia/Shanghai');
define('IN_WEB' , true);
define('ROOT' , __DIR__ . '/../');
$server_id = getParam('s');
if($server_id){
    define('CONFIG_SERVER_ID' , $server_id);
    //内网测试的服务器使用的配置，一套单服代码多份db配置
    require_once ROOT . '/config/db_' . $server_id . '.php';
}
else{
    //外网正式服配置
    require_once ROOT . '/config/db.php';
}
function autoload($className){
    @include_once "$className.php";
}
function getParam($name = '' , $default = ''){
    return isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $default);
}
spl_autoload_register('autoload');
$dirs = array(
    get_include_path(),
    ROOT . '/controller/cron',
    ROOT . '/model',
    ROOT . '/includes'
);
set_include_path(implode(PATH_SEPARATOR , $dirs));
empty($argv[1]) && exit();
$params = count($argv) > 2 ? array_slice($argv , 2) : null;
$className = ucfirst(strtolower($argv[1])) . 'Controller';
!class_exists($className) && exit();
$rc = new ReflectionClass($className);
!$rc->isSubclassOf('CronController') && exit();
$rc->newInstance()->run($params);
