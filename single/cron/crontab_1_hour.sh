source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#登录统计
#run login;

#七日留存
#run sevenremain;

#账号留存
#run accountsevenremain;

#排行榜
#run rank;

#在线统计
run online
