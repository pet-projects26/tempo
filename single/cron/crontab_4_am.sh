source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}

#LTV统计
#run ltv;

#后续充值
run follow;

#新增后续充值
run follownew;

#充值留存率
run remain;

#产出消耗
#run item;

#ARPU
#run arpu;

#新增玩家
run chargenew;

#每日充值
#run chargedaily;

#角色数据汇总
#run totaldaily;

#账号数据汇总
#run acctotaldaily;


