#!/usr/bin/env bash
source /etc/profile
work_dir=$(dirname "$0");

function run() {
    ps -elf | grep "$work_dir/cron.php $1" | grep -v grep
    if [ $? -ne 0 ]; then
    	php $work_dir/cron.php $@;
    fi
}


#排行榜统计
#run ranklist

#等级留存
#run levelremain $1