<?php
ini_set('display_errors', 'on');
error_reporting(0);
date_default_timezone_set('Asia/Shanghai');
define('IN_WEB', true);
define('ROOT', __DIR__);

//外网正式服配置
require_once ROOT . '/config/db.php';

require_once ROOT . '/includes/Util.php';
require_once ROOT . '/includes/Fields.php';
require_once ROOT . '/includes/redisServer.php';
define('DATA_DIR', ROOT . '/../bin/config/config/');
spl_autoload_register('autoload');
$dirs = array(
    get_include_path(),
    ROOT . '/includes',
    ROOT . '/controller',
    ROOT . '/model',
    ROOT . '/redis'
);
set_include_path(implode(PATH_SEPARATOR, $dirs));

$login = new LoginModel();

$json = $login->getRoleLogHonor(1557694800);

echo '<pre>';
print_r($json);
echo '</pre>';

file_put_contents('honor.json', $json);

function autoload($className){
    @include_once $className . '.php';
}