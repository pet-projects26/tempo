<?php

/***
 * Class Fields
 * JSON配置文件返回的对应下标
 */
class Fields
{

    public $jsonDir = '';

    public $equip_json_file_path = '/item_equip.json'; //装备json路径

    public $equip_attr_json_file_path = '/item_equip.json';  //装备属性json路径

    public $material_json_file_path = '/item_material.json';  //物品json路径

    public $stone_json_file_path = '/item_stone.json';  //宝石json路径

    public $exterior_json_file_path = '/exterior.json';  //外观名字json路径

    public $skill_json_file_path = '/skill.json'; //技能json路径

    public $xianwei_json_file_path = '/xianwei_rise.json'; //仙位json路径

    public $action_open_json_file_path = '/action_open.json'; //开启功能json路径

    public $mall_json_file_path = '/mall.json'; //商城配置json路径

    public $recharge_json_file_path = '/recharge.json'; //充值配置json路径

    public $rune_json_file_path = '/item_rune.json'; //符文配置json

    public $xianfu_illustrated_handbook_json_file_path = '/xianfu_illustrated_handbook.json';

    public $xianfu_decorate_json_file_path = '/xianfu_decorate.json';

    //装备json对应字段
    public static $item_equipFields = array(
        'itemId' => 0,  //装备ID
        'name' => 1,    //装备名
        'overlap' => 2, //叠加数量
        'sale' => 3,    //出售价格
        'era' => 4, //转生要求
        'wearLvl' => 5, //穿戴等级
        'fight' => 6,   //战力
        'des' => 7,     //描述
        'ico' => 8,     //图标ID
        'baseAttr' => 9,    //基础属性 [id#value]#[id#value]
        'blueAttrCount' => 10,  //蓝色属性条数
        'blueWeightPool' => 11, //蓝色属性ID池 [id#weight]#[id#weight]
        'purpleAttrCount' => 12, //紫色属性条数
        'purpleWeightPool' => 13, //紫色属性ID池 [id#weight]#[id#weight]
        'advisePurpleAttr' => 14, //推荐紫色属性 id#id
        'orangeAttrCount' => 15, //橙色属性条数
        'orangeWeightPool' => 16,   //橙色属性ID池 [id#weight]#[id#weight]
        'adviseOrangeAttr' => 17,    //推荐橙色属性 id#id
        'smelt' => 18,            /*熔炼收益 熔炼经验值#金币#强化石*/
        'notGeneratedScore' => 19,            /*未生成装备评分*/
        'itemSourceId' => 20,            /*装备来源Id id#id#id#id*/
        'isChat' => 21,            /*是否可以在聊天中发送  0：不可，1：可以*/
        'showId' => 22,            /*模型ID/图片ID*/
        'isMove' => 23,            /*展示模型、图片 是否 上下动  0否 1是*/
        'isModel' => 24,            /*是否模型(0模型1特效2图片)*/
        'customClipId' => 25,            /*道具展示特效ID*/
        'layerNum' => 26,            /*特效展示层级 0 道具图标和道具底图之间 1 图标的最上层*/
    );

    //装备json对应字段
    public static $item_equip_attrFields = array(
        'id' => 0, //装备属性ID
        'name' => 1, //属性名
        'type' => 2, //属性类型
        'value' => 3, //属性值
        'quality' => 4 //品质
    );

    //物品json对应字段
    public static $item_materialFields = array(
        'itemId' => 0, //道具ID
        'name' => 1, //礼包名
        'userLvl' => 2, //使用等级
        'vipLvl' => 3, //VIP等级限制
        'overlap' => 4, //叠加数量
        'sale' => 5, //出售价格
        'shortcutUse' => 6, //快捷使用
        'des' => 7, //描述
        'ico' => 8, //图标ID
        'fixGifbag' => 9, //礼包固定内容
        'values' => 10 //参数 value#value
    );

    //仙石json对应字段
    public static $item_stoneFields = array(
        'itemId' => 0, //仙石ID
        'name' => 1,    //仙石名
        'overlap' => 2, //叠加数量
        'sale' => 3,    //出售价格
        'des' => 4, //描述
        'ico' => 5  //图标ID
    );

    //外观名字json对应字段
    public static $item_exteriorFields = array(
        'id' => 0, //外观ID
        'path' => 1, //外观文件路径
        'name' => 2, // 外观名称
        'icon' => 3, //外观图标
        'quality' => 4, //品质
        'get_way' => 5, //来源
        'effects' => 6 /*挂载的特效：特效1#特效2#....*/
    );

    //技能json对应字段
    public static $item_skillFields = array(
        'id' => 0,            /*技能ID*/
        'name' => 1,            /*技能名*/
        'skillType' => 2,            /*类型 0:普攻 1:主动 2:永久被动 3:轮询被动 4:触发被动*/
        'petSkill' => 3,            /*非0即宠物技能*/
        'cd' => 4,            /*cd*/
        'readyTime' => 5,            /*预备时间,用于主动*/
        'delayTime' => 6,            /*释放时间,用于主动*/
        'pollTime' => 7,            /*轮询时间*/
        'per' => 8,            /*概率*/
        'skillPer' => 9,            /*技能系数*/
        'fixedHurt' => 10,            /*固定伤害*/
        'eventType' => 11,            /*触发被动类型,用于触发被动 1:开始攻击 2:被攻击开始 3:命中 4:被命中 5:闪避 6:被闪避 7:暴击 8:被暴击 9:致死 10:被致死 11:攻击结束 12:被攻击结束*/
        'skillGain' => 12,            /*对主动技能增益,用于触发被动 0:主角 1:宠物  2:主角或宠物*/
        'trigger' => 13,            /*触发被动条件,用于触发被动 0:无条件 1:自身血量少于XX% 2:目标血量少于XX% 3:自身血量大于XX% 4:目标血量大于XX% 5:自身眩晕 6:目标眩晕 7:自身沉默 8:目标沉默*/
        'triParam' => 14,            /*触发参数 参数#参数*/
        'rangeType' => 15,            /*范围类型 1:自身周围 2:目标周围*/
        'radius' => 16,            /*半径(格子): -1:全图*/
        'randomCount' => 17,            /*随机个数 -1:全部*/
        'param' => 18,            /*技能参数: 效果类型#数值#效果类型#数值*/
        'effectTarget' => 19,            /*0:单个(减益BUFF为被攻击者，增益BUFF为攻击者) 1:范围(减益BUFF为随中目标，增益BUFF为自己)*/
        'effect' => 20,            /*BUFF列表 id#ms#id#ms*/
        'des' => 21,            /*描述*/
        'icon' => 22,            /*图标*/
    );

    //仙位rise json对应字段
    public static $item_xianweiFields = array(
        'id' => 0,            /*id*/
        'name' => 1,            /*名字*/
        'maxExp' => 2,            /*最大仙力值*/
        'reward' => 3,            /*升阶奖励 [itemId#count]#[itemId#count]*/
        'wages' => 4,            /*每日俸禄 [itemId#count]#[itemId#count]*/
        'fighting' => 5,            /*战力*/
        'attack' => 6,            /*攻击力*/
        'hp' => 7,            /*生命*/
        'defense' => 8,            /*防御*/
    );

    //开启功能json 对应字段
    public static $item_action_openFields = array(
        'id' => 0,            /*功能ID*/
        'showType' => 1,            /*显示 1:或 2:与 [[A#B]#[C#D]] 1#1#1:((A或B)或(C或D)) 1#1#2:((A或B)且(C或D)) 1#2#1:((A或B)或(C且D)) 1#2#2:((A或B)且(C且D)) 2#1#1:((A且B)或(C或D)) 2#1#2:((A且B)且(C或D)) 2#2#1:((A且B)或(C且D)) 2#2#2:((A且B)且(C且D))*/
        'showLevel' => 2,            /*[[类型#等级]#[类型#等级]]#[[类型#等级]#[类型#等级]] 1:角色等级 2:天关关数 3:开服天数 4:VIP*/
        'openType' => 3,            /*开启 1:或 2:与 [[A#B]#[C#D]] 1#1#1:((A或B)或(C或D)) 1#1#2:((A或B)且(C或D)) 1#2#1:((A或B)或(C且D)) 1#2#2:((A或B)且(C且D)) 2#1#1:((A且B)或(C或D)) 2#1#2:((A且B)且(C或D)) 2#2#1:((A且B)或(C且D)) 2#2#2:((A且B)且(C且D))*/
        'openLevel' => 4,            /*[[类型#等级]#[类型#等级]]#[[类型#等级]#[类型#等级]] 1:角色等级 2:天关关数 3:开服天数 4:VIP*/
        'closeType' => 5,            /*关闭 1:或 2:与 [[A#B]#[C#D]] 1#1#1:((A或B)或(C或D)) 1#1#2:((A或B)且(C或D)) 1#2#1:((A或B)或(C且D)) 1#2#2:((A或B)且(C且D)) 2#1#1:((A且B)或(C或D)) 2#1#2:((A且B)且(C或D)) 2#2#1:((A且B)或(C且D)) 2#2#2:((A且B)且(C且D))*/
        'closeLevel' => 6,           /*[[类型#等级]#[类型#等级]]#[[类型#等级]#[类型#等级]] 1:角色等级 2:天关关数 3:开服天数 4:VIP 10:自定义*/
    );

    //商城json
    public static $item_mallFields = array(
        'id' => 0,            /*唯一id*/
        'itemId' => 1,            /*道具id*/
        'count' => 2,            /*数量*/
        'mallType' => 3,            /*商城类型 0商城 10商店*/
        'mallName' => 4,            /*商城名字*/
        'childMallType' => 5,            /*子商城类型 1道具商城 2材料商城 3装备商城 4限购商城*/
        'childMallName' => 6,            /*子商城名字*/
        'originalPrice' => 7,            /*原价 itemId#数量*/
        'realityPrice' => 8,            /*现价 itemId#数量*/
        'limitBuy' => 9,            /*限购(0不限购 1每天 2每周) 类型#数量*/
        'vip' => 10,            /*vip条件 0表示不限制*/
        'shortcut' => 11,            /*是否弹出购买界面 0弹 1不弹*/
        'desId' => 12,            /*折扣id*/
        'sortId' => 13,            /*排序id*/
    );

    //充值json
    public static $item_rechargeFields = array(
        'index' => 0,            /*档位*/
        'name' => 1,            /*档位名称*/
        'price' => 2,            /*档位价格*/
        'describe' => 3,            /*档位描述*/
        'ico' => 4,            /*图标ID*/
        'baseId' => 5,            /*底图id*/
        'reward' => 6,            /*奖励 [itemId#count]#[itemId#count]*/
        'exReward' => 7,            /*额外赠送奖励 [itemId#count]#[itemId#count]*/
        'type' => 8,            /*充值类型 0：特卖，1：普通*/
        'sortId' => 9,            /*排序id*/
        'visible' => 10,            /*档位购买后是否隐藏 0：隐藏，1：不隐藏*/
        'descriptive' => 11,            /*档位描述2*/
    );

    //符文json
    public static $item_runeFields = array(
        'itemId' => 0,            /*符文ID*/
        'name' => 1,            /*符文名*/
        'overlap' => 2,            /*叠加数量*/
        'inlayNumber' => 3,            /*可镶嵌序号 1#8 闭区间*/
        'ico' => 4,            /*图标ID*/
        'layer' => 5,            /*解锁层数*/
        'isChat' => 6,            /*是否可以在聊天中发送  0：不可，1：可以*/
        'itemSourceId' => 7,            /*道具来源ID id#id#id#id*/
    );

    //灵兽图鉴json
    public static $item_xianfu_illustrated_handbookFields = array(
        'id' => 0,            /*图鉴id*/
        'ico' => 1,            /*图标ID*/
        'name' => 2,            /*名字*/
        'des' => 3,            /*描述*/
        'level' => 4,            /*等级*/
        'quality' => 5,            /*品质 1-4*/
        'items' => 6,            /*升级或激活所需道具 itemId#count*/
        'fighting' => 7,            /*战力*/
        'attack' => 8,            /*攻击力*/
        'hp' => 9,            /*生命*/
        'defense' => 10,            /*防御*/
        'disDefense' => 11,            /*破防*/
        'hit' => 12,            /*命中*/
        'dodge' => 13,            /*闪避*/
        'crit' => 14,            /*暴击*/
        'tough' => 15,            /*韧性*/
        'hitPer' => 16,            /*命中率*/
        'dodgePer' => 17,            /*闪避率*/
        'critPer' => 18,            /*暴击率*/
        'toughPer' => 19,            /*韧性率*/
        'hurtDeep' => 20,            /*伤害加深*/
        'hurtLess' => 21,            /*伤害减免*/
        'critHurtDeep' => 22,            /*暴击增伤*/
        'critHurtLess' => 23,            /*暴击减伤*/
        'rebound' => 24,            /*伤害反射*/
        'pvpHurtDeep' => 25,            /*PVP增伤*/
        'pvpHurtLess' => 26,            /*PVP减伤*/
        'bossHurtDeep' => 27,            /*BOSS增伤*/
        'armor' => 28,            /*人物护甲*/
        'disArmor' => 29,            /*人物破甲*/
        'eleAttack' => 30,            /*元素攻击*/
        'eleResistant' => 31,            /*抗性*/
        'realHurt' => 32,            /*真实伤害*/
        'realArmor' => 33,            /*真实护甲*/
        'expPer' => 34,            /*经验加成*/
        'speed' => 35,            /*移速*/
        'perAttr' => 36,            /*百分比属性 id#id*/
    );

    public static $item_xianfu_decorateFields = array(
        'id' => 0,            /*物件id*/
        'items' => 1,            /*升级所需要道具id#数量*/
        'type' => 2,            /*风水效果类型 0对指定建筑产出加成 1对所有建筑制作减少消耗% 2对所有建筑制作增加成功率% 3对所有建筑增加每日制作上限 4游历消费减少% 5游历经验增加% 6游历次数增加*/
        'param' => 3,            /*参数 对于类型0要配多个参数 建筑id#加成类型#加成参数值#额外值(暴击倍数) 加成类型：1固定点 2百分点 3暴击概率*/
        'fengshuiValue' => 4,            /*风水值*/
        'fight' => 5,            /*战力值*/
        'attack' => 6,            /*攻击力*/
        'hp' => 7,            /*生命*/
        'defense' => 8,            /*防御*/
        'disDefense' => 9,            /*破防*/
        'hit' => 10,            /*命中*/
        'dodge' => 11,            /*闪避*/
        'crit' => 12,            /*暴击*/
        'tough' => 13,            /*韧性*/
        'hitPer' => 14,            /*命中率*/
        'dodgePer' => 15,            /*闪避率*/
        'critPer' => 16,            /*暴击率*/
        'toughPer' => 17,            /*韧性率*/
        'hurtDeep' => 18,            /*伤害加深*/
        'hurtLess' => 19,            /*伤害减免*/
        'critHurtDeep' => 20,            /*暴击增伤*/
        'critHurtLess' => 21,            /*暴击减伤*/
        'rebound' => 22,            /*伤害反射*/
        'pvpHurtDeep' => 23,            /*PVP增伤*/
        'pvpHurtLess' => 24,            /*PVP减伤*/
        'bossHurtDeep' => 25,            /*BOSS增伤*/
        'armor' => 26,            /*人物护甲*/
        'disArmor' => 27,            /*人物破甲*/
        'eleAttack' => 28,            /*元素攻击*/
        'eleResistant' => 29,            /*抗性*/
        'realHurt' => 30,            /*真实伤害*/
        'realArmor' => 31,            /*真实护甲*/
        'expPer' => 32,            /*经验加成*/
        'speed' => 33,            /*移速*/
        'perAttr' => 34,            /*百分比属性 id#id*/
        'nameRes' => 35,            /*名字资源名称*/
        'iconRes' => 36,            /*图标资源名称*/
    );

    public static function getJsonData($item, $fields = [], $keyField = 'itemId')
    {
        $FieldsArr = 'item_' . $item . 'Fields';
        $jsonPath = $item . '_json_file_path';

        //判定configs_path是否有效
        if (defined('CONFIGS_PATH') && is_dir(CONFIGS_PATH)) {
            $jsonDir = CONFIGS_PATH;
        } else {
            $jsonDir = ROOT;
        }

        $Fields = new self();
        $jsonPath = $jsonDir . $Fields->$jsonPath;

        if (!isset(self::$$FieldsArr) || !isset($jsonPath) || !array_key_exists($keyField, self::$$FieldsArr)) {
            return false;
        }

        if (!file_exists($jsonPath)) return false;

        $res = file_get_contents($jsonPath);

        $res = json_decode($res);

        //value - key arr
        $arr = self::$$FieldsArr;
        if (empty($fields)) {
            $fields = self::$$FieldsArr;
        } else {
            $fields = array_flip($fields);
            foreach ($fields as $key => $value) {
                $fields[$key] = $arr[$key];
            }
        }
        //key-name arr
        $fieldsName = array_flip($fields);
        $data = [];
        foreach ($res as $key => $value) {
            foreach ($value as $k => $v) {
                if (in_array($k, $fields)) {
                    $data[$value[$arr[$keyField]]][$fieldsName[$k]] = $v;
                }
            }
        }

        return $data;
    }
}
