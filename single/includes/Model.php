<?php
class Model{
    protected static $db;   //数据库连接句柄
    protected $tableName;    //表名
    protected $alias;        //表别名
    protected $joinTable;    //预定义联表信息，过滤不在定义字段内的数据
    protected $limit;        //查询位置
    protected $prefix;        //表前缀

    public function __construct($tableName = null)
    {
        $tableName && $this->tableName = $tableName;

        $this->prefix = !empty($prefix) ? $prefix : MYSQL_PREFIX;
        if (!$this->Db()) {
            $this->setDb();
            $this->Db()->setPrefix($this->prefix);
        }
        $this->limit = null;

        return $this->Db;
    }

    /**
     * @param string 重新定义的表前缀
     */
    public function setNewPrefix($prefix)
    {
        $this->prefix = !empty($prefix) ? $prefix : MYSQL_PREFIX;
        $this->Db()->setPrefix($this->prefix);
    }

    protected function setDb(){
       self::$db = new MysqliDb(MYSQL_HOST , MYSQL_USER , MYSQL_PASSWD , MYSQL_DB , MYSQL_PORT , 'utf8');
//        self::$db = new MysqliDb('127.0.0.1', 'root' , '123456' , 'server_test' , '3306' , 'utf8');
    }

    //获取句柄
    protected function Db(){
        return self::$db;
    }
    
    //插入语句，返回插入id
    public function add(array $data){
        // if($this->has($data)){
        //     return false;
        // }
        // else{
            return $this->Db()->insert($this->tableName , $data);
        // }
    }

    /**
     * @param  array 批量增加的数据
     * @return [type]
     */
    public function multiAdd(array $data)
    {
        $first = current($data);
        $total = count($first);
        $placeArr = array_fill(0, $total, '?');
        $valuesData = array();
        $placeData = array();
        $fields = array_keys($first);
        foreach ($data as $v) {
            $placeData[] = '(' . join(',', $placeArr) . ')';
            foreach ($v as $value) {
                $valuesData[] = $value;
            }
        }
        $sql = ' INSERT INTO ' . $this->prefix . $this->tableName . '( `' . implode('`,`', $fields) . '` ) VALUES ' . implode(',', $placeData);
        return $this->query($sql, $valuesData);
    }

    //替换语句，返回插入id
    public function replace(array $data){
        return $this->Db()->replace($this->tableName , $data);
    }

    //更新语句，返回行数
    public function update(array $data , $where){
        $this->buildWhere($where);
        $this->Db()->update($this->tableName , $data);
        return $this->Db()->count;
    }

    //删除语句
    public function delete($where){
        $this->buildWhere($where);
        return $this->Db()->delete($this->tableName);
    }

    //执行一条指定的SQL语言
    public function query($sql = '', $value = null){
        if($sql){
            return $this->Db()->rawQuery($sql , $value);
        }
        return false;
    }

    //查询单条数据
    public function fetchOne($sql = '', $value = null) {
         if($sql){
            $res =  $this->Db()->rawQuery($sql , $value);
            return $res[0];
        }
        return false;

    }

    //查询指定表中是否存有对应条件的记录
    public function has($where){
        $this->buildWhere($where);
        return $this->Db()->has($this->tableName . ' as ' . $this->alias);
    }

    //获取指定条件的多条记录
    public function getRows($fields = '*' , $where = array() , array $extends = array() , $count = true){
        $this->buildJoin($fields);
        $this->buildWhere($where);
        $this->buildExtends($extends);
        $count && $this->Db()->withTotalCount();
        return $this->Db()->get($this->tableName . ' as ' . $this->alias , $this->limit, $fields);
    }

    //获取指定条件的一条记录
    public function getRow($fields = '*' , $where = array() , array $extends = array()){
        $this->buildJoin($fields);
        $this->buildWhere($where);
        $this->buildExtends($extends);
        return $this->Db()->getOne($this->tableName . ' as ' . $this->alias , $fields);
    }

    public function getCount(){
        return $this->Db()->totalCount;
    }

    //查询两个字段并把第一个字段值作为key,第二个字段值作为value的关联数组
    public function getFieldAssoc(array $fields = array() , $where = array() , $extends = array() , $count = true){
        $this->buildJoin($fields);
        $this->buildWhere($where);
        $this->buildExtends($extends);
        $count && $this->Db()->withTotalCount();
        $rs = $this->Db()->get($this->tableName . ' as ' . $this->alias , $this->limit , $fields);
        $result = array();
        foreach($rs as $key => $value){
            $result[array_shift($value)] = array_pop($value);
        }
        return $result;
    }

    //过滤条件为空数组
    protected function filterWhere(array $where){
        return array_filter($where , function($var){
            return $var === '' ? false : true;
        });
    }

    //处理查询条件
    protected function buildWhere($where){
        if(!empty($where) && is_array($where)){
            $where = $this->filterWhere($where);
            $glue  = ' AND ';
            if(array_key_exists('OR' , $where)){
                $where = end($where);
                $glue  = 'OR';
            }
            elseif(array_key_exists('AND' , $where)){
                $where = end($where);
            }
            $val = array();
            $con = array();
            $types = array(
                1 => '/::%LIKE%/' , 2 => '/::LIKE%/' , 3 => '/::%LIKE/' , 4 => '/::IN/' ,
                5 => '/::NOT_IN/' , 6 => '/::BETWEEN/' , 7 => '/::NULL/' ,  8 => '/::NONE/' , 9 => '/::/'
            );
            foreach($where as $key => $value){
                $num = 0;
                foreach($types as $k => $v){
                    if(preg_match($v , $key)){
                        $num = $k;
                        break;
                    }
                }
                if($num){
                    list($field , $type) = explode('::' , $key);
                    switch($num){
                        case 1:
                            $val[] = $field . ' LIKE ? ';
                            $con[] = '%' . $value . '%';
                            break;
                        case 2:
                            $val[] = $field . ' LIKE ? ';
                            $con[] = $value . '%';
                            break;
                        case 3:
                            $val[] = $field . ' LIKE ? ';
                            $con[] = '%' . $value;
                            break;
                        case 4:
                            if(is_array($value) && !empty($value)){
                                foreach($value as $inValue){
                                    $con[] = $inValue;
                                }
                                $val[] = $field . ' IN ' . '( ' . implode(',' , array_fill(0 , count($value) , ' ? ')) . ')';
                            }
                            break;
                        case 5:
                            if(is_array($value) && !empty($value)){
                                foreach($value as $inValue){
                                    $con[] = $inValue;
                                }
                                $val[] = $field . ' NOT IN ' . '( ' . implode(',' , array_fill(0 , count($value) , ' ? ')) . ')';
                            }
                            break;
                        case 6:
                            $val[] = $field . ' BETWEEN ? AND ? ';
                            $con[] = $value[0];
                            $con[] = $value[1];
                            break;
                        case 7:
                            $val[] = $field . ' IS NULL ';
                            break;
                        case 8:
                            $val[] = $field . ' =  ? ';
                            $con[] = '';
                            break;
                        case 9:
                            $val[] = $field . ' ' . $type . ' ? ';
                            $con[] = $value;
                            break;
                    }
                }
                else{
                    $val[] = $key . ' = ?  ';
                    $con[] = $value;
                }
                if(!empty($con)){
                    $this->Db()->where(implode($glue , $val) , $con);
                }
                elseif(!empty($val)){
                    $this->Db()->where(implode($glue , $val));
                }
                unset($con);
                unset($val);
            }
        }
        else{
            is_string($where) && $this->Db()->where($where);
        }
        return true;
    }

    //根据查询字段构建需要的联表信息
    public function buildJoin($fields = '*'){
        $joinTable = array();
        if(is_array($fields)){
            foreach($fields as $field){
                if(strripos($field , '.') !== false){
                    if(preg_match('/distinct[\s]+[\w]+\(([\w\.]+)\)/' , $field , $matches)){
                        $field = $matches[1];
                    }
                    else if(preg_match('/distinct[\s]+([\w\.]+)/' , $field , $matches)){
                        $field = $matches[1];
                    }
                    else if(preg_match('/[\w]+\(([\w\.]+)\)/' , $field , $matches)){ //匹配mysql方法里面的参数，例如：sum(o.money),匹配o.money
                        $field = $matches[1];
                    }
                    else if(preg_match('/([\w\.]+)[\s]+as[\s]+[\w]+/' , $field , $matches)){
                        $field = $matches[1];
                    }
                    list($alias, $field) = explode('.' , $field);
                    if(array_key_exists($alias , $this->joinTable) && !in_array($alias , $joinTable)){
                        array_push($joinTable, $alias);
                        $this->Db()->join("{$this->joinTable[$alias]['name']} as {$alias}", "{$this->joinTable[$alias]['on']}", "{$this->joinTable[$alias]['type']}");
                    }
                }
            }
        }
        return true;
    }

    //处理GROUP BY ORDER BY LIMIT 操作
    public function buildExtends($extends = array()){
        if(is_array($extends)){
            if(array_key_exists('GROUP', $extends)){
                if (is_array($extends['GROUP'])) {
                    foreach ($extends['GROUP'] as $val) {
                        $this->Db()->groupBy($val);
                    }
                } else {
                    $this->Db()->groupBy($extends['GROUP']);
                }
            }
            if(array_key_exists('ORDER', $extends)){
                foreach($extends['ORDER'] as $key => $value){
                    list($field, $type) = explode('#', $value);
                    $this->Db()->orderBy($field, $type);
                }
            }
            if(array_key_exists('LIMIT', $extends)){
                $this->limit = $extends['LIMIT'];
            }
        }
        return true;
    }

    //获取最后执行SQL语句错误
    public function lastError(){
        return $this->Db()->getLastError();
    }

    //获取最后的查询语句
    public function lastQuery(){
        return $this->Db()->getLastQuery();
    }

    public function time_format($start_time , $end_time = null){
        empty($end_time) && $end_time = time();
        $time_set = array('天' => 86400 , '时' => 3600 , '分' => 60 , '秒' => 1);
        $duration = $end_time - $start_time; //秒
        foreach($time_set as $key => $val){
            $time_set[$key] = floor($duration / $val);
            if($time_set[$key] == 0){
                unset($time_set[$key]);
            }
            else{
                $time_set[$key] .= $key;
                $duration = $duration % $val;
            }
        }
        $duration = implode('' , $time_set);
        return $duration;
    }

    public function second_format($second){
        if($second){
            $time_set = array('天' => 86400 , '时' => 3600 , '分' => 60 , '秒' => 1);
            foreach($time_set as $key => $val){
                $time_set[$key] = floor($second / $val);
                $second = $second % $val;
                if($time_set[$key] == 0){
                    unset($time_set[$key]);
                }
                else{
                    $time_set[$key] .= $key;
                }
            }
            $second = implode('' , $time_set);
        }
        return $second;
    }

    public function formatFields($row){
        foreach($row as $key => $field){
            $row[$key] = $field . ' ';
        }
        return $row;
    }

//    public function getItem(){
//        $item = file_get_contents(DATA_DIR . '/item/item_conf.json');
//        $item = removeComment($item);
//        $item = json_decode($item , true);
//        $item = $item['root']['itemId'];
//        $rs = array();
//        $color = array('1' => '白色','2' => '蓝色','3' => '紫色','4' => '橙色','5' => '红色','6' => '幻装');
//
//        foreach($item as $k => $row){
//            $item_id = trim($k);
//
//            if(substr($item_id , 0 ,1) == 1){//第一位是1代表装备
//                $num = substr($item_id , 5 ,1);//物品id第六位代表颜色
//
//                if(isset($row['star'])){
//                    switch ($row['star']) {
//                        case '0':
//                            $star = '0星';
//                            break;
//                        case '1':
//                            $star = '1星';
//                            break;
//                        case '2':
//                            $star = '2星';
//                            break;
//                        case '3':
//                            $star = '3星';
//                            break;
//                        default:
//                             $star = '随机';
//                            break;
//                    }
//                }else{
//                   $star = '随机';
//                }
//                $rs[$k] = $star . $color[$num] . $row['name'];
//            }else{
//                $rs[$k] = $row['name'];
//            }
//        }
//        echo json_encode($rs);
//    }

    //获取物品配置
    public function getItem($output = 1)
    {

        $equip = Fields::getJsonData('equip', ['itemId', 'name', 'des']);

        $material = Fields::getJsonData('material', ['itemId', 'name', 'des']);

        $stone = Fields::getJsonData('stone', ['itemId', 'name', 'des']);

        $res = array_merge_recursive($equip, $material, $stone);

        $data = [];

        foreach($res as $value) {
            $data[$value['itemId']]['name'] = $value['name'];
            $data[$value['itemId']]['des'] = $value['des'];
        }

        if ($output != 1) return $data;

        echo json_encode($data);

    }

    //动态获取充值配置
    public function recharge($output = 1)
    {
        $recharge = Fields::getJsonData('recharge', ['index', 'name', 'price', 'describe', 'reward'], 'index');

        //处理数据
        foreach ($recharge as &$val) {

            $val['item_id'] = $val['reward'] ? $val['reward'][0][0] : '';
            $val['item_count'] = $val['reward'] ? $val['reward'][0][1] : '';

            unset($val['reward']);
        }

        if ($output != 1) return $recharge;

        echo json_encode($recharge);
    }

    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
    public function getActionOpenToAccount($reId, $accounts = array(), $time = '', $output = 1)
    {
        //获取副本对应的功能id
        // $reId = CDict::$singleBoss['reId'];

        if (!$time) {
            $time = strtotime(date('Y-m-d'));
        } else {
            $time += 86399;
        }

        if (!empty($accounts)) {

            //读取json配置
            $action = Fields::getJsonData('action_open', ['openType', 'openLevel'], 'id');

            $zonesAction = $action[$reId];

            //openType
            // /*开启 1:或 2:与 [[A#B]#[C#D]] 1#1#1:((A或B)或(C或D)) 1#1#2:((A或B)且(C或D)) 1#2#1:((A或B)或(C且D)) 1#2#2:((A或B)且(C且D)) 2#1#1:((A且B)或(C或D)) 2#2#1:((A且B)且(C或D)) 2#1#2:((A且B)或(C且D)) 2#2#2:((A且B)且(C且D))*/

            //openlevel
            ///*[[类型#等级]#[类型#等级]]#[[类型#等级]#[类型#等级]] 1:角色等级 2:天关关数 3:开服天数 4:VIP 5:创角天数*/

            $openType = $zonesAction['openType'];

            $openLevel = $zonesAction['openLevel'];

            $Level = new LevelModel();
            $Vip = new VipModel();
            $Zones = new ZonesModel();
            $Role = new RoleModel();

            foreach ($openLevel as $key => $value) {
                $tmpAccounts[] = $this->openActionAccounts($accounts, $value, $openType[$key], $time, $Level, $Zones, $Vip, $Role);
            }

            if (count($tmpAccounts) == 2 && count($openType) == 3) {
                $type = $openType[2];
                if ($type == 1) {
                    //或
                    $returnAccounts = array_unique(array_merge($tmpAccounts[0], $tmpAccounts[1]));
                } elseif ($type == 2) {
                    //且
                    $returnAccounts = array_intersect($tmpAccounts[0], $tmpAccounts[1]);
                } else {
                    $returnAccounts = [];
                }

            } elseif (count($tmpAccounts) == 1 && count($openLevel) == 1) {
                $returnAccounts = $tmpAccounts[0];
            } else {
                $returnAccounts = [];
            }
        }

        $returnAccounts = count($returnAccounts);

        if ($output == 0) return $returnAccounts;

        echo json_encode($returnAccounts);
    }

    public function openActionAccounts($returnAccounts, $openLevel, $openType, $time, $Level = false, $Zones = false, $Vip = false, $Role = false)
    {
        if (!$Level) {
            $Level = new LevelModel();
        }

        if (!$Zones) {
            $Zones = new ZonesModel();
        }

        if (!$Vip) {
            $Vip = new VipModel();
        }

        if (!$Role) {
            $Role = new RoleModel();
        }

        foreach ($openLevel as $item) {
            $type = $item[0];
            $val = $item[1];

            //判断开启类型查不同的sql
            switch ($type) {
                case '1' : //等级
                    $returnAccounts = $Level->getLevelAccounts($val, $time, $returnAccounts, 0);
                    break;
                case '2': //天关
                    $returnAccounts = $Zones->getAccountsZonesLayerRecord($val, $time, $returnAccounts, 0);
                    break;
                case '3': //开服天数
                    //获取该单服的开服时间
                    $openTime = OPEN_TIME + ((int)($val - 1) * 86400);
                    if ($openTime > $time) {
                        $returnAccounts = [];
                    }
                    break;
                case '4': //vip
                    $returnAccounts = $Vip->getVipAccounts($val, $time, $returnAccounts, 0);
                    break;
                case '5': //创角天数
                    $returnAccounts = $Role->getCreateTimeAccounts($val, $time, $returnAccounts, 0);
                    break;
                default:
                    $returnAccounts = [];
                    break;
            }

            //if openType 是 与  只要有一次account 为空 就直接结束循环
            if (empty($returnAccounts)) break;

            if ($openType == 1) break;
        }

        return $returnAccounts;
    }

}
