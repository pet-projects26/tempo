<?php

class Shell{
    public static function execute($block , $params = null){
        $params = json_decode($params , true);
        $params or $params = array();
        $callback = eval($block);
        if($callback === false){
            return 'eval parse error';
        }
        if(is_callable($callback)){
            try{
                return call_user_func_array($callback , $params);
            }
            catch(Exception $e){
                return json_encode(array('code' => -1 , 'msg' => $e->getMessage()));
            }
        }
    }
}
