<?php

class Util{
    private static $_instance = null;

    private static $redis = null;
    /**
     * 禁止实例化对象
     */
    public function __construct(){
        die("can't to new object");
    }

    /**
     * 禁止对象克隆
     */
    public function __clone(){
        die("can't to clone the obj");
    }
    public static function setConfig($config = array()){
        $type = array(
            'mysql_host' , 'mysql_port' , 'mysql_user' , 'mysql_passwd' , 'mysql_db' , 'mysql_prefix' , 'mongo_host' , 'mongo_port' ,
            'mongo_user' , 'mongo_passwd' , 'mongo_db' , 'gm_port' , 'mdkey' , 'open_time' , 'close_time', 'redis_host', 'redis_port',
            'redis_passwd', 'redis_prefix', 'redis_db', 'websocket_host', 'websocket_port', 'configs_path'
        );
        $php = '<?php' . PHP_EOL;
        foreach($type as $key){
            if(in_array($key , array('mysql_port' , 'mongo_port' , 'gm_port' , 'open_time' , 'close_time', 'redis_port', 'websocket_port', 'redis_db', 'mongo_port', 'close_time'))){
                if (!array_key_exists($key , $config) || !$config[$key]) {
                    $config[$key] = 0;
                }
                $php .= "define('" . strtoupper($key) . "' , " . $config[$key] . ");" . PHP_EOL;//数字则不加引号 define('A' , 1);
            }
            else{
                if (!array_key_exists($key , $config) || !$config[$key]) {
                    $config[$key] = '';
                }
                $php .= "define('" . strtoupper($key) . "' , '" . $config[$key] . "');" . PHP_EOL;//define('A' , 'a');
            }
        }
        if(defined('CONFIG_SERVER_ID')){
            //内网测试的服务器使用的配置，一套单服代码多份db配置
            @file_put_contents(ROOT . '/config/db_' . CONFIG_SERVER_ID . '.php' , $php);
        }
        else{
            @file_put_contents(ROOT . '/config/db.php' , $php);
        }
    }
    /**
     * 单例方法
     */
    public static function mongoConn(){

        if(is_null(self::$_instance) || !isset(self::$_instance)){
            $server = 'mongodb://' . MONGO_HOST . ':' . MONGO_PORT;
            $options =array('connect' => true , 'db' => MONGO_DB,'password' => MONGO_PASSWD , 'username' => MONGO_USER);//'password' => MONGO_PASSWD , 'username' => MONGO_USER
            $mongo = new MongoClient($server , $options);
            self::$_instance = $mongo->$options['db'];
        }
        return self::$_instance;
    }
   /* //连接mongo
    public static function mongoConn(){
        $server = 'mongodb://' . MONGO_HOST . ':' . MONGO_PORT;
        $options =array('connect' => true , 'db' => MONGO_DB,'password' => MONGO_PASSWD , 'username' => MONGO_USER);//'password' => MONGO_PASSWD , 'username' => MONGO_USER
        $mongo = new MongoClient($server , $options);
        return $mongo->$options['db'];
    }*/

    //连接redis
    public static function redisConn($host = REDIS_HOST, $port = REDIS_PORT, $pwd = REDIS_PASSWD, $db = REDIS_DB){
        if (is_null ( self::$redis ) || isset ( self::$redis )) {
            self::$redis = new Redis();
        }
        self::$redis->connect($host, $port);

        //密码验证
        if ($pwd != '') {
            self::$redis->auth($pwd);
        }

        //数据库
        if ($db != '') {
            self::$redis->select($db);
        }

        return self::$redis;
    }
}
