<?php

class redisServer
{

    protected $redis;

    //redis角色表
    protected static $ActorBaseName = 'ActorBase';

    //redis角色装备表 key为role_id
    protected static $ActorEquipName = 'ActorEquip';

    //redis角色装备信息表
    protected static $EquipGridName = 'EquipGrid';

    //redis角色灵宠信息表
    protected static $PetName = 'Pet';

    //redis仙器信息表
    protected static $RideName = 'Ride';

    //redis角色仙法信息表
    protected static $ActorSkillName = 'ActorSkillInfo';

    //redis角色法宝信息表
    protected static $AmuletName = 'Amulet';

    //redis角色金身信息表
    protected static $SoulName = 'Soul';

    //redis角色神兵信息表
    protected static $ShenbingName = 'Shenbing';

    //redis角色背包信息表
    protected static $BagInfoName = 'BagInfo';

    //redis角色仙位信息表
    protected static $XianweiName = 'Xianwei';

    //redis角色仙翼信息表
    protected static $WingName = 'Wing';

    //redis订单表
    public static $OrderName = 'RechargeOrderRecord';

    //redisVIP表
    public static $VipName = 'Vip';

    //redis排行榜
    public static $RankName = 'RankMgr';

    //redis远古符阵符文
    public static $RuneName = 'Rune';

    //redis仙府信息表
    public static $XianFuName = 'XianFu';

    protected static $ActorBaseFields = array(
        0 => 'account', //账号
        1 => 'id', //角色id
        2 => 'name', //角色名
        3 => 'timestamps', //创建时间
        4 => 'level', //角色等级
        5 => 'occ', //角色职业
        6 => 'eraLvl', //转生等级
        7 => 'eraNum', //转生重数
        8 => 'hp', //剩余血量
        9 => 'firstLogin', //是否第一次登录
        10 => 'exp', //经验
        11 => 'gold', //元宝
        12 => 'bind_gold', //绑定元宝
        13 => 'copper', //金币
        14 => 'zq', //真气
        15 => 'smeltLevel', //熔炼等级
        16 => 'smeltExp', //熔炼经验
        17 => 'outlineTime', //玩家离线时间
        18 => 'platform', //平台id
        19 => 'group', //组id
        20 => 'fight', //战力
        21 => 'tiantiHonor', //天梯荣誉
        22 => 'tiantiSeg', //天梯段位
        23 => 'guideList', //完成的指引列表
        24 => 'dayTime', //日更新
        25 => 'loginDay', //登录天数
        26 => 'onceReWard' //单次奖励
    );

    protected static $ActorEquipFields = array(
        0 => 'EquipGrids', //装备格子
        1 => 'gemRiseLevel', //仙石大师等级
        2 => 'strongRiseLevel', //强化大师等级
        3 => 'strongRise2Level', //强化神匠等级
        4 => 'shihunList',
        5 => 'equipSuit', //套装
        6 => 'xilianLevel', //洗练
        7 => 'xilianTimes', //洗练时间
        8 => 'updateTime' //更新时间
    );

    protected static $EquipFields = array(
        1 => 'weapon', //武器
        2 => 'hats', //帽子
        3 => 'clothes', //衣服
        4 => 'hand', //护手
        5 => 'shoes', //鞋子
        6 => 'belt', //腰带
        7 => 'necklace', //项链
        8 => 'bangle', //手镯
        9 => 'ring', // 戒指
        10 => 'jude' // 玉佩
    );

    protected static $EquipGridFields = array(
        0 => 'number', //格子编号
        1 => 'item', //装备的装备ID
        2 => 'strongLvl', //强化等级
        3 => 'star', //星级
        4 => 'gems', //宝石等级
        5 => 'zhuhunLv', //铸魂等级
        6 => 'zhuhunExp', //铸魂经验
        7 => 'equipXilians'
    );

    //灵宠字段
    protected static $PetFields = array(
        0 => 'level', //喂养等级
        1 => 'exp', //当前经验
        2 => 'feedSkillList', //技能列表
        3 => 'star', //星级
        4 => 'blessing', //当前祝福值
        5 => 'showId', //当前外观
        6 => 'showList', //外观列表
        7 => 'rankSkillList', //技能列表
        8 => 'magicShowList', //幻化列表
        9 => 'refineList', //修炼列表
        10 => 'fazhenId', //当前法阵外观
        11 => 'fazhenList' //法阵列表
    );

    //灵宠仙器幻化列表字段
    protected static $PetMagicShowInfoFields = array(
        0 => 'showId', //幻化Id
        1 => 'star' //星级
    );

    //灵宠仙器修炼列表
    protected static $PetRefineInfoFields = array(
        0 => 'type', //类型
        1 => 'level' //等级
    );

    //灵宠修炼类型列表
    protected static $PetRefineInfoTypeName = array(
        0 => '悟性',
        1 => '潜能',
        2 => '灵体',
        3 => '根骨'
    );

    //仙器字段
    protected static $RideFields = array(
        0 => 'level', //喂养等级
        1 => 'exp', //当前经验
        2 => 'feedSkillList', //技能列表
        3 => 'star', //星级
        4 => 'blessing', //当前祝福值
        5 => 'showId', //当前外观
        6 => 'showList', //外观列表
        7 => 'rankSkillList', //技能列表
        8 => 'magicShowList', //幻化列表
        9 => 'refineList', //修炼列表
        10 => 'fazhenId', //当前法阵外观
        11 => 'fazhenList' //法阵列表
    );

    //仙器修炼类型列表
    protected static $RideRefineInfoTypeName = array(
        0 => '器灵锐',
        1 => '器灵御',
        2 => '器灵攻',
        3 => '器灵迅'
    );

    //角色仙法字段
    protected static $ActorSkillFields = array(
        0 => 'Skills', //技能列表
        1 => 'Points' //未激活技能列表
    );

    //仙法字段
    protected static $SkillFields = array(
        0 => 'SkillId', //技能ID
        1 => 'level' //技能等级
    );

    //法宝字段
    protected static $AmuletFields = array(
        0 => 'refineList', // 法宝列表
        1 => 'riseLevel', //修为等级
        2 => 'cultivation' //当前修为
    );

    //法宝列表字段
    protected static $AmuletRefineListFields = array(
        0 => 'id', //法宝id
        1 => 'level' //等级
    );

    //法宝分类
    protected static $AmuletRefineTypeFields = array(
        1 => '后天',
        2 => '先天',
        3 => '混沌',
        4 => '鸿蒙'
    );

    //金身字段
    protected static $SoulFields = array(
        0 => 'refineList', //金身列表
        1 => 'riseLevel' //不败金身等级
    );

    //金身列表字段
    protected static $SoulRefineFields = array(
        0 => 'type', //类型
        1 => 'level' //等级
    );

    //金身类型列表
    protected static $SoulTypeName = array(
        0 => '无垢体',
        1 => '琼玉体',
        2 => '琉璃体',
        3 => '菩提体',
        4 => '金刚体',
        5 => '归元体',
        6 => '通玄体',
        7 => '不灭体'
    );

    //神兵字段
    protected static $ShenbingFields = array(
        0 => 'level', //喂养等级
        1 => 'feedSkillList', //技能列表
        2 => 'magicShowId', //当前幻化外观
        3 => 'showList', //幻化列表
        4 => 'refineList' //修炼列表
    );

    //仙翼字段
    protected static $WingFields = array(
        0 => 'level', //喂养等级
        1 => 'feedSkillList', //技能列表
        2 => 'magicShowId', //当前幻化外观
        3 => 'showList', //幻化列表
        4 => 'refineList' //修炼列表
    );

    //神兵幻化列表字段
    protected static $ShenbingMagicShowFields = array(
        0 => 'showId', //幻化id
        1 => 'level' //等级
    );

    //神兵修炼列表类型名称
    protected static $ShenbingRefineInfoTypeName = array(
        0 => '刑天兵魂',
        1 => '幽天兵魂',
        2 => '昊天兵魂',
        3 => '钧天兵魂'
    );

    //仙翼修炼列表类型名称
    protected static $WingRefineInfoTypeName = array(
        0 => '朱雀羽魂',
        1 => '毕方羽魂',
        2 => '鲲鹏羽魂',
        3 => '红鸾羽魂'
    );

    //背包字段
    protected static $BagInfoFields = array(
        0 => 'Bags', //背包列表
    );

    //背包列表字段
    protected static $BagFields = array(
        0 => 'bagId',
        1 => 'items',
    );

    //道具字段
    protected static $ItemFields = array(
        0 => 'itemId', //道具id
        1 => 'count', //数量
        2 => 'iMsg', //信息
        3 => 'createTm' //创建时间
    );

    //bagId对应名称
    protected static $BagIdTypeName = array(
        0 => 'itemType', //道具类
        1 => 'equipType', //装备类
        2 => 'stoneType', //仙石类
        3 => 'magicWeaponType' //法宝类
    );

    //角色仙位表字段
    protected static $XianweiFields = array(
        0 => 'updateTime', //上一次更新时间
        1 => 'wages', //未领取俸禄表
        2 => 'list',
        3 => 'riseId', //当前升阶ID
        4 => 'riseExp' //当前升阶仙力
    );

    //Vip字段
    protected static $VipFields = array(
        0 => 'grade', // 等级
        1 => 'exp', // 当前经验
        2 => 'rewardList', // 已领奖励列表
        3 => 'dayRewardState', //每日奖励是否领取
        4 => 'updateTime' //更新时间
    );

    //充值订单表字段
    public static $RechargeOrderRecord = array(
        0 => 'orderId', // 订单号
        1 => 'objId', // 玩家id
        2 => 'money', // 充值人民币
        3 => 'index', // 充值的档位
        4 => 'status', // 状态
        5 => 'buyTm', // 充值时间
        6 => 'isFake' // 是否假订单
    );

    //排行榜表字段
    public static $RankMgrFields = array(
        0 => 'actorRanks', //所有的角色排行
        1 => 'judageRecords',
        2 => 'recordUpdateTime'
    );

    public static $ActorRanksFields = array(
        0 => 'agentId',            /*玩家ID*/
        1 => 'occ',            /*职业*/
        2 => 'name',            /*玩家名*/
        3 => 'vip',            /*VIP*/
        4 => 'ranks',
        5 => 'show',            /*角色外观*/
        6 => 'vipF'
    );

    public static $RankDataFields = array(
        0 => 'rankType', //排行类型
        1 => 'param', //参数
        2 => 'tm' //时间
    );

    public static $ActorRankShowFields = array(
        0 => 'fashion', //时装
        1 => 'shenbing', //神兵
        2 => 'wing', //翅膀
        3 => 'pet', //灵宠
        4 => 'ride', //仙器
        5 => 'amulet' //法宝修为
    );

    public static $RuneFields = array(
        0 => 'slot', //符文
        1 => 'exp', //符文经验
        2 => 'rFlags', //分解勾选
        3 => 'eFlags' // 兑换勾选
    );

    public static $RuneSlotFields = array(
        0 => 'id', //符文槽序号id
        1 => 'itemId' //镶嵌符文id
    );

    public static $RuneType = array(
        2 => '蓝',
        3 => '紫',
        4 => '橙',
        5 => '红'
    );

    //仙府信息字段
    public static $XianFuFields = array(
        0 => 'level',            /*仙府等级*/
        1 => 'lingQi',            /*灵气值*/
        2 => 'riches',            /*财富值*/
        3 => 'active',            /*活跃值*/
        4 => 'activeIndex',            /*活跃值已领取的档位*/
        5 => 'updateTime',            /*更新时间*/
        6 => 'buildInfo',            /*建筑信息*/
        7 => 'spiritAnimalInfo',            /*灵兽信息*/
        8 => 'illBookInfo',            /*图鉴信息*/
        9 => 'eventInfo',            /*事件信息*/
        10 => 'taskInfo',            /*任务信息*/
        11 => 'fengShuiInfo'            /*风水信息*/
    );

    //仙府灵兽信息表字段
    public static $SpiritAnimalFields = array(
        0 => 'id',            /*灵兽id*/
        1 => 'level',            /*灵兽等级*/
        2 => 'exp',            /*灵兽经验*/
        3 => 'travelTime',            /*游历开始时间*/
        4 => 'rangeId',            /*范围id*/
        5 => 'state',            /*灵兽状态*/
        6 => 'extraId',            /*额外风信子id*/
        7 => 'isAmulet',            /*是否使用护身符*/
        8 => 'endPackIndex',            /*终点礼包索引*/
        9 => 'passByPackIndex',            /*沿途礼包索引*/
        10 => 'travelCount',            /*每日游历次数*/
    );

    //仙府灵兽图鉴信息字段
    public static $IllBookFields = array(
        0 => 'id', //图鉴id : 等级
        1 => 'resId' //图鉴资源id : 数量
    );

    //灵兽图鉴id信息字段
    public static $IllBookIdFields = array(
        0 => 'id', //id
        1 => 'level' //等级
    );

    //灵兽图鉴对应品质
    public static $IllBookQuality = array(
        1 => '蓝品',
        2 => '紫品',
        3 => '橙品',
        4 => '红品'
    );

    //灵兽图鉴资源id信息字段
    public static $IllBookResIdFields = array(
        0 => 'id', //id
        1 => 'count' //数量
    );

    //仙府风水信息表
    public static $XianFuFengShuiFields = array(
        0 => 'exp',
        1 => 'level', //风水等级
        2 => 'decorate' //激活的物件
    );

    //仙府风水物件分类
    public static $XianfuDecorateType = array(
        1 => '祥饰',
        2 => '吉物',
        3 => '瑞景'
    );

    public function __construct()
    {
        $this->redis = Util::redisConn();
    }

    public function arrayCombine(array $fields, $values)
    {
        if (empty($values)) {
            $values = [];
        }

        $fieldsCount = count($fields);
        $valuesCount = count($values);

        if ($fieldsCount > $valuesCount) {

            $num = $fieldsCount - $valuesCount;

            for ($i = $fieldsCount + 1; $i <= $fieldsCount + $num; $i++) {
                array_push($values, $i);
            }
        } else if ($valuesCount > $fieldsCount) {
            $num = $valuesCount - $fieldsCount;

            for ($i = $valuesCount + 1; $i <= $valuesCount + $num; $i++) {
                array_push($fields, $i);
            }
        }

        return array_combine($fields, $values);
    }


}