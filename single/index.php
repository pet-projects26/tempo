<?php
ini_set('display_errors', 'off');
error_reporting(0);
date_default_timezone_set('Asia/Shanghai');
define('IN_WEB' , true);
define('ROOT' , __DIR__);
$server_id = getParam('s');
if($server_id){
    define('CONFIG_SERVER_ID' , $server_id);
    //内网测试的服务器使用的配置，一套单服代码多份db配置
    $file = ROOT . '/config/db_' . $server_id . '.php';
    if(!file_exists($file)){
        @file_put_contents(ROOT . '/config/db_' . CONFIG_SERVER_ID . '.php','');
    }
    require_once ROOT . '/config/db_' . $server_id . '.php';
}
else{
    //外网正式服配置
    require_once ROOT . '/config/db.php';
}
require_once ROOT . '/includes/Util.php';
require_once ROOT . '/includes/Fields.php';
require_once ROOT . '/includes/redisServer.php';
define('DATA_DIR' , ROOT . '/../bin/config/config/');
spl_autoload_register('autoload');
$dirs = array(
    get_include_path(),
    ROOT . '/includes',
    ROOT . '/controller',
    ROOT . '/model',
    ROOT . '/redis'
);
set_include_path(implode(PATH_SEPARATOR , $dirs));

//检查签名
/*
$sign     = $_REQUEST['sign'];
$unixtime = $_REQUEST['unixtime'];
if(md5(MDKEY . $unixtime) != $sign){
    exit('invalid sign');
}
*/

$r = $_REQUEST['r'];
if($r == 'shell'){
    $params = $_REQUEST['params'];
    echo Shell::execute($_REQUEST['block'], $params);
}
//直接调用方法
else if($r == 'call_method'){
    $class = $_POST['class'];
    $method = $_POST['method'];
    $params = json_decode($_POST['params'] , true);
    try{
        $reflection_method = new ReflectionMethod($class, $method);
    }
    catch(Exception $e){
        exit(json_encode(array('errno' => 1 , 'error' => $e->getMessage())));
    }
    $argv = array();
    $reflection_params = $reflection_method->getParameters();
    foreach($reflection_params as $p){
        $name = $p->getName();
        if(isset($params[$name])){
            $argv[] = ($p->isArray() && !is_array($params[$name])) ? json_decode($params[$name] , true) : $params[$name];
        }
        else if($p->isDefaultValueAvailable()){
            $argv[] = $p->getDefaultValue();
        }
        else{
            exit(json_encode(['errno' => 2 , 'error' => "missing required parameter '$name'"]));
        }
    }
    $reflection_method->invokeArgs(new $class, $argv);
}
else{
    $splits = explode('/' , $r);
    if(count($splits) != 2){
        return;
    }
    $controller = ucfirst(strtolower($splits[0])) . 'Controller';
    $action     = 'Action' . ucfirst($splits[1]);
    if(!class_exists($controller) || !method_exists($controller , $action)){
        exit("404");
    }
    $c = new $controller();
    $c->$action();
}

function autoload($className){
    @include_once $className . '.php';
}

function removeComment($content){
    return preg_replace("/(\/\*.*?\*\/)|(#.*?\n)|(\/\/.*?\n)/s" , '' , str_replace(array("\r\n" , "\r") , "\n" , $content));
}

function getParam($name = '' , $default = ''){
    return isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $default);
}