<?php

class AccLoginModel extends Model{

    private $r;

    public function __construct(){
        parent::__construct('acc_login');
        $this->r = new RoleModel();
        $this->alias = 'l';
    }
	//登录人数
    public function getAccountLogNumByDate($date , array $account = array()){
        $fields = array('count(distinct account) as count_account_id');
        $conditions = array();
        $account && $conditions['WHERE']['account::IN'] = $account;
        $conditions['WHERE']["from_unixtime(start_time/1000,'%Y-%m-%d')"] = $date;
        $rs = $this->getRow($fields , $conditions['WHERE']);
        return $rs['count_account_id'];
    }

    //获取玩家在线时长 old 等于 1，则算的是老玩家（算账号）
    public function getOlTimeByDate($date , $time,$old = 0){
        $fields = array('l.id' , 'l.account' , 'l.start_time' , 'l.end_time');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(l.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRows($fields , $conditions['WHERE']);
        $rss = array();
        foreach($rs as $k => $row){
            !isset($rss[$row['account']]) && $rss[$row['account']] = 0;
            //如果角色已登出则在线时长等于 end_time 减去 start_time ，否则等于当前时间减去 start_time
            $rss[$row['account']] += $row['end_time'] ? (round(round($row['end_time']/1000, 2) - round($row['start_time']/1000, 2), 2)) : (round($time - round($row['start_time']/1000, 2), 2));
        }
        return $rss;
    }


//    public function getAccountLogByDate($date , array $account = array()){
//        $fields = array('distinct account as account');
//        $conditions = array();
//        $account && $conditions['WHERE']['account::IN'] = $account;
//        $conditions['WHERE']["from_unixtime(start_time/1000,'%Y-%m-%d')"] = $date;
//        $rs = $this->getRows($fields , $conditions['WHERE']);
//
//        $data = [];
//
//        foreach ($rs as $value) {
//            array_push($data , $value);
//        }
//        return $data;
//    }

//    //获取登录账户和登录开始结束时间
//    public function getAccountLogData($date, array $account = array())
//    {
//        $fields = array('account, start_time, end_time');
//        $conditions = array();
//        $account && $conditions['WHERE']['account::IN'] = $account;
//        $conditions['WHERE']["from_unixtime(start_time/1000,'%Y-%m-%d')"] = $date;
//        $rs = $this->getRows($fields , $conditions['WHERE']);
//        return $rs;
//    }


    //获取某个时间段内的玩家登录服务器次数
    public function getAccountLoginNumByBetweenDate($date, $end_date)
    {
        $fields = array('count(distinct account) as count_account_id');
        $conditions = array();
        $date && $conditions['WHERE']['start_time::>='] =  $date;
        $end_date && $conditions['WHERE']['start_time::<='] =  $end_date;
        $rs = $this->getRow($fields , $conditions['WHERE']);
        return $rs['count_account_id'];
    }

    //获取用户连接数
    public function getNewAccountLogNumByDate($date , $first = 2,array $account = array()){
        $fields = array('count(distinct account) as count_account_id');
        $conditions = array();
        $account && $conditions['WHERE']['account::IN'] = $account;
        $conditions['WHERE']["from_unixtime(start_time/1000,'%Y-%m-%d')"] = $date;
        if($first != 2) $conditions['WHERE']["first"] = $first;
        $rs = $this->getRow($fields , $conditions['WHERE']);
        return $rs['count_account_id'];
    }

    //获取新增用户
    public function getNewAccountByDate($date , $first = 2,array $account = array()){
        $fields = array('distinct account as account');
        $conditions = array();
        $account && $conditions['WHERE']['account::IN'] = $account;
        $conditions['WHERE']["from_unixtime(start_time/1000,'%Y-%m-%d')"] = $date;
        if($first != 2) $conditions['WHERE']["first"] = $first;
        $rs = $this->getRows($fields , $conditions['WHERE']);

        $data = [];

        foreach ($rs as $value) {
            array_push($data , $value);
        }
        return $data;
    }

    //获取老玩家数
    public function getOldAccNum($date)
    {
        $fields = array('count(distinct account) as count_account_id');
        $conditions = array();
        $account = $this->getNewAccountByDate($date, 0);
        $account && $conditions['WHERE']['account::IN'] = $account;
        $conditions['WHERE']["from_unixtime(create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->r->getRow($fields , $conditions['WHERE']);

        return $rs['count_account_id'];
    }

    //获取老玩家
    public function getOldAcc($date)
    {
        $fields = array('distinct account as account');
        $conditions = array();
        $account = $this->getNewAccountByDate($date, 0);

        if (!$account) return 0;

        $account && $conditions['WHERE']['account::IN'] = $account;
        $conditions['WHERE']["from_unixtime(create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->r->getRows($fields , $conditions['WHERE']);
        $data = [];

        foreach ($rs as $value) {
            array_push($data , $value);
        }
        return $data;
    }



}