<?php

class AccountDataModel extends Model{

    public function __construct(){
        parent::__construct('');
    }
    //执行shell脚本
    public function execShellOpenServer($conditions)  {
//     	$db = $conditions['db'];
//     	$open_time = $conditions['open_time'];
//     	$path="cron/";
//     	$command = "export LC_ALL=zh_CN.UTF-8;cd %s;/bin/bash open_game.sh " . $db  . " " . $open_time;
//     	$command = sprintf($command, $path);
//     	echo $command."<br/>";
    	
//     	$handle = @popen($command,'r');
//     	$res = stream_get_contents($handle);
//     	pclose($handle);
//     	echo $res;
    	 
    	$db = $conditions['db'];
    	$open_time = $conditions['open_time'];
    	$shell = "sudo /bin/bash cron/auto_game.sh  $db $open_time";
    	exec($shell, $result, $status);
    	echo json_encode($status);
    	 
    }
    //获取所有注册数
    public function getAllRegister()  {
    	$sql = "SELECT COUNT(DISTINCT account) AS count FROM  ny_role ";
    	$res = $this->query ( $sql );
    	echo json_encode($res[0]);
    }

    //新增账号数
    public function getCreateAccount($conditions)  {
    	$sql = "SELECT COUNT(DISTINCT account) AS account
    	FROM  ny_role
    	WHERE create_time  BETWEEN {$conditions['start_time']} AND {$conditions['end_time']} ";
		$res = $this->query ( $sql );
// 		file_put_contents('/tmp/role.txt', json_encode($res)."\n\r",FILE_APPEND);
		echo json_encode($res[0]);
    }
    //角色登录
    public function getLoginRole($conditions)  {
    	$sql = "SELECT DISTINCT role_id AS role_id
    	FROM  ny_login
    	WHERE start_time  BETWEEN {$conditions['start_time']} AND {$conditions['end_time']} ";
    	$data = [];
		$res = $this->query ( $sql );
		if ($res != array()) {
			foreach ($res as $value) {
				$data[] = $value['role_id'];
			}
		}
		echo json_encode($data);
    }
    //新增的角色
    public function getCreateRoleid($conditions)  {
    	$sql = "SELECT DISTINCT role_id AS role_id
    	FROM  ny_role
    	WHERE create_time  BETWEEN {$conditions['start_time']} AND {$conditions['end_time']} ";
    	$data = [];
		$res = $this->query ( $sql );
		if ($res != array()) {
			foreach ($res as $value) {
				$data[] = $value['role_id'];
			}
		}
		echo json_encode($data);
    }
    //获取付费人数和活跃数(角色)
    public function getPayRoles($conditions)  {
    	$sql = " SELECT COUNT(DISTINCT role_id) AS payRoles FROM ny_order
    	WHERE create_time >={$conditions['start_time']} AND create_time < {$conditions['end_time']}  ";
    	$res = $this->query ( $sql );
    
    	$sql = "SELECT COUNT(DISTINCT role_id) AS loginRoles
    	FROM  ny_login
    	WHERE start_time  BETWEEN {$conditions['start_time']} AND {$conditions['end_time']} ";
		$res2 = $this->query ( $sql );
    	echo json_encode([$res[0]['payRoles'],$res2[0]['loginRoles']]);
    }
    //获取当天新增的账户和总数(账户)
    public function getNewAccount($conditions)  {
    	$sql = "SELECT DISTINCT account AS oldAccount FROM  ny_role WHERE  create_time <  {$conditions['start_time']}";
    	$res = $this->query ( $sql );
    	 
    	$sql = "SELECT DISTINCT account AS todayAccount FROM  ny_role WHERE create_time  BETWEEN {$conditions['start_time']} AND {$conditions['end_time']} ";
    	$res2 = $this->query ( $sql );
    	$oldAccount = $todayAccount = [];
    	foreach ($res as $value) {
    		$oldAccount[] = $value['oldAccount'];
    	}
    	foreach ($res2 as $value) {
    		$todayAccount[] = $value['todayAccount'];
    	}
    	$new = count(array_diff($todayAccount,$oldAccount));
    	$all = count($oldAccount) + $new;
    	echo json_encode([$all,$new]);
    }
    //获取七天和一个月的角色登录数
    public function getAccountLogins($conditions)  {
    	$end_time = $conditions['end_time'];
    	$week['start_time'] = $end_time - 7 * 60 * 60 * 24;//往前7天
    	$week['end_time'] = $end_time;
    	$month['start_time'] = $end_time - 30 * 60 * 60 * 24;//往前30天
    	$month['end_time'] = $end_time;
    	 
    	 
    	$sql = " SELECT COUNT(DISTINCT role_id) AS role_id_7 FROM ny_login
    	WHERE start_time >={$week['start_time']} AND start_time < {$week['end_time']}  ";
    	$res = $this->query ( $sql );
    	$sql = "SELECT COUNT(DISTINCT role_id) AS role_id_30
    			FROM  ny_login
    			WHERE start_time  >= {$month['start_time']} AND  start_time < {$month['end_time']} ";
    	$res2 = $this->query ( $sql );
    	echo json_encode([$res[0]['role_id_7'],$res2[0]['role_id_30']]);
    }
}