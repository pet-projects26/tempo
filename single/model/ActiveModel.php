<?php
class ActiveModel extends Model{
    public function __construct(){
        parent::__construct('active');
        $this->alias = 'a';
    }
	public function record_data($conditions, $from = 'json'){

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", "active_num", 'login_num', 'avg_login_num', 'avg_login_longtime', 'new_acc_num', 'new_acc_avg_login_num', 'new_acc_avg_login_longtime', 'old_acc_num', 'old_acc_avg_login_num', 'old_acc_avg_login_longtime');

        $rs = $this->getRows($fields , $conditions['WHERE']);
        if ($from != 'json') {
            return $rs;
        } else {
            echo json_encode($rs);
        }
	}

    public function getRowData($time)
    {

        $conditions = [];
        $conditions['WHERE']['create_time'] = $time;

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", "active_num", 'login_num', 'avg_login_num', 'avg_login_longtime', 'new_acc_num', 'new_acc_avg_login_num', 'new_acc_avg_login_longtime', 'old_acc_num', 'old_acc_avg_login_num', 'old_acc_avg_login_longtime');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }

    public function setActive($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }
}