<?php

class ActivityModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [添加活动]
     * @param [array] $document [写入mongo的数组]
     */
    public function addAct($document, $die = true)
    {
        ini_set('mongo.native_long', 0);
        ini_set('mongo.long_as_object', 1);

        $_id = $document['_id'];
        $db = Util::mongoConn();

        $db->bg_activity->remove(array('_id' => $document['_id']));

        $aId = intval($document['aId']);
        $document['aId'] = $aId;


        $start = $document['tm']['start'];
        $close = $document['tm']['close'];

        $db = Util::mongoConn();

        $rs = $db->bg_activity->find(array('aId' => $aId));

        $vars = array();

        foreach ($rs as $k => $row) {
            $m_start = $row['tm']['start'];
            $m_close = $row['tm']['close'];

            //查找是否有交集, 删除掉
            if ($m_start <= $start && $m_close > $start) {
                $db->bg_activity->remove(array('_id' => $row['_id']));
                continue;
            }

            if ($m_start >= $start && $m_start > $close) {
                $db->bg_activity->remove(array('_id' => $row['_id']));
            }

        }

        $document = Helper::mongoInt64($document);

        $rs = $db->bg_activity->insert($document);

        $return = isset($rs['ok']) ? $rs['ok'] : 0;

        if ($die) {
            die ($return);
        }

        return $return;
    }

    /**
     * [检查活动类型]
     * @return [array] [类型]
     */
    public function checkAct($document, $die = true)
    {
        $start = $document['start'];
        $close = $document['close'];
        $aId = $document['aId'];

        try {
            ini_set('mongo.native_long', 0);
            ini_set('mongo.long_as_object', 1);

            $aId = intval($document['aId']);
            $uuid = $document['_id'];

            $db = Util::mongoConn();

            $rs = $db->bg_activity->find(array('aId' => $aId, '_id' => array('$ne' => $uuid)));


            $vars = array();

            foreach ($rs as $k => $row) {
                $m_start = $row['tm']['start'];

                $m_close = $row['tm']['close'];


                //查找是否有交集
                if ($m_start <= $start && $m_close > $start) {
                    $vars[$k] = array('_id' => $row['_id'], 'name' => $row['name']);
                    continue;
                }

                if ($m_start >= $start && $m_start < $close) {
                    $vars[$k] = array('_id' => $row['_id'], 'name' => $row['name']);
                    continue;
                }
            }

            if ($die) {
                die(json_encode($vars));
            }
            return $vars;

        } catch (\Exception $e) {
            $vars = $e->getMessage();

            if (!is_array($vars)) {
                $vars = array($vars);
            }

            if ($die) {
                die(json_encode($vars));
            }

            return $vars;
        }

    }

    /**
     * 检查多条活动是否存在
     * @param  [type] $document [description]
     * @return [type]           [description]
     */
    public function checkMultAct($document)
    {
        $msg = array();
        foreach ($document as $row) {
            $rs = $this->checkAct($row, false);

            if (!empty($rs)) {
                foreach ($rs as $r) {
                    $msg[] = $r;
                }
            }
        }

        die(json_encode($msg));
    }

    /**
     * 插入多条
     * @param [type] $document [description]
     */
    public function addMultAct($document)
    {
        $code = array();
        foreach ($document as $row) {
            $code[] = $this->addAct($row, false);
        }

        array_unique($code);

        if (count($code) != 1) {
            echo 0;
            die();
        }

        if ($code[0] != 1) {
            echo 0;
            die();
        }

        echo 1;
        die();
    }

    /**
     * [删除活动]
     * @param  [string] $id [活动ID]
     * @return [type]     [description]
     */
    public function resetAct($document)
    {
        $id = $document['_id'];

        ini_set('mongo.native_long', 0);
        $db = Util::mongoConn();

        if (is_array($id)) {
            foreach ($id as $k => $v) {
                $rs = $db->bg_activity->remove(array('_id' => $v));
            }
        } else {
            $rs = $db->bg_activity->remove(array('_id' => $id));
        }

        $return = isset($rs['ok']) ? $rs['ok'] : 0;
        print_r($return);
        die();
    }
}