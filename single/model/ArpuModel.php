<?php
class ArpuModel extends Model{

    public function __construct(){
        parent::__construct('arpu');
        $this->alias = 'a';
    }

    public function getArpuData($conditions){
        $fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'arpu' , 'arppu' , 'first_arpu' , 'reg_arpu');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getArpuByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $fields = array('date' , 'arpu' , 'arppu' , 'first_arpu' , 'reg_arpu');
        return $this->getRow($fields , $conditions['WHERE']);
    }

    public function addArpu($data){
        return $this->add($data);
    }
}