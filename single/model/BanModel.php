<?php
class BanModel extends Model{
    public function __construct(){
        parent::__construct('ban');
        $this->alias = 'b';
    }

    public function getRecordData($conditions){
        $fields = array('id' , 'name' , 'type' , 'time' , 'reason' , 'user' , 'other' , 'create_time' , 'deleted');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getBanByTypeName($type = '' , $name = array() , $fields = array()){
        empty($fields) && $fields = array('id' , 'name' , 'type' , 'time' , 'reason' , 'user' , 'other' , 'create_time' , 'deleted');
        $conditions = array();
        if(is_array($name)){
            $name && $conditions['WHERE']['name::IN'] = $name;
        }
        else{
            $name && $conditions['WHERE']['name'] = $name;
        }
        $type && $conditions['WHERE']['type'] = $type;
        $conditions['WHERE']['deleted'] = 0;
        $rs = $this->getRows($fields , $conditions['WHERE']);
        echo json_encode($rs);
    }

    public function addBan($data){
        $content = array();
        foreach($data as $row){
            if($id = $this->add($row)){
                $content[$id] = $row['name'];
            }
            else{
                exit(json_encode(array('state' => false)));
            }
        }
        echo json_encode(array('state' => true , 'content' => $content));
    }

    public function updateBan($data , $id){
        $conditions = array();
        $id = implode(',', $id);
        $rs = $this->update($data , array('id' => $id ));
        $json = array('state' => $rs ? true : false);
        echo json_encode($json);
    }

    public function delBan($id)
    {
    	$conditions = array();
    	$data = array('deleted' => 1);
        $rs = $this->update($data, array('id::=' => $id));
    	$json = array('state' => $rs ? true : false);
    	echo json_encode($json);
    }

    public function getBan($id)
    {
        $fields = ['name', 'type', 'time', 'reason', 'other', 'deleted'];
        $rs = $this->getRow($fields, ['id::=' => $id]);
        echo json_encode($rs);
    }
}