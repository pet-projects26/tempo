<?php

class BosshomeModel extends Model{

    public function __construct()
    {
        parent::__construct('boss_home');
        $this->alias = 'z';
        $this->joinTable = array(
            'r' => array('name' => 'role' , 'type' => 'LEFT' , 'on' => 'z.role_id = r.role_id')
        );
    }

	//获取时间内的数据
    public function getDataByRangtime($layer, $start_time, $end_time)
    {
        if (!array_key_exists($layer, CDict::$bossHomeLayer)) return false;
        $fields = array('count(distinct z.role_id) as role_num', 'count(z.role_id) as role_count');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["z.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["z.create_time/1000::<="] = $end_time;
        $conditions['WHERE']["z.boss_layer"] = $layer;

        //获取本层参与人数和boss总死亡次数
        $fields2 = array('count(distinct z.role_id) as join_role','sum(z.reward) as boss_die_num');
        $join_num = $this->getRow($fields2 , $conditions['WHERE']);

        //获取本层免费进入玩家数(去重 和 免费进入总次数
        $conditions['WHERE']['z.is_pay'] = 0;
        $free = $this->getRow($fields, $conditions['WHERE']);

        //获取本次付费进入玩家数(去重 和 付费进入总次数
        $conditions['WHERE']['z.is_pay'] = 1;
        $pay = $this->getRow($fields, $conditions['WHERE']);

        $data['layer_join_num'] = $join_num['join_role'] ? $join_num['join_role'] : 0;  //参与人数
        $data['boss_die_num'] = $join_num['boss_die_num'] ? $join_num['boss_die_num'] : 0;
        $data['free_num'] = $free['role_num'] ? $free['role_num'] : 0;
        $data['free_count'] = $free['role_count'] ? $free['role_count'] : 0;
        $data['pay_num'] = $pay['role_num'] ? $pay['role_num'] : 0;
        $data['pay_count'] = $pay['role_count'] ? $pay['role_count'] : 0;

        echo json_encode($data);
    }

    //获取时间内的boss之家参与人数
    public function getJoinNumByRangtime($start_time, $end_time)
    {
        $fields = array('count(distinct z.role_id) as count_role');
        $conditions = array();
        $conditions['WHERE']["z.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["z.create_time/1000::<="] = $end_time;
        $join_num = $this->getRow($fields , $conditions['WHERE']);

        $joinNum = $join_num['count_role'] ? $join_num['count_role'] : 0;

        echo json_encode($joinNum);
    }


    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
//    public function getActionOpenToAccount($accounts = array(), $output = 1)
//    {
//        //获取副本对应的功能id
//        $reId = CDict::$bossHome['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = (new ZonesModel())->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

