<?php

class CareerModel extends Model{

    public function __construct(){
        parent::__construct('career');
        $this->alias = 'cr';
    }

    public function getCareer($role_id = '' , $fields = array()){
        $conditions = array();
        if(is_array($role_id)){
            empty($fields) && $fields = array('role_id' , 'career' , 'create_time');
            $conditions['WHERE'] = array();
            $role_id && $conditions['WHERE']['role_id::IN'] = $role_id;
            $conditions['Extends']['ORDER'] = 'id#desc';
            return $this->getRow($fields , $conditions['WHERE'] , $conditions['Extends']);
        }
        else{
            $fields = array('career');
            $role_id && $conditions['WHERE']['role_id'] = $role_id;
            $rs = $this->getRow($fields , $conditions['WHERE']);
            return $rs['career'];
        }
    }
}