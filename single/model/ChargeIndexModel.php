<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/4/30
 * Time: 16:49
 */

class ChargeIndexModel extends Model
{
    public function __construct()
    {
        parent::__construct('order');
        $this->alias = 'o';
    }

    //获取时间内的统计数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $conditions = [];
        $conditions['WHERE']['o.create_time::>='] = $start_time;
        $conditions['WHERE']['o.create_time::<='] = $end_time;

        $conditions['Extends']['GROUP'] = 'o.item_id';

        $fields = array('o.item_id as `index`', 'sum(o.money) as charge_money', 'count(o.id) as charge_count', 'count(distinct o.role_id) as charge_num');

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rsFormat = [];

        if ($rs) {
            foreach ($rs as $key => $val) {
                $rsFormat[$val['index']] = $val;
            }
            unset($key);
            unset($val);
        }

        $recharge = $this->recharge(2);

        $return  = [];
        if ($recharge) {
            foreach ($recharge as $index => $row) {

                $dat = [];
                $dat['index'] = $row['index'];
                $dat['name'] = $row['name'];
                $dat['price'] = $row['price'];

                if (!empty($rsFormat) && array_key_exists($index, $rsFormat)) {
                    $charge = $rsFormat[$index];
                    $dat['charge_money'] = $charge['charge_money'] ? $charge['charge_money'] : 0;
                    $dat['charge_count'] = $charge['charge_count'] ? $charge['charge_count'] : 0;
                    $dat['charge_num'] = $charge['charge_num'] ? $charge['charge_num'] : 0;
                } else {
                    $dat['charge_money'] = 0;
                    $dat['charge_count'] = 0;
                    $dat['charge_num'] = 0;
                }

                $return[] = $dat;
            }
        }

        echo json_encode($return);
    }
}