<?php

class ChargeModel extends Model
{
    public function __construct()
    {
        parent::__construct('order');
        $this->alias = 'o';
        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'r.role_id = o.role_id'),
            'l' => array('name' => 'login', 'type' => 'LEFT', 'on' => 'l.role_id = o.role_id')
        );
    }

    public function getRecordData($conditions)
    {
        $fields = array(
            'id', 'role_name', 'account', 'role_level', 'money', 'gold', 'order_num',
            "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'status'
        );
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode(array($rs, $this->getCount()));
    }

    public function getRecordExport($conditions)
    {
        unset($conditions['Extends']['LIMIT']);
        $fields = array(
            'id', 'order_num', 'account', 'money', 'gold', 'role_id', 'role_name',
            'role_level', 'role_career', "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'status', 'first'
        );
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode($rs);
    }

    public function getLevelChargeData($conditions)
    {
        $time_field = "from_unixtime(start_time,'%Y-%m-%d')";
        !isset($conditions['WHERE'][$time_field]) && $conditions['WHERE'][$time_field] = date('Y-m-d');
        $section = 10;
        $max_level = CDict::$maxLevel;

        $spare_level = $max_level % $section;
        $near_level = $max_level - $spare_level;
        $fields = array();
        for ($s = 0; $s < $near_level; $s += $section) {
            $e = $s + $section;
            $fields[] = 'sum(role_level >= ' . $s . ' and role_level < ' . $e . ') as l' . $e;
        }

        $spare_level && $fields[] = 'sum(role_level >= ' . $near_level . ' and role_level < ' . $max_level . ') as lnmax';
        $fields[] = 'sum(role_level = ' . $max_level . ') as lmax';
        $fields_str = implode(',', $fields);

        //今天或者选定的日期
        $day = $conditions['WHERE'][$time_field];
        $sql = 'select max(role_level) as role_level from ny_login ';
        $sql .= 'where role_level <= ' . $max_level . ' and ' . $time_field . "='" . $day . "' group by role_id";
        $sql = 'select ' . $fields_str . ' from (' . $sql . ') as level';
        $rss = (new LoginModel())->query($sql);

        $rss = $rss[0];
        foreach ($rss as $k => $row) {
            $rss[$k] = array('login_num' => ($row) ? $row : 0);
        }
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $conditions['WHERE']["from_unixtime(start_time,'%Y-%m-%d')"];
        unset($conditions['WHERE']["from_unixtime(start_time,'%Y-%m-%d')"]);


        $fields = array('role_id', 'role_level', 'money');
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);//充值人数，充值次数和充值金额

        foreach ($rs as $row) {
            $level = ($row['role_level'] == $max_level) ? 'lmax' : 'l' . (ceil($row['role_level'] / $section) * $section);
            $rss[$level]['role'][$row['role_id']][] = $row['money'];
        }
        //昨天或者选定的日期的昨天
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = date('Y-m-d', strtotime($day) - 86400);
        $last_rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        foreach ($last_rs as $row) {
            $level = ($row['role_level'] == $max_level) ? 'lmax' : 'l' . (ceil($row['role_level'] / $section) * $section);
            $last_rss[$level]['role'][$row['role_id']][] = $row['money'];
        }
        $rs = array();
        foreach ($rss as $k => $row) {
            $e = substr($k, 1);
            switch ($e) {
                case 'max':
                    $level = $max_level . '级（满级）';
                    break;
                case 'nmax':
                    $level = $near_level . '-' . ($max_level - 1) . '级';
                    break;
                default :
                    $level = ((int)$e - $section) . '-' . ((int)$e - 1) . '级';
                    break;
            }
            $charge_roles = 0;
            $charge_times = 0;
            $money = 0;
            $percent = 0;
            $last_money = 0;
            if (isset($row['role'])) {
                $charge_roles = count($row['role']);
                $percent = $row['login_num'] ? sprintf("%.2f", ($charge_roles / $row['login_num'] * 100)) . '%' : '0%';
                foreach ($row['role'] as $role) {
                    $charge_times += count($role);
                    $money += array_sum($role);
                }
            }
            if (isset($last_rss[$k]['role'])) {
                foreach ($last_rss[$k]['role'] as $role) {
                    $last_money += array_sum($role);
                }
            }

            $rs[] = array(
                $level,
                $row['login_num'],
                $charge_roles,
                $charge_times,
                $percent,
                $money,
                $last_money
            );
        }

        echo json_encode(array($rs, count($rs)));
    }

    public function getRankData($conditions)
    {
        unset($conditions['Extends']['LIMIT']);
        $end_time = isset($conditions['WHERE']['o.create_time::<=']) ? $conditions['WHERE']['o.create_time::<='] : time();
        $fields = array(
            'o.role_id', 'o.account', 'o.role_name', 'o.role_career', 'count(o.id) as count_id', 'max(o.money) as max_money',
            'sum(o.money) as sum_money', 'sum(o.gold) as sum_gold', 'max(o.create_time) as max_create_time', 'r.create_time as role_create_time'
        );
        $conditions['Extends']['GROUP'] = 'o.role_id';
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        //最后登录时间
        $conditions = array();
        $conditions['WHERE'] = array();
        $conditions['Extends']['GROUP'] = 'role_id';
        $fields = array('role_id', 'max(start_time) as final_login_time');
        $lgrs = (new LoginModel())->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $lg = array();
        foreach ($lgrs as $row) {
            $lg[$row['role_id'] . ''] = $row['final_login_time'];
        }
        //当前等级
        $conditions = array();
        $conditions['WHERE'] = array();
        $conditions['Extends']['GROUP'] = 'role_id';
        $fields = array('role_id', 'max(level) as level');
        $lvrs = (new LevelModel())->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $lv = array();
        foreach ($lvrs as $row) {
            $lv[$row['role_id'] . ''] = $row['level'];
        }
        $rss = array();
        foreach ($rs as $k => $row) {
            $start_time = isset($conditions['WHERE']['o.create_time::>=']) ? $conditions['WHERE']['o.create_time::>='] : $row['role_create_time'];
            $row['role_career'] = CDict::$career[substr($row['role_career'], 0, 1)][$row['role_career']];
            $rss[] = array(
                $k + 1,
                $row['account'],
                $row['role_name'],
                $row['role_career'],
                $lv[$row['role_id']] ? $lv[$row['role_id']] : 1,
                $row['max_money'],
                $row['sum_money'],
                $row['sum_gold'],
                $row['count_id'],
                date('Y-m-d H:i:s', $row['max_create_time']),
                date('Y-m-d H:i:s', $lg[$row['role_id']]),
                $this->time_format($start_time, $end_time)
            );
        }
        echo json_encode(array($rss, count($rss)));
    }

    /*
     * 获取指定日期的订单
     * 如果$roles有赋值，则获取指定账号在指定日期的订单
     * */
    public function getChargeByDate($date, array $accounts = array(), $fields = array(), $first = false)
    {
        empty($fields) && $fields = array('order_num', 'account', 'money', 'role_id', 'role_name', 'role_level', 'role_career', 'gold', 'first', 'create_time');
        $conditions = array();
        ($first != false) && $conditions['WHERE']['first'] = $first;
        $accounts && $conditions['WHERE']['account::IN'] = $accounts;
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['Extends']['ORDER'] = array('id#desc');
        return $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
    }

    //根据账号获取指定日期充值人数
    public function getChargeNumByDate($date, array $accounts = array())
    {
        $fields = array('count(distinct account) as count_account');
        $conditions = array();
        (count($accounts) > 0) && $conditions['WHERE']['account::IN'] = $accounts;
        if (is_array($date)) {
            $conditions['WHERE']['create_time::BETWEEN'] = array(strtotime($date[0]), strtotime($date[1]));
        } else {
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        }
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['count_account'];
    }

    //根据账号获取指定日期充值总金额
    public function getChargeMoneyByDate($date, array $accounts = array())
    {
        $fields = array('sum(money) as sum_money');
        $conditions = array();
        (count($accounts) > 0) && $conditions['WHERE']['account::IN'] = $accounts;
        if (is_array($date)) {
            $conditions['WHERE']['create_time::BETWEEN'] = array(strtotime($date[0]), strtotime($date[1]));
        } else {
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        }
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['sum_money'];
    }

    //获取账号指定日期的充值人数和充值总金额
    public function getChargeDataByDate($date, array $accounts = array())
    {
        $data = [];
        $num = $this->getChargeNumByDate($date, $accounts);
        $money = $this->getChargeMoneyByDate($date, $accounts);

        $data['num'] = $num ? $num : 0;
        $data['money'] = $money ? $money : 0.00;

        echo json_encode($data);
    }

    //获取指定日期的首冲人数
    public function getFirstChargeNum($date)
    {
        $fields = array('count(o.id) as count');
        $conditions = array();
        if (is_array($date)) {
            $conditions['WHERE']['o.create_time::BETWEEN'] = array(strtotime($date[0]), strtotime($date[1]) + 86399);
        } else {
            $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = $date;
        }
        $conditions['WHERE']['first::='] = 1;
        $rs = $this->getRow($fields, $conditions['WHERE']);

        $first = $rs['count'] ? $rs['count'] : 0;

        echo json_encode($first);
    }

    //根据角色id获取指定时间前的充值总金额
    public function getChargeMoneyByRole($time, array $roles = array())
    {
        $fields = array('sum(o.money) as totalcash');
        $conditions = array();
        (count($roles) > 0) && $conditions['WHERE']['o.role_id::IN'] = $roles;
        $time && $conditions['WHERE']['o.create_time::<='] = $time;

        $rs = $this->getRow($fields, $conditions['WHERE']);

        $totalcash = $rs['totalcash'] ? $rs['totalcash'] : 0;

        return $totalcash;
    }

    //获取指定日期的充值用户 (账号
    public function getChargeAccountsByDate($date, array $accounts = array(), $output = 1)
    {
        $fields = array('distinct o.account as account');
        $conditions = array();
        (count($accounts) > 0) && $conditions['WHERE']['o.account::IN'] = $accounts;
        if (is_array($date)) {
            $conditions['WHERE']['o.create_time::BETWEEN'] = array(strtotime($date[0]), strtotime($date[1]) + 86399);
        } else {
            $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = $date;
        }
        $rs = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        if ($rs) {
            foreach ($rs as $value) {
                array_push($data, $value['account']);
            }
        }

        if ($output) {
            echo json_encode($data);
        } else {
            return $data;
        }

    }

    //获取付费人数
    public function getPayingUserNum($date = '')
    {
        $fields = array('count(distinct o.account) as count_account');
        $conditions = array();

        $date && $conditions['WHERE']['o.create_time::<='] = strtotime($date);
        $conditions['WHERE']['o.status'] = 1;

        $rs = $this->getRow($fields, $conditions['WHERE']);

        $count = $role_id = $rs['count_account'] ? $rs['count_account'] : 0;

        echo json_encode($count);
    }

    //获取付费金额
    public function getPayingUserMoney($date, array $accounts = [])
    {
        $fields = array('sum(o.money) as sum_money');
        $conditions = array();
        $conditions['WHERE']['o.create_time::<='] = strtotime($date);
        $conditions['WHERE']['o.status'] = 1;
        (count($accounts) > 0) && $conditions['WHERE']['o.account::IN'] = $accounts;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        $count = $rs['sum_money'] ? $rs['sum_money'] : 0;

        echo json_encode($count);
    }

    //设置订单，$type为0则是游戏里面充值，$type为1则是后台充值
    public function setOrder($order, $type = '0')
    {
        $fields = array('id');
        $conditions = array();
        $conditions['WHERE']['role_id'] = $order['role_id'];
        $rs = $this->getRow($fields, $conditions['WHERE']);
        $first = empty($rs) ? 1 : 0;
        $now = time();
        $role = (new RoleModel())->getRole($order['role_id'], array('name', 'account', 'career'));
        $level = (new LevelModel())->getLevel($order['role_id']);
        $career = (new CareerModel())->getCareer($order['role_id']);
        $career = $career ? $career : $role['career'];
        $charge = $this->recharge(2);
        $json = array(
            'order_num' => $type . $now . $order['role_id'],
            'corder_num' => $order['order_num'],
            'money' => intval($order['money']),
            'account' => $role['account'],
            'role_id' => $order['role_id'],
            'role_name' => $role['name'],
            'role_level' => $level,
            'role_career' => $career,
            'gold' => $charge[$order['item_id']]['item_id'],
            'create_time' => $now,
            'status' => 0,
            'first' => $first
        );
        echo json_encode($json);
    }

    //充值 , code表示执行状态 1成功，2写入mysql失败，3程序执行错误
    public function addOrderOld($data, $item_id)
    {
        try {
            ini_set('mongo.long_as_object', 1);

            // //@todo 由于客户端获取不到玩家中文名称
            // $m = new Model('role');

            // $row = $m->getRow('*', array('id' => $data['role_id']));

            // $data['role_name'] = isset($row['role_name']) ? $row['role_name'] : $data['role_name'];

            $data['first'] = isset($data['first']) ? $data['first'] : 0;

            $exists = $this->getRow('*', array('order_num' => $data['order_num']));
            //如果存在，证明订单成功，不再通知
            if ($exists) {
                $json = array('state' => true, 'code' => 1);
                die(json_encode($json));
            }

            $document = array(
                '_id' => $data['order_num'],                    //订单号
                'item_id' => new MongoInt32($item_id),         //物品号
                'obj_id' => new MongoInt64($data['role_id']),  //角色ID
                'money' => new MongoInt32($data['money']),     //充值人民币数量
                'first' => new MongoInt32($data['first']),     //是否首冲
                'status' => new MongoInt32(0)                  //支付状态，php提供订单，游戏充值成功后端则会自动改写成1
            );
            $db = Util::mongoConn();

            $cur = $db->order->findOne(array('_id' => $data['order_num']));

            if (empty($cur)) {
                $rs = $db->order->insert($document, array('w' => 1));
            } else {
                $rs['err'] = null;
            }

            if ($rs['err'] == null) {
                //插入订单表
                $insertId = $this->add($data);

                if (empty($insertId)) {
                    $json = array('state' => false, 'code' => 2);
                    die(json_encode($json));
                }

                $json = array('state' => true, 'code' => 1);
                die(json_encode($json));
            } else {
                $json = array('state' => false, 'code' => 3);
                die(json_encode($json));
            }


        } catch (Exception $e) {
            echo $e->getMessage();
            $json = array('state' => false, 'code' => 4);
        }

        die(json_encode($json));
    }

    //充值 addOrder
    public function addOrder($data, $item_id)
    {
        try {

            $exists = $this->getRow('id', array('order_num' => $data['order_num']));

            //如果存在，证明订单成功，不再通知
            if ($exists) {
                $json = array('state' => true, 'code' => 1);
                die(json_encode($json));
            }

            $data['first'] = $this->checkIsFirst($data['role_id']);
            $data['item_id'] = $item_id;
            //插入订单表
            $insertId = $this->add($data);

            if (empty($insertId)) {
                $json = array('state' => false, 'code' => 2);
                die(json_encode($json));
            }

            $json = array('state' => true, 'code' => 1);
            die(json_encode($json));

        } catch (Exception $e) {
            echo $e->getMessage();
            $json = array('state' => false, 'code' => 3);
        }

        die(json_encode($json));
    }

    //查询是否为首单
    public function checkIsFirst($role_id)
    {
        $rs = $this->getRow('id', array('role_id' => $role_id));

        $first = $rs['id'] ? 0 : 1;

        return $first;
    }

    //补单
    public function supplementOrder($data, $item_id)
    {
        //查询单服是否存在该记录,不存在就添加
        $conditions['WHERE']['order_num'] = $data['order_num'];
        $order = $this->getRow(array('id'), $conditions['WHERE']);

        if (!$order) {
            $this->add($data);
        }
        //查询mongo是否存在该记录,不存在就添加
        $db = Util::mongoConn();
        $rs = $db->order->find(array('_id' => $data['order_num']));
        if (!$rs) {
            $document = array(
                '_id' => $data['order_num'],                    //订单号
                'item_id' => new MongoInt32($item_id),         //物品号
                'obj_id' => new MongoInt64($data['role_id']),  //角色ID
                'money' => new MongoInt32($data['money']),     //充值人民币数量
                'first' => new MongoInt32($data['first']),     //是否首冲
                'status' => new MongoInt32(0)                  //支付状态，php提供订单，游戏充值成功后端则会自动改写成1
            );
            $db = Util::mongoConn();
            $rs = $db->order->insert($document, array('w' => 1));

            if ($rs['err'] == null) {
                $json = array('state' => true, 'code' => 1);
            } else {
                $json = array('state' => false, 'code' => 3);
            }
        } else {
            $json = array('state' => true, 'code' => 1);
        }
        echo json_encode($json);
    }


    //生成虚拟订单
    public function setVirtualOrder($order, $type = '0')
    {
        $now = time();
        $role = (new RoleModel())->getRole($order['role_id'], array('name', 'account'));
        $charge = $this->recharge(2);
        $json = array(
            'order_num' => $type . $now . $order['role_id'],
            'account' => $role['account'],
            'role_id' => $order['role_id'],
            'role_name' => $role['name'],
            'create_time' => $now,
            'first' => 0,
            'item_id' => $order['item_id'],
            'money' => $charge[$order['item_id']]['price'],
        );

        echo json_encode($json);
    }

    public function addVirtualOrder($data)
    {
        try {

            $document = array(
                '_id' => $data['order_num'],                    //订单号
                'item_id' => new MongoInt32($data['item_id']),         //物品号
                'obj_id' => new MongoInt64($data['role_id']),  //角色ID
                'money' => new MongoInt32($data['money']),     //充值人民币数量
                'first' => new MongoInt32($data['first']),     //是否首冲
                'status' => new MongoInt32(0)                  //支付状态，php提供订单，游戏充值成功后端则会自动改写成1
            );
            $db = Util::mongoConn();
            $rs = $db->order->insert($document, array('w' => 1));
            if ($rs['err'] == null) {
                $json = array('state' => true, 'code' => 1);
            } else {
                $json = array('state' => false, 'code' => 3, 'msg' => $rs['err']);
            }

        } catch (Exception $e) {
            $json = array('state' => false, 'code' => 4, 'msg' => $e->getMessage());
        }
        echo json_encode($json);
    }

    //根据账号和包获取指定日期充值人数
    public function getChargeNumByDatePackage($date, $package, array $accounts = array())
    {
        $fields = array('count(distinct o.account) as count_account');
        $conditions = array();
        (count($accounts) > 0) && $conditions['WHERE']['o.account::IN'] = $accounts;
        if (is_array($date)) {
            $conditions['WHERE']['o.create_time::BETWEEN'] = array(strtotime($date[0]), strtotime($date[1]));
        } else {
            $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = $date;
        }
        $conditions['WHERE']['r.package'] = $package;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['count_account'];
    }

    //根据账号和包获取指定日期充值总金额
    public function getChargeMoneyByDatePackage($date, $package, array $accounts = array())
    {
        $fields = array('sum(o.money) as sum_money');
        $conditions = array();
        (count($accounts) > 0) && $conditions['WHERE']['o.account::IN'] = $accounts;
        if (is_array($date)) {
            $conditions['WHERE']['o.create_time::BETWEEN'] = array(strtotime($date[0]), strtotime($date[1]));
        } else {
            $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = $date;
        }
        $conditions['WHERE']['r.package'] = $package;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['sum_money'];
    }

    //获取账号和包指定日期的充值人数和充值总金额
    public function getChargeDataByDatePackage($date, $package, array $accounts = array())
    {
        $data = [];
        $num = $this->getChargeNumByDatePackage($date, $package, $accounts);
        $money = $this->getChargeMoneyByDatePackage($date, $package, $accounts);

        $data['num'] = $num ? $num : 0;
        $data['money'] = $money ? $money : 0.00;

        echo json_encode($data);
    }

}