<?php
class ChargedailyModel extends Model{

    public function __construct(){
        parent::__construct('charge_daily');
        $this->alias = 'cd';
    }

    public function getDailyData($conditions){
        $fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'money' , 'pay_num' , 'pay_times' , 'first_money' , 'first_num' , 'first_times' , 'arpu' , 'role_num' , 'login_num');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getDailyByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $fields = array('date' , 'money' , 'pay_num' , 'pay_times' , 'first_money' , 'first_num' , 'first_times' , 'arpu' , 'role_num' , 'login_num');
        return $this->getRow($fields , $conditions['WHERE']);
    }

    public function addDaily($data){
        $this->add($data);
    }
}