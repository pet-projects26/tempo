<?php

class ChargenewModel extends Model{

    public function __construct(){
        parent::__construct('charge_new');
        $this->alias = 'cn';
    }

    public function getChargenewData($conditions){
        $fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'first_num' , 'new_num' , 'new_money' , 'new_percent');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getChargenewByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $fields = array('date' , 'first_num' , 'new_num' , 'new_money' , 'new_percent');
        return $this->getRow($fields , $conditions['WHERE']);
    }

    public function addChargenew($data){
        return $this->add($data);
    }
}