<?php
class ComModel extends Model{
    
    public function __construct() {
    	parent::__construct();
    }

    /**
     * [修改tableName]
     * @param [string] $tableName [描述]
     */
    public function setTable($tableName) {
    	$this->tableName = $tableName;
        $this->alias = $this->tableName;
    	return $this->tableName;
    }

    /**
     * [query description]
     * @param  [type] $sqlArr [description]
     * @return [type]         [description]
     */
    public function queryData($sqlArr) {
    	if (is_string($sqlArr)) {
    		return  $this->query($sqlArr);
    	}

    	$tmp = array();
    	
        foreach ($sqlArr as $key => $value) {

            if (preg_match('/drop|truncate/i', $value)) {
                $tmp[$key] = 'error params';
            } else {
                $tmp[$key] = $this->query($value);
            }
    	}

    	return $tmp;
    }
}