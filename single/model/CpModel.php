<?php

class CpModel extends Model{

    public function __construct(){
        parent::__construct('consume_produce');
        $this->alias = 'cp';
    }

    /*
     * 获取指定日期的消耗或者产出的记录
     * 如果 $goods_id 有赋值，则获取指定日期的指定物品的消耗或者产出的记录
     * */
    public function getGoodsByDate($date , $type , $goods_id = ''){  //type 0消耗 1产出
        $fields = array('sum(item_num) as sum_item');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time/1000,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['type'] = $type;
        $goods_id && $conditions['WHERE']['item_id'] = $goods_id;
        $rs = $this->getRow($fields , $conditions['WHERE']);
        return $rs['sum_item'] ? $rs['sum_item'] : 0; //道具的产出或消耗的数量
    }

    public function getGoodsAccByDate($date , $type , $goods_id = ''){
        $fields = array('distinct role_id');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time/1000,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['type'] = $type;
        $goods_id && $conditions['WHERE']['item_id'] = $goods_id;
        $rs = $this->getRows($fields , $conditions['WHERE']);//参与道具的产出或消耗的角色
        if($rs){
            $role_id = array();
            foreach($rs as $row){
                $role_id[] = $row['role_id'];
            }
            $account = (new RoleModel())->getAccByRole($role_id);
            return count($account);
        }
        else{
            return 0;
        }
    }
}