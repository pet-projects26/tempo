<?php

class DailytrialModel extends Model{

    public function __construct()
    {
        parent::__construct('daily_trial');
        $this->alias = 'z';
        $this->joinTable = array(
            'r' => array('name' => 'role' , 'type' => 'LEFT' , 'on' => 'z.role_id = r.role_id')
        );
    }

	//获取时间内的数据
    public function getDataByRangtime($type, $start_time, $end_time)
    {
        if (!array_key_exists($type, CDict::$trialTypes)) return false;

        $fields = array('count(distinct z.role_id) as count_role','sum(z.inspire_num) as inspire_count');
        $fields2 = array('count(z.role_id) as count_role');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["z.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["z.create_time/1000::<="] = $end_time;
        $conditions['WHERE']["z.type"] = $type;

        $join_num = $this->getRow($fields , $conditions['WHERE']);

        $conditions['WHERE']['z.inspire_num::>='] = 1;
        unset($fields[1]);
        $inspire_num = $this->getRow($fields, $conditions['WHERE']);
        unset($conditions['WHERE']['z.inspire_num::>=']);

        $conditions['WHERE']['z.result'] = 1; //成功
        $challenge_num = $this->getRow($fields, $conditions['WHERE']);
        $challenge_count = $this->getRow($fields2, $conditions['WHERE']);

        $data['join_num'] = $join_num['count_role'] ? $join_num['count_role'] : 0;  //参与人数
        $data['inspire_count'] = $join_num['inspire_count'] ? $join_num['inspire_count'] : 0; //总鼓舞次数
        $data['inspire_num'] = $inspire_num['count_role'] ? $inspire_num['count_role'] : 0; //鼓舞玩家数
        $data['challenge_num'] = $challenge_num['count_role'] ? $challenge_num['count_role'] : 0; //挑战成功人数
        $data['challenge_count'] = $challenge_count['count_role'] ? $challenge_count['count_role'] : 0;  //挑战成功次数

        //获取时间段内的购买人数和购买总次数
        $addNumData = (new SceneAddRecordModel())->getDataByRangtime($type, $start_time, $end_time, 2, 0);

        $data['add_num_role'] = $addNumData['add_num_role'];
        $data['add_num_sum'] = $addNumData['add_num_sum'];

        echo json_encode($data);
    }

    //获取角色的所有副本挑战记录
    public function getRoleTrialMaxLayerRecord($role_id)
    {
        $field = array('max(z.difficult) as max_layers', 'z.type');
        $data = [];
        $conditions = array();
        $conditions['WHERE']["z.role_id"] = $role_id;
        $conditions['WHERE']["z.result"] = 1;
        $conditions['Extends']['GROUP'] = 'z.type';

        $res = $this->getRows($field, $conditions['WHERE'], $conditions['Extends']);

        if ($res) {
            foreach($res as $key => $val) {

                if ($val['type'] == 7 || $val['type'] == 8) {
                    $valArr = bcdiv($val['max_layers'], 10, 1);
                    $valArr = explode('.', strval($valArr));
                    $rank = $valArr[0];
                    $star = $valArr[1];
                    $val['max_layers'] = CDict::$trialDiff[$rank] . '-' . $star . '星';
                }

                $data[$val['type']] = $val;
            }
        }

        return $data;
    }

    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
//    public function getActionOpenToAccount($type, $accounts = array(), $output = 1)
//    {
//        if (!array_key_exists($type, CDict::$trialTypes)) return false;
//
//        //获取副本对应的功能id
//        $reId = CDict::$trialTypes[$type]['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = (new ZonesModel())->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

