<?php

class DataModel extends Model{
	public $l;
	public $o;
    public function __construct(){
        parent::__construct('role');
        $this->alias = 'r';
		$this->lg= new LoginModel();
		$this->o= new OrderModel();
	}
	public function getRealData($conditions){
		$nowtime=date('H:i:s');
		
		$conditions['WHERE']['create_time::>=']=!empty($conditions['WHERE']['create_time::>='])?$conditions['WHERE']['create_time::>=']:strtotime(date('Y-m-d 00:00:00'));
		$conditions['WHERE']['create_time::<=']=!empty($conditions['WHERE']['create_time::<='])? 
		strtotime(date('Y-m-d',$conditions['WHERE']['create_time::<=']).' '.$nowtime):time();
		$fields=array('account','role_id',"create_time","FROM_UNIXTIME(create_time,'%Y-%m-%d') as date");
		$result=$this->getRows($fields,$conditions['WHERE']);
		if($result){
			foreach($result as $k=>$v){
				$date=$v['date'];
				$starttime=strtotime($v['date']);
				$endtime=strtotime($v['date'].' '.$nowtime);
				
				if($v['create_time']>=$starttime && $v['create_time']<=$endtime){
					$a[$date][]=$v['role_id'];
					$b[$date][]=$v['account'];
				}
				$b[$date]=array_unique($b[$date]);
				$arr[$date]['date']=$date;//日期
				$arr[$date]['account']=count($b[$date]);//新增注册数
				$arr[$date]['role_id']=count($a[$date]);//新增角色数
				
			}
			if($arr){
				foreach($arr as $key =>$val){
					$date=$key;	
					$starttime=strtotime($date);
					$endtime=strtotime($date.' '.$nowtime);
					$role=$a[$date];
					if($role){
						$count=$this->lg->getOlTimeCount($starttime,$endtime);
						$arr[$date]['active']=$count;//活跃人数
					}else{
						$arr[$date]['active']=0;	
					}
					//$online=$this->lg->getOnlineCount($starttime,$endtime);
					//$arr[$date]['online']=$online;//实时在线	
					
					$onlinetime=date('Y-m-d H:i',$endtime);
					$sql="select role_num from ny_online_hook where FROM_UNIXTIME(create_time,'%Y-%m-%d %H:%i')='$onlinetime'";
					$online=$this->query($sql);
					if(!empty($online)){
						$arr[$date]['online']=$online[0]['role_num'];//实时在线	
					}else{
						$arr[$date]['online']=0;//实时在线		
					}

					$newrolecount=$this->o->getOrderNewRoleCount($starttime,$endtime);//新增付费人数
					$newmoneycount=$this->o->getOrderNewMoneyCount($starttime,$endtime);//新增付费金额
					if($newmoneycount==''){$newmoneycount=0;}
					if($newrolecount !=0){
						$newap=sprintf("%.2f" , $newmoneycount/$newrolecount);//新增付费AP
					}else{
						$newap=0;	
					}
					
					$rolecount=$this->o->getOrderRoleCount($starttime,$endtime);//付费人数
					$moneycount=$this->o->getOrderMoneyCount($starttime,$endtime);//付费金额
					if($moneycount==''){$moneycount=0;}
					if($rolecount !=0){
						$ap=sprintf("%.2f" , $moneycount/$rolecount);//付费AP
					}else{
						$ap=0;	
					}
				
					$arr[$date]['newrolecount']=$newrolecount;
					$arr[$date]['newmoneycount']=$newmoneycount;
					$arr[$date]['newap']=$newap;
					$arr[$date]['rolecount']=$rolecount;
					$arr[$date]['moneycount']=$moneycount;
					$arr[$date]['ap']=$ap;
				}
			}
		}
		 echo json_encode(array($arr, count($arr)));
	}
}