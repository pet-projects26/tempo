<?php
class FactionModel extends Model{
    public function __construct(){
        parent::__construct('faction');
        $this->alias = 'f';
    }
	public function getFaction($conditions){
		$fields=array('faction_id','name','level','leader_role_id','create_time');
		$conditions['WHERE']['del_flag']=0;
		$result=$this->getRows($fields,$conditions['WHERE'],$conditions['Extends']);
		
		foreach($result as $k=>$v){
			$role=new RoleModel();
			$arr=$role->getRole($v['leader_role_id'],array('name'));
			$result[$k]['leader_role_id']=$arr['name'];
		}
		
		echo json_encode(array($result , $this->getCount()));
		
	}
}