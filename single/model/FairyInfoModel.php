<?php

class FairyInfoModel extends Model
{
    public function __construct()
    {
        parent::__construct('fairy_info');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        //获取护送人数 护送之和 拦截人数 拦截之和
        $fields = array('count(distinct t.role_id) as count_role', 'count(t.role_id) as count');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
        $conditions['WHERE']['t.type'] = 1; //护送

        $escort = $this->getRow($fields, $conditions['WHERE']); //护送人数

        $conditions['WHERE']['t.type'] = 2; //拦截

        $rob = $this->getRow($fields, $conditions['WHERE']); //拦截人数

        $data['escort_num'] = $escort['count_role'] ? $escort['count_role'] : 0;  //护送人数
        $data['escort_count'] = $escort['count'] ? $escort['count'] : 0; //护送之和
        $data['rob_num'] = $rob['count_role'] ? $rob['count_role'] : 0;  //拦截人数
        $data['rob_count'] = $rob['count'] ? $rob['count'] : 0; //拦截之和

        //进行过刷新仙女的人数 包括女帝   刷新仙女总次数
        unset($conditions['WHERE']['t.type']);

        $conditions['WHERE']['t.type::IN'] = [3, 4];

        $refresh = $this->getRow($fields, $conditions['WHERE']);

        $data['refresh_num'] = $refresh['count_role'] ? $refresh['count_role'] : 0;  //刷新人数
        $data['refresh_count'] = $refresh['count'] ? $refresh['count'] : 0; //刷新之和

        unset($conditions['WHERE']['t.type::IN']);
        //选择女帝人数
        $conditions['WHERE']['t.type'] = 4;

        $select = $this->getRow($fields, $conditions['WHERE']);

        $data['select_num'] = $select['count_role'] ? $select['count_role'] : 0;  //刷新人数
        $data['select_count'] = $select['count'] ? $select['count'] : 0; //刷新之和

        //参与护送中 > vip 5 的玩家数
        $conditions['WHERE']['t.type'] = 1;
        $conditions['WHERE']['t.vip_level::>'] = 5;

        $join_vip6 = $this->getRow($fields, $conditions['WHERE']);

        $data['join_vip6'] = $join_vip6['count_role'] ? $join_vip6['count_role'] : 0;

        echo json_encode($data);
    }
}
