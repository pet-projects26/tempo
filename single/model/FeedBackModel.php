<?php


class FeedBackModel extends Model
{
    public function __construct()
    {
        parent::__construct('feedback');
        $this->alias = 'f';
    }

    public function record_data($conditions)
    {

        $whereBetMoney = [];
        !empty($conditions['WHERE']['o.money::BETWEEN']) && $whereBetMoney = $conditions['WHERE']['o.money::BETWEEN'];
        unset($conditions['WHERE']['o.money::BETWEEN']);

        $fields = ['f.id as id', 'f.account as account', 'f.uuid as uuid', 'f.name as name', 'f.role_id as role_id', 'f.type as type', 'f.content as content', 'f.create_time as create_time', 'f.status as status', 'fr.create_time as reply_time', 'fr.reply_user as reply_user', 'r.package as package'];

        $this->joinTable = array(
            'fr' => array('name' => 'feedback_reply', 'type' => 'LEFT', 'on' => 'f.id = fr.fid'),
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'f.role_id = r.role_id')
        );

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $roles = [];

        if ($rs) {

            foreach ($rs as $v) {
                $roles[] = $v['role_id'];
            }
            unset($v);

            $roles = array_unique($roles);

            $Where = [];
            !empty($whereBetMoney) && $Where['money::BETWEEN'] = $whereBetMoney;
            $Where['role_id::IN'] = $roles;
            $Where['status'] = 1;
            $Extends = [];
            $Extends['GROUP'] = 'role_id';

            $fields2 = ['role_id', 'sum(money) as money'];

            $Order = new OrderModel();

            $orderRs = $Order->getRows($fields2, $Where, $Extends);

            $roleMoneys = [];

            if ($orderRs) {
                foreach ($orderRs as $v) {
                    $roleMoneys[$v['role_id']] = $v['money'];
                }
                unset($v);

                foreach ($rs as $k => &$row) {
                    $role_id = (int)$row['role_id'];
                    $row['money'] = $roleMoneys && array_key_exists($role_id, $roleMoneys) ? $roleMoneys[$role_id] : 0.00;

                    if (!empty($whereBetMoney) && !$row['money']) {
                        unset($rs[$k]);
                    }

                }
            }
        }

        echo json_encode($rs);

    }
}