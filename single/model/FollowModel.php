<?php

class FollowModel extends Model{
    public function __construct(){
        parent::__construct('follow');
        $this->alias = 'fl';
    }

    public function getFollowData($conditions){
        $fileds = array("from_unixtime(date,'%Y-%m-%d') as date" , 'pay_num' , 'money' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        $rs = $this->getRows($fileds , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function setFollow($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

    //获取指定日期的一条记录
    public function getFollowByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $fields = array('date' , 'pay_num' , 'money' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        return $this->getRow($fields , $conditions['WHERE']);
    }

    /**
     * @version 2017-10-31
     * @mark 新版本，分包统计数据，若干条记录
     * @mark 兼容合服psId
     * @param string $date
     * @param string $package
    */
    public function getFollowByDateWithPackage($date,$package){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['package'] = $package;
        $fields = array('date' , 'pay_num' , 'money' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        return $this->getRow($fields , $conditions['WHERE']);
    }
}