<?php

class FollownewModel extends Model{
    public function __construct(){
        parent::__construct('follow_new');
        $this->alias = 'fn';
    }

    public function getFollownewData($conditions){
        $fileds = array("from_unixtime(date,'%Y-%m-%d') as date" , 'new_num' , 'money' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        $rs = $this->getRows($fileds , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function setFollownew($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

    //获取指定日期的一条记录
    public function getFollownewByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $fields = array('date' , 'new_num' , 'money' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        return $this->getRow($fields , $conditions['WHERE']);
    }
}