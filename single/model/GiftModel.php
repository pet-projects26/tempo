<?php
class GiftModel extends Model{
    public function __construct(){
        parent::__construct('gift');
        $this->alias = 'g';
    }

    /**
     * [礼包添加]
     * @param [type] $document [description]
     */
    public function addGift($document){
        ini_set('mongo.native_long' , 0);
        $db = Util::mongoConn();

        $db->exchange->remove(array('activeId' => $document[0]['activeId']));

        $rs = $db->exchange->batchInsert($document);

        if (isset($rs['ok']) && $rs['ok'] == 1) {
            die('success');
        }

        die('fail');

    }

    /**
     * [礼包删除]
     * @param  [type] $document [description]
     * @return [type]           [description]
     */
    public function resetGift($document){
        ini_set('mongo.native_long', 0);
        $db = Util::mongoConn();
        $db->exchange->remove(array('activeId' => $document[0]['activeId']));
        $db->exchange->batchInsert($document);

        if (isset($rs['ok']) && $rs['ok'] == 1) {
            die('success');
        }

        die('fail');
    }

    /**
     * [删除礼包码]
     * @param  [type] $document [description]
     * @return [type]           [description]
     */
    public function delGift($document) {
    	ini_set('mongo.native_long', 0);
    	$db = Util::mongoConn();
    
    	$rs = $db->exchange->remove(array('activeId' => (int)$document['activeId']));
    	if ($rs['ok'] == 1) {
    		die('success');
    	}
    	die('fail');
    }
}