<?php

class GiftlogModel extends Model{
    public function __construct(){
		$this->gift = new GiftModel();
    }
    
    
	public function gift_data($conditions){
		$result = $this->gift->getRows($conditions['fields'],$conditions['WHERE'] , $conditions['Extends']);
		echo json_encode(array($result, $this->getCount()));
	}
	
}