<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/5/6
 * Time: 20:29
 */

class GuideModel extends Model
{
    public function __construct()
    {
        parent::__construct('guide');
        $this->alias = 'g';

        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'g.role_id = r.role_id')
        );
    }

    /***
     * @param $start_time
     * @param $end_time
     * @param array $accounts
     */
    public function getDataByRangtime($start_time, $end_time, array $accounts = [])
    {

        $fields = array('g.guide_id as guide_id');

        $conditions = array();
        $conditions['WHERE']['g.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['g.create_time::<='] = $end_time * 1000;
        $conditions['Extends']['GROUP'] = 'g.guide_id';

        $todayGuide = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = [];

        $one_guide_get_acc = $this->getGuideStatusAccount('1001', $start_time, $end_time, 1, $accounts);
        $one_guide_get_acc = !empty($one_guide_get_acc) ? count($one_guide_get_acc) : 0;

        if (!empty($todayGuide)) {

            foreach ($todayGuide as $key => $row) {
                $rs[$key]['guide_id'] = $row['guide_id'];
                $rs[$key]['one_guide_get_num'] = $one_guide_get_acc;
                $getAccounts = $this->getGuideStatusAccount($row['guide_id'], $start_time, $end_time, 1, $accounts);
                $getNum = count($getAccounts);

                if ($getNum > 0) {
                    $doneAccounts = $this->getGuideStatusAccount($row['guide_id'], $start_time, $end_time, 2, $getAccounts);
                }

                $rs[$key]['get_num'] = $getNum;
                $rs[$key]['done_num'] = isset($doneAccounts) ? count($doneAccounts) : 0;
            }
        }

        echo json_encode($rs);
    }

    /***
     * @param $start_time
     * @param $end_time
     * @param $status
     * @param array $accounts
     * @return array
     */
    public function getGuideStatusAccount($guide_id, $start_time, $end_time, $status, array $accounts = [])
    {

        $fields = array('distinct r.account as account');

        $conditions = array();
        $conditions['WHERE']['g.guide_id'] = $guide_id;
        $conditions['WHERE']['g.status'] = $status;
        $conditions['WHERE']['g.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['g.create_time::<='] = $end_time * 1000;
        !empty($accounts) && $conditions['WHERE']['r.account::IN'] = $accounts;

        $res = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                array_push($data, $value['account']);
            }
        }

        return $data;
    }
}