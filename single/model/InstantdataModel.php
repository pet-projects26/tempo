<?php

class InstantdataModel extends Model
{
    public function __construct()
    {
        parent::__construct('role');
        $this->alias = 'r';
    }

    public function getRealData($time)
    {

        $date = date('Y-m-d', $time);

        $starttime = strtotime($date);
        $endtime = $starttime + 86399;

        $starttime_sql = $starttime * 1000;
        $endtime_sql = $endtime * 1000;

        $sql = "select role_num from ny_online_hook where create_time >=  '$starttime_sql' and create_time <= '$endtime_sql' order by id desc limit 1";

        $online = $this->query($sql);

        $online_num = 0;
        if (!empty($online[0])) {
            $online_num = $online[0]['role_num'];
        }

        //获取当前创角人数
        $sql_role = "select count(*) as role_num from ny_role where create_time >= '$starttime_sql' and create_time <= '$endtime_sql'";

        $role = $this->query($sql_role);
        $role_num = 0;
        if (!empty($role[0])) {
            $role_num = $role[0]['role_num'];
        }

        $Order = new OrderModel();

        $rolecount = $Order->getOrderRoleCount($starttime, $endtime);//付费人数
        $moneycount = $Order->getOrderMoneyCount($starttime, $endtime);//付费金数

        $rs = array();
        $rs['role_num'] = $role_num;
        $rs['online_num'] = $online_num;
        $rs['pay_num'] = $rolecount ? $rolecount : 0;
        $rs['money'] = $moneycount ? $moneycount : 0;

        echo json_encode($rs);
    }

    public function getServerRealData($output = 0)
    {
        //获取当前创角人数
        $sql_role = "select count(*) as role_num from ny_role";

        $role = $this->query($sql_role);
        $role_num = 0;
        if (!empty($role[0])) {
            $role_num = $role[0]['role_num'];
        }
        $Order = new OrderModel();
        $rolecount = $Order->getOrderRoleCount();//付费人数

        $rs = array();
        $rs['role_num'] = $role_num;
        $rs['pay_num'] = $rolecount ? $rolecount : 0;

        if ($output == 0)  return $rs;

        echo json_encode($rs);
    }
}