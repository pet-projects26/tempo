<?php

class ItemCheckModel extends Model{

    public function __construct(){
        parent::__construct('');
    }
    
    //道具
    public function getItem($conditions)  {
		
		$sql = "SELECT log.role_id ,item_id,SUM(item_num) AS item_num,r.name AS role_name,GROUP_CONCAT(DISTINCT source) AS get_source
                  FROM ny_consume_produce  log LEFT JOIN ny_role r ON log.role_id = r.role_id
                  WHERE  log.create_time BETWEEN '{$conditions['start_time']}' AND '{$conditions['end_time']}' AND log.type = '{$conditions['type']}'  AND item_id IN ({$conditions['item_id']})  GROUP BY log.role_id,item_id ORDER BY item_id";
		$resitem = $this->query ( $sql );
		
		// 		file_put_contents('/tmp/role.txt', json_encode($res)."\n\r",FILE_APPEND);
		echo json_encode($resitem);
    }
    //元宝(只包括拍卖)
    public function getGold($conditions){
    	
    	$sql = "SELECT  DISTINCT log.role_id AS role_id,SUM(log.`gold`) AS gold,role.name AS role_name,GROUP_CONCAT(DISTINCT coin_source) AS get_source
    	FROM ny_payment AS log
    	LEFT JOIN ny_role AS role ON log.role_id = role.role_id
    	WHERE  log.create_time BETWEEN '{$conditions['start_time']}' AND '{$conditions['end_time']}' AND log.type = {$conditions['type']} AND log.`gold`  >0  AND log.coin_source  IN (5)
    	GROUP BY log.role_id";
    	$resgold = $this->query ( $sql );
    	echo json_encode($resgold);
    }
    //内部元宝(只包括充值卡)
    public function getNeibuGold($conditions){
    	
    	$sql = "SELECT  DISTINCT log.role_id AS role_id,SUM(log.`gold`) AS gold,role.name AS role_name,GROUP_CONCAT(DISTINCT coin_source) AS get_source
    	FROM ny_payment AS log
    	LEFT JOIN ny_role AS role ON log.role_id = role.role_id
    	WHERE  log.create_time BETWEEN '{$conditions['start_time']}' AND '{$conditions['end_time']}' AND log.type = {$conditions['type']} AND log.`gold`  >0  AND log.coin_source  IN (205)
    	GROUP BY log.role_id";
    	$resgold = $this->query ( $sql );
    	echo json_encode($resgold);
    }
    //绑元(不包括充值卡和充值赠送的绑元)
    public function getBindGold($conditions){
    	
    	$sql = "SELECT  DISTINCT log.role_id AS role_id, SUM(log.bind_gold) AS bind_gold,role.name AS role_name,GROUP_CONCAT(DISTINCT coin_source) AS get_source
    	FROM ny_payment AS log
    	LEFT JOIN ny_role AS role ON log.role_id = role.role_id
    	WHERE  log.create_time BETWEEN '{$conditions['start_time']}' AND '{$conditions['end_time']}' AND log.type = {$conditions['type']} AND log.bind_gold > 0 AND log.money_type = 3 AND log.coin_source NOT IN (205,16)
    	GROUP BY log.role_id";
    	$resgold = $this->query ( $sql );
    	echo json_encode($resgold);
    }
  
}