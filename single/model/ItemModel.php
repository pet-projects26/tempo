<?php

class ItemModel extends Model{

    public function __construct(){
        parent::__construct('item');
        $this->alias = 'i';
    }
    public function getItemData($conditions){
        $fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'get_num' , 'get_role_num'  , 'use_num' , 'use_role_num' , 'inventory');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		asort($rs);
		foreach($rs as $k=>$v){
			$total_get += $v['get_num'];
			$rs[$k]['total_get'] = $total_get;
			$total_use += $v['use_num'];
			$rs[$k]['total_use'] = $total_use;
			$rs[$k]['percent']= sprintf("%.2f",(1- $rs[$k]['total_use']/$rs[$k]['total_get'])*100).'%';
		}
		ksort($rs);
		/*foreach($rs as $k=>$v){
			$rs[$k]['total_get'] = $total_get;
			$rs[$k]['total_use'] = $total_use;
		}*/
		
        echo json_encode(array($rs , $this->getCount()));
    }

    public function setItem($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

    /*
     * 获取指定日期和指定货币类型的一条记录或记录条数
     * 如果$item_type有赋值，则获取指定日期和指定货币类型的一条记录
     * */
    public function getItemByDateType($date , $item_type = ''){
        $conditions = array();
        $fields = array('date');
        !empty($item_type) && $conditions['WHERE']['type'] = $item_type;
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        return $this->getRow($fields , $conditions['WHERE']);
    }
	
	public function pie_data($params){
		$type=$params['type'];
		$start_time=$params['start_time'];
		$end_time=$params['end_time'];
		$time = ' 1';
		if($start_time !=0){
			$time .= " and create_time >= $start_time ";
		}
		if($end_time !=0){
			$time .= " and create_time <= $end_time ";
		}
		$totalsql="SELECT  sum($type) as sum_coin FROM ny_payment as p WHERE $time   AND $type > '0'  AND type = '0'    LIMIT 1";
		$sql="SELECT  sum($type) as sum_coin,coin_source FROM ny_payment  WHERE   $time  AND $type > '0'  AND type = '0'   GROUP BY coin_source";
		$total= $this->query($totalsql);
		$total=$total[0]['sum_coin'];
		$source=$this->query($sql);
		$itemtype=CDict::$itemType;
		$arr=array();
		$sum=$total;
		if($source){
			foreach($source as $k=>$v){
				$coin_percent=sprintf('%.4f' , $v['sum_coin']/$total);
				if($coin_percent >= 0.005){
					$arr[$k]['coin_source']=$itemtype[$v['coin_source']];
					$arr[$k]['sum_coin']=$v['sum_coin'];
					$sum=$sum-$v['sum_coin'];
				}
			}
			$arr[100]['coin_source']='其他';
			$arr[100]['sum_coin']=$sum;
		}
		echo json_encode($arr);
	}
	
	public function pie_detail($params){
		$type=$params['type'];
		$start_time=$params['start_time'];
		$end_time=$params['end_time'];
		$time = ' 1';
		if($start_time !=0){
			$time .= " and create_time >= $start_time ";
		}
		if($end_time !=0){
			$time .= " and create_time <= $end_time ";
		}
		$totalsql="SELECT  sum($type) as sum_coin FROM ny_payment as p WHERE $time   AND $type > '0'  AND type = '0'    LIMIT 1";
		$sql="SELECT   sum($type) as sum_coin,coin_source FROM ny_payment  WHERE   $time  AND $type > '0'  AND type = '0'   GROUP BY coin_source";
		
		$numsql ="select coin_source, count(DISTINCT(role_id)) as count from ny_payment  WHERE   $time  AND $type > '0'  AND type = '0'   GROUP BY coin_source";
		$num = $this->query($numsql);
		
		$people =  array();
		
		foreach($num as $k=>$v){
			$people[$v['coin_source']] = $v['count'];	
		}
		
		$total= $this->query($totalsql);
		$total=$total[0]['sum_coin'];
		$source=$this->query($sql);
		$itemtype=CDict::$itemType;
		$arr=array();
		$sum=$total;
		if($source){
			foreach($source as $k=>$v){
				$coin_percent=sprintf('%.4f' , $v['sum_coin']/$total)* 100 .'%';
				$arr[$k]['coin_percent']=$coin_percent;
				$arr[$k]['coin_source']=$itemtype[$v['coin_source']];
				$arr[$k]['sum_coin']=$v['sum_coin'];
				$arr[$k]['num']=$people[$v['coin_source']];
			}
		}
		
		
		echo json_encode($arr);
		
			
	}
	
	
	
	
}