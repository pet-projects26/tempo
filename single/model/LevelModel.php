<?php

class LevelModel extends Model
{

    public function __construct()
    {
        parent::__construct('level');
        $this->alias = 'lv';
        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'lv.role_id = r.role_id')
        );
    }

    public function getLevelAccounts($level, $time, $accounts = array(), $output = 1)
    {
        // $start_time = $time * 1000;
        $end_time = $time  * 1000;

        $field = array('distinct r.account as account');
        $data = [];
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;

        $conditions['WHERE']['lv.level::>='] = $level;
        //$conditions['WHERE']['lv.create_time::>='] = $start_time;
        $conditions['WHERE']['lv.create_time::<='] = $end_time;

        $res = $this->getRows($field, $conditions['WHERE']);

        $data = [];

        foreach ($res as $value) {
            array_push($data, $value['account']);
        }

        if ($output == 0) return $data;

        echo json_encode($data);

    }

    public function getLevel($role_id = '')
    {
        $conditions = array();
        $fields = array('max(level) as level');
        $role_id && $conditions['WHERE']['role_id'] = $role_id;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['level'] ? $rs['level'] : 1;
    }

    public function getMaxLevelsByDates($start = '', $end = '')
    {
        $conditions = array();
        //$start && $conditions['WHERE']['create_time::>='] =  $start;
        $end && $conditions['WHERE']['create_time::<='] = $end;
        $fields = array('role_id', 'max(level) as level');
        $conditions['Extends']['GROUP'] = 'role_id';
        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        return $result;
    }

    public function getLevelLossData($date = '')
    {

        $date = $date ? $date : date('Y-m-d');
        $start = strtotime($date);
        $end = strtotime($date . '23:59:59');

        $rs = $this->getMaxLevelsByDates('', $end);

        $total_num = count($rs); //总人数
        $roles = array();
        foreach ($rs as $row) {
            $roles[$row['level']][] = $row['role_id'];
        }

        $start_three_ago = $start - 3 * 86400; //三天前
        $end_three_ago = $end - 3 * 86400; //三天前

        $rs_three_ago = $this->getMaxLevelsByDates('', $end_three_ago);

        $roles_three_ago = array();
        foreach ($rs_three_ago as $row) {
            $roles_three_ago[$row['level']][] = $row['role_id'];
        }

        $rsl = (new LoginModel())->getLastLogsByDates($start_three_ago, $end);

        $login_roles = array();
        foreach ($rsl as $row) {
            $login_roles[] = $row['role_id'];
        }
        $data = array();
        for ($l = 1; $l <= CDict::$maxLevel; $l++) {
            $data[$l] = array();
            $data[$l]['level'] = $l;
            $data[$l]['num'] = count($roles[$l]); //等级人数
            $data[$l]['num_percent'] = $total_num ? sprintf("%.2f", $data[$l]['num'] / $total_num * 100) . '%' : '0%'; //比例
            $intersect = array_intersect($roles[$l], $roles_three_ago[$l]); //与三天前的等级相同的角色集合
            $same_num = count($intersect);
            $intersect = array_diff($intersect, $login_roles); //三天没有登录的角色集合
            $data[$l]['loss_num'] = count($intersect);  //等级流失人数
            $data[$l]['loss_percent'] = $same_num ? sprintf("%.2f", $data[$l]['loss_num'] / $same_num * 100) . '%' : '0%'; ////等级流失率
        }
        echo json_encode(array($data, count($data)));
    }

    public function getDataByRangtime($end_time, array $accounts = [])
    {
        $conditions = array();
        $end_time && $conditions['WHERE']['lv.create_time::<='] = $end_time * 1000;
        !empty($accounts) && $conditions['WHERE']['r.account::IN'] = $accounts;
        $fields = array('count(distinct lv.role_id) as count', 'lv.level as level', 'r.account as account');
        $conditions['Extends']['GROUP'] = 'level';
        $conditions['Extends']['ORDER'] = ['level#asc'];
        $result = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $fields2 = array('distinct lv.role_id as role_id', 'max(lv.level) as level', 'r.account as account');
        $conditions['Extends']['GROUP'] = 'role_id';
        $conditions['Extends']['ORDER'] = ['level#asc'];
        $result2 = $this->getRows($fields2, $conditions['WHERE'], $conditions['Extends']);

        $data = [];

        if (!empty($result) && !empty($result2)) {

            $count = count($result2);

            $roles = [];

            foreach ($result2 as $v) {
                $roles[$v['level']][] = $v;
            }

            foreach ($result as $k => $v) {

                $level = $v['level'];

                $dat = [];
                $dat['level'] = $level;

                $levelCount = isset($roles[$level]) ? count($roles[$level]) : 0;

                $dat['level_count'] = $levelCount;

                $dat['reach_num'] = $v['count'];

                $dat['pass_num'] = $dat['reach_num'] - $levelCount;

                $data[] = $dat;
            }
        }

        echo json_encode($data);
    }

}