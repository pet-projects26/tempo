<?php

class LevelremainModel extends Model
{

    public function __construct()
    {
        parent::__construct('level_remain');
        $this->alias = 'lr';
    }

    public function record_data($conditions, $from = 'json')
    {

        $fields = array('level', 'level_count', 'role_count',
            'level_rate', 'stay_num', 'level_loss_num', 'level_loss_rate', 'create_time');

        $rs = $this->getRows('*', $conditions['WHERE'], $conditions['Extends']);

        if ($from != 'json') {
            return $rs;
        } else {
            echo json_encode($rs);
        }
    }

    public function getRowData($date)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(lr.create_time,'%Y-%m-%d')"] = $date;

        $fields = array('level', 'level_count', 'role_count',
            'level_rate', 'stay_num', 'level_loss_num', 'level_loss_rate', 'create_time');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }

    public function setMultiday($data, $date = '')
    {
        if (empty($date)) {
            $this->add($data);
        } else {
            $conditions = array();
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
            $this->update($data, $conditions['WHERE']);
        }
    }

}
