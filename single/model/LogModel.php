<?php

class LogModel extends Model{
    public function __construct(){
		$this->lv=new LevelModel();
		$this->cp=new CpModel();
		$this->p=new PaymentModel();
		$this->r=new RideModel();
		$this->t=new TaskModel();
		$this->lg=new LoginModel();
		$this->e=new EquipModel();
		$this->er=new EquipreplaceModel();
		$this->ce=new CeModel();
		$this->gem=new GemModel();
    }
	
	public function level_data($conditions){
		
		$fields=array('level',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$result=$this->lv->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		echo json_encode(array($result, $this->getCount()));
	}
	
	
	public function prop_data($conditions){

		$m = new Model();
		isset($conditions['WHERE']['create_time::>=']) && $where[] = " create_time >= '{$conditions['WHERE']['create_time::>=']}'";
		isset($conditions['WHERE']['create_time::<=']) && $where[] = " create_time < '{$conditions['WHERE']['create_time::<=']}'";
		isset($conditions['WHERE']['item_id::IN']) && $where[] = " item_id in ('".array_shift($conditions['WHERE']['item_id::IN'])."')";
		isset($conditions['WHERE']['role_id']) && $where[] = " role_id = '{$conditions['WHERE']['role_id']}'";
		isset($conditions['WHERE']['source']) && $where[] = " source = '{$conditions['WHERE']['source']}'";
		$where1 = join(" AND ", $where);
		$sql = "SELECT type,source,item_id,item_num,from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') AS create_time FROM
		ny_consume_produce WHERE $where1 ORDER BY create_time DESC";
		$result = [];
		$result = $m->query($sql);
		echo json_encode(array($result, count($result)));
		
		
	}
	public function money_data($conditions){
		
		$result=$this->p->getRows('*',$conditions['WHERE'] , $conditions['Extends']);
		
		echo json_encode(array($result, $this->getCount()));
	}
	
	public function ride_data($conditions){
		
		$fields=array('ride_class','ride_level',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$result=$this->r->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		echo json_encode(array($result, $this->getCount()));
	}
	
	public function task_data($conditions){
		$task=$this->t->getAllTask();
		
		$conditions['WHERE']['type::!=']  =4;
		
		
		if(!empty($conditions['WHERE']['task_name'])){
			$task_name = $conditions['WHERE']['task_name'];
			
			foreach($task as $k=>$v){
				strpos($v['name'] , $task_name)	!== false && $id[] = $k;
			}
			if(!empty($id)){
				$conditions['WHERE']['task_id::IN'] = $id;
			}else{
				echo json_encode(array(array(), 0));exit;
			}
		}
		unset($conditions['WHERE']['task_name']);
		
		$fields=array('task_id','type','status',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$result=$this->t->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		
		$arr=array();
		foreach($result as $k=>$v){
			$task_id=$v['task_id'];
			$arr[$k]['task_id']	=$task_id;
			if($v['type']== 1){
				$arr[$k]['type'] ='主线任务';	
			}elseif($v['type']==2){
				$arr[$k]['type'] ='支线任务';
			}elseif($v['type']==5){
				$arr[$k]['type'] ='仙盟任务';
			}else{
				$arr[$k]['type'] ='';	
			}
			$arr[$k]['name']= !empty($task["$task_id"]['name']) ? $task["$task_id"]['name'] : '';
			
			if($v['status']==3 || $v['status']==4){
				$arr[$k]['status']='已接受任务';
			}elseif($v['status']==5){
				$arr[$k]['status']='已完成任务';
			}else{
				$arr[$k]['status']='';	
			}
			$arr[$k]['create_time']=$v['create_time'];
		}
		echo json_encode(array($arr, $this->getCount()));
	}
	
	public function login_data($conditions){
		
		$fields=array('role_level','ip','start_time','end_time');
		$result=$this->lg->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		foreach($result as $k=>$v){
			$result[$k]['online']=$this->time_format($v['start_time'],$v['end_time']);
			$result[$k]['start_time']=date('Y-m-d H:i:s',$v['start_time']);
			$result[$k]['end_time']=date('Y-m-d H:i:s',$v['end_time']);
		}
		echo json_encode(array($result, $this->getCount()));
	}
	
	public function equip_data($conditions){
		
		$equip=CDict::$equie;
		$fields=array('part','level',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$result=$this->e->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);

		foreach($result as $k=>$v){
			$result[$k]['part']=$equip[$v['part']];
		}
		echo json_encode(array($result, $this->getCount()));
	}
	
	public function equip_replace_data($conditions){
		
		$fields=array('type','item_id',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$result=$this->er->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		echo json_encode(array($result, $this->getCount()));
	}
	/**
	 * [ce_data 战斗力流水]
	 * @param  [type] $conditions [description]
	 * @return [type]             [description]
	 */
	public function ce_data($conditions){
		$fields=array('ce',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time");
		$result=$this->ce->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		echo json_encode($result);
	}

	/**
	 * [gem_data 宝石流水]
	 * @param  [type] $conditions [description]
	 * @return [type]             [description]
	 */
	public function gem_data($conditions){
		$fields=array('item_id','part', 'type' ,"create_time");
		$result=$this->gem->getRows($fields,$conditions['WHERE'] , $conditions['Extends']);
		echo json_encode($result);
	}
	
}