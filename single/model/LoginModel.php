<?php

class LoginModel extends Model
{
    public function __construct()
    {
        parent::__construct('login');
        $this->alias = 'l';
        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'l.role_id = r.role_id')
        );
    }

    //获取当前在线
    public function getOnlineData($conditions)
    {
        $fields = array('role_id', 'role_name', 'start_time', 'ip');
        $conditions['WHERE']['end_time'] = 0;
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        foreach ($rs as $k => $row) {
            $row = array(
                $row['role_id'],
                $row['role_name'],
                date('Y-m-d H:i:s', $row['start_time']),
                $this->time_format($row['start_time'], time()),
                $row['ip']
            );
            $rs[$k] = $row;
        }
        echo json_encode(array($rs, count($rs)));
    }

    //获取指定日期和指定角色的登录记录
    public function getLogByDate($date, array $roles = array())
    {
        $fields = array('role_id', 'role_level', 'role_name', 'role_career', 'start_time', 'first', 'ip');
        $conditions = array();
        (count($roles) > 0) && $conditions['WHERE']['role_id::IN'] = $roles;
        $conditions['WHERE']["from_unixtime(start_time,'%Y-%m-%d')"] = $date;
        $conditions['Extends']['ORDER'] = array('id#desc');
        return $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
    }

    //获取所有玩家在指定时间段内的最后登录时间（算角色）
    public function getLastLogsByDates($start = '', $end = '')
    {
        $conditions = array();
        $start && $conditions['WHERE']['start_time::>='] = $start;
        $end && $conditions['WHERE']['start_time::<='] = $end;
        $conditions['Extends']['GROUP'] = 'role_id';
        $fields = array('role_id', 'max(start_time) as last_login_time');
        return $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
    }

    //登录人数（算账号）
    public function getLogNumByDate($date, array $accounts = array(), $old = 0, $output = 0)
    {
        $fields = array('count(distinct r.account) as count_account', 'l.id');
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        if ($output) {
            echo json_encode(array($rs['count_account']));
        } else {
            return $rs['count_account'];
        }
    }

    //登录账户（算账号）
    public function getLoginAccountByDate($date, array $accounts = array(), $old = 0, $output = 0)
    {
        $fields = array('distinct r.account as account');
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        if ($rs) {
            foreach ($rs as $value) {
                array_push($data, $value['account']);
            }
        }

        if ($output) {
            echo json_encode($data);
        } else {
            return $data;
        }
    }


    //登录次数（算账号）
    public function getLogTimesByDate($date, array $accounts = array(), $old = 0)
    {
        $fields = array('count(r.account) as count_account', 'l.id');
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['count_account'];
    }

    public function getLogTimesByDate_json($date, array $accounts = array(), $old = 0)
    {
        $fields = array('count(r.account) as count_account', 'l.id');
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        echo json_encode($rs['count_account']);
    }

    //获取玩家在线时长 old 等于 1，则算的是老玩家（算账号）
    public function getOlTimeByDate($date, $old = 0)
    {
        $fields = array('l.id', 'r.account', 'l.start_time', 'l.end_time', 'r.role_id');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRows($fields, $conditions['WHERE']);
        $rss = array();
        $time = strtotime($date) + 86399;
        foreach ($rs as $k => $row) {
            !isset($rss[$row['account']]) && $rss[$row['account']] = 0;
            //如果角色已登出则在线时长等于 end_time 减去 start_time ，否则等于当前时间减去 start_time
            $ol_time = $row['end_time'] ? (round(round($row['end_time'] / 1000, 2) - round($row['start_time'] / 1000, 2), 2)) : (round($time - round($row['start_time'] / 1000, 2), 2));

            $rss[$row['account']] = round($rss[$row['account']] + $ol_time, 2);
        }
        return $rss;
    }


    public function getOlTimeByDate_json($start_time, $end_time, $accounts = array())
    {
        $start_time *= 1000;
        $end_time *= 1000;

        $fields = array('l.id', 'r.account', 'l.start_time', 'l.end_time', 'r.role_id');
        $conditions = array();
        $start_time && $conditions['WHERE']['start_time::>='] = $start_time;
        $end_time && $conditions['WHERE']['start_time::<='] = $end_time;
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $rs = $this->getRows($fields, $conditions['WHERE']);
        $rss = array();
        $time = $end_time / 1000;
        foreach ($rs as $k => $row) {
            !isset($rss[$row['account']]) && $rss[$row['account']] = 0;
            //如果角色已登出则在线时长等于 end_time 减去 start_time ，否则等于当前时间减去 start_time
            $ol_time = $row['end_time'] ? (round(round($row['end_time'] / 1000, 2) - round($row['start_time'] / 1000, 2), 2)) : (round($time - round($row['start_time'] / 1000, 2), 2));

            $rss[$row['account']] = round($rss[$row['account']] + $ol_time, 2);

        }
        echo json_encode($rss);
    }

    /*
        public function getOlTimeByDate_json2($date, $accounts = array())
        {
            $fields = array('l.id', 'r.account', 'l.start_time', 'l.end_time', 'r.role_id');
            $conditions = array();
            $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
            $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
            $rs = $this->getRows($fields, $conditions['WHERE']);
            $rss = array();
            $time = strtotime($date) + 86399;
            foreach ($rs as $k => $row) {
                !isset($rss[$row['account']]) && $rss[$row['account']] = 0;
                //如果角色已登出则在线时长等于 end_time 减去 start_time ，否则等于当前时间减去 start_time
                $ol_time = $row['end_time'] ? (round(round($row['end_time'] / 1000, 2) - round($row['start_time'] / 1000, 2), 2)) : (round($time - round($row['start_time'] / 1000, 2), 2));

                $rss[$row['account']] = round($rss[$row['account']] + $ol_time, 2);

            }
            echo json_encode($rss);
        }

    */
    //登录人数（算角色）
    public function getRoleLogNumByDate($date, array $roles = array(), $old = 0)
    {
        $fields = array('count(distinct l.role_id) as count_role_id', 'r.id');
        $conditions = array();
        $roles && $conditions['WHERE']['l.role_id::IN'] = $roles;
        $conditions['WHERE']["from_unixtime(l.start_time,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['count_role_id'];
    }

    //登录次数（算角色）
    public function getRoleLogTimesByDate($date, array $role_ids = array(), $old = 0)
    {
        $fields = array('count(l.role_id) as count_role_id', 'r.id');
        $conditions = array();
        $role_ids && $conditions['WHERE']['l.role_id::IN'] = $role_ids;
        $conditions['WHERE']["from_unixtime(l.start_time,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['count_role_id'];
    }

    //获取玩家在线时长 old 等于 1，则算的是老玩家（算角色）
    public function getRoleOlTimeByDate($date, $old = 0)
    {
        $fields = array('distinct(l.role_id) as role_id', 'r.id', 'l.start_time', 'l.end_time');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(l.start_time/1000,'%Y-%m-%d')"] = $date;
        $old && $conditions['WHERE']["from_unixtime(r.create_time/1000,'%Y-%m-%d')::!="] = $date;
        $rs = $this->getRows($fields, $conditions['WHERE']);//, $conditions['Extends']
        $time = strtotime($date) + 86399;
        $rss = array();
        foreach ($rs as $k => $row) {
            !isset($rss[$row['role_id']]) && $rss[$row['role_id']] = 0;
            //如果角色已登出则在线时长等于 end_time 减去 start_time ，否则等于当前时间减去 start_time
            $rss[$row['account']] += $row['end_time'] ? (round(round($row['end_time'] / 1000, 2) - round($row['start_time'] / 1000, 2), 2)) : (round($time - round($row['start_time'] / 1000, 2), 2));
        }
        return $rss;
    }


    //获取玩家在线时长
    public function getRoleOlTime($role_id, $start_time = '', $end_time = '')
    {
        $fields = array('distinct(l.role_id) as role_id', 'l.start_time', 'l.end_time');
        $conditions = array();
        $start_time && $conditions['WHERE']['start_time::>='] = $start_time * 1000;
        $end_time && $conditions['WHERE']['start_time::<='] = $end_time * 1000;
        $conditions['WHERE']['role_id'] = $role_id;
        $rs = $this->getRows($fields, $conditions['WHERE']);

        $onlineTime = 0;

        $endTime = $end_time ? $end_time : time();

        if (!empty($rs)) {
            foreach ($rs as $k => $row) {
                $onlineTime += $row['end_time'] ? (round(round($row['end_time'] / 1000, 2) - round($row['start_time'] / 1000, 2), 2)) : (round($endTime - round($row['start_time'] / 1000, 2), 2));
            }
        }
        return $onlineTime;
    }

    //获取在线时长大于30分钟的玩家数（实时数据对比）
    public function getOlTimeCount($start, $end)
    {

        $sql = "SELECT SUM(end_time - start_time ) AS num , role_id   FROM ny_login 
                WHERE start_time BETWEEN {$start}  AND {$end} 
              AND end_time > start_time  GROUP BY role_id HAVING num >= 1800 ";

        $activeSql = "SELECT COUNT(DISTINCT tmp.role_id) AS `active_num` FROM ny_role AS role  
                      LEFT JOIN ({$sql}) AS tmp ON role.role_id = tmp.role_id ";

        $rs = $this->query($activeSql);
        return $rs[0]['active_num'];

    }

    //获取指定时间段的在线人数
    public function getOnlineCount($start, $end)
    {
        $fields = array('count(id) as ol_count');
        $conditions = array();
        $conditions['WHERE']['start_time::>='] = $start;
        $conditions['WHERE']['start_time::<='] = $end;
        $conditions['WHERE']['end_time'] = 0;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['ol_count'];
    }

    //获取当前在线人数
    public function getOnlineNum()
    {
        $fields = array('count(distinct l.role_id) as ol_count');
        $conditions = array();
        $conditions['WHERE']['end_time'] = 0;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['ol_count'];
    }


    /**
     * @version 2017-10-30
     * @mark 根据包 获取所有玩家在指定时间段内的最后登录时间（算角色）
     * @mark psId 兼容合服情况
     * @param string $start_time
     * @param string $end_time
     * @param array $conditions
     */
    public function getLastLogsByDatesWithPackage($start_time, $end_time, $conditions)
    {
        $fields = array('lg.role_id', 'max(lg.start_time) as last_login_time');
        $package = $conditions['package'];
        $psId = $conditions['psId'];
        $where = '1=1';
        if ($end_time) {
            if ($start_time) {
                $where .= " and lg.start_time >='" . $start_time . "' && lg.start_time <= '" . $end_time . "'";
            } else {
                $where .= "and lg.start_time <='" . $end_time . "'";
            }
        } else if ($start_time) {
            $where .= " and lg.start_time >='" . $start_time . "' && lg.start_time <= '" . time() . "'";
        } else {
            #code for noting
        }
        //分包统计
        if (!empty($package)) {
            $where .= " and r.package in('" . join('\',\'', $package) . "')";
        }
        //兼容合服
        if (!empty($psId)) {
            $where .= " and r.psId in('" . join('\',\'', $psId) . "')";
        }

        $sql = "select " . join(',', $fields) . " from ny_login as lg left join ny_role as r on lg.role_id = r.role_id where " . $where . " group by lg.role_id";

        $row = $this->query($sql);
        empty($row) && $row = array();
        return $row;
    }

    public function getTodayNotLogoutRole($start_time, $end_time, array $account = [])
    {
        $start_time *= 1000;
        $end_time *= 1000;

        $fields = 'l.role_id as role_id, r.account as account, r.package as package';

        $where = 'l.start_time >= ' . $start_time . ' and l.start_time <= ' . $end_time . ' and l.end_time = 0 and (r.last_logout_time > ' . $start_time . ' or r.last_logout_time = 0) and l.start_time = r.last_login_time and l.start_time > r.last_logout_time';

        if (!empty($account)) {
            $where .= " and r.account in('" . join('\',\'', $account) . "')";
        }

        $sql = 'select ' . $fields . ' from ny_login as l left join ny_role as r on l.role_id = r.role_id where ' . $where . ' group by role_id';

        $rs = $this->query($sql);

        empty($rs) && $rs = [];

        echo json_encode($rs);
    }


    public function getRoleLogHonor($start_time, $end_time)
    {
        $start_time = $start_time * 1000;
        $end_time = $end_time * 1000;

        $fields = array('distinct l.role_id as role_id');
        $conditions = array();
        $conditions['WHERE']["l.start_time::>="] = $start_time;
        $conditions['WHERE']["l.start_time::<"] = $end_time;
        $rs = $this->getRows($fields, $conditions['WHERE']);

        $json = [];
        if (!empty($rs)) {

            //获取天梯荣誉产出最晚值
            $conditions = [];
            $conditions['WHERE']['money_type'] = 23;
            $conditions['WHERE']['create_time::<'] = $start_time;
            $conditions['Extends']['ORDER'] = array('create_time#desc');
            $conditions['Extends']['LIMIT'] = 1;
            $fields = ['role_id', 'balance as honor'];

            $Payment = new PaymentModel();

            foreach ($rs as $row) {

                $conditions['WHERE']['role_id'] = $row['role_id'];

                $val = $Payment->getRow($fields, $conditions['WHERE'], $conditions['Extends']);

                if (empty($val)) {
                    $val = [];
                    $val['role_id'] = $row['role_id'];
                    $val['honor'] = 0;
                }

                $json[] = $val;
            }

        }
        return json_encode($json);
    }
}