<?php
class LoginDailyModel extends Model{
    public function __construct(){
        parent::__construct('login_daily');
        $this->alias = 'ld';
    }

    public function getLogindailyData($conditions){
        $fields = array(
            "from_unixtime(date,'%Y-%m-%d') as date" , 'lg_num' , 'lg_times' , 'avg_lg_times' ,
            'old_num' , 'avg_old_oltime' , 'atv_num' , 'loy_num'
        );
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getLogByDate($date){
        $fields = array('date' , 'lg_num' , 'lg_times' , 'avg_lg_times' , 'old_num' , 'avg_old_oltime' , 'atv_num' , 'loy_num');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $conditions['Extends']['ORDER'] = array('id#desc');
        return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
    }
}