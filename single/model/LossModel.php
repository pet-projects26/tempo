<?php

class LossModel extends Model{

    public function __construct(){
        parent::__construct('loss');
        $this->alias = 'l';
    }

    public function record_data($conditions){
        $fields = array(
            'date' , 'loss_num' , 'reg_anum' , 'reg_rnum' , 'five_min' , 'thirty_min' , 'one_hour' , 'three' ,
            'five' , 'seven' , 'ten' , 'fifteen' , 'thirty' , 'next' , 'standard' , 'threeloss'
        );
        $conditions['Extends']['ORDER'] = array('date#desc');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
        $this->setPageData($rs , $this->getCount());
    }
    public function record_export($conditions){
        unset($conditions['Extends']['LIMIT']);
        $fields = array(
            'date' , 'loss_num' , 'reg_anum' , 'reg_rnum' , 'five_min' , 'thirty_min' , 'one_hour' , 'three' ,
            'five' , 'seven' , 'ten' , 'fifteen' , 'thirty' , 'next' , 'standard' , 'threeloss'
        );
        $conditions['Extends']['ORDER'] = array('date#desc');
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $result = array();
        $result[] = array(
            '日期' , '流失人数' , '当天注册数' , '总注册数' , '5分钟' , '30分钟' , '1小时' ,
            '3天' , '5天' , '7天' , '10天' , '30天' , '次日流失率' , '标准流失率' , '三日流失率'
        );
        foreach($rs as $row){
            $result[] = $this->formatFields($row);
        }
        Util::exportExcel($result , '流失统计（' . date('Y年m月d日H时i分s秒') . '）');
    }
}