<?php
class LtvModel extends Model{

    public function __construct(){
        parent::__construct('ltv');
        $this->alias = 'ltv';
    }

    public function getRolecreateData($conditions){
        $conditions['WHERE']['type'] = 1;
        $fileds = array(
            "from_unixtime(date,'%Y-%m-%d') as date" , 'num',
            'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty' , 'sixty' , 'ninety'
        );

        $rs = $this->getRows($fileds , $conditions['WHERE'] , $conditions['Extends']);

        $res = json_encode(array($rs , $this->getCount()));
        die($res);
    }

    public function getRegisterData($conditions){
        $conditions['WHERE']['type'] = 2;
        $fileds = array(
            "from_unixtime(date,'%Y-%m-%d') as date" , 'num' ,
            'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty' , 'sixty' , 'ninety'
        );
        $rs = $this->getRows($fileds , $conditions['WHERE'] , $conditions['Extends']);

        $res = json_encode(array($rs , $this->getCount()));
        die($res);
    }

    /**
     * 插入数据
     */
    public function setRecord($data ){
        return $this->add($data);
    }

    //获取指定日期的一条记录
    public function getRecordByDate($date , $type){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $conditions['WHERE']['type'] = $type;
        $fields = array('date' , 'num' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty' , 'sixty' , 'ninety');

        $res = $this->getRow($fields , $conditions['WHERE']);
        return $res;
    }

    /**
     * 删除记录通过时间戳
     *
     */
    public function delRecordByDate($date) {
        $time = strtotime($date);

        $sql = "SELECT id FROM  ny_ltv WHERE `date` = '{$time}' ";

        $res = $this->query($sql);

        if (empty($res)) {
            return true;
        }

        $ids = array();

        foreach ($res as $k => $r) {
            $ids[] = $r['id'];
        }

        $str = implode(',', $ids);

        $sql = "DELETE FROM ny_ltv WHERE id in ({$str})";

        return $this->query($sql);
    }
    
}