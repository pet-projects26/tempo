<?php

class MallRecordModel extends Model
{

    public $itemConfig;

    public function __construct($type = 1)
    {
        parent::__construct('mall_record');
        $this->alias = 'mr';

        if ($type == 1) {
            //获取所有物品数据
            $this->itemConfig = $this->getItem(2);
        }
    }

    //获取商城商店分类
    public function getMallType($output = 1)
    {

        $mallItem = Fields::getJsonData('mall', ['mallType', 'mallName', 'childMallType', 'childMallName'], 'id');

        $mallType = [];

        foreach ($mallItem as $key => $val) {
            if (!isset($mallType[$val['mallType']])) {
                $mallType[$val['mallType']] = [
                    'mallType' => $val['mallType'],
                    'mallName' => $val['mallName'],
                    'child' => []
                ];
            }

            if (isset($mallType[$val['mallType']]) && !array_key_exists($val['childMallType'], $mallType[$val['mallType']])) {
                $mallType[$val['mallType']]['child'][$val['childMallType']] = [
                    'mallType' => $val['childMallType'],
                    'mallName' => $val['childMallName']
                ];
            }
        }

        if ($output != 1) return $mallType;

        echo json_encode($mallType);
    }


    public function getMallProduct($output = 1)
    {
        //获取所有商城售卖的itemID
        $mallItem = Fields::getJsonData('mall', ['itemId', 'realityPrice', 'mallType', 'mallName', 'childMallType', 'childMallName'], 'id');

        $itemConfig = $this->itemConfig;

        if ($mallItem) {
            foreach ($mallItem as $key => $val) {

                $mallItem[$key]['currency_id'] = intval($val['realityPrice'][0]);
                $mallItem[$key]['currency_name'] = $itemConfig[$val['realityPrice'][0]]['name'];
                $mallItem[$key]['product_id'] = $key;
                $mallItem[$key]['item_id'] = $val['itemId'];
                $itemName = array_key_exists($val['itemId'], $itemConfig) ? $itemConfig[$val['itemId']]['name'] : '未知物品';
                $mallItem[$key]['item_name'] = $key . '-' . $itemName;
                $mallItem[$key]['mall_type'] = $val['mallType'];
                $mallItem[$key]['mall_name'] = $val['mallName'];
                $mallItem[$key]['child_mall_type'] = $val['childMallType'];
                $mallItem[$key]['child_mall_name'] = $val['childMallName'];

                unset($mallItem[$key]['realityPrice']);
                unset($mallItem[$key]['itemId']);
                unset($mallItem[$key]['mallType']);
                unset($mallItem[$key]['childMallType']);
                unset($mallItem[$key]['mallName']);
                unset($mallItem[$key]['childMallName']);
            }
        } else {
            $mallItem = [];
        }

        if ($output == 1) return $mallItem;

        echo json_encode($mallItem);
    }

    public function getRecordData($date)
    {
        $conditions = array();
        if (is_array($date)) {
            $start_time = $date[0] * 1000;
            $end_time = $date[1] * 1000;
            $conditions['WHERE']['create_time::BETWEEN'] = array(strtotime($start_time), strtotime($end_time));
        } else {
            $conditions['WHERE']["from_unixtime(create_time/1000,'%Y-%m-%d')"] = $date;
        }

        $fields = array('sum(monetary) as mall_monetary', 'count(distinct role_id) as mall_consumer');

        //每个独立商城的总消费金额和总消费人数 (按分类分出来
        $mallSum = [];

        $mallType = $this->getMallType(2);

        foreach ($mallType as $type) {
            foreach ($type['child'] as $child) {
                $conditions['WHERE']['mall_type'] = $type['mallType'];
                $conditions['WHERE']['child_mall_type'] = $child['mallType'];
                $rs = $this->getRow($fields, $conditions['WHERE']);
                $mallSum[$type['mallType']][$child['mallType']]['mall_monetary'] = $rs['mall_monetary'] ? $rs['mall_monetary'] : 0.00;
                $mallSum[$type['mallType']][$child['mallType']]['mall_consumer'] = $rs['mall_consumer'] ? $rs['mall_consumer'] : 0;
            }
        }

        $fields = array('product_id', 'item_id', 'currency_id', 'mall_type', 'child_mall_type','sum(monetary) as monetary', 'sum(buy_num) as total_buy_num', 'count(distinct role_id) as consumer');

        unset($conditions['WHERE']['mall_type']);
        unset($conditions['WHERE']['child_mall_type']);

        $conditions['Extends'] = [];

        $conditions['Extends']['GROUP'] = 'product_id';

        $productSum = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $productData = $this->getMallProduct(1);

        $productSumFormat = [];

        if ($productSum) {
            foreach ($productSum as $key => $val) {
                $productSumFormat[$val['product_id']] = $val;
            }
            unset($key);
            unset($val);
        }

        //$itemConfig = $this->itemConfig;

        if ($productData && $productSumFormat) {
            foreach ($productData as $key => &$val) {

                if (array_key_exists($key, $productSumFormat)) {
                    $val['item_id'] = $productSumFormat[$key]['item_id'];
                    $val['currency_id'] = $productSumFormat[$key]['currency_id'] ? intval($productSumFormat[$key]['currency_id']) : $val['currency_id'];
                    $val['currency_name'] = $val['currency_id'] ? $productData[$key]['currency_name'] : '未知';
                    $val['mall_type'] = $productSumFormat[$key]['mall_type'];
                    $val['mall_name'] = $mallType[$val['mall_type']]['mallName'];
                    $val['child_mall_type'] = $productSumFormat[$key]['child_mall_type'];
                    $val['child_mall_name'] = $mallType[$val['mall_type']]['child'][$val['child_mall_type']]['mallName'];
                    $val['monetary'] = $productSumFormat[$key]['monetary'] ? $productSumFormat[$key]['monetary'] : 0;
                    // $val['total_buy_num'] = $productSumFormat[$key]['total_buy_num'];
                    $val['consumer'] = $productSumFormat[$key]['consumer'] ? $productSumFormat[$key]['consumer'] : 0;
                } else {
                    $val['monetary'] = 0;
                    //$val['total_buy_num'] = 0;
                    $val['consumer'] = 0;
                }

                $val['mall_monetary'] = $mallSum[$val['mall_type']][$val['child_mall_type']]['mall_monetary'] ? $mallSum[$val['mall_type']][$val['child_mall_type']]['mall_monetary'] : 0.00;
                $val['mall_consumer'] = $mallSum[$val['mall_type']][$val['child_mall_type']]['mall_consumer'] ? $mallSum[$val['mall_type']][$val['child_mall_type']]['mall_consumer'] : 0;
            }
        }

        echo json_encode($productData);
    }
}
