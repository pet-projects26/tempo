<?php

class ManyBossJoinModel extends Model{

    private $r;

    public function __construct(){
        parent::__construct('many_boss_join');
        $this->alias = 's';
    }

	//获取时间内的数据
    public function getJoinNumByRangtime($start_time, $end_time)
    {
        $fields = array('count(distinct role_id) as count_role');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["create_time/1000::>="] = $start_time;
        $conditions['WHERE']["create_time/1000::<="] = $end_time;
        $count_role = $this->getRow($fields , $conditions['WHERE']);

        $data['join_num'] = $count_role['count_role'] ? $count_role['count_role'] : 0;

        //获取时间段内的多人boss使用卷轴玩家数和使用的卷轴总数
        $addNumData = (new SceneAddRecordModel())->getDataByRangtime(CDict::$manyBoss['sceneType'], $start_time, $end_time, 1, 0);

        $data['add_num_role'] = $addNumData['add_num_role'];
        $data['add_num_sum'] = $addNumData['add_num_sum'];

        echo json_encode($data);
    }

    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
//    public function getActionOpenToAccount($accounts = array(), $output = 1)
//    {
//        //获取副本对应的功能id
//        $reId = CDict::$manyBoss['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = (new ZonesModel())->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

