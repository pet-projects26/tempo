<?php

class ManyBossRewardModel extends Model
{

    public function __construct()
    {
        parent::__construct('many_boss_reward');
        $this->alias = 's';
    }

    //获取时间内的数据 挑战人数 挑战总数
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = 'count(role_id) as count_role';

        $fields2 = 'count(distinct role_id) as count_role';

        $conditions = array();
        $conditions['WHERE']['create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['create_time::<='] = $end_time * 1000;

        $challenge_count = $this->getRow($fields, $conditions['WHERE']);
        $challenge_num = $this->getRow($fields2, $conditions['WHERE']);

        $data = [];
        $data['challenge_count'] = $challenge_count['count_role'] ? $challenge_count['count_role'] : 0;
        $data['challenge_num'] = $challenge_num['count_role'] ? $challenge_num['count_role'] : 0;

        echo json_encode($data);
    }
}

