<?php

class MultidayModel extends Model{

    public function __construct(){
        parent::__construct('multiday_retention');
        $this->alias = 'm';
    }

    public function record_data($conditions, $from = 'json'){

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", "new_acc_num", 'one_day', 'two_day', 'three_day', 'four_day', 'five_day', 'six_day', 'seven_day', 'eight_day', 'nine_day', 'ten_day' , 'fourteen_day', 'twenty_day', 'thirty_day', 'forty_five_day', 'sixty_day', 'ninety_day', 'one_hundred_twenty_day');

        $rs = $this->getRows($fields, $conditions['WHERE']);
        if ($from != 'json') {
            return $rs;
        } else {
            echo json_encode($rs);
        }
    }

    public function getRowData($time)
    {

        $conditions = [];
        $conditions['WHERE']['create_time'] = $time;

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", "new_acc_num", 'one_day', 'two_day', 'three_day', 'four_day', 'five_day', 'six_day', 'seven_day', 'eight_day', 'nine_day', 'ten_day' , 'fourteen_day', 'twenty_day', 'thirty_day', 'forty_five_day', 'sixty_day', 'ninety_day', 'one_hundred_twenty_day');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }

    public function setMultiday($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

}
