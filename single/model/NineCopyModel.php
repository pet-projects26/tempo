<?php

class NineCopyModel extends Model
{
    public function __construct()
    {
        parent::__construct('nine_copy');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = array('max(t.layer) as layer');

        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;

        $conditions['Extends']['GROUP'] = 't.role_id';

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $join_num = 0;

        $layerArr = [
            1 => 'one_layer_num',
            2 => 'two_layer_num',
            3 => 'three_layer_num',
            4 => 'four_layer_num',
            5 => 'five_layer_num',
            6 => 'six_layer_num',
            7 => 'seven_layer_num',
            8 => 'eight_layer_num'
        ];

        $data = [];

        foreach ($layerArr as $v) {
            $data[$v] = 0;
        }

        if (!empty($rs)) {
            $join_num = count($rs);
            foreach ($rs as $v) {
                if (array_key_exists($v['layer'], $layerArr)) {
                    $data[$layerArr[$v['layer']]] += 1;
                }
            }
        }

        $data['join_num'] = $join_num; //参与人数

        echo json_encode($data);
    }
}

