<?php

class OnlineModel extends Model{

    public function __construct(){
        parent::__construct('online_hook');
        $this->alias = 'oh';
    }

    public function getRealData($conditions, $from = 'json'){
        unset($conditions['Extends']['LIMIT']);

        $fields = array("from_unixtime(create_time/1000,'%H:%i') as time" , 'role_num');
        $conditions['Extends']['GROUP'] = 'time';
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        if ($from != 'json') {
            return $rs;
        }
        echo json_encode($rs);
    }

    public function setOnlineData($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

    public function getMultiData($conditions){
        $fields = array(
            "from_unixtime(create_time,'%Y-%m-%d') as date",
            'max(role_num) as max_num',
            'avg(role_num) as avg_num',
            'min(role_num) as min_num'
        );
        $conditions['Extends']['GROUP'] = 'date';
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
		
        $rss = array();
        foreach($rs as $row){
            $rss['x'][] = $row['date'];
            $rss['y'][0][] = $row['max_num'] ? $row['max_num'] : 0;
			$avg = $row['avg_num'] ? round($row['avg_num']) : 0;
            $rss['y'][1][] = $avg ? $avg : 0;
            $rss['y'][2][] = $row['min_num'] ? $row['min_num'] : 0;
        }
        echo json_encode($rss);
    }

    public function getMaxOnlineByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $fileds = array('max(role_num) as max_num');
        $rs = $this->getRow($fileds , $conditions['WHERE']);
        return $rs['max_num'];
    }

    public function getAvgOnlineByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $fields = array('avg(role_num) as avg_num');
        $rs = $this->getRow($fields , $conditions['WHERE']);
        return round($rs['avg_num']);
    }

}
