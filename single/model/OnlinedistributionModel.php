<?php
class OnlinedistributionModel extends Model{
    public function __construct(){
        parent::__construct('online_distribution');
        $this->alias = 'o';
    }
	public function distribution_data($conditions, $from = 'json'){

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", 'active_num','`(0,1]`' , '`(1,5]`' , '`(5,10]`', '`(10,20]`', '`(20,30]`', '`(30,60]`', '`(60,90]`', '`(90,120]`', '`(120,150]`', '`(150,180]`', '`(180,240]`', '`(240,300]`', '`(300,1440]`', '`(1440,-]`');

        $rs = $this->getRows($fields , $conditions['WHERE']);
        if ($from != 'json') {
            return $rs;
        } else {
            echo json_encode($rs);
        }
	}


    public function getRowData($time)
    {

        $conditions = [];
        $conditions['WHERE']["from_unixtime(o.create_time,'%Y-%m-%d')"] = $time;

        $fields = array("from_unixtime(create_time,'%Y-%m-%d') as create_time", 'active_num','`(0,1]`' , '`(1,5]`' , '`(5,10]`', '`(10,20]`', '`(20,30]`', '`(30,60]`', '`(60,90]`', '`(90,120]`', '`(120,150]`', '`(150,180]`', '`(180,240]`', '`(240,300]`', '`(300,1440]`', '`(1440,-]`');

        $rs = $this->getRow($fields, $conditions['WHERE']);

        return $rs;
    }


    public function setOnlineDistribution($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }
}