<?php

class OrderModel extends Model{
	public $r;
    public function __construct(){
        parent::__construct('order');
        $this->alias = 'o';
		$this->r= new RoleModel();
		$this->joinTable = array(
            'r' => array('name' => 'role' , 'type' => 'LEFT' , 'on' => 'o.role_id = r.role_id')
        );
			
	}
	//根据时间段获取新增的付费人数(按角色统计)
	public function getOrderNewRoleCount($starttime,$endtime){
		$conditions=array();
		$conditions['WHERE']['o.create_time::>=']=$starttime;
		$conditions['WHERE']['o.create_time::<=']=$endtime;
		$conditions['WHERE']['r.create_time::>=']=$starttime;
		$conditions['WHERE']['r.create_time::<=']=$endtime;
		//$conditions['WHERE']['o.status']=1;
		$fileds = array('count(DISTINCT o.role_id) as count','r.id');
		$result=$this->getRow($fileds,$conditions['WHERE']);
		
		return $result['count'];
	}
	//根据时间段获取付费人数(按角色统计)
	public function getOrderRoleCount($starttime = '',$endtime = ''){
		$conditions=array();
		$starttime && $conditions['WHERE']['o.create_time::>=']=$starttime;
		$endtime && $conditions['WHERE']['o.create_time::<=']=$endtime;
		//$conditions['WHERE']['o.status']=1;
		$fileds = array('count(DISTINCT o.role_id) as count');
		$result=$this->getRow($fileds,$conditions['WHERE']);
		return $result['count'];	
	}
	//根据时间段获取新增的付费金额(按角色统计)
	public function getOrderNewMoneyCount($starttime,$endtime){
		$conditions=array();
		$conditions['WHERE']['o.create_time::>=']=$starttime;
		$conditions['WHERE']['o.create_time::<=']=$endtime;
		$conditions['WHERE']['r.create_time::>=']=$starttime;
		$conditions['WHERE']['r.create_time::<=']=$endtime;
		//$conditions['WHERE']['o.status']=1;
		$fileds = array('sum(money) as money','r.id');
		$result=$this->getRow($fileds,$conditions['WHERE']);
		return $result['money'];
		
	}
	//根据时间段获取付费金额(按角色统计)
	public function getOrderMoneyCount($starttime = '',$endtime = ''){
		$conditions=array();
        $starttime && $conditions['WHERE']['o.create_time::>=']=$starttime;
		$endtime && $conditions['WHERE']['o.create_time::<=']=$endtime;
		$conditions['WHERE']['o.status']=1;
		$fileds = array('sum(money) as money');
		$result=$this->getRow($fileds,$conditions['WHERE']);
		return $result['money'];
	}
	public function getOrder($conditions){
		$conditions['WHERE']['create_time::>=']=$conditions['WHERE']['create_time::>=']?$conditions['WHERE']['create_time::>=']:0;
		$conditions['WHERE']['create_time::<=']=$conditions['WHERE']['create_time::<=']?$conditions['WHERE']['create_time::<=']:time();
		//$conditions['WHERE']['status']=1;
		$fileds=array('sum(money) as money');
		$result=$this->getRows($fileds,$conditions['WHERE'],$conditions['Extends']);
		$count=count($result);
		$countfileds=array('count(1) as count');
		unset($conditions['WHERE']);
		$countresult=$this->r->getRow($countfileds,$conditions['WHERE']);
		$totalcount=$countresult['count'];
		$arr=array();
		$arr[0]['scope']='1-50';
		$arr[1]['scope']='51-100';
		$arr[2]['scope']='101-300';
		$arr[3]['scope']='301-500';
		$arr[4]['scope']='501-1000';
		$arr[5]['scope']='1001-3000';
		$arr[6]['scope']='3001-5000';
		$arr[7]['scope']='5001-10000';
		$arr[8]['scope']='10001-15000';
		$arr[9]['scope']='15000-N';
		for($i=0;$i<=9;$i++){
			$arr[$i]['people']=0;
			$arr[$i]['money']=0;
			$arr[$i]['totalpercent']='0%';
			$arr[$i]['percent']='0%';
		}
		if($result){
			foreach($result as $k=>$v){
				$money=$v['money'];
				if($money>=1 && $money<=50){
					$arr[0]['people']++;
					$arr[0]['money']+=$money;
					//$arr[0]['avg']=sprintf("%.2f",$arr[0]['money']/$arr[0]['people']);
				}elseif($money>=51 && $money<=100){
					$arr[1]['people']++;
					$arr[1]['money']+=$money;
					//$arr[1]['avg']=sprintf("%.2f",$arr[1]['money']/$arr[1]['people']);
				}elseif($money>=101 && $money<=300){
					$arr[2]['people']++;
					$arr[2]['money']+=$money;
					//$arr[2]['avg']=sprintf("%.2f",$arr[2]['money']/$arr[2]['people']);
				}elseif($money>=301 && $money<=500){
					$arr[3]['people']++;
					$arr[3]['money']+=$money;
					//$arr[3]['avg']=sprintf("%.2f",$arr[3]['money']/$arr[3]['people']);
				}elseif($money>=501 && $money<=1000){
					$arr[4]['people']++;
					$arr[4]['money']+=$money;
					//$arr[4]['avg']=sprintf("%.2f",$arr[4]['money']/$arr[4]['people']);
				}elseif($money>=1001 && $money<=3000){
					$arr[5]['people']++;
					$arr[5]['money']+=$money;
					//$arr[5]['avg']=sprintf("%.2f",$arr[5]['money']/$arr[5]['people']);
				}elseif($money>=3001 && $money<=5000){
					$arr[6]['people']++;
					$arr[6]['money']+=$money;
					//$arr[6]['avg']=sprintf("%.2f",$arr[6]['money']/$arr[6]['people']);
				}elseif($money>=5001 && $money<=10000){
					$arr[7]['people']++;
					$arr[7]['money']+=$money;
					//$arr[7]['avg']=sprintf("%.2f",$arr[7]['money']/$arr[7]['people']);
				}elseif($money>=10001 && $money<=15000){
					$arr[8]['people']++;
					$arr[8]['money']+=$money;
					//$arr[8]['avg']=sprintf("%.2f",$arr[8]['money']/$arr[8]['people']);
				}elseif($money>=15000){
					$arr[9]['people']++;
					$arr[9]['money']+=$money;
					//$arr[9]['avg']=sprintf("%.2f",$arr[9]['money']/$arr[9]['people']);
				}
			}
			if($count !=0){
				for($i=0;$i<=9;$i++){
					$arr[$i]['totalpercent']=sprintf("%.2f",$arr[$i]['people']/$totalcount*100).'%';
					$arr[$i]['percent']=sprintf("%.2f",$arr[$i]['people']/$count*100).'%';
				}
			}
		}
		echo json_encode(array($arr,10));
	}

	//获取指定日期指定角色的充值人数（算角色）
	public function getOrderRoleNumByDate($date , $role_id = array()){
		$conditions = array();
		$conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] =  $date;
		$role_id && $conditions['WHERE']['role_id::IN'] =  $role_id;
		$fileds = array('count(distinct role_id) as count');
		$rs = $this->getRow($fileds , $conditions['WHERE']);
		return $rs['count'];
	}

	//获取指定日期指定角色的充值人数（算账号）
	public function getOrderNumByDate($date , $account = array()){
		$conditions = array();
		$conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] =  $date;
		$account && $conditions['WHERE']['account::IN'] =  $account;
		$fileds = array('count(distinct account) as count');
		$rs = $this->getRow($fileds , $conditions['WHERE']);
		return $rs['count'];
	}

	//获取指定日期指定角色的充值金额
	public function getOrderRoleMoneyByDate($date , $role_id = array()){
		$conditions = array();
		$conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] =  $date;
		$role_id && $conditions['WHERE']['role_id::IN'] =  $role_id;
		$fileds = array('sum(money) as sum_money');
		$rs = $this->getRow($fileds , $conditions['WHERE']);
		return $rs['sum_money'];
	}

	//获取指定日期指定角色的充值金额（算账号）
	public function getOrderMoneyByDate($date , $account = array()){
		$conditions = array();
		$conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] =  $date;
		$account && $conditions['WHERE']['account::IN'] =  $account;
		$fileds = array('sum(money) as sum_money');
		$rs = $this->getRow($fileds , $conditions['WHERE']);
		return $rs['sum_money'];
	}
}