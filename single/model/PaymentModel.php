<?php

class PaymentModel extends Model
{

    public function __construct()
    {
        parent::__construct('payment');
        $this->alias = 'p';
    }

    //获取指定日期的指定货币的产出或消耗的 数量 和 参与角色数
    public function getCoinByDate($date, $coin, $type)
    {  //type 0消耗 1产出
        $fields = array('sum(' . $coin . ') as sum_coin');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE'][$coin . '::>'] = 0;
        $conditions['WHERE']['type'] = $type;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        return $rs['sum_coin'] ? $rs['sum_coin'] : 0; //货币产出或消耗的数量
    }

    public function getCoinAccByDate($date, $coin, $type)
    {
        $fields = array('distinct role_id');
        $conditions = array();
        $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
        $conditions['WHERE'][$coin . '::>'] = 0;
        $conditions['WHERE']['type'] = $type;
        $rs = $this->getRows($fields, $conditions['WHERE']); //参与货币产出或消耗的角色
        if ($rs) {
            $role_id = array();
            foreach ($rs as $row) {
                $role_id[] = $row['role_id'];
            }
            $account = (new RoleModel())->getAccByRole($role_id);
            return count($account);
        } else {
            return 0;
        }
    }

    //获取指定时间段的货币的产出或消耗
    public function getCoinsByDates($start = '', $end = '')
    {
        $conditions = array();
        $start && $conditions['WHERE']['create_time::>='] = $start;
        $end && $conditions['WHERE']['create_time::<='] = $end;
        $fields = array('role_id', 'type', 'gold', 'bind_gold', 'copper', 'sorce');
        return $this->getRows($fields, $conditions['WHERE']);
    }

    //获取每日消费统计数据
    public function getDailyConsume($date)
    {
        $conditions = array();
        if (is_array($date)) {
            $start_time = $date[0] * 1000;
            $end_time = $date[1] * 1000;
            $conditions['WHERE']['create_time::BETWEEN'] = array(strtotime($start_time), strtotime($end_time));
        } else {
            $conditions['WHERE']["from_unixtime(create_time/1000,'%Y-%m-%d')"] = $date;
        }

        //日消费元宝数量
        //总元宝产出
        //充值元宝产出
        //免费元宝产出
        //消费人数
        //元宝总存量
        //type 0 消费 1产出 money_type 1 铜钱 2元宝 3绑元 4积分

        $conditions['WHERE']['type'] = 0;

        $conditions['WHERE']['money_type'] = 1;

        $fields = ['sum(count) as consume_gold', 'count(distinct role_id) as consume_num'];

        $consume = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['type'] = 1;

        $fields = ['sum(count) as count'];

        $produce_gold = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['coin_source::IN'] = ['44', '48'];

        $fields = ['sum(count) as count'];

        $charge_produce_gold = $this->getRow($fields, $conditions['WHERE']);

        $returnData = [];

        $returnData['consume_gold'] = $consume['consume_gold'] ? $consume['consume_gold'] : 0;
        $returnData['consume_num'] = $consume['consume_num'] ? $consume['consume_num'] : 0;
        $returnData['produce_gold'] = $produce_gold['count'] ? $produce_gold['count'] : 0;
        $returnData['charge_produce_gold'] = $charge_produce_gold['count'] ? $charge_produce_gold['count'] : 0;
        $returnData['free_produce_gold'] = $returnData['produce_gold'] - $returnData['charge_produce_gold'];
        $returnData['gold_total_stock'] = (new RoleModel())->getRoleGoldStock();

        echo json_encode($returnData);
    }

    public function getConsumerData($start_time, $end_time)
    {
        $moneyType = [
            1 => [42, 37, 92, 133, 132, 150, 156, 146, 151, 98, 34, 35, 169, 73, 57, 10, 50, 15, 145, 120, 121, 122, 165, 94, 170, 144, 162, 107, 174, 71],
            41 => [166, 37, 67]
        ];

        $start_time = $start_time * 1000;
        $end_time = $end_time * 1000;

        $data = [];
        $consumer_all_moneyData = [];

        foreach ($moneyType as $type => $source) {

            $conditions = array();
            $conditions['WHERE']['create_time::>='] = $start_time;
            $conditions['WHERE']['create_time::<='] = $end_time;
            $conditions['WHERE']['money_type'] = $type;
            $conditions['WHERE']['type'] = 0;
            //获取该货币类型的消费总数

            $rs = $this->getRow('sum(count) as consumer_all_money', $conditions['WHERE']);

            $consumer_all_moneyData[$type] = $rs['consumer_all_money'] ? $rs['consumer_all_money'] : 0;

            $conditions['WHERE']['coin_source::IN'] = $source;
            $conditions['Extends']['GROUP'] = 'coin_source';

            $dat = $this->getRows(['coin_source as source' ,'sum(count) as consumer_money', 'count(distinct role_id) as consumer_num', 'money_type'], $conditions['WHERE'], $conditions['Extends']);

            array_push($data, $dat);
        }

        $returnData = [
            'consumer_all_money' => $consumer_all_moneyData,
            'data' => $data
        ];

        echo json_encode($returnData);
    }
}