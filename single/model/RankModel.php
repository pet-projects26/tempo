<?php
class RankModel extends Model{
    public function __construct(){
        parent::__construct('rank');
        $this->alias = 'r';
    }
	public function getRankData($conditions){
		$fields=('name,rank,contents');
		$rs=$this->getRows($fields,$conditions['WHERE'],$conditions['Extends']);
		echo json_encode($rs);
	}
}