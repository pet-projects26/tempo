<?php
class RanklistModel extends Model{
    public function __construct(){
        parent::__construct('ranklist');
        $this->alias = 'rl';
    }
	public function record_data($conditions){

        $fields = array('rank', 'level', 'faction_id', 'power', 'recharge_money', 'over_ingots', 'offline_time',"from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');


        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);

        if (!empty($rs)) {
            foreach ($rs as $key => $row) {
                $role_name = (new RoleModel())->getRow('name', ['role_id' => $row['role_id']]);
                $rs[$key]['role_name'] = $role_name['name'] ? $role_name['name'] : $row['role_id'];
            }
        }

        echo json_encode($rs);

	}

	//查询某个时间端是否存在数据
	public function getDateRankListData($time)
    {
        $fields = array('count(id) as count_id');
        $conditions['WHERE']["from_unixtime(rl.create_time,'%Y-%m-%d')"] = $time ;
        $rs = $this->getRow($fields , $conditions['WHERE']);

        return $rs['count_id'];
    }
}