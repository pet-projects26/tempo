<?php
class RemainModel extends Model{
    public function __construct(){
        parent::__construct('remain');
        $this->alias = 'rm';
    }

    public function getRemainData($conditions){
        $fileds = array("from_unixtime(date,'%Y-%m-%d') as date" , 'pay_num' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        $rs = $this->getRows($fileds , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function setRemain($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }

    //获取指定日期的一条记录
    public function getRemainByDate($date){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        $fields = array('date' , 'pay_num' , 'one' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven' , 'fourteen' , 'thirty');
        return $this->getRow($fields , $conditions['WHERE']);
    }
}