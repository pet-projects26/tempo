<?php

class RichesCopyModel extends Model
{
    public function __construct()
    {
        parent::__construct('riches_copy');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = array('max(count) as count');

        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
        $conditions['WHERE']['t.count::>'] = 0; //采集数大于0的玩家
        $conditions['Extends']['GROUP'] = 't.role_id';
        //参与人数
        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $join_num = 0;
        $pick_count = 0;

        if (!empty($rs)) {
            foreach ($rs as $v) {
                $pick_count += $v['count'];
            }
            $join_num = count($rs);
        }

        $data['join_num'] = $join_num;  //参与人数
        $data['pick_count'] = $pick_count; //采集总数

        echo json_encode($data);

    }
}

