<?php

class RoleModel extends Model
{

    public function __construct()
    {
        parent::__construct('role');
        $this->alias = 'r';
    }

    //角色列表(优化20180320)
    public function getRoleListData($conditions)
    {

        $fields = array(
            'role_id', 'name', 'account', 'career', "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time",
            'role_level', 'gold', "from_unixtime(last_login_time,'%Y-%m-%d %H:%i:%s') as last_login_time", 'package', 'last_login_ip',
            "from_unixtime(last_logout_time,'%Y-%m-%d %H:%i:%s') as last_logout_time"
        );

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $arr = array();
        foreach ($rs as $k => $v) {
            $arr[$k]['role_id'] = $v['role_id'];
            $arr[$k]['name'] = $v['name'];
            $arr[$k]['account'] = $v['account'];
            $arr[$k]['package'] = $v['package'];
            $arr[$k]['career'] = $v['career'];
            $arr[$k]['level'] = $v['role_level'];
            $arr[$k]['gold'] = $v['gold'];
            $arr[$k]['create_time'] = $v['create_time'];
            $arr[$k]['start_time'] = $v['last_login_time'];
            $arr[$k]['last_logout_time'] = $v['last_logout_time'];
            $arr[$k]['last_login_ip'] = $v['last_login_ip'];

        }
        echo json_encode(array($arr, count($rs)));

    }

    //从mnogo库获取单个角色信息
    public function getRoleByMongo($id)
    {
        $db = Util::mongoConn();
        $query = '{ "_id" : NumberLong(' . $id . ') }';
        $rs = $db->character->findOne($query);
        echo json_encode($rs);
    }

    //获取指定日期创建的角色或者角色数量 ， $type 预设类型：eq获取某个时间角色 , between获取时间段角色 , count获取角色数量
    public function getRoleByDate($type, $date, $end_date = '', $order = array('id#desc'), $conditon = array())
    {
        $types = array('eq', 'between', 'bcount', 'ecount', 'pcount');
        if (in_array($type, $types)) {
            $conditions = array();
            $conditions['Extends']['ORDER'] = $order;
            switch ($type) {
                case 'eq':
                    $fields = array('account', 'role_id', 'name', 'career', 'create_time');
                    $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
                    return $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
                case 'between':
                    $fields = array('account', 'role_id', 'name', 'career', 'create_time', 'last_login_ip');
                    $conditions['WHERE']['create_time::>='] = $date;
                    $end_date && $conditions['WHERE']['create_time::<='] = $end_date;
                    return $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
                case 'bcount':
                    $fields = array('count(id) as count_id');
                    $date && $conditions['WHERE']['create_time::>='] = $date;
                    $end_date && $conditions['WHERE']['create_time::<='] = $end_date;
                    $rs = $this->getRow($fields, $conditions['WHERE']);
                    return $rs['count_id'];
                case 'ecount':
                    $fields = array('count(id) as count_id');
                    $date && $conditions['WHERE']["from_unixtime(create_time,'%Y-%m-%d')"] = $date;
                    $rs = $this->getRow($fields, $conditions['WHERE']);
                    return $rs['count_id'];
                case 'pcount':
                    $fields = array('count(id) as count_id');
                    $date && $conditions['WHERE']['create_time::>='] = $date;
                    $end_date && $conditions['WHERE']['create_time::<='] = $end_date;
                    !empty($conditon['package']) && $conditions['WHERE']['package::IN'] = array_filter($conditon['package']);
                    !empty($conditon['psId']) && $conditions['WHERE']['psId::IN'] = array_filter($conditon['psId']);
                    $rs = $this->getRow($fields, $conditions['WHERE']);//分包获取role_id 总数
                    return $rs['count_id'];
                default:
                    break;
            }
        } else {
            return false;
        }
    }

    //根据角色id获取账号
    public function getAccByRole($role_id)
    {
        $rss = array();
        if ($role_id) {
            $fields = array('distinct account');
            $conditions = array();
            $conditions['WHERE']['role_id::IN'] = array_unique($role_id);
            $rs = $this->getRows($fields, $conditions['WHERE']);
            $rss = array();
            foreach ($rs as $row) {
                $rss[] = $row['account'];
            }
        }
        return $rss;
    }

    public function getRole($role_id = '', $fields = array(), $start = '', $end = '')
    {

        empty($fields) && $fields = array('account', 'role_id', 'name', 'career', 'create_time');
        $conditions = array();
        if (is_array($role_id)) {
            $conditions['WHERE'] = array();
            $role_id && $conditions['WHERE']['role_id::IN'] = $role_id;
            $start && $conditions['WHERE']['create_time::>='] = $start;
            $end && $conditions['WHERE']['create_time::<='] = $end;
            $conditions['Extends']['ORDER'] = array('id#asc');
            return $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        } else {
            $role_id && $conditions['WHERE']['role_id'] = $role_id;
            return $this->getRow($fields, $conditions['WHERE']);
        }
    }

    public function getRoleJson($role_id = '', $fields = array(), $start = '', $end = '')
    {
        $role = $this->getRole($role_id, $fields, $start, $end);

        exit(json_encode($role));
    }

    public function baseinfo($id)
    {
        ini_set('mongo.long_as_object', 1);
        $create_time = $this->getRow('create_time', array('role_id' => $id));
        $id = new MongoInt64($id);
        $db = Util::mongoConn();
        $collection = $db->character;
        $info = $collection->findOne(array('_id' => $id));//查找角色信息
        $arr = array();
        $arr['id'] = $info['_id']->value;//id
        $arr['account'] = $info['account'];//帐号
        $arr['name'] = $info['name'];//名字
        $arr['level'] = $info['level'];//等级
        //$arr['create_tm']=$info['create_tm'];//创号时间
        $arr['occ'] = $info['occ'];//职业
        $arr['ce'] = $info['ce'];//战力

        if (is_object($info['exp'])) {
            $arr['exp'] = $info['exp']->value;//经验
        } else {
            $arr['exp'] = $info['exp'];//经验
        }

        $vip = $db->vip->findOne(array('_id' => $id), array('diamond' => true, 'vip' => true));
        $arr['diamond'] = $vip['diamond'];//充值元宝
        $arr['vip'] = $vip['vip'];//vip等级
        $bag = $db->bag->findOne(array('_id' => $id), array('moneyList' => true));
        $moneyList = $bag['moneyList'];

        if (is_object($moneyList[0])) {
            $moneyList[0] = $moneyList[0]->value;
        } else {
            $moneyList[0] = $moneyList[0];
        }

        $tongqian = $moneyList[0];
        $yuanbao = $moneyList[1];
        $bangyuan = $moneyList[2];
        $jifen = $moneyList[3];
        $arr['tongqian'] = $tongqian;//铜钱
        $arr['yuanbao'] = $yuanbao;//元宝
        $arr['bangyuan'] = $bangyuan;//绑元
        $arr['jifen'] = $jifen;//积分
        $effect = $db->effect->findOne(array('_id' => $id), array('redName' => true));
        $arr['redName'] = $effect['redName'];//杀戮值
        $arr['create_tm'] = $create_time['create_time'];//创号时间

        $arr['hp'] = $info['hp'];//血量
        $arr['attackList'] = end($info['attackList']);//攻击
        $arr['totalDefense'] = $info['totalDefense'];//防御
        if (isset($info['rename'])) {
            $arr['rename'] = $info['rename'];//重命名
        }

        //帐号权限
        echo json_encode($arr);
    }

    public function bag($id)
    {
        ini_set('mongo.long_as_object', 1);
        $id = new MongoInt64($id);
        $db = Util::mongoConn();
        $collection = $db->bag;
        $baglist = $collection->findOne(array('_id' => $id), array('bagList' => true));//查找角色信息
        $bag = $baglist['bagList'][1]['itemList'];
        $arr = array();
        foreach ($bag as $k => $v) {
            $arr[$k]['item_id'] = $v['itemId']->value;
            $arr[$k]['count'] = $v['count'];
        }
        echo json_encode($arr);
    }

    //角色列表
    public function getRoleList($conditions)
    {

        $fields = array(
            'role_id', 'name', 'account', 'career', 'role_level', 'gold', 'create_time', 'last_login_time'
        );

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $arr = array();
        foreach ($rs as $k => $v) {
            $arr[$k]['role_id'] = $v['role_id'];
            $arr[$k]['name'] = $v['name'];
            $arr[$k]['account'] = $v['account'];
            $arr[$k]['career'] = $v['career'];
            $arr[$k]['level'] = $v['role_level'];
            $arr[$k]['gold'] = $v['gold'] ? $v['gold'] : 0;
            $arr[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            $arr[$k]['start_time'] = date('Y-m-d H:i:s', $v['last_login_time']);
        }
        echo json_encode(array($arr, $this->getCount()));

    }

    /********************************* NEW METHOD ************************/
    /**
     * 查找新增玩家人数
     * @param string @date 日期
     * @return int num 创角人数
     */
    public function getCreateRoleNum($date = '')
    {
        if (empty($date)) {
            $date = date('y-m-d', strtotime('-1 day'));
        }

        $start_time = strtotime($date);
        $end_time = $start_time + 86399;

        $sql = "SELECT COUNT(`id`) AS num
                    FROM  ny_role 
                WHERE create_time  BETWEEN  {$start_time} AND {$end_time} ";

        $res = $this->query($sql);

        if (empty($res)) {
            return 0;
        }

        return $res[0]['num'];
    }


    /**
     * 查找新增玩家数
     *
     */
    public function getCreateAccountNum($date = '')
    {
        if (empty($date)) {
            $date = date('y-m-d', strtotime('-1 day'));
        }

        $start_time = strtotime($date);
        $end_time = $start_time + 86399;

        $sql = "SELECT COUNT(1) AS num 
                    FROM  (
                        SELECT MIN(create_time) AS `time`, role_id FROM ny_role GROUP BY  account
                    ) AS tmp
                WHERE  `time`  BETWEEN  {$start_time} AND {$end_time} ";

        $res = $this->query($sql);

        if (empty($res)) {
            return 0;
        }

        return $res[0]['num'];

    }

    //获取老玩家
    public function getOldAccounts_json($time, $accounts = array())
    {
        $fields = array('distinct r.account as account');
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['WHERE']["r.create_time/1000::<"] = $time;
        $rs = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        foreach ($rs as $value) {
            array_push($data, $value['account']);
        }

        echo json_encode($data);
    }

    //获取查询条件的玩家数
    public function getRoleNum($where, $accounts = array())
    {
        $fields = array('count(distinct account) as account');
        $conditions = array();
        $conditions['WHERE'] = $where;
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $rs = $this->getRow($fields, $conditions['WHERE']);
        $count = $rs['account'] ? $rs['account'] : 0;

        echo json_encode($count);
    }

    //过滤查询条件的玩家
    public function getRoleAccounts($where, $accounts = array(), $output = 1)
    {
        $fields = array('distinct account as account');
        $conditions = array();
        $conditions['WHERE'] = $where;
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $rs = $this->getRows($fields, $conditions['WHERE']);
        $data = [];

        foreach ($rs as $value) {
            array_push($data, $value['account']);
        }

        if ($output == 0) return $data;

        echo json_encode($data);
    }

    //根据类型获取角色id
    public function getRoleIdToType($type, $value)
    {
        $fields = array('role_id');
        $conditions = array();
        $conditions['WHERE'][$type] = $value;
        $conditions['Extends']['ORDER'][] = 'create_time#desc';
        $rs = $this->getRow($fields, $conditions['WHERE'], $conditions['Extends']);
        $role_id = $rs['role_id'] ? $rs['role_id'] : 0;

        echo json_encode($role_id);
    }

    //根据类型获取角色字段
    public function getRoleInfoToType($type, $value, $fields = array())
    {
        !$fields && $fields = array('role_id');
        $conditions = array();
        $conditions['WHERE'][$type] = $value;
        $conditions['Extends']['ORDER'][] = 'create_time#desc';
        $rs = $this->getRow($fields, $conditions['WHERE'], $conditions['Extends']);

        echo json_encode($rs);
    }

    //根据字段获取角色字段
    public function getRoleFields($role_id, $field)
    {
        $fields = array($field);

        $conditions = array();
        $conditions['WHERE']['r.role_id'] = $role_id;

        $rs = $this->getRow($fields, $conditions['WHERE']);

        $fieldVal = $rs[$field] ? $rs[$field] : '';

        return $fieldVal;

    }

    //获取最新的创建的角色id
    public function getLatestRoleId()
    {
        $conditions = array();
        $conditions['WHERE'] = [];

        $conditions['Extends']['ORDER'] = ['create_time#desc'];

        $rs = $this->getRow('role_id', $conditions['WHERE'], $conditions['Extends']);

        $role_id = $rs['role_id'] ? $rs['role_id'] : 0;

        return $role_id;

    }

    //获取各个vip等级的人数
    public function getVipCount()
    {
        $fields = ['vip_level', 'count(distinct account) as count'];

        $conditions = array();
        $conditions['WHERE']['vip_level::>'] = 0;
        $conditions['Extends']['GROUP'] = 'vip_level';

        $rs = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        echo json_encode($rs);
    }

    //获取vip人数
    public function getVipAccount($vipLevel = 0, array $accounts = [], $output = 1)
    {
        $fields = ['distinct account as account'];

        $conditions = array();
        $conditions['WHERE']['vip_level::>'] = $vipLevel;
        !empty($accounts) && $conditions['WHERE']['account::IN'] = $accounts;

        $rs = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        foreach ($rs as $value) {
            array_push($data, $value['account']);
        }

        if ($output == 0) return $data;

        echo json_encode($data);
    }

    //获取元宝总存量 查询mysql
    public function getRoleGoldStock()
    {
        $fields = ['sum(gold) as gold_stock'];

        $rs = $this->getRow($fields);

        $gold_stock = $rs['gold_stock'] ? $rs['gold_stock'] : 0;

        return $gold_stock;
    }

    //等级留存数据
    public function getLevelRemainDataByRangtime($start_time, $end_time)
    {
        $return = [];

        $start_time = $start_time * 1000;
        $end_time = $end_time * 1000;

        $method = $this->getRow(['count(role_id) as role_count']);

        $role_count = $method['role_count'] ? $method['role_count'] : 0;

        if ($role_count == 0) die(json_encode($return));

        $fidldsInRole = ['count(role_id) as level_count', 'role_level as level'];
        $Extends['GROUP'] = 'role_level';
        $rs = $this->getRows($fidldsInRole, [], $Extends);

        foreach ($rs as $k => $v) {
            $rs[$k]['role_count'] = $role_count;

            $rs[$k]['level_rate'] = sprintf("%.2f", $v['level_count'] / $method['role_count'] * 100) . '%';

            $sql = "SELECT count(role_id) as level_loss_num FROM ny_role  WHERE  (last_login_time <= '$start_time'  or last_login_time >= '$end_time')  AND role_level = '" . $v['level'] . "' LIMIT 1";
            //查出当前等级流失数
            $level_loss_num = $this->query($sql);

            $rs[$k]['level_loss_num'] = $level_loss_num[0]['level_loss_num'];

            $rs[$k]['level_loss_rate'] = $level_loss_num[0]['level_loss_num'] ? sprintf("%.2f", $level_loss_num[0]['level_loss_num'] / $v['level_count'] * 100) . '%' : '0.00%';
        }

        echo json_encode($rs);
    }

    //排行榜数据
    public function getRankListDataByRangtime($start_time, $end_time)
    {
        $return = [];

        $OrderModel = new OrderModel();

        //等级排行 查询role
        $sql = "select role_level as `level`, power, gold as over_ingots, last_logout_time, last_login_time, faction_id, role_id, `name` as role_name from ny_role order by role_level desc limit 100";

        $levelRes = $this->query($sql);

        if (!empty($levelRes)) {
            $rank = 1;
            foreach ($levelRes as $key => $row) {

                //根据role_id获取充值金额
                $sql_money = 'select sum(money) as recharge_money from ny_order where role_id = ' . $row['role_id'] . ' and status = 1 and create_time <= ' . $end_time;
                $money = $OrderModel->query($sql_money);
                $levelRes[$key]['recharge_money'] = $money[0]['recharge_money'] ? $money[0]['recharge_money'] : 0;
                $levelRes[$key]['rank'] = $rank;
                $levelRes[$key]['type'] = 0; //等级排行
                //计算离线时长 单位:天
                $levelRes[$key]['offline_time'] = $row['last_logout_time'] ? round($end_time - round($row['last_logout_time'] / 1000, 2), 2) : 0;

                if ($row['last_logout_time']) {
                    $levelRes[$key]['offline_time'] = round($end_time - round($row['last_logout_time'] / 1000, 2), 2);
                } else {
                    $levelRes[$key]['offline_time'] = 0;
                }

                unset($levelRes[$key]['last_logout_time']);
                unset($levelRes[$key]['last_login_time']);
                $rank++;
            }
            $return[] = $levelRes;
        }

        //战斗力排行 role
        $sql = "select role_level as `level`, power, gold as over_ingots, last_logout_time, last_login_time, faction_id, role_id, `name` as role_name from ny_role order by `power` desc limit 100";

        $powerRes = $this->query($sql);

        if (!empty($powerRes)) {
            $rank = 1;
            foreach ($powerRes as $key => $row) {
                //根据role_id获取充值金额
                $sql_money = 'select sum(money) as recharge_money from ny_order where role_id = ' . $row['role_id'] . ' and status = 1 and create_time <= ' . $end_time;
                $money = $OrderModel->query($sql_money);
                $powerRes[$key]['recharge_money'] = $money[0]['recharge_money'] ? $money[0]['recharge_money'] : 0;
                $powerRes[$key]['rank'] = $rank;
                $powerRes[$key]['type'] = 1; //战斗力排行
                //计算离线时长 单位:天
                if ($row['last_logout_time']) {
                    $powerRes[$key]['offline_time'] = round($end_time - round($row['last_logout_time'] / 1000, 2), 2);
                } else {
                    $powerRes[$key]['offline_time'] = 0;
                }

                unset($powerRes[$key]['last_logout_time']);
                unset($powerRes[$key]['last_login_time']);
                $rank++;
            }
            $return[] = $powerRes;
        }

        //充值排行 order
        $sql = 'select r.`role_level` as `level`, r.power as power, r.gold as over_ingots, r.last_logout_time as last_logout_time, r.last_login_time as last_login_time,r.faction_id as faction_id, r.role_id as role_id,  r.`name` as role_name, sum(o.money) as recharge_money from ny_role as r left join ny_order as o on r.role_id = o.role_id where o.status = 1 and o.create_time <= "' . $end_time . '" group by o.role_id order by sum(o.money) desc limit 100';

        $rechargeRes = $this->query($sql);
        if (!empty($rechargeRes)) {
            $rank = 1;
            foreach ($rechargeRes as $key => $row) {
                $rechargeRes[$key]['rank'] = $rank;
                $rechargeRes[$key]['type'] = 2; //充值排行
                //计算离线时长 单位:天
                if ($row['last_logout_time']) {
                    $rechargeRes[$key]['offline_time'] = round($end_time - round($row['last_logout_time'] / 1000, 2), 2);
                } else {
                    $rechargeRes[$key]['offline_time'] = 0;
                }

                unset($rechargeRes[$key]['last_logout_time']);
                unset($rechargeRes[$key]['last_login_time']);
                $rank++;
            }
            $return[] = $rechargeRes;
        }

        echo json_encode($return);
    }

    public function nodeTmp($start_time, $end_time, array $accounts)
    {
        //查询角色表是否 最后登录时间 大于 上一天开始时间

        $last_day_start_time = $start_time - 86400;
        $last_day_start_time *= 1000;

        $conditions = array();
        $conditions['WHERE']['r.last_logout_time::>='] = $last_day_start_time;
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['Extends']['GROUP'] = 'account';
        $res = $this->getRows('r.account as account', $conditions['WHERE'], $conditions['Extends']);

        if (!empty($res)) {
            //判断流水里是否有玩家记录 如果没有就del
            $start_time *= 1000;
            $end_time *= 1000;

            $conditions = array();
            $conditions['WHERE']['z.create_time::>='] = $start_time;
            $conditions['WHERE']['z.create_time::<='] = $end_time;
            $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
            $conditions['Extends']['GROUP'] = 'account';
            $tableArr = ['zones', 'level', 'task', 'guide', 'payment', 'consume_produce'];

            foreach ($tableArr as $table) {
                $this->joinTable = array(
                    'z' => array('name' => $table, 'type' => 'LEFT', 'on' => 'r.role_id = z.role_id')
                );

                $res = $this->getRows(['r.account as account', 'z.id'], $conditions['WHERE'], $conditions['Extends']);

                if (!empty($res)) break;
            }
        }

        $returnAccount = [];

        if (!empty($res)) {

            foreach ($res as $re) {
                array_push($returnAccount, $re['account']);
            }
        }

        echo json_encode($returnAccount);
    }

    public function getCreateTimeAccounts($day, $time, $accounts, $output = 0)
    {
        $end_time = $time * 1000;

        $fields = ['account', 'create_time'];
        $conditions = array();
        !empty($accounts) && $conditions['WHERE']['r.account::IN'] = $accounts;
        $conditions['WHERE']['r.create_time::<='] = $end_time;

        $rs = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        $hour = date('H', $time);

        if (!empty($rs)) {
            foreach ($rs as $value) {

                if ($hour >= 5) {
                    $roleCreateDay = ceil(($time - ($value['create_time'] / 1000)) / 86400);
                } else {
                    $roleCreateDay = ceil(($time - ($value['create_time'] / 1000)) / 86400) - 1;
                }

                if ($roleCreateDay >= $day) {
                    array_push($data, $value['account']);
                }
            }
        }

        if ($output == 0) return $data;

        echo json_encode($data);
    }

    public function getSingleRoleName($output = 0)
    {
        $fields = ['name'];

        $rs = $this->getRows($fields);

        $data = [];

        if (!empty($rs)) {
            foreach ($rs as $value) {
                array_push($data, $value['name']);
            }
        }

        if ($output == 0) return $data;

        echo json_encode($data);
    }

}