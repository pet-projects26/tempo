<?php
class RoledailyModel extends Model{
    public function __construct(){
        parent::__construct('role_daily');
        $this->alias = 'rd';
    }
    

    public function getRecordData($conditions){
        $fields = array(
            'role_id' , 'name' , 'account' , 'career' , 'level' , 'gold' ,
            "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time" ,
            "from_unixtime(last_login_time,'%Y-%m-%d %H:%i:%s') as last_login_time"
        );
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getCareerData($conditions){
        $fields = array(
            "from_unixtime(create_time,'%Y-%m-%d') as date",
            'sum(career<200) as djs',
            'sum(career>=200 and career<300) as yls',
            'count(role_id) as count_role_id'
        );
        $conditions['Extends']['GROUP'] = 'date';
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getLevelData($conditions){
        $fields = array('count(role_id) as count_role_id');
        $method = $this->getRow($fields , $conditions['WHERE'] , $conditions['Extends']);
        $fields[] = 'level';
        $conditions['Extends']['GROUP'] = 'level';
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array(
                $row['level'],
                $row['count_role_id'],
                $method['count_role_id'],
                sprintf("%.2f" , $row['count_role_id'] / $method['count_role_id'] * 100) . '%'
            );
        }
        echo json_encode(array($rs , $this->getCount()));
    }

    public function getFortuneData(){
        $fields = array('gold');
        $rs = $this->getRows($fields);
        $role_num = count($rs); //游戏总人数
        $rss = array(
            0 => array('0元宝' , 0 , 0 , '0%' , 0),
            1 => array('1元宝到10元宝' , 0 , 0 , '0%' , 0),
            2 => array('11元宝到100元宝' , 0 , 0 , '0%' , 0),
            3 => array('101元宝到500元宝' , 0 , 0 , '0%' , 0),
            4 => array('501元宝到1000元宝' , 0 , 0 , '0%' , 0),
            5 => array('1001元宝到5000元宝' , 0 , 0 , '0%' , 0),
            6 => array('5000元宝以上' , 0 , 0 , '0%' , 0)
        );
        if($rs && $role_num){
            foreach($rs as $k => $row){
                if($row['gold'] == 0){
                    $rss[0][1] ++;
                }
                else if($row['gold'] > 0 && $row['gold'] <= 10){
                    $rss[1][1] ++;
                    $rss[1][2] += $row['gold'];
                }
                else if($row['gold'] > 11 && $row['gold'] <= 100){
                    $rss[2][1] ++;
                    $rss[2][2] += $row['gold'];
                }
                else if($row['gold'] > 100 && $row['gold'] <= 500){
                    $rss[3][1] ++;
                    $rss[3][2] += $row['gold'];
                }
                else if($row['gold'] > 500 && $row['gold'] <= 1000){
                    $rss[4][1] ++;
                    $rss[4][2] += $row['gold'];
                }
                else if($row['gold'] > 1000 && $row['gold'] <= 5000){
                    $rss[5][1] ++;
                    $rss[5][2] += $row['gold'];
                }
                else if($row['gold'] > 5000){
                    $rss[6][1] ++;
                    $rss[6][2] += $row['gold'];
                }
            }
            foreach($rss as $k => $row){
                $rss[$k][3] =  sprintf("%.2f" , $row[1] / $role_num * 100) . '%';
                $rss[$k][4] = $row[1] ? sprintf("%.2f" , $row[2] / $row[1]) : 0;
            }
        }
        echo json_encode(array($rss , 7));
    }

    public function getCreateAnalysisData($conditions){
        $fields = array(
            'count(role_id) as count_role_id',
            'count(distinct account) as count_account'
        );
        $conditions['WHERE'] = array();
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , 1));
    }

    public function getRoles($role_id = '' , $fields = array() , $start = '' , $end = ''){
        empty($fields) && $fields = array('account' , 'role_id' , 'name' , 'career' , 'create_time' , 'level' , 'gold' , 'bind_gold' , 'copper' , 'sorce' , 'last_login_time');
        $conditions = array();
        if(is_array($role_id)){
            $conditions['WHERE'] = array();
            $role_id && $conditions['WHERE']['role_id::IN'] = $role_id;
            $start && $conditions['WHERE']['create_time::>='] =  $start;
            $end && $conditions['WHERE']['create_time::<='] =  $end;
            $conditions['Extends']['ORDER'] = array('id#desc');
            return $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        }
        else{
            $role_id && $conditions['WHERE']['role_id'] =  $role_id;
         	return $this->getRow($fields , $conditions['WHERE']);
        }
    }

    public function addRoles($data){
        foreach($data as $row){
            if(!$this->getRoles($row['role_id'] , array('id'))){
                 $this->add($row);
            }
        }
    }

    public function updateRoles($data){
        foreach($data as $k => $row){
            if($this->getRoles($k , array('id'))){
                $conditions = array();
                $conditions['WHERE']['role_id'] = $k;
                $this->update($row , $conditions['WHERE']);
            }
        }
    }
}