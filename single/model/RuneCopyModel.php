<?php

class RuneCopyModel extends Model
{
    public function __construct()
    {
        parent::__construct('rune_copy');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = array('count(distinct t.role_id) as count_role');
        $fields2 = array('count(t.role_id) as count_role');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;

        $join_num = $this->getRow($fields , $conditions['WHERE']);

        $conditions['WHERE']['t.result'] = 1; //成功
        $challenge_num = $this->getRow($fields2, $conditions['WHERE']);
        unset($conditions['WHERE']['t.create_time/1000::>=']);
        $pass_num = $this->getRow($fields, $conditions['WHERE']);

        $data['join_num'] = $join_num['count_role'] ? $join_num['count_role'] : 0;  //参与人数
        $data['challenge_num'] = $challenge_num['count_role'] ? $challenge_num['count_role'] : 0; //挑战成功次数
        $data['pass_num'] = $pass_num['count_role'] ? $pass_num['count_role'] : 0; //通关数≥1的玩家数

        //获取今天领取每日奖励的人数
        $data['reward_num'] = (new RuneCopyRewardModel())->getCountByRangtime($start_time, $end_time);

        echo json_encode($data);
    }

    //活跃玩家中通关数≥1的玩家数
//    public function getPassNumByRangtime($start_time, $end_time, array $accounts = [])
//    {
//        $this->joinTable = array(
//            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 't.role_id = r.role_id')
//        );
//
//        $conditions = array();
//        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
//        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
//        $conditions['WHERE']['t.result'] = 1; //成功
//        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
//
//        $fields = array('count(distinct t.role_id) as count_role');
//
//        $rs = $this->getRow($fields, $conditions['WHERE']);
//
//        $pass_num = $rs['count_role'] ? $rs['count_role'] : 0;
//
//        echo json_encode($pass_num);
//    }

    //获取角色远古符阵的挑战记录
    public function getRoleMaxLayerRecord($role_id)
    {
        $field = array('max(t.layer) as max_layers');
        $data = [];
        $conditions = array();
        $conditions['WHERE']["t.role_id"] = $role_id;
        $conditions['WHERE']["t.result"] = 1;

        $res = $this->getRow($field, $conditions['WHERE']);

        return $res;
    }

    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
//    public function getActionOpenToAccount($accounts = array(), $output = 1)
//    {
//        //获取副本对应的功能id
//        $reId = CDict::$runeCopy['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = (new ZonesModel())->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

