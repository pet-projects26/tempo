<?php

class RuneCopyRewardModel extends Model
{
    public function __construct()
    {
        parent::__construct('rune_copy_reward');
        $this->alias = 't';
    }

    //获取时间内的领取每日奖励的人数
    public function getCountByRangtime($start_time, $end_time)
    {
        $fields = array('count(distinct t.role_id) as count_role');;
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
        $rs = $this->getRow($fields , $conditions['WHERE']);
        $count = $rs['count_role'] ? $rs['count_role'] : 0;  //领取人数
        return $count;
    }
}

