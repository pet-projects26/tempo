<?php

class SceneAddRecordModel extends Model
{
    public function __construct()
    {
        parent::__construct('scene_add_record');
        $this->alias = 's';
    }

    //获取时间内的数据
    public function getDataByRangtime($type, $start_time, $end_time, $add_type = 0, $output = 1)
    {
        $data = array();

        //如果$type不在
        if (!array_key_exists($type, CDict::$sceneType)) {
            echo json_encode($data);
            exit;
        }

        $fields = array('count(distinct role_id) as count_role', 'sum(add_num) as sum_add_num');

        $conditions = array();
        $conditions['WHERE']['scene_type'] = $type;
        $conditions['WHERE']["create_time/1000::>="] = $start_time;
        $conditions['WHERE']["create_time/1000::<="] = $end_time;
        //$add_type 1为使用卷轴 2为元宝购买
        $add_type && $conditions['WHERE']['add_type'] = $add_type;
        $rs = $this->getRow($fields, $conditions['WHERE']);

        $data['add_num_role'] = $rs['count_role'] ? $rs['count_role'] : 0;
        $data['add_num_sum'] = $rs['sum_add_num'] ? $rs['sum_add_num'] : 0;

        if ($output == 0) return $data;

        echo json_encode($data);
    }
}

