<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/3/14
 * Time: 14:43
 */

class SettingModel extends Model
{

    public function cleanSql($type)
    {
        //查出所有表
        $sql = "show full tables where Table_type = 'BASE TABLE'";

        $tableData = $this->query($sql);

//Tables_in_server_test
        $a = 1;

        $keyName = 'Tables_in_' . MYSQL_DB;

        $sqlType = $type ? 'delete' : 'truncate';

        foreach ($tableData as $key) {

            $tableName = $key[$keyName];

            $sql = $sqlType . ' table ' . $tableName;

            $rs = $this->query($sql);

            if ($this->lastError()) {
                exit(
                json_encode([
                    'state' => 400,
                    'msg' => $this->lastError()
                ])
                );
            }
        }

        echo(
        json_encode([
            'state' => 200
        ])
        );

//delete table 表名;

//truncate table 表名;

    }

}