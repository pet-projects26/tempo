<?php
class SevenremainModel extends Model{
	public $a;
    public function __construct(){
        parent::__construct('seven_remain');
        $this->alias = 'sr';
		$this->a= new AccountSevenRemainModel();
    }

    public function getSevenremainData($conditions){
        $fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'role_num' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven'  , 'eight' , 'nine' , 'ten' , 'eleven' , 'twelve' , 'thirteen' , 'fourteen' , 'fifteen' , 'sixteen' , 'seventeen' , 'eighteen' , 'nineteen' , 'twenty' , 'twenty_one' , 'twenty_two' , 'twenty_three' , 'twenty_four' , 'twenty_five' , 'twenty_six' , 'twenty_seven' , 'twenty_eight' , 'twenty_nine' , 'thirty' );
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }
	
	public function getAccountSevenremainData($conditions){
        $fields = array("from_unixtime(date,'%Y-%m-%d') as date" , 'account_num' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven'   , 'eight' , 'nine' , 'ten' , 'eleven' , 'twelve' , 'thirteen' , 'fourteen' , 'fifteen' , 'sixteen' , 'seventeen' , 'eighteen' , 'nineteen' , 'twenty' , 'twenty_one' , 'twenty_two' , 'twenty_three' , 'twenty_four' , 'twenty_five' , 'twenty_six' , 'twenty_seven' , 'twenty_eight' , 'twenty_nine' , 'thirty' );
        $rs = $this->a->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        echo json_encode(array($rs , $this->getCount()));
    }
	
    //根据日期获取7日留存表的一条记录
    public function getSevenRemainByDate($date){
        $fields = array('date' , 'role_num' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven'  , 'eight' , 'nine' , 'ten' , 'eleven' , 'twelve' , 'thirteen' , 'fourteen' , 'fifteen' , 'sixteen' , 'seventeen' , 'eighteen' , 'nineteen' , 'twenty' , 'twenty_one' , 'twenty_two' , 'twenty_three' , 'twenty_four' , 'twenty_five' , 'twenty_six' , 'twenty_seven' , 'twenty_eight' , 'twenty_nine' , 'thirty' );
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        return $this->getRow($fields , $conditions['WHERE']);
    }

    public function setSevenRemain($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }
	
	//根据日期获取7日留存表的一条记录
    public function getAccountSevenRemainByDate($date){
        $fields = array('date' , 'account_num' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven'  , 'eight' , 'nine' , 'ten' , 'eleven' , 'twelve' , 'thirteen' , 'fourteen' , 'fifteen' , 'sixteen' , 'seventeen' , 'eighteen' , 'nineteen' , 'twenty' , 'twenty_one' , 'twenty_two' , 'twenty_three' , 'twenty_four' , 'twenty_five' , 'twenty_six' , 'twenty_seven' , 'twenty_eight' , 'twenty_nine' , 'thirty' );
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        return $this->a->getRow($fields , $conditions['WHERE']);
    }

    public function setAccountSevenRemain($data , $date = ''){
        if(empty($date)){
            $this->a->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->a->update($data , $conditions['WHERE']);
        }
    }
	
	
}