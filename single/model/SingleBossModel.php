<?php

class SingleBossModel extends Model
{

    private $r;

    public function __construct()
    {
        parent::__construct('single_boss');
        $this->alias = 's';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = array('count(distinct role_id) as count_role');
        $fields2 = array('count(role_id) as count_role');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["create_time/1000::>="] = $start_time;
        $conditions['WHERE']["create_time/1000::<="] = $end_time;
        $count_role = $this->getRow($fields, $conditions['WHERE']);

        //50级挑战人数与次数
        $conditions['WHERE']["result"] = 1;
        $conditions['WHERE']['boss_level'] = 1;
        $count_role_50 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_50_count = $this->getRow($fields2, $conditions['WHERE']);
        //1转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_1 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_1_count = $this->getRow($fields2, $conditions['WHERE']);
        //2转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_2 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_2_count = $this->getRow($fields2, $conditions['WHERE']);
        //3转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_3 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_3_count = $this->getRow($fields2, $conditions['WHERE']);
        //4转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_4 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_4_count = $this->getRow($fields2, $conditions['WHERE']);
        //5转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_5 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_5_count = $this->getRow($fields2, $conditions['WHERE']);
        //6转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_6 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_6_count = $this->getRow($fields2, $conditions['WHERE']);
        //7转挑战人数
        $conditions['WHERE']['boss_level'] += 1;
        $count_role_7 = $this->getRow($fields, $conditions['WHERE']);
        $count_role_7_count = $this->getRow($fields2, $conditions['WHERE']);

        $data['join_num'] = $count_role['count_role'] ? $count_role['count_role'] : 0;
        $data['join_for_level_50_num'] = $count_role_50['count_role'] ? $count_role_50['count_role'] : 0;
        $data['join_for_level_50_count'] = $count_role_50_count['count_role'] ? $count_role_50_count['count_role'] : 0;
        $data['join_for_rebirth_1_num'] = $count_role_1['count_role'] ? $count_role_1['count_role'] : 0;
        $data['join_for_rebirth_1_count'] = $count_role_1_count['count_role'] ? $count_role_1_count['count_role'] : 0;
        $data['join_for_rebirth_2_num'] = $count_role_2['count_role'] ? $count_role_2['count_role'] : 0;
        $data['join_for_rebirth_2_count'] = $count_role_2_count['count_role'] ? $count_role_2_count['count_role'] : 0;
        $data['join_for_rebirth_3_num'] = $count_role_3['count_role'] ? $count_role_3['count_role'] : 0;
        $data['join_for_rebirth_3_count'] = $count_role_3_count['count_role'] ? $count_role_3_count['count_role'] : 0;
        $data['join_for_rebirth_4_num'] = $count_role_4['count_role'] ? $count_role_4['count_role'] : 0;
        $data['join_for_rebirth_4_count'] = $count_role_4_count['count_role'] ? $count_role_4_count['count_role'] : 0;
        $data['join_for_rebirth_5_num'] = $count_role_5['count_role'] ? $count_role_5['count_role'] : 0;
        $data['join_for_rebirth_5_count'] = $count_role_5_count['count_role'] ? $count_role_5_count['count_role'] : 0;
        $data['join_for_rebirth_6_num'] = $count_role_6['count_role'] ? $count_role_6['count_role'] : 0;
        $data['join_for_rebirth_6_count'] = $count_role_6_count['count_role'] ? $count_role_6_count['count_role'] : 0;
        $data['join_for_rebirth_7_num'] = $count_role_7['count_role'] ? $count_role_7['count_role'] : 0;
        $data['join_for_rebirth_7_count'] = $count_role_7_count['count_role'] ? $count_role_7_count['count_role'] : 0;

        echo json_encode($data);
    }
}

