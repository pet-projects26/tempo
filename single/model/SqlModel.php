<?php

class SqlModel extends Model{

    public function __construct(){
        parent::__construct();
       
    }
	
	public function getData(){
		
        ini_set('mongo.long_as_object', 1); 

		$db = Util::mongoConn();

		$activity = $db->bg_activity->find();

		foreach ($activity as $key => $value) {
			$rs[] = $value;    
		}

		die(json_encode($rs));
	}

	/**
	 * 获取mongo的数据
	 * @param  [array] $document [description]
	 * @return [type]           [description]
	 */
	public function getMongo($document) {
		try {
			ini_set('mongo.native_long' , 0);
        	ini_set('mongo.long_as_object', 1); 

			$db = Util::mongoConn();

			$table = $document['table'];

			$data = $document['data'];

			$activity = $db->$table->find($data);

			$rs = array();
			foreach ($activity as $key => $value) {
				$rs[] = $value;    
			}

			die(json_encode($rs));
		} catch (\MongoException $e) {
			die(json_encode(array($e->getMessage())));
		}

	}
}