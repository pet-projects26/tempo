<?php

class StreamModel extends Model
{
    protected $lv;
    protected $cp;
    protected $p;
    // protected $r;
    //protected $t;
    // protected $lg;
    // protected $e;
    //protected $er;
    //protected $ce;
    // protected $gem;
    protected $email;

    public function __construct()
    {
        parent::__construct('role');
        $this->alias = 'r';

        $this->lv = new LevelModel();
        $this->cp = new CpModel();
        $this->p = new PaymentModel();
        // $this->r = new RideModel();
        // $this->t = new TaskModel();
        $this->lg = new LoginModel();
        // $this->e = new EquipModel();
        // $this->er = new EquipreplaceModel();
        // $this->ce = new CeModel();
        //$this->gem = new GemModel();
        $this->email = new EmailflowModel();
    }

    /*
    //获取汇总数据
    public function getSummaryData($conditions)
    {
        $returnData = [];
        $order = $conditions['Extends']['ORDER'][0];
        //截取最后一个#字符出现的位置
        $sort = substr($order, strripos($order, '#') + 1);

        $Role = new RoleModel();

        //判断是否查询了role_id 如果没有 默认查询最新的角色id
        if (empty($conditions['WHERE']['role_id'])) {
            $role_id = $Role->getLatestRoleId();
            $role_id && $conditions['WHERE']['role_id'] = $role_id;
        }

        //获取道具流水
        $fields = array('type', 'source', 'item_id', 'item_num', 'create_time', 'role_id', 'role_name');
        $itemArr = $this->cp->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        //获取等级流水
        $fields = array('level', 'create_time', 'role_id');
        $levelArr = $this->lv->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        //获取金钱流水
        $fields = array('role_id', 'role_name', 'type', 'count', 'coin_source', 'bag_copper', 'bag_gold', 'bag_bind_gold', 'money_type', 'create_time');
        $moneyArr = $this->p->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        //判断时间筛选是否存在 如果存在 改为start_time
        if (isset($conditions['WHERE']['create_time::>='])) {
            $conditions['WHERE']['start_time::>='] = $conditions['WHERE']['create_time::>='];
            unset($conditions['WHERE']['create_time::>=']);
        }
        if (isset($conditions['WHERE']['create_time::<='])) {
            $conditions['WHERE']['start_time::<='] = $conditions['WHERE']['create_time::<='];
            unset($conditions['WHERE']['create_time::<=']);
        }

        unset($conditions['Extends']['ORDER']);

        $conditions['Extends']['ORDER'] = ['start_time#' . $sort];

        //获取登录流水
        $fields = array('role_level', 'ip', 'start_time', 'end_time', 'role_id', 'role_name', 'first');
        $loginArr = $this->lg->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        if (!empty($itemArr)) {
            //获取所有物品集合
            $AllItem = $this->getItem(2);
            foreach ($itemArr as $itemKey => $itemVal) {
                $dat = [];
                $dat['role_id'] = $itemVal['role_id'];
                $dat['role_name'] = $itemVal['role_name'];

                if (!empty($AllItem[$itemVal['item_id']])) {
                    $itemName = '[' . $AllItem[$itemVal['item_id']]['name'] . ']';
                } else {
                    $itemName = '[未知道具]';
                }
                $itemNum = $itemVal['type'] ? '+' . $itemVal['item_num'] : '-' . $itemVal['item_num'];

                $sourceType = array_key_exists($itemVal['source'], CDict::$itemSource) ? CDict::$itemSource[$itemVal['source']] : '未知';

                $dat['content'] = '道具:' . $itemName . '; 类型:' . $sourceType . '; 数量:' . $itemNum;

                $dat['create_time'] = $itemVal['create_time'];

                array_push($returnData, $dat);
            }
        }

        if (!empty($levelArr)) {
            foreach ($levelArr as $lvlKey => $lvlVal) {

                $dat = [];
                $dat['role_id'] = $lvlVal['role_id'];
                $role_name = $Role->getRoleFields($lvlVal['role_id'], 'name');

                $dat['role_name'] = $role_name ? $role_name : '[MySql未知]';

                $dat['content'] = '达到等级LV.' . $lvlVal['level'];

                $dat['create_time'] = $lvlVal['create_time'];

                array_push($returnData, $dat);
            }
        }

        if (!empty($moneyArr)) {
            foreach ($moneyArr as $monKey => $monVal) {
                $dat = [];
                $dat['role_id'] = $monVal['role_id'];
                $dat['role_name'] = $monVal['role_name'];

                $moneyCount = $monVal['type'] ? '+' . $monVal['count'] : '-' . $monVal['count'];

                $sourceType = array_key_exists($monVal['coin_source'], CDict::$itemSource) ? CDict::$itemSource[$monVal['coin_source']] : '未知';

                $moneyType = array_key_exists($monVal['money_type'], CDict::$money) ? CDict::$money[$monVal['money_type']] : '未知';


                $dat['content'] = '金钱类型:' . $moneyType . '; 类型:' . $sourceType . '; 数量:' . $moneyCount . '; (背包剩余铜钱:' . $monVal['bag_copper'] . '; 剩余元宝:' . $monVal['bag_gold'] . '; 剩余绑定元宝: ' . $monVal['bag_bind_gold'] . ')';

                $dat['create_time'] = $monVal['create_time'];
                array_push($returnData, $dat);
            }
        }

        if (!empty($loginArr)) {
            foreach ($loginArr as $loKey => $loVal) {

                $dat = [];
                $dat['role_id'] = $loVal['role_id'];
                $dat['role_name'] = $loVal['role_name'];

                $start_time = date('Y-m-d H:i:s', $loVal['start_time'] / 1000);

                $end_time = $loVal['end_time'] ? date('Y-m-d H:i:s', $loVal['end_time'] / 1000) : '未知';

                $first = $loVal['first'] ? '是' : '否';

                $dat['content'] = '登录时等级:' . $loVal['role_level'] . '; 是否为首次登录: ' . $first . '; 登录IP:' . $loVal['ip'] . '; 登录时间:' . $start_time . '; 离线时间:' . $end_time;

                $dat['create_time'] = $loVal['start_time'];

                array_push($returnData, $dat);
            }
        }

        //再以所有键的create_time排序
        $sortKey = array_column($returnData, 'create_time');

        if ($sort == 'desc') {
            array_multisort($sortKey, SORT_DESC, $returnData);
        } else if ($order == 'asc') {
            array_multisort($sortKey, SORT_ASC, $returnData);
        } else {
            array_multisort($sortKey, SORT_DESC, $returnData);
        }

        foreach ($returnData as &$val) {
            $val['create_time'] = date('Y-m-d H:i:s', $val['create_time'] / 1000);
        }

        echo json_encode(array($returnData, count($returnData)));
    }
    */
    public function getitemstream($conditions)
    {
        $fields = array('type', 'source', 'item_id', 'item_num', "from_unixtime(create_time/1000,'%Y-%m-%d %H:%i:%s') AS create_time", 'role_id');
        $result = $this->cp->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode(array($result, $this->getCount()));
    }

    public function level_data($conditions)
    {
        $fields = array('level', "from_unixtime(create_time/1000,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');
        $result = $this->lv->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode(array($result, $this->getCount()));
    }

    /*
        public function prop_data($conditions)
        {
            $fields = array('type', 'source', 'item_id', 'item_num', "from_unixtime(create_time/1000,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');
            $result = $this->cp->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
            echo json_encode(array($result, $this->getCount()));
        }
        */
    public function money_data($conditions)
    {

        $result = $this->p->getRows(array('coin_source', 'money_type', 'type', 'count', 'balance', 'create_time', 'role_id'), $conditions['WHERE'], $conditions['Extends']);

        echo json_encode(array($result, $this->getCount()));
    }

    /*
    public function ride_data($conditions)
    {

        $fields = array('ride_class', 'ride_level', "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');
        $result = $this->r->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode(array($result, $this->getCount()));
    }

    public function task_data($conditions)
    {
        $task = $this->t->getAllTask();

        $conditions['WHERE']['type::!='] = 4;

        if (!empty($conditions['WHERE']['task_name'])) {
            $task_name = $conditions['WHERE']['task_name'];

            foreach ($task as $k => $v) {
                strpos($v['name'], $task_name) !== false && $id[] = $k;
            }
            if (!empty($id)) {
                $conditions['WHERE']['task_id::IN'] = $id;
            } else {
                echo json_encode(array(array(), 0));
                exit;
            }
        }
        unset($conditions['WHERE']['task_name']);

        $fields = array('task_id', 'type', 'status', "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');
        $result = $this->t->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        $arr = array();
        foreach ($result as $k => $v) {
            $task_id = $v['task_id'];
            $arr[$k]['task_id'] = $task_id;
            if ($v['type'] == 1) {
                $arr[$k]['type'] = '主线任务';
            } elseif ($v['type'] == 2) {
                $arr[$k]['type'] = '支线任务';
            } elseif ($v['type'] == 5) {
                $arr[$k]['type'] = '仙盟任务';
            } else {
                $arr[$k]['type'] = '';
            }
            $arr[$k]['name'] = !empty($task["$task_id"]['name']) ? $task["$task_id"]['name'] : '';

            if ($v['status'] == 3 || $v['status'] == 4) {
                $arr[$k]['status'] = '已接受任务';
            } elseif ($v['status'] == 5) {
                $arr[$k]['status'] = '已完成任务';
            } else {
                $arr[$k]['status'] = '';
            }
            $arr[$k]['create_time'] = $v['create_time'];
            $arr[$k]['role_id'] = $v['role_id'];
        }
        echo json_encode(array($arr, $this->getCount()));
    }
*/
    public function login_data($conditions)
    {

        $fields = array('role_level', 'ip', 'start_time', 'end_time', 'role_id');
        $result = $this->lg->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        foreach ($result as $k => $v) {
            $result[$k]['online'] = $this->time_format($v['start_time'] / 1000, $v['end_time'] / 1000);
            $result[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time'] / 1000);
            $result[$k]['end_time'] = $v['end_time'] ? date('Y-m-d H:i:s', $v['end_time'] / 1000) : '';
        }
        echo json_encode(array($result, $this->getCount()));
    }

    public function email_data($conditions)
    {
        $fields = array('type', 'email_id', 'email_name', 'create_time', 'role_id');
        $result = $this->email->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        foreach ($result as $k => $v) {
            $result[$k]['role_id'] = $v['role_id'] === 0 ? '全服玩家' : $v['role_id'];
            $result[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time'] / 1000);
        }
        echo json_encode(array($result, $this->getCount()));
    }
    /*
        public function equip_data($conditions)
        {
            $equip = CDict::$equie;
            $result = $this->e->getRows($conditions['fields'], $conditions['WHERE'], $conditions['Extends']);
            foreach ($result as $k => $v) {
                $result[$k]['part'] = $equip[$v['part']];
            }
            echo json_encode(array($result, $this->getCount()));
        }

        public function equip_replace_data($conditions)
        {

            $result = $this->er->getRows($conditions['fields'], $conditions['WHERE'], $conditions['Extends']);
            echo json_encode(array($result, $this->getCount()));
        }
    */
    /**
     * [ce_data 战斗力流水]
     * @param  [type] $conditions [description]
     * @return [type]             [description]
     */
    /*
    public function ce_data($conditions)
    {
        $fields = array('ce', "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');
        $result = $this->ce->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode(array($result, $this->getCount()));
    }*/

    /**
     * [gem_data 宝石流水]
     * @param  [type] $conditions [description]
     * @return [type]             [description]
     */
    /*
    public function gem_data($conditions)
    {
        $fields = array('item_id', 'part', 'type', "from_unixtime(create_time,'%Y-%m-%d %H:%i:%s') as create_time", 'role_id');
        $result = $this->gem->getRows($fields, $conditions['WHERE'], $conditions['Extends']);
        echo json_encode(array($result, $this->getCount()));
    }*/

}