<?php

class SwimmingCopyModel extends Model
{
    public function __construct()
    {
        parent::__construct('swimming_copy');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        //获取参与人数 总沐浴时长 捡肥皂人数 捡肥皂总次数
        $fields = array('count(distinct t.role_id) as count_role', 'sum(count) as count');
        $fields2 = array('start_time', 'end_time');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.end_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.end_time/1000::<="] = $end_time;

        //参与人数
        $join_num = $this->getRow($fields, $conditions['WHERE']);
        //沐浴时长
        $longTime = $this->getRows($fields2, $conditions['WHERE']);
        $conditions['WHERE']['t.count::>'] = 0; //捡肥皂数大于0的玩家
        $count_num = $this->getRow($fields, $conditions['WHERE']);

        $swim_time = 0;

        if (!empty($longTime)) {
            foreach ($longTime as $v) {

                $swim_time += $v['end_time'] - $v['start_time'];

            }
            //毫秒转换成秒
            $swim_time /= 1000;
        }

        $data['join_num'] = $join_num['count_role'] ? $join_num['count_role'] : 0;  //参与人数
        $data['soap_num'] = $count_num['count_role'] ? $count_num['count_role'] : 0; //捡肥皂人数
        $data['soap_count'] = $count_num['count'] ? $count_num['count'] : 0; //捡肥皂次数
        $data['swim_time'] = $swim_time; //沐浴时长

        echo json_encode($data);
    }
}

