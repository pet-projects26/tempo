<?php

class TalkDataModel extends Model{

    public function __construct(){
        parent::__construct('talk');
        $this->alias = 't';
    }
    
    
    //获取聊天内容
    public function getTalk($conditions)  {
    	$sql = "SELECT role_name,content FROM  ny_talk WHERE create_time  BETWEEN {$conditions['start_time']} AND {$conditions['end_time']} ";
		$res = $this->query ( $sql );
// 		file_put_contents('/tmp/role.txt', json_encode($res)."\n\r",FILE_APPEND);
		echo json_encode($res);
    }
   
}