<?php
class TaskModel extends Model{

    public function __construct(){
        parent::__construct('task');
        $this->alias = 't';

        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 't.role_id = r.role_id')
        );
    }
    /*
        //多个服的
        public function getAllSerTaskLossData($conditions){
            $type=$conditions['WHERE']['type'];
            $starttime=!empty($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : '' ;
            $endtime=!empty($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : time();
            $roleModel=new RoleModel();
            $role_num = $roleModel->getRoleByDate('bcount' ,$starttime,$endtime); //总人数
            $task = !empty($type) ? $this->getTaskByType($type) : $this->getAllTask();
            $get = $this->getTaskByStatus(array(3 , 4),$starttime,$endtime);//任务接受数
            $done= $this->getTaskByStatus(5,$starttime,$endtime);//任务完成数
            foreach( $task as $k=>$row){
                $row['role_num']=$role_num;
                $row['get'] = !empty($get[$k]) ? $get[$k] : 0;   //接受任务数
                $row['done'] = !empty($done[$k]) ? $done[$k] : 0;   //任务完成数
                $row['type'] = CDict::$taskType[$row['type']];
                $task[$k] = $row;
            }
            echo json_encode(array($task , count($task)));
        }

        public function getTaskLossData($conditions){
            $type=$conditions['WHERE']['type'];
            $starttime=!empty($conditions['WHERE']['create_time::>=']) ? $conditions['WHERE']['create_time::>='] : '' ;
            $endtime=!empty($conditions['WHERE']['create_time::<=']) ? $conditions['WHERE']['create_time::<='] : time();
            $roleModel=new RoleModel();
            $role_num = $roleModel->getRoleByDate('bcount' ,$starttime,$endtime); //总人数
            $task = $type ? $this->getTaskByType($type) : $this->getAllTask();
            $get = $this->getTaskByStatus(array(3 , 4),$starttime,$endtime);//任务接受数
            $done= $this->getTaskByStatus(5,$starttime,$endtime);//任务完成数
            foreach( $task as $k=>$row){
                $row['role_num']=$role_num;
                $row['get'] = !empty($get[$k]) ? $get[$k] : 0;   //接受任务数
                $row['done'] = !empty($done[$k]) ? $done[$k] : 0;   //任务完成数
                if($role_num !=0){

                    $row['role_done']=sprintf("%.2f" ,$row['done']/$role_num * 100) . '%';//角色完成率

                }else{
                    $row['role_done']=0;
                    $row['task_get']=0;
                }

                if($row['get'] !=0){
                    $row['task_done']=sprintf("%.2f" ,$row['done']/$row['get'] * 100) . '%';//任务通过率
                }else{
                    $row['task_done']=0;
                }

                if($role_num !=0){

                    $row['task_get']=sprintf("%.4f" ,$row['get']/$role_num ) ;//任务接受率

                }else{
                    $row['role_done']=0;
                    $row['task_get']=0;
                }

                $row['loss']=(sprintf("%.2f" ,(1-$row['task_get'])* 100)).'%';
                $row['task_get']=$row['task_get']*100 . '%';//任务接受率
                $row['type'] = CDict::$taskType[$row['type']];
                $task[$k] = $row;
            }

            //file_put_contents('test.txt', json_encode($task) , 8);exit;
            echo json_encode(array($task , count($task)));
        }

        public function getTaskByStatus($status , $starttime,$endtime){
            $fields = array('task_id' , 'count(role_id) as count_role_id');
            $conditions = array();
            $conditions['WHERE'][is_array($status) ? 'status::IN' : 'status'] = $status;
            $starttime && $conditions['WHERE']["create_time::>="] = $starttime;
            $endtime && $conditions['WHERE']["create_time::<="] = $endtime;
            $conditions['Extends']['GROUP'] = 'task_id';

            $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
            $rss = array();
            foreach($rs as $row){
                $rss[$row['task_id']] = $row['count_role_id'];
            }
            return $rss;
        }

        public function getTaskByType($type){
            $task = array();
            $task_type = $this->getJsonTask($type);
            foreach($task_type as $k => $t){
                $task[$k] = array(
                    'id' => $k,                          //任务ID
                    'type' => $t['type'],                //任务类型
                    'name' => $t['name'],                //任务名称

                );
            }
            return $task;
        }

        public function getAllTask(){
            $task = array();
            $types = array(1 , 2 ); //1主线任务 2支线任务 4赏金任务
            foreach($types as $type){
                $task_type = $this->getJsonTask($type);
                foreach($task_type as $k => $t){
                    $task[$k] = array(
                        'id' => $k,                          //任务ID
                        'type' => $t['type'],                //任务类型
                        'name' => $t['name'],                //任务名称

                    );
                }
            }
            return $task;
        }

        //获取任务的配置文件
        public function getJsonTask($type){
            $types = array(
                1 => 'task_main.json',
                2 => 'task_branch.json',
                5 => 'task_faction.json'
            );
            if(array_key_exists($type , $types)){
                $json = file_get_contents(DATA_DIR . '/task/' . $types[$type]);
                return json_decode($json , true);
            }
            else{
                return false;
            }
        }
    */

    /***
     * @param $start_time
     * @param $end_time
     * @param array $accounts
     */
    public function getDataByRangtime($start_time, $end_time, array $accounts = [])
    {

        $fields = array('t.task_id as task_id ', 't.task_name as task_name');

        $conditions = array();
        $conditions['WHERE']['t.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['t.create_time::<='] = $end_time * 1000;
        $conditions['Extends']['GROUP'] = 'task_id';

        $todayTask = $this->getRows($fields, $conditions['WHERE'], $conditions['Extends']);

        $rs = [];

        $one_task_get_acc = $this->getTaskStatusAccount('m1001', $start_time, $end_time, 0, $accounts);
        $one_task_get_num = !empty($one_task_get_acc) ? count($one_task_get_acc) : 0;

        if (!empty($todayTask)) {

            foreach ($todayTask as $key => $row) {
                $rs[$key]['task_id'] = $row['task_id'];
                $rs[$key]['task_name'] = $row['task_name'];
                $rs[$key]['one_task_get_num'] = $one_task_get_num;
                $getAccounts = $this->getTaskStatusAccount($row['task_id'], $start_time, $end_time, 0, $accounts);
                $getNum = count($getAccounts);

                if ($getNum > 0) {
                    $doneAccounts = $this->getTaskStatusAccount($row['task_id'], $start_time, $end_time, 1, $getAccounts);
                }

                $rs[$key]['get_num'] = $getNum;
                $rs[$key]['done_num'] = isset($doneAccounts) ? count($doneAccounts) : 0;
            }
        }

        echo json_encode($rs);
    }

    /***
     * @param $start_time
     * @param $end_time
     * @param $status
     * @param array $accounts
     * @return array
     */
    public function getTaskStatusAccount($task_id, $start_time, $end_time, $status, array $accounts = [])
    {

        $fields = array('distinct r.account as account');

        $conditions = array();
        $conditions['WHERE']['t.task_id'] = $task_id;
        $conditions['WHERE']['t.status'] = $status;
        $conditions['WHERE']['t.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['t.create_time::<='] = $end_time * 1000;
        !empty($accounts) && $conditions['WHERE']['r.account::IN'] = $accounts;

        $res = $this->getRows($fields, $conditions['WHERE']);

        $data = [];

        if ($res) {
            foreach ($res as $value) {
                array_push($data, $value['account']);
            }
        }

        return $data;
    }
}