<?php

class TeamCopyModel extends Model
{
    public function __construct()
    {
        parent::__construct('team_copy');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {

        $fields = array('count(distinct t.role_id) as count_role');
        $fields2 = array('count(t.role_id) as count_role');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;

        $join_num = $this->getRow($fields , $conditions['WHERE']);

        $conditions['WHERE']['t.result'] = 1; //成功
        $challenge_num = $this->getRow($fields, $conditions['WHERE']);
        $challenge_count = $this->getRow($fields2, $conditions['WHERE']);

        $data['join_num'] = $join_num['count_role'] ? $join_num['count_role'] : 0;  //参与人数
        $data['challenge_count'] = $challenge_count['count_role'] ? $challenge_count['count_role'] : 0; //挑战成功次数
        $data['challenge_num'] = $challenge_num['count_role'] ? $challenge_num['count_role'] : 0; //挑战成功人数

        //获取时间段内的购买人数和购买总次数
        $addNumData = (new SceneAddRecordModel())->getDataByRangtime(CDict::$teamCopy['sceneType'], $start_time, $end_time, 2, 0);

        $data['add_num_role'] = $addNumData['add_num_role'];
        $data['add_num_sum'] = $addNumData['add_num_sum'];

        echo json_encode($data);
    }

    //获取角色组队副本的最大波数
    public function getRoleMaxLayerRecord($role_id)
    {
        $field = array('max(t.layer) as max_layers');
        $data = [];
        $conditions = array();
        $conditions['WHERE']["t.role_id"] = $role_id;
        $conditions['WHERE']["t.result"] = 1;

        $res = $this->getRow($field, $conditions['WHERE']);

        return $res;
    }

    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
//    public function getActionOpenToAccount($accounts = array(), $output = 1)
//    {
//        //获取副本对应的功能id
//        $reId = CDict::$teamCopy['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = (new ZonesModel())->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

