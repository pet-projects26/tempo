<?php

class ThreerealmsBossInfoModel extends Model
{

    public function __construct()
    {
        parent::__construct('three_realms_boss_info');
        $this->alias = 's';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = 'count(distinct r.role_id) as count_role';
        $fields2 = 'sum(s.gold_inspire_num) as gold_inspire_sum, sum(s.ingots_inspire_num) as ingots_inspire_sum, sum(s.relive_num) as relive_sum';
        $tablename = 'ny_' . $this->tableName . ' as s left join ny_role as r on s.role_id = r.role_id';

        $where = ' s.create_time/1000 >= ' . $start_time . ' and s.create_time/1000 <= ' . $end_time;

        $data = array();

        $sql = 'select ' . $fields2 . ' from ' . $tablename . ' where ' . $where . ' limit 1';

        $count_role = $this->query($sql); //挑战人数 无去重

        //鼓舞人数 去重
        $where_inspire_num = ' and (s.gold_inspire_num > 0 or s.ingots_inspire_num > 0 )';

        $sql_inspire_num = 'select ' . $fields . ' from ' . $tablename . ' where ' . $where . $where_inspire_num . ' limit 1';

        $count_role_inspire_num = $this->query($sql_inspire_num);

        //金币鼓舞人数 去重
        $where_gold_inspire_num = ' and s.gold_inspire_num > 0';

        $sql_gold_inspire_num = 'select ' . $fields . ' from ' . $tablename . ' where ' . $where . $where_gold_inspire_num . ' limit 1';

        $count_role_gold_inspire_num = $this->query($sql_gold_inspire_num);

        //元宝鼓舞人数 去重
        $where_ingots_inspire_num = ' and s.ingots_inspire_num > 0';

        $sql_ingots_inspire_num = 'select ' . $fields . ' from ' . $tablename . ' where ' . $where . $where_ingots_inspire_num . ' limit 1';

        $count_role_ingots_inspire_num = $this->query($sql_ingots_inspire_num);

        //涅槃复活人数 去重
        $where_relive_num = ' and s.relive_num > 0';

        $sql_relive_num = 'select ' . $fields . ' from ' . $tablename . ' where ' . $where . $where_relive_num . ' limit 1';

        $count_role_relive_num = $this->query($sql_relive_num);

        $data['gold_inspire_count'] = $count_role[0]['gold_inspire_sum'] ? $count_role[0]['gold_inspire_sum'] : 0;
        $data['ingots_inspire_count'] = $count_role[0]['ingots_inspire_sum'] ? $count_role[0]['ingots_inspire_sum'] : 0;
        $data['relive_count'] = $count_role[0]['relive_sum'] ? $count_role[0]['relive_sum'] : 0;

        $data['inspire_num'] = $count_role_inspire_num[0]['count_role'] ? $count_role_inspire_num[0]['count_role'] : 0;
        $data['gold_inspire_num'] = $count_role_gold_inspire_num[0]['count_role'] ? $count_role_gold_inspire_num[0]['count_role'] : 0;
        $data['ingots_inspire_num'] = $count_role_ingots_inspire_num[0]['count_role'] ? $count_role_ingots_inspire_num[0]['count_role'] : 0;
        $data['relive_num'] = $count_role_relive_num[0]['count_role'] ? $count_role_relive_num[0]['count_role'] : 0;

        echo json_encode($data);
    }
}

