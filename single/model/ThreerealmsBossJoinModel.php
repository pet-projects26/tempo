<?php

class ThreerealmsBossJoinModel extends Model{

    private $r;

    public function __construct(){
        parent::__construct('three_realms_boss_join');
        $this->alias = 's';
    }

	//获取时间内的数据
    public function getJoinNumByRangtime($start_time, $end_time)
    {
        $fields = array('count(distinct r.role_id) as count_role');

        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 's.role_id = r.role_id')
        );

        $data = array();
        $conditions = array();
        $conditions['WHERE']["s.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["s.create_time/1000::<="] = $end_time;
        $count_role = $this->getRow($fields , $conditions['WHERE']);

        $data['join_num'] = $count_role['count_role'] ? $count_role['count_role'] : 0;

        //获取时间段内的购买人数和购买总次数
        $addNumData = (new SceneAddRecordModel())->getDataByRangtime(CDict::$threeRealmsBoss['sceneType'], $start_time, $end_time, 2, 0);

        $data['add_num_role'] = $addNumData['add_num_role'];
        $data['add_num_sum'] = $addNumData['add_num_sum'];

        echo json_encode($data);
    }

    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts
//    public function getActionOpenToAccount($accounts = array(), $output = 1)
//    {
//        //获取副本对应的功能id
//        $reId = CDict::$threeRealmsBoss['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = (new ZonesModel())->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

