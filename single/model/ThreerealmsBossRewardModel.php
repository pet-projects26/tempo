<?php

class ThreerealmsBossRewardModel extends Model
{

    public function __construct()
    {
        parent::__construct('three_realms_boss_reward');
        $this->alias = 's';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        $fields = ['r.role_id', 'count(s.role_id) as count_role'];

        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 's.role_id = r.role_id')
        );
        $conditions = array();
        $conditions['WHERE']['s.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['s.create_time::<='] = $end_time * 1000;

        $rs = $this->getRow($fields, $conditions['WHERE']);

        $challenge_num = $rs['count_role'] ? $rs['count_role'] : 0;

        echo json_encode($challenge_num);
    }
}

