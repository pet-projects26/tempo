<?php

class TiantiCopyModel extends Model
{
    public function __construct()
    {
        parent::__construct('tianti_copy');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        //获取参与人数  次数总和
        $fields = array('count(distinct t.role_id) as count_role', 'count(t.role_id) as count');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;

        //参与人数
        $rs = $this->getRow($fields, $conditions['WHERE']);

        $data['join_num'] = $rs['count_role'] ? $rs['count_role'] : 0;  //参与人数
        $data['count'] = $rs['count'] ? $rs['count'] : 0;

        //获取时间段内的购买人数和购买总次数
        $addNumData = (new SceneAddRecordModel())->getDataByRangtime(CDict::$tiantiCopy['sceneType'], $start_time, $end_time, 2, 0);

        $data['add_num_role'] = $addNumData['add_num_role'];
        $data['add_num_sum'] = $addNumData['add_num_sum'];

        echo json_encode($data);
    }
}

