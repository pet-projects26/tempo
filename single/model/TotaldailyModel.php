<?php
class TotaldailyModel extends Model{
    public function __construct(){
        parent::__construct('total_daily');
        $this->alias = 'td';
    }

    public function getRecordData($conditions){
        $fields = array(
            "from_unixtime(date,'%Y-%m-%d') as date" , 'reg_num' , 'role_num' , 'login_num' , 'atv_num' , 'old_num' , 'pay_num' ,
            'money' , 'lc_percent' , 'arpu' , 'first_pay_num' , 'first_money' , 'first_percent' ,
            'first_arpu' , 'old_pay_num' , 'old_money' , 'old_percent', 'old_arpu' , 'reg_arpu' ,
            'max_num' , 'avg_num' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven', 'fifteen' , 'thirty'
        );
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        foreach($rs as $k => $row){
            $rs[$k] = array_values($row);
        }
        echo json_encode(array($rs , $this->getCount()));
    }
    public function getTotaldailyByDate($date , $fields = array()){
        $conditions = array();
        $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
        empty($fields) && $fields = array(
            'date' , 'reg_num' , 'role_num' , 'login_num' , 'atv_num' , 'old_num' , 'pay_num' ,
            'money' , 'lc_percent' , 'arpu' , 'first_pay_num' , 'first_money' , 'first_percent' ,
            'first_arpu' , 'old_pay_num' , 'old_money' , 'old_percent', 'old_arpu' , 'reg_arpu' ,
            'max_num' , 'avg_num' , 'two' , 'three' , 'four' , 'five' , 'six' , 'seven', 'fifteen' , 'thirty'
        );
        return $this->getRow($fields , $conditions['WHERE']);
    }
    public function setTotaldaily($data , $date = ''){
        if(empty($date)){
            $this->add($data);
        }
        else{
            $conditions = array();
            $conditions['WHERE']["from_unixtime(date,'%Y-%m-%d')"] = $date;
            $this->update($data , $conditions['WHERE']);
        }
    }
}