<?php

class VipModel extends Model{

    public function __construct(){
        parent::__construct('vip');
        $this->alias = 'v';
        $this->joinTable = array(
            'rd' => array('name' => 'role_daily' , 'type' => 'LEFT' , 'on' => 'rd.role_id = v.role_id')
        );
    }

    public function getRecordData($conditions){
        $sql  = 'select max(v.level) as level , max(v.create_time) as create_time , v.day , v.role_id , rd.name from ny_vip as v ';
        $sql .= 'left join ny_role_daily as rd on rd.role_id = v.role_id where day !=0  ';
        isset($conditions['WHERE']['v.level']) && $conditions['WHERE']['v.level'] != 0 && $sql .= ' and v.level = ' . $conditions['WHERE']['v.level'];
        isset($conditions['WHERE']['rd.name']) && $sql .= ' and rd.name = ' . $conditions['WHERE']['rd.name'];
        isset($conditions['WHERE']['rd.create_time::>=']) && $sql .= ' and rd.create_time >=' . $conditions['WHERE']['rd.create_time::>='];
        isset($conditions['WHERE']['rd.create_time::<=']) && $sql .= ' and rd.create_time <=' . $conditions['WHERE']['rd.create_time::<='];
        $sql .= ' group by v.role_id having max(v.level) > 0 order by v.id desc';
        $rs = $this->query($sql);
        if(is_array($rs) && $rs){
            foreach($rs as $k => $row){
                $rs[$k] = array(
                    $row['level'],
                    $row['name'],
                    date('Y-m-d H:i:s' , $row['create_time']),
                    date('Y-m-d H:i:s' , $row['create_time'] + $row['day'] * 86400)
                );
            }
        }
        echo json_encode(array($rs , count($rs)));
    }

    public function getTotaData($conditions){
        unset($conditions['Extends']['LIMIT']);
        $fields = array('max(level) as level' , 'role_id');
        $conditions['Extends']['ORDER'] = array('id#asc');
		$conditions['Extends']['GROUP'] = 'role_id';
		$conditions['WHERE']['day::!=']=0;
        $rs = $this->getRows($fields , $conditions['WHERE'] , $conditions['Extends']);
        $rss = array();
        foreach($rs as $row){
            $rss[$row['role_id']] = $row;
        }
        $total_vip_num = count($rss);
        $rs = array();
        foreach($rss as $row){
            $rs[$row['level']][] = $row['role_id'];
        }
		$sql="select count(*) as count from ny_role";
		$role_num =$this->query($sql);
		$role_num=$role_num[0]['count'];
        $levels = CDict::$vip;
        unset($levels[0]);
        $rss = array();
        foreach($levels as $k => $level){
            $level_num = count($rs[$k]);
            $rss[] = array(
                $level,
                $level_num,
                ($total_vip_num ? sprintf("%.2f" , $level_num / $total_vip_num * 100) : 0) . '%',
                ($role_num ? sprintf("%.2f" , $level_num / $role_num * 100) : 0) . '%'
            );
        }
        echo json_encode(array($rss , count($rss)));
    }

    public function getVipAccounts($vipLevel, $time, $accounts = array(), $output = 1)
    {
        $this->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'r.role_id = v.role_id')
        );

        // $start_time = $time * 1000;
        $end_time = ($time + 86399) * 1000;

        $field = array('distinct r.account as account');
        $data = [];
        $conditions = array();
        $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;

        $conditions['WHERE']['v.vip_level::>='] = $vipLevel;
        //$conditions['WHERE']['v.create_time::>='] = $start_time;
        $conditions['WHERE']['v.create_time::<='] = $end_time;

        $res = $this->getRows($field, $conditions['WHERE']);

        $data = [];

        foreach ($res as $value) {
            array_push($data, $value['account']);
        }

        if ($output == 0) return $data;

        echo json_encode($data);

    }
}