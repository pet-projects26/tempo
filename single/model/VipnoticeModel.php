<?php
class VipnoticeModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 新增&&更新mongo内容
     * @param array $document
     * @return int $output
    */
    public function addNotice($document,$die=true){
        //plan1:每个渠道组只能有一条，即每个渠道组下面的服务器各只能有一条，后台不允许对该条记录进行删除操作，保持id的唯一对应性[server]=[_id]
        //plan2:清空该mongo表，然后再写入一条新的数据，以保证每个表只有一条记录
        ini_set('mongo.native_log',0);
        ini_set('mongo.long_as_object',1);
        $id = 1;
        $db = null;
        try{
            $db = Util::mongoConn();
        }catch (\pblib\Protobuf\Exception $e){
            error_log($e->getMessage()."\r",3,'error.log');
        }
        if(is_null($db))return 0;
        $res = $db->member->findOne(array('_id'=>1));
        if(empty($res) || in_array($document['type'],array('2','3'))){
            $document['updateTm'] = time();//只有初始化和选择邮件方式才允许更新
        }else{
            $upTime = $res['updateTm']->value;
            $document['updateTm'] = new MongoInt64($upTime);
        }
        $db->member->remove();
        unset($document['type']);
        $aId = intval($id);
        $document['_id'] = $aId;
        $document = Helper::mongoInt64($document);
        $rs = $db->member->insert($document);

        $return = isset($rs['ok']) ? $rs['ok'] : 0;
        if ($die)  {
            die ($return);
        }
        return $return;
    }

    /**
     * 删除VIP公告 功能待确定
     * @param $document
     * #return int
    */
    public function resetNotice($document){

    }
}
?>