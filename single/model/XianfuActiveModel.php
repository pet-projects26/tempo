<?php

class XianfuActiveModel extends Model
{
    public function __construct()
    {
        parent::__construct('xianfu_active');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time, &$data)
    {
        //获取 active_val_20_num  active_val_60_num active_val_100_num active_val_150_num
        $fields = array('count(distinct t.role_id) as count_role');
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
        $conditions['WHERE']['t.count::>='] = 20;
        //20
        $active_val_20 = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.count::>='] = 60;
        //60
        $active_val_60 = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.count::>='] = 100;
        //100
        $active_val_100 = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.count::>='] = 150;
        //150
        $active_val_150 = $this->getRow($fields, $conditions['WHERE']);

        $data['active_val_20_num'] = $active_val_20['count_role'] ? $active_val_20['count_role'] : 0;  //参与人数
        $data['active_val_60_num'] = $active_val_60['count_role'] ? $active_val_60['count_role'] : 0;
        $data['active_val_100_num'] = $active_val_100['count_role'] ? $active_val_100['count_role'] : 0;  //参与人数
        $data['active_val_150_num'] = $active_val_150['count'] ? $active_val_150['count'] : 0;

        return $data;
    }
}

