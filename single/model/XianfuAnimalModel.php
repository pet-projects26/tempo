<?php

class XianfuAnimalModel extends Model
{
    public function __construct()
    {
        parent::__construct('xianfu_animal');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time, &$data)
    {
        //获取 youli_num youli_count is_compass_num is_compass_count is_sihai_or_jiuzhou_count  is_end_num is_end_count
        $fields = array('count(distinct t.role_id) as count_role', 'count(t.role_id) as count');
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
        $conditions['WHERE']['t.status'] = 2; //出发
        //youli
        $youli = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.status'] = 3; //立即结束
        $end = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.status'] = 2; //出发
        $conditions['WHERE']['t.range_id::IN'] = [3, 4]; //后两个才能用罗盘

        $is_sihai_or_jiuzhou = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.is_compass'] = 1;
        //compass
        $compass = $this->getRow($fields, $conditions['WHERE']);


        $data['youli_num'] = $youli['count_role'] ? $youli['count_role'] : 0;
        $data['youli_count'] = $youli['count'] ? $youli['count'] : 0;
        $data['is_compass_num'] = $compass['count_role'] ? $compass['count_role'] : 0;
        $data['is_compass_count'] = $compass['count'] ? $compass['count'] : 0;
        $data['is_end_num'] = $end['count_role'] ? $end['count_role'] : 0;
        $data['is_end_count'] = $end['count'] ? $end['count'] : 0;
        $data['is_sihai_or_jiuzhou_count'] = $is_sihai_or_jiuzhou['count'] ? $is_sihai_or_jiuzhou['count'] : 0;

        return $data;
    }
}

