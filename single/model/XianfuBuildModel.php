<?php

class XianfuBuildModel extends Model
{
    public function __construct()
    {
        parent::__construct('xianfu_build');
        $this->alias = 't';
    }

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time, &$data)
    {
        $fields = array('count(distinct t.role_id) as count_role', 'count(t.role_id) as count');
        $conditions = array();
        $conditions['WHERE']["t.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["t.create_time/1000::<="] = $end_time;
        $conditions['WHERE']['t.build_id'] = CDict::$xianfuBuild['huiling'];
        $conditions['WHERE']['t.status'] = 1;
        //汇灵
        $huiling = $this->getRow($fields, $conditions['WHERE']);

        $conditions['WHERE']['t.build_id'] = CDict::$xianfuBuild['jubao'];

        $jubao = $this->getRow($fields, $conditions['WHERE']);

        //lianzhi
        unset($conditions['WHERE']['t.build_id']);
        $conditions['WHERE']['t.build_id::IN'] = [CDict::$xianfuBuild['liandan'], CDict::$xianfuBuild['lianqi'], CDict::$xianfuBuild['lianhun']];

        $conditions['WHERE']['t.status'] = 2;
        $lianzhi = $this->getRow($fields, $conditions['WHERE']);

        $data['huiling_num'] = $huiling['count_role'] ? $huiling['count_role'] : 0;  //参与人数
        $data['jubao_num'] = $jubao['count_role'] ? $jubao['count_role'] : 0;
        $data['lianzhi_num'] = $lianzhi['count_role'] ? $lianzhi['count_role'] : 0;  //参与人数
        $data['lianzhi_count'] = $lianzhi['count'] ? $lianzhi['count'] : 0;

        return $data;
    }
}

