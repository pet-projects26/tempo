<?php
/**
 * Created by PhpStorm.
 * User: w
 * Date: 2019/4/20
 * Time: 15:54
 */

class XianfuInfoModel extends Model
{

    //获取时间内的数据
    public function getDataByRangtime($start_time, $end_time)
    {
        //获取三个表的内容
        $data = [];
        //仙府建筑
        $Build = new XianfuBuildModel();

        $data = $Build->getDataByRangtime($start_time, $end_time, $data);

        //仙府灵兽
        $Animal = new XianfuAnimalModel();
        $data = $Animal->getDataByRangtime($start_time, $end_time, $data);

        //仙府活跃
        $Active = new XianfuActiveModel();
        $data = $Active->getDataByRangtime($start_time, $end_time, $data);

        echo json_encode($data);
    }
}