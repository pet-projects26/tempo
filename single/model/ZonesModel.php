<?php

class ZonesModel extends Model{

    public function __construct()
    {
        parent::__construct('zones');
        $this->alias = 'z';
        $this->joinTable = array(
            'r' => array('name' => 'role' , 'type' => 'LEFT' , 'on' => 'z.role_id = r.role_id')
        );
    }

	//获取时间内的数据
    public function getDataByRangtime($type, $start_time, $end_time)
    {

        //1 天关系统 2 大荒古塔
        if (!array_key_exists($type, CDict::$zonesTypes)) return false;

        $fields = array('count(distinct z.role_id) as count_role');
        $fields2 = array('count(z.role_id) as count_role');
        $data = array();
        $conditions = array();
        $conditions['WHERE']["z.create_time/1000::>="] = $start_time;
        $conditions['WHERE']["z.create_time/1000::<="] = $end_time;
        $conditions['WHERE']["z.zones_type"] = $type;

        $join_num = $this->getRow($fields , $conditions['WHERE']);

        $conditions['WHERE']['z.result'] = 1; //成功

        $challenge_num = $this->getRow($fields2, $conditions['WHERE']);

        $data['join_num'] = $join_num['count_role'] ? $join_num['count_role'] : 0;
        $data['challenge_num'] = $challenge_num['count_role'] ? $challenge_num['count_role'] : 0;

        echo json_encode($data);
    }

    //获取角色的所有副本挑战记录
    public function getRoleZonesMaxLayerRecord($role_id)
    {
        $field = array('max(z.zones_layers) as max_layers', 'z.zones_type as type');
        $data = [];
        $conditions = array();
        $conditions['WHERE']["z.role_id"] = $role_id;
        $conditions['WHERE']["z.result"] = 1;
        $conditions['Extends']['GROUP'] = 'z.zones_type';

        $res = $this->getRows($field, $conditions['WHERE'], $conditions['Extends']);

        if ($res) {
            foreach($res as $key => $val) {
                $data[$val['type']] = $val;
            }
        }

        return $data;

    }

    //过滤不满足开启条件的账号
    public function getAccountsZonesLayerRecord($layer, $time, $accounts = array(), $output = 1)
   {
       //$start_time = $time * 1000;
       $end_time = $time * 1000;

       $field = array('distinct r.account as account');
       $data = [];
       $conditions = array();
       $accounts && $conditions['WHERE']['r.account::IN'] = $accounts;
       $conditions['WHERE']["z.result"] = 1;
       $conditions['WHERE']['z.zones_type'] = 1;
       $conditions['WHERE']['z.zones_layers::>='] = $layer;
       //$conditions['WHERE']['z.create_time::>='] = $start_time;
       $conditions['WHERE']['z.create_time::<='] = $end_time;

       $res = $this->getRows($field, $conditions['WHERE']);

       $data = [];

       foreach ($res as $value) {
           array_push($data, $value['account']);
       }

       if ($output == 0) return $data;

       echo json_encode($data);
   }


    //根据传入的副本类型获取功能ID查询是否开启功能  return 过滤后的accounts数量
//    public function getActionOpenToAccount($zonesType, $accounts = array(), $output = 1)
//    {
//        if (!array_key_exists($zonesType, CDict::$zonesTypes)) return false;
//
//        //获取副本对应的功能id
//        $reId = CDict::$zonesTypes[$zonesType]['reId'];
//
//        //读取json配置
//        $action = Fields::getJsonData('action_open', ['name', 'openType', 'openLevel'], 'id');
//
//        $zonesAction = $action[$reId];
//
//        $returnAccounts = 0;
//        //判断开启类型查不同的sql
//        switch ($zonesAction['openType']) {
//            case '1' :
//                $where = [];
//                $where['role_level::>='] = $zonesAction['openLevel'];
//                $returnAccounts = (new RoleModel())->getRoleNum($where, $accounts);
//                break;
//            case '2':
//                $returnAccounts = $this->getAccountZonesLayerRecord($zonesAction['openLevel'], $accounts);
//                break;
//            default:
//                $returnAccounts = 0;
//                break;
//        }
//
//        if ($output == 0) return $returnAccounts;
//
//        echo json_encode($returnAccounts);
//    }
}

