<?php

class _9130Model extends Model
{
    // private $role;

    public function __construct()
    {
//        $this->joinTable = array(
//            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'l.role_id = r.role_id')
//        );
    }

    public function getCreateRole($start_time, $end_time, $package)
    {
        //from	服务器id
        //userid	账号id
        //roleid	角色id
        //account	角色名
        //platform	渠道id
        //os	平台id
        //ip	ip地址
        $fields = array('account as userid', 'role_id as roleid', 'name as account', 'create_time', 'last_login_ip as ip', 'package');
        $conditions['WHERE']['create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['create_time::<='] = $end_time * 1000;
        $conditions['WHERE']['package::IN'] = $package;
        $rs = (new RoleModel())->getRows($fields, $conditions['WHERE']);

        echo json_encode($rs);
    }

    //获取login记录
    //from	服务器id
    //userid	账号id
    //roleid	角色id
    //account	角色名
    //lev	级别
    //platform	渠道id
    //mac	设备地址
    //os	平台id
    //totalcash	总充值金额
    public function getRoleLogin($type, $start_time, $end_time, $package)
    {

        $loginModel = new LoginModel();

        $fields = array('r.account as userid', 'r.role_id as roleid', 'r.name as account', 'r.package as package', 'l.role_level as lev', 'l.logout_role_level as logout_lev', 'l.ip as ip', 'l.start_time', 'l.end_time', 'r.mac as mac');

        if ($type == 1) {
            $conditions['WHERE']['l.start_time::>='] = $start_time * 1000;
            $conditions['WHERE']['l.start_time::<='] = $end_time * 1000;
        } else {
            $conditions['WHERE']['l.end_time::>='] = $start_time * 1000;
            $conditions['WHERE']['l.end_time::<='] = $end_time * 1000;
        }

        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $loginModel->getRows($fields, $conditions['WHERE']);

        if ($rs) {
            foreach ($rs as $k => $row) {

                $time = $type == 1 ? $row['start_time'] : $row['end_time'];

                $time /= 1000;

                //获取充值总金额
                $totalcash = (new ChargeModel())->getChargeMoneyByRole($time, [$row['roleid']]);

                $rs[$k]['totalcash'] = $totalcash;
            }
        }

        echo json_encode($rs);
    }


    //获取角色升级记录
    public function getRoleLevUp($start_time, $end_time, $package)
    {
        $levelModel = new LevelModel();

        $levelModel->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'lv.role_id = r.role_id')
        );

        $fields = array('r.account as userid', 'r.role_id as roleid', 'r.name as account', 'r.package as package', 'lv.level as lev', 'r.vip_level as viplev', 'r.mac as mac', 'lv.create_time as create_time');

        $conditions['WHERE']['lv.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['lv.create_time::<='] = $end_time * 1000;
        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $levelModel->getRows($fields, $conditions['WHERE']);

        if ($rs) {
            foreach ($rs as $k => $row) {

                $time = $row['create_time'];

                $time /= 1000;

                //获取充值总金额
                $rs[$k]['totalcash'] = (new ChargeModel())->getChargeMoneyByRole($time, [$row['roleid']]);
            }
        }

        echo json_encode($rs);
    }

    //获取角色货币产出消耗记录
    //typeinfo	本次货币增加的途径
    //yuanbao	本次增加的元宝
    //type	货币类型
    public function getRoleYuanBao($type, $start_time, $end_time, $package)
    {
        $paymentModel = new PaymentModel();

        $paymentModel->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'p.role_id = r.role_id')
        );

        $fields = array('r.account as userid', 'r.role_id as roleid', 'r.name as account', 'r.mac as mac', 'r.package as package', 'p.create_time as create_time', 'p.money_type as money_type', 'p.coin_source as typeinfo', 'p.count as yuanbao', 'p.role_level as lev', 'p.balance as totalnum');

        $conditions['WHERE']['p.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['p.create_time::<='] = $end_time * 1000;
        $conditions['WHERE']['p.type'] = $type;
        $conditions['WHERE']['p.money_type::IN'] = [1, 2];
        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $paymentModel->getRows($fields, $conditions['WHERE']);

        if ($rs) {
            foreach ($rs as $k => $row) {

                $time = $row['create_time'];

                $time /= 1000;

                //获取充值总金额
                $rs[$k]['totalcash'] = (new ChargeModel())->getChargeMoneyByRole($time, [$row['roleid']]);
            }
        }

        echo json_encode($rs);
    }

    //item_id	物品id
    //item_type	物品类型id
    //item_count	物品个数
    //cash_type	货币类型
    //cash_need	本次元宝花费
    //order_id
    //cash_need1	本次绑定元宝花费
    //商城消费
    public function getShopTrade($start_time, $end_time, $package)
    {
        $mallRecordModel = new MallRecordModel(0);

        $mallRecordModel->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'mr.role_id = r.role_id')
        );

        $fields = array('r.account as userid', 'r.role_id as roleid', 'r.name as account', 'r.mac as mac', 'mr.role_level as lev', 'r.package as package', 'mr.item_id as item_id', 'mr.item_num as item_num', 'mr.buy_num as buy_num', 'mr.currency_id as cash_type', 'mr.monetary as cash_need', 'mr.create_time as create_time');

        $conditions['WHERE']['mr.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['mr.create_time::<='] = $end_time * 1000;
        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $mallRecordModel->getRows($fields, $conditions['WHERE']);

        echo json_encode($rs);
    }


    //角色快照
    //from	同前
    //userid	同前
    //roleid	同前
    //account	同前
    //platform	同前
    //mac	同前
    //os		同前
    //createtime	角色创建时间
    //lastlogintime	最近登录时间
    //totalonlinetime	总在线时长
    //dayonlinetime	当日在线时长
    //lev	角色等级
    //viplev	角色VIP等级
    //exp	经验数值
    //fight	战斗力数值
    //totalcash	累计付费金额
    //yuanbaoowned	剩余元宝数值
    //jinbiowned	剩余金币数值
    public function getRoleCharData($start_time, $end_time, $package)
    {
        $roleModel = new RoleModel();

        $fields = array('account as userid', 'role_id as roleid', 'name as account', 'mac as mac', 'role_level as lev', 'create_time', 'last_login_time as lastlogintime', 'vip_level as viplev', 'power as fight', 'gold as yuanbaoowned', 'coin as jinbiowned', 'package');
        $conditions = [];
        $conditions['WHERE']['package::IN'] = $package;

        $rs = $roleModel->getRows($fields, $conditions['WHERE']);

        if ($rs) {
            foreach ($rs as $k => $row) {
                //获取充值总金额
                $rs[$k]['totalcash'] = (new ChargeModel())->getChargeMoneyByRole(false, [$row['roleid']]);

                //获取当日在线时长
                $rs[$k]['dayonlinetime'] = (new LoginModel())->getRoleOlTime($row['roleid'], $start_time, $end_time);

                //获取角色在线总时长
                $rs[$k]['totalonlinetime'] = (new LoginModel())->getRoleOlTime($row['roleid']);
            }
        }

        echo json_encode($rs);
    }

    //from	同前
    //userid	同前
    //roleid	同前
    //account	同前
    //lev	同前
    //platform	同前
    //mac	同前
    //os		同前
    //itemid	物品id
    //count	物品数量
    //guid	物品唯一号
    //reason	原因
    //hint	预留字段
    //type	类型id
    //物品消耗产出
    public function getRoleGainLoseItemData($type, $start_time, $end_time, $package)
    {

        $cpModel = new CpModel();

        $cpModel->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 'cp.role_id = r.role_id')
        );

        $fields = array('r.account as userid', 'r.role_id as roleid', 'r.name as account', 'r.mac as mac', 'r.package as package', 'cp.role_level as lev', 'cp.source as reason', 'cp.item_id as itemid', 'cp.item_num as count', 'cp.create_time as create_time');

        $conditions['WHERE']['cp.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['cp.create_time::<='] = $end_time * 1000;
        $conditions['WHERE']['cp.type'] = $type;
        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $cpModel->getRows($fields, $conditions['WHERE']);

        echo json_encode($rs);
    }


    //充值日志
    public function getChargeData($start_time, $end_time, $package)
    {
        $chargeModel = new ChargeModel();

        $fields = array('r.account as userid', 'r.role_id as roleid', 'r.name as account', 'r.mac as mac', 'r.package as package', 'o.role_level as lev', 'o.money as cash', 'o.gold as yuanbao', 'o.order_num as id1', 'o.corder_num as id2', 'o.ip as ip', 'o.create_time as create_time');

        $conditions['WHERE']['o.create_time::>='] = $start_time;
        $conditions['WHERE']['o.create_time::<='] = $end_time;
        $conditions['WHERE']['o.status'] = 1;
        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $chargeModel->getRows($fields, $conditions['WHERE']);

        if ($rs) {
            foreach ($rs as $k => $row) {
                //获取充值总金额
                $rs[$k]['totalcash'] = $chargeModel->getChargeMoneyByRole($row['create_time'], [$row['roleid']]);
            }
        }

        echo json_encode($rs);
    }

    //聊天信息
    public function getTalkData($packages, $start_time, $end_time, $package)
    {
        $talkModel = new TalkDataModel();

        $talkModel->joinTable = array(
            'r' => array('name' => 'role', 'type' => 'LEFT', 'on' => 't.role_id = r.role_id')
        );

        $fields = array('t.account as platform_uid', 't.role_id as roleid', 't.role_name as rolename', 't.role_level as role_level', 't.ip as user_ip', 't.content as content', 't.type as chat_type', 'r.package as package');

        $conditions['WHERE']['r.package::IN'] = $packages;
        $conditions['WHERE']['t.create_time::>='] = $start_time * 1000;
        $conditions['WHERE']['t.create_time::<='] = $end_time * 1000;
        $conditions['WHERE']['r.package::IN'] = $package;

        $rs = $talkModel->getRows($fields, $conditions['WHERE']);

        echo json_encode($rs);
    }

}
