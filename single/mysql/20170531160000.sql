/*
Navicat MySQL Data Transfer

Source Server         : 192.168.2.248
Source Server Version : 50634
Source Host           : 192.168.2.248:3306
Source Database       : dcxj_248_248

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2017-05-17 01:14:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ny_acc_login
-- ----------------------------
DROP TABLE IF EXISTS `ny_acc_login`;
CREATE TABLE `ny_acc_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL,
  `session` varchar(100) DEFAULT NULL,
  `first` tinyint(2) DEFAULT '0' COMMENT '1首次登录 0非首次登录',
  `start_time` int(11) DEFAULT NULL,
  `end_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_arena
-- ----------------------------
DROP TABLE IF EXISTS `ny_arena`;
CREATE TABLE `ny_arena` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `enemy_id` double(50,0) DEFAULT NULL COMMENT '敌方',
  `num` tinyint(1) DEFAULT NULL COMMENT '场数',
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='竞技场';

-- ----------------------------
-- Table structure for ny_arpu
-- ----------------------------
DROP TABLE IF EXISTS `ny_arpu`;
CREATE TABLE `ny_arpu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `arpu` varchar(20) DEFAULT '0',
  `arppu` varchar(20) DEFAULT '0',
  `first_arpu` varchar(20) DEFAULT '0' COMMENT '首充arpu',
  `reg_arpu` varchar(20) DEFAULT '0' COMMENT '注册arpu',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_ban
-- ----------------------------
DROP TABLE IF EXISTS `ny_ban`;
CREATE TABLE `ny_ban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COMMENT '封禁内容',
  `type` tinyint(1) DEFAULT NULL COMMENT '封禁类型',
  `time` int(11) DEFAULT NULL COMMENT '封禁时间，单位（秒）',
  `reason` int(4) DEFAULT NULL COMMENT '封禁理由',
  `user` varchar(128) DEFAULT NULL COMMENT '操作的管理员',
  `other` mediumtext COMMENT '其它封禁理由',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '是否已解封 0未解封 1已解封',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_boss
-- ----------------------------
DROP TABLE IF EXISTS `ny_boss`;
CREATE TABLE `ny_boss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `attach` text COMMENT '归属',
  `boss_id` int(11) DEFAULT NULL COMMENT 'boss id',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='boss击杀';

-- ----------------------------
-- Table structure for ny_career
-- ----------------------------
DROP TABLE IF EXISTS `ny_career`;
CREATE TABLE `ny_career` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `career` int(4) DEFAULT NULL COMMENT '职业',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='转职时更新一条记录';

-- ----------------------------
-- Table structure for ny_charge_daily
-- ----------------------------
DROP TABLE IF EXISTS `ny_charge_daily`;
CREATE TABLE `ny_charge_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `money` int(11) DEFAULT '0' COMMENT '充值总额',
  `pay_num` int(11) DEFAULT '0' COMMENT '充值人数',
  `pay_times` int(11) DEFAULT '0' COMMENT '充值次数',
  `first_money` int(11) DEFAULT '0' COMMENT '首冲总额',
  `first_num` int(11) DEFAULT '0' COMMENT '首冲人数',
  `first_times` int(11) DEFAULT '0' COMMENT '首冲次数',
  `arpu` varchar(20) DEFAULT '0' COMMENT 'ARPU',
  `role_num` int(11) DEFAULT '0' COMMENT '创角数',
  `login_num` int(11) DEFAULT '0' COMMENT '登录数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_charge_new
-- ----------------------------
DROP TABLE IF EXISTS `ny_charge_new`;
CREATE TABLE `ny_charge_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `first_num` int(11) DEFAULT '0' COMMENT '首充人数',
  `new_num` int(11) DEFAULT '0' COMMENT '新增人数',
  `new_money` int(11) DEFAULT '0' COMMENT '新增充值金额',
  `new_percent` varchar(6) DEFAULT '0%' COMMENT '新增充值比例',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_choujiang
-- ----------------------------
DROP TABLE IF EXISTS `ny_choujiang`;
CREATE TABLE `ny_choujiang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '0法印 1装备',
  `multi` tinyint(2) DEFAULT NULL COMMENT '寻宝次数',
  `free` tinyint(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='寻宝';

-- ----------------------------
-- Table structure for ny_consume_produce
-- ----------------------------
DROP TABLE IF EXISTS `ny_consume_produce`;
CREATE TABLE `ny_consume_produce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名',
  `role_career` varchar(50) DEFAULT NULL COMMENT '角色职业',
  `role_level` int(11) DEFAULT NULL COMMENT '角色等级',
  `type` tinyint(4) DEFAULT NULL COMMENT '类型 0消耗 1产出',
  `source` int(11) DEFAULT NULL COMMENT '来源或去向',
  `item_id` double(50,0) DEFAULT NULL COMMENT '物品ID',
  `item_num` int(11) DEFAULT '1' COMMENT '物品数量',
  `item_info` mediumtext COMMENT '物品详细信息',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='所有物品的产出和消耗的记录';

-- ----------------------------
-- Table structure for ny_equip
-- ----------------------------
DROP TABLE IF EXISTS `ny_equip`;
CREATE TABLE `ny_equip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `part` tinyint(1) DEFAULT NULL COMMENT '部位',
  `level` int(4) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='装备强化\r\n1主武器\r\n2副武器\r\n3项链\r\n4左戒指\r\n5右戒指\r\n6头盔\r\n7衣服\r\n8腰带\r\n9护腿\r\n10鞋子\r\n11翅膀\r\n12守护\r\n';

-- ----------------------------
-- Table structure for ny_equip_replace
-- ----------------------------
DROP TABLE IF EXISTS `ny_equip_replace`;
CREATE TABLE `ny_equip_replace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `item_id` double(50,0) DEFAULT NULL,
  `type` int(1) DEFAULT NULL COMMENT '0穿上 1脱下',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_faction
-- ----------------------------
DROP TABLE IF EXISTS `ny_faction`;
CREATE TABLE `ny_faction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faction_id` varchar(50) DEFAULT NULL COMMENT '帮派id',
  `name` varchar(50) DEFAULT NULL COMMENT '帮派名称',
  `role_num` int(4) DEFAULT '0' COMMENT '帮派人数',
  `level` int(4) DEFAULT '1' COMMENT '帮派等级',
  `leader_role_id` double(50,0) DEFAULT NULL COMMENT '帮主的角色id',
  `create_time` int(11) DEFAULT NULL COMMENT 'delete为0时为帮派创建时间，delete为1时为帮派删除时间',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '是否已删除，1已删除，0未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='帮派';

-- ----------------------------
-- Table structure for ny_faction_bag
-- ----------------------------
DROP TABLE IF EXISTS `ny_faction_bag`;
CREATE TABLE `ny_faction_bag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faction_id` varchar(50) DEFAULT NULL COMMENT '帮派id',
  `type` tinyint(4) DEFAULT NULL COMMENT '类型 0捐献 1领取',
  `role_id` double(50,0) DEFAULT NULL COMMENT '捐献或者领取装备的帮派成员的id',
  `item_id` double(50,0) DEFAULT NULL COMMENT '装备id',
  `item` mediumtext COMMENT '物品详细信息',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_follow
-- ----------------------------
DROP TABLE IF EXISTS `ny_follow`;
CREATE TABLE `ny_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `pay_num` int(11) DEFAULT '0' COMMENT '当日付费人数',
  `money` int(11) DEFAULT '0' COMMENT '当日付费总额',
  `one` varchar(11) DEFAULT '-' COMMENT '后续第1天的付费金额（后续第1天的付费金额 / 付费总额）',
  `two` varchar(11) DEFAULT '-' COMMENT '后续第2天的付费金额（后续第1天的付费金额 / 付费总额）',
  `three` varchar(11) DEFAULT '-' COMMENT '后续第3天的付费金额（后续第1天的付费金额 / 付费总额）',
  `four` varchar(11) DEFAULT '-' COMMENT '后续第4天的付费金额（后续第1天的付费金额 / 付费总额）',
  `five` varchar(11) DEFAULT '-' COMMENT '后续第5天的付费金额（后续第1天的付费金额 / 付费总额）',
  `six` varchar(11) DEFAULT '-' COMMENT '后续第6天的付费金额（后续第1天的付费金额 / 付费总额）',
  `seven` varchar(11) DEFAULT '-' COMMENT '后续第7天的付费金额（后续第1天的付费金额 / 付费总额）',
  `fourteen` varchar(11) DEFAULT '-' COMMENT '后续第14天的付费金额（后续第1天的付费金额 / 付费总额）',
  `thirty` varchar(11) DEFAULT '-' COMMENT '后续第30天的付费金额（后续第1天的付费金额 / 付费总额）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='后续付费';

-- ----------------------------
-- Table structure for ny_follow_new
-- ----------------------------
DROP TABLE IF EXISTS `ny_follow_new`;
CREATE TABLE `ny_follow_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `new_num` int(11) DEFAULT '0' COMMENT '新增人数',
  `money` int(11) DEFAULT '0' COMMENT '新增充值金额',
  `one` varchar(11) DEFAULT '-' COMMENT '后续第1天付费数据',
  `two` varchar(11) DEFAULT '-' COMMENT '后续第2天付费数据',
  `three` varchar(11) DEFAULT '-' COMMENT '后续第3天付费数据',
  `four` varchar(11) DEFAULT '-' COMMENT '后续第4天付费数据',
  `five` varchar(11) DEFAULT '-' COMMENT '后续第5天付费数据',
  `six` varchar(11) DEFAULT '-' COMMENT '后续第6天付费数据',
  `seven` varchar(11) DEFAULT '-' COMMENT '后续第7天付费数据',
  `fourteen` varchar(11) DEFAULT '-' COMMENT '后续第14天付费数据',
  `thirty` varchar(11) DEFAULT '-' COMMENT '后续第30天付费数据',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='新增后续付费，付费数据的格式是： 付费金额(付费人数)';

-- ----------------------------
-- Table structure for ny_fuben
-- ----------------------------
DROP TABLE IF EXISTS `ny_fuben`;
CREATE TABLE `ny_fuben` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `fuben_id` int(11) DEFAULT NULL COMMENT '副本id',
  `stars` tinyint(1) DEFAULT NULL COMMENT '星级',
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='副本';

-- ----------------------------
-- Table structure for ny_gem
-- ----------------------------
DROP TABLE IF EXISTS `ny_gem`;
CREATE TABLE `ny_gem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `red_level` int(11) DEFAULT '0',
  `green_level` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='宝石';

-- ----------------------------
-- Table structure for ny_gift
-- ----------------------------
DROP TABLE IF EXISTS `ny_gift`;
CREATE TABLE `ny_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_id` int(11) DEFAULT NULL COMMENT '礼包id',
  `code` varchar(16) DEFAULT NULL COMMENT '兑换码',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名',
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色id',
  `create_time` int(11) DEFAULT NULL COMMENT '领取时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_hook
-- ----------------------------
DROP TABLE IF EXISTS `ny_hook`;
CREATE TABLE `ny_hook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `role_level` int(11) DEFAULT NULL COMMENT '登录时等级',
  `role_name` varchar(50) DEFAULT NULL COMMENT '登录时角色名',
  `role_career` int(11) DEFAULT NULL COMMENT '登录时职业',
  `start_time` int(11) DEFAULT NULL COMMENT '登录时间',
  `end_time` int(11) DEFAULT NULL,
  `first` tinyint(4) DEFAULT '0' COMMENT '1 首次登陆 0非首次登陆',
  `ip` varchar(100) DEFAULT NULL COMMENT '登录ip',
  `session` varchar(100) DEFAULT NULL COMMENT '登录跟登出对应的会话id',
  PRIMARY KEY (`id`),
  KEY `create_time` (`start_time`) USING BTREE,
  KEY `role_level` (`role_level`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `role_career` (`role_career`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='角色离线登录日志';

-- ----------------------------
-- Table structure for ny_item
-- ----------------------------
DROP TABLE IF EXISTS `ny_item`;
CREATE TABLE `ny_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `get_num` bigint(20) DEFAULT NULL COMMENT '总产生物品数',
  `get_day_num` int(11) DEFAULT '0' COMMENT '当日产生物品数',
  `get_account_num` int(11) DEFAULT '0' COMMENT '参与产生数',
  `get_avg_num` int(11) DEFAULT '0' COMMENT '人均产生',
  `use_num` bigint(20) DEFAULT NULL COMMENT '总消耗物品数',
  `use_day_num` int(11) DEFAULT '0' COMMENT '当日消耗物品数',
  `use_account_num` int(11) DEFAULT '0' COMMENT '当日参与消耗数',
  `use_avg_num` int(11) DEFAULT '0' COMMENT '人均消耗',
  `percent` varchar(6) DEFAULT '0%' COMMENT '滞留率',
  `type` tinyint(4) DEFAULT NULL COMMENT '1元宝 2绑定元宝 3铜钱 4道具 5积分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='物品的消耗和产出的表包括铜钱，元宝，绑定元宝，道具';

-- ----------------------------
-- Table structure for ny_level
-- ----------------------------
DROP TABLE IF EXISTS `ny_level`;
CREATE TABLE `ny_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `level` int(6) DEFAULT NULL COMMENT '等级',
  `create_time` int(11) DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='玩家等级';

-- ----------------------------
-- Table structure for ny_login
-- ----------------------------
DROP TABLE IF EXISTS `ny_login`;
CREATE TABLE `ny_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `role_level` int(11) DEFAULT NULL COMMENT '登录时等级',
  `role_name` varchar(50) DEFAULT NULL COMMENT '登录时角色名',
  `role_career` int(11) DEFAULT NULL COMMENT '登录时职业',
  `start_time` int(11) DEFAULT '0' COMMENT '登录时间',
  `end_time` int(11) DEFAULT '0',
  `first` tinyint(4) DEFAULT '0' COMMENT '1 首次登陆 0非首次登陆',
  `ip` varchar(100) DEFAULT NULL COMMENT '登录ip',
  `session` varchar(100) DEFAULT NULL COMMENT '登录跟登出对应的会话id',
  PRIMARY KEY (`id`),
  KEY `create_time` (`start_time`) USING BTREE,
  KEY `role_level` (`role_level`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `role_career` (`role_career`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='角色离线登录日志';

-- ----------------------------
-- Table structure for ny_login_daily
-- ----------------------------
DROP TABLE IF EXISTS `ny_login_daily`;
CREATE TABLE `ny_login_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `lg_num` int(8) DEFAULT NULL COMMENT '登录人数',
  `lg_times` int(11) DEFAULT NULL COMMENT '登录次数',
  `avg_lg_times` varchar(11) DEFAULT NULL COMMENT '平均登录次数',
  `old_num` int(11) DEFAULT NULL COMMENT '老玩家数',
  `avg_old_oltime` int(11) DEFAULT '0' COMMENT '老玩家平均在线时长',
  `old_remain` varchar(6) DEFAULT NULL COMMENT '老玩家留存',
  `atv_num` int(11) DEFAULT NULL COMMENT '活跃玩家数',
  `loy_num` int(11) DEFAULT NULL COMMENT '忠实玩家数',
  `reg_num` int(11) DEFAULT NULL COMMENT '总注册量',
  `device_num` int(11) DEFAULT NULL COMMENT '老玩家设备数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_loss
-- ----------------------------
DROP TABLE IF EXISTS `ny_loss`;
CREATE TABLE `ny_loss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `loss_num` int(11) DEFAULT '0' COMMENT '流失人数',
  `reg_anum` int(11) DEFAULT '0' COMMENT '当天注册玩家数',
  `reg_rnum` int(11) DEFAULT '0' COMMENT '当天创角数',
  `five_min` int(11) DEFAULT '0' COMMENT '5分钟流失人数',
  `thirty_min` int(11) DEFAULT '0' COMMENT '30分钟流失人数',
  `one_hour` int(11) DEFAULT '0' COMMENT '1小时流失人数',
  `three` int(11) DEFAULT '0' COMMENT '三天流失人数',
  `five` int(11) DEFAULT '0' COMMENT '5天流失人数',
  `seven` int(11) DEFAULT '0' COMMENT '7天流失人数',
  `ten` int(11) DEFAULT '0' COMMENT '10天流失人数',
  `fifteen` int(11) DEFAULT '0' COMMENT '15天流失人数',
  `thirty` int(11) DEFAULT '0' COMMENT '30天流失人数',
  `next` varchar(6) DEFAULT '0%' COMMENT '次日流失率',
  `standard` varchar(6) DEFAULT '0%' COMMENT '标准流失率',
  `threeloss` varchar(6) DEFAULT '0%' COMMENT '三日流失率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流失统计';

-- ----------------------------
-- Table structure for ny_ltv
-- ----------------------------
DROP TABLE IF EXISTS `ny_ltv`;
CREATE TABLE `ny_ltv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL COMMENT '日期',
  `type` tinyint(2) NOT NULL COMMENT 'ltv类型 1创角ltv 2注册ltv',
  `num` int(11) DEFAULT '0' COMMENT 'type为1时是当日创建角色数，type为2时是当日注册人数',
  `one` varchar(11) DEFAULT '-' COMMENT '后续1天付费金额 / 创角数或注册数',
  `two` varchar(11) DEFAULT '-' COMMENT '后续2天付费金额 / 创角数或注册数',
  `three` varchar(11) DEFAULT '-' COMMENT '后续3天付费金额 / 创角数或注册数',
  `four` varchar(11) DEFAULT '-' COMMENT '后续4天付费金额 / 创角数或注册数',
  `five` varchar(11) DEFAULT '-' COMMENT '后续5天付费金额 / 创角数或注册数',
  `six` varchar(11) DEFAULT '-' COMMENT '后续6天付费金额 / 创角数或注册数',
  `seven` varchar(11) DEFAULT '-' COMMENT '后续7天付费金额 / 创角数或注册数',
  `fourteen` varchar(11) DEFAULT '-' COMMENT '后续14天付费金额 / 创角数或注册数',
  `thirty` varchar(11) DEFAULT '-' COMMENT '后续30天付费金额 / 创角数或注册数',
  `sixty` varchar(11) DEFAULT '-' COMMENT '后续60天付费金额 / 创角数或注册数',
  `ninety` varchar(11) DEFAULT '-' COMMENT '后续90天付费金额 / 创角数或注册数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='LTV统计';

-- ----------------------------
-- Table structure for ny_online_hook
-- ----------------------------
DROP TABLE IF EXISTS `ny_online_hook`;
CREATE TABLE `ny_online_hook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_num` int(11) NOT NULL DEFAULT '0' COMMENT '在线角色数量',
  `hook_num` int(11) NOT NULL DEFAULT '0' COMMENT '脱机挂机角色数',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_order
-- ----------------------------
DROP TABLE IF EXISTS `ny_order`;
CREATE TABLE `ny_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `order_num` varchar(50) DEFAULT NULL COMMENT '订单号',
  `corder_num` varchar(50) DEFAULT NULL COMMENT '渠道订单号',
  `money` int(11) DEFAULT NULL COMMENT '充值金额',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `role_name` varchar(50) DEFAULT NULL COMMENT '充值时角色名',
  `role_level` int(11) DEFAULT NULL COMMENT '充值时角色等级',
  `role_career` int(4) DEFAULT NULL COMMENT '充值时职业',
  `gold` int(11) DEFAULT NULL COMMENT '获得的元宝数量',
  `status` int(4) DEFAULT NULL COMMENT '订单状态，1支付成功，2支付失败，3错误订单',
  `first` int(4) DEFAULT NULL COMMENT '是否首冲，1是首冲，0不是首冲',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_num` (`order_num`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `role_level` (`role_level`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `money` (`money`) USING BTREE,
  KEY `gold` (`gold`) USING BTREE,
  KEY `role_career` (`role_career`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Table structure for ny_payment
-- ----------------------------
DROP TABLE IF EXISTS `ny_payment`;
CREATE TABLE `ny_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名',
  `role_career` varchar(50) DEFAULT NULL COMMENT '角色职业',
  `role_level` int(5) DEFAULT NULL COMMENT '角色等级',
  `type` tinyint(4) DEFAULT NULL COMMENT '货币 0消耗 1产出',
  `coin_source` tinyint(4) DEFAULT NULL COMMENT '货币来源',
  `copper` bigint(20) DEFAULT '0' COMMENT '铜钱',
  `gold` bigint(20) DEFAULT '0' COMMENT '元宝',
  `bind_gold` bigint(20) DEFAULT '0' COMMENT '绑定元宝',
  `sorce` bigint(20) DEFAULT '0' COMMENT '积分',
  `bag_copper` bigint(20) NOT NULL DEFAULT '0' COMMENT '剩余铜钱',
  `bag_gold` bigint(20) NOT NULL DEFAULT '0' COMMENT '剩余元宝',
  `bag_bind_gold` bigint(20) NOT NULL DEFAULT '0' COMMENT '剩余绑宝',
  `bag_sorce` bigint(20) NOT NULL DEFAULT '0' COMMENT '剩余积分',
  `money_type` int(1) NOT NULL COMMENT '1 铜钱 2元宝 3绑元 4积分',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_pet
-- ----------------------------
DROP TABLE IF EXISTS `ny_pet`;
CREATE TABLE `ny_pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `level` int(4) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='法器';

-- ----------------------------
-- Table structure for ny_rank
-- ----------------------------
DROP TABLE IF EXISTS `ny_rank`;
CREATE TABLE `ny_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rank` int(3) DEFAULT NULL,
  `contents` varchar(255) DEFAULT NULL,
  `type` int(1) DEFAULT NULL COMMENT '0等级 1坐骑 2宝石 3战力 4充值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_remain
-- ----------------------------
DROP TABLE IF EXISTS `ny_remain`;
CREATE TABLE `ny_remain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `pay_num` int(11) DEFAULT '0' COMMENT '当日付费人数',
  `one` varchar(11) DEFAULT '-' COMMENT '后续第1天登录人数 / 付费人数',
  `two` varchar(11) DEFAULT '-' COMMENT '后续第2天登录人数 / 付费人数',
  `three` varchar(11) DEFAULT '-' COMMENT '后续第3天登录人数 / 付费人数',
  `four` varchar(11) DEFAULT '-' COMMENT '后续第4天登录人数 / 付费人数',
  `five` varchar(11) DEFAULT '-' COMMENT '后续第5天登录人数 / 付费人数',
  `six` varchar(11) DEFAULT '-' COMMENT '后续第6天登录人数 / 付费人数',
  `seven` varchar(11) DEFAULT '-' COMMENT '后续第7天登录人数 / 付费人数',
  `fourteen` varchar(11) DEFAULT '-' COMMENT '后续第14天登录人数 / 付费人数',
  `thirty` varchar(11) DEFAULT '-' COMMENT '后续第30天登录人数 / 付费人数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='付费留存率';

-- ----------------------------
-- Table structure for ny_ride
-- ----------------------------
DROP TABLE IF EXISTS `ny_ride`;
CREATE TABLE `ny_ride` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `ride_class` tinyint(4) DEFAULT NULL COMMENT '坐骑阶级',
  `ride_level` int(4) DEFAULT NULL COMMENT '坐骑等级',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='坐骑，升级时写记录';

-- ----------------------------
-- Table structure for ny_role
-- ----------------------------
DROP TABLE IF EXISTS `ny_role`;
CREATE TABLE `ny_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名',
  `career` int(4) DEFAULT NULL COMMENT '角色当前职业',
  `create_time` int(11) NOT NULL COMMENT '角色创建时间',
  PRIMARY KEY (`id`),
  KEY `create_time` (`create_time`) USING BTREE,
  KEY `name` (`name`),
  KEY `career` (`career`),
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_role_daily
-- ----------------------------
DROP TABLE IF EXISTS `ny_role_daily`;
CREATE TABLE `ny_role_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `create_time` int(11) DEFAULT NULL COMMENT '创号时间',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名字',
  `career` int(4) DEFAULT NULL COMMENT '职业',
  `level` int(4) DEFAULT '1' COMMENT '当前等级',
  `gold` int(11) DEFAULT '0' COMMENT '当前持有元宝',
  `bind_gold` int(11) DEFAULT '0' COMMENT '当前持有绑定元宝',
  `copper` int(20) DEFAULT '0' COMMENT '当前持有铜钱',
  `sorce` int(11) DEFAULT '0' COMMENT '当前持有积分',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后登录时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_seven_remain
-- ----------------------------
DROP TABLE IF EXISTS `ny_seven_remain`;
CREATE TABLE `ny_seven_remain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `role_num` int(11) DEFAULT '0' COMMENT '创角数',
  `two` varchar(15) DEFAULT '-' COMMENT '第2天留存',
  `three` varchar(15) DEFAULT '-' COMMENT '第3天留存',
  `four` varchar(15) DEFAULT '-' COMMENT '第4天留存',
  `five` varchar(15) DEFAULT '-' COMMENT '第5天留存',
  `six` varchar(15) DEFAULT '-' COMMENT '第6天留存',
  `seven` varchar(15) DEFAULT '-' COMMENT '第7天留存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_shenbing
-- ----------------------------
DROP TABLE IF EXISTS `ny_shenbing`;
CREATE TABLE `ny_shenbing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '1生命 2攻击 3暴击',
  `level` int(4) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='神兵';

-- ----------------------------
-- Table structure for ny_supplicate
-- ----------------------------
DROP TABLE IF EXISTS `ny_supplicate`;
CREATE TABLE `ny_supplicate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL,
  `free` tinyint(1) DEFAULT NULL COMMENT '0免费 1不是免费',
  `type` tinyint(1) DEFAULT NULL COMMENT '1铜钱 2经验',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='祈愿';

-- ----------------------------
-- Table structure for ny_task
-- ----------------------------
DROP TABLE IF EXISTS `ny_task`;
CREATE TABLE `ny_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(32) DEFAULT NULL COMMENT '任务ID',
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `type` tinyint(4) DEFAULT NULL COMMENT '任务类型 1主线任务 2支线任务 4赏金任务',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态 0已接受任务 1已完成任务',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  `exp` tinyint(1) DEFAULT '0' COMMENT '双倍经验',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ny_total_daily
-- ----------------------------
DROP TABLE IF EXISTS `ny_total_daily`;
CREATE TABLE `ny_total_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `reg_num` int(11) DEFAULT NULL COMMENT '注册数',
  `role_num` int(11) DEFAULT NULL COMMENT '创角数',
  `login_num` int(11) DEFAULT NULL COMMENT '登录数',
  `atv_num` int(11) DEFAULT NULL COMMENT '活跃玩家数',
  `old_num` int(11) DEFAULT NULL COMMENT '老玩家数',
  `pay_num` int(11) DEFAULT NULL COMMENT '充值人数',
  `money` int(11) DEFAULT NULL COMMENT '充值金额',
  `lc_percent` varchar(6) DEFAULT '' COMMENT '活跃付费率',
  `arpu` varchar(11) DEFAULT NULL COMMENT '总arpu',
  `first_pay_num` int(11) DEFAULT NULL COMMENT '新充值人数',
  `first_money` int(11) DEFAULT NULL COMMENT '新充值金额',
  `first_percent` varchar(6) DEFAULT NULL COMMENT '新增付费率',
  `first_arpu` varchar(11) DEFAULT NULL COMMENT '新充值ARPU',
  `old_pay_num` int(11) DEFAULT NULL COMMENT '老充值人数',
  `old_money` int(11) DEFAULT NULL COMMENT '老充值金额',
  `old_percent` varchar(6) DEFAULT NULL COMMENT '老付费率',
  `old_arpu` varchar(11) DEFAULT NULL COMMENT '老ARPU',
  `reg_arpu` varchar(11) DEFAULT NULL COMMENT '注册ARPU',
  `max_num` int(11) DEFAULT NULL,
  `avg_num` int(11) DEFAULT NULL,
  `two` varchar(6) DEFAULT '-' COMMENT '次日留存率',
  `three` varchar(6) DEFAULT '-' COMMENT '三日留存率',
  `four` varchar(6) DEFAULT '-' COMMENT '四日留存率',
  `five` varchar(6) DEFAULT '-' COMMENT '五日留存率',
  `six` varchar(6) DEFAULT '-' COMMENT '六日留存率',
  `seven` varchar(6) DEFAULT '-' COMMENT '七日留存率',
  `fifteen` varchar(6) DEFAULT '-' COMMENT '十五日留存率',
  `thirty` varchar(6) DEFAULT '-' COMMENT '三十日留存率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ny_vip
-- ----------------------------
DROP TABLE IF EXISTS `ny_vip`;
CREATE TABLE `ny_vip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `level` int(11) DEFAULT NULL COMMENT 'vip等级',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `day` int(11) DEFAULT '30' COMMENT '持续天数 30天 90天 180天',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role_attr
-- ----------------------------
DROP TABLE IF EXISTS `role_attr`;
CREATE TABLE `role_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) DEFAULT NULL COMMENT '角色ID',
  `level` int(11) DEFAULT NULL COMMENT '角色等级',
  `power` int(11) DEFAULT NULL COMMENT '当前力量',
  `strength` int(11) DEFAULT NULL COMMENT '当前体魄',
  `agile` int(11) DEFAULT NULL COMMENT '当前敏捷',
  `wisdom` int(11) DEFAULT NULL COMMENT '当前智慧',
  `attrfree` int(11) DEFAULT NULL COMMENT '当前自由点',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ny_acc_total_daily`;
CREATE TABLE `ny_acc_total_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `reg_num` int(11) DEFAULT NULL COMMENT '注册数',
  `login_num` int(11) DEFAULT NULL COMMENT '登录数',
  `old_num` int(11) DEFAULT NULL COMMENT '老玩家数',
  `pay_num` int(11) DEFAULT NULL COMMENT '充值人数',
  `money` int(11) DEFAULT NULL COMMENT '充值金额',
  `lc_percent` varchar(6) DEFAULT '' COMMENT '活跃付费率',
  `arpu` varchar(11) DEFAULT NULL COMMENT '总arpu',
  `first_pay_num` int(11) DEFAULT NULL COMMENT '新充值人数',
  `first_money` int(11) DEFAULT NULL COMMENT '新充值金额',
  `first_percent` varchar(6) DEFAULT NULL COMMENT '新增付费率',
  `first_arpu` varchar(11) DEFAULT NULL COMMENT '新充值ARPU',
  `old_pay_num` int(11) DEFAULT NULL COMMENT '老充值人数',
  `old_money` int(11) DEFAULT NULL COMMENT '老充值金额',
  `old_percent` varchar(6) DEFAULT NULL COMMENT '老付费率',
  `old_arpu` varchar(11) DEFAULT NULL COMMENT '老ARPU',
  `reg_arpu` varchar(11) DEFAULT NULL COMMENT '注册ARPU',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

DROP TABLE IF EXISTS `ny_account_seven_remain`;
CREATE TABLE `ny_account_seven_remain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `account_num` int(11) DEFAULT '0' COMMENT '创号数',
  `two` varchar(15) DEFAULT '-' COMMENT '第2天留存',
  `three` varchar(15) DEFAULT '-' COMMENT '第3天留存',
  `four` varchar(15) DEFAULT '-' COMMENT '第4天留存',
  `five` varchar(15) DEFAULT '-' COMMENT '第5天留存',
  `six` varchar(15) DEFAULT '-' COMMENT '第6天留存',
  `seven` varchar(15) DEFAULT '-' COMMENT '第7天留存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

