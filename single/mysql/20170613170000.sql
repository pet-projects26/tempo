/*增加角色留存8~30天的字段*/
ALTER TABLE ny_seven_remain ADD eight varchar(15) DEFAULT '-' COMMENT '第8天留存';
ALTER TABLE ny_seven_remain ADD nine  varchar(15) DEFAULT '-' COMMENT '第9天留存';
ALTER TABLE ny_seven_remain ADD ten  varchar(15) DEFAULT '-' COMMENT '第10天留存';
ALTER TABLE ny_seven_remain ADD eleven  varchar(15) DEFAULT '-' COMMENT '第11天留存';
ALTER TABLE ny_seven_remain ADD twelve  varchar(15) DEFAULT '-' COMMENT '第12天留存';
ALTER TABLE ny_seven_remain ADD thirteen  varchar(15) DEFAULT '-' COMMENT '第13天留存';
ALTER TABLE ny_seven_remain ADD fourteen  varchar(15) DEFAULT '-' COMMENT '第14天留存';
ALTER TABLE ny_seven_remain ADD fifteen  varchar(15) DEFAULT '-' COMMENT '第15天留存';
ALTER TABLE ny_seven_remain ADD sixteen  varchar(15) DEFAULT '-' COMMENT '第16天留存';
ALTER TABLE ny_seven_remain ADD seventeen  varchar(15) DEFAULT '-' COMMENT '第17天留存';
ALTER TABLE ny_seven_remain ADD eighteen  varchar(15) DEFAULT '-' COMMENT '第18天留存';
ALTER TABLE ny_seven_remain ADD nineteen  varchar(15) DEFAULT '-' COMMENT '第19天留存';
ALTER TABLE ny_seven_remain ADD twenty varchar(15) DEFAULT '-' COMMENT '第20天留存';
ALTER TABLE ny_seven_remain ADD twenty_one varchar(15) DEFAULT '-' COMMENT '第21天留存';
ALTER TABLE ny_seven_remain ADD twenty_two  varchar(15) DEFAULT '-' COMMENT '第22天留存';
ALTER TABLE ny_seven_remain ADD twenty_three  varchar(15) DEFAULT '-' COMMENT '第23天留存';
ALTER TABLE ny_seven_remain ADD twenty_four  varchar(15) DEFAULT '-' COMMENT '第24天留存';
ALTER TABLE ny_seven_remain ADD twenty_five  varchar(15) DEFAULT '-' COMMENT '第25天留存';
ALTER TABLE ny_seven_remain ADD twenty_six  varchar(15) DEFAULT '-' COMMENT '第26天留存';
ALTER TABLE ny_seven_remain ADD twenty_seven  varchar(15) DEFAULT '-' COMMENT '第27天留存';
ALTER TABLE ny_seven_remain ADD twenty_eight  varchar(15) DEFAULT '-' COMMENT '第28天留存';
ALTER TABLE ny_seven_remain ADD twenty_nine  varchar(15) DEFAULT '-' COMMENT '第29天留存';
ALTER TABLE ny_seven_remain ADD thirty  varchar(15) DEFAULT '-' COMMENT '第30天留存';



/*增加帐号留存8~30天的字段*/
ALTER TABLE ny_account_seven_remain ADD eight varchar(15) DEFAULT '-' COMMENT '第8天留存';
ALTER TABLE ny_account_seven_remain ADD nine  varchar(15) DEFAULT '-' COMMENT '第9天留存';
ALTER TABLE ny_account_seven_remain ADD ten  varchar(15) DEFAULT '-' COMMENT '第10天留存';
ALTER TABLE ny_account_seven_remain ADD eleven  varchar(15) DEFAULT '-' COMMENT '第11天留存';
ALTER TABLE ny_account_seven_remain ADD twelve  varchar(15) DEFAULT '-' COMMENT '第12天留存';
ALTER TABLE ny_account_seven_remain ADD thirteen  varchar(15) DEFAULT '-' COMMENT '第13天留存';
ALTER TABLE ny_account_seven_remain ADD fourteen  varchar(15) DEFAULT '-' COMMENT '第14天留存';
ALTER TABLE ny_account_seven_remain ADD fifteen  varchar(15) DEFAULT '-' COMMENT '第15天留存';
ALTER TABLE ny_account_seven_remain ADD sixteen  varchar(15) DEFAULT '-' COMMENT '第16天留存';
ALTER TABLE ny_account_seven_remain ADD seventeen  varchar(15) DEFAULT '-' COMMENT '第17天留存';
ALTER TABLE ny_account_seven_remain ADD eighteen  varchar(15) DEFAULT '-' COMMENT '第18天留存';
ALTER TABLE ny_account_seven_remain ADD nineteen  varchar(15) DEFAULT '-' COMMENT '第19天留存';
ALTER TABLE ny_account_seven_remain ADD twenty varchar(15) DEFAULT '-' COMMENT '第20天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_one varchar(15) DEFAULT '-' COMMENT '第21天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_two  varchar(15) DEFAULT '-' COMMENT '第22天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_three  varchar(15) DEFAULT '-' COMMENT '第23天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_four  varchar(15) DEFAULT '-' COMMENT '第24天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_five  varchar(15) DEFAULT '-' COMMENT '第25天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_six  varchar(15) DEFAULT '-' COMMENT '第26天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_seven  varchar(15) DEFAULT '-' COMMENT '第27天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_eight  varchar(15) DEFAULT '-' COMMENT '第28天留存';
ALTER TABLE ny_account_seven_remain ADD twenty_nine  varchar(15) DEFAULT '-' COMMENT '第29天留存';
ALTER TABLE ny_account_seven_remain ADD thirty  varchar(15) DEFAULT '-' COMMENT '第30天留存';



/*删除旧的item表，新增更改过的item表*/
DROP TABLE IF EXISTS `ny_item`;
CREATE TABLE `ny_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL COMMENT '日期',
  `use_num` bigint (11) DEFAULT '0' COMMENT '消耗货币',
  `use_role_num` int(11) DEFAULT '0' COMMENT '参与消耗的角色数',
  `get_num` bigint (11) DEFAULT '0' COMMENT '产出货币',
  `get_role_num` int(11) DEFAULT '0' COMMENT '参与产出的角色数',
  `inventory` varchar(20) DEFAULT '0' COMMENT '库存',
  `type` int(1) DEFAULT NULL COMMENT '1元宝 2绑定元宝 3铜钱 4道具 5积分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产出消耗表';
