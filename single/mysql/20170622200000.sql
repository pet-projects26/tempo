
ALTER TABLE `ny_role`
MODIFY COLUMN `role_id`  double(50,0) NOT NULL DEFAULT 0 COMMENT '角色ID' AFTER `account`,
MODIFY COLUMN `name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL  DEFAULT ''  COMMENT '角色名' AFTER `role_id`,
MODIFY COLUMN `career`  int(4) NOT NULL COMMENT '角色当前职业' AFTER `name`,
ADD COLUMN `package`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '包号' AFTER `account`,
ADD COLUMN `last_login_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间' AFTER `career`,
ADD COLUMN `last_logout_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间' AFTER `last_login_time`,
ADD COLUMN `last_login_ip`  varchar(20) NOT NULL DEFAULT '' COMMENT '最后登录时间' AFTER `last_logout_time`;

ALTER TABLE `ny_role` CHANGE `name` `name` VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci DEFAULT ''  NOT NULL  COMMENT '角色名';
ALTER TABLE `ny_role` CHANGE `career` `career` INT(4) DEFAULT 0  NOT NULL  COMMENT '角色当前职业';
ALTER TABLE `ny_role` MODIFY COLUMN `account`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '账号' AFTER `id`;