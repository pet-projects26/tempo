CREATE TABLE `ny_friendlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` double(50,0) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `other_role_id` double(50,0) unsigned NOT NULL DEFAULT '0' COMMENT '好友ID',
  `initmacy` tinyint(2) NOT NULL DEFAULT '0' COMMENT '亲密度',
  `nexusid` tinyint(2) NOT NULL DEFAULT '0' COMMENT '关系ID 1.夫妻 2.结拜 3.情侣 4.师徒 5.仇人 6.好友 (6开始为自定义编号)',
  `nexusname` varchar(20) NOT NULL DEFAULT '' COMMENT '关系名称',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '建立时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

ALTER TABLE `ny_role`  ADD COLUMN `faction_id` varchar(50) NOT NULL DEFAULT '' COMMENT '帮派id';
ALTER TABLE `ny_role`  ADD COLUMN `faction_name` varchar(50) NOT NULL DEFAULT '' COMMENT '帮派名称';
ALTER TABLE `ny_role`  ADD COLUMN `position` int(11) NOT NULL DEFAULT 0 COMMENT '职位ID';
ALTER TABLE `ny_role`  ADD COLUMN `position_name` varchar(50) NOT NULL DEFAULT '' COMMENT '职位名称';


ALTER TABLE `ny_role`  ADD COLUMN `gold` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '元宝';
ALTER TABLE `ny_role`  ADD COLUMN `bind_gold` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '绑定元宝';
ALTER TABLE `ny_role`  ADD COLUMN `coin` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '铜币';
ALTER TABLE `ny_role`  ADD COLUMN `power` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '战力';
ALTER TABLE `ny_role`  ADD COLUMN `vip_level` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'vip等级';

ALTER TABLE `ny_role` CHANGE `position` `position` INT(11) UNSIGNED DEFAULT 0  NOT NULL  COMMENT '职位';



DROP TABLE IF EXISTS `ny_feedback_reply`;
CREATE TABLE `ny_feedback_reply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '反馈ID',
  `reply` varchar(500) NOT NULL DEFAULT '' COMMENT '回复',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '回复内容',
  KEY `id` (`id`),
  KEY `fid` (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='玩家反馈回复表';


DROP TABLE IF EXISTS `ny_feedback`;
CREATE TABLE `ny_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '类型，1：意见，2BUG',
  `account` varchar(200) NOT NULL DEFAULT '' COMMENT '帐号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_id` double(50,0) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(500) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否回复 0：否1:是',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `role_id` (`role_id`,`name`,`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='玩家反馈表';


ALTER TABLE `ny_role`
MODIFY COLUMN `role_id`  double(50,0) NOT NULL DEFAULT 0 COMMENT '角色ID' AFTER `account`,
MODIFY COLUMN `name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL  DEFAULT ''  COMMENT '角色名' AFTER `role_id`,
MODIFY COLUMN `career`  int(4) NOT NULL COMMENT '角色当前职业' AFTER `name`,
ADD COLUMN `package`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '包号' AFTER `account`,
ADD COLUMN `last_login_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间' AFTER `career`,
ADD COLUMN `last_logout_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间' AFTER `last_login_time`,
ADD COLUMN `last_login_ip`  varchar(20) NOT NULL DEFAULT '' COMMENT '最后登录时间' AFTER `last_logout_time`;

ALTER TABLE `ny_role` CHANGE `name` `name` VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci DEFAULT ''  NOT NULL  COMMENT '角色名';
ALTER TABLE `ny_role` CHANGE `career` `career` INT(4) DEFAULT 0  NOT NULL  COMMENT '角色当前职业';
ALTER TABLE `ny_role` MODIFY COLUMN `account`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '账号' AFTER `id`;

CREATE TABLE `ny_lua_trace` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '号码',
  `trace` text NOT NULL COMMENT '调试',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='lua调试';


ALTER TABLE `ny_role` DROP INDEX `role_id`, ADD  UNIQUE INDEX `role_id` (`role_id`);

ALTER TABLE `ny_role` ADD COLUMN `role_level` SMALLINT(5) UNSIGNED DEFAULT 0  NOT NULL  COMMENT '等级' AFTER `name`;


ALTER TABLE `ny_consume_produce` ADD INDEX (`create_time`);





