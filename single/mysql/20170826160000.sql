CREATE TABLE `ny_kfcb` (
  `id` int(10) NOT NULL auto_increment comment 'ID',
  `aId` varchar(100) default '' comment '活动唯一标识',
  `packe_name` varchar(100) default '' comment '礼包名',
  `obj_count` int(11) default '0' comment '玩家参与数量',
  `buy_count` int(11) default '0' comment '总购买数量',
  `upper_count` int(11) default '0' comment '达到上限的玩家数量',
  `supply_times` int(11) default '0' comment '补仓数量',
  `create_time` int(11) default '0' comment '更新时间',
  UNIQUE KEY (`aId`,`packe_name`),
    PRIMARY KEY(`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8


ALTER TABLE `ny_role`
ADD COLUMN `mac`  varchar(30) NOT NULL DEFAULT '0' COMMENT 'MAC地址，0表示未知' AFTER `vip_level`;

ALTER TABLE `ny_role`
ADD COLUMN `psId`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '服务器标识，渠号*10000+服务器号';

//待更新
ALTER TABLE `ny_online_hook`
ADD COLUMN `psId`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '服务器标识，渠号*10000+服务器号';


