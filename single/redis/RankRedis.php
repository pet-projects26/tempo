<?php
/**
 * 单服roleRedis操作类
 */

class RankRedis extends redisServer
{

    public function __construct()
    {
        parent::__construct();
    }

    //查询排行榜数据
    public function getTypeRankData($type)
    {

        if (!$type)  exit(json_encode(['errno' => 2 , 'error' => "type is empty"]));

        $res = [];

        $key = self::$RankName;
        $data =  $this->redis->hGetAll($key);

        $data = $data['rankmgr'];
        $data = msgpack_unpack($data);

        $data = $this->arrayCombine(self::$RankMgrFields, $data);

        if (!empty($data['actorRanks'])) {

            foreach ($data['actorRanks'] as $key => $value) {
                $data['actorRanks'][$key] = $this->arrayCombine(self::$ActorRanksFields, $value);

                if (!empty($data['actorRanks'][$key]['ranks'])) {

                    foreach ($data['actorRanks'][$key]['ranks'] as $rankKey => $rankVal) {

                        $data['actorRanks'][$key]['ranks'][$rankKey] = $this->arrayCombine(self::$RankDataFields, $rankVal);

                        if ($data['actorRanks'][$key]['ranks'][$rankKey]['rankType'] == $type) {

                            $tmp = array_merge($data['actorRanks'][$key], $data['actorRanks'][$key]['ranks'][$rankKey]);

                            unset($tmp['ranks']);
                            unset($tmp['show']);

                            //获取vip经验值
                            $roleVip = $this->redis->hGet(self::$VipName, $tmp['agentId']);

                            $roleVip = msgpack_unpack($roleVip);

                            if (!empty($roleVip)) {
                                $roleVip = $this->arrayCombine(self::$VipFields, $roleVip);
                                $tmp['vip'] = $roleVip['grade'];
                                $tmp['vipExp'] = $roleVip['exp'];
                            } else {
                                //防止vipExp为空无法排序
                                $tmp['vipExp'] = 0;
                            }

                            $res[] = $tmp;
                            //跳出两层 进入下一个角色循环
                            continue 2;
                        }
                    }
                }
            }
            unset($key);
            unset($value);
        }

        //排序
        if (!empty($res)) {
            //再以所有键的param排序
            $sortKey = array_column($res, 'param');
            //vip
            $sortKey2 = array_column($res, 'vip');
            //vip经验
            $sortKey3 = array_column($res, 'vipExp');
            //时间
            $sortKey4 = array_column($res, 'tm');
            //多条件排序
            array_multisort($sortKey, SORT_DESC, $sortKey2, SORT_DESC, $sortKey3, SORT_DESC, $sortKey4, SORT_ASC, $res);
        }

        echo json_encode($res);
    }

}