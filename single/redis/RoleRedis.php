<?php
/**
 * 单服roleRedis操作类
 */

class RoleRedis extends redisServer
{

    public function __construct()
    {
        parent::__construct();
    }

    //查询角色数据
    public function getRoleInfo($role_id)
    {

        if (!$role_id)  exit(json_encode(['errno' => 2 , 'error' => "role_id is empty"]));
        /*
                $key = self::$XianFuName;

                //测试阶段 先把想看到key 列出来看一下
                $account =  $this->redis->hGet($key, $role_id);
                $data = [];

                 $info =  msgpack_unpack($account);

                 $info = $this->arrayCombine(self::$XianFuFields, $info);


                $a = $info;
        //
                echo json_encode($data);
        //        var_dump($account);exit;
        */
        $data = [];
        //首先获取ActorBase hash表的角色信息 根据role_id 获取对应的角色信息
        $role_info = $this->redis->hGet(self::$ActorBaseName, $role_id);

        if (!$role_info) exit(json_encode(['errno' => 2 , 'error' => "this role does not exist"]));

        $role_info = msgpack_unpack($role_info);

        $role_info = $this->arrayCombine(self::$ActorBaseFields, $role_info);

        $role_info['id'] = abs($role_info['id']);
        //角色创建时间 毫秒
        $role_info['timestamps'] = $role_info['timestamps'] / 1000;

        $RoleModel = new RoleModel();
        //获取角色的战力
        $role_power = $RoleModel->getRoleFields($role_id, 'power');
        $role_package = $RoleModel->getRoleFields($role_id, 'package');
        $role_info['power'] = $role_power;
        $role_info['package'] = $role_package;

        //获取角色仙位
        $role_xianwei = $this->redis->hGet(self::$XianweiName, $role_id);

        $role_xianwei = msgpack_unpack($role_xianwei);

        $role_xianwei = $this->arrayCombine(self::$XianweiFields, $role_xianwei);

        if($role_xianwei) {
            //从配置JSON中读取仙位信息
            $xianwei = Fields::getJsonData('xianwei', ['name'], 'id');

            $role_xianwei['riseName'] = $xianwei[$role_xianwei['riseId']]['name'];

            $role_info['xianwei_name'] = $role_xianwei['riseName'];
        }

        //获取角色VIP等级
        $role_vip = $this->redis->hGet(self::$VipName, $role_id);

        $role_vip = msgpack_unpack($role_vip);

        $role_vip = $this->arrayCombine(self::$VipFields, $role_vip);

        $role_info['vip_level'] = $role_vip['grade'];

        //获取角色充值金额
        $role_info['money'] = (new ChargeModel())->getChargeMoneyByRole(0, [$role_id]);

        //再根据角色id获取对应的装备栏
        $role_equip = $this->redis->hGet(self::$ActorEquipName, $role_id);

        $role_equip = msgpack_unpack($role_equip);

        $role_equip = $this->arrayCombine(self::$ActorEquipFields, $role_equip);

        //角色装备数组对应部位字段
        $role_equip['EquipGrids'] = $this->arrayCombine(self::$EquipFields, $role_equip['EquipGrids']);

        //从配置JSON中读取装备信息
        $equip = Fields::getJsonData('equip', ['name']);

        //从配置JSON中读取仙石信息
        $stone = Fields::getJsonData('stone', ['name']);

        foreach ($role_equip['EquipGrids'] as $grid_key  => $grid_val) {

            //装备里对应的字段
            $role_equip['EquipGrids'][$grid_key] = $this->arrayCombine(self::$EquipGridFields, $grid_val);

            $role_equip['EquipGrids'][$grid_key]['item'] = $this->arrayCombine(self::$ItemFields, $role_equip['EquipGrids'][$grid_key]['item']);

            if (!empty($role_equip['EquipGrids'][$grid_key]['item']['itemId'])) {
                //装备阶级 23位
                $role_equip['EquipGrids'][$grid_key]['item']['class'] = substr($role_equip['EquipGrids'][$grid_key]['item']['itemId'], 1, 2) ? substr($role_equip['EquipGrids'][$grid_key]['item']['itemId'], 1, 2) : 0;
                //装备品质 4位
                $role_equip['EquipGrids'][$grid_key]['item']['grade'] = substr($role_equip['EquipGrids'][$grid_key]['item']['itemId'], 3, 1) ? substr($role_equip['EquipGrids'][$grid_key]['item']['itemId'], 3, 1) : 0;

                //装备星级 5位 第5位数，1代表0星，2代表1星，3代表2星，4代表3星
                $role_equip['EquipGrids'][$grid_key]['item']['star'] = substr($role_equip['EquipGrids'][$grid_key]['item']['itemId'], 4, 1) ? substr($role_equip['EquipGrids'][$grid_key]['item']['itemId'], 4, 1) - 1 : 0;

                //装备名
                $role_equip['EquipGrids'][$grid_key]['item']['itemName'] = $equip[$role_equip['EquipGrids'][$grid_key]['item']['itemId']]['name'];
            }

            //仙石名
            foreach ($role_equip['EquipGrids'][$grid_key]['gems'] as $gem_key => $gem_val) {
                $role_equip['EquipGrids'][$grid_key]['gems'][$gem_key] = $role_equip['EquipGrids'][$grid_key]['gems'][$gem_key] != 0 ? $stone[$role_equip['EquipGrids'][$grid_key]['gems'][$gem_key]]['name'] : '无';
            }
        }


        //阶 = /10  灵宠
        //星 = %10

        //再根据角色id获取对应的灵宠
        $role_pet = $this->redis->hGet(self::$PetName, $role_id);

        $role_pet = msgpack_unpack($role_pet);

        $role_pet = $this->arrayCombine(self::$PetFields, $role_pet);

//        $role_pet['magicShowList'][0] = array(0 => 2004, 1 => 1);

        $levelVal = $this->starFormat($role_pet['star']);
        //阶
        $role_pet['rank'] = $levelVal[0];
        //星
        $role_pet['stars'] = $levelVal[1];


        if (isset($role_pet['magicShowList'])) {
            //配置读取
            $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            $role_pet['magicShow_str'] = '';
            foreach ($role_pet['magicShowList'] as $magic_key => $magic_val) {
                $role_pet['magicShowList'][$magic_key] = $this->arrayCombine(self::$PetMagicShowInfoFields, $magic_val);
                $role_pet['magicShowList'][$magic_key]['name'] = $exterior[$role_pet['magicShowList'][$magic_key]['showId']]['name'];
                $role_pet['magicShow_str'] .= $role_pet['magicShowList'][$magic_key]['name'] . '-' . $role_pet['magicShowList'][$magic_key]['star'] . '级</br>';
            }
        } else {
            $role_pet['magicShow_str'] = '无';
        }

        if (isset($role_pet['refineList'])) {
            $role_pet['refineList_str'] = '';
            //灵宠修炼配置读取
            foreach ($role_pet['refineList'] as $refine_key => $refine_val) {
                $role_pet['refineList'][$refine_key] = $this->arrayCombine(self::$PetRefineInfoFields, $refine_val);
                $role_pet['refineList'][$refine_key]['typeName'] = self::$PetRefineInfoTypeName[$role_pet['refineList'][$refine_key]['type']];
                $role_pet['refineList_str'] .= $role_pet['refineList'][$refine_key]['typeName'].$role_pet['refineList'][$refine_key]['level'].'级</br>';
            }
        } else {
            $role_pet['refineList_str'] = '无';
        }

        if (isset($role_pet['fazhenList'])) {
            //配置读取
            if (!isset($exterior)) {
                $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            }
            $role_pet['fazhenList_str'] = '';
            //灵宠修炼配置读取
            foreach ($role_pet['fazhenList'] as $refine_key => $refine_val) {
                $role_pet['fazhenList'][$refine_key] = $this->arrayCombine(self::$PetMagicShowInfoFields, $refine_val);
                $role_pet['fazhenList'][$refine_key]['name'] = $exterior[$role_pet['fazhenList'][$refine_key]['showId']]['name'];
                $role_pet['fazhenList_str'] .= $role_pet['fazhenList'][$refine_key]['name'] . '-' . $role_pet['fazhenList'][$refine_key]['star'] . '阶</br>';
            }
        } else {
            $role_pet['fazhenList_str'] = '无';
        }


        //仙器
        //再根据角色id获取对应的仙器
        $role_ride = $this->redis->hGet(self::$RideName, $role_id);

        $role_ride = msgpack_unpack($role_ride);

        $role_ride = $this->arrayCombine(self::$RideFields, $role_ride);

        $levelVal = $this->starFormat($role_ride['star']);
        //阶
        $role_ride['rank'] = $levelVal[0];
        //星
        $role_ride['stars'] = $levelVal[1];

        if (isset($role_ride['magicShowList'])) {
            //配置读取
            if (!isset($exterior)) {
                $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            }
            $role_ride['magicShow_str'] = '';
            foreach ($role_ride['magicShowList'] as $magic_key => $magic_val) {
                $role_ride['magicShowList'][$magic_key] = $this->arrayCombine(self::$PetMagicShowInfoFields, $magic_val);
                $role_ride['magicShowList'][$magic_key]['name'] = $exterior[$role_ride['magicShowList'][$magic_key]['showId']]['name'];
                $role_ride['magicShow_str'] .= $role_ride['magicShowList'][$magic_key]['name'] . '-' . $role_ride['magicShowList'][$magic_key]['star'] . '级</br>';
            }
        } else {
            $role_ride['magicShow_str'] = '无';
        }

        if (isset($role_ride['refineList'])) {
            $role_ride['refineList_str'] = '';
            //仙器修炼配置读取
            foreach ($role_ride['refineList'] as $refine_key => $refine_val) {
                $role_ride['refineList'][$refine_key] = $this->arrayCombine(self::$PetRefineInfoFields, $refine_val);
                $role_ride['refineList'][$refine_key]['typeName'] = self::$RideRefineInfoTypeName[$role_ride['refineList'][$refine_key]['type']];
                $role_ride['refineList_str'] .= $role_ride['refineList'][$refine_key]['typeName'].$role_ride['refineList'][$refine_key]['level'].'级</br>';
            }
        } else {
            $role_ride['refineList_str'] = '无';
        }

        if (isset($role_ride['fazhenList'])) {
            //配置读取
            if (!isset($exterior)) {
                $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            }
            $role_ride['fazhenList_str'] = '';
            foreach ($role_ride['fazhenList'] as $magic_key => $magic_val) {
                $role_ride['fazhenList'][$magic_key] = $this->arrayCombine(self::$PetMagicShowInfoFields, $magic_val);
                $role_ride['fazhenList'][$magic_key]['name'] = $exterior[$role_ride['fazhenList'][$magic_key]['showId']]['name'];
                $role_ride['fazhenList_str'] .= $role_ride['fazhenList'][$magic_key]['name'] . '-' . $role_ride['fazhenList'][$magic_key]['star'] . '阶</br>';
            }
        } else {
            $role_ride['fazhenList_str'] = '无';
        }

        //仙法
        $role_skill = $this->redis->hGet(self::$ActorSkillName, $role_id);

        $role_skill = msgpack_unpack($role_skill);

        $role_skill = $this->arrayCombine(self::$ActorSkillFields, $role_skill);

        if (isset($role_skill['Skills'])) {
            //技能配置读取
            $skill = Fields::getJsonData('skill', ['name'], 'id');
            foreach ($role_skill['Skills'] as $skill_key => $skill_val) {
                $role_skill['Skills'][$skill_key] = $this->arrayCombine(self::$SkillFields, $skill_val);

                $role_skill['Skills'][$skill_key]['intactSkillId'] = $role_skill['Skills'][$skill_key]['SkillId'] . str_pad($role_skill['Skills'][$skill_key]['level'], 4, '0', STR_PAD_LEFT);

                $role_skill['Skills'][$skill_key]['name'] = $skill[$role_skill['Skills'][$skill_key]['intactSkillId']]['name'];
            }
        }

        //法宝
        $role_amulet = $this->redis->hGet(self::$AmuletName, $role_id);

        $role_amulet = msgpack_unpack($role_amulet);

        $role_amulet = $this->arrayCombine(self::$AmuletFields, $role_amulet);

        if (isset($role_amulet['refineList'])) {
            //物品配置读取
            $material = Fields::getJsonData('material', ['name']);
            foreach ($role_amulet['refineList'] as $amulet_key => $amulet_val) {
                //对应字段
                $role_amulet['refineList'][$amulet_key] = $this->arrayCombine(self::$AmuletRefineListFields, $amulet_val);

                $role_amulet['refineList'][$amulet_key]['name'] = $material[$role_amulet['refineList'][$amulet_key]['id']]['name'];

                //法宝类型 2 , 3位
                $role_amulet['refineList'][$amulet_key]['type'] = substr($role_amulet['refineList'][$amulet_key]['id'], 3, 1) ? substr($role_amulet['refineList'][$amulet_key]['id'], 1, 2) : 0;

                $role_amulet['refineList'][$amulet_key]['type'] = intval($role_amulet['refineList'][$amulet_key]['type']);

                //法宝类型名称
                $role_amulet['refineList'][$amulet_key]['typeName'] = $role_amulet['refineList'][$amulet_key]['type'] != 0 ? self::$AmuletRefineTypeFields[$role_amulet['refineList'][$amulet_key]['type']] : '无';

                //法宝品质 第4位
                $role_amulet['refineList'][$amulet_key]['grade'] =  substr($role_amulet['refineList'][$amulet_key]['id'], 3, 1) ? substr($role_amulet['refineList'][$amulet_key]['id'], 3, 1) : 0;
            }
        }

        //金身
        $role_soul = $this->redis->hGet(self::$SoulName, $role_id);

        $role_soul = msgpack_unpack($role_soul);

        $role_soul = $this->arrayCombine(self::$SoulFields, $role_soul);

        if (isset($role_soul['refineList'])) {

            foreach ($role_soul['refineList'] as $soul_key => $soul_val) {

                $role_soul['refineList'][$soul_key] = $this->arrayCombine(self::$SoulRefineFields, $soul_val);

                //修炼等级
                //1-10代表1阶1-10重
                //11-20代表2阶1-10重
                $role_soul['refineList'][$soul_key]['typeName'] = self::$SoulTypeName[$role_soul['refineList'][$soul_key]['type']];

                $levelVal = $this->starFormat($role_soul['refineList'][$soul_key]['level']);

                //阶
                $role_soul['refineList'][$soul_key]['rank'] = $levelVal[0];

                //重
                $role_soul['refineList'][$soul_key]['layer'] = $levelVal[1];

                //阶重
                $role_soul['refineList'][$soul_key]['rankLayer'] = $levelVal[0].'阶'.$levelVal[1].'重';
            }
        }

        //神兵
        $role_shenbing = $this->redis->hGet(self::$ShenbingName, $role_id);

        $role_shenbing = msgpack_unpack($role_shenbing);

        $role_shenbing = $this->arrayCombine(self::$ShenbingFields, $role_shenbing);


        if (isset($role_shenbing['showList'])) {
            $role_shenbing['showList_str'] = '';
            //配置读取
            if (!isset($exterior)) {
                $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            }
            foreach ($role_shenbing['showList'] as $magic_key => $magic_val) {
                $role_shenbing['showList'][$magic_key] = $this->arrayCombine(self::$ShenbingMagicShowFields, $magic_val);
                $role_shenbing['showList'][$magic_key]['name'] = $exterior[$role_shenbing['showList'][$magic_key]['showId']]['name'];
                $role_shenbing['showList_str'] .= $role_shenbing['showList'][$magic_key]['name'].'-'. $role_shenbing['showList'][$magic_key]['level'].'阶</br>';
            }
        } else {
            $role_shenbing['showList_str'] = '无';
        }

        if (isset($role_shenbing['refineList'])) {
            $role_shenbing['refineList_str'] = '';
            //神兵修炼配置读取
            foreach ($role_shenbing['refineList'] as $refine_key => $refine_val) {
                $role_shenbing['refineList'][$refine_key] = $this->arrayCombine(self::$PetRefineInfoFields, $refine_val);
                $role_shenbing['refineList'][$refine_key]['typeName'] = self::$ShenbingRefineInfoTypeName[$role_shenbing['refineList'][$refine_key]['type']];
                $role_shenbing['refineList_str'] .= $role_shenbing['refineList'][$refine_key]['typeName'].'-'.$role_shenbing['refineList'][$refine_key]['level'].'级</br>';
            }
        } else {
            $role_shenbing['refineList_str'] = '无';
        }

        //仙翼 wing
        $role_wing = $this->redis->hGet(self::$WingName, $role_id);

        $role_wing = msgpack_unpack($role_wing);

        $role_wing = $this->arrayCombine(self::$WingFields, $role_wing);

        if (isset($role_wing['showList'])) {
            $role_wing['showList_str'] = '';
            //配置读取
            if (!isset($exterior)) {
                $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            }
            foreach ($role_wing['showList'] as $magic_key => $magic_val) {
                $role_wing['showList'][$magic_key] = $this->arrayCombine(self::$ShenbingMagicShowFields, $magic_val);
                $role_wing['showList'][$magic_key]['name'] = $exterior[$role_wing['showList'][$magic_key]['showId']]['name'];
                $role_wing['showList_str'] .= $role_wing['showList'][$magic_key]['name'].'-'. $role_wing['showList'][$magic_key]['level'].'阶</br>';
            }
        } else {
            $role_wing['showList_str'] = '无';
        }

        if (isset($role_wing['refineList'])) {
            $role_wing['refineList_str'] = '';
            //神兵修炼配置读取
            foreach ($role_wing['refineList'] as $refine_key => $refine_val) {
                $role_wing['refineList'][$refine_key] = $this->arrayCombine(self::$PetRefineInfoFields, $refine_val);
                $role_wing['refineList'][$refine_key]['typeName'] = self::$WingRefineInfoTypeName[$role_wing['refineList'][$refine_key]['type']];
                $role_wing['refineList_str'] .= $role_wing['refineList'][$refine_key]['typeName'].'-'.$role_wing['refineList'][$refine_key]['level'].'级</br>';
            }
        } else {
            $role_wing['refineList_str'] = '无';
        }

        //背包
        $role_bagInfo = $this->redis->hGet(self::$BagInfoName, $role_id);

        $role_bagInfo = msgpack_unpack($role_bagInfo);

        $role_bagInfo = $this->arrayCombine(self::$BagInfoFields, $role_bagInfo);

//        $role_bagInfo['Bags'] = $this->arrayCombine(self::$BagIdTypeName, $role_bagInfo['Bags']);

        $role_item_bagInfo = $role_bagInfo['Bags'][0]; //只拿道具类背包

        $role_item_bagInfo = $this->arrayCombine(self::$BagFields, $role_item_bagInfo);

        if (isset($role_item_bagInfo['items'])) {
            //读取配置赋予道具名字
            //配置读取
            if (!isset($material)) {
                $material = Fields::getJsonData('material', ['name']);
            }
            foreach($role_item_bagInfo['items'] as $bag_key =>  $bag_val) {
                $role_item_bagInfo['items'][$bag_key] = $this->arrayCombine(self::$ItemFields, $bag_val);
                $role_item_bagInfo['items'][$bag_key]['itemName'] = array_key_exists($role_item_bagInfo['items'][$bag_key]['itemId'], $material) ? $material[$role_item_bagInfo['items'][$bag_key]['itemId']]['name'] : '未知';
            }
        }

        $challenge_record = [];

        //玩家副本挑战层数 查询mysql zones 天关 大荒
        $role_zones = (new ZonesModel())->getRoleZonesMaxLayerRecord($role_id);

        if ($role_zones) {
            foreach ($role_zones as $zone_key => $zone_val) {
//                $role_zones[$zone_key]['zonesName'] = CDict::$zonesTypes[$zone_key]['name'];
                $zone_val['name'] = CDict::$zonesTypes[$zone_key]['name'];
                array_push($challenge_record, $zone_val);
            }
        }

        //每日试炼
        $role_trial = (new DailytrialModel())->getRoleTrialMaxLayerRecord($role_id);

        if ($role_trial) {
            foreach ($role_trial as $trial_key => $trial_val) {
//                $role_zones[$zone_key]['zonesName'] = CDict::$zonesTypes[$zone_key]['name'];
                $trial_val['name'] = CDict::$trialTypes[$trial_key]['name'];
                array_push($challenge_record, $trial_val);
            }
        }

        //远古符阵
        $role_rune_copy = (new RuneCopyModel())->getRoleMaxLayerRecord($role_id);

        if ($role_rune_copy['max_layers']) {
            $role_rune_copy['name'] = '远古符阵';
            array_push($challenge_record, $role_rune_copy);
        }

        //组队副本
        $role_team_copy = (new TeamCopyModel())->getRoleMaxLayerRecord($role_id);

        if ($role_team_copy['max_layers']) {
            $role_team_copy['name'] = '幽冥鬼境';
            array_push($challenge_record, $role_team_copy);
        }

        //符文
        $role_rune = $this->redis->hGet(self::$RuneName, $role_id);

        $role_rune = msgpack_unpack($role_rune);

        $role_rune = $this->arrayCombine(self::$RuneFields, $role_rune);

        if (isset($role_rune['slot'])) {
            //读取配置
            $item_rune = Fields::getJsonData('rune', ['itemId', 'name'], 'itemId');

            foreach ($role_rune['slot'] as $key => $value) {

                $role_rune['slot'][$key] = $this->arrayCombine(self::$RuneSlotFields, $value);

                if ($role_rune['slot'][$key]['itemId']) {
                    //后4位数是等级
                    $role_rune['slot'][$key]['level'] = (int)substr($role_rune['slot'][$key]['itemId'], -4, 4);

                    $role_rune['slot'][$key]['type'] = (int)substr($role_rune['slot'][$key]['itemId'], 3, 1);

                    $role_rune['slot'][$key]['typeName'] = array_key_exists($role_rune['slot'][$key]['type'], self::$RuneType) ? self::$RuneType[$role_rune['slot'][$key]['type']] : '未知品质';

                    $role_rune['slot'][$key]['itemId2'] = $role_rune['slot'][$key]['itemId'] - $role_rune['slot'][$key]['level'];

                    $role_rune['slot'][$key]['itemName'] = $item_rune[$role_rune['slot'][$key]['itemId2']]['name'] ? $item_rune[$role_rune['slot'][$key]['itemId2']]['name'] . ' | ' . $role_rune['slot'][$key]['typeName'] : '未知';
                }
            }

            $sortKey = array_column($role_rune['slot'], 'id');
            //按符文槽id正序排序
            array_multisort($sortKey, SORT_ASC, $role_rune['slot']);

        }

        //玩家仙府数据
        $role_xianfu = $this->redis->hGet(self::$XianFuName, $role_id);

        $role_xianfu = msgpack_unpack($role_xianfu);

        $role_xianfu = $this->arrayCombine(self::$XianFuFields, $role_xianfu);

        $role_xianfu_animalInfo = [];
        $role_xianfu_illBookInfo = [];
        $role_xianfu_fengShuiInfo = [];

        $role_info['xianfu_level'] = $role_xianfu['level'];
        //灵兽
        if (isset($role_xianfu['spiritAnimalInfo'])) {
            //配置读取
            if (!isset($exterior)) {
                $exterior = Fields::getJsonData('exterior', ['name'], 'id');
            }
            $role_xianfu_animalInfo = $role_xianfu['spiritAnimalInfo'];
            foreach ($role_xianfu_animalInfo as $key => &$value) {
                $value = $this->arrayCombine(self::$SpiritAnimalFields, $value);
                $value['name'] = array_key_exists($value['id'], $exterior) ? $exterior[$value['id']]['name'] : '未知';
            }
            unset($key);
            unset($value);
        }
        //灵兽图鉴
        if (isset($role_xianfu['illBookInfo'])) {
            $role_xianfu_illBookInfo = $this->arrayCombine(self::$IllBookFields, $role_xianfu['illBookInfo']);
            $role_xianfu_illBookInfo = $role_xianfu_illBookInfo['id'];

            //读取配置
            $illBook = Fields::getJsonData('xianfu_illustrated_handbook', ['name', 'level', 'quality'], 'id');

            foreach ($role_xianfu_illBookInfo as $key => &$value) {
                $value = $this->arrayCombine(self::$IllBookIdFields, $value);

                if (array_key_exists($value['id'], $illBook)) {
                    $value['name'] = $illBook[$value['id']]['name'];
                    $value['quality'] = $illBook[$value['id']]['quality'];
                } else {
                    $value['name'] = '未知';
                    $value['quality'] = 0;
                }

                $value['qualityName'] = array_key_exists($value['quality'], self::$IllBookQuality) ? self::$IllBookQuality[$value['quality']] : '未知';
            }
        }
        //风水物件
        if (isset($role_xianfu['fengShuiInfo'])) {
            $role_xianfu_fengShuiInfo = $this->arrayCombine(self::$XianFuFengShuiFields, $role_xianfu['fengShuiInfo']);

            $decorate = Fields::getJsonData('xianfu_decorate', ['items'], 'id');

            //配置读取
            if (!isset($material)) {
                $material = Fields::getJsonData('material', ['name']);
            }

            if (isset($role_xianfu_fengShuiInfo['decorate'])) {

                foreach ($role_xianfu_fengShuiInfo['decorate'] as $key => &$value) {

                    $val = [];
                    $val['id'] = $value;
                    if (array_key_exists($value, $decorate)) {
                        $item_id = $decorate[$value]['items'][0];
                        $val['item_id'] = $item_id;
                        $val['name'] = array_key_exists($item_id, $material) ? $material[$item_id]['name'] : '未知';
                        $val['level'] = (int)substr($value, -1, 2);
                    } else {
                        $val['item_id'] = '未知';
                        $val['name'] = '未知';
                    }
                    //类型 id 第一位 直接截取
                    $val['type'] = (int)substr($value, 0, 1);
                    $val['typeName'] = array_key_exists($val['type'], self::$XianfuDecorateType) ? self::$XianfuDecorateType[$val['type']] : '未知';
                    $value = $val;
                }
            }
        }

        //将所有数据合并在$data
        $data['role_info'] = $role_info; //角色基本信息
        $data['role_equip'] = $role_equip; //角色装备信息
        $data['role_pet'] = $role_pet; //角色灵宠信息
        $data['role_ride'] = $role_ride; //角色仙器信息
        $data['role_skill'] = $role_skill; // 角色仙法信息
        $data['role_amulet'] = $role_amulet; //角色法宝信息
        $data['role_soul'] = $role_soul; //角色金身信息
        $data['role_shenbing'] = $role_shenbing; //角色神兵信息
        $data['role_wing'] = $role_wing; //角色仙翼信息
        $data['role_item_bagInfo'] = $role_item_bagInfo; //角色道具背包信息
        $data['role_zones'] = $challenge_record; //角色副本记录信息
        $data['role_rune'] = $role_rune; //角色符文信息
        $data['role_xianfu_animalInfo'] = $role_xianfu_animalInfo; //角色仙府灵兽信息
        $data['role_xianfu_illBookInfo'] = $role_xianfu_illBookInfo; //角色仙府灵兽图鉴
        $data['role_xianfu_fengShuiInfo'] = $role_xianfu_fengShuiInfo; //角色仙府风水信息


        exit(json_encode($data));
    }

    public function starFormat($star)
    {
        if ($star < 0) {
            $valArr[0] = 0;
            $valArr[1] = 0;
            return $valArr;
        }

        $val = bcdiv($star + 10, 10, 1);

        $valArr = explode('.', strval($val));

        if ($valArr[1] == 0) {

            $valArr[0] -= 1;
            $valArr[1] = 10;

        }
        return $valArr;
    }
}