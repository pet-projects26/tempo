<?php
/**
 * GM指令
 * Created by PhpStorm.
 * User: w
 * Date: 2018/12/26
 * Time: 10:16
 */
require_once SOCKET.'/WebsocketClient.php';
require_once SOCKET.'/WebSocketParser.php';
require_once SOCKET.'/Parser.php';

ini_set('display_errors', 'Off');
error_reporting(0);
//gmcmd('192.168.2.188', '9999', ['opcode' => gmPact, 'str' => [role_id, gmcmd]]);

function gmcmd($host, $port, $params)
{

    $Client = new \Swoole\Client\WebsocketClient($host, $port);

    $Client->connect();

    $send_buff = $Client->publish($params['opcode'], $params['str']);

    $Client->send($send_buff, 'bin');
}