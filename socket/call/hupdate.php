<?php
/**
 * 热更新
 * Created by PhpStorm.
 * User: w
 * Date: 2018/11/23
 * Time: 16:26
 */
require_once SOCKET.'/WebsocketClient.php';
require_once SOCKET.'/WebSocketParser.php';
require_once SOCKET.'/Parser.php';

ini_set('display_errors', 'Off');
error_reporting(0);
//hupdate('192.168.2.188', '9999', ['opcode' => 0x12000, 'str' => [number,['/agent/gm_command.js', '...', '...']]]);

function hupdate($host, $port, $params)
{

    $Client = new \Swoole\Client\WebsocketClient($host, $port);

    $Client->connect();

    $send_buff = $Client->publish($params['opcode'], $params['str']);

    $Client->send($send_buff, 'bin');

    $returnData = $Client->recv();

    //解码
    $res = $Client->unPublish($returnData);

    return $res[0];
}

