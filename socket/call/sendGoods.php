<?php
/**
 * 商品发货
 * Created by PhpStorm.
 * User: w
 * Date: 2018/12/27
 * Time: 14:45
 */
require_once SOCKET.'/WebsocketClient.php';
require_once SOCKET.'/WebSocketParser.php';
require_once SOCKET.'/Parser.php';

//hupdate('192.168.2.188', '9999', ['opcode' => 0x12000, 'str' => [number,['/agent/gm_command.js', '...', '...']]]);

ini_set('display_errors', 'Off');
error_reporting(0);

function sendGoods($host, $port, $params)
{
    $Client = new \Swoole\Client\WebsocketClient($host, $port);

    $Client->connect();

    $send_buff = $Client->publish($params['opcode'], $params['str']);

    $Client->send($send_buff, 'bin');

    $returnData = $Client->recv();
    //解码
    $res = $Client->unPublish($returnData);

    return $res[0];
}


