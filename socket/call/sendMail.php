<?php
/**
 * 邮件发送
 * Created by PhpStorm.
 * User: w
 * Date: 2018/12/27
 * Time: 14:45
 */
require_once SOCKET.'/WebsocketClient.php';
require_once SOCKET.'/WebSocketParser.php';
require_once SOCKET.'/Parser.php';

ini_set('display_errors', 'Off');
error_reporting(0);

function sendMail($host, $port, $params)
{
    $Client = new \Swoole\Client\WebsocketClient($host, $port);

    $Client->connect();

    $send_buff = $Client->publish($params['opcode'], $params['str']);

    $Client->send($send_buff, 'bin');

    $returnData = $Client->recv();
    //解码
    $res = $Client->unPublish($returnData);

    $return = empty($res) ? 0 : 1;

    return $return;
}


