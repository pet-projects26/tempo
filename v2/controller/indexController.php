<?php
/**
 * 主要是游戏内公用的一些方法，比如说订单号的获取
 * 
 */
class indexController extends BaseController{
    private $_key = 'Y0FG2GR8';


    public function index() {
    	$sign = getParam('sign');
    	if (empty($sign)) {
    		echo -1;
    		die();
    	}

    	$method = getParam('method');

    	if (empty($method)) {
    		echo -2;
    		die();
    	}

    	$row = (new ServerconfigModel())->getConfig($sign);

    	if (empty($row)) {
    		echo -3;
    		die();
    	}

    	$tk = getParam('tk');

    	$md5 = md5($sign . $this->_key);

    	if ($tk != $md5) {
    		echo -4;
    		die();
    	}
 
    	$ip = $row['ip'];
    	$gm_port = $row['gm_port'];

        if($method == 'closeTrade' || $method == 'openTrade'){

            $ip = TRADE_IP;
            $gm_port = TRADE_PORT;
        }

   		$param = isset($_POST['param']) ? $_POST['param'] : array();

   		if (empty($param)) {
			$res = (new Model())->rpcCall($ip , $gm_port , $method);
   		} else {
   			$res = (new Model())->rpcCall($ip , $gm_port ,$method, $param);
   		}

		if ($res == 0) {
			echo 1;
			die();
		}

		print_r($res);
		die();

    }

    /**
     * test
     * @return [type] [description]
     */
    public function test() {
        $method = 'uactivity';
		$res = (new Model())->rpcCall('124.222.50.8' , 6714, $method);
        print_r($res);

        var_dump($res);
    }
}
