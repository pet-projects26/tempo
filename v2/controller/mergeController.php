<?php
/**
 * 合服接口
 */
class mergeController extends BaseController{
    private $_key = 'Y0FG2GR8';


    /**
     * 要合并的表
     */
    private function table() {
        return array(
            //角色表
            'ny_role' => 'account, package, role_id, name, role_level, career, last_login_time, last_logout_time, last_login_ip,create_time, faction_id, faction_name, position, position_name, gold, bind_gold,coin, power, vip_level, mac, psId,shenhun,wing,strong,zuojihunshi,beast',

            //金钱消耗表
            'ny_payment' => 'role_id, role_name, role_career, role_level, type, coin_source,copper,gold,bind_gold, sorce,bag_copper,  bag_gold, bag_bind_gold, bag_sorce, money_type, create_time',
            
            //订单表
            'ny_order' => 'order_num, corder_num,money, account, role_id, role_name, role_level, role_career,gold, status, first,   create_time',
            
            //登录表
            'ny_login' => 'role_id, role_level, role_name, role_career, start_time, end_time, first, ip, session',

            //消耗产出表
            'ny_consume_produce' => 'role_id,role_name,role_career,role_level,type,source, item_id,item_num, item_info, create_time'
        );

    }

    /**
     * 生成合服脚本
     * @return [type] [description]
     */
    public function index() {
    	$childrenDb = getParam('childrenDb');
    	if (empty($childrenDb)) {
            $code = Helper::getCode(8404);
            die($code);
    	}

        $newDb = getParam('newDb');
        if (empty($newDb)) {
            $code = Helper::getCode(8404);
            die($code);
        }

        $tk = getParam('tk');

        if (empty($tk)) {
            $code = Helper::getCode(8404);
            die($code);
        }

        $myTk = md5($childrenDb.'&'. $newDb .'&'. $this->_key);

        if ($tk != $myTk) {
            $code = Helper::getCode(8406);
            die($code);
        }

        $tables = $this->table();

        $time = time() - 15 * 86400;
        //保留日志15天
        
        $sql = "";


        $dbs = explode(',', $childrenDb);

        foreach ($tables as $table => $files) {
            foreach ($dbs as $db) {
                switch ($table) {
                    case 'ny_role':
                            //先删除索引，防止名称冲突
                            $sql .= "DROP INDEX `name` ON ny_role;";
                            
                            $sql .= "\r\n";
                            $sql .= "/**  角色表  **/";
                            $sql .= "\r\n";

                            $sql .= "INSERT INTO {$newDb}.{$table} SELECT NULL, {$files} FROM {$db}.{$table} WHERE 1 ;";

                            $sql .= "\r\n"; 
                        break;
                    case 'ny_payment':
                        $sql .= "/**  金钱消耗表  **/";
                        $sql .= "\r\n";

                        $sql .= "INSERT INTO {$newDb}.{$table} SELECT NULL, {$files} FROM {$db}.{$table} WHERE create_time > {$time} ;";

                        $sql .= "\r\n"; 

                        break;
                    case 'ny_order':
                        $sql .= "/**  订单表  **/";
                        $sql .= "\r\n";

                        $sql .= "INSERT INTO {$newDb}.{$table} SELECT NULL, {$files} FROM {$db}.{$table} WHERE 1;";

                        $sql .= "\r\n";
                        break; 

                    case 'ny_login':
                        $sql .= "/**  登录表  **/";
                        $sql .= "\r\n";

                        $sql .= "INSERT INTO {$newDb}.{$table} SELECT NULL, {$files} FROM {$db}.{$table} WHERE start_time > {$time} ;";

                        $sql .= "\r\n";
                        break;
                    case 'ny_consume_produce':
                        $sql .= "/**  消耗产出表  **/";
                        $sql .= "\r\n";

                        $sql .= "INSERT INTO {$newDb}.{$table} SELECT NULL, {$files} FROM {$db}.{$table} WHERE create_time > {$time}; ";

                        $sql .= "\r\n";
                        break; 
                    default:
                        # code...
                        break;
                }
            }
        }

        $file =  dirname(dirname(dirname(__FILE__)));
        $file = rtrim($file, '/') . '/logs/merge_sql';

        if (!file_exists($file)) {
            @mkdir($file);
        }

        $filename = $file . '/' . $childrenDb .'-'. date('ymd') . '.sql';

        file_put_contents($filename, $sql);
        echo $sql;
        //echo $filename;
        die();
    }

    /**
     * 更新相对应的ID
     * 查出重名的玩家
     * @return [type] [description]
     */
    public function update() {
        $server = getParam('server'); //母服

        if (empty($server)) {
            $code = Helper::getCode(8404);
            die($code);
        }

        $tk = getParam('tk');

        if(empty($tk)) {
            $code = Helper::getCode(8404);
            die($code);
        }

        $myTk = md5($server. '&' .$this->_key);

        if ($myTk != $tk) {
            $code = Helper::getCode(8406);
            die($code);
        }

        //当出现两个角色名相同时, 进行改名
        $sql = "";

        $sql .= "/***** 角色重名 **********/ \r\n";

        $sql .= "UPDATE ny_role SET `name` = CONCAT(floor(`psId` / 100000)-1 , CONCAT('s', (`psId` % 100000), '*', `name`))   WHERE `name` IN (SELECT `name` FROM (SELECT `name`,COUNT(*) AS num FROM ny_role GROUP BY  `name` HAVING num >= 2) AS tmp);";

        $sql .= "\r\n/**************** 添加索引 ************/ \r\n";

        $sql .= "ALTER TABLE `ny_role` ADD  UNIQUE INDEX `name` (`name`);" ; 

        $sql .= "\r\n";

        //@todo 查询活动
        $vars = array (
            'document' => array(
                'table' => 'objIdMapping',
                'data' => array(
                ),
            )
        );

        $postData = array(
            'r' => 'call_method',
            'class' => 'SqlModel',
            'method' => 'getMongo',
            'params' => json_encode($vars),
        );

        $instance  = new ServerconfigModel();
        
        $urls = $instance->getApiUrl(array($server), true);

        //@todo查找mongo

        $rs = Helper::rolling_curl($urls, $postData);

        $rs = isset($rs[$server]) ? json_decode($rs[$server], true) : array();

        $tables = array_keys($this->table());

        $result = array();

        foreach ($rs as $k => $row) {
            $oldObjId = $row['oldObjId'];
            $newObjId = $row['newObjId'];

            preg_match_all('/(\d+)/', $oldObjId, $res);

            $oldObjId = isset($res[0][0]) ? $res[0][0] : $oldObjId;

            $result[$oldObjId] = $newObjId;

        }

        $ids = implode(',', array_keys($result)); 

        foreach ($tables as $table) {
            $sql .= "UPDATE {$table} SET role_id = CASE role_id "; 

            foreach ($result as $oldObjId => $newObjId) { 

                $sql .=" WHEN '$oldObjId' THEN '$newObjId' ";

            } 

            $sql .= " END WHERE role_id IN ($ids);"; 
        }

        $file =  dirname(dirname(dirname(__FILE__)));
        $file = rtrim($file, '/') . '/logs/merge_sql';

        if (!file_exists($file)) {
            @mkdir($file);
        }

        $filename = $file . '/update-' . $server .'-'. date('ymd') . '.sql';

        file_put_contents($filename, $sql);
        echo $sql;
        die();
        //die($filename);
    }

    /**
     * 修改母服配置
     * @return [type] [description]
     */
    public function config() {
        $server       = getParam('server'); //服务器标识符
        $ip           = getParam('ip'); //服务器IP
        $domain       = getParam('domain'); //域名
        $api_url      = getParam('api_url');
        $mysql_host   = getParam('mysql_host');
        $mysql_port   = getParam('mysql_port');
        $mysql_user   = getParam('mysql_user');
        $mysql_passwd = getParam('mysql_passwd');
        $mysql_db     = getParam('mysql_db');
        $mysql_prefix = getParam('mysql_prefix');
        $mongo_host   = getParam('mongo_host');
        $mongo_port   = getParam('mongo_port');
        $mongo_user   = getParam('mongo_user');
        $mongo_passwd = getParam('mongo_passwd');
        $mongo_db     = getParam('mongo_db');
        $login_host   = getParam('login_host');
        $login_port   = getParam('login_port');
        $gm_port      = getParam('gm_port');
        $mdkey        = getParam('mdkey');
        $sign         = getParam('sign');

        $id           = getParam('id'); //合服任务ID

        if (empty($server)) {
            die('error server');
        }

        if (empty($ip)) {
            die('error ip');
        }

        if (empty($api_url)) {
            die('error api_url');
        }

        if (empty($mysql_host)) {
            die('error mysql_host');
        }

        if (empty($mysql_port)) {
            die('error mysql_port');
        }

        if (empty($mysql_user)) {
            die('error mysql_user');
        }

        if (empty($mysql_passwd)) {
            die('error mysql_passwd');
        }

        if (empty($mysql_db)) {
            die('error mysql_db');
        }

        if (empty($mysql_prefix)) {
            die('error mysql_prefix');
        }

        if (empty($mongo_host)) {
            die ('error mongo_host');
        }

        if (empty($mongo_port)) {
            die ('error mongo_port');
        }

        if (empty($mongo_user)) {
            die('error mongo_user');
        }

        if (empty($mongo_passwd)) {
            die('error mongo_passwd');
        }

        if (empty($mongo_db)) {
            die('error mongo_db');
        }

        if (empty($login_host)) {
            die('error login_host');
        }

        if (empty($login_port)) {
            die('error login_port');
        }

        if (empty($gm_port)) {
            die('error gm_port');
        }

        if (empty($mdkey)) {
            die('error mdkey');
        }

        if (empty($sign)) {
            die('error sign'); 
        }

        $mySign = md5($server .'+'. $ip . '+'.$mysql_db . '+'.$mysql_passwd . '+' .$this->_key);

        if ($mySign != $sign) {
            die ('error sign ckeck to ');
        }


        $this->db = new Model();

        $sql = "SELECT * FROM ny_server WHERE server_id = '{$server}'";

        $row = $this->db->fetchOne($sql);

        if (empty($row)) {
            die('fail to find server');
        }

        $sql = "SELECT * FROM ny_server_config WHERE server_id = '{$server}'";

        $row = $this->db->fetchOne($sql);

        if (empty($row)) {
            die('fail to find config');
        }

        $this->db = new Model('server_config');
        
        $data = array(
            'server_id' => $server,
            'ip' => $ip,
            'api_url' => $api_url,
            'domain' => $domain,
            'mysql_host' => $mysql_host,
            'mysql_port' => $mysql_port,
            
            'mysql_user' => $mysql_user,
            
            'mysql_passwd' => $mysql_passwd,
            
            'mysql_db' => $mysql_db,
            'mysql_prefix' => $mysql_prefix,
            'mongo_host' => $mongo_host,
            'mongo_port' => $mongo_port,
            'mongo_user' => $mongo_user,
            'mongo_passwd' => $mongo_passwd,
            'mongo_db' => $mongo_db,
            'login_host' => $login_host,
            'login_port' => $login_port,
            'gm_port' => $gm_port,
            'mdkey' => $mdkey
        );

        $this->db->update($data, array('server_id' => $server));

        $vars = array('config' => $data);

        $postData = array(
            'r' => 'call_method',
            'class' => 'ServerconfigController',
            'method' => 'setConfig',
            'params' => json_encode($vars),
        );


        //update 相对应的db的php
        $res = Helper::httpRequest($api_url, http_build_query($postData));
        die('success');
    }

    /**
     * 合服成功
     * @return [type] [description]
     */
    public function success() {
        $id = getParam('id');
        $tk = getParam('tk');

        if (empty($id)) {
            $code = Helper::getCode(8404);
            die($code);
        }

        if (empty($tk)) {
            $code = Helper::getCode(8404);
            die($code);            
        }

        $myTk = md5($id . '&' . $this->_key);

        if ($tk != $myTk) {
            $code = Helper::getCode(8406);
            die($code);
        }


        //@todo 修改为正式状态
        
        $this->db = new Model();

        $sql = "SELECT * FROM ny_merge WHERE id = '{$id}'";

        $res = $this->db->fetchOne($sql); 

        if (empty($res)) {
            die ('error id');
        }

        $server = $res['parent'] . ',' . $res['children'];

        $parent = $res['parent'];

        //更新
        $children = explode(',', $res['children']);

        foreach ($children as $sign) {
            $sql = "update ny_server set `type` = 2 ,  `merge_status` = 0, `mom_server` =  '{$parent}' WHERE server_id = '{$sign}'";
            $this->db->query($sql);
        }

        //母服
        $sql = "update ny_server set  `merge_status` = 0, `mom_server` =  '{$parent}' WHERE server_id = '{$parent}'";
        $this->db->query($sql);

        //更新任务
        
        $sql = "update ny_merge set status = 1 WHERE id = '{$id}' ";

        $this->db->query($sql);
        
        //判断第二次合服的
        foreach ($children as $sign) {
        	$sql = "update ny_server set `type` = 2 ,  `merge_status` = 0, `mom_server` =  '{$parent}' WHERE mom_server = '{$sign}'";
        	$this->db->query($sql);
        }
//         foreach ($children as $sign) {
//         	$sql = "SELECT children FROM ny_merge WHERE parent = '{$sign}' AND status = 1";
//         	$resSecond = $this->db->query($sql);
//         	if ($resSecond != array()) {
//         		foreach ($resSecond as $vv) {
//         			if ($vv['children']) {
//         				$Secondchildren = explode(',', $vv['children']);
//         				foreach ($Secondchildren as $k => &$v) {
//         					$v = "'{$v}'";
//         				}
//         				$allServers = implode(',', $Secondchildren);
//         				$sql = "update ny_server set `type` = 2 ,  `merge_status` = 0, `mom_server` =  '{$parent}' WHERE server_id in ({$allServers})";
//         				$this->db->query($sql);
//         				//         				foreach ($Secondchildren as $value) {
//         				//         					$sql = "update ny_server set `type` = 2 ,  `merge_status` = 0, `mom_server` =  '{$parent}' WHERE server_id = '{$value}'";
//         				//         					$this->db->query($sql);
//         				//         				}
//         			}
//         		}
//         	}
        
//         }
        
        die('success');
    }


}
