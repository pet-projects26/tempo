<?php
class BaseController{
    
    private $db;

    public function __construct() {

    }

    /**
     * [查找订单信息]
     * @param  [string] $orderNo [CP订单号]
     * @return [array]          [订单信息]
     */
    public function getOrderRow($orderNo) {
    	$this->db = new Model('order');
    	return $this->db->getRow('*', array('order_num' => $orderNo));
    }

    /**
     * [更新订单号]
     * @param  [array] $data   [更新数据]
     * @param  [type] $orderNo [订单号]
     * @return [type]          [description]
     */
    public function updateOrder($data, $orderNo) {
        $this->db = new Model('order');
    	return $this->db->update($data, array('order_num' => $orderNo));
    }

    /**
     * [向单服发货]
     * @param  [string] $server [服务器标识符]
     * @param  [array]  $params [参数]
     * @return  
     */
    public function sendGoods($server, $param = array()) {
    	$this->db = new Model('order');
    	$data = array(
    		'order_num' => $param['order_num'],
    		'corder_num' => $param['corder_num'],
            'role_name' => $param['role_name'],
    		'money' => $param['money'],
    		'account' => $param['account'],
    		'role_id' => $param['role_id'],
    		'role_level' => $param['role_level'],
    		'role_career' => $param['role_career'],
    		'gold' => $param['gold'],
    		'create_time' => time(),
    	);

    	$item_id = $param['item_id'];

    	$order_num = $data['order_num'];
    	$role_id = $data['role_id'];

    	//向单服进行发货, 写入mysql, 写入mongo 库
        $rs = $this->db->call('Charge' , 'addOrder' , array('data' => $data , 'item_id' => $item_id) , $server);


        $result = $rs[$server];

        //当为true的时候代表发货成功
        if($result['state']) {
            
            $serverRow = $this->findServerBySign($server);
            //发送指令告诉服务端有新订单
            $msg = $this->db->rpcCall($serverRow['ip'] , $serverRow['gm_port'] , 'order' , array('order_num' => $order_num , 'role_id' => $role_id));

            if($msg === 0) {

                //@todo 转发到PHP，用于统计
                /*$sendData = array(
                    'version' => '',
                    'serviceid' => $server,
                    'accountid' => $param['account'],
                    'roleid' => $param['role_id'],
                    'rolename' => $param['role_name'],
                    'level' => $param['role_level'],
                    'channel' => isset($param['channel']) ? $param['channel'] : '',
                    'package' => isset($param['package']) ? $param['package'] : '',
                    'module' => 'recharge',
                    'Amount' => $param['money'],
                    'orderID' => $param['order_num'],
                );

                $url = "http://dpyh.center.gzfengyou.com/api/sdk/point.php";

                $post_string = http_build_query($sendData);
                Helper::httpRequest($url, $post_string, 'get');*/

            	return 1; //成功
            } else {
            	return 2; //发货不成功
            }
        }

       return 3; //发货不成功
    }


    /**
     * [生成http 相对应的参数]
     * @param  [string]  $server_id  [服务器标识符]
     * @param  array   $data       [传入相对应的参数]
     * @param  boolean $encryption [是否需要相对应加密]
     * @return [array] 返回加密后的数据
     */
    public function makeRowHttpParams($server_id, $data = array(), $encryption = true) {
        $sql = "SELECT server_id, api_url, mdkey FROM ny_server_config WHERE server_id = '{$server_id}' LIMIT 1";
        
        $this->db = new Model('ny_server_config ');

        $row = $this->db->fetchOne($sql);

        if (empty($row)) {
            return array();
        }

        $time = time();
        $server_id = $row['server_id'];

        $url = $row['api_url'];
        $mdkey = $row['mdkey'];
        $url = str_replace('index.php', 'api.php', $url);

        $json  = json_encode($data);

        if ($encryption == true) {
            $md5 = Helper::authcode($json, 'ENCODE');
        } else {
            $md5 = $json;
        }
        
        $post = array('unixtime' => $time, 'data' => $md5);
        $post['sign'] = Helper::checkSign($post, $mdkey);

        return  array('urls' => $url, 'params' => $post);
    }

    /**
     * [查找服务器列表]
     * @param  [string] $server [服务器标识符]
     * @return array 
     */
    public function findServerBySign($server, $table = 'server_config') {
    	$this->db = new Model($table);
    	$row = $this->db->getRow('*', array('server_id' => $server));
    	return $row;
    }



    /**
     * [生产订单号]
     * @param  [int] $role_id [角色ID]
     * @return [type]          [description]
     */
    public function makeOrder($role_id) {
    	$len = strlen($role_id);
		if ($len < 4) {
			$num = 4 - $len;
			$str = '';
			for($i = 0 ; $i < $num; $i++) {
				$str .= 0;
			}
			$str .= $role_id;
		} else {
			$str = substr($role_id, -4);
		}

		//防止科学记数法出现
		$order_num = date('ymdhis') .'E'. $str  .rand(1000, 9999);


		return $order_num;
    }

    /**
     * [生成订单信息]
     * @param  array  $data  [要插入订表号]
     * @return [type]        [description]
     */
    public function makeOrderRow($data = array()) {
        Helper::log($_REQUEST, 'game',  __FUNCTION__);

        $server    = getParam('server');
        $channel   = getParam('channel');
        $role_id   = getParam('role_id');  
        $role_name = getParam('role_name');
        $productID = getParam('productID');
        $account   = getParam('account');

        $role_level = getParam('role_level');
        $money     = getParam('money'); 
        $package   = getParam('package');
        $gold      = getParam('gold');
        $extra     = getParam('extra');
        $role_career = getParam('role_career');

        if (empty($server) || empty($channel) || empty($role_id) || empty($role_name) || empty($productID) || empty($account) 
            || empty($role_level) || empty($money) || empty($package) ||  empty($gold)  || empty($role_career)) {
            
            $code = Helper::getCode(8404);
            die($code);
        }

        if (!is_numeric($money) || !is_numeric($role_id) || !is_numeric($role_level) || !is_numeric($role_career)) {
            $code = Helper::getCode(8407);
            die($code);
        }

        $order_num = $this->makeOrder($role_id);

        $charge = CDict::$charge;

        $gold = isset($charge[$productID]) ? $charge[$productID]['gold'] : false;

        if ($gold === false) {
            $code = Helper::getCode(8602);
            die($code);
        }

        $time = time();
        $insert = array(
            'server'=> $server,
            'channel' => $channel,
            'package' => $package,
            'order_num' => $order_num,
            'money' => $money,
            'account' => $account,
            'role_id' => $role_id,
            'role_name' => $role_name,
            'role_level' => $role_level,
            'role_career' => $role_career,
            'item_id' => $productID,
            'gold' => $gold,
            'create_time' => $time,
        );

        $m = new Model('order');
        $m->setAlias('ny_order');

        $extra = array();

        $insert = array_merge($data, $insert);

        $insertId = $m->add($insert);

        if (empty($insertId)) {
            $code  = Helper::getCode(8500);
            die($code);
        }

        return $insert;
    }
}
